<?php
/**
 * Plugin Name: Lufft
 * Plugin URI:  https://erstellbar.de
 * Description: Wordpress Plugins for Lufft
 * Version:     0.0.0
 * Author:      Sven Friedemann
 * Author URI:  https://erstellbar.de
 * Donate link: https://erstellbar.de
 * License:     GPLv2
 * Text Domain: lufft
 * Domain Path: /languages
 */

/**
 * Copyright (c) 2016 Sven Friedemann (email : sven@erstellbar.de)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2 or, at
 * your discretion, any later version, as published by the Free
 * Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * Built using generator-plugin-wp
 */

require_once __DIR__ . "/vendor/autoload.php";

define( 'LUFFT_MODULES_DIR', plugin_dir_path( __FILE__ ) . 'includes/modules/' );
define( 'LUFFT_MODULES_URL', plugins_url( 'includes/modules/', __FILE__ ) );


/**
 * Main initiation class
 *
 * @since  0.0.0
 * @var  string $version  Plugin version
 * @var  string $basename Plugin basename
 * @var  string $url      Plugin URL
 * @var  string $path     Plugin Path
 */
class Lufft {

	/**
	 * Current version
	 *
	 * @var  string
	 * @since  0.0.0
	 */
	const VERSION = '0.0.0';

	/**
	 * URL of plugin directory
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected $url = '';

	/**
	 * Path of plugin directory
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected $path = '';

	/**
	 * Plugin basename
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected $basename = '';

	/**
	 * Singleton instance of plugin
	 *
	 * @var Lufft
	 * @since  0.0.0
	 */
	protected static $single_instance = null;

	/**
	 * Instance of L_Calender
	 *
	 * @since 0.0.0
	 * @var L_Events
	 */
	protected $calender;

	/**
	 * Instance of L_Author
	 *
	 * @since NEXT
	 * @var L_Author
	 */
	protected $author;

	/**
	 * Instance of L_Ctp_Select
	 *
	 * @since NEXT
	 * @var L_Post
	 */
	protected $post;

	/**
	 * Instance of L_Category
	 *
	 * @since NEXT
	 * @var L_Category
	 */
	protected $category;

	/**
	 * Instance of L_Author_Module
	 *
	 * @since NEXT
	 * @var L_Author_Module
	 */
	protected $author_module;

	/**
	 * @var L_Role
	 */
	protected $role_tax;

	/**
	 * Creates or returns an instance of this class.
	 *
	 * @since  0.0.0
	 * @return Lufft A single instance of this class.
	 */
	public static function get_instance() {
		if ( null === self::$single_instance ) {
			self::$single_instance = new self();
		}

		return self::$single_instance;
	}

	/**
	 * Sets up our plugin
	 *
	 * @since  0.0.0
	 */
	protected function __construct() {
		$this->basename = plugin_basename( __FILE__ );
		$this->url      = plugin_dir_url( __FILE__ );
		$this->path     = plugin_dir_path( __FILE__ );

		$this->plugin_classes();
	}

	/**
	 * Attach other plugin classes to the base plugin class.
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function plugin_classes() {
		// Attach other plugin classes to the base plugin class.
		require( self::dir( 'includes/class-social-icons-widget.php' ) );
		require( self::dir( 'includes/class-guest-author-widget.php' ) );
		$this->calender = new L_Events( $this );
		require( self::dir( 'includes/class-events-widget.php' ) );
		$this->author = new L_Author( $this );
		$this->post = new L_Post( $this );
		$this->category = new L_Category( $this );
		$this->author_module = new L_Author_Module( $this );
		$this->role_tax = new L_Role();
	} // END OF PLUGIN CLASSES FUNCTION

	/**
	 * Add hooks and filters
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function hooks() {

		add_action( 'init', array( $this, 'init' ) );
	}

	/**
	 * Activate the plugin
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function _activate() {
		// Make sure any rewrite functionality has been loaded.
		flush_rewrite_rules();
	}

	/**
	 * Deactivate the plugin
	 * Uninstall routines should be in uninstall.php
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function _deactivate() {}

	/**
	 * Init hooks
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function init() {
		if ( $this->check_requirements() ) {
			load_plugin_textdomain( 'lufft', false, dirname( $this->basename ) . '/languages/' );
		}
	}

	/**
	 * Check if the plugin meets requirements and
	 * disable it if they are not present.
	 *
	 * @since  0.0.0
	 * @return boolean result of meets_requirements
	 */
	public function check_requirements() {
		if ( ! $this->meets_requirements() ) {

			// Add a dashboard notice.
			add_action( 'all_admin_notices', array( $this, 'requirements_not_met_notice' ) );

			// Deactivate our plugin.
			add_action( 'admin_init', array( $this, 'deactivate_me' ) );

			return false;
		}

		return true;
	}

	/**
	 * Deactivates this plugin, hook this function on admin_init.
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function deactivate_me() {
		deactivate_plugins( $this->basename );
	}

	/**
	 * Check that all plugin requirements are met
	 *
	 * @since  0.0.0
	 * @return boolean True if requirements are met.
	 */
	public static function meets_requirements() {
		// Do checks for required classes / functions
		// function_exists('') & class_exists('').
		// We have met all requirements.
		return true;
	}

	/**
	 * Adds a notice to the dashboard if the plugin requirements are not met
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function requirements_not_met_notice() {
		// Output our error.
		echo '<div id="message" class="error">';
		echo '<p>' . sprintf( __( 'Lufft is missing requirements and has been <a href="%s">deactivated</a>. Please make sure all requirements are available.', 'lufft' ), admin_url( 'plugins.php' ) ) . '</p>';
		echo '</div>';
	}

	/**
	 * Magic getter for our object.
	 *
	 * @since  0.0.0
	 * @param string $field Field to get.
	 * @throws Exception Throws an exception if the field is invalid.
	 * @return mixed
	 */
	public function __get( $field ) {
		switch ( $field ) {
			case 'version':
				return self::VERSION;
			case 'basename':
			case 'url':
			case 'path':
			case 'calender':
			case 'author':
			case 'ctp_select':
			case 'category':
			case 'author_module':
				return $this->$field;
			default:
				throw new Exception( 'Invalid '. __CLASS__ .' property: ' . $field );
		}
	}

	/**
	 * Include a file from the includes directory
	 *
	 * @since  0.0.0
	 * @param  string $filename Name of the file to be included.
	 * @return bool   Result of include call.
	 */
	public static function include_file( $filename ) {
		$file = self::dir( 'includes/class-'. $filename .'.php' );
		if ( file_exists( $file ) ) {
			return include_once( $file );
		}
		return false;
	}

	/**
	 * This plugin's directory
	 *
	 * @since  0.0.0
	 * @param  string $path (optional) appended path.
	 * @return string       Directory and path
	 */
	public static function dir( $path = '' ) {
		static $dir;
		$dir = $dir ? $dir : trailingslashit( dirname( __FILE__ ) );
		return $dir . $path;
	}

	/**
	 * This plugin's url
	 *
	 * @since  0.0.0
	 * @param  string $path (optional) appended path.
	 * @return string       URL and path
	 */
	public static function url( $path = '' ) {
		static $url;
		$url = $url ? $url : trailingslashit( plugin_dir_url( __FILE__ ) );
		return $url . $path;
	}
}

/**
 * Grab the Lufft object and return it.
 * Wrapper for Lufft::get_instance()
 *
 * @since  0.0.0
 * @return Lufft  Singleton instance of plugin class.
 */
function lufft() {
	return Lufft::get_instance();
}

// Kick it off.
add_action( 'plugins_loaded', array( lufft(), 'hooks' ) );

register_activation_hook( __FILE__, array( lufft(), '_activate' ) );
register_deactivation_hook( __FILE__, array( lufft(), '_deactivate' ) );
