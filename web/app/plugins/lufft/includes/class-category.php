<?php
/**
 * Lufft Category
 * @version 0.0.0
 * @package Lufft
 */


class L_Category {
	/**
	 * Parent plugin class
	 *
	 * @var   class
	 * @since NEXT
	 */
	protected $plugin = null;

	/**
	 * Constructor
	 *
	 * @since  NEXT
	 * @param  object $plugin Main plugin object.
	 * @return void
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
		$this->hooks();
	}

	/**
	 * Initiate our hooks
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function hooks() {
        add_action('cmb2-taxonomy_meta_boxes', array($this, 'fields'));
	}

    public function fields() {
        $prefix = 'l_category_';

        $meta_boxes['category_metabox'] = array(
            'id'            => $prefix . 'metabox',
            'title'         => __( 'Category Options', 'lufft' ),
            'object_types'  => array( 'category' ),
            'fields' => array(
                array(
                    'id' => $prefix . 'image',
                    'name' => __('Category Image', 'lufft'),
                    'type' => 'file'
                )
        ));

        return $meta_boxes;
    }
}
