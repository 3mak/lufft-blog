<?php
/**
 * Lufft Calender
 *
 * @version 0.0.0
 * @package Lufft
 */


class L_Events extends CPT_Core {
	/**
	 * Parent plugin class
	 *
	 * @var class
	 * @since  0.0.0
	 */
	protected $plugin = null;

	/**
	 * Constructor
	 * Register Custom Post Types. See documentation in CPT_Core, and in wp-includes/post.php
	 *
	 * @since  0.0.0
	 * @param  object $plugin Main plugin object.
	 * @return void
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
		$this->hooks();

		// Register this cpt
		// First parameter should be an array with Singular, Plural, and Registered name.
		parent::__construct(
			array( __( 'Event', 'lufft' ), __( 'Events', 'lufft' ), 'l-calender' ),
			array(
				'supports' => array( 'title' ),
				'has_archive' => false,
				'public' => false,
			)
		);
	}

	/**
	 * Initiate our hooks
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function hooks() {
		add_action( 'cmb2_init', array( $this, 'fields' ) );
	}

	/**
	 * Add custom fields to the CPT
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function fields() {
		$prefix = 'l_calender_';
		$cmb = new_cmb2_box( array(
			'id'            => $prefix . 'metabox',
			'title'         => __( 'Lufft Events Meta Box', 'lufft' ),
			'object_types'  => array( 'l-calender' ),
		) );

		$cmb->add_field(array(
			'id' => $prefix . 'start_date',
			'name' => __( 'Start date:', 'lufft' ),
			'desc' => __('When does the event begin?', 'lufft'),
			'type' => 'text_date_timestamp',
		));

		$cmb->add_field(array(
			'id' => $prefix . 'end_date',
			'name' => __( 'End date:', 'lufft' ),
				'desc' => __('When does the event ends?', 'lufft'),
			'type' => 'text_date_timestamp',
		));

		$cmb->add_field(array(
			'id' => $prefix . 'location',
			'name' => __( 'Location:', 'lufft' ),
			'type' => 'text',
		));


        $cmb->add_field(array(
            'id' => $prefix . 'URL',
            'name' => __( 'URL:', 'lufft' ),
            'type' => 'text',
        ));
	}

	/**
	 * Registers admin columns to display. Hooked in via CPT_Core.
	 *
	 * @since  0.0.0
	 * @param  array $columns Array of registered column names/labels.
	 * @return array          Modified array
	 */
	public function columns( $columns ) {
		$new_column = array();
		return array_merge( $new_column, $columns );
	}

	/**
	 * Handles admin column display. Hooked in via CPT_Core.
	 *
	 * @since  0.0.0
	 * @param array $column  Column currently being rendered.
	 * @param int   $post_id ID of post to display column for.
	 */
	public function columns_display( $column, $post_id ) {
	}
}
