<?php
/**
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 */
?>

<div class="author-module">
    <div class="author-image">
        <?php echo wp_get_attachment_image($settings->photo, 'author-thumbnail'); ?>
    </div>
    <div class="author-text">
        <h6 class="author-name"><?php echo $settings->name; ?></h6>
        <div class="author-position"><?php echo $settings->position; ?></div>
        <div class="icon-bar">
            <a href="mailto:<?php echo $settings->email; ?>" class="button email"></a>
            <a target="_blank" href="<?php echo $settings->linkedin; ?>" class="button linkedin"></a>
            <a href="<?php echo get_permalink($settings->link_with); ?>" class="round-button linked-author"><?php echo __('Articles', 'lufft'); ?> ›</a>
        </div>
    </div>
    <div class="author-biography">
        <?php echo nl2br($settings->bio); ?>
        <div class="fade"></div>
    </div><div class="open"></div>
</div>
