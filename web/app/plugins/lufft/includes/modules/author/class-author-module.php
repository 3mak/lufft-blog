<?php
/**
 * Lufft Author Module
 * @version 0.0.0
 * @package Lufft
 */

class L_Author_Module extends FLBuilderModule {

    /**
     * Constructor
     *
     * @since  NEXT
     */
	public function __construct() {

        parent::__construct(array(
            'name' => __('Author', 'lufft'),
            'description' => __('Lufft Author', 'lufft'),
            'category' => __('Lufft Modules', 'lufft'),
            'dir' => LUFFT_MODULES_DIR . 'author/',
            'url' => LUFFT_MODULES_URL . 'author/'
        ));
		$this->hooks();
	}

	/**
	 * Initiate our hooks
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function hooks() {

	}
}


FLBuilder::register_module('L_Author_Module', array(
    'general' => array(
        'title' => __('General', 'lufft'),
        'sections' => array(
            'author' => array(
                'title' => __('Author', 'lufft'),
                'fields' => array(
                    'name' => array(
                        'type' => 'text',
                        'label' => 'Author Name',
                    ),
                    'position' => array(
                        'type' => 'text',
                        'label' => 'Author Name',
                    ),
                    'email' => array(
                        'type' => 'text',
                        'label' => 'Author Email',
                    ),
                    'linkedin' => array(
                        'type' => 'text',
                        'label' => 'LinkedIn URL',
                    ),
                    'photo' => array(
                        'type' => 'photo',
                        'label' => 'Author Photo',
                    ),
                    'bio' => array(
                        'type' => 'editor',
                        'label' => 'Author Bioagraphy',
                    ),
                    'link_with' => array(
                        'type'          => 'suggest',
                        'label'         => __( 'Linked Author', 'fl-builder' ),
                        'action'        => 'fl_as_posts', // Search posts.
                        'data'          => 'l-author', // Slug of the post type to search.
                        'limit'         => 1, // Limits the number of selections that can be made.
                    ),
                )
            )
        )
    )
));
