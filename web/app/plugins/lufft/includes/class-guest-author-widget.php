<?php
/**
 * Lufft Guest Author
 * @version 0.0.0
 * @package Lufft
 */

class L_Guest_Author_Widget extends WP_Widget {

	/**
	 * Unique identifier for this widget.
	 *
	 * Will also serve as the widget class.
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected $widget_slug = 'lufft-guest-author';


	/**
	 * Widget name displayed in Widgets dashboard.
	 * Set in __construct since __() shouldn't take a variable.
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected $widget_name = '';


	/**
	 * Default widget title displayed in Widgets dashboard.
	 * Set in __construct since __() shouldn't take a variable.
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected $default_widget_title = '';


	/**
	 * Shortcode name for this widget
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected static $shortcode = 'lufft-guest-author';


	/**
	 * Construct widget class.
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function __construct() {

		$this->widget_name          = esc_html__( 'Lufft Guest Author', 'lufft' );
		$this->default_widget_title = esc_html__( 'Lufft Guest Author', 'lufft' );

		parent::__construct(
			$this->widget_slug,
			$this->widget_name,
			array(
				'classname'   => $this->widget_slug,
				'description' => esc_html__( 'Guest Author Widget.', 'lufft' ),
			)
		);

		add_action( 'save_post',    array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );
		add_shortcode( self::$shortcode, array( __CLASS__, 'get_widget' ) );
	}


	/**
	 * Delete this widget's cache.
	 *
	 * Note: Could also delete any transients
	 * delete_transient( 'some-transient-generated-by-this-widget' );
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function flush_widget_cache() {
		wp_cache_delete( $this->widget_slug, 'widget' );
	}


	/**
	 * Front-end display of widget.
	 *
	 * @since  0.0.0
	 * @param  array $args     The widget arguments set up when a sidebar is registered.
	 * @param  array $instance The widget settings as set by user.
	 * @return void
	 */
	public function widget( $args, $instance ) {
		echo self::get_widget( array(
			'before_widget' => $args['before_widget'],
			'after_widget'  => $args['after_widget'],
			'before_title'  => $args['before_title'],
			'after_title'   => $args['after_title'],
			'title'         => apply_filters('widget_title', $instance['title']),
			'text'          => apply_filters('wpml_translate_single_string', $instance['text'], 'Guest author widget', 'Widget Text'),
			'button_text'   => apply_filters('wpml_translate_single_string', $instance['button_text'], 'Guest author widget', 'Link Text'),
			'link_target'   => $instance['link_target'],
		) );
	}


	/**
	 * Return the widget/shortcode output
	 *
	 * @since  0.0.0
	 * @param  array $atts Array of widget/shortcode attributes/args.
	 * @return string       Widget output
	 */
	public static function get_widget( $atts ) {
		$widget = '';
		// Set up default values for attributes.
		$atts = shortcode_atts(
			array(
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
				'title'         => '',
				'text'          => '',
				'link_target'   => '',
				'button_text'   => '',
			),
			(array) $atts,
			self::$shortcode
		);

		// Before widget hook.
		$widget .= $atts['before_widget'];

		// Title.
		$widget .= ( $atts['title'] ) ? $atts['before_title'] . esc_html( $atts['title'] ) . $atts['after_title'] : '';

		$widget .= '<div class="guest-author-icon"></div>';
		$widget .= '<div class="guest-author-text">' . wpautop( wp_kses_post( $atts['text'] ) ) . '</div>';
		$widget .= '<div class="guest-author-button"><a href="' . get_permalink($atts['link_target']) . '">' . esc_html($atts['button_text']) . '</a></div>';

		// After widget hook.
		$widget .= $atts['after_widget'];

		return $widget;
	}


	/**
	 * Update form values as they are saved.
	 *
	 * @since  0.0.0
	 * @param  array $new_instance New settings for this instance as input by the user.
	 * @param  array $old_instance Old settings for this instance.
	 * @return array               Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {

		// Previously saved values.
		$instance = $old_instance;

		// Sanitize title before saving to database.
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['link_target'] = sanitize_text_field( $new_instance['link_target'] );
		$instance['button_text'] = sanitize_text_field( $new_instance['button_text'] );

		// Sanitize text before saving to database.
		if ( current_user_can( 'unfiltered_html' ) ) {
			$instance['text'] = force_balance_tags( $new_instance['text'] );
		} else {
            $instance['text'] = stripslashes(wp_filter_post_kses(addslashes($new_instance['text'])));
        }

		do_action('wpml_register_single_string', 'Guest author widget', 'Widget Text', $instance['text']);
    do_action('wpml_register_single_string', 'Guest author widget', 'Link Text', $instance['button_text']);

		// Flush cache.
		$this->flush_widget_cache();

		return $instance;
	}


	/**
	 * Back-end widget form with defaults.
	 *
	 * @since  0.0.0
	 * @param  array $instance Current settings.
	 * @return void
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance,
			array(
				'title' => $this->default_widget_title,
				'text'  => '',
				'link_target' => '',
				'button_text' => '',
			)
		);

		?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'lufft'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>"
				   name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text"
				   value="<?php echo esc_html($instance['title']); ?>" placeholder="optional"/>
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('text')); ?>"><?php esc_html_e('Text:', 'lufft'); ?></label>
			<textarea class="widefat" rows="16" cols="20" id="<?php echo esc_attr($this->get_field_id('text')); ?>"
					  name="<?php echo esc_attr($this->get_field_name('text')); ?>"><?php echo esc_textarea($instance['text']); ?></textarea>
		</p>
		<p class="description"><?php esc_html_e('Basic HTML tags are allowed.', 'lufft'); ?></p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('button_text')); ?>"><?php esc_html_e('Button Text:', 'lufft'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('button_text')); ?>"
				   name="<?php echo esc_attr($this->get_field_name('button_text')); ?>" type="text"
				   value="<?php echo esc_html($instance['button_text']); ?>" placeholder="optional"/>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('link_target'); ?>"><?php _e('CTA Link Target:', 'lufft'); ?></label>
			<?php wp_dropdown_pages(array('id' => $this->get_field_id('link_target'), 'name' => $this->get_field_name('link_target'))); ?>
		</p>
		<?php
	}
}


/**
 * Register this widget with WordPress. Can also move this function to the parent plugin.
 *
 * @since  0.0.0
 * @return void
 */
function register_lufft_guest_author() {
	register_widget( 'L_Guest_Author_Widget' );
}
add_action( 'widgets_init', 'register_lufft_guest_author' );
