<?php
/**
 * Social Icons Frontend
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 * @package Lufft
 */

?>
<?php echo $args['before_widget']; ?>
<ul class="social-media-icons">
    <?php foreach (L_Social_Icons_Widget::$social_media_channels as $slug => $name ) : ?>
        <li><a href="<?php echo $instance[ $slug ]; ?>" target="_blank"><i class="fa fa-<?php echo $slug ?>"></i></a></li>
    <?php endforeach; ?>
</ul>
<?php echo $args['after_widget']; ?>
