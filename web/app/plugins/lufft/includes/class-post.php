<?php
/**
 * Lufft Ctp Select
 * @version 0.0.0
 * @package Lufft
 */

class L_Post {
	/**
	 * Parent plugin class
	 *
	 * @var   class
	 * @since NEXT
	 */
	protected $plugin = null;

	/**
	 * Constructor
	 *
	 * @since  NEXT
	 * @param  object $plugin Main plugin object.
	 * @return void
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
		$this->hooks();
	}

	/**
	 * Initiate our hooks
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function hooks() {
		add_action('cmb2_init', array($this, 'fields'));
	}

	public function fields() {
		$prefix = 'l_post_';
		$cmb = new_cmb2_box( array(
				'id'            => $prefix . 'metabox',
				'title'         => __( 'Author Options', 'lufft' ),
				'object_types'  => array( 'post' ),
				'context' => 'side',
				'priority' => 'low'
		) );


		$cmb->add_field(array(
			'name' => __( 'Author', 'lufft' ),
			'desc' => __( 'Overrides Wordpress author' ),
			'id' => $prefix . 'author',
			'type' => 'select',
			'options_cb' => array($this, 'cmb2_get_your_post_type_post_options'),
		));

		$cmb_related_links = new_cmb2_box( array(
				'id'            => $prefix . 'related_links',
				'title'         => __( 'Related Links', 'lufft' ),
				'object_types'  => array( 'post' ),
		) );

		$group_field_id = $cmb_related_links->add_field( array(
				'id'          => 'related_links_group',
				'type'        => 'group',
				'description' => __( 'Add related Links to bottom of article', 'lufft' ),
			// 'repeatable'  => false, // use false if you want non-repeatable group
				'options'     => array(
						'group_title'   => __( 'Link {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
						'add_button'    => __( 'Add another link', 'cmb2' ),
						'remove_button' => __( 'Remove link', 'cmb2' ),
						'sortable'      => true, // beta
						'closed'     => true, // true to have the groups closed by default
				),
		) );

		$cmb_related_links->add_group_field($group_field_id, array(
				'name' => __( 'Link Name', 'lufft' ),
				'id' => $prefix . 'link_name',
				'type' => 'text'
		));

		$cmb_related_links->add_group_field($group_field_id, array(
				'name' => __( 'Link URL', 'lufft' ),
				'id' => $prefix . 'link_url',
				'type' => 'text_url',
				'protocols' => array( 'http', 'https'),
		));


        $cmb_subject = new_cmb2_box( array(
            'id'           => $prefix.'subject-metabox',
            'title'        => __('Suggest Subject Widget', 'lufft'),
            'object_types' => array( 'page' ), // post type
            'show_on'      => array( 'key' => 'page-template', 'value' => 'page-templates/subjects.php' ),
            'context'      => 'normal', //  'normal', 'advanced', or 'side'
            'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
            'show_names'   => true, // Show field names on the left
        ) );

        $cmb_subject->add_field(array(
            'name' => __('Image', 'lufft'),
            'id' => $prefix.'sub_image',
            'type' => 'file_list',
        ));

        $cmb_subject->add_field(array(
            'name' => __('Title', 'lufft'),
            'id' => $prefix.'sub_title',
            'type' => 'text',
        ));

        $cmb_subject->add_field(array(
            'name' => __('Text', 'lufft'),
            'id' => $prefix.'sub_text',
            'type' => 'textarea',
        ));

	}

	/**
	 * Gets a number of posts and displays them as options
	 * @param  array $query_args Optional. Overrides defaults.
	 * @return array             An array of options that matches the CMB2 options array
	 */
	protected function cmb2_get_post_options( $query_args ) {

		$args = wp_parse_args( $query_args, array(
				'post_type'   => 'post',
				'numberposts' => 10,
				'order' => 'ASC',
				'orderby' => 'title',
		) );

		$posts = get_posts( $args );

		$post_options = array();
		if ( $posts ) {
			foreach ( $posts as $post ) {
				$post_options[ $post->ID ] = $post->post_title;
			}
		}

		return $post_options;
	}

	/**
	 * Gets 5 posts for your_post_type and displays them as options
	 * @return array An array of options that matches the CMB2 options array
	 */
	public function cmb2_get_your_post_type_post_options($field) {
		return $this->cmb2_get_post_options( array( 'post_type' => 'l-author', 'numberposts' => 99, 'suppress_filters' => false ) );
	}
}
