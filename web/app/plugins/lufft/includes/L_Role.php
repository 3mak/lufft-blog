<?php

/**
 * Created by PhpStorm.
 * User: emak
 * Date: 28.06.16
 * Time: 16:00
 */
class L_Role extends Taxonomy_Core {

    public function __construct()
    {
        $this->hooks();

        parent::__construct(array('Role', 'Roles', 'l-role'), array('
                hierarchical' => false
            ),
            $object_types = array()
        );
    }

    public function hooks() {
        add_action('cmb2-taxonomy_meta_boxes', array($this, 'fields'));
    }

    public function fields() {
        $prefix = 'l_role_';

        $meta_boxes['role_metabox'] = array(
            'id'            => $prefix . 'metabox',
            'title'         => __( 'Role Options', 'lufft' ),
            'object_types'  => array( 'l-role' ),
            'fields' => array(
                array(
                    'id' => $prefix . 'page',
                    'name' => __('More Info Link', 'lufft'),
                    'type' => 'select',
                    'options_cb' => array($this, 'cmb2_get_your_post_type_post_options')
                )
            ));

        return $meta_boxes;
    }

    /**
     * Gets a number of posts and displays them as options
     * @param  array $query_args Optional. Overrides defaults.
     * @return array             An array of options that matches the CMB2 options array
     */
    public function cmb2_get_post_options( $query_args ) {

        $args = wp_parse_args( $query_args, array(
            'post_type'   => 'post',
            'numberposts' => 10,
            'suppress_filters' => 0
        ) );

        $posts = get_posts( $args );

        $post_options = array();
        if ( $posts ) {
            foreach ( $posts as $post ) {
                $post_options[ $post->ID ] = $post->post_title;
            }
        }

        return $post_options;
    }

    /**
     * Gets 5 posts for your_post_type and displays them as options
     * @return array An array of options that matches the CMB2 options array
     */
    public function cmb2_get_your_post_type_post_options() {
        return $this->cmb2_get_post_options( array( 'post_type' => 'page', 'numberposts' => -1 ) );
    }

}