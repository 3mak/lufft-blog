<?php
/**
 * Lufft Events
 * @version 0.0.0
 * @package Lufft
 */

class L_Events_Widget extends WP_Widget {

	/**
	 * Unique identifier for this widget.
	 *
	 * Will also serve as the widget class.
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected $widget_slug = 'lufft-events';


	/**
	 * Widget name displayed in Widgets dashboard.
	 * Set in __construct since __() shouldn't take a variable.
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected $widget_name = '';


	/**
	 * Default widget title displayed in Widgets dashboard.
	 * Set in __construct since __() shouldn't take a variable.
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected $default_widget_title = '';


	/**
	 * Shortcode name for this widget
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected static $shortcode = 'lufft-events';

    /**
     * Custom post type slug of events
     *
     * @var string
     */
    protected static $event_cpt = 'l-calender';


	/**
	 * Construct widget class.
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function __construct() {

		$this->widget_name          = esc_html__( 'Lufft Events', 'lufft' );
		$this->default_widget_title = esc_html__( 'Lufft Events', 'lufft' );

		parent::__construct(
			$this->widget_slug,
			$this->widget_name,
			array(
				'classname'   => $this->widget_slug,
				'description' => esc_html__( 'List Lufft events.', 'lufft' ),
			)
		);

		add_action( 'save_post',    array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );
		add_shortcode( self::$shortcode, array( __CLASS__, 'get_widget' ) );
	}


	/**
	 * Delete this widget's cache.
	 *
	 * Note: Could also delete any transients
	 * delete_transient( 'some-transient-generated-by-this-widget' );
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function flush_widget_cache() {
		wp_cache_delete( $this->widget_slug, 'widget' );
	}


	/**
	 * Front-end display of widget.
	 *
	 * @since  0.0.0
	 * @param  array $args     The widget arguments set up when a sidebar is registered.
	 * @param  array $instance The widget settings as set by user.
	 * @return void
	 */
	public function widget( $args, $instance ) {

		echo self::get_widget( array(
			'before_widget' => $args['before_widget'],
			'after_widget'  => $args['after_widget'],
			'before_title'  => $args['before_title'],
			'after_title'   => $args['after_title'],
			'title'         => apply_filters('widet_title', $instance['title']),
			'link_target'   => apply_filters('wpml_translate_single_string', $instance['link_target'], 'Event Widget', 'Event Link Target'),
			'link_text'     => apply_filters('wpml_translate_single_string', $instance['link_text'], 'Event Widget', 'Event Link Text'),
            'event_count'   => $instance['event_count'],
		) );
	}


	/**
	 * Return the widget/shortcode output
	 *
	 * @since  0.0.0
	 * @param  array $atts Array of widget/shortcode attributes/args.
	 * @return string       Widget output
	 */
	public static function get_widget( $atts ) {
		$widget = '';

        $query = new WP_Query(array(
            'post_type' => self::$event_cpt,
            'posts_per_page' => (int)$atts['event_count'],
						// 'orderby' => 'ID',
						// 'order' => 'ASC'
        ));

		// Before widget hook.
		$widget .= $atts['before_widget'];

		// Title.
		$widget .= ( $atts['title'] ) ? $atts['before_title'] . esc_html( $atts['title'] ) . $atts['after_title'] : '';

        while ($query->have_posts() ) {
            $query->the_post();
            $start_date = get_post_meta(get_the_ID(), 'l_calender_start_date', true);
            $end_date = get_post_meta(get_the_ID(), 'l_calender_end_date', true);

            $widget .= "<div class='event'>";
            $widget .= "<div class='event-date'>";
            $widget .= "<time datetime='" . date('c', $start_date). "'>" . date(__('d.m.Y'), $start_date). '</time> - ';
            $widget .= "<time datetime='" . date('c', $end_date). "'>" . date(__('d.m.Y'), $end_date). '</time>';
            $widget .= '</div>';
            $widget .= "<div class='event-title'>" . get_the_title() . '</div>';
            $widget .= "<div class='event-location'>" . get_post_meta(get_the_ID(), 'l_calender_location', true) . '</div>';
			$widget .= "<a href='" . get_post_meta(get_the_ID(), 'l_calender_URL', true). "' target='_blank'></a>";
            $widget .= '</div>';
        }
        $widget .= "<a class='event-more round-button white expand' target='_blank' href=\"{$atts['link_target']}\">" . esc_html($atts['link_text']) . "</a>";

		// After widget hook.
		$widget .= $atts['after_widget'];

        wp_reset_postdata();

		return $widget;
	}


	/**
	 * Update form values as they are saved.
	 *
	 * @since  0.0.0
	 * @param  array $new_instance New settings for this instance as input by the user.
	 * @param  array $old_instance Old settings for this instance.
	 * @return array               Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {

		// Previously saved values.
		$instance = $old_instance;

		// Sanitize title before saving to database.
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['link_target'] = sanitize_text_field( $new_instance['link_target'] );
		$instance['link_text'] = sanitize_text_field( $new_instance['link_text'] );
        $instance['event_count'] = sanitize_text_field( $new_instance['event_count'] );

		// Sanitize text before saving to database.
		if ( current_user_can( 'unfiltered_html' ) ) {
			$instance['text'] = force_balance_tags( $new_instance['text'] );
		} else {
			$instance['text'] = stripslashes( wp_filter_post_kses( addslashes( $new_instance['text'] ) ) );
		}

        if(function_exists('icl_register_string')) {
            icl_register_string('Event Widget', 'Event Link Text', $instance['link_text']);
            icl_register_string('Event Widget', 'Event Link Target', $instance['link_target']);
        }

		// Flush cache.
		$this->flush_widget_cache();

		return $instance;
	}


	/**
	 * Back-end widget form with defaults.
	 *
	 * @since  0.0.0
	 * @param  array $instance Current settings.
	 * @return void
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance,
			array(
				'title' => $this->default_widget_title,
				'text' => '',
				'link_text' => _('Show All'),
				'link_target' => '',
                'event_count' => 3,
			)
		);

		?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'lufft'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>"
				   name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text"
				   value="<?php echo esc_html($instance['title']); ?>" placeholder="optional"/>
		</p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('event_count')); ?>"><?php esc_html_e('Count of displayed events:', 'lufft'); ?></label>
            <input class="tiny-text" id="<?php echo esc_attr($this->get_field_id('event_count')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('event_count')); ?>" type="number"
                   value="<?php echo esc_html($instance['event_count']); ?>" placeholder="1"/>
        </p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('link_text')); ?>"><?php esc_html_e('Link Text:', 'lufft'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('link_text')); ?>"
				   name="<?php echo esc_attr($this->get_field_name('link_text')); ?>" type="text"
				   value="<?php echo esc_html($instance['link_text']); ?>" placeholder="Link Text"/>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('link_target'); ?>"><?php _e('Link Target:', 'lufft'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('link_target')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('link_target')); ?>" type="text"
                   value="<?php echo esc_html($instance['link_target']); ?>" placeholder="Link Target"/>
		</p>
		<?php
	}
}


/**
 * Register this widget with WordPress. Can also move this function to the parent plugin.
 *
 * @since  0.0.0
 * @return void
 */
function register_lufft_events() {
	register_widget( 'L_Events_Widget' );
}
add_action( 'widgets_init', 'register_lufft_events' );
