<?php
/**
 * Lufft Social Icons
 * @version 0.0.0
 * @package Lufft
 */

class L_Social_Icons_Widget extends WP_Widget {

	/**
	 * Unique identifier for this widget.
	 *
	 * Will also serve as the widget class.
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected $widget_slug = 'lufft-social-icons';


	/**
	 * Widget name displayed in Widgets dashboard.
	 * Set in __construct since __() shouldn't take a variable.
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected $widget_name = '';


	/**
	 * Default widget title displayed in Widgets dashboard.
	 * Set in __construct since __() shouldn't take a variable.
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected $default_widget_title = '';


	/**
	 * Shortcode name for this widget
	 *
	 * @var string
	 * @since  0.0.0
	 */
	protected static $shortcode = 'lufft-social-icons';

	/**
	 * Available Social Media channels
	 *
	 * @var array
	 */
	public static $social_media_channels = array(
			'facebook' => 'Facebook',
			'twitter' => 'Twitter',
			'instagram' => 'Instagram',
			'linkedin'	=> 'LinkedIn',
			'youtube' => 'Youtube',
	);


	/**
	 * Construct widget class.
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function __construct() {

		$this->widget_name          = esc_html__( 'Lufft Social Icons', 'lufft' );
		$this->default_widget_title = esc_html__( 'Lufft Social Icons', 'lufft' );

		parent::__construct(
			$this->widget_slug,
			$this->widget_name,
			array(
				'classname'   => $this->widget_slug,
				'description' => esc_html__( 'Display Social Channels.', 'lufft' ),
			)
		);

		add_action( 'save_post',    array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );
		add_shortcode( self::$shortcode, array( __CLASS__, 'get_widget' ) );
	}


	/**
	 * Delete this widget's cache.
	 *
	 * Note: Could also delete any transients
	 * delete_transient( 'some-transient-generated-by-this-widget' );
	 *
	 * @since  0.0.0
	 * @return void
	 */
	public function flush_widget_cache() {
		wp_cache_delete( $this->widget_slug, 'widget' );
	}


	/**
	 * Front-end display of widget.
	 *
	 * @since  0.0.0
	 * @param  array $args     The widget arguments set up when a sidebar is registered.
	 * @param  array $instance The widget settings as set by user.
	 * @return void
	 */
	public function widget( $args, $instance ) {
		include __DIR__ . '/social-icons/frontend.php';
	}


	/**
	 * Update form values as they are saved.
	 *
	 * @since  0.0.0
	 * @param  array $new_instance New settings for this instance as input by the user.
	 * @param  array $old_instance Old settings for this instance.
	 * @return array               Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {

		// Previously saved values.
		$instance = $old_instance;

		// Sanitize title before saving to database.
		foreach (self::$social_media_channels as $slug => $name ) {
			$instance[ $slug ] = sanitize_text_field( $new_instance[ $slug ] );
		}

		// Flush cache.
		$this->flush_widget_cache();

		return $instance;
	}


	/**
	 * Back-end widget form with defaults.
	 *
	 * @since  0.0.0
	 * @param  array $instance Current settings.
	 * @return void
	 */
	public function form( $instance ) {

		$instance = wp_parse_args( (array) $instance,
			array(
				'title' => $this->default_widget_title,
				'text'  => '',
			)
		); ?>

		<p>Bitte füllen Sie die Entsprechende Urls in die passenden Felder</p>

		<?php foreach (self::$social_media_channels as $slug => $name ) { ?>
			<p><label for="<?php echo esc_attr( $this->get_field_id( $slug ) ); ?>"><?php esc_html_e( $name.':', 'lufft' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( $slug ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( $slug ) ); ?>" type="text" value="<?php echo esc_html( $instance[ $slug ] ); ?>" placeholder="http://<?php echo $slug?>.com" /></p>
		<?php }
	}
}


/**
 * Register this widget with WordPress. Can also move this function to the parent plugin.
 *
 * @since  0.0.0
 * @return void
 */
function register_lufft_social_icons() {
	register_widget( 'L_Social_Icons_Widget' );
}
add_action( 'widgets_init', 'register_lufft_social_icons' );
