<?php

class L_Calender_Test extends WP_UnitTestCase {

	function test_sample() {
		// replace this with some actual testing code
		$this->assertTrue( true );
	}

	function test_class_exists() {
		$this->assertTrue( class_exists('L_Events') );
	}

	function test_class_access() {
		$this->assertTrue( lufft()->calender instanceof L_Events );
	}

  function test_cpt_exists() {
    $this->assertTrue( post_type_exists( 'l-calender' ) );
  }
}
