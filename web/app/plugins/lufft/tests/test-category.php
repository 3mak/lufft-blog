<?php

class L_Category_Test extends WP_UnitTestCase {

	function test_sample() {
		// replace this with some actual testing code
		$this->assertTrue( true );
	}

	function test_class_exists() {
		$this->assertTrue( class_exists( 'L_Category') );
	}

	function test_class_access() {
		$this->assertTrue( lufft()->category instanceof L_Category );
	}
}
