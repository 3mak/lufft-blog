��    :      �  O   �      �     �  ,   	     6     B     R  *   `     �  
   �     �     �     �     �     �     �     �     �          +  	   3     =     C     J     P     X  	   m     w  
   �     �     �     �  	   �     �     �     �     �     �          "     0     C     J     P  
   e     p  	   ~     �     �     �     �     �     �     �     �     �     �     �       �       �	  0   �	     
     
     /
  0   B
     s
     z
     �
     �
     �
     �
     �
  $   �
     �
  )   �
          &  	   4     >     L     \  	   c     m     �  	   �  	   �     �     �      �     �     �     �     �          *     I     Z     g     z     �     �  
   �     �     �  
   �     �     �     �     �                              >     \               5              1   9              
   !          *      '   /   4   )                           :      -          .      ,         7                    $   (      8      &          2              "           +              0       %             #   3   6                	    %1$s published. %1$s published. <a href="%2$s">View %1$s</a> %1$s saved. %1$s submitted. %1$s updated. %1$s updated. <a href="%2$s">View %1$s</a> %s Title Add New %s All %s Articles Author Author Options Authors Basic HTML tags are allowed. Button Text: Count of displayed events: Display Social Channels. Edit %s End date: Event Events File: General Guest Author Widget. Link Name Link Target: Link Text: Link {#} Linked Author List Lufft events. Location: Lufft Lufft Author Lufft Author Meta Box Lufft Events Lufft Events Meta Box Lufft Guest Author Lufft Modules Lufft Social Icons New %s No %s No %s found in Trash Parent %s: Related Links Search %s Show All Start date: Suggest Subject Widget Text Text: Time Title Title: View %s When does the event begin? When does the event ends? d.m.Y Project-Id-Version: Lufft 0.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/lufft
POT-Creation-Date: 2016-06-24 11:15+0200
PO-Revision-Date: 2016-06-24 11:16+0200
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: .
 %1$s gespeichert. %1$s aktualisiert. <a href="%2$s">Zeige %1$s</a> %1$s gespeichert. %1$s gespeichert. %1$s aktualisiert. %1$s aktualisiert. <a href="%2$s">Zeige %1$s</a> Titel: %s erstellen Alle %s Artikel Autor Autor Optionen Autoren Grundlegende HTML Tags sind erlaubt. Buttontext: Anzahl der anzuzeigenden Veranstaltungen: Zeigt Soziale Kanäle. %s bearbeiten Enddatum: Veranstaltung Veranstaltungen Datei: Allgemein Lufft Gast Autor Widget Linkname Linkziel: Linktext: Link {#} verknüpfter Autor Liste von Lufft Veranstaltungen. Ort: LUFFT Lufft Autor Lufft Veranstaltungen Meta Box Lufft Veranslteungen Lufft Veranstaltungen Meta Box Lufft Gast Autor Lufft Module Lufft Social Icons Neue %s Keine %s Keine %s im Papierkorb Eltern %s: Verwandte Links Suche %s Zeige Alle Startdatum: Thema Vorschlagen Widget Text: Text: Zeit Titel: Titel: Zeige %s Wann beginnt die Veranstaltung? Wann endet die Veranstaltung? d.m.Y 