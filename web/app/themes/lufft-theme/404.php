<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div class="error-page">
    <div class="content">
        <div class="big-error">404</div>
        <h1><?php _e('Page not found', 'foundationpress'); ?></h1>
        <p><?php echo sprintf(__('Please excuse. The page you requested is not available. Please go back to the start page by using the button or leave a message through our <a href="%s">contact form</a>. Your Lufft Team.', 'foundationpress'), get_permalink(10758)); ?></p>
        <a href="<?php echo home_url(); ?>" class="round-button back"><?php _e('Back to home');?></a>
    </div>
</div>
<?php get_footer();
