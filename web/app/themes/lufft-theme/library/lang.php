<?php
/**
 * Created by PhpStorm.
 * User: emak
 * Date: 18.05.16
 * Time: 20:25
 */

function is_default_lang() {
    global $sitepress;
    return $sitepress->get_default_language() == ICL_LANGUAGE_CODE;
}