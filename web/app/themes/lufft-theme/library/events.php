<?php
/**
 *  Event Queryies
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 * @package  Foundationpress
 */

if ( ! function_exists('get_current_events') ) {
    function get_current_events() {
        $query = array(
            'post_type' => 'l-calender',
            'posts_per_page' => 24,
            'meta_query' => array(
                array(
                    'key' => 'l_calender_end_date',
                    'value' => time(),
                    'compare' => '>',
                ),
            ),
        );

        return new WP_Query($query);
    }
}

if ( ! function_exists('get_past_events') ) {
    function get_past_events() {
        $query = array(
            'post_type' => 'l-calender',
            'posts_per_page' => 24,
            'meta_query' => array(
                array(
                    'key' => 'l_calender_end_date',
                    'value' => time(),
                    'compare' => '<',
                ),
            ),
        );

        return new WP_Query($query);
    }
}
