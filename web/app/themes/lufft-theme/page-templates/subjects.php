 <?php
/*
Template Name: Subjects
*/
get_header(); ?>

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
<div id="subjects" role="main">
		<div <?php post_class('subject-content') ?> id="post-<?php the_ID(); ?>">
			<?php do_action( 'foundationpress_page_before_entry_content' ); ?>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<footer>
				<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
				<p><?php the_tags(); ?></p>
			</footer>
			<?php do_action( 'foundationpress_page_before_comments' ); ?>
			<?php comments_template(); ?>
			<?php do_action( 'foundationpress_page_after_comments' ); ?>
		</div>
        <div class="subjects-container row medium-up-2">
            <?php $subjects = get_categories('hide_empty=1');?>
            <?php foreach ($subjects as $subject ) : ?>
                <?php if ($subject->term_id === apply_filters('wpml_object_id', $subject->term_id, 'category', false) ) : ?>
                    <article class="subject column">
                        <?php $image_id = get_metadata('term', $subject->term_id, 'l_category_image_id', true); ?>
                        <?php echo wp_get_attachment_image($image_id, 'medium'); ?>
                        <h3 class="subject-title"><?php echo $subject->name?></h3>
                        <p class="subject-description"><?php echo $subject->category_description; ?></p>
                        <a class="overlay-anchor" href="<?php echo get_term_link($subject->term_id); ?>"></a>
                    </article>
                <?php endif; ?>
            <?php endforeach; ?>
            <article class="subject suggest column">
                <?php $subject_img = get_post_meta(get_the_ID(), 'l_post_sub_image', true); ?>
                <?php echo wp_get_attachment_image(key($subject_img), 'medium'); ?>
                <div class="text">
                    <h3 class="subject-title"><?php echo get_post_meta(get_the_ID(), 'l_post_sub_title', true)?></h3>

                    <p class="subject-description">
                        <?php echo get_post_meta(get_the_ID(), 'l_post_sub_text', true)?>
                    </p>
                    <a class="round-button white" href="<?php echo get_permalink(icl_object_id(10657, 'page', true)); ?>">
                        <?php _e('Suggest subject', 'foundationpress'); ?> ›
                    </a>
                </div>
            </article>
        </div>

</div>
<?php endwhile;?>
<?php do_action( 'foundationpress_after_content' ); ?>





<?php get_footer();
