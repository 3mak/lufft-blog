<?php
/*
Template Name: Events
*/
get_header(); ?>

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
<div id="events" role="main" class="lufft-events">
		<div <?php post_class('calender-content') ?> id="post-<?php the_ID(); ?>">
			<?php do_action( 'foundationpress_page_before_entry_content' ); ?>
            <header>
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<footer>
				<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
				<p><?php the_tags(); ?></p>
			</footer>
			<?php do_action( 'foundationpress_page_before_comments' ); ?>
			<?php comments_template(); ?>
			<?php do_action( 'foundationpress_page_after_comments' ); ?>
		</div>
        <div class="row">
            <?php $query_current = get_current_events(); ?>
            <?php while ($query_current->have_posts() ) : $query_current->the_post(); ?>
                <?php get_template_part('template-parts/event'); ?>
            <?php endwhile; ?>
        </div>
        <div class="row past">
            <div class="small-12 columns">
                <h2 class="past-events-title"><?php _e('Past events', 'foundationpress') ?></h2>
            </div>
            <?php $query_past = get_past_events(); ?>
            <?php while ($query_past->have_posts() ) : $query_past->the_post(); ?>
                <?php get_template_part('template-parts/event'); ?>
            <?php endwhile; ?>
        </div>
</div>
<?php endwhile;?>
<?php do_action( 'foundationpress_after_content' ); ?>





<?php get_footer();
