<?php
/*
Template Name: Newsletter
*/
get_header(); ?>
<?php $newsletter_id = (ICL_LANGUAGE_CODE == 'de') ? 'fcho.dc34e' : 'fchn.2h7jgp7' ?>
<?php get_template_part('template-parts/featured-image'); ?>

    <div id="page-full-width" class="newsletter-page" role="main">

        <?php do_action('foundationpress_before_content'); ?>
        <?php while (have_posts()) : the_post(); ?>
            <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
                <?php do_action('foundationpress_page_before_entry_content'); ?>
                <div class="entry-content">
                    <?php the_content(); ?>

                    <div class="newsletter-container">
                        <h3 class="newsletter-title title large"><?php _e('Newsletter form', 'foundationpress'); ?></h3>
                        <div class="newsletter-form">
                            <form action="http://scnem2.com/art_resource.php?sid=<?php echo $newsletter_id; ?>"
                                  method="post" target="_blank">
                                <div class="row">
                                    <div class="medium-6 columns">
                                        <label class="attributesalutation typesalutation" for="form_SALUTATION">
                                            <?php _e('Salutation', 'foundationpress'); ?>
                                            <select name="form_SALUTATION" id="form_SALUTATION"
                                                    class="attributesalutation typesalutation">
                                                <option value="0" selected="selected">-</option>
                                                <option value="1"><?php _e('Ms./Mrs.', 'foundationpress'); ?></option>
                                                <option value="2"><?php _e('Mr.', 'foundationpress'); ?></option>
                                                <option value="3"><?php _e('Family', 'foundationpress'); ?></option>
                                                <option value="4"><?php _e('Company', 'foundationpress'); ?></option>
                                            </select>
                                        </label>
                                        <label class="attributefirstname typeinput"
                                               for="form_FIRSTNAME"><?php _e('First name', 'foundationpress'); ?>
                                            <input type="text" name="form_FIRSTNAME" id="form_FIRSTNAME"
                                                   class="attributefirstname typeinput" value="">
                                        </label>
                                        <label class="attributename typeinput"
                                               for="form_NAME"><?php _e('Last name', 'foundationpress'); ?>
                                            <input type="text" name="form_NAME" id="form_NAME"
                                                   class="attributename typeinput"
                                                   value="">
                                        </label>
                                        <label class="attributeemail typeemail mandatory"
                                               for="form_EMAIL"><?php _e('E-Mail', 'foundationpress'); ?>
                                            <input type="text" name="form_EMAIL" id="form_EMAIL"
                                                   class="attributeemail typeemail mandatory"
                                                   value="<?php echo $_POST['form_EMAIL']; ?>">
                                        </label>
                                    </div>
                                    <div class="medium-6 columns">

                                        <label class="attributelanguage typelanguage"
                                               for="form_LANGUAGE"><?php _e('Language', 'foundationpress'); ?>

                                            <select name="form_LANGUAGE" id="form_LANGUAGE"
                                                    class="attributelanguage typelanguage">
                                                <option value="0" selected="selected">Keine</option>
                                                <option value="1"><?php _e('German', 'foundationpress'); ?></option>
                                                <option value="2"><?php _e('English', 'foundationpress'); ?></option>
                                            </select>
                                        </label>

                                        <label
                                            class="topic-title"><?php _e('Which topics you want to subscribe?', 'foundationpress'); ?></label>
                                        <div class="topic-container">


                                            <label class="attributeigwindwetter typeboolean"
                                                   for="form_IGWINDWETTER">
                                                <input type="checkbox" name="form_IGWINDWETTER" id="form_IGWINDWETTER"
                                                       class="attributeigwindwetter typeboolean checkbox" value="0">
                                                <?php _e('Wind & Weather', 'foundationpress'); ?></label>


                                            <label class="attributeigverkehrwetter typeboolean"
                                                   for="form_IGVERKEHRWETTER">
                                                <input type="checkbox" name="form_IGVERKEHRWETTER"
                                                       id="form_IGVERKEHRWETTER"
                                                       class="attributeigverkehrwetter typeboolean checkbox" value="0">
                                                <?php _e('Traffic & Weather', 'foundationpress'); ?></label>


                                            <label class="attributeigindustrielledatenerfassung typeboolean"
                                                   for="form_IGINDUSTRIELLEDATENERFASSUNG">
                                                <input type="checkbox" name="form_IGINDUSTRIELLEDATENERFASSUNG"
                                                       id="form_IGINDUSTRIELLEDATENERFASSUNG"
                                                       class="attributeigindustrielledatenerfassung typeboolean checkbox"
                                                       value="0">
                                                <?php _e('Industrial data collection', 'foundationpress'); ?></label>


                                            <label class="attributeigoptischesensoren typeboolean"
                                                   for="form_IGOPTISCHESENSOREN">
                                                <input type="checkbox" name="form_IGOPTISCHESENSOREN"
                                                       id="form_IGOPTISCHESENSOREN"
                                                       class="attributeigoptischesensoren typeboolean checkbox"
                                                       value="0">
                                                <?php _e('Optical sensors', 'foundationpress'); ?></label>


                                            <label class="attributeigmobilesensoren typeboolean"
                                                   for="form_IGMOBILESENSOREN">
                                                <input type="checkbox" name="form_IGMOBILESENSOREN"
                                                       id="form_IGMOBILESENSOREN"
                                                       class="attributeigmobilesensoren typeboolean checkbox" value="0">
                                                <?php _e('Mobile sensors', 'foundationpress'); ?></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="small-12 columns">
                                        <label for="form_NLLUFFTPOST" class="agb">
                                            <input type="checkbox" name="form_NLLUFFTPOST" id="form_NLLUFFTPOST"
                                                   class="attributenllufftpost typeboolean checkbox" value="0">
                                            <?php echo sprintf(__('I have read the <a href="%s" target="_blank">privacy information</a> and agree.', 'foundationpress'), get_permalink('10663')) ?>
                                        </label>
                                        <input type="submit" class="round-button"
                                               value="<?php _e('Sign up for newsletter', 'foundationpress') ?> ›">
                                        <div class="info">* <?php _e('Required Fields', 'foundationpress'); ?></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </article>
        <?php endwhile; ?>

        <?php do_action('foundationpress_after_content'); ?>

    </div>

<?php get_footer();
