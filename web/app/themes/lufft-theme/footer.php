<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

		</section>
        <?php if ( ! is_404() ) : ?>
		<div id="pre-footer-container">
			<footer id="pre-footer">
				<?php do_action( 'foundationpress_before_pre_footer' ); ?>
				<?php dynamic_sidebar( 'footer-widgets' ); ?>
				<?php do_action( 'foundationpress_after_pre_footer' ); ?>
			</footer>
		</div>
		<div id="footer-container">
			<footer id="footer">
				<?php foundationpress_footer_menu(); ?>
				<div class="logo"><span><?php _e('Powered by', 'foundationpress'); ?></span> <a href="<?php _e('http://lufft.com', 'foundationpress')?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri()?>/assets/images/lufft_logo.png" alt="Lufft.com"></a></div>
				<div class="copyright">&copy; <?php echo date('Y', time()); ?></div>
			</footer>
		</div>
        <?php endif; ?>
		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>


<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-30242281-1', 'auto', {'allowLinker': true});
ga('require', 'linker');
ga('linker:autoLink', ['lufft-marwis.de', 'lufft-xseries.com' ] );
ga('set', 'anonymizeIp', true);
ga('send', 'pageview');

</script>
</body>
</html>
