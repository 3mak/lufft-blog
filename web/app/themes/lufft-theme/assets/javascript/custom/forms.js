(function($) {
  var instance = $('.newsletter-form form').parsley();
  var lang = $('html').attr('lang').slice(0,2);


  var messages = {
    'de': {
      'email': 'Bitte geben Sie eine gültige E-Mail-Adresse an.',
      'data': 'Bitte akzeptieren Sie die Datenschutzbestimmungen.'
    },
    'en': {
      'email': 'Please enter a valid e-mailaddress.',
      'data': ' Please accept the privacy policy.'
    }
  }


  $('#form_EMAIL')
    .attr('required', true)
    .attr('data-parsley-type', 'email')
    .attr('data-parsley-error-message', messages[lang].email);

  $('#form_NLLUFFTPOST')
    .attr('required', true)
    .attr('data-parsley-error-message', messages[lang].data);


  var commentInstance = $('#commentform').parsley();
  $('#author').attr('required', true);
  $('#email').attr('required', true)
    .attr('data-parsley-type', 'email')
    .attr('data-parsley-error-message', messages[lang].email);
    
  $('#comment').attr('required', true).attr('data-parsley-minlength', 3);

})(jQuery);
