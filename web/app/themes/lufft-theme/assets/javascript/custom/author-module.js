/**
 * Created by emak on 21.04.16.
 */
(function ($) {
    'use strict';
    $('.author-module .open').click(function() {
        $(this).toggleClass('active');
        $(this).parent().toggleClass('active');
    });
})(jQuery);
