'use strict';

window.whatInput = function () {

  'use strict';

  /*
    ---------------
    variables
    ---------------
  */

  // array of actively pressed keys

  var activeKeys = [];

  // cache document.body
  var body;

  // boolean: true if touch buffer timer is running
  var buffer = false;

  // the last used input type
  var currentInput = null;

  // `input` types that don't accept text
  var nonTypingInputs = ['button', 'checkbox', 'file', 'image', 'radio', 'reset', 'submit'];

  // detect version of mouse wheel event to use
  // via https://developer.mozilla.org/en-US/docs/Web/Events/wheel
  var mouseWheel = detectWheel();

  // list of modifier keys commonly used with the mouse and
  // can be safely ignored to prevent false keyboard detection
  var ignoreMap = [16, // shift
  17, // control
  18, // alt
  91, // Windows key / left Apple cmd
  93 // Windows menu / right Apple cmd
  ];

  // mapping of events to input types
  var inputMap = {
    'keydown': 'keyboard',
    'keyup': 'keyboard',
    'mousedown': 'mouse',
    'mousemove': 'mouse',
    'MSPointerDown': 'pointer',
    'MSPointerMove': 'pointer',
    'pointerdown': 'pointer',
    'pointermove': 'pointer',
    'touchstart': 'touch'
  };

  // add correct mouse wheel event mapping to `inputMap`
  inputMap[detectWheel()] = 'mouse';

  // array of all used input types
  var inputTypes = [];

  // mapping of key codes to a common name
  var keyMap = {
    9: 'tab',
    13: 'enter',
    16: 'shift',
    27: 'esc',
    32: 'space',
    37: 'left',
    38: 'up',
    39: 'right',
    40: 'down'
  };

  // map of IE 10 pointer events
  var pointerMap = {
    2: 'touch',
    3: 'touch', // treat pen like touch
    4: 'mouse'
  };

  // touch buffer timer
  var timer;

  /*
    ---------------
    functions
    ---------------
  */

  // allows events that are also triggered to be filtered out for `touchstart`
  function eventBuffer() {
    clearTimer();
    setInput(event);

    buffer = true;
    timer = window.setTimeout(function () {
      buffer = false;
    }, 650);
  }

  function bufferedEvent(event) {
    if (!buffer) setInput(event);
  }

  function unBufferedEvent(event) {
    clearTimer();
    setInput(event);
  }

  function clearTimer() {
    window.clearTimeout(timer);
  }

  function setInput(event) {
    var eventKey = key(event);
    var value = inputMap[event.type];
    if (value === 'pointer') value = pointerType(event);

    // don't do anything if the value matches the input type already set
    if (currentInput !== value) {
      var eventTarget = target(event);
      var eventTargetNode = eventTarget.nodeName.toLowerCase();
      var eventTargetType = eventTargetNode === 'input' ? eventTarget.getAttribute('type') : null;

      if ( // only if the user flag to allow typing in form fields isn't set
      !body.hasAttribute('data-whatinput-formtyping') &&

      // only if currentInput has a value
      currentInput &&

      // only if the input is `keyboard`
      value === 'keyboard' &&

      // not if the key is `TAB`
      keyMap[eventKey] !== 'tab' && (

      // only if the target is a form input that accepts text
      eventTargetNode === 'textarea' || eventTargetNode === 'select' || eventTargetNode === 'input' && nonTypingInputs.indexOf(eventTargetType) < 0) ||
      // ignore modifier keys
      ignoreMap.indexOf(eventKey) > -1) {
        // ignore keyboard typing
      } else {
        switchInput(value);
      }
    }

    if (value === 'keyboard') logKeys(eventKey);
  }

  function switchInput(string) {
    currentInput = string;
    body.setAttribute('data-whatinput', currentInput);

    if (inputTypes.indexOf(currentInput) === -1) inputTypes.push(currentInput);
  }

  function key(event) {
    return event.keyCode ? event.keyCode : event.which;
  }

  function target(event) {
    return event.target || event.srcElement;
  }

  function pointerType(event) {
    if (typeof event.pointerType === 'number') {
      return pointerMap[event.pointerType];
    } else {
      return event.pointerType === 'pen' ? 'touch' : event.pointerType; // treat pen like touch
    }
  }

  // keyboard logging
  function logKeys(eventKey) {
    if (activeKeys.indexOf(keyMap[eventKey]) === -1 && keyMap[eventKey]) activeKeys.push(keyMap[eventKey]);
  }

  function unLogKeys(event) {
    var eventKey = key(event);
    var arrayPos = activeKeys.indexOf(keyMap[eventKey]);

    if (arrayPos !== -1) activeKeys.splice(arrayPos, 1);
  }

  function bindEvents() {
    body = document.body;

    // pointer events (mouse, pen, touch)
    if (window.PointerEvent) {
      body.addEventListener('pointerdown', bufferedEvent);
      body.addEventListener('pointermove', bufferedEvent);
    } else if (window.MSPointerEvent) {
      body.addEventListener('MSPointerDown', bufferedEvent);
      body.addEventListener('MSPointerMove', bufferedEvent);
    } else {

      // mouse events
      body.addEventListener('mousedown', bufferedEvent);
      body.addEventListener('mousemove', bufferedEvent);

      // touch events
      if ('ontouchstart' in window) {
        body.addEventListener('touchstart', eventBuffer);
      }
    }

    // mouse wheel
    body.addEventListener(mouseWheel, bufferedEvent);

    // keyboard events
    body.addEventListener('keydown', unBufferedEvent);
    body.addEventListener('keyup', unBufferedEvent);
    document.addEventListener('keyup', unLogKeys);
  }

  /*
    ---------------
    utilities
    ---------------
  */

  // detect version of mouse wheel event to use
  // via https://developer.mozilla.org/en-US/docs/Web/Events/wheel
  function detectWheel() {
    return mouseWheel = 'onwheel' in document.createElement('div') ? 'wheel' : // Modern browsers support "wheel"

    document.onmousewheel !== undefined ? 'mousewheel' : // Webkit and IE support at least "mousewheel"
    'DOMMouseScroll'; // let's assume that remaining browsers are older Firefox
  }

  /*
    ---------------
    init
     don't start script unless browser cuts the mustard,
    also passes if polyfills are used
    ---------------
  */

  if ('addEventListener' in window && Array.prototype.indexOf) {

    // if the dom is already ready already (script was placed at bottom of <body>)
    if (document.body) {
      bindEvents();

      // otherwise wait for the dom to load (script was placed in the <head>)
    } else {
      document.addEventListener('DOMContentLoaded', bindEvents);
    }
  }

  /*
    ---------------
    api
    ---------------
  */

  return {

    // returns string: the current input type
    ask: function () {
      return currentInput;
    },

    // returns array: currently pressed keys
    keys: function () {
      return activeKeys;
    },

    // returns array: all the detected input types
    types: function () {
      return inputTypes;
    },

    // accepts string: manually set the input type
    set: switchInput
  };
}();
;'use strict';

!function ($) {

  "use strict";

  var FOUNDATION_VERSION = '6.2.4';

  // Global Foundation object
  // This is attached to the window, or used as a module for AMD/Browserify
  var Foundation = {
    version: FOUNDATION_VERSION,

    /**
     * Stores initialized plugins.
     */
    _plugins: {},

    /**
     * Stores generated unique ids for plugin instances
     */
    _uuids: [],

    /**
     * Returns a boolean for RTL support
     */
    rtl: function () {
      return $('html').attr('dir') === 'rtl';
    },
    /**
     * Defines a Foundation plugin, adding it to the `Foundation` namespace and the list of plugins to initialize when reflowing.
     * @param {Object} plugin - The constructor of the plugin.
     */
    plugin: function (plugin, name) {
      // Object key to use when adding to global Foundation object
      // Examples: Foundation.Reveal, Foundation.OffCanvas
      var className = name || functionName(plugin);
      // Object key to use when storing the plugin, also used to create the identifying data attribute for the plugin
      // Examples: data-reveal, data-off-canvas
      var attrName = hyphenate(className);

      // Add to the Foundation object and the plugins list (for reflowing)
      this._plugins[attrName] = this[className] = plugin;
    },
    /**
     * @function
     * Populates the _uuids array with pointers to each individual plugin instance.
     * Adds the `zfPlugin` data-attribute to programmatically created plugins to allow use of $(selector).foundation(method) calls.
     * Also fires the initialization event for each plugin, consolidating repetitive code.
     * @param {Object} plugin - an instance of a plugin, usually `this` in context.
     * @param {String} name - the name of the plugin, passed as a camelCased string.
     * @fires Plugin#init
     */
    registerPlugin: function (plugin, name) {
      var pluginName = name ? hyphenate(name) : functionName(plugin.constructor).toLowerCase();
      plugin.uuid = this.GetYoDigits(6, pluginName);

      if (!plugin.$element.attr('data-' + pluginName)) {
        plugin.$element.attr('data-' + pluginName, plugin.uuid);
      }
      if (!plugin.$element.data('zfPlugin')) {
        plugin.$element.data('zfPlugin', plugin);
      }
      /**
       * Fires when the plugin has initialized.
       * @event Plugin#init
       */
      plugin.$element.trigger('init.zf.' + pluginName);

      this._uuids.push(plugin.uuid);

      return;
    },
    /**
     * @function
     * Removes the plugins uuid from the _uuids array.
     * Removes the zfPlugin data attribute, as well as the data-plugin-name attribute.
     * Also fires the destroyed event for the plugin, consolidating repetitive code.
     * @param {Object} plugin - an instance of a plugin, usually `this` in context.
     * @fires Plugin#destroyed
     */
    unregisterPlugin: function (plugin) {
      var pluginName = hyphenate(functionName(plugin.$element.data('zfPlugin').constructor));

      this._uuids.splice(this._uuids.indexOf(plugin.uuid), 1);
      plugin.$element.removeAttr('data-' + pluginName).removeData('zfPlugin')
      /**
       * Fires when the plugin has been destroyed.
       * @event Plugin#destroyed
       */
      .trigger('destroyed.zf.' + pluginName);
      for (var prop in plugin) {
        plugin[prop] = null; //clean up script to prep for garbage collection.
      }
      return;
    },

    /**
     * @function
     * Causes one or more active plugins to re-initialize, resetting event listeners, recalculating positions, etc.
     * @param {String} plugins - optional string of an individual plugin key, attained by calling `$(element).data('pluginName')`, or string of a plugin class i.e. `'dropdown'`
     * @default If no argument is passed, reflow all currently active plugins.
     */
    reInit: function (plugins) {
      var isJQ = plugins instanceof $;
      try {
        if (isJQ) {
          plugins.each(function () {
            $(this).data('zfPlugin')._init();
          });
        } else {
          var type = typeof plugins,
              _this = this,
              fns = {
            'object': function (plgs) {
              plgs.forEach(function (p) {
                p = hyphenate(p);
                $('[data-' + p + ']').foundation('_init');
              });
            },
            'string': function () {
              plugins = hyphenate(plugins);
              $('[data-' + plugins + ']').foundation('_init');
            },
            'undefined': function () {
              this['object'](Object.keys(_this._plugins));
            }
          };
          fns[type](plugins);
        }
      } catch (err) {
        console.error(err);
      } finally {
        return plugins;
      }
    },

    /**
     * returns a random base-36 uid with namespacing
     * @function
     * @param {Number} length - number of random base-36 digits desired. Increase for more random strings.
     * @param {String} namespace - name of plugin to be incorporated in uid, optional.
     * @default {String} '' - if no plugin name is provided, nothing is appended to the uid.
     * @returns {String} - unique id
     */
    GetYoDigits: function (length, namespace) {
      length = length || 6;
      return Math.round(Math.pow(36, length + 1) - Math.random() * Math.pow(36, length)).toString(36).slice(1) + (namespace ? '-' + namespace : '');
    },
    /**
     * Initialize plugins on any elements within `elem` (and `elem` itself) that aren't already initialized.
     * @param {Object} elem - jQuery object containing the element to check inside. Also checks the element itself, unless it's the `document` object.
     * @param {String|Array} plugins - A list of plugins to initialize. Leave this out to initialize everything.
     */
    reflow: function (elem, plugins) {

      // If plugins is undefined, just grab everything
      if (typeof plugins === 'undefined') {
        plugins = Object.keys(this._plugins);
      }
      // If plugins is a string, convert it to an array with one item
      else if (typeof plugins === 'string') {
          plugins = [plugins];
        }

      var _this = this;

      // Iterate through each plugin
      $.each(plugins, function (i, name) {
        // Get the current plugin
        var plugin = _this._plugins[name];

        // Localize the search to all elements inside elem, as well as elem itself, unless elem === document
        var $elem = $(elem).find('[data-' + name + ']').addBack('[data-' + name + ']');

        // For each plugin found, initialize it
        $elem.each(function () {
          var $el = $(this),
              opts = {};
          // Don't double-dip on plugins
          if ($el.data('zfPlugin')) {
            console.warn("Tried to initialize " + name + " on an element that already has a Foundation plugin.");
            return;
          }

          if ($el.attr('data-options')) {
            var thing = $el.attr('data-options').split(';').forEach(function (e, i) {
              var opt = e.split(':').map(function (el) {
                return el.trim();
              });
              if (opt[0]) opts[opt[0]] = parseValue(opt[1]);
            });
          }
          try {
            $el.data('zfPlugin', new plugin($(this), opts));
          } catch (er) {
            console.error(er);
          } finally {
            return;
          }
        });
      });
    },
    getFnName: functionName,
    transitionend: function ($elem) {
      var transitions = {
        'transition': 'transitionend',
        'WebkitTransition': 'webkitTransitionEnd',
        'MozTransition': 'transitionend',
        'OTransition': 'otransitionend'
      };
      var elem = document.createElement('div'),
          end;

      for (var t in transitions) {
        if (typeof elem.style[t] !== 'undefined') {
          end = transitions[t];
        }
      }
      if (end) {
        return end;
      } else {
        end = setTimeout(function () {
          $elem.triggerHandler('transitionend', [$elem]);
        }, 1);
        return 'transitionend';
      }
    }
  };

  Foundation.util = {
    /**
     * Function for applying a debounce effect to a function call.
     * @function
     * @param {Function} func - Function to be called at end of timeout.
     * @param {Number} delay - Time in ms to delay the call of `func`.
     * @returns function
     */
    throttle: function (func, delay) {
      var timer = null;

      return function () {
        var context = this,
            args = arguments;

        if (timer === null) {
          timer = setTimeout(function () {
            func.apply(context, args);
            timer = null;
          }, delay);
        }
      };
    }
  };

  // TODO: consider not making this a jQuery function
  // TODO: need way to reflow vs. re-initialize
  /**
   * The Foundation jQuery method.
   * @param {String|Array} method - An action to perform on the current jQuery object.
   */
  var foundation = function (method) {
    var type = typeof method,
        $meta = $('meta.foundation-mq'),
        $noJS = $('.no-js');

    if (!$meta.length) {
      $('<meta class="foundation-mq">').appendTo(document.head);
    }
    if ($noJS.length) {
      $noJS.removeClass('no-js');
    }

    if (type === 'undefined') {
      //needs to initialize the Foundation object, or an individual plugin.
      Foundation.MediaQuery._init();
      Foundation.reflow(this);
    } else if (type === 'string') {
      //an individual method to invoke on a plugin or group of plugins
      var args = Array.prototype.slice.call(arguments, 1); //collect all the arguments, if necessary
      var plugClass = this.data('zfPlugin'); //determine the class of plugin

      if (plugClass !== undefined && plugClass[method] !== undefined) {
        //make sure both the class and method exist
        if (this.length === 1) {
          //if there's only one, call it directly.
          plugClass[method].apply(plugClass, args);
        } else {
          this.each(function (i, el) {
            //otherwise loop through the jQuery collection and invoke the method on each
            plugClass[method].apply($(el).data('zfPlugin'), args);
          });
        }
      } else {
        //error for no class or no method
        throw new ReferenceError("We're sorry, '" + method + "' is not an available method for " + (plugClass ? functionName(plugClass) : 'this element') + '.');
      }
    } else {
      //error for invalid argument type
      throw new TypeError('We\'re sorry, ' + type + ' is not a valid parameter. You must use a string representing the method you wish to invoke.');
    }
    return this;
  };

  window.Foundation = Foundation;
  $.fn.foundation = foundation;

  // Polyfill for requestAnimationFrame
  (function () {
    if (!Date.now || !window.Date.now) window.Date.now = Date.now = function () {
      return new Date().getTime();
    };

    var vendors = ['webkit', 'moz'];
    for (var i = 0; i < vendors.length && !window.requestAnimationFrame; ++i) {
      var vp = vendors[i];
      window.requestAnimationFrame = window[vp + 'RequestAnimationFrame'];
      window.cancelAnimationFrame = window[vp + 'CancelAnimationFrame'] || window[vp + 'CancelRequestAnimationFrame'];
    }
    if (/iP(ad|hone|od).*OS 6/.test(window.navigator.userAgent) || !window.requestAnimationFrame || !window.cancelAnimationFrame) {
      var lastTime = 0;
      window.requestAnimationFrame = function (callback) {
        var now = Date.now();
        var nextTime = Math.max(lastTime + 16, now);
        return setTimeout(function () {
          callback(lastTime = nextTime);
        }, nextTime - now);
      };
      window.cancelAnimationFrame = clearTimeout;
    }
    /**
     * Polyfill for performance.now, required by rAF
     */
    if (!window.performance || !window.performance.now) {
      window.performance = {
        start: Date.now(),
        now: function () {
          return Date.now() - this.start;
        }
      };
    }
  })();
  if (!Function.prototype.bind) {
    Function.prototype.bind = function (oThis) {
      if (typeof this !== 'function') {
        // closest thing possible to the ECMAScript 5
        // internal IsCallable function
        throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
      }

      var aArgs = Array.prototype.slice.call(arguments, 1),
          fToBind = this,
          fNOP = function () {},
          fBound = function () {
        return fToBind.apply(this instanceof fNOP ? this : oThis, aArgs.concat(Array.prototype.slice.call(arguments)));
      };

      if (this.prototype) {
        // native functions don't have a prototype
        fNOP.prototype = this.prototype;
      }
      fBound.prototype = new fNOP();

      return fBound;
    };
  }
  // Polyfill to get the name of a function in IE9
  function functionName(fn) {
    if (Function.prototype.name === undefined) {
      var funcNameRegex = /function\s([^(]{1,})\(/;
      var results = funcNameRegex.exec(fn.toString());
      return results && results.length > 1 ? results[1].trim() : "";
    } else if (fn.prototype === undefined) {
      return fn.constructor.name;
    } else {
      return fn.prototype.constructor.name;
    }
  }
  function parseValue(str) {
    if (/true/.test(str)) return true;else if (/false/.test(str)) return false;else if (!isNaN(str * 1)) return parseFloat(str);
    return str;
  }
  // Convert PascalCase to kebab-case
  // Thank you: http://stackoverflow.com/a/8955580
  function hyphenate(str) {
    return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
  }
}(jQuery);
;'use strict';

!function ($) {

  Foundation.Box = {
    ImNotTouchingYou: ImNotTouchingYou,
    GetDimensions: GetDimensions,
    GetOffsets: GetOffsets

    /**
     * Compares the dimensions of an element to a container and determines collision events with container.
     * @function
     * @param {jQuery} element - jQuery object to test for collisions.
     * @param {jQuery} parent - jQuery object to use as bounding container.
     * @param {Boolean} lrOnly - set to true to check left and right values only.
     * @param {Boolean} tbOnly - set to true to check top and bottom values only.
     * @default if no parent object passed, detects collisions with `window`.
     * @returns {Boolean} - true if collision free, false if a collision in any direction.
     */
  };function ImNotTouchingYou(element, parent, lrOnly, tbOnly) {
    var eleDims = GetDimensions(element),
        top,
        bottom,
        left,
        right;

    if (parent) {
      var parDims = GetDimensions(parent);

      bottom = eleDims.offset.top + eleDims.height <= parDims.height + parDims.offset.top;
      top = eleDims.offset.top >= parDims.offset.top;
      left = eleDims.offset.left >= parDims.offset.left;
      right = eleDims.offset.left + eleDims.width <= parDims.width + parDims.offset.left;
    } else {
      bottom = eleDims.offset.top + eleDims.height <= eleDims.windowDims.height + eleDims.windowDims.offset.top;
      top = eleDims.offset.top >= eleDims.windowDims.offset.top;
      left = eleDims.offset.left >= eleDims.windowDims.offset.left;
      right = eleDims.offset.left + eleDims.width <= eleDims.windowDims.width;
    }

    var allDirs = [bottom, top, left, right];

    if (lrOnly) {
      return left === right === true;
    }

    if (tbOnly) {
      return top === bottom === true;
    }

    return allDirs.indexOf(false) === -1;
  };

  /**
   * Uses native methods to return an object of dimension values.
   * @function
   * @param {jQuery || HTML} element - jQuery object or DOM element for which to get the dimensions. Can be any element other that document or window.
   * @returns {Object} - nested object of integer pixel values
   * TODO - if element is window, return only those values.
   */
  function GetDimensions(elem, test) {
    elem = elem.length ? elem[0] : elem;

    if (elem === window || elem === document) {
      throw new Error("I'm sorry, Dave. I'm afraid I can't do that.");
    }

    var rect = elem.getBoundingClientRect(),
        parRect = elem.parentNode.getBoundingClientRect(),
        winRect = document.body.getBoundingClientRect(),
        winY = window.pageYOffset,
        winX = window.pageXOffset;

    return {
      width: rect.width,
      height: rect.height,
      offset: {
        top: rect.top + winY,
        left: rect.left + winX
      },
      parentDims: {
        width: parRect.width,
        height: parRect.height,
        offset: {
          top: parRect.top + winY,
          left: parRect.left + winX
        }
      },
      windowDims: {
        width: winRect.width,
        height: winRect.height,
        offset: {
          top: winY,
          left: winX
        }
      }
    };
  }

  /**
   * Returns an object of top and left integer pixel values for dynamically rendered elements,
   * such as: Tooltip, Reveal, and Dropdown
   * @function
   * @param {jQuery} element - jQuery object for the element being positioned.
   * @param {jQuery} anchor - jQuery object for the element's anchor point.
   * @param {String} position - a string relating to the desired position of the element, relative to it's anchor
   * @param {Number} vOffset - integer pixel value of desired vertical separation between anchor and element.
   * @param {Number} hOffset - integer pixel value of desired horizontal separation between anchor and element.
   * @param {Boolean} isOverflow - if a collision event is detected, sets to true to default the element to full width - any desired offset.
   * TODO alter/rewrite to work with `em` values as well/instead of pixels
   */
  function GetOffsets(element, anchor, position, vOffset, hOffset, isOverflow) {
    var $eleDims = GetDimensions(element),
        $anchorDims = anchor ? GetDimensions(anchor) : null;

    switch (position) {
      case 'top':
        return {
          left: Foundation.rtl() ? $anchorDims.offset.left - $eleDims.width + $anchorDims.width : $anchorDims.offset.left,
          top: $anchorDims.offset.top - ($eleDims.height + vOffset)
        };
        break;
      case 'left':
        return {
          left: $anchorDims.offset.left - ($eleDims.width + hOffset),
          top: $anchorDims.offset.top
        };
        break;
      case 'right':
        return {
          left: $anchorDims.offset.left + $anchorDims.width + hOffset,
          top: $anchorDims.offset.top
        };
        break;
      case 'center top':
        return {
          left: $anchorDims.offset.left + $anchorDims.width / 2 - $eleDims.width / 2,
          top: $anchorDims.offset.top - ($eleDims.height + vOffset)
        };
        break;
      case 'center bottom':
        return {
          left: isOverflow ? hOffset : $anchorDims.offset.left + $anchorDims.width / 2 - $eleDims.width / 2,
          top: $anchorDims.offset.top + $anchorDims.height + vOffset
        };
        break;
      case 'center left':
        return {
          left: $anchorDims.offset.left - ($eleDims.width + hOffset),
          top: $anchorDims.offset.top + $anchorDims.height / 2 - $eleDims.height / 2
        };
        break;
      case 'center right':
        return {
          left: $anchorDims.offset.left + $anchorDims.width + hOffset + 1,
          top: $anchorDims.offset.top + $anchorDims.height / 2 - $eleDims.height / 2
        };
        break;
      case 'center':
        return {
          left: $eleDims.windowDims.offset.left + $eleDims.windowDims.width / 2 - $eleDims.width / 2,
          top: $eleDims.windowDims.offset.top + $eleDims.windowDims.height / 2 - $eleDims.height / 2
        };
        break;
      case 'reveal':
        return {
          left: ($eleDims.windowDims.width - $eleDims.width) / 2,
          top: $eleDims.windowDims.offset.top + vOffset
        };
      case 'reveal full':
        return {
          left: $eleDims.windowDims.offset.left,
          top: $eleDims.windowDims.offset.top
        };
        break;
      case 'left bottom':
        return {
          left: $anchorDims.offset.left,
          top: $anchorDims.offset.top + $anchorDims.height
        };
        break;
      case 'right bottom':
        return {
          left: $anchorDims.offset.left + $anchorDims.width + hOffset - $eleDims.width,
          top: $anchorDims.offset.top + $anchorDims.height
        };
        break;
      default:
        return {
          left: Foundation.rtl() ? $anchorDims.offset.left - $eleDims.width + $anchorDims.width : $anchorDims.offset.left + hOffset,
          top: $anchorDims.offset.top + $anchorDims.height + vOffset
        };
    }
  }
}(jQuery);
;/*******************************************
 *                                         *
 * This util was created by Marius Olbertz *
 * Please thank Marius on GitHub /owlbertz *
 * or the web http://www.mariusolbertz.de/ *
 *                                         *
 ******************************************/

'use strict';

!function ($) {

  var keyCodes = {
    9: 'TAB',
    13: 'ENTER',
    27: 'ESCAPE',
    32: 'SPACE',
    37: 'ARROW_LEFT',
    38: 'ARROW_UP',
    39: 'ARROW_RIGHT',
    40: 'ARROW_DOWN'
  };

  var commands = {};

  var Keyboard = {
    keys: getKeyCodes(keyCodes),

    /**
     * Parses the (keyboard) event and returns a String that represents its key
     * Can be used like Foundation.parseKey(event) === Foundation.keys.SPACE
     * @param {Event} event - the event generated by the event handler
     * @return String key - String that represents the key pressed
     */
    parseKey: function (event) {
      var key = keyCodes[event.which || event.keyCode] || String.fromCharCode(event.which).toUpperCase();
      if (event.shiftKey) key = 'SHIFT_' + key;
      if (event.ctrlKey) key = 'CTRL_' + key;
      if (event.altKey) key = 'ALT_' + key;
      return key;
    },


    /**
     * Handles the given (keyboard) event
     * @param {Event} event - the event generated by the event handler
     * @param {String} component - Foundation component's name, e.g. Slider or Reveal
     * @param {Objects} functions - collection of functions that are to be executed
     */
    handleKey: function (event, component, functions) {
      var commandList = commands[component],
          keyCode = this.parseKey(event),
          cmds,
          command,
          fn;

      if (!commandList) return console.warn('Component not defined!');

      if (typeof commandList.ltr === 'undefined') {
        // this component does not differentiate between ltr and rtl
        cmds = commandList; // use plain list
      } else {
        // merge ltr and rtl: if document is rtl, rtl overwrites ltr and vice versa
        if (Foundation.rtl()) cmds = $.extend({}, commandList.ltr, commandList.rtl);else cmds = $.extend({}, commandList.rtl, commandList.ltr);
      }
      command = cmds[keyCode];

      fn = functions[command];
      if (fn && typeof fn === 'function') {
        // execute function  if exists
        var returnValue = fn.apply();
        if (functions.handled || typeof functions.handled === 'function') {
          // execute function when event was handled
          functions.handled(returnValue);
        }
      } else {
        if (functions.unhandled || typeof functions.unhandled === 'function') {
          // execute function when event was not handled
          functions.unhandled();
        }
      }
    },


    /**
     * Finds all focusable elements within the given `$element`
     * @param {jQuery} $element - jQuery object to search within
     * @return {jQuery} $focusable - all focusable elements within `$element`
     */
    findFocusable: function ($element) {
      return $element.find('a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]').filter(function () {
        if (!$(this).is(':visible') || $(this).attr('tabindex') < 0) {
          return false;
        } //only have visible elements and those that have a tabindex greater or equal 0
        return true;
      });
    },


    /**
     * Returns the component name name
     * @param {Object} component - Foundation component, e.g. Slider or Reveal
     * @return String componentName
     */

    register: function (componentName, cmds) {
      commands[componentName] = cmds;
    }
  };

  /*
   * Constants for easier comparing.
   * Can be used like Foundation.parseKey(event) === Foundation.keys.SPACE
   */
  function getKeyCodes(kcs) {
    var k = {};
    for (var kc in kcs) {
      k[kcs[kc]] = kcs[kc];
    }return k;
  }

  Foundation.Keyboard = Keyboard;
}(jQuery);
;'use strict';

!function ($) {

  // Default set of media queries
  var defaultQueries = {
    'default': 'only screen',
    landscape: 'only screen and (orientation: landscape)',
    portrait: 'only screen and (orientation: portrait)',
    retina: 'only screen and (-webkit-min-device-pixel-ratio: 2),' + 'only screen and (min--moz-device-pixel-ratio: 2),' + 'only screen and (-o-min-device-pixel-ratio: 2/1),' + 'only screen and (min-device-pixel-ratio: 2),' + 'only screen and (min-resolution: 192dpi),' + 'only screen and (min-resolution: 2dppx)'
  };

  var MediaQuery = {
    queries: [],

    current: '',

    /**
     * Initializes the media query helper, by extracting the breakpoint list from the CSS and activating the breakpoint watcher.
     * @function
     * @private
     */
    _init: function () {
      var self = this;
      var extractedStyles = $('.foundation-mq').css('font-family');
      var namedQueries;

      namedQueries = parseStyleToObject(extractedStyles);

      for (var key in namedQueries) {
        if (namedQueries.hasOwnProperty(key)) {
          self.queries.push({
            name: key,
            value: 'only screen and (min-width: ' + namedQueries[key] + ')'
          });
        }
      }

      this.current = this._getCurrentSize();

      this._watcher();
    },


    /**
     * Checks if the screen is at least as wide as a breakpoint.
     * @function
     * @param {String} size - Name of the breakpoint to check.
     * @returns {Boolean} `true` if the breakpoint matches, `false` if it's smaller.
     */
    atLeast: function (size) {
      var query = this.get(size);

      if (query) {
        return window.matchMedia(query).matches;
      }

      return false;
    },


    /**
     * Gets the media query of a breakpoint.
     * @function
     * @param {String} size - Name of the breakpoint to get.
     * @returns {String|null} - The media query of the breakpoint, or `null` if the breakpoint doesn't exist.
     */
    get: function (size) {
      for (var i in this.queries) {
        if (this.queries.hasOwnProperty(i)) {
          var query = this.queries[i];
          if (size === query.name) return query.value;
        }
      }

      return null;
    },


    /**
     * Gets the current breakpoint name by testing every breakpoint and returning the last one to match (the biggest one).
     * @function
     * @private
     * @returns {String} Name of the current breakpoint.
     */
    _getCurrentSize: function () {
      var matched;

      for (var i = 0; i < this.queries.length; i++) {
        var query = this.queries[i];

        if (window.matchMedia(query.value).matches) {
          matched = query;
        }
      }

      if (typeof matched === 'object') {
        return matched.name;
      } else {
        return matched;
      }
    },


    /**
     * Activates the breakpoint watcher, which fires an event on the window whenever the breakpoint changes.
     * @function
     * @private
     */
    _watcher: function () {
      var _this = this;

      $(window).on('resize.zf.mediaquery', function () {
        var newSize = _this._getCurrentSize(),
            currentSize = _this.current;

        if (newSize !== currentSize) {
          // Change the current media query
          _this.current = newSize;

          // Broadcast the media query change on the window
          $(window).trigger('changed.zf.mediaquery', [newSize, currentSize]);
        }
      });
    }
  };

  Foundation.MediaQuery = MediaQuery;

  // matchMedia() polyfill - Test a CSS media type/query in JS.
  // Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas, David Knight. Dual MIT/BSD license
  window.matchMedia || (window.matchMedia = function () {
    'use strict';

    // For browsers that support matchMedium api such as IE 9 and webkit

    var styleMedia = window.styleMedia || window.media;

    // For those that don't support matchMedium
    if (!styleMedia) {
      var style = document.createElement('style'),
          script = document.getElementsByTagName('script')[0],
          info = null;

      style.type = 'text/css';
      style.id = 'matchmediajs-test';

      script && script.parentNode && script.parentNode.insertBefore(style, script);

      // 'style.currentStyle' is used by IE <= 8 and 'window.getComputedStyle' for all other browsers
      info = 'getComputedStyle' in window && window.getComputedStyle(style, null) || style.currentStyle;

      styleMedia = {
        matchMedium: function (media) {
          var text = '@media ' + media + '{ #matchmediajs-test { width: 1px; } }';

          // 'style.styleSheet' is used by IE <= 8 and 'style.textContent' for all other browsers
          if (style.styleSheet) {
            style.styleSheet.cssText = text;
          } else {
            style.textContent = text;
          }

          // Test if media query is true or false
          return info.width === '1px';
        }
      };
    }

    return function (media) {
      return {
        matches: styleMedia.matchMedium(media || 'all'),
        media: media || 'all'
      };
    };
  }());

  // Thank you: https://github.com/sindresorhus/query-string
  function parseStyleToObject(str) {
    var styleObject = {};

    if (typeof str !== 'string') {
      return styleObject;
    }

    str = str.trim().slice(1, -1); // browsers re-quote string style values

    if (!str) {
      return styleObject;
    }

    styleObject = str.split('&').reduce(function (ret, param) {
      var parts = param.replace(/\+/g, ' ').split('=');
      var key = parts[0];
      var val = parts[1];
      key = decodeURIComponent(key);

      // missing `=` should be `null`:
      // http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
      val = val === undefined ? null : decodeURIComponent(val);

      if (!ret.hasOwnProperty(key)) {
        ret[key] = val;
      } else if (Array.isArray(ret[key])) {
        ret[key].push(val);
      } else {
        ret[key] = [ret[key], val];
      }
      return ret;
    }, {});

    return styleObject;
  }

  Foundation.MediaQuery = MediaQuery;
}(jQuery);
;'use strict';

!function ($) {

  /**
   * Motion module.
   * @module foundation.motion
   */

  var initClasses = ['mui-enter', 'mui-leave'];
  var activeClasses = ['mui-enter-active', 'mui-leave-active'];

  var Motion = {
    animateIn: function (element, animation, cb) {
      animate(true, element, animation, cb);
    },

    animateOut: function (element, animation, cb) {
      animate(false, element, animation, cb);
    }
  };

  function Move(duration, elem, fn) {
    var anim,
        prog,
        start = null;
    // console.log('called');

    function move(ts) {
      if (!start) start = window.performance.now();
      // console.log(start, ts);
      prog = ts - start;
      fn.apply(elem);

      if (prog < duration) {
        anim = window.requestAnimationFrame(move, elem);
      } else {
        window.cancelAnimationFrame(anim);
        elem.trigger('finished.zf.animate', [elem]).triggerHandler('finished.zf.animate', [elem]);
      }
    }
    anim = window.requestAnimationFrame(move);
  }

  /**
   * Animates an element in or out using a CSS transition class.
   * @function
   * @private
   * @param {Boolean} isIn - Defines if the animation is in or out.
   * @param {Object} element - jQuery or HTML object to animate.
   * @param {String} animation - CSS class to use.
   * @param {Function} cb - Callback to run when animation is finished.
   */
  function animate(isIn, element, animation, cb) {
    element = $(element).eq(0);

    if (!element.length) return;

    var initClass = isIn ? initClasses[0] : initClasses[1];
    var activeClass = isIn ? activeClasses[0] : activeClasses[1];

    // Set up the animation
    reset();

    element.addClass(animation).css('transition', 'none');

    requestAnimationFrame(function () {
      element.addClass(initClass);
      if (isIn) element.show();
    });

    // Start the animation
    requestAnimationFrame(function () {
      element[0].offsetWidth;
      element.css('transition', '').addClass(activeClass);
    });

    // Clean up the animation when it finishes
    element.one(Foundation.transitionend(element), finish);

    // Hides the element (for out animations), resets the element, and runs a callback
    function finish() {
      if (!isIn) element.hide();
      reset();
      if (cb) cb.apply(element);
    }

    // Resets transitions and removes motion-specific classes
    function reset() {
      element[0].style.transitionDuration = 0;
      element.removeClass(initClass + ' ' + activeClass + ' ' + animation);
    }
  }

  Foundation.Move = Move;
  Foundation.Motion = Motion;
}(jQuery);
;'use strict';

!function ($) {

  var Nest = {
    Feather: function (menu) {
      var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'zf';

      menu.attr('role', 'menubar');

      var items = menu.find('li').attr({ 'role': 'menuitem' }),
          subMenuClass = 'is-' + type + '-submenu',
          subItemClass = subMenuClass + '-item',
          hasSubClass = 'is-' + type + '-submenu-parent';

      menu.find('a:first').attr('tabindex', 0);

      items.each(function () {
        var $item = $(this),
            $sub = $item.children('ul');

        if ($sub.length) {
          $item.addClass(hasSubClass).attr({
            'aria-haspopup': true,
            'aria-expanded': false,
            'aria-label': $item.children('a:first').text()
          });

          $sub.addClass('submenu ' + subMenuClass).attr({
            'data-submenu': '',
            'aria-hidden': true,
            'role': 'menu'
          });
        }

        if ($item.parent('[data-submenu]').length) {
          $item.addClass('is-submenu-item ' + subItemClass);
        }
      });

      return;
    },
    Burn: function (menu, type) {
      var items = menu.find('li').removeAttr('tabindex'),
          subMenuClass = 'is-' + type + '-submenu',
          subItemClass = subMenuClass + '-item',
          hasSubClass = 'is-' + type + '-submenu-parent';

      menu.find('>li, .menu, .menu > li').removeClass(subMenuClass + ' ' + subItemClass + ' ' + hasSubClass + ' is-submenu-item submenu is-active').removeAttr('data-submenu').css('display', '');

      // console.log(      menu.find('.' + subMenuClass + ', .' + subItemClass + ', .has-submenu, .is-submenu-item, .submenu, [data-submenu]')
      //           .removeClass(subMenuClass + ' ' + subItemClass + ' has-submenu is-submenu-item submenu')
      //           .removeAttr('data-submenu'));
      // items.each(function(){
      //   var $item = $(this),
      //       $sub = $item.children('ul');
      //   if($item.parent('[data-submenu]').length){
      //     $item.removeClass('is-submenu-item ' + subItemClass);
      //   }
      //   if($sub.length){
      //     $item.removeClass('has-submenu');
      //     $sub.removeClass('submenu ' + subMenuClass).removeAttr('data-submenu');
      //   }
      // });
    }
  };

  Foundation.Nest = Nest;
}(jQuery);
;'use strict';

!function ($) {

  function Timer(elem, options, cb) {
    var _this = this,
        duration = options.duration,
        //options is an object for easily adding features later.
    nameSpace = Object.keys(elem.data())[0] || 'timer',
        remain = -1,
        start,
        timer;

    this.isPaused = false;

    this.restart = function () {
      remain = -1;
      clearTimeout(timer);
      this.start();
    };

    this.start = function () {
      this.isPaused = false;
      // if(!elem.data('paused')){ return false; }//maybe implement this sanity check if used for other things.
      clearTimeout(timer);
      remain = remain <= 0 ? duration : remain;
      elem.data('paused', false);
      start = Date.now();
      timer = setTimeout(function () {
        if (options.infinite) {
          _this.restart(); //rerun the timer.
        }
        if (cb && typeof cb === 'function') {
          cb();
        }
      }, remain);
      elem.trigger('timerstart.zf.' + nameSpace);
    };

    this.pause = function () {
      this.isPaused = true;
      //if(elem.data('paused')){ return false; }//maybe implement this sanity check if used for other things.
      clearTimeout(timer);
      elem.data('paused', true);
      var end = Date.now();
      remain = remain - (end - start);
      elem.trigger('timerpaused.zf.' + nameSpace);
    };
  }

  /**
   * Runs a callback function when images are fully loaded.
   * @param {Object} images - Image(s) to check if loaded.
   * @param {Func} callback - Function to execute when image is fully loaded.
   */
  function onImagesLoaded(images, callback) {
    var self = this,
        unloaded = images.length;

    if (unloaded === 0) {
      callback();
    }

    images.each(function () {
      if (this.complete) {
        singleImageLoaded();
      } else if (typeof this.naturalWidth !== 'undefined' && this.naturalWidth > 0) {
        singleImageLoaded();
      } else {
        $(this).one('load', function () {
          singleImageLoaded();
        });
      }
    });

    function singleImageLoaded() {
      unloaded--;
      if (unloaded === 0) {
        callback();
      }
    }
  }

  Foundation.Timer = Timer;
  Foundation.onImagesLoaded = onImagesLoaded;
}(jQuery);
;'use strict';

//**************************************************
//**Work inspired by multiple jquery swipe plugins**
//**Done by Yohai Ararat ***************************
//**************************************************
(function ($) {

	$.spotSwipe = {
		version: '1.0.0',
		enabled: 'ontouchstart' in document.documentElement,
		preventDefault: false,
		moveThreshold: 75,
		timeThreshold: 200
	};

	var startPosX,
	    startPosY,
	    startTime,
	    elapsedTime,
	    isMoving = false;

	function onTouchEnd() {
		//  alert(this);
		this.removeEventListener('touchmove', onTouchMove);
		this.removeEventListener('touchend', onTouchEnd);
		isMoving = false;
	}

	function onTouchMove(e) {
		if ($.spotSwipe.preventDefault) {
			e.preventDefault();
		}
		if (isMoving) {
			var x = e.touches[0].pageX;
			var y = e.touches[0].pageY;
			var dx = startPosX - x;
			var dy = startPosY - y;
			var dir;
			elapsedTime = new Date().getTime() - startTime;
			if (Math.abs(dx) >= $.spotSwipe.moveThreshold && elapsedTime <= $.spotSwipe.timeThreshold) {
				dir = dx > 0 ? 'left' : 'right';
			}
			// else if(Math.abs(dy) >= $.spotSwipe.moveThreshold && elapsedTime <= $.spotSwipe.timeThreshold) {
			//   dir = dy > 0 ? 'down' : 'up';
			// }
			if (dir) {
				e.preventDefault();
				onTouchEnd.call(this);
				$(this).trigger('swipe', dir).trigger('swipe' + dir);
			}
		}
	}

	function onTouchStart(e) {
		if (e.touches.length == 1) {
			startPosX = e.touches[0].pageX;
			startPosY = e.touches[0].pageY;
			isMoving = true;
			startTime = new Date().getTime();
			this.addEventListener('touchmove', onTouchMove, false);
			this.addEventListener('touchend', onTouchEnd, false);
		}
	}

	function init() {
		this.addEventListener && this.addEventListener('touchstart', onTouchStart, false);
	}

	function teardown() {
		this.removeEventListener('touchstart', onTouchStart);
	}

	$.event.special.swipe = { setup: init };

	$.each(['left', 'up', 'down', 'right'], function () {
		$.event.special['swipe' + this] = { setup: function () {
				$(this).on('swipe', $.noop);
			} };
	});
})(jQuery);
/****************************************************
 * Method for adding psuedo drag events to elements *
 ***************************************************/
!function ($) {
	$.fn.addTouch = function () {
		this.each(function (i, el) {
			$(el).bind('touchstart touchmove touchend touchcancel', function () {
				//we pass the original event object because the jQuery event
				//object is normalized to w3c specs and does not provide the TouchList
				handleTouch(event);
			});
		});

		var handleTouch = function (event) {
			var touches = event.changedTouches,
			    first = touches[0],
			    eventTypes = {
				touchstart: 'mousedown',
				touchmove: 'mousemove',
				touchend: 'mouseup'
			},
			    type = eventTypes[event.type],
			    simulatedEvent;

			if ('MouseEvent' in window && typeof window.MouseEvent === 'function') {
				simulatedEvent = new window.MouseEvent(type, {
					'bubbles': true,
					'cancelable': true,
					'screenX': first.screenX,
					'screenY': first.screenY,
					'clientX': first.clientX,
					'clientY': first.clientY
				});
			} else {
				simulatedEvent = document.createEvent('MouseEvent');
				simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0 /*left*/, null);
			}
			first.target.dispatchEvent(simulatedEvent);
		};
	};
}(jQuery);

//**********************************
//**From the jQuery Mobile Library**
//**need to recreate functionality**
//**and try to improve if possible**
//**********************************

/* Removing the jQuery function ****
************************************

(function( $, window, undefined ) {

	var $document = $( document ),
		// supportTouch = $.mobile.support.touch,
		touchStartEvent = 'touchstart'//supportTouch ? "touchstart" : "mousedown",
		touchStopEvent = 'touchend'//supportTouch ? "touchend" : "mouseup",
		touchMoveEvent = 'touchmove'//supportTouch ? "touchmove" : "mousemove";

	// setup new event shortcuts
	$.each( ( "touchstart touchmove touchend " +
		"swipe swipeleft swiperight" ).split( " " ), function( i, name ) {

		$.fn[ name ] = function( fn ) {
			return fn ? this.bind( name, fn ) : this.trigger( name );
		};

		// jQuery < 1.8
		if ( $.attrFn ) {
			$.attrFn[ name ] = true;
		}
	});

	function triggerCustomEvent( obj, eventType, event, bubble ) {
		var originalType = event.type;
		event.type = eventType;
		if ( bubble ) {
			$.event.trigger( event, undefined, obj );
		} else {
			$.event.dispatch.call( obj, event );
		}
		event.type = originalType;
	}

	// also handles taphold

	// Also handles swipeleft, swiperight
	$.event.special.swipe = {

		// More than this horizontal displacement, and we will suppress scrolling.
		scrollSupressionThreshold: 30,

		// More time than this, and it isn't a swipe.
		durationThreshold: 1000,

		// Swipe horizontal displacement must be more than this.
		horizontalDistanceThreshold: window.devicePixelRatio >= 2 ? 15 : 30,

		// Swipe vertical displacement must be less than this.
		verticalDistanceThreshold: window.devicePixelRatio >= 2 ? 15 : 30,

		getLocation: function ( event ) {
			var winPageX = window.pageXOffset,
				winPageY = window.pageYOffset,
				x = event.clientX,
				y = event.clientY;

			if ( event.pageY === 0 && Math.floor( y ) > Math.floor( event.pageY ) ||
				event.pageX === 0 && Math.floor( x ) > Math.floor( event.pageX ) ) {

				// iOS4 clientX/clientY have the value that should have been
				// in pageX/pageY. While pageX/page/ have the value 0
				x = x - winPageX;
				y = y - winPageY;
			} else if ( y < ( event.pageY - winPageY) || x < ( event.pageX - winPageX ) ) {

				// Some Android browsers have totally bogus values for clientX/Y
				// when scrolling/zooming a page. Detectable since clientX/clientY
				// should never be smaller than pageX/pageY minus page scroll
				x = event.pageX - winPageX;
				y = event.pageY - winPageY;
			}

			return {
				x: x,
				y: y
			};
		},

		start: function( event ) {
			var data = event.originalEvent.touches ?
					event.originalEvent.touches[ 0 ] : event,
				location = $.event.special.swipe.getLocation( data );
			return {
						time: ( new Date() ).getTime(),
						coords: [ location.x, location.y ],
						origin: $( event.target )
					};
		},

		stop: function( event ) {
			var data = event.originalEvent.touches ?
					event.originalEvent.touches[ 0 ] : event,
				location = $.event.special.swipe.getLocation( data );
			return {
						time: ( new Date() ).getTime(),
						coords: [ location.x, location.y ]
					};
		},

		handleSwipe: function( start, stop, thisObject, origTarget ) {
			if ( stop.time - start.time < $.event.special.swipe.durationThreshold &&
				Math.abs( start.coords[ 0 ] - stop.coords[ 0 ] ) > $.event.special.swipe.horizontalDistanceThreshold &&
				Math.abs( start.coords[ 1 ] - stop.coords[ 1 ] ) < $.event.special.swipe.verticalDistanceThreshold ) {
				var direction = start.coords[0] > stop.coords[ 0 ] ? "swipeleft" : "swiperight";

				triggerCustomEvent( thisObject, "swipe", $.Event( "swipe", { target: origTarget, swipestart: start, swipestop: stop }), true );
				triggerCustomEvent( thisObject, direction,$.Event( direction, { target: origTarget, swipestart: start, swipestop: stop } ), true );
				return true;
			}
			return false;

		},

		// This serves as a flag to ensure that at most one swipe event event is
		// in work at any given time
		eventInProgress: false,

		setup: function() {
			var events,
				thisObject = this,
				$this = $( thisObject ),
				context = {};

			// Retrieve the events data for this element and add the swipe context
			events = $.data( this, "mobile-events" );
			if ( !events ) {
				events = { length: 0 };
				$.data( this, "mobile-events", events );
			}
			events.length++;
			events.swipe = context;

			context.start = function( event ) {

				// Bail if we're already working on a swipe event
				if ( $.event.special.swipe.eventInProgress ) {
					return;
				}
				$.event.special.swipe.eventInProgress = true;

				var stop,
					start = $.event.special.swipe.start( event ),
					origTarget = event.target,
					emitted = false;

				context.move = function( event ) {
					if ( !start || event.isDefaultPrevented() ) {
						return;
					}

					stop = $.event.special.swipe.stop( event );
					if ( !emitted ) {
						emitted = $.event.special.swipe.handleSwipe( start, stop, thisObject, origTarget );
						if ( emitted ) {

							// Reset the context to make way for the next swipe event
							$.event.special.swipe.eventInProgress = false;
						}
					}
					// prevent scrolling
					if ( Math.abs( start.coords[ 0 ] - stop.coords[ 0 ] ) > $.event.special.swipe.scrollSupressionThreshold ) {
						event.preventDefault();
					}
				};

				context.stop = function() {
						emitted = true;

						// Reset the context to make way for the next swipe event
						$.event.special.swipe.eventInProgress = false;
						$document.off( touchMoveEvent, context.move );
						context.move = null;
				};

				$document.on( touchMoveEvent, context.move )
					.one( touchStopEvent, context.stop );
			};
			$this.on( touchStartEvent, context.start );
		},

		teardown: function() {
			var events, context;

			events = $.data( this, "mobile-events" );
			if ( events ) {
				context = events.swipe;
				delete events.swipe;
				events.length--;
				if ( events.length === 0 ) {
					$.removeData( this, "mobile-events" );
				}
			}

			if ( context ) {
				if ( context.start ) {
					$( this ).off( touchStartEvent, context.start );
				}
				if ( context.move ) {
					$document.off( touchMoveEvent, context.move );
				}
				if ( context.stop ) {
					$document.off( touchStopEvent, context.stop );
				}
			}
		}
	};
	$.each({
		swipeleft: "swipe.left",
		swiperight: "swipe.right"
	}, function( event, sourceEvent ) {

		$.event.special[ event ] = {
			setup: function() {
				$( this ).bind( sourceEvent, $.noop );
			},
			teardown: function() {
				$( this ).unbind( sourceEvent );
			}
		};
	});
})( jQuery, this );
*/
;'use strict';

!function ($) {

  var MutationObserver = function () {
    var prefixes = ['WebKit', 'Moz', 'O', 'Ms', ''];
    for (var i = 0; i < prefixes.length; i++) {
      if (prefixes[i] + 'MutationObserver' in window) {
        return window[prefixes[i] + 'MutationObserver'];
      }
    }
    return false;
  }();

  var triggers = function (el, type) {
    el.data(type).split(' ').forEach(function (id) {
      $('#' + id)[type === 'close' ? 'trigger' : 'triggerHandler'](type + '.zf.trigger', [el]);
    });
  };
  // Elements with [data-open] will reveal a plugin that supports it when clicked.
  $(document).on('click.zf.trigger', '[data-open]', function () {
    triggers($(this), 'open');
  });

  // Elements with [data-close] will close a plugin that supports it when clicked.
  // If used without a value on [data-close], the event will bubble, allowing it to close a parent component.
  $(document).on('click.zf.trigger', '[data-close]', function () {
    var id = $(this).data('close');
    if (id) {
      triggers($(this), 'close');
    } else {
      $(this).trigger('close.zf.trigger');
    }
  });

  // Elements with [data-toggle] will toggle a plugin that supports it when clicked.
  $(document).on('click.zf.trigger', '[data-toggle]', function () {
    triggers($(this), 'toggle');
  });

  // Elements with [data-closable] will respond to close.zf.trigger events.
  $(document).on('close.zf.trigger', '[data-closable]', function (e) {
    e.stopPropagation();
    var animation = $(this).data('closable');

    if (animation !== '') {
      Foundation.Motion.animateOut($(this), animation, function () {
        $(this).trigger('closed.zf');
      });
    } else {
      $(this).fadeOut().trigger('closed.zf');
    }
  });

  $(document).on('focus.zf.trigger blur.zf.trigger', '[data-toggle-focus]', function () {
    var id = $(this).data('toggle-focus');
    $('#' + id).triggerHandler('toggle.zf.trigger', [$(this)]);
  });

  /**
  * Fires once after all other scripts have loaded
  * @function
  * @private
  */
  $(window).on('load', function () {
    checkListeners();
  });

  function checkListeners() {
    eventsListener();
    resizeListener();
    scrollListener();
    closemeListener();
  }

  //******** only fires this function once on load, if there's something to watch ********
  function closemeListener(pluginName) {
    var yetiBoxes = $('[data-yeti-box]'),
        plugNames = ['dropdown', 'tooltip', 'reveal'];

    if (pluginName) {
      if (typeof pluginName === 'string') {
        plugNames.push(pluginName);
      } else if (typeof pluginName === 'object' && typeof pluginName[0] === 'string') {
        plugNames.concat(pluginName);
      } else {
        console.error('Plugin names must be strings');
      }
    }
    if (yetiBoxes.length) {
      var listeners = plugNames.map(function (name) {
        return 'closeme.zf.' + name;
      }).join(' ');

      $(window).off(listeners).on(listeners, function (e, pluginId) {
        var plugin = e.namespace.split('.')[0];
        var plugins = $('[data-' + plugin + ']').not('[data-yeti-box="' + pluginId + '"]');

        plugins.each(function () {
          var _this = $(this);

          _this.triggerHandler('close.zf.trigger', [_this]);
        });
      });
    }
  }

  function resizeListener(debounce) {
    var timer = void 0,
        $nodes = $('[data-resize]');
    if ($nodes.length) {
      $(window).off('resize.zf.trigger').on('resize.zf.trigger', function (e) {
        if (timer) {
          clearTimeout(timer);
        }

        timer = setTimeout(function () {

          if (!MutationObserver) {
            //fallback for IE 9
            $nodes.each(function () {
              $(this).triggerHandler('resizeme.zf.trigger');
            });
          }
          //trigger all listening elements and signal a resize event
          $nodes.attr('data-events', "resize");
        }, debounce || 10); //default time to emit resize event
      });
    }
  }

  function scrollListener(debounce) {
    var timer = void 0,
        $nodes = $('[data-scroll]');
    if ($nodes.length) {
      $(window).off('scroll.zf.trigger').on('scroll.zf.trigger', function (e) {
        if (timer) {
          clearTimeout(timer);
        }

        timer = setTimeout(function () {

          if (!MutationObserver) {
            //fallback for IE 9
            $nodes.each(function () {
              $(this).triggerHandler('scrollme.zf.trigger');
            });
          }
          //trigger all listening elements and signal a scroll event
          $nodes.attr('data-events', "scroll");
        }, debounce || 10); //default time to emit scroll event
      });
    }
  }

  function eventsListener() {
    if (!MutationObserver) {
      return false;
    }
    var nodes = document.querySelectorAll('[data-resize], [data-scroll], [data-mutate]');

    //element callback
    var listeningElementsMutation = function (mutationRecordsList) {
      var $target = $(mutationRecordsList[0].target);
      //trigger the event handler for the element depending on type
      switch ($target.attr("data-events")) {

        case "resize":
          $target.triggerHandler('resizeme.zf.trigger', [$target]);
          break;

        case "scroll":
          $target.triggerHandler('scrollme.zf.trigger', [$target, window.pageYOffset]);
          break;

        // case "mutate" :
        // console.log('mutate', $target);
        // $target.triggerHandler('mutate.zf.trigger');
        //
        // //make sure we don't get stuck in an infinite loop from sloppy codeing
        // if ($target.index('[data-mutate]') == $("[data-mutate]").length-1) {
        //   domMutationObserver();
        // }
        // break;

        default:
          return false;
        //nothing
      }
    };

    if (nodes.length) {
      //for each element that needs to listen for resizing, scrolling, (or coming soon mutation) add a single observer
      for (var i = 0; i <= nodes.length - 1; i++) {
        var elementObserver = new MutationObserver(listeningElementsMutation);
        elementObserver.observe(nodes[i], { attributes: true, childList: false, characterData: false, subtree: false, attributeFilter: ["data-events"] });
      }
    }
  }

  // ------------------------------------

  // [PH]
  // Foundation.CheckWatchers = checkWatchers;
  Foundation.IHearYou = checkListeners;
  // Foundation.ISeeYou = scrollListener;
  // Foundation.IFeelYou = closemeListener;
}(jQuery);

// function domMutationObserver(debounce) {
//   // !!! This is coming soon and needs more work; not active  !!! //
//   var timer,
//   nodes = document.querySelectorAll('[data-mutate]');
//   //
//   if (nodes.length) {
//     // var MutationObserver = (function () {
//     //   var prefixes = ['WebKit', 'Moz', 'O', 'Ms', ''];
//     //   for (var i=0; i < prefixes.length; i++) {
//     //     if (prefixes[i] + 'MutationObserver' in window) {
//     //       return window[prefixes[i] + 'MutationObserver'];
//     //     }
//     //   }
//     //   return false;
//     // }());
//
//
//     //for the body, we need to listen for all changes effecting the style and class attributes
//     var bodyObserver = new MutationObserver(bodyMutation);
//     bodyObserver.observe(document.body, { attributes: true, childList: true, characterData: false, subtree:true, attributeFilter:["style", "class"]});
//
//
//     //body callback
//     function bodyMutation(mutate) {
//       //trigger all listening elements and signal a mutation event
//       if (timer) { clearTimeout(timer); }
//
//       timer = setTimeout(function() {
//         bodyObserver.disconnect();
//         $('[data-mutate]').attr('data-events',"mutate");
//       }, debounce || 150);
//     }
//   }
// }
;'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function ($) {

  /**
   * OffCanvas module.
   * @module foundation.offcanvas
   * @requires foundation.util.mediaQuery
   * @requires foundation.util.triggers
   * @requires foundation.util.motion
   */

  var OffCanvas = function () {
    /**
     * Creates a new instance of an off-canvas wrapper.
     * @class
     * @fires OffCanvas#init
     * @param {Object} element - jQuery object to initialize.
     * @param {Object} options - Overrides to the default plugin settings.
     */
    function OffCanvas(element, options) {
      _classCallCheck(this, OffCanvas);

      this.$element = element;
      this.options = $.extend({}, OffCanvas.defaults, this.$element.data(), options);
      this.$lastTrigger = $();
      this.$triggers = $();

      this._init();
      this._events();

      Foundation.registerPlugin(this, 'OffCanvas');
      Foundation.Keyboard.register('OffCanvas', {
        'ESCAPE': 'close'
      });
    }

    /**
     * Initializes the off-canvas wrapper by adding the exit overlay (if needed).
     * @function
     * @private
     */


    _createClass(OffCanvas, [{
      key: '_init',
      value: function _init() {
        var id = this.$element.attr('id');

        this.$element.attr('aria-hidden', 'true');

        // Find triggers that affect this element and add aria-expanded to them
        this.$triggers = $(document).find('[data-open="' + id + '"], [data-close="' + id + '"], [data-toggle="' + id + '"]').attr('aria-expanded', 'false').attr('aria-controls', id);

        // Add a close trigger over the body if necessary
        if (this.options.closeOnClick) {
          if ($('.js-off-canvas-exit').length) {
            this.$exiter = $('.js-off-canvas-exit');
          } else {
            var exiter = document.createElement('div');
            exiter.setAttribute('class', 'js-off-canvas-exit');
            $('[data-off-canvas-content]').append(exiter);

            this.$exiter = $(exiter);
          }
        }

        this.options.isRevealed = this.options.isRevealed || new RegExp(this.options.revealClass, 'g').test(this.$element[0].className);

        if (this.options.isRevealed) {
          this.options.revealOn = this.options.revealOn || this.$element[0].className.match(/(reveal-for-medium|reveal-for-large)/g)[0].split('-')[2];
          this._setMQChecker();
        }
        if (!this.options.transitionTime) {
          this.options.transitionTime = parseFloat(window.getComputedStyle($('[data-off-canvas-wrapper]')[0]).transitionDuration) * 1000;
        }
      }

      /**
       * Adds event handlers to the off-canvas wrapper and the exit overlay.
       * @function
       * @private
       */

    }, {
      key: '_events',
      value: function _events() {
        this.$element.off('.zf.trigger .zf.offcanvas').on({
          'open.zf.trigger': this.open.bind(this),
          'close.zf.trigger': this.close.bind(this),
          'toggle.zf.trigger': this.toggle.bind(this),
          'keydown.zf.offcanvas': this._handleKeyboard.bind(this)
        });

        if (this.options.closeOnClick && this.$exiter.length) {
          this.$exiter.on({ 'click.zf.offcanvas': this.close.bind(this) });
        }
      }

      /**
       * Applies event listener for elements that will reveal at certain breakpoints.
       * @private
       */

    }, {
      key: '_setMQChecker',
      value: function _setMQChecker() {
        var _this = this;

        $(window).on('changed.zf.mediaquery', function () {
          if (Foundation.MediaQuery.atLeast(_this.options.revealOn)) {
            _this.reveal(true);
          } else {
            _this.reveal(false);
          }
        }).one('load.zf.offcanvas', function () {
          if (Foundation.MediaQuery.atLeast(_this.options.revealOn)) {
            _this.reveal(true);
          }
        });
      }

      /**
       * Handles the revealing/hiding the off-canvas at breakpoints, not the same as open.
       * @param {Boolean} isRevealed - true if element should be revealed.
       * @function
       */

    }, {
      key: 'reveal',
      value: function reveal(isRevealed) {
        var $closer = this.$element.find('[data-close]');
        if (isRevealed) {
          this.close();
          this.isRevealed = true;
          // if (!this.options.forceTop) {
          //   var scrollPos = parseInt(window.pageYOffset);
          //   this.$element[0].style.transform = 'translate(0,' + scrollPos + 'px)';
          // }
          // if (this.options.isSticky) { this._stick(); }
          this.$element.off('open.zf.trigger toggle.zf.trigger');
          if ($closer.length) {
            $closer.hide();
          }
        } else {
          this.isRevealed = false;
          // if (this.options.isSticky || !this.options.forceTop) {
          //   this.$element[0].style.transform = '';
          //   $(window).off('scroll.zf.offcanvas');
          // }
          this.$element.on({
            'open.zf.trigger': this.open.bind(this),
            'toggle.zf.trigger': this.toggle.bind(this)
          });
          if ($closer.length) {
            $closer.show();
          }
        }
      }

      /**
       * Opens the off-canvas menu.
       * @function
       * @param {Object} event - Event object passed from listener.
       * @param {jQuery} trigger - element that triggered the off-canvas to open.
       * @fires OffCanvas#opened
       */

    }, {
      key: 'open',
      value: function open(event, trigger) {
        if (this.$element.hasClass('is-open') || this.isRevealed) {
          return;
        }
        var _this = this,
            $body = $(document.body);

        if (this.options.forceTop) {
          $('body').scrollTop(0);
        }
        // window.pageYOffset = 0;

        // if (!this.options.forceTop) {
        //   var scrollPos = parseInt(window.pageYOffset);
        //   this.$element[0].style.transform = 'translate(0,' + scrollPos + 'px)';
        //   if (this.$exiter.length) {
        //     this.$exiter[0].style.transform = 'translate(0,' + scrollPos + 'px)';
        //   }
        // }
        /**
         * Fires when the off-canvas menu opens.
         * @event OffCanvas#opened
         */

        var $wrapper = $('[data-off-canvas-wrapper]');
        $wrapper.addClass('is-off-canvas-open is-open-' + _this.options.position);

        _this.$element.addClass('is-open');

        // if (_this.options.isSticky) {
        //   _this._stick();
        // }

        this.$triggers.attr('aria-expanded', 'true');
        this.$element.attr('aria-hidden', 'false').trigger('opened.zf.offcanvas');

        if (this.options.closeOnClick) {
          this.$exiter.addClass('is-visible');
        }

        if (trigger) {
          this.$lastTrigger = trigger;
        }

        if (this.options.autoFocus) {
          $wrapper.one(Foundation.transitionend($wrapper), function () {
            if (_this.$element.hasClass('is-open')) {
              // handle double clicks
              _this.$element.attr('tabindex', '-1');
              _this.$element.focus();
            }
          });
        }

        if (this.options.trapFocus) {
          $wrapper.one(Foundation.transitionend($wrapper), function () {
            if (_this.$element.hasClass('is-open')) {
              // handle double clicks
              _this.$element.attr('tabindex', '-1');
              _this.trapFocus();
            }
          });
        }
      }

      /**
       * Traps focus within the offcanvas on open.
       * @private
       */

    }, {
      key: '_trapFocus',
      value: function _trapFocus() {
        var focusable = Foundation.Keyboard.findFocusable(this.$element),
            first = focusable.eq(0),
            last = focusable.eq(-1);

        focusable.off('.zf.offcanvas').on('keydown.zf.offcanvas', function (e) {
          var key = Foundation.Keyboard.parseKey(e);
          if (key === 'TAB' && e.target === last[0]) {
            e.preventDefault();
            first.focus();
          }
          if (key === 'SHIFT_TAB' && e.target === first[0]) {
            e.preventDefault();
            last.focus();
          }
        });
      }

      /**
       * Allows the offcanvas to appear sticky utilizing translate properties.
       * @private
       */
      // OffCanvas.prototype._stick = function() {
      //   var elStyle = this.$element[0].style;
      //
      //   if (this.options.closeOnClick) {
      //     var exitStyle = this.$exiter[0].style;
      //   }
      //
      //   $(window).on('scroll.zf.offcanvas', function(e) {
      //     console.log(e);
      //     var pageY = window.pageYOffset;
      //     elStyle.transform = 'translate(0,' + pageY + 'px)';
      //     if (exitStyle !== undefined) { exitStyle.transform = 'translate(0,' + pageY + 'px)'; }
      //   });
      //   // this.$element.trigger('stuck.zf.offcanvas');
      // };
      /**
       * Closes the off-canvas menu.
       * @function
       * @param {Function} cb - optional cb to fire after closure.
       * @fires OffCanvas#closed
       */

    }, {
      key: 'close',
      value: function close(cb) {
        if (!this.$element.hasClass('is-open') || this.isRevealed) {
          return;
        }

        var _this = this;

        //  Foundation.Move(this.options.transitionTime, this.$element, function() {
        $('[data-off-canvas-wrapper]').removeClass('is-off-canvas-open is-open-' + _this.options.position);
        _this.$element.removeClass('is-open');
        // Foundation._reflow();
        // });
        this.$element.attr('aria-hidden', 'true')
        /**
         * Fires when the off-canvas menu opens.
         * @event OffCanvas#closed
         */
        .trigger('closed.zf.offcanvas');
        // if (_this.options.isSticky || !_this.options.forceTop) {
        //   setTimeout(function() {
        //     _this.$element[0].style.transform = '';
        //     $(window).off('scroll.zf.offcanvas');
        //   }, this.options.transitionTime);
        // }
        if (this.options.closeOnClick) {
          this.$exiter.removeClass('is-visible');
        }

        this.$triggers.attr('aria-expanded', 'false');
        if (this.options.trapFocus) {
          $('[data-off-canvas-content]').removeAttr('tabindex');
        }
      }

      /**
       * Toggles the off-canvas menu open or closed.
       * @function
       * @param {Object} event - Event object passed from listener.
       * @param {jQuery} trigger - element that triggered the off-canvas to open.
       */

    }, {
      key: 'toggle',
      value: function toggle(event, trigger) {
        if (this.$element.hasClass('is-open')) {
          this.close(event, trigger);
        } else {
          this.open(event, trigger);
        }
      }

      /**
       * Handles keyboard input when detected. When the escape key is pressed, the off-canvas menu closes, and focus is restored to the element that opened the menu.
       * @function
       * @private
       */

    }, {
      key: '_handleKeyboard',
      value: function _handleKeyboard(e) {
        var _this2 = this;

        Foundation.Keyboard.handleKey(e, 'OffCanvas', {
          close: function () {
            _this2.close();
            _this2.$lastTrigger.focus();
            return true;
          },
          handled: function () {
            e.stopPropagation();
            e.preventDefault();
          }
        });
      }

      /**
       * Destroys the offcanvas plugin.
       * @function
       */

    }, {
      key: 'destroy',
      value: function destroy() {
        this.close();
        this.$element.off('.zf.trigger .zf.offcanvas');
        this.$exiter.off('.zf.offcanvas');

        Foundation.unregisterPlugin(this);
      }
    }]);

    return OffCanvas;
  }();

  OffCanvas.defaults = {
    /**
     * Allow the user to click outside of the menu to close it.
     * @option
     * @example true
     */
    closeOnClick: true,

    /**
     * Amount of time in ms the open and close transition requires. If none selected, pulls from body style.
     * @option
     * @example 500
     */
    transitionTime: 0,

    /**
     * Direction the offcanvas opens from. Determines class applied to body.
     * @option
     * @example left
     */
    position: 'left',

    /**
     * Force the page to scroll to top on open.
     * @option
     * @example true
     */
    forceTop: true,

    /**
     * Allow the offcanvas to remain open for certain breakpoints.
     * @option
     * @example false
     */
    isRevealed: false,

    /**
     * Breakpoint at which to reveal. JS will use a RegExp to target standard classes, if changing classnames, pass your class with the `revealClass` option.
     * @option
     * @example reveal-for-large
     */
    revealOn: null,

    /**
     * Force focus to the offcanvas on open. If true, will focus the opening trigger on close. Sets tabindex of [data-off-canvas-content] to -1 for accessibility purposes.
     * @option
     * @example true
     */
    autoFocus: true,

    /**
     * Class used to force an offcanvas to remain open. Foundation defaults for this are `reveal-for-large` & `reveal-for-medium`.
     * @option
     * TODO improve the regex testing for this.
     * @example reveal-for-large
     */
    revealClass: 'reveal-for-',

    /**
     * Triggers optional focus trapping when opening an offcanvas. Sets tabindex of [data-off-canvas-content] to -1 for accessibility purposes.
     * @option
     * @example true
     */
    trapFocus: false

    // Window exports
  };Foundation.plugin(OffCanvas, 'OffCanvas');
}(jQuery);
;'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function ($) {

  /**
   * Reveal module.
   * @module foundation.reveal
   * @requires foundation.util.keyboard
   * @requires foundation.util.box
   * @requires foundation.util.triggers
   * @requires foundation.util.mediaQuery
   * @requires foundation.util.motion if using animations
   */

  var Reveal = function () {
    /**
     * Creates a new instance of Reveal.
     * @class
     * @param {jQuery} element - jQuery object to use for the modal.
     * @param {Object} options - optional parameters.
     */
    function Reveal(element, options) {
      _classCallCheck(this, Reveal);

      this.$element = element;
      this.options = $.extend({}, Reveal.defaults, this.$element.data(), options);
      this._init();

      Foundation.registerPlugin(this, 'Reveal');
      Foundation.Keyboard.register('Reveal', {
        'ENTER': 'open',
        'SPACE': 'open',
        'ESCAPE': 'close',
        'TAB': 'tab_forward',
        'SHIFT_TAB': 'tab_backward'
      });
    }

    /**
     * Initializes the modal by adding the overlay and close buttons, (if selected).
     * @private
     */


    _createClass(Reveal, [{
      key: '_init',
      value: function _init() {
        this.id = this.$element.attr('id');
        this.isActive = false;
        this.cached = { mq: Foundation.MediaQuery.current };
        this.isMobile = mobileSniff();

        this.$anchor = $('[data-open="' + this.id + '"]').length ? $('[data-open="' + this.id + '"]') : $('[data-toggle="' + this.id + '"]');
        this.$anchor.attr({
          'aria-controls': this.id,
          'aria-haspopup': true,
          'tabindex': 0
        });

        if (this.options.fullScreen || this.$element.hasClass('full')) {
          this.options.fullScreen = true;
          this.options.overlay = false;
        }
        if (this.options.overlay && !this.$overlay) {
          this.$overlay = this._makeOverlay(this.id);
        }

        this.$element.attr({
          'role': 'dialog',
          'aria-hidden': true,
          'data-yeti-box': this.id,
          'data-resize': this.id
        });

        if (this.$overlay) {
          this.$element.detach().appendTo(this.$overlay);
        } else {
          this.$element.detach().appendTo($('body'));
          this.$element.addClass('without-overlay');
        }
        this._events();
        if (this.options.deepLink && window.location.hash === '#' + this.id) {
          $(window).one('load.zf.reveal', this.open.bind(this));
        }
      }

      /**
       * Creates an overlay div to display behind the modal.
       * @private
       */

    }, {
      key: '_makeOverlay',
      value: function _makeOverlay(id) {
        var $overlay = $('<div></div>').addClass('reveal-overlay').appendTo('body');
        return $overlay;
      }

      /**
       * Updates position of modal
       * TODO:  Figure out if we actually need to cache these values or if it doesn't matter
       * @private
       */

    }, {
      key: '_updatePosition',
      value: function _updatePosition() {
        var width = this.$element.outerWidth();
        var outerWidth = $(window).width();
        var height = this.$element.outerHeight();
        var outerHeight = $(window).height();
        var left, top;
        if (this.options.hOffset === 'auto') {
          left = parseInt((outerWidth - width) / 2, 10);
        } else {
          left = parseInt(this.options.hOffset, 10);
        }
        if (this.options.vOffset === 'auto') {
          if (height > outerHeight) {
            top = parseInt(Math.min(100, outerHeight / 10), 10);
          } else {
            top = parseInt((outerHeight - height) / 4, 10);
          }
        } else {
          top = parseInt(this.options.vOffset, 10);
        }
        this.$element.css({ top: top + 'px' });
        // only worry about left if we don't have an overlay or we havea  horizontal offset,
        // otherwise we're perfectly in the middle
        if (!this.$overlay || this.options.hOffset !== 'auto') {
          this.$element.css({ left: left + 'px' });
          this.$element.css({ margin: '0px' });
        }
      }

      /**
       * Adds event handlers for the modal.
       * @private
       */

    }, {
      key: '_events',
      value: function _events() {
        var _this2 = this;

        var _this = this;

        this.$element.on({
          'open.zf.trigger': this.open.bind(this),
          'close.zf.trigger': function (event, $element) {
            if (event.target === _this.$element[0] || $(event.target).parents('[data-closable]')[0] === $element) {
              // only close reveal when it's explicitly called
              return _this2.close.apply(_this2);
            }
          },
          'toggle.zf.trigger': this.toggle.bind(this),
          'resizeme.zf.trigger': function () {
            _this._updatePosition();
          }
        });

        if (this.$anchor.length) {
          this.$anchor.on('keydown.zf.reveal', function (e) {
            if (e.which === 13 || e.which === 32) {
              e.stopPropagation();
              e.preventDefault();
              _this.open();
            }
          });
        }

        if (this.options.closeOnClick && this.options.overlay) {
          this.$overlay.off('.zf.reveal').on('click.zf.reveal', function (e) {
            if (e.target === _this.$element[0] || $.contains(_this.$element[0], e.target) || !$.contains(document, e.target)) {
              return;
            }
            _this.close();
          });
        }
        if (this.options.deepLink) {
          $(window).on('popstate.zf.reveal:' + this.id, this._handleState.bind(this));
        }
      }

      /**
       * Handles modal methods on back/forward button clicks or any other event that triggers popstate.
       * @private
       */

    }, {
      key: '_handleState',
      value: function _handleState(e) {
        if (window.location.hash === '#' + this.id && !this.isActive) {
          this.open();
        } else {
          this.close();
        }
      }

      /**
       * Opens the modal controlled by `this.$anchor`, and closes all others by default.
       * @function
       * @fires Reveal#closeme
       * @fires Reveal#open
       */

    }, {
      key: 'open',
      value: function open() {
        var _this3 = this;

        if (this.options.deepLink) {
          var hash = '#' + this.id;

          if (window.history.pushState) {
            window.history.pushState(null, null, hash);
          } else {
            window.location.hash = hash;
          }
        }

        this.isActive = true;

        // Make elements invisible, but remove display: none so we can get size and positioning
        this.$element.css({ 'visibility': 'hidden' }).show().scrollTop(0);
        if (this.options.overlay) {
          this.$overlay.css({ 'visibility': 'hidden' }).show();
        }

        this._updatePosition();

        this.$element.hide().css({ 'visibility': '' });

        if (this.$overlay) {
          this.$overlay.css({ 'visibility': '' }).hide();
          if (this.$element.hasClass('fast')) {
            this.$overlay.addClass('fast');
          } else if (this.$element.hasClass('slow')) {
            this.$overlay.addClass('slow');
          }
        }

        if (!this.options.multipleOpened) {
          /**
           * Fires immediately before the modal opens.
           * Closes any other modals that are currently open
           * @event Reveal#closeme
           */
          this.$element.trigger('closeme.zf.reveal', this.id);
        }
        // Motion UI method of reveal
        if (this.options.animationIn) {
          var afterAnimationFocus = function () {
            _this.$element.attr({
              'aria-hidden': false,
              'tabindex': -1
            }).focus();
          };

          var _this = this;

          if (this.options.overlay) {
            Foundation.Motion.animateIn(this.$overlay, 'fade-in');
          }
          Foundation.Motion.animateIn(this.$element, this.options.animationIn, function () {
            _this3.focusableElements = Foundation.Keyboard.findFocusable(_this3.$element);
            afterAnimationFocus();
          });
        }
        // jQuery method of reveal
        else {
            if (this.options.overlay) {
              this.$overlay.show(0);
            }
            this.$element.show(this.options.showDelay);
          }

        // handle accessibility
        this.$element.attr({
          'aria-hidden': false,
          'tabindex': -1
        }).focus();

        /**
         * Fires when the modal has successfully opened.
         * @event Reveal#open
         */
        this.$element.trigger('open.zf.reveal');

        if (this.isMobile) {
          this.originalScrollPos = window.pageYOffset;
          $('html, body').addClass('is-reveal-open');
        } else {
          $('body').addClass('is-reveal-open');
        }

        setTimeout(function () {
          _this3._extraHandlers();
        }, 0);
      }

      /**
       * Adds extra event handlers for the body and window if necessary.
       * @private
       */

    }, {
      key: '_extraHandlers',
      value: function _extraHandlers() {
        var _this = this;
        this.focusableElements = Foundation.Keyboard.findFocusable(this.$element);

        if (!this.options.overlay && this.options.closeOnClick && !this.options.fullScreen) {
          $('body').on('click.zf.reveal', function (e) {
            if (e.target === _this.$element[0] || $.contains(_this.$element[0], e.target) || !$.contains(document, e.target)) {
              return;
            }
            _this.close();
          });
        }

        if (this.options.closeOnEsc) {
          $(window).on('keydown.zf.reveal', function (e) {
            Foundation.Keyboard.handleKey(e, 'Reveal', {
              close: function () {
                if (_this.options.closeOnEsc) {
                  _this.close();
                  _this.$anchor.focus();
                }
              }
            });
          });
        }

        // lock focus within modal while tabbing
        this.$element.on('keydown.zf.reveal', function (e) {
          var $target = $(this);
          // handle keyboard event with keyboard util
          Foundation.Keyboard.handleKey(e, 'Reveal', {
            tab_forward: function () {
              _this.focusableElements = Foundation.Keyboard.findFocusable(_this.$element);
              if (_this.$element.find(':focus').is(_this.focusableElements.eq(-1))) {
                // left modal downwards, setting focus to first element
                _this.focusableElements.eq(0).focus();
                return true;
              }
              if (_this.focusableElements.length === 0) {
                // no focusable elements inside the modal at all, prevent tabbing in general
                return true;
              }
            },
            tab_backward: function () {
              _this.focusableElements = Foundation.Keyboard.findFocusable(_this.$element);
              if (_this.$element.find(':focus').is(_this.focusableElements.eq(0)) || _this.$element.is(':focus')) {
                // left modal upwards, setting focus to last element
                _this.focusableElements.eq(-1).focus();
                return true;
              }
              if (_this.focusableElements.length === 0) {
                // no focusable elements inside the modal at all, prevent tabbing in general
                return true;
              }
            },
            open: function () {
              if (_this.$element.find(':focus').is(_this.$element.find('[data-close]'))) {
                setTimeout(function () {
                  // set focus back to anchor if close button has been activated
                  _this.$anchor.focus();
                }, 1);
              } else if ($target.is(_this.focusableElements)) {
                // dont't trigger if acual element has focus (i.e. inputs, links, ...)
                _this.open();
              }
            },
            close: function () {
              if (_this.options.closeOnEsc) {
                _this.close();
                _this.$anchor.focus();
              }
            },
            handled: function (preventDefault) {
              if (preventDefault) {
                e.preventDefault();
              }
            }
          });
        });
      }

      /**
       * Closes the modal.
       * @function
       * @fires Reveal#closed
       */

    }, {
      key: 'close',
      value: function close() {
        if (!this.isActive || !this.$element.is(':visible')) {
          return false;
        }
        var _this = this;

        // Motion UI method of hiding
        if (this.options.animationOut) {
          if (this.options.overlay) {
            Foundation.Motion.animateOut(this.$overlay, 'fade-out', finishUp);
          } else {
            finishUp();
          }

          Foundation.Motion.animateOut(this.$element, this.options.animationOut);
        }
        // jQuery method of hiding
        else {
            if (this.options.overlay) {
              this.$overlay.hide(0, finishUp);
            } else {
              finishUp();
            }

            this.$element.hide(this.options.hideDelay);
          }

        // Conditionals to remove extra event listeners added on open
        if (this.options.closeOnEsc) {
          $(window).off('keydown.zf.reveal');
        }

        if (!this.options.overlay && this.options.closeOnClick) {
          $('body').off('click.zf.reveal');
        }

        this.$element.off('keydown.zf.reveal');

        function finishUp() {
          if (_this.isMobile) {
            $('html, body').removeClass('is-reveal-open');
            if (_this.originalScrollPos) {
              $('body').scrollTop(_this.originalScrollPos);
              _this.originalScrollPos = null;
            }
          } else {
            $('body').removeClass('is-reveal-open');
          }

          _this.$element.attr('aria-hidden', true);

          /**
          * Fires when the modal is done closing.
          * @event Reveal#closed
          */
          _this.$element.trigger('closed.zf.reveal');
        }

        /**
        * Resets the modal content
        * This prevents a running video to keep going in the background
        */
        if (this.options.resetOnClose) {
          this.$element.html(this.$element.html());
        }

        this.isActive = false;
        if (_this.options.deepLink) {
          if (window.history.replaceState) {
            window.history.replaceState("", document.title, window.location.pathname);
          } else {
            window.location.hash = '';
          }
        }
      }

      /**
       * Toggles the open/closed state of a modal.
       * @function
       */

    }, {
      key: 'toggle',
      value: function toggle() {
        if (this.isActive) {
          this.close();
        } else {
          this.open();
        }
      }
    }, {
      key: 'destroy',


      /**
       * Destroys an instance of a modal.
       * @function
       */
      value: function destroy() {
        if (this.options.overlay) {
          this.$element.appendTo($('body')); // move $element outside of $overlay to prevent error unregisterPlugin()
          this.$overlay.hide().off().remove();
        }
        this.$element.hide().off();
        this.$anchor.off('.zf');
        $(window).off('.zf.reveal:' + this.id);

        Foundation.unregisterPlugin(this);
      }
    }]);

    return Reveal;
  }();

  Reveal.defaults = {
    /**
     * Motion-UI class to use for animated elements. If none used, defaults to simple show/hide.
     * @option
     * @example 'slide-in-left'
     */
    animationIn: '',
    /**
     * Motion-UI class to use for animated elements. If none used, defaults to simple show/hide.
     * @option
     * @example 'slide-out-right'
     */
    animationOut: '',
    /**
     * Time, in ms, to delay the opening of a modal after a click if no animation used.
     * @option
     * @example 10
     */
    showDelay: 0,
    /**
     * Time, in ms, to delay the closing of a modal after a click if no animation used.
     * @option
     * @example 10
     */
    hideDelay: 0,
    /**
     * Allows a click on the body/overlay to close the modal.
     * @option
     * @example true
     */
    closeOnClick: true,
    /**
     * Allows the modal to close if the user presses the `ESCAPE` key.
     * @option
     * @example true
     */
    closeOnEsc: true,
    /**
     * If true, allows multiple modals to be displayed at once.
     * @option
     * @example false
     */
    multipleOpened: false,
    /**
     * Distance, in pixels, the modal should push down from the top of the screen.
     * @option
     * @example auto
     */
    vOffset: 'auto',
    /**
     * Distance, in pixels, the modal should push in from the side of the screen.
     * @option
     * @example auto
     */
    hOffset: 'auto',
    /**
     * Allows the modal to be fullscreen, completely blocking out the rest of the view. JS checks for this as well.
     * @option
     * @example false
     */
    fullScreen: false,
    /**
     * Percentage of screen height the modal should push up from the bottom of the view.
     * @option
     * @example 10
     */
    btmOffsetPct: 10,
    /**
     * Allows the modal to generate an overlay div, which will cover the view when modal opens.
     * @option
     * @example true
     */
    overlay: true,
    /**
     * Allows the modal to remove and reinject markup on close. Should be true if using video elements w/o using provider's api, otherwise, videos will continue to play in the background.
     * @option
     * @example false
     */
    resetOnClose: false,
    /**
     * Allows the modal to alter the url on open/close, and allows the use of the `back` button to close modals. ALSO, allows a modal to auto-maniacally open on page load IF the hash === the modal's user-set id.
     * @option
     * @example false
     */
    deepLink: false
  };

  // Window exports
  Foundation.plugin(Reveal, 'Reveal');

  function iPhoneSniff() {
    return (/iP(ad|hone|od).*OS/.test(window.navigator.userAgent)
    );
  }

  function androidSniff() {
    return (/Android/.test(window.navigator.userAgent)
    );
  }

  function mobileSniff() {
    return iPhoneSniff() || androidSniff();
  }
}(jQuery);
;'use strict';

/*!
 * Waves v0.6.4
 * http://fian.my.id/Waves
 *
 * Copyright 2014 Alfiana E. Sibuea and other contributors
 * Released under the MIT license
 * https://github.com/fians/Waves/blob/master/LICENSE
 */

;(function (window) {
    'use strict';

    var Waves = Waves || {};
    var $$ = document.querySelectorAll.bind(document);

    // Find exact position of element
    function isWindow(obj) {
        return obj !== null && obj === obj.window;
    }

    function getWindow(elem) {
        return isWindow(elem) ? elem : elem.nodeType === 9 && elem.defaultView;
    }

    function offset(elem) {
        var docElem,
            win,
            box = { top: 0, left: 0 },
            doc = elem && elem.ownerDocument;

        docElem = doc.documentElement;

        if (typeof elem.getBoundingClientRect !== typeof undefined) {
            box = elem.getBoundingClientRect();
        }
        win = getWindow(doc);
        return {
            top: box.top + win.pageYOffset - docElem.clientTop,
            left: box.left + win.pageXOffset - docElem.clientLeft
        };
    }

    function convertStyle(obj) {
        var style = '';

        for (var a in obj) {
            if (obj.hasOwnProperty(a)) {
                style += a + ':' + obj[a] + ';';
            }
        }

        return style;
    }

    var Effect = {

        // Effect delay
        duration: 750,

        show: function (e, element) {

            // Disable right click
            if (e.button === 2) {
                return false;
            }

            var el = element || this;

            // Create ripple
            var ripple = document.createElement('div');
            ripple.className = 'waves-ripple';
            el.appendChild(ripple);

            // Get click coordinate and element witdh
            var pos = offset(el);
            var relativeY = e.pageY - pos.top;
            var relativeX = e.pageX - pos.left;
            var scale = 'scale(' + el.clientWidth / 100 * 10 + ')';

            // Support for touch devices
            if ('touches' in e) {
                relativeY = e.touches[0].pageY - pos.top;
                relativeX = e.touches[0].pageX - pos.left;
            }

            // Attach data to element
            ripple.setAttribute('data-hold', Date.now());
            ripple.setAttribute('data-scale', scale);
            ripple.setAttribute('data-x', relativeX);
            ripple.setAttribute('data-y', relativeY);

            // Set ripple position
            var rippleStyle = {
                'top': relativeY + 'px',
                'left': relativeX + 'px'
            };

            ripple.className = ripple.className + ' waves-notransition';
            ripple.setAttribute('style', convertStyle(rippleStyle));
            ripple.className = ripple.className.replace('waves-notransition', '');

            // Scale the ripple
            rippleStyle['-webkit-transform'] = scale;
            rippleStyle['-moz-transform'] = scale;
            rippleStyle['-ms-transform'] = scale;
            rippleStyle['-o-transform'] = scale;
            rippleStyle.transform = scale;
            rippleStyle.opacity = '1';

            rippleStyle['-webkit-transition-duration'] = Effect.duration + 'ms';
            rippleStyle['-moz-transition-duration'] = Effect.duration + 'ms';
            rippleStyle['-o-transition-duration'] = Effect.duration + 'ms';
            rippleStyle['transition-duration'] = Effect.duration + 'ms';

            rippleStyle['-webkit-transition-timing-function'] = 'cubic-bezier(0.250, 0.460, 0.450, 0.940)';
            rippleStyle['-moz-transition-timing-function'] = 'cubic-bezier(0.250, 0.460, 0.450, 0.940)';
            rippleStyle['-o-transition-timing-function'] = 'cubic-bezier(0.250, 0.460, 0.450, 0.940)';
            rippleStyle['transition-timing-function'] = 'cubic-bezier(0.250, 0.460, 0.450, 0.940)';

            ripple.setAttribute('style', convertStyle(rippleStyle));
        },

        hide: function (e) {
            TouchHandler.touchup(e);

            var el = this;
            var width = el.clientWidth * 1.4;

            // Get first ripple
            var ripple = null;
            var ripples = el.getElementsByClassName('waves-ripple');
            if (ripples.length > 0) {
                ripple = ripples[ripples.length - 1];
            } else {
                return false;
            }

            var relativeX = ripple.getAttribute('data-x');
            var relativeY = ripple.getAttribute('data-y');
            var scale = ripple.getAttribute('data-scale');

            // Get delay beetween mousedown and mouse leave
            var diff = Date.now() - Number(ripple.getAttribute('data-hold'));
            var delay = 350 - diff;

            if (delay < 0) {
                delay = 0;
            }

            // Fade out ripple after delay
            setTimeout(function () {
                var style = {
                    'top': relativeY + 'px',
                    'left': relativeX + 'px',
                    'opacity': '0',

                    // Duration
                    '-webkit-transition-duration': Effect.duration + 'ms',
                    '-moz-transition-duration': Effect.duration + 'ms',
                    '-o-transition-duration': Effect.duration + 'ms',
                    'transition-duration': Effect.duration + 'ms',
                    '-webkit-transform': scale,
                    '-moz-transform': scale,
                    '-ms-transform': scale,
                    '-o-transform': scale,
                    'transform': scale
                };

                ripple.setAttribute('style', convertStyle(style));

                setTimeout(function () {
                    try {
                        el.removeChild(ripple);
                    } catch (e) {
                        return false;
                    }
                }, Effect.duration);
            }, delay);
        },

        // Little hack to make <input> can perform waves effect
        wrapInput: function (elements) {
            for (var a = 0; a < elements.length; a++) {
                var el = elements[a];

                if (el.tagName.toLowerCase() === 'input') {
                    var parent = el.parentNode;

                    // If input already have parent just pass through
                    if (parent.tagName.toLowerCase() === 'i' && parent.className.indexOf('waves-effect') !== -1) {
                        continue;
                    }

                    // Put element class and style to the specified parent
                    var wrapper = document.createElement('i');
                    wrapper.className = el.className + ' waves-input-wrapper';

                    var elementStyle = el.getAttribute('style');

                    if (!elementStyle) {
                        elementStyle = '';
                    }

                    wrapper.setAttribute('style', elementStyle);

                    el.className = 'waves-button-input';
                    el.removeAttribute('style');

                    // Put element as child
                    parent.replaceChild(wrapper, el);
                    wrapper.appendChild(el);
                }
            }
        }
    };

    /**
     * Disable mousedown event for 500ms during and after touch
     */
    var TouchHandler = {
        /* uses an integer rather than bool so there's no issues with
         * needing to clear timeouts if another touch event occurred
         * within the 500ms. Cannot mouseup between touchstart and
         * touchend, nor in the 500ms after touchend. */
        touches: 0,
        allowEvent: function (e) {
            var allow = true;

            if (e.type === 'touchstart') {
                TouchHandler.touches += 1; //push
            } else if (e.type === 'touchend' || e.type === 'touchcancel') {
                setTimeout(function () {
                    if (TouchHandler.touches > 0) {
                        TouchHandler.touches -= 1; //pop after 500ms
                    }
                }, 500);
            } else if (e.type === 'mousedown' && TouchHandler.touches > 0) {
                allow = false;
            }

            return allow;
        },
        touchup: function (e) {
            TouchHandler.allowEvent(e);
        }
    };

    /**
     * Delegated click handler for .waves-effect element.
     * returns null when .waves-effect element not in "click tree"
     */
    function getWavesEffectElement(e) {
        if (TouchHandler.allowEvent(e) === false) {
            return null;
        }

        var element = null;
        var target = e.target || e.srcElement;

        while (target.parentElement !== null) {
            if (!(target instanceof SVGElement) && target.className.indexOf('waves-effect') !== -1) {
                element = target;
                break;
            } else if (target.classList.contains('waves-effect')) {
                element = target;
                break;
            }
            target = target.parentElement;
        }

        return element;
    }

    /**
     * Bubble the click and show effect if .waves-effect elem was found
     */
    function showEffect(e) {
        var element = getWavesEffectElement(e);

        if (element !== null) {
            Effect.show(e, element);

            if ('ontouchstart' in window) {
                element.addEventListener('touchend', Effect.hide, false);
                element.addEventListener('touchcancel', Effect.hide, false);
            }

            element.addEventListener('mouseup', Effect.hide, false);
            element.addEventListener('mouseleave', Effect.hide, false);
        }
    }

    Waves.displayEffect = function (options) {
        options = options || {};

        if ('duration' in options) {
            Effect.duration = options.duration;
        }

        //Wrap input inside <i> tag
        Effect.wrapInput($$('.waves-effect'));

        if ('ontouchstart' in window) {
            document.body.addEventListener('touchstart', showEffect, false);
        }

        document.body.addEventListener('mousedown', showEffect, false);
    };

    /**
     * Attach Waves to an input element (or any element which doesn't
     * bubble mouseup/mousedown events).
     *   Intended to be used with dynamically loaded forms/inputs, or
     * where the user doesn't want a delegated click handler.
     */
    Waves.attach = function (element) {
        //FUTURE: automatically add waves classes and allow users
        // to specify them with an options param? Eg. light/classic/button
        if (element.tagName.toLowerCase() === 'input') {
            Effect.wrapInput([element]);
            element = element.parentElement;
        }

        if ('ontouchstart' in window) {
            element.addEventListener('touchstart', showEffect, false);
        }

        element.addEventListener('mousedown', showEffect, false);
    };

    window.Waves = Waves;

    document.addEventListener('DOMContentLoaded', function () {
        Waves.displayEffect();
    }, false);
})(window);
;"use strict";

!function () {
  "use strict";
  for (var t = document.querySelectorAll(".switch"), e = 0; e < t.length; ++e) {
    !function () {
      var i = t[e];t[e].addEventListener("click", function (t) {
        t.preventDefault(), i.classList.toggle("is-active"), i.classList.add("is-loading"), window.location = i.querySelector("a.not-active").getAttribute("href");
      });
    }();
  }
}();
;"use strict";

!function (t, e) {
  "object" == typeof exports && "undefined" != typeof module ? module.exports = e(require("jquery")) : "function" == typeof define && define.amd ? define(["jquery"], e) : t.parsley = e(t.jQuery);
}(this, function (h) {
  "use strict";
  function r(t) {
    return (r = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (t) {
      return typeof t;
    } : function (t) {
      return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
    })(t);
  }function o() {
    return (o = Object.assign || function (t) {
      for (var e = 1; e < arguments.length; e++) {
        var i = arguments[e];for (var n in i) {
          Object.prototype.hasOwnProperty.call(i, n) && (t[n] = i[n]);
        }
      }return t;
    }).apply(this, arguments);
  }function l(t, e) {
    return function (t) {
      if (Array.isArray(t)) return t;
    }(t) || function (t, e) {
      var i = [],
          n = !0,
          r = !1,
          s = void 0;try {
        for (var a, o = t[Symbol.iterator](); !(n = (a = o.next()).done) && (i.push(a.value), !e || i.length !== e); n = !0) {}
      } catch (t) {
        r = !0, s = t;
      } finally {
        try {
          n || null == o.return || o.return();
        } finally {
          if (r) throw s;
        }
      }return i;
    }(t, e) || function () {
      throw new TypeError("Invalid attempt to destructure non-iterable instance");
    }();
  }function u(t) {
    return function (t) {
      if (Array.isArray(t)) {
        for (var e = 0, i = new Array(t.length); e < t.length; e++) {
          i[e] = t[e];
        }return i;
      }
    }(t) || function (t) {
      if (Symbol.iterator in Object(t) || "[object Arguments]" === Object.prototype.toString.call(t)) return Array.from(t);
    }(t) || function () {
      throw new TypeError("Invalid attempt to spread non-iterable instance");
    }();
  }var i,
      t = 1,
      e = {},
      d = { attr: function (t, e, i) {
      var n,
          r,
          s,
          a = new RegExp("^" + e, "i");if (void 0 === i) i = {};else for (n in i) {
        i.hasOwnProperty(n) && delete i[n];
      }if (!t) return i;for (n = (s = t.attributes).length; n--;) {
        (r = s[n]) && r.specified && a.test(r.name) && (i[this.camelize(r.name.slice(e.length))] = this.deserializeValue(r.value));
      }return i;
    }, checkAttr: function (t, e, i) {
      return t.hasAttribute(e + i);
    }, setAttr: function (t, e, i, n) {
      t.setAttribute(this.dasherize(e + i), String(n));
    }, getType: function (t) {
      return t.getAttribute("type") || "text";
    }, generateID: function () {
      return "" + t++;
    }, deserializeValue: function (e) {
      var t;try {
        return e ? "true" == e || "false" != e && ("null" == e ? null : isNaN(t = Number(e)) ? /^[\[\{]/.test(e) ? JSON.parse(e) : e : t) : e;
      } catch (t) {
        return e;
      }
    }, camelize: function (t) {
      return t.replace(/-+(.)?/g, function (t, e) {
        return e ? e.toUpperCase() : "";
      });
    }, dasherize: function (t) {
      return t.replace(/::/g, "/").replace(/([A-Z]+)([A-Z][a-z])/g, "$1_$2").replace(/([a-z\d])([A-Z])/g, "$1_$2").replace(/_/g, "-").toLowerCase();
    }, warn: function () {
      var t;window.console && "function" == typeof window.console.warn && (t = window.console).warn.apply(t, arguments);
    }, warnOnce: function (t) {
      e[t] || (e[t] = !0, this.warn.apply(this, arguments));
    }, _resetWarnings: function () {
      e = {};
    }, trimString: function (t) {
      return t.replace(/^\s+|\s+$/g, "");
    }, parse: { date: function (t) {
        var e = t.match(/^(\d{4,})-(\d\d)-(\d\d)$/);if (!e) return null;var i = l(e.map(function (t) {
          return parseInt(t, 10);
        }), 4),
            n = (i[0], i[1]),
            r = i[2],
            s = i[3],
            a = new Date(n, r - 1, s);return a.getFullYear() !== n || a.getMonth() + 1 !== r || a.getDate() !== s ? null : a;
      }, string: function (t) {
        return t;
      }, integer: function (t) {
        return isNaN(t) ? null : parseInt(t, 10);
      }, number: function (t) {
        if (isNaN(t)) throw null;return parseFloat(t);
      }, boolean: function (t) {
        return !/^\s*false\s*$/i.test(t);
      }, object: function (t) {
        return d.deserializeValue(t);
      }, regexp: function (t) {
        var e = "";return t = /^\/.*\/(?:[gimy]*)$/.test(t) ? (e = t.replace(/.*\/([gimy]*)$/, "$1"), t.replace(new RegExp("^/(.*?)/" + e + "$"), "$1")) : "^" + t + "$", new RegExp(t, e);
      } }, parseRequirement: function (t, e) {
      var i = this.parse[t || "string"];if (!i) throw 'Unknown requirement specification: "' + t + '"';var n = i(e);if (null === n) throw "Requirement is not a ".concat(t, ': "').concat(e, '"');return n;
    }, namespaceEvents: function (t, e) {
      return (t = this.trimString(t || "").split(/\s+/))[0] ? h.map(t, function (t) {
        return "".concat(t, ".").concat(e);
      }).join(" ") : "";
    }, difference: function (t, i) {
      var n = [];return h.each(t, function (t, e) {
        -1 == i.indexOf(e) && n.push(e);
      }), n;
    }, all: function (t) {
      return h.when.apply(h, u(t).concat([42, 42]));
    }, objectCreate: Object.create || (i = function () {}, function (t) {
      if (1 < arguments.length) throw Error("Second argument not supported");if ("object" != r(t)) throw TypeError("Argument must be an object");i.prototype = t;var e = new i();return i.prototype = null, e;
    }), _SubmitSelector: 'input[type="submit"], button:submit' },
      n = { namespace: "data-parsley-", inputs: "input, textarea, select", excluded: "input[type=button], input[type=submit], input[type=reset], input[type=hidden]", priorityEnabled: !0, multiple: null, group: null, uiEnabled: !0, validationThreshold: 3, focus: "first", trigger: !1, triggerAfterFailure: "input", errorClass: "parsley-error", successClass: "parsley-success", classHandler: function (t) {}, errorsContainer: function (t) {}, errorsWrapper: '<ul class="parsley-errors-list"></ul>', errorTemplate: "<li></li>" },
      s = function () {
    this.__id__ = d.generateID();
  };s.prototype = { asyncSupport: !0, _pipeAccordingToValidationResult: function () {
      var e = this,
          t = function () {
        var t = h.Deferred();return !0 !== e.validationResult && t.reject(), t.resolve().promise();
      };return [t, t];
    }, actualizeOptions: function () {
      return d.attr(this.element, this.options.namespace, this.domOptions), this.parent && this.parent.actualizeOptions && this.parent.actualizeOptions(), this;
    }, _resetOptions: function (t) {
      for (var e in this.domOptions = d.objectCreate(this.parent.options), this.options = d.objectCreate(this.domOptions), t) {
        t.hasOwnProperty(e) && (this.options[e] = t[e]);
      }this.actualizeOptions();
    }, _listeners: null, on: function (t, e) {
      return this._listeners = this._listeners || {}, (this._listeners[t] = this._listeners[t] || []).push(e), this;
    }, subscribe: function (t, e) {
      h.listenTo(this, t.toLowerCase(), e);
    }, off: function (t, e) {
      var i = this._listeners && this._listeners[t];if (i) if (e) for (var n = i.length; n--;) {
        i[n] === e && i.splice(n, 1);
      } else delete this._listeners[t];return this;
    }, unsubscribe: function (t, e) {
      h.unsubscribeTo(this, t.toLowerCase());
    }, trigger: function (t, e, i) {
      e = e || this;var n,
          r = this._listeners && this._listeners[t];if (r) for (var s = r.length; s--;) {
        if (!1 === (n = r[s].call(e, e, i))) return n;
      }return !this.parent || this.parent.trigger(t, e, i);
    }, asyncIsValid: function (t, e) {
      return d.warnOnce("asyncIsValid is deprecated; please use whenValid instead"), this.whenValid({ group: t, force: e });
    }, _findRelated: function () {
      return this.options.multiple ? h(this.parent.element.querySelectorAll("[".concat(this.options.namespace, 'multiple="').concat(this.options.multiple, '"]'))) : this.$element;
    } };var c = function (t) {
    h.extend(!0, this, t);
  };c.prototype = { validate: function (t, e) {
      if (this.fn) return 3 < arguments.length && (e = [].slice.call(arguments, 1, -1)), this.fn(t, e);if (Array.isArray(t)) {
        if (!this.validateMultiple) throw "Validator `" + this.name + "` does not handle multiple values";return this.validateMultiple.apply(this, arguments);
      }var i = arguments[arguments.length - 1];if (this.validateDate && i._isDateInput()) return null !== (t = d.parse.date(t)) && this.validateDate.apply(this, arguments);if (this.validateNumber) return !isNaN(t) && (t = parseFloat(t), this.validateNumber.apply(this, arguments));if (this.validateString) return this.validateString.apply(this, arguments);throw "Validator `" + this.name + "` only handles multiple values";
    }, parseRequirements: function (t, e) {
      if ("string" != typeof t) return Array.isArray(t) ? t : [t];var i = this.requirementType;if (Array.isArray(i)) {
        for (var n = function (t, e) {
          var i = t.match(/^\s*\[(.*)\]\s*$/);if (!i) throw 'Requirement is not an array: "' + t + '"';var n = i[1].split(",").map(d.trimString);if (n.length !== e) throw "Requirement has " + n.length + " values when " + e + " are needed";return n;
        }(t, i.length), r = 0; r < n.length; r++) {
          n[r] = d.parseRequirement(i[r], n[r]);
        }return n;
      }return h.isPlainObject(i) ? function (t, e, i) {
        var n = null,
            r = {};for (var s in t) {
          if (s) {
            var a = i(s);"string" == typeof a && (a = d.parseRequirement(t[s], a)), r[s] = a;
          } else n = d.parseRequirement(t[s], e);
        }return [n, r];
      }(i, t, e) : [d.parseRequirement(i, t)];
    }, requirementType: "string", priority: 2 };var a = function (t, e) {
    this.__class__ = "ValidatorRegistry", this.locale = "en", this.init(t || {}, e || {});
  },
      p = { email: /^((([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/, number: /^-?(\d*\.)?\d+(e[-+]?\d+)?$/i, integer: /^-?\d+$/, digits: /^\d+$/, alphanum: /^\w+$/i, date: { test: function (t) {
        return null !== d.parse.date(t);
      } }, url: new RegExp("^(?:(?:https?|ftp)://)?(?:\\S+(?::\\S*)?@)?(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-zA-Z\\u00a1-\\uffff0-9]-*)*[a-zA-Z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-zA-Z\\u00a1-\\uffff0-9]-*)*[a-zA-Z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-zA-Z\\u00a1-\\uffff]{2,})))(?::\\d{2,5})?(?:/\\S*)?$") };p.range = p.number;var f = function (t) {
    var e = ("" + t).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);return e ? Math.max(0, (e[1] ? e[1].length : 0) - (e[2] ? +e[2] : 0)) : 0;
  },
      m = function (s, a) {
    return function (t) {
      for (var e = arguments.length, i = new Array(1 < e ? e - 1 : 0), n = 1; n < e; n++) {
        i[n - 1] = arguments[n];
      }return i.pop(), !t || a.apply(void 0, [t].concat(u((r = s, i.map(d.parse[r])))));var r;
    };
  },
      g = function (t) {
    return { validateDate: m("date", t), validateNumber: m("number", t), requirementType: t.length <= 2 ? "string" : ["string", "string"], priority: 30 };
  };a.prototype = { init: function (t, e) {
      for (var i in this.catalog = e, this.validators = o({}, this.validators), t) {
        this.addValidator(i, t[i].fn, t[i].priority);
      }window.Parsley.trigger("parsley:validator:init");
    }, setLocale: function (t) {
      if (void 0 === this.catalog[t]) throw new Error(t + " is not available in the catalog");return this.locale = t, this;
    }, addCatalog: function (t, e, i) {
      return "object" === r(e) && (this.catalog[t] = e), !0 === i ? this.setLocale(t) : this;
    }, addMessage: function (t, e, i) {
      return void 0 === this.catalog[t] && (this.catalog[t] = {}), this.catalog[t][e] = i, this;
    }, addMessages: function (t, e) {
      for (var i in e) {
        this.addMessage(t, i, e[i]);
      }return this;
    }, addValidator: function (t, e, i) {
      if (this.validators[t]) d.warn('Validator "' + t + '" is already defined.');else if (n.hasOwnProperty(t)) return void d.warn('"' + t + '" is a restricted keyword and is not a valid validator name.');return this._setValidator.apply(this, arguments);
    }, hasValidator: function (t) {
      return !!this.validators[t];
    }, updateValidator: function (t, e, i) {
      return this.validators[t] ? this._setValidator.apply(this, arguments) : (d.warn('Validator "' + t + '" is not already defined.'), this.addValidator.apply(this, arguments));
    }, removeValidator: function (t) {
      return this.validators[t] || d.warn('Validator "' + t + '" is not defined.'), delete this.validators[t], this;
    }, _setValidator: function (t, e, i) {
      for (var n in "object" !== r(e) && (e = { fn: e, priority: i }), e.validate || (e = new c(e)), (this.validators[t] = e).messages || {}) {
        this.addMessage(n, t, e.messages[n]);
      }return this;
    }, getErrorMessage: function (t) {
      var e;"type" === t.name ? e = (this.catalog[this.locale][t.name] || {})[t.requirements] : e = this.formatMessage(this.catalog[this.locale][t.name], t.requirements);return e || this.catalog[this.locale].defaultMessage || this.catalog.en.defaultMessage;
    }, formatMessage: function (t, e) {
      if ("object" !== r(e)) return "string" == typeof t ? t.replace(/%s/i, e) : "";for (var i in e) {
        t = this.formatMessage(t, e[i]);
      }return t;
    }, validators: { notblank: { validateString: function (t) {
          return (/\S/.test(t)
          );
        }, priority: 2 }, required: { validateMultiple: function (t) {
          return 0 < t.length;
        }, validateString: function (t) {
          return (/\S/.test(t)
          );
        }, priority: 512 }, type: { validateString: function (t, e) {
          var i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {},
              n = i.step,
              r = void 0 === n ? "any" : n,
              s = i.base,
              a = void 0 === s ? 0 : s,
              o = p[e];if (!o) throw new Error("validator type `" + e + "` is not supported");if (!t) return !0;if (!o.test(t)) return !1;if ("number" === e && !/^any$/i.test(r || "")) {
            var l = Number(t),
                u = Math.max(f(r), f(a));if (f(l) > u) return !1;var d = function (t) {
              return Math.round(t * Math.pow(10, u));
            };if ((d(l) - d(a)) % d(r) != 0) return !1;
          }return !0;
        }, requirementType: { "": "string", step: "string", base: "number" }, priority: 256 }, pattern: { validateString: function (t, e) {
          return !t || e.test(t);
        }, requirementType: "regexp", priority: 64 }, minlength: { validateString: function (t, e) {
          return !t || t.length >= e;
        }, requirementType: "integer", priority: 30 }, maxlength: { validateString: function (t, e) {
          return t.length <= e;
        }, requirementType: "integer", priority: 30 }, length: { validateString: function (t, e, i) {
          return !t || t.length >= e && t.length <= i;
        }, requirementType: ["integer", "integer"], priority: 30 }, mincheck: { validateMultiple: function (t, e) {
          return t.length >= e;
        }, requirementType: "integer", priority: 30 }, maxcheck: { validateMultiple: function (t, e) {
          return t.length <= e;
        }, requirementType: "integer", priority: 30 }, check: { validateMultiple: function (t, e, i) {
          return t.length >= e && t.length <= i;
        }, requirementType: ["integer", "integer"], priority: 30 }, min: g(function (t, e) {
        return e <= t;
      }), max: g(function (t, e) {
        return t <= e;
      }), range: g(function (t, e, i) {
        return e <= t && t <= i;
      }), equalto: { validateString: function (t, e) {
          if (!t) return !0;var i = h(e);return i.length ? t === i.val() : t === e;
        }, priority: 256 }, euvatin: { validateString: function (t, e) {
          if (!t) return !0;return (/^[A-Z][A-Z][A-Za-z0-9 -]{2,}$/.test(t)
          );
        }, priority: 30 } } };var v = {};v.Form = { _actualizeTriggers: function () {
      var e = this;this.$element.on("submit.Parsley", function (t) {
        e.onSubmitValidate(t);
      }), this.$element.on("click.Parsley", d._SubmitSelector, function (t) {
        e.onSubmitButton(t);
      }), !1 !== this.options.uiEnabled && this.element.setAttribute("novalidate", "");
    }, focus: function () {
      if (!(this._focusedField = null) === this.validationResult || "none" === this.options.focus) return null;for (var t = 0; t < this.fields.length; t++) {
        var e = this.fields[t];if (!0 !== e.validationResult && 0 < e.validationResult.length && void 0 === e.options.noFocus && (this._focusedField = e.$element, "first" === this.options.focus)) break;
      }return null === this._focusedField ? null : this._focusedField.focus();
    }, _destroyUI: function () {
      this.$element.off(".Parsley");
    } }, v.Field = { _reflowUI: function () {
      if (this._buildUI(), this._ui) {
        var t = function t(e, i, n) {
          for (var r = [], s = [], a = 0; a < e.length; a++) {
            for (var o = !1, l = 0; l < i.length; l++) {
              if (e[a].assert.name === i[l].assert.name) {
                o = !0;break;
              }
            }o ? s.push(e[a]) : r.push(e[a]);
          }return { kept: s, added: r, removed: n ? [] : t(i, e, !0).added };
        }(this.validationResult, this._ui.lastValidationResult);this._ui.lastValidationResult = this.validationResult, this._manageStatusClass(), this._manageErrorsMessages(t), this._actualizeTriggers(), !t.kept.length && !t.added.length || this._failedOnce || (this._failedOnce = !0, this._actualizeTriggers());
      }
    }, getErrorsMessages: function () {
      if (!0 === this.validationResult) return [];for (var t = [], e = 0; e < this.validationResult.length; e++) {
        t.push(this.validationResult[e].errorMessage || this._getErrorMessage(this.validationResult[e].assert));
      }return t;
    }, addError: function (t) {
      var e = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {},
          i = e.message,
          n = e.assert,
          r = e.updateClass,
          s = void 0 === r || r;this._buildUI(), this._addError(t, { message: i, assert: n }), s && this._errorClass();
    }, updateError: function (t) {
      var e = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {},
          i = e.message,
          n = e.assert,
          r = e.updateClass,
          s = void 0 === r || r;this._buildUI(), this._updateError(t, { message: i, assert: n }), s && this._errorClass();
    }, removeError: function (t) {
      var e = (1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {}).updateClass,
          i = void 0 === e || e;this._buildUI(), this._removeError(t), i && this._manageStatusClass();
    }, _manageStatusClass: function () {
      this.hasConstraints() && this.needsValidation() && !0 === this.validationResult ? this._successClass() : 0 < this.validationResult.length ? this._errorClass() : this._resetClass();
    }, _manageErrorsMessages: function (t) {
      if (void 0 === this.options.errorsMessagesDisabled) {
        if (void 0 !== this.options.errorMessage) return t.added.length || t.kept.length ? (this._insertErrorWrapper(), 0 === this._ui.$errorsWrapper.find(".parsley-custom-error-message").length && this._ui.$errorsWrapper.append(h(this.options.errorTemplate).addClass("parsley-custom-error-message")), this._ui.$errorsWrapper.addClass("filled").find(".parsley-custom-error-message").html(this.options.errorMessage)) : this._ui.$errorsWrapper.removeClass("filled").find(".parsley-custom-error-message").remove();for (var e = 0; e < t.removed.length; e++) {
          this._removeError(t.removed[e].assert.name);
        }for (e = 0; e < t.added.length; e++) {
          this._addError(t.added[e].assert.name, { message: t.added[e].errorMessage, assert: t.added[e].assert });
        }for (e = 0; e < t.kept.length; e++) {
          this._updateError(t.kept[e].assert.name, { message: t.kept[e].errorMessage, assert: t.kept[e].assert });
        }
      }
    }, _addError: function (t, e) {
      var i = e.message,
          n = e.assert;this._insertErrorWrapper(), this._ui.$errorClassHandler.attr("aria-describedby", this._ui.errorsWrapperId), this._ui.$errorsWrapper.addClass("filled").append(h(this.options.errorTemplate).addClass("parsley-" + t).html(i || this._getErrorMessage(n)));
    }, _updateError: function (t, e) {
      var i = e.message,
          n = e.assert;this._ui.$errorsWrapper.addClass("filled").find(".parsley-" + t).html(i || this._getErrorMessage(n));
    }, _removeError: function (t) {
      this._ui.$errorClassHandler.removeAttr("aria-describedby"), this._ui.$errorsWrapper.removeClass("filled").find(".parsley-" + t).remove();
    }, _getErrorMessage: function (t) {
      var e = t.name + "Message";return void 0 !== this.options[e] ? window.Parsley.formatMessage(this.options[e], t.requirements) : window.Parsley.getErrorMessage(t);
    }, _buildUI: function () {
      if (!this._ui && !1 !== this.options.uiEnabled) {
        var t = {};this.element.setAttribute(this.options.namespace + "id", this.__id__), t.$errorClassHandler = this._manageClassHandler(), t.errorsWrapperId = "parsley-id-" + (this.options.multiple ? "multiple-" + this.options.multiple : this.__id__), t.$errorsWrapper = h(this.options.errorsWrapper).attr("id", t.errorsWrapperId), t.lastValidationResult = [], t.validationInformationVisible = !1, this._ui = t;
      }
    }, _manageClassHandler: function () {
      if ("string" == typeof this.options.classHandler && h(this.options.classHandler).length) return h(this.options.classHandler);var t = this.options.classHandler;if ("string" == typeof this.options.classHandler && "function" == typeof window[this.options.classHandler] && (t = window[this.options.classHandler]), "function" == typeof t) {
        var e = t.call(this, this);if (void 0 !== e && e.length) return e;
      } else {
        if ("object" === r(t) && t instanceof jQuery && t.length) return t;t && d.warn("The class handler `" + t + "` does not exist in DOM nor as a global JS function");
      }return this._inputHolder();
    }, _inputHolder: function () {
      return this.options.multiple && "SELECT" !== this.element.nodeName ? this.$element.parent() : this.$element;
    }, _insertErrorWrapper: function () {
      var t = this.options.errorsContainer;if (0 !== this._ui.$errorsWrapper.parent().length) return this._ui.$errorsWrapper.parent();if ("string" == typeof t) {
        if (h(t).length) return h(t).append(this._ui.$errorsWrapper);"function" == typeof window[t] ? t = window[t] : d.warn("The errors container `" + t + "` does not exist in DOM nor as a global JS function");
      }return "function" == typeof t && (t = t.call(this, this)), "object" === r(t) && t.length ? t.append(this._ui.$errorsWrapper) : this._inputHolder().after(this._ui.$errorsWrapper);
    }, _actualizeTriggers: function () {
      var t,
          e = this,
          i = this._findRelated();i.off(".Parsley"), this._failedOnce ? i.on(d.namespaceEvents(this.options.triggerAfterFailure, "Parsley"), function () {
        e._validateIfNeeded();
      }) : (t = d.namespaceEvents(this.options.trigger, "Parsley")) && i.on(t, function (t) {
        e._validateIfNeeded(t);
      });
    }, _validateIfNeeded: function (t) {
      var e = this;t && /key|input/.test(t.type) && (!this._ui || !this._ui.validationInformationVisible) && this.getValue().length <= this.options.validationThreshold || (this.options.debounce ? (window.clearTimeout(this._debounced), this._debounced = window.setTimeout(function () {
        return e.validate();
      }, this.options.debounce)) : this.validate());
    }, _resetUI: function () {
      this._failedOnce = !1, this._actualizeTriggers(), void 0 !== this._ui && (this._ui.$errorsWrapper.removeClass("filled").children().remove(), this._resetClass(), this._ui.lastValidationResult = [], this._ui.validationInformationVisible = !1);
    }, _destroyUI: function () {
      this._resetUI(), void 0 !== this._ui && this._ui.$errorsWrapper.remove(), delete this._ui;
    }, _successClass: function () {
      this._ui.validationInformationVisible = !0, this._ui.$errorClassHandler.removeClass(this.options.errorClass).addClass(this.options.successClass);
    }, _errorClass: function () {
      this._ui.validationInformationVisible = !0, this._ui.$errorClassHandler.removeClass(this.options.successClass).addClass(this.options.errorClass);
    }, _resetClass: function () {
      this._ui.$errorClassHandler.removeClass(this.options.successClass).removeClass(this.options.errorClass);
    } };var y = function (t, e, i) {
    this.__class__ = "Form", this.element = t, this.$element = h(t), this.domOptions = e, this.options = i, this.parent = window.Parsley, this.fields = [], this.validationResult = null;
  },
      _ = { pending: null, resolved: !0, rejected: !1 };y.prototype = { onSubmitValidate: function (t) {
      var e = this;if (!0 !== t.parsley) {
        var i = this._submitSource || this.$element.find(d._SubmitSelector)[0];if (this._submitSource = null, this.$element.find(".parsley-synthetic-submit-button").prop("disabled", !0), !i || null === i.getAttribute("formnovalidate")) {
          window.Parsley._remoteCache = {};var n = this.whenValidate({ event: t });"resolved" === n.state() && !1 !== this._trigger("submit") || (t.stopImmediatePropagation(), t.preventDefault(), "pending" === n.state() && n.done(function () {
            e._submit(i);
          }));
        }
      }
    }, onSubmitButton: function (t) {
      this._submitSource = t.currentTarget;
    }, _submit: function (t) {
      if (!1 !== this._trigger("submit")) {
        if (t) {
          var e = this.$element.find(".parsley-synthetic-submit-button").prop("disabled", !1);0 === e.length && (e = h('<input class="parsley-synthetic-submit-button" type="hidden">').appendTo(this.$element)), e.attr({ name: t.getAttribute("name"), value: t.getAttribute("value") });
        }this.$element.trigger(o(h.Event("submit"), { parsley: !0 }));
      }
    }, validate: function (t) {
      if (1 <= arguments.length && !h.isPlainObject(t)) {
        d.warnOnce("Calling validate on a parsley form without passing arguments as an object is deprecated.");var e = Array.prototype.slice.call(arguments);t = { group: e[0], force: e[1], event: e[2] };
      }return _[this.whenValidate(t).state()];
    }, whenValidate: function () {
      var t,
          e = this,
          i = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {},
          n = i.group,
          r = i.force,
          s = i.event;(this.submitEvent = s) && (this.submitEvent = o({}, s, { preventDefault: function () {
          d.warnOnce("Using `this.submitEvent.preventDefault()` is deprecated; instead, call `this.validationResult = false`"), e.validationResult = !1;
        } })), this.validationResult = !0, this._trigger("validate"), this._refreshFields();var a = this._withoutReactualizingFormOptions(function () {
        return h.map(e.fields, function (t) {
          return t.whenValidate({ force: r, group: n });
        });
      });return (t = d.all(a).done(function () {
        e._trigger("success");
      }).fail(function () {
        e.validationResult = !1, e.focus(), e._trigger("error");
      }).always(function () {
        e._trigger("validated");
      })).pipe.apply(t, u(this._pipeAccordingToValidationResult()));
    }, isValid: function (t) {
      if (1 <= arguments.length && !h.isPlainObject(t)) {
        d.warnOnce("Calling isValid on a parsley form without passing arguments as an object is deprecated.");var e = Array.prototype.slice.call(arguments);t = { group: e[0], force: e[1] };
      }return _[this.whenValid(t).state()];
    }, whenValid: function () {
      var t = this,
          e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {},
          i = e.group,
          n = e.force;this._refreshFields();var r = this._withoutReactualizingFormOptions(function () {
        return h.map(t.fields, function (t) {
          return t.whenValid({ group: i, force: n });
        });
      });return d.all(r);
    }, refresh: function () {
      return this._refreshFields(), this;
    }, reset: function () {
      for (var t = 0; t < this.fields.length; t++) {
        this.fields[t].reset();
      }this._trigger("reset");
    }, destroy: function () {
      this._destroyUI();for (var t = 0; t < this.fields.length; t++) {
        this.fields[t].destroy();
      }this.$element.removeData("Parsley"), this._trigger("destroy");
    }, _refreshFields: function () {
      return this.actualizeOptions()._bindFields();
    }, _bindFields: function () {
      var r = this,
          t = this.fields;return this.fields = [], this.fieldsMappedById = {}, this._withoutReactualizingFormOptions(function () {
        r.$element.find(r.options.inputs).not(r.options.excluded).not("[".concat(r.options.namespace, "excluded=true]")).each(function (t, e) {
          var i = new window.Parsley.Factory(e, {}, r);if ("Field" === i.__class__ || "FieldMultiple" === i.__class__) {
            var n = i.__class__ + "-" + i.__id__;void 0 === r.fieldsMappedById[n] && (r.fieldsMappedById[n] = i, r.fields.push(i));
          }
        }), h.each(d.difference(t, r.fields), function (t, e) {
          e.reset();
        });
      }), this;
    }, _withoutReactualizingFormOptions: function (t) {
      var e = this.actualizeOptions;this.actualizeOptions = function () {
        return this;
      };var i = t();return this.actualizeOptions = e, i;
    }, _trigger: function (t) {
      return this.trigger("form:" + t);
    } };var w = function (t, e, i, n, r) {
    var s = window.Parsley._validatorRegistry.validators[e],
        a = new c(s);o(this, { validator: a, name: e, requirements: i, priority: n = n || t.options[e + "Priority"] || a.priority, isDomConstraint: r = !0 === r }), this._parseRequirements(t.options);
  },
      b = function (t, e, i, n) {
    this.__class__ = "Field", this.element = t, this.$element = h(t), void 0 !== n && (this.parent = n), this.options = i, this.domOptions = e, this.constraints = [], this.constraintsByName = {}, this.validationResult = !0, this._bindConstraints();
  },
      F = { pending: null, resolved: !0, rejected: !(w.prototype = { validate: function (t, e) {
        var i;return (i = this.validator).validate.apply(i, [t].concat(u(this.requirementList), [e]));
      }, _parseRequirements: function (i) {
        var n = this;this.requirementList = this.validator.parseRequirements(this.requirements, function (t) {
          return i[n.name + (e = t, e[0].toUpperCase() + e.slice(1))];var e;
        });
      } }) };b.prototype = { validate: function (t) {
      1 <= arguments.length && !h.isPlainObject(t) && (d.warnOnce("Calling validate on a parsley field without passing arguments as an object is deprecated."), t = { options: t });var e = this.whenValidate(t);if (!e) return !0;switch (e.state()) {case "pending":
          return null;case "resolved":
          return !0;case "rejected":
          return this.validationResult;}
    }, whenValidate: function () {
      var t,
          e = this,
          i = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {},
          n = i.force,
          r = i.group;if (this.refresh(), !r || this._isInGroup(r)) return this.value = this.getValue(), this._trigger("validate"), (t = this.whenValid({ force: n, value: this.value, _refreshed: !0 }).always(function () {
        e._reflowUI();
      }).done(function () {
        e._trigger("success");
      }).fail(function () {
        e._trigger("error");
      }).always(function () {
        e._trigger("validated");
      })).pipe.apply(t, u(this._pipeAccordingToValidationResult()));
    }, hasConstraints: function () {
      return 0 !== this.constraints.length;
    }, needsValidation: function (t) {
      return void 0 === t && (t = this.getValue()), !(!t.length && !this._isRequired() && void 0 === this.options.validateIfEmpty);
    }, _isInGroup: function (t) {
      return Array.isArray(this.options.group) ? -1 !== h.inArray(t, this.options.group) : this.options.group === t;
    }, isValid: function (t) {
      if (1 <= arguments.length && !h.isPlainObject(t)) {
        d.warnOnce("Calling isValid on a parsley field without passing arguments as an object is deprecated.");var e = Array.prototype.slice.call(arguments);t = { force: e[0], value: e[1] };
      }var i = this.whenValid(t);return !i || F[i.state()];
    }, whenValid: function () {
      var n = this,
          t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {},
          e = t.force,
          i = void 0 !== e && e,
          r = t.value,
          s = t.group;if (t._refreshed || this.refresh(), !s || this._isInGroup(s)) {
        if (this.validationResult = !0, !this.hasConstraints()) return h.when();if (null == r && (r = this.getValue()), !this.needsValidation(r) && !0 !== i) return h.when();var a = this._getGroupedConstraints(),
            o = [];return h.each(a, function (t, e) {
          var i = d.all(h.map(e, function (t) {
            return n._validateConstraint(r, t);
          }));if (o.push(i), "rejected" === i.state()) return !1;
        }), d.all(o);
      }
    }, _validateConstraint: function (t, e) {
      var i = this,
          n = e.validate(t, this);return !1 === n && (n = h.Deferred().reject()), d.all([n]).fail(function (t) {
        i.validationResult instanceof Array || (i.validationResult = []), i.validationResult.push({ assert: e, errorMessage: "string" == typeof t && t });
      });
    }, getValue: function () {
      var t;return null == (t = "function" == typeof this.options.value ? this.options.value(this) : void 0 !== this.options.value ? this.options.value : this.$element.val()) ? "" : this._handleWhitespace(t);
    }, reset: function () {
      return this._resetUI(), this._trigger("reset");
    }, destroy: function () {
      this._destroyUI(), this.$element.removeData("Parsley"), this.$element.removeData("FieldMultiple"), this._trigger("destroy");
    }, refresh: function () {
      return this._refreshConstraints(), this;
    }, _refreshConstraints: function () {
      return this.actualizeOptions()._bindConstraints();
    }, refreshConstraints: function () {
      return d.warnOnce("Parsley's refreshConstraints is deprecated. Please use refresh"), this.refresh();
    }, addConstraint: function (t, e, i, n) {
      if (window.Parsley._validatorRegistry.validators[t]) {
        var r = new w(this, t, e, i, n);"undefined" !== this.constraintsByName[r.name] && this.removeConstraint(r.name), this.constraints.push(r), this.constraintsByName[r.name] = r;
      }return this;
    }, removeConstraint: function (t) {
      for (var e = 0; e < this.constraints.length; e++) {
        if (t === this.constraints[e].name) {
          this.constraints.splice(e, 1);break;
        }
      }return delete this.constraintsByName[t], this;
    }, updateConstraint: function (t, e, i) {
      return this.removeConstraint(t).addConstraint(t, e, i);
    }, _bindConstraints: function () {
      for (var t = [], e = {}, i = 0; i < this.constraints.length; i++) {
        !1 === this.constraints[i].isDomConstraint && (t.push(this.constraints[i]), e[this.constraints[i].name] = this.constraints[i]);
      }for (var n in this.constraints = t, this.constraintsByName = e, this.options) {
        this.addConstraint(n, this.options[n], void 0, !0);
      }return this._bindHtml5Constraints();
    }, _bindHtml5Constraints: function () {
      null !== this.element.getAttribute("required") && this.addConstraint("required", !0, void 0, !0), null !== this.element.getAttribute("pattern") && this.addConstraint("pattern", this.element.getAttribute("pattern"), void 0, !0);var t = this.element.getAttribute("min"),
          e = this.element.getAttribute("max");null !== t && null !== e ? this.addConstraint("range", [t, e], void 0, !0) : null !== t ? this.addConstraint("min", t, void 0, !0) : null !== e && this.addConstraint("max", e, void 0, !0), null !== this.element.getAttribute("minlength") && null !== this.element.getAttribute("maxlength") ? this.addConstraint("length", [this.element.getAttribute("minlength"), this.element.getAttribute("maxlength")], void 0, !0) : null !== this.element.getAttribute("minlength") ? this.addConstraint("minlength", this.element.getAttribute("minlength"), void 0, !0) : null !== this.element.getAttribute("maxlength") && this.addConstraint("maxlength", this.element.getAttribute("maxlength"), void 0, !0);var i = d.getType(this.element);return "number" === i ? this.addConstraint("type", ["number", { step: this.element.getAttribute("step") || "1", base: t || this.element.getAttribute("value") }], void 0, !0) : /^(email|url|range|date)$/i.test(i) ? this.addConstraint("type", i, void 0, !0) : this;
    }, _isRequired: function () {
      return void 0 !== this.constraintsByName.required && !1 !== this.constraintsByName.required.requirements;
    }, _trigger: function (t) {
      return this.trigger("field:" + t);
    }, _handleWhitespace: function (t) {
      return !0 === this.options.trimValue && d.warnOnce('data-parsley-trim-value="true" is deprecated, please use data-parsley-whitespace="trim"'), "squish" === this.options.whitespace && (t = t.replace(/\s{2,}/g, " ")), "trim" !== this.options.whitespace && "squish" !== this.options.whitespace && !0 !== this.options.trimValue || (t = d.trimString(t)), t;
    }, _isDateInput: function () {
      var t = this.constraintsByName.type;return t && "date" === t.requirements;
    }, _getGroupedConstraints: function () {
      if (!1 === this.options.priorityEnabled) return [this.constraints];for (var t = [], e = {}, i = 0; i < this.constraints.length; i++) {
        var n = this.constraints[i].priority;e[n] || t.push(e[n] = []), e[n].push(this.constraints[i]);
      }return t.sort(function (t, e) {
        return e[0].priority - t[0].priority;
      }), t;
    } };var C = function () {
    this.__class__ = "FieldMultiple";
  };C.prototype = { addElement: function (t) {
      return this.$elements.push(t), this;
    }, _refreshConstraints: function () {
      var t;if (this.constraints = [], "SELECT" === this.element.nodeName) return this.actualizeOptions()._bindConstraints(), this;for (var e = 0; e < this.$elements.length; e++) {
        if (h("html").has(this.$elements[e]).length) {
          t = this.$elements[e].data("FieldMultiple")._refreshConstraints().constraints;for (var i = 0; i < t.length; i++) {
            this.addConstraint(t[i].name, t[i].requirements, t[i].priority, t[i].isDomConstraint);
          }
        } else this.$elements.splice(e, 1);
      }return this;
    }, getValue: function () {
      if ("function" == typeof this.options.value) return this.options.value(this);if (void 0 !== this.options.value) return this.options.value;if ("INPUT" === this.element.nodeName) {
        var t = d.getType(this.element);if ("radio" === t) return this._findRelated().filter(":checked").val() || "";if ("checkbox" === t) {
          var e = [];return this._findRelated().filter(":checked").each(function () {
            e.push(h(this).val());
          }), e;
        }
      }return "SELECT" === this.element.nodeName && null === this.$element.val() ? [] : this.$element.val();
    }, _init: function () {
      return this.$elements = [this.$element], this;
    } };var A = function (t, e, i) {
    this.element = t, this.$element = h(t);var n = this.$element.data("Parsley");if (n) return void 0 !== i && n.parent === window.Parsley && (n.parent = i, n._resetOptions(n.options)), "object" === r(e) && o(n.options, e), n;if (!this.$element.length) throw new Error("You must bind Parsley on an existing element.");if (void 0 !== i && "Form" !== i.__class__) throw new Error("Parent instance must be a Form instance");return this.parent = i || window.Parsley, this.init(e);
  };A.prototype = { init: function (t) {
      return this.__class__ = "Parsley", this.__version__ = "2.9.0", this.__id__ = d.generateID(), this._resetOptions(t), "FORM" === this.element.nodeName || d.checkAttr(this.element, this.options.namespace, "validate") && !this.$element.is(this.options.inputs) ? this.bind("parsleyForm") : this.isMultiple() ? this.handleMultiple() : this.bind("parsleyField");
    }, isMultiple: function () {
      var t = d.getType(this.element);return "radio" === t || "checkbox" === t || "SELECT" === this.element.nodeName && null !== this.element.getAttribute("multiple");
    }, handleMultiple: function () {
      var t,
          e,
          n = this;if (this.options.multiple = this.options.multiple || (t = this.element.getAttribute("name")) || this.element.getAttribute("id"), "SELECT" === this.element.nodeName && null !== this.element.getAttribute("multiple")) return this.options.multiple = this.options.multiple || this.__id__, this.bind("parsleyFieldMultiple");if (!this.options.multiple) return d.warn("To be bound by Parsley, a radio, a checkbox and a multiple select input must have either a name or a multiple option.", this.$element), this;this.options.multiple = this.options.multiple.replace(/(:|\.|\[|\]|\{|\}|\$)/g, ""), t && h('input[name="' + t + '"]').each(function (t, e) {
        var i = d.getType(e);"radio" !== i && "checkbox" !== i || e.setAttribute(n.options.namespace + "multiple", n.options.multiple);
      });for (var i = this._findRelated(), r = 0; r < i.length; r++) {
        if (void 0 !== (e = h(i.get(r)).data("Parsley"))) {
          this.$element.data("FieldMultiple") || e.addElement(this.$element);break;
        }
      }return this.bind("parsleyField", !0), e || this.bind("parsleyFieldMultiple");
    }, bind: function (t, e) {
      var i;switch (t) {case "parsleyForm":
          i = h.extend(new y(this.element, this.domOptions, this.options), new s(), window.ParsleyExtend)._bindFields();break;case "parsleyField":
          i = h.extend(new b(this.element, this.domOptions, this.options, this.parent), new s(), window.ParsleyExtend);break;case "parsleyFieldMultiple":
          i = h.extend(new b(this.element, this.domOptions, this.options, this.parent), new C(), new s(), window.ParsleyExtend)._init();break;default:
          throw new Error(t + "is not a supported Parsley type");}return this.options.multiple && d.setAttr(this.element, this.options.namespace, "multiple", this.options.multiple), void 0 !== e ? this.$element.data("FieldMultiple", i) : (this.$element.data("Parsley", i), i._actualizeTriggers(), i._trigger("init")), i;
    } };var E = h.fn.jquery.split(".");if (parseInt(E[0]) <= 1 && parseInt(E[1]) < 8) throw "The loaded version of jQuery is too old. Please upgrade to 1.8.x or better.";E.forEach || d.warn("Parsley requires ES5 to run properly. Please include https://github.com/es-shims/es5-shim");var x = o(new s(), { element: document, $element: h(document), actualizeOptions: null, _resetOptions: null, Factory: A, version: "2.9.0" });o(b.prototype, v.Field, s.prototype), o(y.prototype, v.Form, s.prototype), o(A.prototype, s.prototype), h.fn.parsley = h.fn.psly = function (t) {
    if (1 < this.length) {
      var e = [];return this.each(function () {
        e.push(h(this).parsley(t));
      }), e;
    }if (0 != this.length) return new A(this[0], t);
  }, void 0 === window.ParsleyExtend && (window.ParsleyExtend = {}), x.options = o(d.objectCreate(n), window.ParsleyConfig), window.ParsleyConfig = x.options, window.Parsley = window.psly = x, x.Utils = d, window.ParsleyUtils = {}, h.each(d, function (t, e) {
    "function" == typeof e && (window.ParsleyUtils[t] = function () {
      return d.warnOnce("Accessing `window.ParsleyUtils` is deprecated. Use `window.Parsley.Utils` instead."), d[t].apply(d, arguments);
    });
  });var $ = window.Parsley._validatorRegistry = new a(window.ParsleyConfig.validators, window.ParsleyConfig.i18n);window.ParsleyValidator = {}, h.each("setLocale addCatalog addMessage addMessages getErrorMessage formatMessage addValidator updateValidator removeValidator hasValidator".split(" "), function (t, e) {
    window.Parsley[e] = function () {
      return $[e].apply($, arguments);
    }, window.ParsleyValidator[e] = function () {
      var t;return d.warnOnce("Accessing the method '".concat(e, "' through Validator is deprecated. Simply call 'window.Parsley.").concat(e, "(...)'")), (t = window.Parsley)[e].apply(t, arguments);
    };
  }), window.Parsley.UI = v, window.ParsleyUI = { removeError: function (t, e, i) {
      var n = !0 !== i;return d.warnOnce("Accessing UI is deprecated. Call 'removeError' on the instance directly. Please comment in issue 1073 as to your need to call this method."), t.removeError(e, { updateClass: n });
    }, getErrorsMessages: function (t) {
      return d.warnOnce("Accessing UI is deprecated. Call 'getErrorsMessages' on the instance directly."), t.getErrorsMessages();
    } }, h.each("addError updateError".split(" "), function (t, a) {
    window.ParsleyUI[a] = function (t, e, i, n, r) {
      var s = !0 !== r;return d.warnOnce("Accessing UI is deprecated. Call '".concat(a, "' on the instance directly. Please comment in issue 1073 as to your need to call this method.")), t[a](e, { message: i, assert: n, updateClass: s });
    };
  }), !1 !== window.ParsleyConfig.autoBind && h(function () {
    h("[data-parsley-validate]").length && h("[data-parsley-validate]").parsley();
  });var V = h({}),
      P = function () {
    d.warnOnce("Parsley's pubsub module is deprecated; use the 'on' and 'off' methods on parsley instances or window.Parsley");
  };function O(e, i) {
    return e.parsleyAdaptedCallback || (e.parsleyAdaptedCallback = function () {
      var t = Array.prototype.slice.call(arguments, 0);t.unshift(this), e.apply(i || V, t);
    }), e.parsleyAdaptedCallback;
  }var T = "parsley:";function M(t) {
    return 0 === t.lastIndexOf(T, 0) ? t.substr(T.length) : t;
  }return h.listen = function (t, e) {
    var i;if (P(), "object" === r(arguments[1]) && "function" == typeof arguments[2] && (i = arguments[1], e = arguments[2]), "function" != typeof e) throw new Error("Wrong parameters");window.Parsley.on(M(t), O(e, i));
  }, h.listenTo = function (t, e, i) {
    if (P(), !(t instanceof b || t instanceof y)) throw new Error("Must give Parsley instance");if ("string" != typeof e || "function" != typeof i) throw new Error("Wrong parameters");t.on(M(e), O(i));
  }, h.unsubscribe = function (t, e) {
    if (P(), "string" != typeof t || "function" != typeof e) throw new Error("Wrong arguments");window.Parsley.off(M(t), e.parsleyAdaptedCallback);
  }, h.unsubscribeTo = function (t, e) {
    if (P(), !(t instanceof b || t instanceof y)) throw new Error("Must give Parsley instance");t.off(M(e));
  }, h.unsubscribeAll = function (e) {
    P(), window.Parsley.off(M(e)), h("form,input,textarea,select").each(function () {
      var t = h(this).data("Parsley");t && t.off(M(e));
    });
  }, h.emit = function (t, e) {
    var i;P();var n = e instanceof b || e instanceof y,
        r = Array.prototype.slice.call(arguments, n ? 2 : 1);r.unshift(M(t)), n || (e = window.Parsley), (i = e).trigger.apply(i, u(r));
  }, h.extend(!0, x, { asyncValidators: { default: { fn: function (t) {
          return 200 <= t.status && t.status < 300;
        }, url: !1 }, reverse: { fn: function (t) {
          return t.status < 200 || 300 <= t.status;
        }, url: !1 } }, addAsyncValidator: function (t, e, i, n) {
      return x.asyncValidators[t] = { fn: e, url: i || !1, options: n || {} }, this;
    } }), x.addValidator("remote", { requirementType: { "": "string", validator: "string", reverse: "boolean", options: "object" }, validateString: function (t, e, i, n) {
      var r,
          s,
          a = {},
          o = i.validator || (!0 === i.reverse ? "reverse" : "default");if (void 0 === x.asyncValidators[o]) throw new Error("Calling an undefined async validator: `" + o + "`");-1 < (e = x.asyncValidators[o].url || e).indexOf("{value}") ? e = e.replace("{value}", encodeURIComponent(t)) : a[n.element.getAttribute("name") || n.element.getAttribute("id")] = t;var l = h.extend(!0, i.options || {}, x.asyncValidators[o].options);r = h.extend(!0, {}, { url: e, data: a, type: "GET" }, l), n.trigger("field:ajaxoptions", n, r), s = h.param(r), void 0 === x._remoteCache && (x._remoteCache = {});var u = x._remoteCache[s] = x._remoteCache[s] || h.ajax(r),
          d = function () {
        var t = x.asyncValidators[o].fn.call(n, u, e, i);return t || (t = h.Deferred().reject()), h.when(t);
      };return u.then(d, d);
    }, priority: -1 }), x.on("form:submit", function () {
    x._remoteCache = {};
  }), s.prototype.addAsyncValidator = function () {
    return d.warnOnce("Accessing the method `addAsyncValidator` through an instance is deprecated. Simply call `Parsley.addAsyncValidator(...)`"), x.addAsyncValidator.apply(x, arguments);
  }, x.addMessages("en", { defaultMessage: "This value seems to be invalid.", type: { email: "This value should be a valid email.", url: "This value should be a valid url.", number: "This value should be a valid number.", integer: "This value should be a valid integer.", digits: "This value should be digits.", alphanum: "This value should be alphanumeric." }, notblank: "This value should not be blank.", required: "This value is required.", pattern: "This value seems to be invalid.", min: "This value should be greater than or equal to %s.", max: "This value should be lower than or equal to %s.", range: "This value should be between %s and %s.", minlength: "This value is too short. It should have %s characters or more.", maxlength: "This value is too long. It should have %s characters or fewer.", length: "This value length is invalid. It should be between %s and %s characters long.", mincheck: "You must select at least %s choices.", maxcheck: "You must select %s choices or fewer.", check: "You must select between %s and %s choices.", equalto: "This value should be the same.", euvatin: "It's not a valid VAT Identification Number." }), x.setLocale("en"), new function () {
    var n = this,
        r = window || global;o(this, { isNativeEvent: function (t) {
        return t.originalEvent && !1 !== t.originalEvent.isTrusted;
      }, fakeInputEvent: function (t) {
        n.isNativeEvent(t) && h(t.target).trigger("input");
      }, misbehaves: function (t) {
        n.isNativeEvent(t) && (n.behavesOk(t), h(document).on("change.inputevent", t.data.selector, n.fakeInputEvent), n.fakeInputEvent(t));
      }, behavesOk: function (t) {
        n.isNativeEvent(t) && h(document).off("input.inputevent", t.data.selector, n.behavesOk).off("change.inputevent", t.data.selector, n.misbehaves);
      }, install: function () {
        if (!r.inputEventPatched) {
          r.inputEventPatched = "0.0.3";for (var t = ["select", 'input[type="checkbox"]', 'input[type="radio"]', 'input[type="file"]'], e = 0; e < t.length; e++) {
            var i = t[e];h(document).on("input.inputevent", i, { selector: i }, n.behavesOk).on("change.inputevent", i, { selector: i }, n.misbehaves);
          }
        }
      }, uninstall: function () {
        delete r.inputEventPatched, h(document).off(".inputevent");
      } });
  }().install(), x;
});
//# sourceMappingURL=parsley.min.js.map
;'use strict';

/**
 * Created by emak on 21.04.16.
 */
(function ($) {
    'use strict';

    $('.author-module .open').click(function () {
        $(this).toggleClass('active');
        $(this).parent().toggleClass('active');
    });
})(jQuery);
;'use strict';

(function ($) {
  var instance = $('.newsletter-form form').parsley();
  var lang = $('html').attr('lang').slice(0, 2);

  var messages = {
    'de': {
      'email': 'Bitte geben Sie eine gültige E-Mail-Adresse an.',
      'data': 'Bitte akzeptieren Sie die Datenschutzbestimmungen.'
    },
    'en': {
      'email': 'Please enter a valid e-mailaddress.',
      'data': ' Please accept the privacy policy.'
    }
  };

  $('#form_EMAIL').attr('required', true).attr('data-parsley-type', 'email').attr('data-parsley-error-message', messages[lang].email);

  $('#form_NLLUFFTPOST').attr('required', true).attr('data-parsley-error-message', messages[lang].data);

  var commentInstance = $('#commentform').parsley();
  $('#author').attr('required', true);
  $('#email').attr('required', true).attr('data-parsley-type', 'email').attr('data-parsley-error-message', messages[lang].email);

  $('#comment').attr('required', true).attr('data-parsley-minlength', 3);
})(jQuery);
;"use strict";

jQuery(document).foundation();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndoYXQtaW5wdXQuanMiLCJmb3VuZGF0aW9uLmNvcmUuanMiLCJmb3VuZGF0aW9uLnV0aWwuYm94LmpzIiwiZm91bmRhdGlvbi51dGlsLmtleWJvYXJkLmpzIiwiZm91bmRhdGlvbi51dGlsLm1lZGlhUXVlcnkuanMiLCJmb3VuZGF0aW9uLnV0aWwubW90aW9uLmpzIiwiZm91bmRhdGlvbi51dGlsLm5lc3QuanMiLCJmb3VuZGF0aW9uLnV0aWwudGltZXJBbmRJbWFnZUxvYWRlci5qcyIsImZvdW5kYXRpb24udXRpbC50b3VjaC5qcyIsImZvdW5kYXRpb24udXRpbC50cmlnZ2Vycy5qcyIsImZvdW5kYXRpb24ub2ZmY2FudmFzLmpzIiwiZm91bmRhdGlvbi5yZXZlYWwuanMiLCJ3YXZlcy5qcyIsImVyc3RlbGxiYXItc3dpdGNoLmpzIiwicGFyc2xleS5taW4uanMiLCJhdXRob3ItbW9kdWxlLmpzIiwiZm9ybXMuanMiLCJpbml0LWZvdW5kYXRpb24uanMiXSwibmFtZXMiOlsid2luZG93Iiwid2hhdElucHV0IiwiYWN0aXZlS2V5cyIsImJvZHkiLCJidWZmZXIiLCJjdXJyZW50SW5wdXQiLCJub25UeXBpbmdJbnB1dHMiLCJtb3VzZVdoZWVsIiwiZGV0ZWN0V2hlZWwiLCJpZ25vcmVNYXAiLCJpbnB1dE1hcCIsImlucHV0VHlwZXMiLCJrZXlNYXAiLCJwb2ludGVyTWFwIiwidGltZXIiLCJldmVudEJ1ZmZlciIsImNsZWFyVGltZXIiLCJzZXRJbnB1dCIsImV2ZW50Iiwic2V0VGltZW91dCIsImJ1ZmZlcmVkRXZlbnQiLCJ1bkJ1ZmZlcmVkRXZlbnQiLCJjbGVhclRpbWVvdXQiLCJldmVudEtleSIsImtleSIsInZhbHVlIiwidHlwZSIsInBvaW50ZXJUeXBlIiwiZXZlbnRUYXJnZXQiLCJ0YXJnZXQiLCJldmVudFRhcmdldE5vZGUiLCJub2RlTmFtZSIsInRvTG93ZXJDYXNlIiwiZXZlbnRUYXJnZXRUeXBlIiwiZ2V0QXR0cmlidXRlIiwiaGFzQXR0cmlidXRlIiwiaW5kZXhPZiIsInN3aXRjaElucHV0IiwibG9nS2V5cyIsInN0cmluZyIsInNldEF0dHJpYnV0ZSIsInB1c2giLCJrZXlDb2RlIiwid2hpY2giLCJzcmNFbGVtZW50IiwidW5Mb2dLZXlzIiwiYXJyYXlQb3MiLCJzcGxpY2UiLCJiaW5kRXZlbnRzIiwiZG9jdW1lbnQiLCJQb2ludGVyRXZlbnQiLCJhZGRFdmVudExpc3RlbmVyIiwiTVNQb2ludGVyRXZlbnQiLCJjcmVhdGVFbGVtZW50Iiwib25tb3VzZXdoZWVsIiwidW5kZWZpbmVkIiwiQXJyYXkiLCJwcm90b3R5cGUiLCJhc2siLCJrZXlzIiwidHlwZXMiLCJzZXQiLCIkIiwiRk9VTkRBVElPTl9WRVJTSU9OIiwiRm91bmRhdGlvbiIsInZlcnNpb24iLCJfcGx1Z2lucyIsIl91dWlkcyIsInJ0bCIsImF0dHIiLCJwbHVnaW4iLCJuYW1lIiwiY2xhc3NOYW1lIiwiZnVuY3Rpb25OYW1lIiwiYXR0ck5hbWUiLCJoeXBoZW5hdGUiLCJyZWdpc3RlclBsdWdpbiIsInBsdWdpbk5hbWUiLCJjb25zdHJ1Y3RvciIsInV1aWQiLCJHZXRZb0RpZ2l0cyIsIiRlbGVtZW50IiwiZGF0YSIsInRyaWdnZXIiLCJ1bnJlZ2lzdGVyUGx1Z2luIiwicmVtb3ZlQXR0ciIsInJlbW92ZURhdGEiLCJwcm9wIiwicmVJbml0IiwicGx1Z2lucyIsImlzSlEiLCJlYWNoIiwiX2luaXQiLCJfdGhpcyIsImZucyIsInBsZ3MiLCJmb3JFYWNoIiwicCIsImZvdW5kYXRpb24iLCJPYmplY3QiLCJlcnIiLCJjb25zb2xlIiwiZXJyb3IiLCJsZW5ndGgiLCJuYW1lc3BhY2UiLCJNYXRoIiwicm91bmQiLCJwb3ciLCJyYW5kb20iLCJ0b1N0cmluZyIsInNsaWNlIiwicmVmbG93IiwiZWxlbSIsImkiLCIkZWxlbSIsImZpbmQiLCJhZGRCYWNrIiwiJGVsIiwib3B0cyIsIndhcm4iLCJ0aGluZyIsInNwbGl0IiwiZSIsIm9wdCIsIm1hcCIsImVsIiwidHJpbSIsInBhcnNlVmFsdWUiLCJlciIsImdldEZuTmFtZSIsInRyYW5zaXRpb25lbmQiLCJ0cmFuc2l0aW9ucyIsImVuZCIsInQiLCJzdHlsZSIsInRyaWdnZXJIYW5kbGVyIiwidXRpbCIsInRocm90dGxlIiwiZnVuYyIsImRlbGF5IiwiY29udGV4dCIsImFyZ3MiLCJhcmd1bWVudHMiLCJhcHBseSIsIm1ldGhvZCIsIiRtZXRhIiwiJG5vSlMiLCJhcHBlbmRUbyIsImhlYWQiLCJyZW1vdmVDbGFzcyIsIk1lZGlhUXVlcnkiLCJjYWxsIiwicGx1Z0NsYXNzIiwiUmVmZXJlbmNlRXJyb3IiLCJUeXBlRXJyb3IiLCJmbiIsIkRhdGUiLCJub3ciLCJnZXRUaW1lIiwidmVuZG9ycyIsInJlcXVlc3RBbmltYXRpb25GcmFtZSIsInZwIiwiY2FuY2VsQW5pbWF0aW9uRnJhbWUiLCJ0ZXN0IiwibmF2aWdhdG9yIiwidXNlckFnZW50IiwibGFzdFRpbWUiLCJjYWxsYmFjayIsIm5leHRUaW1lIiwibWF4IiwicGVyZm9ybWFuY2UiLCJzdGFydCIsIkZ1bmN0aW9uIiwiYmluZCIsIm9UaGlzIiwiYUFyZ3MiLCJmVG9CaW5kIiwiZk5PUCIsImZCb3VuZCIsImNvbmNhdCIsImZ1bmNOYW1lUmVnZXgiLCJyZXN1bHRzIiwiZXhlYyIsInN0ciIsImlzTmFOIiwicGFyc2VGbG9hdCIsInJlcGxhY2UiLCJqUXVlcnkiLCJCb3giLCJJbU5vdFRvdWNoaW5nWW91IiwiR2V0RGltZW5zaW9ucyIsIkdldE9mZnNldHMiLCJlbGVtZW50IiwicGFyZW50IiwibHJPbmx5IiwidGJPbmx5IiwiZWxlRGltcyIsInRvcCIsImJvdHRvbSIsImxlZnQiLCJyaWdodCIsInBhckRpbXMiLCJvZmZzZXQiLCJoZWlnaHQiLCJ3aWR0aCIsIndpbmRvd0RpbXMiLCJhbGxEaXJzIiwiRXJyb3IiLCJyZWN0IiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0IiwicGFyUmVjdCIsInBhcmVudE5vZGUiLCJ3aW5SZWN0Iiwid2luWSIsInBhZ2VZT2Zmc2V0Iiwid2luWCIsInBhZ2VYT2Zmc2V0IiwicGFyZW50RGltcyIsImFuY2hvciIsInBvc2l0aW9uIiwidk9mZnNldCIsImhPZmZzZXQiLCJpc092ZXJmbG93IiwiJGVsZURpbXMiLCIkYW5jaG9yRGltcyIsImtleUNvZGVzIiwiY29tbWFuZHMiLCJLZXlib2FyZCIsImdldEtleUNvZGVzIiwicGFyc2VLZXkiLCJTdHJpbmciLCJmcm9tQ2hhckNvZGUiLCJ0b1VwcGVyQ2FzZSIsInNoaWZ0S2V5IiwiY3RybEtleSIsImFsdEtleSIsImhhbmRsZUtleSIsImNvbXBvbmVudCIsImZ1bmN0aW9ucyIsImNvbW1hbmRMaXN0IiwiY21kcyIsImNvbW1hbmQiLCJsdHIiLCJleHRlbmQiLCJyZXR1cm5WYWx1ZSIsImhhbmRsZWQiLCJ1bmhhbmRsZWQiLCJmaW5kRm9jdXNhYmxlIiwiZmlsdGVyIiwiaXMiLCJyZWdpc3RlciIsImNvbXBvbmVudE5hbWUiLCJrY3MiLCJrIiwia2MiLCJkZWZhdWx0UXVlcmllcyIsImxhbmRzY2FwZSIsInBvcnRyYWl0IiwicmV0aW5hIiwicXVlcmllcyIsImN1cnJlbnQiLCJzZWxmIiwiZXh0cmFjdGVkU3R5bGVzIiwiY3NzIiwibmFtZWRRdWVyaWVzIiwicGFyc2VTdHlsZVRvT2JqZWN0IiwiaGFzT3duUHJvcGVydHkiLCJfZ2V0Q3VycmVudFNpemUiLCJfd2F0Y2hlciIsImF0TGVhc3QiLCJzaXplIiwicXVlcnkiLCJnZXQiLCJtYXRjaE1lZGlhIiwibWF0Y2hlcyIsIm1hdGNoZWQiLCJvbiIsIm5ld1NpemUiLCJjdXJyZW50U2l6ZSIsInN0eWxlTWVkaWEiLCJtZWRpYSIsInNjcmlwdCIsImdldEVsZW1lbnRzQnlUYWdOYW1lIiwiaW5mbyIsImlkIiwiaW5zZXJ0QmVmb3JlIiwiZ2V0Q29tcHV0ZWRTdHlsZSIsImN1cnJlbnRTdHlsZSIsIm1hdGNoTWVkaXVtIiwidGV4dCIsInN0eWxlU2hlZXQiLCJjc3NUZXh0IiwidGV4dENvbnRlbnQiLCJzdHlsZU9iamVjdCIsInJlZHVjZSIsInJldCIsInBhcmFtIiwicGFydHMiLCJ2YWwiLCJkZWNvZGVVUklDb21wb25lbnQiLCJpc0FycmF5IiwiaW5pdENsYXNzZXMiLCJhY3RpdmVDbGFzc2VzIiwiTW90aW9uIiwiYW5pbWF0ZUluIiwiYW5pbWF0aW9uIiwiY2IiLCJhbmltYXRlIiwiYW5pbWF0ZU91dCIsIk1vdmUiLCJkdXJhdGlvbiIsImFuaW0iLCJwcm9nIiwibW92ZSIsInRzIiwiaXNJbiIsImVxIiwiaW5pdENsYXNzIiwiYWN0aXZlQ2xhc3MiLCJyZXNldCIsImFkZENsYXNzIiwic2hvdyIsIm9mZnNldFdpZHRoIiwib25lIiwiZmluaXNoIiwiaGlkZSIsInRyYW5zaXRpb25EdXJhdGlvbiIsIk5lc3QiLCJGZWF0aGVyIiwibWVudSIsIml0ZW1zIiwic3ViTWVudUNsYXNzIiwic3ViSXRlbUNsYXNzIiwiaGFzU3ViQ2xhc3MiLCIkaXRlbSIsIiRzdWIiLCJjaGlsZHJlbiIsIkJ1cm4iLCJUaW1lciIsIm9wdGlvbnMiLCJuYW1lU3BhY2UiLCJyZW1haW4iLCJpc1BhdXNlZCIsInJlc3RhcnQiLCJpbmZpbml0ZSIsInBhdXNlIiwib25JbWFnZXNMb2FkZWQiLCJpbWFnZXMiLCJ1bmxvYWRlZCIsImNvbXBsZXRlIiwic2luZ2xlSW1hZ2VMb2FkZWQiLCJuYXR1cmFsV2lkdGgiLCJzcG90U3dpcGUiLCJlbmFibGVkIiwiZG9jdW1lbnRFbGVtZW50IiwicHJldmVudERlZmF1bHQiLCJtb3ZlVGhyZXNob2xkIiwidGltZVRocmVzaG9sZCIsInN0YXJ0UG9zWCIsInN0YXJ0UG9zWSIsInN0YXJ0VGltZSIsImVsYXBzZWRUaW1lIiwiaXNNb3ZpbmciLCJvblRvdWNoRW5kIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsIm9uVG91Y2hNb3ZlIiwieCIsInRvdWNoZXMiLCJwYWdlWCIsInkiLCJwYWdlWSIsImR4IiwiZHkiLCJkaXIiLCJhYnMiLCJvblRvdWNoU3RhcnQiLCJpbml0IiwidGVhcmRvd24iLCJzcGVjaWFsIiwic3dpcGUiLCJzZXR1cCIsIm5vb3AiLCJhZGRUb3VjaCIsImhhbmRsZVRvdWNoIiwiY2hhbmdlZFRvdWNoZXMiLCJmaXJzdCIsImV2ZW50VHlwZXMiLCJ0b3VjaHN0YXJ0IiwidG91Y2htb3ZlIiwidG91Y2hlbmQiLCJzaW11bGF0ZWRFdmVudCIsIk1vdXNlRXZlbnQiLCJzY3JlZW5YIiwic2NyZWVuWSIsImNsaWVudFgiLCJjbGllbnRZIiwiY3JlYXRlRXZlbnQiLCJpbml0TW91c2VFdmVudCIsImRpc3BhdGNoRXZlbnQiLCJNdXRhdGlvbk9ic2VydmVyIiwicHJlZml4ZXMiLCJ0cmlnZ2VycyIsInN0b3BQcm9wYWdhdGlvbiIsImZhZGVPdXQiLCJjaGVja0xpc3RlbmVycyIsImV2ZW50c0xpc3RlbmVyIiwicmVzaXplTGlzdGVuZXIiLCJzY3JvbGxMaXN0ZW5lciIsImNsb3NlbWVMaXN0ZW5lciIsInlldGlCb3hlcyIsInBsdWdOYW1lcyIsImxpc3RlbmVycyIsImpvaW4iLCJvZmYiLCJwbHVnaW5JZCIsIm5vdCIsImRlYm91bmNlIiwiJG5vZGVzIiwibm9kZXMiLCJxdWVyeVNlbGVjdG9yQWxsIiwibGlzdGVuaW5nRWxlbWVudHNNdXRhdGlvbiIsIm11dGF0aW9uUmVjb3Jkc0xpc3QiLCIkdGFyZ2V0IiwiZWxlbWVudE9ic2VydmVyIiwib2JzZXJ2ZSIsImF0dHJpYnV0ZXMiLCJjaGlsZExpc3QiLCJjaGFyYWN0ZXJEYXRhIiwic3VidHJlZSIsImF0dHJpYnV0ZUZpbHRlciIsIklIZWFyWW91IiwiT2ZmQ2FudmFzIiwiZGVmYXVsdHMiLCIkbGFzdFRyaWdnZXIiLCIkdHJpZ2dlcnMiLCJfZXZlbnRzIiwiY2xvc2VPbkNsaWNrIiwiJGV4aXRlciIsImV4aXRlciIsImFwcGVuZCIsImlzUmV2ZWFsZWQiLCJSZWdFeHAiLCJyZXZlYWxDbGFzcyIsInJldmVhbE9uIiwibWF0Y2giLCJfc2V0TVFDaGVja2VyIiwidHJhbnNpdGlvblRpbWUiLCJvcGVuIiwiY2xvc2UiLCJ0b2dnbGUiLCJfaGFuZGxlS2V5Ym9hcmQiLCJyZXZlYWwiLCIkY2xvc2VyIiwiaGFzQ2xhc3MiLCIkYm9keSIsImZvcmNlVG9wIiwic2Nyb2xsVG9wIiwiJHdyYXBwZXIiLCJhdXRvRm9jdXMiLCJmb2N1cyIsInRyYXBGb2N1cyIsImZvY3VzYWJsZSIsImxhc3QiLCJSZXZlYWwiLCJpc0FjdGl2ZSIsImNhY2hlZCIsIm1xIiwiaXNNb2JpbGUiLCJtb2JpbGVTbmlmZiIsIiRhbmNob3IiLCJmdWxsU2NyZWVuIiwib3ZlcmxheSIsIiRvdmVybGF5IiwiX21ha2VPdmVybGF5IiwiZGV0YWNoIiwiZGVlcExpbmsiLCJsb2NhdGlvbiIsImhhc2giLCJvdXRlcldpZHRoIiwib3V0ZXJIZWlnaHQiLCJwYXJzZUludCIsIm1pbiIsIm1hcmdpbiIsInBhcmVudHMiLCJfdXBkYXRlUG9zaXRpb24iLCJjb250YWlucyIsIl9oYW5kbGVTdGF0ZSIsImhpc3RvcnkiLCJwdXNoU3RhdGUiLCJtdWx0aXBsZU9wZW5lZCIsImFuaW1hdGlvbkluIiwiYWZ0ZXJBbmltYXRpb25Gb2N1cyIsImZvY3VzYWJsZUVsZW1lbnRzIiwic2hvd0RlbGF5Iiwib3JpZ2luYWxTY3JvbGxQb3MiLCJfZXh0cmFIYW5kbGVycyIsImNsb3NlT25Fc2MiLCJ0YWJfZm9yd2FyZCIsInRhYl9iYWNrd2FyZCIsImFuaW1hdGlvbk91dCIsImZpbmlzaFVwIiwiaGlkZURlbGF5IiwicmVzZXRPbkNsb3NlIiwiaHRtbCIsInJlcGxhY2VTdGF0ZSIsInRpdGxlIiwicGF0aG5hbWUiLCJyZW1vdmUiLCJidG1PZmZzZXRQY3QiLCJpUGhvbmVTbmlmZiIsImFuZHJvaWRTbmlmZiIsIldhdmVzIiwiJCQiLCJpc1dpbmRvdyIsIm9iaiIsImdldFdpbmRvdyIsIm5vZGVUeXBlIiwiZGVmYXVsdFZpZXciLCJkb2NFbGVtIiwid2luIiwiYm94IiwiZG9jIiwib3duZXJEb2N1bWVudCIsImNsaWVudFRvcCIsImNsaWVudExlZnQiLCJjb252ZXJ0U3R5bGUiLCJhIiwiRWZmZWN0IiwiYnV0dG9uIiwicmlwcGxlIiwiYXBwZW5kQ2hpbGQiLCJwb3MiLCJyZWxhdGl2ZVkiLCJyZWxhdGl2ZVgiLCJzY2FsZSIsImNsaWVudFdpZHRoIiwicmlwcGxlU3R5bGUiLCJ0cmFuc2Zvcm0iLCJvcGFjaXR5IiwiVG91Y2hIYW5kbGVyIiwidG91Y2h1cCIsInJpcHBsZXMiLCJnZXRFbGVtZW50c0J5Q2xhc3NOYW1lIiwiZGlmZiIsIk51bWJlciIsInJlbW92ZUNoaWxkIiwid3JhcElucHV0IiwiZWxlbWVudHMiLCJ0YWdOYW1lIiwid3JhcHBlciIsImVsZW1lbnRTdHlsZSIsInJlbW92ZUF0dHJpYnV0ZSIsInJlcGxhY2VDaGlsZCIsImFsbG93RXZlbnQiLCJhbGxvdyIsImdldFdhdmVzRWZmZWN0RWxlbWVudCIsInBhcmVudEVsZW1lbnQiLCJTVkdFbGVtZW50IiwiY2xhc3NMaXN0Iiwic2hvd0VmZmVjdCIsImRpc3BsYXlFZmZlY3QiLCJhdHRhY2giLCJhZGQiLCJxdWVyeVNlbGVjdG9yIiwiZXhwb3J0cyIsIm1vZHVsZSIsInJlcXVpcmUiLCJkZWZpbmUiLCJhbWQiLCJwYXJzbGV5IiwiaCIsInIiLCJTeW1ib2wiLCJpdGVyYXRvciIsIm8iLCJhc3NpZ24iLCJuIiwibCIsInMiLCJuZXh0IiwiZG9uZSIsInJldHVybiIsInUiLCJmcm9tIiwiZCIsInNwZWNpZmllZCIsImNhbWVsaXplIiwiZGVzZXJpYWxpemVWYWx1ZSIsImNoZWNrQXR0ciIsInNldEF0dHIiLCJkYXNoZXJpemUiLCJnZXRUeXBlIiwiZ2VuZXJhdGVJRCIsIkpTT04iLCJwYXJzZSIsIndhcm5PbmNlIiwiX3Jlc2V0V2FybmluZ3MiLCJ0cmltU3RyaW5nIiwiZGF0ZSIsImdldEZ1bGxZZWFyIiwiZ2V0TW9udGgiLCJnZXREYXRlIiwiaW50ZWdlciIsIm51bWJlciIsImJvb2xlYW4iLCJvYmplY3QiLCJyZWdleHAiLCJwYXJzZVJlcXVpcmVtZW50IiwibmFtZXNwYWNlRXZlbnRzIiwiZGlmZmVyZW5jZSIsImFsbCIsIndoZW4iLCJvYmplY3RDcmVhdGUiLCJjcmVhdGUiLCJfU3VibWl0U2VsZWN0b3IiLCJpbnB1dHMiLCJleGNsdWRlZCIsInByaW9yaXR5RW5hYmxlZCIsIm11bHRpcGxlIiwiZ3JvdXAiLCJ1aUVuYWJsZWQiLCJ2YWxpZGF0aW9uVGhyZXNob2xkIiwidHJpZ2dlckFmdGVyRmFpbHVyZSIsImVycm9yQ2xhc3MiLCJzdWNjZXNzQ2xhc3MiLCJjbGFzc0hhbmRsZXIiLCJlcnJvcnNDb250YWluZXIiLCJlcnJvcnNXcmFwcGVyIiwiZXJyb3JUZW1wbGF0ZSIsIl9faWRfXyIsImFzeW5jU3VwcG9ydCIsIl9waXBlQWNjb3JkaW5nVG9WYWxpZGF0aW9uUmVzdWx0IiwiRGVmZXJyZWQiLCJ2YWxpZGF0aW9uUmVzdWx0IiwicmVqZWN0IiwicmVzb2x2ZSIsInByb21pc2UiLCJhY3R1YWxpemVPcHRpb25zIiwiZG9tT3B0aW9ucyIsIl9yZXNldE9wdGlvbnMiLCJfbGlzdGVuZXJzIiwic3Vic2NyaWJlIiwibGlzdGVuVG8iLCJ1bnN1YnNjcmliZSIsInVuc3Vic2NyaWJlVG8iLCJhc3luY0lzVmFsaWQiLCJ3aGVuVmFsaWQiLCJmb3JjZSIsIl9maW5kUmVsYXRlZCIsImMiLCJ2YWxpZGF0ZSIsInZhbGlkYXRlTXVsdGlwbGUiLCJ2YWxpZGF0ZURhdGUiLCJfaXNEYXRlSW5wdXQiLCJ2YWxpZGF0ZU51bWJlciIsInZhbGlkYXRlU3RyaW5nIiwicGFyc2VSZXF1aXJlbWVudHMiLCJyZXF1aXJlbWVudFR5cGUiLCJpc1BsYWluT2JqZWN0IiwicHJpb3JpdHkiLCJfX2NsYXNzX18iLCJsb2NhbGUiLCJlbWFpbCIsImRpZ2l0cyIsImFscGhhbnVtIiwidXJsIiwicmFuZ2UiLCJmIiwibSIsInBvcCIsImciLCJjYXRhbG9nIiwidmFsaWRhdG9ycyIsImFkZFZhbGlkYXRvciIsIlBhcnNsZXkiLCJzZXRMb2NhbGUiLCJhZGRDYXRhbG9nIiwiYWRkTWVzc2FnZSIsImFkZE1lc3NhZ2VzIiwiX3NldFZhbGlkYXRvciIsImhhc1ZhbGlkYXRvciIsInVwZGF0ZVZhbGlkYXRvciIsInJlbW92ZVZhbGlkYXRvciIsIm1lc3NhZ2VzIiwiZ2V0RXJyb3JNZXNzYWdlIiwicmVxdWlyZW1lbnRzIiwiZm9ybWF0TWVzc2FnZSIsImRlZmF1bHRNZXNzYWdlIiwiZW4iLCJub3RibGFuayIsInJlcXVpcmVkIiwic3RlcCIsImJhc2UiLCJwYXR0ZXJuIiwibWlubGVuZ3RoIiwibWF4bGVuZ3RoIiwibWluY2hlY2siLCJtYXhjaGVjayIsImNoZWNrIiwiZXF1YWx0byIsImV1dmF0aW4iLCJ2IiwiRm9ybSIsIl9hY3R1YWxpemVUcmlnZ2VycyIsIm9uU3VibWl0VmFsaWRhdGUiLCJvblN1Ym1pdEJ1dHRvbiIsIl9mb2N1c2VkRmllbGQiLCJmaWVsZHMiLCJub0ZvY3VzIiwiX2Rlc3Ryb3lVSSIsIkZpZWxkIiwiX3JlZmxvd1VJIiwiX2J1aWxkVUkiLCJfdWkiLCJhc3NlcnQiLCJrZXB0IiwiYWRkZWQiLCJyZW1vdmVkIiwibGFzdFZhbGlkYXRpb25SZXN1bHQiLCJfbWFuYWdlU3RhdHVzQ2xhc3MiLCJfbWFuYWdlRXJyb3JzTWVzc2FnZXMiLCJfZmFpbGVkT25jZSIsImdldEVycm9yc01lc3NhZ2VzIiwiZXJyb3JNZXNzYWdlIiwiX2dldEVycm9yTWVzc2FnZSIsImFkZEVycm9yIiwibWVzc2FnZSIsInVwZGF0ZUNsYXNzIiwiX2FkZEVycm9yIiwiX2Vycm9yQ2xhc3MiLCJ1cGRhdGVFcnJvciIsIl91cGRhdGVFcnJvciIsInJlbW92ZUVycm9yIiwiX3JlbW92ZUVycm9yIiwiaGFzQ29uc3RyYWludHMiLCJuZWVkc1ZhbGlkYXRpb24iLCJfc3VjY2Vzc0NsYXNzIiwiX3Jlc2V0Q2xhc3MiLCJlcnJvcnNNZXNzYWdlc0Rpc2FibGVkIiwiX2luc2VydEVycm9yV3JhcHBlciIsIiRlcnJvcnNXcmFwcGVyIiwiJGVycm9yQ2xhc3NIYW5kbGVyIiwiZXJyb3JzV3JhcHBlcklkIiwiX21hbmFnZUNsYXNzSGFuZGxlciIsInZhbGlkYXRpb25JbmZvcm1hdGlvblZpc2libGUiLCJfaW5wdXRIb2xkZXIiLCJhZnRlciIsIl92YWxpZGF0ZUlmTmVlZGVkIiwiZ2V0VmFsdWUiLCJfZGVib3VuY2VkIiwiX3Jlc2V0VUkiLCJfIiwicGVuZGluZyIsInJlc29sdmVkIiwicmVqZWN0ZWQiLCJfc3VibWl0U291cmNlIiwiX3JlbW90ZUNhY2hlIiwid2hlblZhbGlkYXRlIiwic3RhdGUiLCJfdHJpZ2dlciIsInN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbiIsIl9zdWJtaXQiLCJjdXJyZW50VGFyZ2V0IiwiRXZlbnQiLCJzdWJtaXRFdmVudCIsIl9yZWZyZXNoRmllbGRzIiwiX3dpdGhvdXRSZWFjdHVhbGl6aW5nRm9ybU9wdGlvbnMiLCJmYWlsIiwiYWx3YXlzIiwicGlwZSIsImlzVmFsaWQiLCJyZWZyZXNoIiwiZGVzdHJveSIsIl9iaW5kRmllbGRzIiwiZmllbGRzTWFwcGVkQnlJZCIsIkZhY3RvcnkiLCJ3IiwiX3ZhbGlkYXRvclJlZ2lzdHJ5IiwidmFsaWRhdG9yIiwiaXNEb21Db25zdHJhaW50IiwiX3BhcnNlUmVxdWlyZW1lbnRzIiwiYiIsImNvbnN0cmFpbnRzIiwiY29uc3RyYWludHNCeU5hbWUiLCJfYmluZENvbnN0cmFpbnRzIiwiRiIsInJlcXVpcmVtZW50TGlzdCIsIl9pc0luR3JvdXAiLCJfcmVmcmVzaGVkIiwiX2lzUmVxdWlyZWQiLCJ2YWxpZGF0ZUlmRW1wdHkiLCJpbkFycmF5IiwiX2dldEdyb3VwZWRDb25zdHJhaW50cyIsIl92YWxpZGF0ZUNvbnN0cmFpbnQiLCJfaGFuZGxlV2hpdGVzcGFjZSIsIl9yZWZyZXNoQ29uc3RyYWludHMiLCJyZWZyZXNoQ29uc3RyYWludHMiLCJhZGRDb25zdHJhaW50IiwicmVtb3ZlQ29uc3RyYWludCIsInVwZGF0ZUNvbnN0cmFpbnQiLCJfYmluZEh0bWw1Q29uc3RyYWludHMiLCJ0cmltVmFsdWUiLCJ3aGl0ZXNwYWNlIiwic29ydCIsIkMiLCJhZGRFbGVtZW50IiwiJGVsZW1lbnRzIiwiaGFzIiwiQSIsIl9fdmVyc2lvbl9fIiwiaXNNdWx0aXBsZSIsImhhbmRsZU11bHRpcGxlIiwiUGFyc2xleUV4dGVuZCIsIkUiLCJqcXVlcnkiLCJwc2x5IiwiUGFyc2xleUNvbmZpZyIsIlV0aWxzIiwiUGFyc2xleVV0aWxzIiwiaTE4biIsIlBhcnNsZXlWYWxpZGF0b3IiLCJVSSIsIlBhcnNsZXlVSSIsImF1dG9CaW5kIiwiViIsIlAiLCJPIiwicGFyc2xleUFkYXB0ZWRDYWxsYmFjayIsInVuc2hpZnQiLCJUIiwiTSIsImxhc3RJbmRleE9mIiwic3Vic3RyIiwibGlzdGVuIiwidW5zdWJzY3JpYmVBbGwiLCJlbWl0IiwiYXN5bmNWYWxpZGF0b3JzIiwiZGVmYXVsdCIsInN0YXR1cyIsInJldmVyc2UiLCJhZGRBc3luY1ZhbGlkYXRvciIsImVuY29kZVVSSUNvbXBvbmVudCIsImFqYXgiLCJ0aGVuIiwiZ2xvYmFsIiwiaXNOYXRpdmVFdmVudCIsIm9yaWdpbmFsRXZlbnQiLCJpc1RydXN0ZWQiLCJmYWtlSW5wdXRFdmVudCIsIm1pc2JlaGF2ZXMiLCJiZWhhdmVzT2siLCJzZWxlY3RvciIsImluc3RhbGwiLCJpbnB1dEV2ZW50UGF0Y2hlZCIsInVuaW5zdGFsbCIsImNsaWNrIiwidG9nZ2xlQ2xhc3MiLCJpbnN0YW5jZSIsImxhbmciLCJjb21tZW50SW5zdGFuY2UiXSwibWFwcGluZ3MiOiI7O0FBQUFBLE9BQU9DLFNBQVAsR0FBb0IsWUFBVzs7QUFFN0I7O0FBRUE7Ozs7OztBQU1BOztBQUNBLE1BQUlDLGFBQWEsRUFBakI7O0FBRUE7QUFDQSxNQUFJQyxJQUFKOztBQUVBO0FBQ0EsTUFBSUMsU0FBUyxLQUFiOztBQUVBO0FBQ0EsTUFBSUMsZUFBZSxJQUFuQjs7QUFFQTtBQUNBLE1BQUlDLGtCQUFrQixDQUNwQixRQURvQixFQUVwQixVQUZvQixFQUdwQixNQUhvQixFQUlwQixPQUpvQixFQUtwQixPQUxvQixFQU1wQixPQU5vQixFQU9wQixRQVBvQixDQUF0Qjs7QUFVQTtBQUNBO0FBQ0EsTUFBSUMsYUFBYUMsYUFBakI7O0FBRUE7QUFDQTtBQUNBLE1BQUlDLFlBQVksQ0FDZCxFQURjLEVBQ1Y7QUFDSixJQUZjLEVBRVY7QUFDSixJQUhjLEVBR1Y7QUFDSixJQUpjLEVBSVY7QUFDSixJQUxjLENBS1Y7QUFMVSxHQUFoQjs7QUFRQTtBQUNBLE1BQUlDLFdBQVc7QUFDYixlQUFXLFVBREU7QUFFYixhQUFTLFVBRkk7QUFHYixpQkFBYSxPQUhBO0FBSWIsaUJBQWEsT0FKQTtBQUtiLHFCQUFpQixTQUxKO0FBTWIscUJBQWlCLFNBTko7QUFPYixtQkFBZSxTQVBGO0FBUWIsbUJBQWUsU0FSRjtBQVNiLGtCQUFjO0FBVEQsR0FBZjs7QUFZQTtBQUNBQSxXQUFTRixhQUFULElBQTBCLE9BQTFCOztBQUVBO0FBQ0EsTUFBSUcsYUFBYSxFQUFqQjs7QUFFQTtBQUNBLE1BQUlDLFNBQVM7QUFDWCxPQUFHLEtBRFE7QUFFWCxRQUFJLE9BRk87QUFHWCxRQUFJLE9BSE87QUFJWCxRQUFJLEtBSk87QUFLWCxRQUFJLE9BTE87QUFNWCxRQUFJLE1BTk87QUFPWCxRQUFJLElBUE87QUFRWCxRQUFJLE9BUk87QUFTWCxRQUFJO0FBVE8sR0FBYjs7QUFZQTtBQUNBLE1BQUlDLGFBQWE7QUFDZixPQUFHLE9BRFk7QUFFZixPQUFHLE9BRlksRUFFSDtBQUNaLE9BQUc7QUFIWSxHQUFqQjs7QUFNQTtBQUNBLE1BQUlDLEtBQUo7O0FBR0E7Ozs7OztBQU1BO0FBQ0EsV0FBU0MsV0FBVCxHQUF1QjtBQUNyQkM7QUFDQUMsYUFBU0MsS0FBVDs7QUFFQWQsYUFBUyxJQUFUO0FBQ0FVLFlBQVFkLE9BQU9tQixVQUFQLENBQWtCLFlBQVc7QUFDbkNmLGVBQVMsS0FBVDtBQUNELEtBRk8sRUFFTCxHQUZLLENBQVI7QUFHRDs7QUFFRCxXQUFTZ0IsYUFBVCxDQUF1QkYsS0FBdkIsRUFBOEI7QUFDNUIsUUFBSSxDQUFDZCxNQUFMLEVBQWFhLFNBQVNDLEtBQVQ7QUFDZDs7QUFFRCxXQUFTRyxlQUFULENBQXlCSCxLQUF6QixFQUFnQztBQUM5QkY7QUFDQUMsYUFBU0MsS0FBVDtBQUNEOztBQUVELFdBQVNGLFVBQVQsR0FBc0I7QUFDcEJoQixXQUFPc0IsWUFBUCxDQUFvQlIsS0FBcEI7QUFDRDs7QUFFRCxXQUFTRyxRQUFULENBQWtCQyxLQUFsQixFQUF5QjtBQUN2QixRQUFJSyxXQUFXQyxJQUFJTixLQUFKLENBQWY7QUFDQSxRQUFJTyxRQUFRZixTQUFTUSxNQUFNUSxJQUFmLENBQVo7QUFDQSxRQUFJRCxVQUFVLFNBQWQsRUFBeUJBLFFBQVFFLFlBQVlULEtBQVosQ0FBUjs7QUFFekI7QUFDQSxRQUFJYixpQkFBaUJvQixLQUFyQixFQUE0QjtBQUMxQixVQUFJRyxjQUFjQyxPQUFPWCxLQUFQLENBQWxCO0FBQ0EsVUFBSVksa0JBQWtCRixZQUFZRyxRQUFaLENBQXFCQyxXQUFyQixFQUF0QjtBQUNBLFVBQUlDLGtCQUFtQkgsb0JBQW9CLE9BQXJCLEdBQWdDRixZQUFZTSxZQUFaLENBQXlCLE1BQXpCLENBQWhDLEdBQW1FLElBQXpGOztBQUVBLFVBQ0UsQ0FBQztBQUNELE9BQUMvQixLQUFLZ0MsWUFBTCxDQUFrQiwyQkFBbEIsQ0FBRDs7QUFFQTtBQUNBOUIsa0JBSEE7O0FBS0E7QUFDQW9CLGdCQUFVLFVBTlY7O0FBUUE7QUFDQWIsYUFBT1csUUFBUCxNQUFxQixLQVRyQjs7QUFXQTtBQUVHTywwQkFBb0IsVUFBcEIsSUFDQUEsb0JBQW9CLFFBRHBCLElBRUNBLG9CQUFvQixPQUFwQixJQUErQnhCLGdCQUFnQjhCLE9BQWhCLENBQXdCSCxlQUF4QixJQUEyQyxDQWY5RSxDQURBO0FBa0JFO0FBQ0F4QixnQkFBVTJCLE9BQVYsQ0FBa0JiLFFBQWxCLElBQThCLENBQUMsQ0FwQm5DLEVBc0JFO0FBQ0E7QUFDRCxPQXhCRCxNQXdCTztBQUNMYyxvQkFBWVosS0FBWjtBQUNEO0FBQ0Y7O0FBRUQsUUFBSUEsVUFBVSxVQUFkLEVBQTBCYSxRQUFRZixRQUFSO0FBQzNCOztBQUVELFdBQVNjLFdBQVQsQ0FBcUJFLE1BQXJCLEVBQTZCO0FBQzNCbEMsbUJBQWVrQyxNQUFmO0FBQ0FwQyxTQUFLcUMsWUFBTCxDQUFrQixnQkFBbEIsRUFBb0NuQyxZQUFwQzs7QUFFQSxRQUFJTSxXQUFXeUIsT0FBWCxDQUFtQi9CLFlBQW5CLE1BQXFDLENBQUMsQ0FBMUMsRUFBNkNNLFdBQVc4QixJQUFYLENBQWdCcEMsWUFBaEI7QUFDOUM7O0FBRUQsV0FBU21CLEdBQVQsQ0FBYU4sS0FBYixFQUFvQjtBQUNsQixXQUFRQSxNQUFNd0IsT0FBUCxHQUFrQnhCLE1BQU13QixPQUF4QixHQUFrQ3hCLE1BQU15QixLQUEvQztBQUNEOztBQUVELFdBQVNkLE1BQVQsQ0FBZ0JYLEtBQWhCLEVBQXVCO0FBQ3JCLFdBQU9BLE1BQU1XLE1BQU4sSUFBZ0JYLE1BQU0wQixVQUE3QjtBQUNEOztBQUVELFdBQVNqQixXQUFULENBQXFCVCxLQUFyQixFQUE0QjtBQUMxQixRQUFJLE9BQU9BLE1BQU1TLFdBQWIsS0FBNkIsUUFBakMsRUFBMkM7QUFDekMsYUFBT2QsV0FBV0ssTUFBTVMsV0FBakIsQ0FBUDtBQUNELEtBRkQsTUFFTztBQUNMLGFBQVFULE1BQU1TLFdBQU4sS0FBc0IsS0FBdkIsR0FBZ0MsT0FBaEMsR0FBMENULE1BQU1TLFdBQXZELENBREssQ0FDK0Q7QUFDckU7QUFDRjs7QUFFRDtBQUNBLFdBQVNXLE9BQVQsQ0FBaUJmLFFBQWpCLEVBQTJCO0FBQ3pCLFFBQUlyQixXQUFXa0MsT0FBWCxDQUFtQnhCLE9BQU9XLFFBQVAsQ0FBbkIsTUFBeUMsQ0FBQyxDQUExQyxJQUErQ1gsT0FBT1csUUFBUCxDQUFuRCxFQUFxRXJCLFdBQVd1QyxJQUFYLENBQWdCN0IsT0FBT1csUUFBUCxDQUFoQjtBQUN0RTs7QUFFRCxXQUFTc0IsU0FBVCxDQUFtQjNCLEtBQW5CLEVBQTBCO0FBQ3hCLFFBQUlLLFdBQVdDLElBQUlOLEtBQUosQ0FBZjtBQUNBLFFBQUk0QixXQUFXNUMsV0FBV2tDLE9BQVgsQ0FBbUJ4QixPQUFPVyxRQUFQLENBQW5CLENBQWY7O0FBRUEsUUFBSXVCLGFBQWEsQ0FBQyxDQUFsQixFQUFxQjVDLFdBQVc2QyxNQUFYLENBQWtCRCxRQUFsQixFQUE0QixDQUE1QjtBQUN0Qjs7QUFFRCxXQUFTRSxVQUFULEdBQXNCO0FBQ3BCN0MsV0FBTzhDLFNBQVM5QyxJQUFoQjs7QUFFQTtBQUNBLFFBQUlILE9BQU9rRCxZQUFYLEVBQXlCO0FBQ3ZCL0MsV0FBS2dELGdCQUFMLENBQXNCLGFBQXRCLEVBQXFDL0IsYUFBckM7QUFDQWpCLFdBQUtnRCxnQkFBTCxDQUFzQixhQUF0QixFQUFxQy9CLGFBQXJDO0FBQ0QsS0FIRCxNQUdPLElBQUlwQixPQUFPb0QsY0FBWCxFQUEyQjtBQUNoQ2pELFdBQUtnRCxnQkFBTCxDQUFzQixlQUF0QixFQUF1Qy9CLGFBQXZDO0FBQ0FqQixXQUFLZ0QsZ0JBQUwsQ0FBc0IsZUFBdEIsRUFBdUMvQixhQUF2QztBQUNELEtBSE0sTUFHQTs7QUFFTDtBQUNBakIsV0FBS2dELGdCQUFMLENBQXNCLFdBQXRCLEVBQW1DL0IsYUFBbkM7QUFDQWpCLFdBQUtnRCxnQkFBTCxDQUFzQixXQUF0QixFQUFtQy9CLGFBQW5DOztBQUVBO0FBQ0EsVUFBSSxrQkFBa0JwQixNQUF0QixFQUE4QjtBQUM1QkcsYUFBS2dELGdCQUFMLENBQXNCLFlBQXRCLEVBQW9DcEMsV0FBcEM7QUFDRDtBQUNGOztBQUVEO0FBQ0FaLFNBQUtnRCxnQkFBTCxDQUFzQjVDLFVBQXRCLEVBQWtDYSxhQUFsQzs7QUFFQTtBQUNBakIsU0FBS2dELGdCQUFMLENBQXNCLFNBQXRCLEVBQWlDOUIsZUFBakM7QUFDQWxCLFNBQUtnRCxnQkFBTCxDQUFzQixPQUF0QixFQUErQjlCLGVBQS9CO0FBQ0E0QixhQUFTRSxnQkFBVCxDQUEwQixPQUExQixFQUFtQ04sU0FBbkM7QUFDRDs7QUFHRDs7Ozs7O0FBTUE7QUFDQTtBQUNBLFdBQVNyQyxXQUFULEdBQXVCO0FBQ3JCLFdBQU9ELGFBQWEsYUFBYTBDLFNBQVNJLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBYixHQUNsQixPQURrQixHQUNSOztBQUVWSixhQUFTSyxZQUFULEtBQTBCQyxTQUExQixHQUNFLFlBREYsR0FDaUI7QUFDZixvQkFMSixDQURxQixDQU1DO0FBQ3ZCOztBQUdEOzs7Ozs7OztBQVNBLE1BQ0Usc0JBQXNCdkQsTUFBdEIsSUFDQXdELE1BQU1DLFNBQU4sQ0FBZ0JyQixPQUZsQixFQUdFOztBQUVBO0FBQ0EsUUFBSWEsU0FBUzlDLElBQWIsRUFBbUI7QUFDakI2Qzs7QUFFRjtBQUNDLEtBSkQsTUFJTztBQUNMQyxlQUFTRSxnQkFBVCxDQUEwQixrQkFBMUIsRUFBOENILFVBQTlDO0FBQ0Q7QUFDRjs7QUFHRDs7Ozs7O0FBTUEsU0FBTzs7QUFFTDtBQUNBVSxTQUFLLFlBQVc7QUFBRSxhQUFPckQsWUFBUDtBQUFzQixLQUhuQzs7QUFLTDtBQUNBc0QsVUFBTSxZQUFXO0FBQUUsYUFBT3pELFVBQVA7QUFBb0IsS0FObEM7O0FBUUw7QUFDQTBELFdBQU8sWUFBVztBQUFFLGFBQU9qRCxVQUFQO0FBQW9CLEtBVG5DOztBQVdMO0FBQ0FrRCxTQUFLeEI7QUFaQSxHQUFQO0FBZUQsQ0F0U21CLEVBQXBCOzs7QUNBQSxDQUFDLFVBQVN5QixDQUFULEVBQVk7O0FBRWI7O0FBRUEsTUFBSUMscUJBQXFCLE9BQXpCOztBQUVBO0FBQ0E7QUFDQSxNQUFJQyxhQUFhO0FBQ2ZDLGFBQVNGLGtCQURNOztBQUdmOzs7QUFHQUcsY0FBVSxFQU5LOztBQVFmOzs7QUFHQUMsWUFBUSxFQVhPOztBQWFmOzs7QUFHQUMsU0FBSyxZQUFVO0FBQ2IsYUFBT04sRUFBRSxNQUFGLEVBQVVPLElBQVYsQ0FBZSxLQUFmLE1BQTBCLEtBQWpDO0FBQ0QsS0FsQmM7QUFtQmY7Ozs7QUFJQUMsWUFBUSxVQUFTQSxNQUFULEVBQWlCQyxJQUFqQixFQUF1QjtBQUM3QjtBQUNBO0FBQ0EsVUFBSUMsWUFBYUQsUUFBUUUsYUFBYUgsTUFBYixDQUF6QjtBQUNBO0FBQ0E7QUFDQSxVQUFJSSxXQUFZQyxVQUFVSCxTQUFWLENBQWhCOztBQUVBO0FBQ0EsV0FBS04sUUFBTCxDQUFjUSxRQUFkLElBQTBCLEtBQUtGLFNBQUwsSUFBa0JGLE1BQTVDO0FBQ0QsS0FqQ2M7QUFrQ2Y7Ozs7Ozs7OztBQVNBTSxvQkFBZ0IsVUFBU04sTUFBVCxFQUFpQkMsSUFBakIsRUFBc0I7QUFDcEMsVUFBSU0sYUFBYU4sT0FBT0ksVUFBVUosSUFBVixDQUFQLEdBQXlCRSxhQUFhSCxPQUFPUSxXQUFwQixFQUFpQzlDLFdBQWpDLEVBQTFDO0FBQ0FzQyxhQUFPUyxJQUFQLEdBQWMsS0FBS0MsV0FBTCxDQUFpQixDQUFqQixFQUFvQkgsVUFBcEIsQ0FBZDs7QUFFQSxVQUFHLENBQUNQLE9BQU9XLFFBQVAsQ0FBZ0JaLElBQWhCLFdBQTZCUSxVQUE3QixDQUFKLEVBQStDO0FBQUVQLGVBQU9XLFFBQVAsQ0FBZ0JaLElBQWhCLFdBQTZCUSxVQUE3QixFQUEyQ1AsT0FBT1MsSUFBbEQ7QUFBMEQ7QUFDM0csVUFBRyxDQUFDVCxPQUFPVyxRQUFQLENBQWdCQyxJQUFoQixDQUFxQixVQUFyQixDQUFKLEVBQXFDO0FBQUVaLGVBQU9XLFFBQVAsQ0FBZ0JDLElBQWhCLENBQXFCLFVBQXJCLEVBQWlDWixNQUFqQztBQUEyQztBQUM1RTs7OztBQUlOQSxhQUFPVyxRQUFQLENBQWdCRSxPQUFoQixjQUFtQ04sVUFBbkM7O0FBRUEsV0FBS1YsTUFBTCxDQUFZMUIsSUFBWixDQUFpQjZCLE9BQU9TLElBQXhCOztBQUVBO0FBQ0QsS0ExRGM7QUEyRGY7Ozs7Ozs7O0FBUUFLLHNCQUFrQixVQUFTZCxNQUFULEVBQWdCO0FBQ2hDLFVBQUlPLGFBQWFGLFVBQVVGLGFBQWFILE9BQU9XLFFBQVAsQ0FBZ0JDLElBQWhCLENBQXFCLFVBQXJCLEVBQWlDSixXQUE5QyxDQUFWLENBQWpCOztBQUVBLFdBQUtYLE1BQUwsQ0FBWXBCLE1BQVosQ0FBbUIsS0FBS29CLE1BQUwsQ0FBWS9CLE9BQVosQ0FBb0JrQyxPQUFPUyxJQUEzQixDQUFuQixFQUFxRCxDQUFyRDtBQUNBVCxhQUFPVyxRQUFQLENBQWdCSSxVQUFoQixXQUFtQ1IsVUFBbkMsRUFBaURTLFVBQWpELENBQTRELFVBQTVEO0FBQ007Ozs7QUFETixPQUtPSCxPQUxQLG1CQUsrQk4sVUFML0I7QUFNQSxXQUFJLElBQUlVLElBQVIsSUFBZ0JqQixNQUFoQixFQUF1QjtBQUNyQkEsZUFBT2lCLElBQVAsSUFBZSxJQUFmLENBRHFCLENBQ0Q7QUFDckI7QUFDRDtBQUNELEtBakZjOztBQW1GZjs7Ozs7O0FBTUNDLFlBQVEsVUFBU0MsT0FBVCxFQUFpQjtBQUN2QixVQUFJQyxPQUFPRCxtQkFBbUIzQixDQUE5QjtBQUNBLFVBQUc7QUFDRCxZQUFHNEIsSUFBSCxFQUFRO0FBQ05ELGtCQUFRRSxJQUFSLENBQWEsWUFBVTtBQUNyQjdCLGNBQUUsSUFBRixFQUFRb0IsSUFBUixDQUFhLFVBQWIsRUFBeUJVLEtBQXpCO0FBQ0QsV0FGRDtBQUdELFNBSkQsTUFJSztBQUNILGNBQUlsRSxPQUFPLE9BQU8rRCxPQUFsQjtBQUFBLGNBQ0FJLFFBQVEsSUFEUjtBQUFBLGNBRUFDLE1BQU07QUFDSixzQkFBVSxVQUFTQyxJQUFULEVBQWM7QUFDdEJBLG1CQUFLQyxPQUFMLENBQWEsVUFBU0MsQ0FBVCxFQUFXO0FBQ3RCQSxvQkFBSXRCLFVBQVVzQixDQUFWLENBQUo7QUFDQW5DLGtCQUFFLFdBQVVtQyxDQUFWLEdBQWEsR0FBZixFQUFvQkMsVUFBcEIsQ0FBK0IsT0FBL0I7QUFDRCxlQUhEO0FBSUQsYUFORztBQU9KLHNCQUFVLFlBQVU7QUFDbEJULHdCQUFVZCxVQUFVYyxPQUFWLENBQVY7QUFDQTNCLGdCQUFFLFdBQVUyQixPQUFWLEdBQW1CLEdBQXJCLEVBQTBCUyxVQUExQixDQUFxQyxPQUFyQztBQUNELGFBVkc7QUFXSix5QkFBYSxZQUFVO0FBQ3JCLG1CQUFLLFFBQUwsRUFBZUMsT0FBT3hDLElBQVAsQ0FBWWtDLE1BQU0zQixRQUFsQixDQUFmO0FBQ0Q7QUFiRyxXQUZOO0FBaUJBNEIsY0FBSXBFLElBQUosRUFBVStELE9BQVY7QUFDRDtBQUNGLE9BekJELENBeUJDLE9BQU1XLEdBQU4sRUFBVTtBQUNUQyxnQkFBUUMsS0FBUixDQUFjRixHQUFkO0FBQ0QsT0EzQkQsU0EyQlE7QUFDTixlQUFPWCxPQUFQO0FBQ0Q7QUFDRixLQXpIYTs7QUEySGY7Ozs7Ozs7O0FBUUFULGlCQUFhLFVBQVN1QixNQUFULEVBQWlCQyxTQUFqQixFQUEyQjtBQUN0Q0QsZUFBU0EsVUFBVSxDQUFuQjtBQUNBLGFBQU9FLEtBQUtDLEtBQUwsQ0FBWUQsS0FBS0UsR0FBTCxDQUFTLEVBQVQsRUFBYUosU0FBUyxDQUF0QixJQUEyQkUsS0FBS0csTUFBTCxLQUFnQkgsS0FBS0UsR0FBTCxDQUFTLEVBQVQsRUFBYUosTUFBYixDQUF2RCxFQUE4RU0sUUFBOUUsQ0FBdUYsRUFBdkYsRUFBMkZDLEtBQTNGLENBQWlHLENBQWpHLEtBQXVHTixrQkFBZ0JBLFNBQWhCLEdBQThCLEVBQXJJLENBQVA7QUFDRCxLQXRJYztBQXVJZjs7Ozs7QUFLQU8sWUFBUSxVQUFTQyxJQUFULEVBQWV2QixPQUFmLEVBQXdCOztBQUU5QjtBQUNBLFVBQUksT0FBT0EsT0FBUCxLQUFtQixXQUF2QixFQUFvQztBQUNsQ0Esa0JBQVVVLE9BQU94QyxJQUFQLENBQVksS0FBS08sUUFBakIsQ0FBVjtBQUNEO0FBQ0Q7QUFIQSxXQUlLLElBQUksT0FBT3VCLE9BQVAsS0FBbUIsUUFBdkIsRUFBaUM7QUFDcENBLG9CQUFVLENBQUNBLE9BQUQsQ0FBVjtBQUNEOztBQUVELFVBQUlJLFFBQVEsSUFBWjs7QUFFQTtBQUNBL0IsUUFBRTZCLElBQUYsQ0FBT0YsT0FBUCxFQUFnQixVQUFTd0IsQ0FBVCxFQUFZMUMsSUFBWixFQUFrQjtBQUNoQztBQUNBLFlBQUlELFNBQVN1QixNQUFNM0IsUUFBTixDQUFlSyxJQUFmLENBQWI7O0FBRUE7QUFDQSxZQUFJMkMsUUFBUXBELEVBQUVrRCxJQUFGLEVBQVFHLElBQVIsQ0FBYSxXQUFTNUMsSUFBVCxHQUFjLEdBQTNCLEVBQWdDNkMsT0FBaEMsQ0FBd0MsV0FBUzdDLElBQVQsR0FBYyxHQUF0RCxDQUFaOztBQUVBO0FBQ0EyQyxjQUFNdkIsSUFBTixDQUFXLFlBQVc7QUFDcEIsY0FBSTBCLE1BQU12RCxFQUFFLElBQUYsQ0FBVjtBQUFBLGNBQ0l3RCxPQUFPLEVBRFg7QUFFQTtBQUNBLGNBQUlELElBQUluQyxJQUFKLENBQVMsVUFBVCxDQUFKLEVBQTBCO0FBQ3hCbUIsb0JBQVFrQixJQUFSLENBQWEseUJBQXVCaEQsSUFBdkIsR0FBNEIsc0RBQXpDO0FBQ0E7QUFDRDs7QUFFRCxjQUFHOEMsSUFBSWhELElBQUosQ0FBUyxjQUFULENBQUgsRUFBNEI7QUFDMUIsZ0JBQUltRCxRQUFRSCxJQUFJaEQsSUFBSixDQUFTLGNBQVQsRUFBeUJvRCxLQUF6QixDQUErQixHQUEvQixFQUFvQ3pCLE9BQXBDLENBQTRDLFVBQVMwQixDQUFULEVBQVlULENBQVosRUFBYztBQUNwRSxrQkFBSVUsTUFBTUQsRUFBRUQsS0FBRixDQUFRLEdBQVIsRUFBYUcsR0FBYixDQUFpQixVQUFTQyxFQUFULEVBQVk7QUFBRSx1QkFBT0EsR0FBR0MsSUFBSCxFQUFQO0FBQW1CLGVBQWxELENBQVY7QUFDQSxrQkFBR0gsSUFBSSxDQUFKLENBQUgsRUFBV0wsS0FBS0ssSUFBSSxDQUFKLENBQUwsSUFBZUksV0FBV0osSUFBSSxDQUFKLENBQVgsQ0FBZjtBQUNaLGFBSFcsQ0FBWjtBQUlEO0FBQ0QsY0FBRztBQUNETixnQkFBSW5DLElBQUosQ0FBUyxVQUFULEVBQXFCLElBQUlaLE1BQUosQ0FBV1IsRUFBRSxJQUFGLENBQVgsRUFBb0J3RCxJQUFwQixDQUFyQjtBQUNELFdBRkQsQ0FFQyxPQUFNVSxFQUFOLEVBQVM7QUFDUjNCLG9CQUFRQyxLQUFSLENBQWMwQixFQUFkO0FBQ0QsV0FKRCxTQUlRO0FBQ047QUFDRDtBQUNGLFNBdEJEO0FBdUJELE9BL0JEO0FBZ0NELEtBMUxjO0FBMkxmQyxlQUFXeEQsWUEzTEk7QUE0TGZ5RCxtQkFBZSxVQUFTaEIsS0FBVCxFQUFlO0FBQzVCLFVBQUlpQixjQUFjO0FBQ2hCLHNCQUFjLGVBREU7QUFFaEIsNEJBQW9CLHFCQUZKO0FBR2hCLHlCQUFpQixlQUhEO0FBSWhCLHVCQUFlO0FBSkMsT0FBbEI7QUFNQSxVQUFJbkIsT0FBTy9ELFNBQVNJLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBWDtBQUFBLFVBQ0krRSxHQURKOztBQUdBLFdBQUssSUFBSUMsQ0FBVCxJQUFjRixXQUFkLEVBQTBCO0FBQ3hCLFlBQUksT0FBT25CLEtBQUtzQixLQUFMLENBQVdELENBQVgsQ0FBUCxLQUF5QixXQUE3QixFQUF5QztBQUN2Q0QsZ0JBQU1ELFlBQVlFLENBQVosQ0FBTjtBQUNEO0FBQ0Y7QUFDRCxVQUFHRCxHQUFILEVBQU87QUFDTCxlQUFPQSxHQUFQO0FBQ0QsT0FGRCxNQUVLO0FBQ0hBLGNBQU1qSCxXQUFXLFlBQVU7QUFDekIrRixnQkFBTXFCLGNBQU4sQ0FBcUIsZUFBckIsRUFBc0MsQ0FBQ3JCLEtBQUQsQ0FBdEM7QUFDRCxTQUZLLEVBRUgsQ0FGRyxDQUFOO0FBR0EsZUFBTyxlQUFQO0FBQ0Q7QUFDRjtBQW5OYyxHQUFqQjs7QUFzTkFsRCxhQUFXd0UsSUFBWCxHQUFrQjtBQUNoQjs7Ozs7OztBQU9BQyxjQUFVLFVBQVVDLElBQVYsRUFBZ0JDLEtBQWhCLEVBQXVCO0FBQy9CLFVBQUk3SCxRQUFRLElBQVo7O0FBRUEsYUFBTyxZQUFZO0FBQ2pCLFlBQUk4SCxVQUFVLElBQWQ7QUFBQSxZQUFvQkMsT0FBT0MsU0FBM0I7O0FBRUEsWUFBSWhJLFVBQVUsSUFBZCxFQUFvQjtBQUNsQkEsa0JBQVFLLFdBQVcsWUFBWTtBQUM3QnVILGlCQUFLSyxLQUFMLENBQVdILE9BQVgsRUFBb0JDLElBQXBCO0FBQ0EvSCxvQkFBUSxJQUFSO0FBQ0QsV0FITyxFQUdMNkgsS0FISyxDQUFSO0FBSUQ7QUFDRixPQVREO0FBVUQ7QUFyQmUsR0FBbEI7O0FBd0JBO0FBQ0E7QUFDQTs7OztBQUlBLE1BQUl6QyxhQUFhLFVBQVM4QyxNQUFULEVBQWlCO0FBQ2hDLFFBQUl0SCxPQUFPLE9BQU9zSCxNQUFsQjtBQUFBLFFBQ0lDLFFBQVFuRixFQUFFLG9CQUFGLENBRFo7QUFBQSxRQUVJb0YsUUFBUXBGLEVBQUUsUUFBRixDQUZaOztBQUlBLFFBQUcsQ0FBQ21GLE1BQU0xQyxNQUFWLEVBQWlCO0FBQ2Z6QyxRQUFFLDhCQUFGLEVBQWtDcUYsUUFBbEMsQ0FBMkNsRyxTQUFTbUcsSUFBcEQ7QUFDRDtBQUNELFFBQUdGLE1BQU0zQyxNQUFULEVBQWdCO0FBQ2QyQyxZQUFNRyxXQUFOLENBQWtCLE9BQWxCO0FBQ0Q7O0FBRUQsUUFBRzNILFNBQVMsV0FBWixFQUF3QjtBQUFDO0FBQ3ZCc0MsaUJBQVdzRixVQUFYLENBQXNCMUQsS0FBdEI7QUFDQTVCLGlCQUFXK0MsTUFBWCxDQUFrQixJQUFsQjtBQUNELEtBSEQsTUFHTSxJQUFHckYsU0FBUyxRQUFaLEVBQXFCO0FBQUM7QUFDMUIsVUFBSW1ILE9BQU9yRixNQUFNQyxTQUFOLENBQWdCcUQsS0FBaEIsQ0FBc0J5QyxJQUF0QixDQUEyQlQsU0FBM0IsRUFBc0MsQ0FBdEMsQ0FBWCxDQUR5QixDQUMyQjtBQUNwRCxVQUFJVSxZQUFZLEtBQUt0RSxJQUFMLENBQVUsVUFBVixDQUFoQixDQUZ5QixDQUVhOztBQUV0QyxVQUFHc0UsY0FBY2pHLFNBQWQsSUFBMkJpRyxVQUFVUixNQUFWLE1BQXNCekYsU0FBcEQsRUFBOEQ7QUFBQztBQUM3RCxZQUFHLEtBQUtnRCxNQUFMLEtBQWdCLENBQW5CLEVBQXFCO0FBQUM7QUFDbEJpRCxvQkFBVVIsTUFBVixFQUFrQkQsS0FBbEIsQ0FBd0JTLFNBQXhCLEVBQW1DWCxJQUFuQztBQUNILFNBRkQsTUFFSztBQUNILGVBQUtsRCxJQUFMLENBQVUsVUFBU3NCLENBQVQsRUFBWVksRUFBWixFQUFlO0FBQUM7QUFDeEIyQixzQkFBVVIsTUFBVixFQUFrQkQsS0FBbEIsQ0FBd0JqRixFQUFFK0QsRUFBRixFQUFNM0MsSUFBTixDQUFXLFVBQVgsQ0FBeEIsRUFBZ0QyRCxJQUFoRDtBQUNELFdBRkQ7QUFHRDtBQUNGLE9BUkQsTUFRSztBQUFDO0FBQ0osY0FBTSxJQUFJWSxjQUFKLENBQW1CLG1CQUFtQlQsTUFBbkIsR0FBNEIsbUNBQTVCLElBQW1FUSxZQUFZL0UsYUFBYStFLFNBQWIsQ0FBWixHQUFzQyxjQUF6RyxJQUEySCxHQUE5SSxDQUFOO0FBQ0Q7QUFDRixLQWZLLE1BZUQ7QUFBQztBQUNKLFlBQU0sSUFBSUUsU0FBSixvQkFBOEJoSSxJQUE5QixrR0FBTjtBQUNEO0FBQ0QsV0FBTyxJQUFQO0FBQ0QsR0FsQ0Q7O0FBb0NBMUIsU0FBT2dFLFVBQVAsR0FBb0JBLFVBQXBCO0FBQ0FGLElBQUU2RixFQUFGLENBQUt6RCxVQUFMLEdBQWtCQSxVQUFsQjs7QUFFQTtBQUNBLEdBQUMsWUFBVztBQUNWLFFBQUksQ0FBQzBELEtBQUtDLEdBQU4sSUFBYSxDQUFDN0osT0FBTzRKLElBQVAsQ0FBWUMsR0FBOUIsRUFDRTdKLE9BQU80SixJQUFQLENBQVlDLEdBQVosR0FBa0JELEtBQUtDLEdBQUwsR0FBVyxZQUFXO0FBQUUsYUFBTyxJQUFJRCxJQUFKLEdBQVdFLE9BQVgsRUFBUDtBQUE4QixLQUF4RTs7QUFFRixRQUFJQyxVQUFVLENBQUMsUUFBRCxFQUFXLEtBQVgsQ0FBZDtBQUNBLFNBQUssSUFBSTlDLElBQUksQ0FBYixFQUFnQkEsSUFBSThDLFFBQVF4RCxNQUFaLElBQXNCLENBQUN2RyxPQUFPZ0sscUJBQTlDLEVBQXFFLEVBQUUvQyxDQUF2RSxFQUEwRTtBQUN0RSxVQUFJZ0QsS0FBS0YsUUFBUTlDLENBQVIsQ0FBVDtBQUNBakgsYUFBT2dLLHFCQUFQLEdBQStCaEssT0FBT2lLLEtBQUcsdUJBQVYsQ0FBL0I7QUFDQWpLLGFBQU9rSyxvQkFBUCxHQUErQmxLLE9BQU9pSyxLQUFHLHNCQUFWLEtBQ0RqSyxPQUFPaUssS0FBRyw2QkFBVixDQUQ5QjtBQUVIO0FBQ0QsUUFBSSx1QkFBdUJFLElBQXZCLENBQTRCbkssT0FBT29LLFNBQVAsQ0FBaUJDLFNBQTdDLEtBQ0MsQ0FBQ3JLLE9BQU9nSyxxQkFEVCxJQUNrQyxDQUFDaEssT0FBT2tLLG9CQUQ5QyxFQUNvRTtBQUNsRSxVQUFJSSxXQUFXLENBQWY7QUFDQXRLLGFBQU9nSyxxQkFBUCxHQUErQixVQUFTTyxRQUFULEVBQW1CO0FBQzlDLFlBQUlWLE1BQU1ELEtBQUtDLEdBQUwsRUFBVjtBQUNBLFlBQUlXLFdBQVcvRCxLQUFLZ0UsR0FBTCxDQUFTSCxXQUFXLEVBQXBCLEVBQXdCVCxHQUF4QixDQUFmO0FBQ0EsZUFBTzFJLFdBQVcsWUFBVztBQUFFb0osbUJBQVNELFdBQVdFLFFBQXBCO0FBQWdDLFNBQXhELEVBQ1dBLFdBQVdYLEdBRHRCLENBQVA7QUFFSCxPQUxEO0FBTUE3SixhQUFPa0ssb0JBQVAsR0FBOEI1SSxZQUE5QjtBQUNEO0FBQ0Q7OztBQUdBLFFBQUcsQ0FBQ3RCLE9BQU8wSyxXQUFSLElBQXVCLENBQUMxSyxPQUFPMEssV0FBUCxDQUFtQmIsR0FBOUMsRUFBa0Q7QUFDaEQ3SixhQUFPMEssV0FBUCxHQUFxQjtBQUNuQkMsZUFBT2YsS0FBS0MsR0FBTCxFQURZO0FBRW5CQSxhQUFLLFlBQVU7QUFBRSxpQkFBT0QsS0FBS0MsR0FBTCxLQUFhLEtBQUtjLEtBQXpCO0FBQWlDO0FBRi9CLE9BQXJCO0FBSUQ7QUFDRixHQS9CRDtBQWdDQSxNQUFJLENBQUNDLFNBQVNuSCxTQUFULENBQW1Cb0gsSUFBeEIsRUFBOEI7QUFDNUJELGFBQVNuSCxTQUFULENBQW1Cb0gsSUFBbkIsR0FBMEIsVUFBU0MsS0FBVCxFQUFnQjtBQUN4QyxVQUFJLE9BQU8sSUFBUCxLQUFnQixVQUFwQixFQUFnQztBQUM5QjtBQUNBO0FBQ0EsY0FBTSxJQUFJcEIsU0FBSixDQUFjLHNFQUFkLENBQU47QUFDRDs7QUFFRCxVQUFJcUIsUUFBVXZILE1BQU1DLFNBQU4sQ0FBZ0JxRCxLQUFoQixDQUFzQnlDLElBQXRCLENBQTJCVCxTQUEzQixFQUFzQyxDQUF0QyxDQUFkO0FBQUEsVUFDSWtDLFVBQVUsSUFEZDtBQUFBLFVBRUlDLE9BQVUsWUFBVyxDQUFFLENBRjNCO0FBQUEsVUFHSUMsU0FBVSxZQUFXO0FBQ25CLGVBQU9GLFFBQVFqQyxLQUFSLENBQWMsZ0JBQWdCa0MsSUFBaEIsR0FDWixJQURZLEdBRVpILEtBRkYsRUFHQUMsTUFBTUksTUFBTixDQUFhM0gsTUFBTUMsU0FBTixDQUFnQnFELEtBQWhCLENBQXNCeUMsSUFBdEIsQ0FBMkJULFNBQTNCLENBQWIsQ0FIQSxDQUFQO0FBSUQsT0FSTDs7QUFVQSxVQUFJLEtBQUtyRixTQUFULEVBQW9CO0FBQ2xCO0FBQ0F3SCxhQUFLeEgsU0FBTCxHQUFpQixLQUFLQSxTQUF0QjtBQUNEO0FBQ0R5SCxhQUFPekgsU0FBUCxHQUFtQixJQUFJd0gsSUFBSixFQUFuQjs7QUFFQSxhQUFPQyxNQUFQO0FBQ0QsS0F4QkQ7QUF5QkQ7QUFDRDtBQUNBLFdBQVN6RyxZQUFULENBQXNCa0YsRUFBdEIsRUFBMEI7QUFDeEIsUUFBSWlCLFNBQVNuSCxTQUFULENBQW1CYyxJQUFuQixLQUE0QmhCLFNBQWhDLEVBQTJDO0FBQ3pDLFVBQUk2SCxnQkFBZ0Isd0JBQXBCO0FBQ0EsVUFBSUMsVUFBV0QsYUFBRCxDQUFnQkUsSUFBaEIsQ0FBc0IzQixFQUFELENBQUs5QyxRQUFMLEVBQXJCLENBQWQ7QUFDQSxhQUFRd0UsV0FBV0EsUUFBUTlFLE1BQVIsR0FBaUIsQ0FBN0IsR0FBa0M4RSxRQUFRLENBQVIsRUFBV3ZELElBQVgsRUFBbEMsR0FBc0QsRUFBN0Q7QUFDRCxLQUpELE1BS0ssSUFBSTZCLEdBQUdsRyxTQUFILEtBQWlCRixTQUFyQixFQUFnQztBQUNuQyxhQUFPb0csR0FBRzdFLFdBQUgsQ0FBZVAsSUFBdEI7QUFDRCxLQUZJLE1BR0E7QUFDSCxhQUFPb0YsR0FBR2xHLFNBQUgsQ0FBYXFCLFdBQWIsQ0FBeUJQLElBQWhDO0FBQ0Q7QUFDRjtBQUNELFdBQVN3RCxVQUFULENBQW9Cd0QsR0FBcEIsRUFBd0I7QUFDdEIsUUFBRyxPQUFPcEIsSUFBUCxDQUFZb0IsR0FBWixDQUFILEVBQXFCLE9BQU8sSUFBUCxDQUFyQixLQUNLLElBQUcsUUFBUXBCLElBQVIsQ0FBYW9CLEdBQWIsQ0FBSCxFQUFzQixPQUFPLEtBQVAsQ0FBdEIsS0FDQSxJQUFHLENBQUNDLE1BQU1ELE1BQU0sQ0FBWixDQUFKLEVBQW9CLE9BQU9FLFdBQVdGLEdBQVgsQ0FBUDtBQUN6QixXQUFPQSxHQUFQO0FBQ0Q7QUFDRDtBQUNBO0FBQ0EsV0FBUzVHLFNBQVQsQ0FBbUI0RyxHQUFuQixFQUF3QjtBQUN0QixXQUFPQSxJQUFJRyxPQUFKLENBQVksaUJBQVosRUFBK0IsT0FBL0IsRUFBd0MxSixXQUF4QyxFQUFQO0FBQ0Q7QUFFQSxDQXpYQSxDQXlYQzJKLE1BelhELENBQUQ7Q0NBQTs7QUFFQSxDQUFDLFVBQVM3SCxDQUFULEVBQVk7O0FBRWJFLGFBQVc0SCxHQUFYLEdBQWlCO0FBQ2ZDLHNCQUFrQkEsZ0JBREg7QUFFZkMsbUJBQWVBLGFBRkE7QUFHZkMsZ0JBQVlBOztBQUdkOzs7Ozs7Ozs7O0FBTmlCLEdBQWpCLENBZ0JBLFNBQVNGLGdCQUFULENBQTBCRyxPQUExQixFQUFtQ0MsTUFBbkMsRUFBMkNDLE1BQTNDLEVBQW1EQyxNQUFuRCxFQUEyRDtBQUN6RCxRQUFJQyxVQUFVTixjQUFjRSxPQUFkLENBQWQ7QUFBQSxRQUNJSyxHQURKO0FBQUEsUUFDU0MsTUFEVDtBQUFBLFFBQ2lCQyxJQURqQjtBQUFBLFFBQ3VCQyxLQUR2Qjs7QUFHQSxRQUFJUCxNQUFKLEVBQVk7QUFDVixVQUFJUSxVQUFVWCxjQUFjRyxNQUFkLENBQWQ7O0FBRUFLLGVBQVVGLFFBQVFNLE1BQVIsQ0FBZUwsR0FBZixHQUFxQkQsUUFBUU8sTUFBN0IsSUFBdUNGLFFBQVFFLE1BQVIsR0FBaUJGLFFBQVFDLE1BQVIsQ0FBZUwsR0FBakY7QUFDQUEsWUFBVUQsUUFBUU0sTUFBUixDQUFlTCxHQUFmLElBQXNCSSxRQUFRQyxNQUFSLENBQWVMLEdBQS9DO0FBQ0FFLGFBQVVILFFBQVFNLE1BQVIsQ0FBZUgsSUFBZixJQUF1QkUsUUFBUUMsTUFBUixDQUFlSCxJQUFoRDtBQUNBQyxjQUFVSixRQUFRTSxNQUFSLENBQWVILElBQWYsR0FBc0JILFFBQVFRLEtBQTlCLElBQXVDSCxRQUFRRyxLQUFSLEdBQWdCSCxRQUFRQyxNQUFSLENBQWVILElBQWhGO0FBQ0QsS0FQRCxNQVFLO0FBQ0hELGVBQVVGLFFBQVFNLE1BQVIsQ0FBZUwsR0FBZixHQUFxQkQsUUFBUU8sTUFBN0IsSUFBdUNQLFFBQVFTLFVBQVIsQ0FBbUJGLE1BQW5CLEdBQTRCUCxRQUFRUyxVQUFSLENBQW1CSCxNQUFuQixDQUEwQkwsR0FBdkc7QUFDQUEsWUFBVUQsUUFBUU0sTUFBUixDQUFlTCxHQUFmLElBQXNCRCxRQUFRUyxVQUFSLENBQW1CSCxNQUFuQixDQUEwQkwsR0FBMUQ7QUFDQUUsYUFBVUgsUUFBUU0sTUFBUixDQUFlSCxJQUFmLElBQXVCSCxRQUFRUyxVQUFSLENBQW1CSCxNQUFuQixDQUEwQkgsSUFBM0Q7QUFDQUMsY0FBVUosUUFBUU0sTUFBUixDQUFlSCxJQUFmLEdBQXNCSCxRQUFRUSxLQUE5QixJQUF1Q1IsUUFBUVMsVUFBUixDQUFtQkQsS0FBcEU7QUFDRDs7QUFFRCxRQUFJRSxVQUFVLENBQUNSLE1BQUQsRUFBU0QsR0FBVCxFQUFjRSxJQUFkLEVBQW9CQyxLQUFwQixDQUFkOztBQUVBLFFBQUlOLE1BQUosRUFBWTtBQUNWLGFBQU9LLFNBQVNDLEtBQVQsS0FBbUIsSUFBMUI7QUFDRDs7QUFFRCxRQUFJTCxNQUFKLEVBQVk7QUFDVixhQUFPRSxRQUFRQyxNQUFSLEtBQW1CLElBQTFCO0FBQ0Q7O0FBRUQsV0FBT1EsUUFBUTFLLE9BQVIsQ0FBZ0IsS0FBaEIsTUFBMkIsQ0FBQyxDQUFuQztBQUNEOztBQUVEOzs7Ozs7O0FBT0EsV0FBUzBKLGFBQVQsQ0FBdUI5RSxJQUF2QixFQUE2Qm1ELElBQTdCLEVBQWtDO0FBQ2hDbkQsV0FBT0EsS0FBS1QsTUFBTCxHQUFjUyxLQUFLLENBQUwsQ0FBZCxHQUF3QkEsSUFBL0I7O0FBRUEsUUFBSUEsU0FBU2hILE1BQVQsSUFBbUJnSCxTQUFTL0QsUUFBaEMsRUFBMEM7QUFDeEMsWUFBTSxJQUFJOEosS0FBSixDQUFVLDhDQUFWLENBQU47QUFDRDs7QUFFRCxRQUFJQyxPQUFPaEcsS0FBS2lHLHFCQUFMLEVBQVg7QUFBQSxRQUNJQyxVQUFVbEcsS0FBS21HLFVBQUwsQ0FBZ0JGLHFCQUFoQixFQURkO0FBQUEsUUFFSUcsVUFBVW5LLFNBQVM5QyxJQUFULENBQWM4TSxxQkFBZCxFQUZkO0FBQUEsUUFHSUksT0FBT3JOLE9BQU9zTixXQUhsQjtBQUFBLFFBSUlDLE9BQU92TixPQUFPd04sV0FKbEI7O0FBTUEsV0FBTztBQUNMWixhQUFPSSxLQUFLSixLQURQO0FBRUxELGNBQVFLLEtBQUtMLE1BRlI7QUFHTEQsY0FBUTtBQUNOTCxhQUFLVyxLQUFLWCxHQUFMLEdBQVdnQixJQURWO0FBRU5kLGNBQU1TLEtBQUtULElBQUwsR0FBWWdCO0FBRlosT0FISDtBQU9MRSxrQkFBWTtBQUNWYixlQUFPTSxRQUFRTixLQURMO0FBRVZELGdCQUFRTyxRQUFRUCxNQUZOO0FBR1ZELGdCQUFRO0FBQ05MLGVBQUthLFFBQVFiLEdBQVIsR0FBY2dCLElBRGI7QUFFTmQsZ0JBQU1XLFFBQVFYLElBQVIsR0FBZWdCO0FBRmY7QUFIRSxPQVBQO0FBZUxWLGtCQUFZO0FBQ1ZELGVBQU9RLFFBQVFSLEtBREw7QUFFVkQsZ0JBQVFTLFFBQVFULE1BRk47QUFHVkQsZ0JBQVE7QUFDTkwsZUFBS2dCLElBREM7QUFFTmQsZ0JBQU1nQjtBQUZBO0FBSEU7QUFmUCxLQUFQO0FBd0JEOztBQUVEOzs7Ozs7Ozs7Ozs7QUFZQSxXQUFTeEIsVUFBVCxDQUFvQkMsT0FBcEIsRUFBNkIwQixNQUE3QixFQUFxQ0MsUUFBckMsRUFBK0NDLE9BQS9DLEVBQXdEQyxPQUF4RCxFQUFpRUMsVUFBakUsRUFBNkU7QUFDM0UsUUFBSUMsV0FBV2pDLGNBQWNFLE9BQWQsQ0FBZjtBQUFBLFFBQ0lnQyxjQUFjTixTQUFTNUIsY0FBYzRCLE1BQWQsQ0FBVCxHQUFpQyxJQURuRDs7QUFHQSxZQUFRQyxRQUFSO0FBQ0UsV0FBSyxLQUFMO0FBQ0UsZUFBTztBQUNMcEIsZ0JBQU92SSxXQUFXSSxHQUFYLEtBQW1CNEosWUFBWXRCLE1BQVosQ0FBbUJILElBQW5CLEdBQTBCd0IsU0FBU25CLEtBQW5DLEdBQTJDb0IsWUFBWXBCLEtBQTFFLEdBQWtGb0IsWUFBWXRCLE1BQVosQ0FBbUJILElBRHZHO0FBRUxGLGVBQUsyQixZQUFZdEIsTUFBWixDQUFtQkwsR0FBbkIsSUFBMEIwQixTQUFTcEIsTUFBVCxHQUFrQmlCLE9BQTVDO0FBRkEsU0FBUDtBQUlBO0FBQ0YsV0FBSyxNQUFMO0FBQ0UsZUFBTztBQUNMckIsZ0JBQU15QixZQUFZdEIsTUFBWixDQUFtQkgsSUFBbkIsSUFBMkJ3QixTQUFTbkIsS0FBVCxHQUFpQmlCLE9BQTVDLENBREQ7QUFFTHhCLGVBQUsyQixZQUFZdEIsTUFBWixDQUFtQkw7QUFGbkIsU0FBUDtBQUlBO0FBQ0YsV0FBSyxPQUFMO0FBQ0UsZUFBTztBQUNMRSxnQkFBTXlCLFlBQVl0QixNQUFaLENBQW1CSCxJQUFuQixHQUEwQnlCLFlBQVlwQixLQUF0QyxHQUE4Q2lCLE9BRC9DO0FBRUx4QixlQUFLMkIsWUFBWXRCLE1BQVosQ0FBbUJMO0FBRm5CLFNBQVA7QUFJQTtBQUNGLFdBQUssWUFBTDtBQUNFLGVBQU87QUFDTEUsZ0JBQU95QixZQUFZdEIsTUFBWixDQUFtQkgsSUFBbkIsR0FBMkJ5QixZQUFZcEIsS0FBWixHQUFvQixDQUFoRCxHQUF1RG1CLFNBQVNuQixLQUFULEdBQWlCLENBRHpFO0FBRUxQLGVBQUsyQixZQUFZdEIsTUFBWixDQUFtQkwsR0FBbkIsSUFBMEIwQixTQUFTcEIsTUFBVCxHQUFrQmlCLE9BQTVDO0FBRkEsU0FBUDtBQUlBO0FBQ0YsV0FBSyxlQUFMO0FBQ0UsZUFBTztBQUNMckIsZ0JBQU11QixhQUFhRCxPQUFiLEdBQXlCRyxZQUFZdEIsTUFBWixDQUFtQkgsSUFBbkIsR0FBMkJ5QixZQUFZcEIsS0FBWixHQUFvQixDQUFoRCxHQUF1RG1CLFNBQVNuQixLQUFULEdBQWlCLENBRGpHO0FBRUxQLGVBQUsyQixZQUFZdEIsTUFBWixDQUFtQkwsR0FBbkIsR0FBeUIyQixZQUFZckIsTUFBckMsR0FBOENpQjtBQUY5QyxTQUFQO0FBSUE7QUFDRixXQUFLLGFBQUw7QUFDRSxlQUFPO0FBQ0xyQixnQkFBTXlCLFlBQVl0QixNQUFaLENBQW1CSCxJQUFuQixJQUEyQndCLFNBQVNuQixLQUFULEdBQWlCaUIsT0FBNUMsQ0FERDtBQUVMeEIsZUFBTTJCLFlBQVl0QixNQUFaLENBQW1CTCxHQUFuQixHQUEwQjJCLFlBQVlyQixNQUFaLEdBQXFCLENBQWhELEdBQXVEb0IsU0FBU3BCLE1BQVQsR0FBa0I7QUFGekUsU0FBUDtBQUlBO0FBQ0YsV0FBSyxjQUFMO0FBQ0UsZUFBTztBQUNMSixnQkFBTXlCLFlBQVl0QixNQUFaLENBQW1CSCxJQUFuQixHQUEwQnlCLFlBQVlwQixLQUF0QyxHQUE4Q2lCLE9BQTlDLEdBQXdELENBRHpEO0FBRUx4QixlQUFNMkIsWUFBWXRCLE1BQVosQ0FBbUJMLEdBQW5CLEdBQTBCMkIsWUFBWXJCLE1BQVosR0FBcUIsQ0FBaEQsR0FBdURvQixTQUFTcEIsTUFBVCxHQUFrQjtBQUZ6RSxTQUFQO0FBSUE7QUFDRixXQUFLLFFBQUw7QUFDRSxlQUFPO0FBQ0xKLGdCQUFPd0IsU0FBU2xCLFVBQVQsQ0FBb0JILE1BQXBCLENBQTJCSCxJQUEzQixHQUFtQ3dCLFNBQVNsQixVQUFULENBQW9CRCxLQUFwQixHQUE0QixDQUFoRSxHQUF1RW1CLFNBQVNuQixLQUFULEdBQWlCLENBRHpGO0FBRUxQLGVBQU0wQixTQUFTbEIsVUFBVCxDQUFvQkgsTUFBcEIsQ0FBMkJMLEdBQTNCLEdBQWtDMEIsU0FBU2xCLFVBQVQsQ0FBb0JGLE1BQXBCLEdBQTZCLENBQWhFLEdBQXVFb0IsU0FBU3BCLE1BQVQsR0FBa0I7QUFGekYsU0FBUDtBQUlBO0FBQ0YsV0FBSyxRQUFMO0FBQ0UsZUFBTztBQUNMSixnQkFBTSxDQUFDd0IsU0FBU2xCLFVBQVQsQ0FBb0JELEtBQXBCLEdBQTRCbUIsU0FBU25CLEtBQXRDLElBQStDLENBRGhEO0FBRUxQLGVBQUswQixTQUFTbEIsVUFBVCxDQUFvQkgsTUFBcEIsQ0FBMkJMLEdBQTNCLEdBQWlDdUI7QUFGakMsU0FBUDtBQUlGLFdBQUssYUFBTDtBQUNFLGVBQU87QUFDTHJCLGdCQUFNd0IsU0FBU2xCLFVBQVQsQ0FBb0JILE1BQXBCLENBQTJCSCxJQUQ1QjtBQUVMRixlQUFLMEIsU0FBU2xCLFVBQVQsQ0FBb0JILE1BQXBCLENBQTJCTDtBQUYzQixTQUFQO0FBSUE7QUFDRixXQUFLLGFBQUw7QUFDRSxlQUFPO0FBQ0xFLGdCQUFNeUIsWUFBWXRCLE1BQVosQ0FBbUJILElBRHBCO0FBRUxGLGVBQUsyQixZQUFZdEIsTUFBWixDQUFtQkwsR0FBbkIsR0FBeUIyQixZQUFZckI7QUFGckMsU0FBUDtBQUlBO0FBQ0YsV0FBSyxjQUFMO0FBQ0UsZUFBTztBQUNMSixnQkFBTXlCLFlBQVl0QixNQUFaLENBQW1CSCxJQUFuQixHQUEwQnlCLFlBQVlwQixLQUF0QyxHQUE4Q2lCLE9BQTlDLEdBQXdERSxTQUFTbkIsS0FEbEU7QUFFTFAsZUFBSzJCLFlBQVl0QixNQUFaLENBQW1CTCxHQUFuQixHQUF5QjJCLFlBQVlyQjtBQUZyQyxTQUFQO0FBSUE7QUFDRjtBQUNFLGVBQU87QUFDTEosZ0JBQU92SSxXQUFXSSxHQUFYLEtBQW1CNEosWUFBWXRCLE1BQVosQ0FBbUJILElBQW5CLEdBQTBCd0IsU0FBU25CLEtBQW5DLEdBQTJDb0IsWUFBWXBCLEtBQTFFLEdBQWtGb0IsWUFBWXRCLE1BQVosQ0FBbUJILElBQW5CLEdBQTBCc0IsT0FEOUc7QUFFTHhCLGVBQUsyQixZQUFZdEIsTUFBWixDQUFtQkwsR0FBbkIsR0FBeUIyQixZQUFZckIsTUFBckMsR0FBOENpQjtBQUY5QyxTQUFQO0FBekVKO0FBOEVEO0FBRUEsQ0FoTUEsQ0FnTUNqQyxNQWhNRCxDQUFEO0NDRkE7Ozs7Ozs7O0FBUUE7O0FBRUEsQ0FBQyxVQUFTN0gsQ0FBVCxFQUFZOztBQUViLE1BQU1tSyxXQUFXO0FBQ2YsT0FBRyxLQURZO0FBRWYsUUFBSSxPQUZXO0FBR2YsUUFBSSxRQUhXO0FBSWYsUUFBSSxPQUpXO0FBS2YsUUFBSSxZQUxXO0FBTWYsUUFBSSxVQU5XO0FBT2YsUUFBSSxhQVBXO0FBUWYsUUFBSTtBQVJXLEdBQWpCOztBQVdBLE1BQUlDLFdBQVcsRUFBZjs7QUFFQSxNQUFJQyxXQUFXO0FBQ2J4SyxVQUFNeUssWUFBWUgsUUFBWixDQURPOztBQUdiOzs7Ozs7QUFNQUksWUFUYSxZQVNKbk4sS0FUSSxFQVNHO0FBQ2QsVUFBSU0sTUFBTXlNLFNBQVMvTSxNQUFNeUIsS0FBTixJQUFlekIsTUFBTXdCLE9BQTlCLEtBQTBDNEwsT0FBT0MsWUFBUCxDQUFvQnJOLE1BQU15QixLQUExQixFQUFpQzZMLFdBQWpDLEVBQXBEO0FBQ0EsVUFBSXROLE1BQU11TixRQUFWLEVBQW9Cak4saUJBQWVBLEdBQWY7QUFDcEIsVUFBSU4sTUFBTXdOLE9BQVYsRUFBbUJsTixnQkFBY0EsR0FBZDtBQUNuQixVQUFJTixNQUFNeU4sTUFBVixFQUFrQm5OLGVBQWFBLEdBQWI7QUFDbEIsYUFBT0EsR0FBUDtBQUNELEtBZlk7OztBQWlCYjs7Ozs7O0FBTUFvTixhQXZCYSxZQXVCSDFOLEtBdkJHLEVBdUJJMk4sU0F2QkosRUF1QmVDLFNBdkJmLEVBdUIwQjtBQUNyQyxVQUFJQyxjQUFjYixTQUFTVyxTQUFULENBQWxCO0FBQUEsVUFDRW5NLFVBQVUsS0FBSzJMLFFBQUwsQ0FBY25OLEtBQWQsQ0FEWjtBQUFBLFVBRUU4TixJQUZGO0FBQUEsVUFHRUMsT0FIRjtBQUFBLFVBSUV0RixFQUpGOztBQU1BLFVBQUksQ0FBQ29GLFdBQUwsRUFBa0IsT0FBTzFJLFFBQVFrQixJQUFSLENBQWEsd0JBQWIsQ0FBUDs7QUFFbEIsVUFBSSxPQUFPd0gsWUFBWUcsR0FBbkIsS0FBMkIsV0FBL0IsRUFBNEM7QUFBRTtBQUMxQ0YsZUFBT0QsV0FBUCxDQUR3QyxDQUNwQjtBQUN2QixPQUZELE1BRU87QUFBRTtBQUNMLFlBQUkvSyxXQUFXSSxHQUFYLEVBQUosRUFBc0I0SyxPQUFPbEwsRUFBRXFMLE1BQUYsQ0FBUyxFQUFULEVBQWFKLFlBQVlHLEdBQXpCLEVBQThCSCxZQUFZM0ssR0FBMUMsQ0FBUCxDQUF0QixLQUVLNEssT0FBT2xMLEVBQUVxTCxNQUFGLENBQVMsRUFBVCxFQUFhSixZQUFZM0ssR0FBekIsRUFBOEIySyxZQUFZRyxHQUExQyxDQUFQO0FBQ1I7QUFDREQsZ0JBQVVELEtBQUt0TSxPQUFMLENBQVY7O0FBRUFpSCxXQUFLbUYsVUFBVUcsT0FBVixDQUFMO0FBQ0EsVUFBSXRGLE1BQU0sT0FBT0EsRUFBUCxLQUFjLFVBQXhCLEVBQW9DO0FBQUU7QUFDcEMsWUFBSXlGLGNBQWN6RixHQUFHWixLQUFILEVBQWxCO0FBQ0EsWUFBSStGLFVBQVVPLE9BQVYsSUFBcUIsT0FBT1AsVUFBVU8sT0FBakIsS0FBNkIsVUFBdEQsRUFBa0U7QUFBRTtBQUNoRVAsb0JBQVVPLE9BQVYsQ0FBa0JELFdBQWxCO0FBQ0g7QUFDRixPQUxELE1BS087QUFDTCxZQUFJTixVQUFVUSxTQUFWLElBQXVCLE9BQU9SLFVBQVVRLFNBQWpCLEtBQStCLFVBQTFELEVBQXNFO0FBQUU7QUFDcEVSLG9CQUFVUSxTQUFWO0FBQ0g7QUFDRjtBQUNGLEtBcERZOzs7QUFzRGI7Ozs7O0FBS0FDLGlCQTNEYSxZQTJEQ3RLLFFBM0RELEVBMkRXO0FBQ3RCLGFBQU9BLFNBQVNrQyxJQUFULENBQWMsOEtBQWQsRUFBOExxSSxNQUE5TCxDQUFxTSxZQUFXO0FBQ3JOLFlBQUksQ0FBQzFMLEVBQUUsSUFBRixFQUFRMkwsRUFBUixDQUFXLFVBQVgsQ0FBRCxJQUEyQjNMLEVBQUUsSUFBRixFQUFRTyxJQUFSLENBQWEsVUFBYixJQUEyQixDQUExRCxFQUE2RDtBQUFFLGlCQUFPLEtBQVA7QUFBZSxTQUR1SSxDQUN0STtBQUMvRSxlQUFPLElBQVA7QUFDRCxPQUhNLENBQVA7QUFJRCxLQWhFWTs7O0FBa0ViOzs7Ozs7QUFNQXFMLFlBeEVhLFlBd0VKQyxhQXhFSSxFQXdFV1gsSUF4RVgsRUF3RWlCO0FBQzVCZCxlQUFTeUIsYUFBVCxJQUEwQlgsSUFBMUI7QUFDRDtBQTFFWSxHQUFmOztBQTZFQTs7OztBQUlBLFdBQVNaLFdBQVQsQ0FBcUJ3QixHQUFyQixFQUEwQjtBQUN4QixRQUFJQyxJQUFJLEVBQVI7QUFDQSxTQUFLLElBQUlDLEVBQVQsSUFBZUYsR0FBZjtBQUFvQkMsUUFBRUQsSUFBSUUsRUFBSixDQUFGLElBQWFGLElBQUlFLEVBQUosQ0FBYjtBQUFwQixLQUNBLE9BQU9ELENBQVA7QUFDRDs7QUFFRDdMLGFBQVdtSyxRQUFYLEdBQXNCQSxRQUF0QjtBQUVDLENBeEdBLENBd0dDeEMsTUF4R0QsQ0FBRDtDQ1ZBOztBQUVBLENBQUMsVUFBUzdILENBQVQsRUFBWTs7QUFFYjtBQUNBLE1BQU1pTSxpQkFBaUI7QUFDckIsZUFBWSxhQURTO0FBRXJCQyxlQUFZLDBDQUZTO0FBR3JCQyxjQUFXLHlDQUhVO0FBSXJCQyxZQUFTLHlEQUNQLG1EQURPLEdBRVAsbURBRk8sR0FHUCw4Q0FITyxHQUlQLDJDQUpPLEdBS1A7QUFUbUIsR0FBdkI7O0FBWUEsTUFBSTVHLGFBQWE7QUFDZjZHLGFBQVMsRUFETTs7QUFHZkMsYUFBUyxFQUhNOztBQUtmOzs7OztBQUtBeEssU0FWZSxjQVVQO0FBQ04sVUFBSXlLLE9BQU8sSUFBWDtBQUNBLFVBQUlDLGtCQUFrQnhNLEVBQUUsZ0JBQUYsRUFBb0J5TSxHQUFwQixDQUF3QixhQUF4QixDQUF0QjtBQUNBLFVBQUlDLFlBQUo7O0FBRUFBLHFCQUFlQyxtQkFBbUJILGVBQW5CLENBQWY7O0FBRUEsV0FBSyxJQUFJOU8sR0FBVCxJQUFnQmdQLFlBQWhCLEVBQThCO0FBQzVCLFlBQUdBLGFBQWFFLGNBQWIsQ0FBNEJsUCxHQUE1QixDQUFILEVBQXFDO0FBQ25DNk8sZUFBS0YsT0FBTCxDQUFhMU4sSUFBYixDQUFrQjtBQUNoQjhCLGtCQUFNL0MsR0FEVTtBQUVoQkMsb0RBQXNDK08sYUFBYWhQLEdBQWIsQ0FBdEM7QUFGZ0IsV0FBbEI7QUFJRDtBQUNGOztBQUVELFdBQUs0TyxPQUFMLEdBQWUsS0FBS08sZUFBTCxFQUFmOztBQUVBLFdBQUtDLFFBQUw7QUFDRCxLQTdCYzs7O0FBK0JmOzs7Ozs7QUFNQUMsV0FyQ2UsWUFxQ1BDLElBckNPLEVBcUNEO0FBQ1osVUFBSUMsUUFBUSxLQUFLQyxHQUFMLENBQVNGLElBQVQsQ0FBWjs7QUFFQSxVQUFJQyxLQUFKLEVBQVc7QUFDVCxlQUFPL1EsT0FBT2lSLFVBQVAsQ0FBa0JGLEtBQWxCLEVBQXlCRyxPQUFoQztBQUNEOztBQUVELGFBQU8sS0FBUDtBQUNELEtBN0NjOzs7QUErQ2Y7Ozs7OztBQU1BRixPQXJEZSxZQXFEWEYsSUFyRFcsRUFxREw7QUFDUixXQUFLLElBQUk3SixDQUFULElBQWMsS0FBS2tKLE9BQW5CLEVBQTRCO0FBQzFCLFlBQUcsS0FBS0EsT0FBTCxDQUFhTyxjQUFiLENBQTRCekosQ0FBNUIsQ0FBSCxFQUFtQztBQUNqQyxjQUFJOEosUUFBUSxLQUFLWixPQUFMLENBQWFsSixDQUFiLENBQVo7QUFDQSxjQUFJNkosU0FBU0MsTUFBTXhNLElBQW5CLEVBQXlCLE9BQU93TSxNQUFNdFAsS0FBYjtBQUMxQjtBQUNGOztBQUVELGFBQU8sSUFBUDtBQUNELEtBOURjOzs7QUFnRWY7Ozs7OztBQU1Ba1AsbUJBdEVlLGNBc0VHO0FBQ2hCLFVBQUlRLE9BQUo7O0FBRUEsV0FBSyxJQUFJbEssSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtrSixPQUFMLENBQWE1SixNQUFqQyxFQUF5Q1UsR0FBekMsRUFBOEM7QUFDNUMsWUFBSThKLFFBQVEsS0FBS1osT0FBTCxDQUFhbEosQ0FBYixDQUFaOztBQUVBLFlBQUlqSCxPQUFPaVIsVUFBUCxDQUFrQkYsTUFBTXRQLEtBQXhCLEVBQStCeVAsT0FBbkMsRUFBNEM7QUFDMUNDLG9CQUFVSixLQUFWO0FBQ0Q7QUFDRjs7QUFFRCxVQUFJLE9BQU9JLE9BQVAsS0FBbUIsUUFBdkIsRUFBaUM7QUFDL0IsZUFBT0EsUUFBUTVNLElBQWY7QUFDRCxPQUZELE1BRU87QUFDTCxlQUFPNE0sT0FBUDtBQUNEO0FBQ0YsS0F0RmM7OztBQXdGZjs7Ozs7QUFLQVAsWUE3RmUsY0E2Rko7QUFBQTs7QUFDVDlNLFFBQUU5RCxNQUFGLEVBQVVvUixFQUFWLENBQWEsc0JBQWIsRUFBcUMsWUFBTTtBQUN6QyxZQUFJQyxVQUFVLE1BQUtWLGVBQUwsRUFBZDtBQUFBLFlBQXNDVyxjQUFjLE1BQUtsQixPQUF6RDs7QUFFQSxZQUFJaUIsWUFBWUMsV0FBaEIsRUFBNkI7QUFDM0I7QUFDQSxnQkFBS2xCLE9BQUwsR0FBZWlCLE9BQWY7O0FBRUE7QUFDQXZOLFlBQUU5RCxNQUFGLEVBQVVtRixPQUFWLENBQWtCLHVCQUFsQixFQUEyQyxDQUFDa00sT0FBRCxFQUFVQyxXQUFWLENBQTNDO0FBQ0Q7QUFDRixPQVZEO0FBV0Q7QUF6R2MsR0FBakI7O0FBNEdBdE4sYUFBV3NGLFVBQVgsR0FBd0JBLFVBQXhCOztBQUVBO0FBQ0E7QUFDQXRKLFNBQU9pUixVQUFQLEtBQXNCalIsT0FBT2lSLFVBQVAsR0FBb0IsWUFBVztBQUNuRDs7QUFFQTs7QUFDQSxRQUFJTSxhQUFjdlIsT0FBT3VSLFVBQVAsSUFBcUJ2UixPQUFPd1IsS0FBOUM7O0FBRUE7QUFDQSxRQUFJLENBQUNELFVBQUwsRUFBaUI7QUFDZixVQUFJakosUUFBVXJGLFNBQVNJLGFBQVQsQ0FBdUIsT0FBdkIsQ0FBZDtBQUFBLFVBQ0FvTyxTQUFjeE8sU0FBU3lPLG9CQUFULENBQThCLFFBQTlCLEVBQXdDLENBQXhDLENBRGQ7QUFBQSxVQUVBQyxPQUFjLElBRmQ7O0FBSUFySixZQUFNNUcsSUFBTixHQUFjLFVBQWQ7QUFDQTRHLFlBQU1zSixFQUFOLEdBQWMsbUJBQWQ7O0FBRUFILGdCQUFVQSxPQUFPdEUsVUFBakIsSUFBK0JzRSxPQUFPdEUsVUFBUCxDQUFrQjBFLFlBQWxCLENBQStCdkosS0FBL0IsRUFBc0NtSixNQUF0QyxDQUEvQjs7QUFFQTtBQUNBRSxhQUFRLHNCQUFzQjNSLE1BQXZCLElBQWtDQSxPQUFPOFIsZ0JBQVAsQ0FBd0J4SixLQUF4QixFQUErQixJQUEvQixDQUFsQyxJQUEwRUEsTUFBTXlKLFlBQXZGOztBQUVBUixtQkFBYTtBQUNYUyxtQkFEVyxZQUNDUixLQURELEVBQ1E7QUFDakIsY0FBSVMsbUJBQWlCVCxLQUFqQiwyQ0FBSjs7QUFFQTtBQUNBLGNBQUlsSixNQUFNNEosVUFBVixFQUFzQjtBQUNwQjVKLGtCQUFNNEosVUFBTixDQUFpQkMsT0FBakIsR0FBMkJGLElBQTNCO0FBQ0QsV0FGRCxNQUVPO0FBQ0wzSixrQkFBTThKLFdBQU4sR0FBb0JILElBQXBCO0FBQ0Q7O0FBRUQ7QUFDQSxpQkFBT04sS0FBSy9FLEtBQUwsS0FBZSxLQUF0QjtBQUNEO0FBYlUsT0FBYjtBQWVEOztBQUVELFdBQU8sVUFBUzRFLEtBQVQsRUFBZ0I7QUFDckIsYUFBTztBQUNMTixpQkFBU0ssV0FBV1MsV0FBWCxDQUF1QlIsU0FBUyxLQUFoQyxDQURKO0FBRUxBLGVBQU9BLFNBQVM7QUFGWCxPQUFQO0FBSUQsS0FMRDtBQU1ELEdBM0N5QyxFQUExQzs7QUE2Q0E7QUFDQSxXQUFTZixrQkFBVCxDQUE0QmxGLEdBQTVCLEVBQWlDO0FBQy9CLFFBQUk4RyxjQUFjLEVBQWxCOztBQUVBLFFBQUksT0FBTzlHLEdBQVAsS0FBZSxRQUFuQixFQUE2QjtBQUMzQixhQUFPOEcsV0FBUDtBQUNEOztBQUVEOUcsVUFBTUEsSUFBSXpELElBQUosR0FBV2hCLEtBQVgsQ0FBaUIsQ0FBakIsRUFBb0IsQ0FBQyxDQUFyQixDQUFOLENBUCtCLENBT0E7O0FBRS9CLFFBQUksQ0FBQ3lFLEdBQUwsRUFBVTtBQUNSLGFBQU84RyxXQUFQO0FBQ0Q7O0FBRURBLGtCQUFjOUcsSUFBSTlELEtBQUosQ0FBVSxHQUFWLEVBQWU2SyxNQUFmLENBQXNCLFVBQVNDLEdBQVQsRUFBY0MsS0FBZCxFQUFxQjtBQUN2RCxVQUFJQyxRQUFRRCxNQUFNOUcsT0FBTixDQUFjLEtBQWQsRUFBcUIsR0FBckIsRUFBMEJqRSxLQUExQixDQUFnQyxHQUFoQyxDQUFaO0FBQ0EsVUFBSWpHLE1BQU1pUixNQUFNLENBQU4sQ0FBVjtBQUNBLFVBQUlDLE1BQU1ELE1BQU0sQ0FBTixDQUFWO0FBQ0FqUixZQUFNbVIsbUJBQW1CblIsR0FBbkIsQ0FBTjs7QUFFQTtBQUNBO0FBQ0FrUixZQUFNQSxRQUFRblAsU0FBUixHQUFvQixJQUFwQixHQUEyQm9QLG1CQUFtQkQsR0FBbkIsQ0FBakM7O0FBRUEsVUFBSSxDQUFDSCxJQUFJN0IsY0FBSixDQUFtQmxQLEdBQW5CLENBQUwsRUFBOEI7QUFDNUIrUSxZQUFJL1EsR0FBSixJQUFXa1IsR0FBWDtBQUNELE9BRkQsTUFFTyxJQUFJbFAsTUFBTW9QLE9BQU4sQ0FBY0wsSUFBSS9RLEdBQUosQ0FBZCxDQUFKLEVBQTZCO0FBQ2xDK1EsWUFBSS9RLEdBQUosRUFBU2lCLElBQVQsQ0FBY2lRLEdBQWQ7QUFDRCxPQUZNLE1BRUE7QUFDTEgsWUFBSS9RLEdBQUosSUFBVyxDQUFDK1EsSUFBSS9RLEdBQUosQ0FBRCxFQUFXa1IsR0FBWCxDQUFYO0FBQ0Q7QUFDRCxhQUFPSCxHQUFQO0FBQ0QsS0FsQmEsRUFrQlgsRUFsQlcsQ0FBZDs7QUFvQkEsV0FBT0YsV0FBUDtBQUNEOztBQUVEck8sYUFBV3NGLFVBQVgsR0FBd0JBLFVBQXhCO0FBRUMsQ0FuTkEsQ0FtTkNxQyxNQW5ORCxDQUFEO0NDRkE7O0FBRUEsQ0FBQyxVQUFTN0gsQ0FBVCxFQUFZOztBQUViOzs7OztBQUtBLE1BQU0rTyxjQUFnQixDQUFDLFdBQUQsRUFBYyxXQUFkLENBQXRCO0FBQ0EsTUFBTUMsZ0JBQWdCLENBQUMsa0JBQUQsRUFBcUIsa0JBQXJCLENBQXRCOztBQUVBLE1BQU1DLFNBQVM7QUFDYkMsZUFBVyxVQUFTaEgsT0FBVCxFQUFrQmlILFNBQWxCLEVBQTZCQyxFQUE3QixFQUFpQztBQUMxQ0MsY0FBUSxJQUFSLEVBQWNuSCxPQUFkLEVBQXVCaUgsU0FBdkIsRUFBa0NDLEVBQWxDO0FBQ0QsS0FIWTs7QUFLYkUsZ0JBQVksVUFBU3BILE9BQVQsRUFBa0JpSCxTQUFsQixFQUE2QkMsRUFBN0IsRUFBaUM7QUFDM0NDLGNBQVEsS0FBUixFQUFlbkgsT0FBZixFQUF3QmlILFNBQXhCLEVBQW1DQyxFQUFuQztBQUNEO0FBUFksR0FBZjs7QUFVQSxXQUFTRyxJQUFULENBQWNDLFFBQWQsRUFBd0J0TSxJQUF4QixFQUE4QjJDLEVBQTlCLEVBQWlDO0FBQy9CLFFBQUk0SixJQUFKO0FBQUEsUUFBVUMsSUFBVjtBQUFBLFFBQWdCN0ksUUFBUSxJQUF4QjtBQUNBOztBQUVBLGFBQVM4SSxJQUFULENBQWNDLEVBQWQsRUFBaUI7QUFDZixVQUFHLENBQUMvSSxLQUFKLEVBQVdBLFFBQVEzSyxPQUFPMEssV0FBUCxDQUFtQmIsR0FBbkIsRUFBUjtBQUNYO0FBQ0EySixhQUFPRSxLQUFLL0ksS0FBWjtBQUNBaEIsU0FBR1osS0FBSCxDQUFTL0IsSUFBVDs7QUFFQSxVQUFHd00sT0FBT0YsUUFBVixFQUFtQjtBQUFFQyxlQUFPdlQsT0FBT2dLLHFCQUFQLENBQTZCeUosSUFBN0IsRUFBbUN6TSxJQUFuQyxDQUFQO0FBQWtELE9BQXZFLE1BQ0k7QUFDRmhILGVBQU9rSyxvQkFBUCxDQUE0QnFKLElBQTVCO0FBQ0F2TSxhQUFLN0IsT0FBTCxDQUFhLHFCQUFiLEVBQW9DLENBQUM2QixJQUFELENBQXBDLEVBQTRDdUIsY0FBNUMsQ0FBMkQscUJBQTNELEVBQWtGLENBQUN2QixJQUFELENBQWxGO0FBQ0Q7QUFDRjtBQUNEdU0sV0FBT3ZULE9BQU9nSyxxQkFBUCxDQUE2QnlKLElBQTdCLENBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7O0FBU0EsV0FBU04sT0FBVCxDQUFpQlEsSUFBakIsRUFBdUIzSCxPQUF2QixFQUFnQ2lILFNBQWhDLEVBQTJDQyxFQUEzQyxFQUErQztBQUM3Q2xILGNBQVVsSSxFQUFFa0ksT0FBRixFQUFXNEgsRUFBWCxDQUFjLENBQWQsQ0FBVjs7QUFFQSxRQUFJLENBQUM1SCxRQUFRekYsTUFBYixFQUFxQjs7QUFFckIsUUFBSXNOLFlBQVlGLE9BQU9kLFlBQVksQ0FBWixDQUFQLEdBQXdCQSxZQUFZLENBQVosQ0FBeEM7QUFDQSxRQUFJaUIsY0FBY0gsT0FBT2IsY0FBYyxDQUFkLENBQVAsR0FBMEJBLGNBQWMsQ0FBZCxDQUE1Qzs7QUFFQTtBQUNBaUI7O0FBRUEvSCxZQUNHZ0ksUUFESCxDQUNZZixTQURaLEVBRUcxQyxHQUZILENBRU8sWUFGUCxFQUVxQixNQUZyQjs7QUFJQXZHLDBCQUFzQixZQUFNO0FBQzFCZ0MsY0FBUWdJLFFBQVIsQ0FBaUJILFNBQWpCO0FBQ0EsVUFBSUYsSUFBSixFQUFVM0gsUUFBUWlJLElBQVI7QUFDWCxLQUhEOztBQUtBO0FBQ0FqSywwQkFBc0IsWUFBTTtBQUMxQmdDLGNBQVEsQ0FBUixFQUFXa0ksV0FBWDtBQUNBbEksY0FDR3VFLEdBREgsQ0FDTyxZQURQLEVBQ3FCLEVBRHJCLEVBRUd5RCxRQUZILENBRVlGLFdBRlo7QUFHRCxLQUxEOztBQU9BO0FBQ0E5SCxZQUFRbUksR0FBUixDQUFZblEsV0FBV2tFLGFBQVgsQ0FBeUI4RCxPQUF6QixDQUFaLEVBQStDb0ksTUFBL0M7O0FBRUE7QUFDQSxhQUFTQSxNQUFULEdBQWtCO0FBQ2hCLFVBQUksQ0FBQ1QsSUFBTCxFQUFXM0gsUUFBUXFJLElBQVI7QUFDWE47QUFDQSxVQUFJYixFQUFKLEVBQVFBLEdBQUduSyxLQUFILENBQVNpRCxPQUFUO0FBQ1Q7O0FBRUQ7QUFDQSxhQUFTK0gsS0FBVCxHQUFpQjtBQUNmL0gsY0FBUSxDQUFSLEVBQVcxRCxLQUFYLENBQWlCZ00sa0JBQWpCLEdBQXNDLENBQXRDO0FBQ0F0SSxjQUFRM0MsV0FBUixDQUF1QndLLFNBQXZCLFNBQW9DQyxXQUFwQyxTQUFtRGIsU0FBbkQ7QUFDRDtBQUNGOztBQUVEalAsYUFBV3FQLElBQVgsR0FBa0JBLElBQWxCO0FBQ0FyUCxhQUFXK08sTUFBWCxHQUFvQkEsTUFBcEI7QUFFQyxDQWhHQSxDQWdHQ3BILE1BaEdELENBQUQ7Q0NGQTs7QUFFQSxDQUFDLFVBQVM3SCxDQUFULEVBQVk7O0FBRWIsTUFBTXlRLE9BQU87QUFDWEMsV0FEVyxZQUNIQyxJQURHLEVBQ2dCO0FBQUEsVUFBYi9TLElBQWEsdUVBQU4sSUFBTTs7QUFDekIrUyxXQUFLcFEsSUFBTCxDQUFVLE1BQVYsRUFBa0IsU0FBbEI7O0FBRUEsVUFBSXFRLFFBQVFELEtBQUt0TixJQUFMLENBQVUsSUFBVixFQUFnQjlDLElBQWhCLENBQXFCLEVBQUMsUUFBUSxVQUFULEVBQXJCLENBQVo7QUFBQSxVQUNJc1EsdUJBQXFCalQsSUFBckIsYUFESjtBQUFBLFVBRUlrVCxlQUFrQkQsWUFBbEIsVUFGSjtBQUFBLFVBR0lFLHNCQUFvQm5ULElBQXBCLG9CQUhKOztBQUtBK1MsV0FBS3ROLElBQUwsQ0FBVSxTQUFWLEVBQXFCOUMsSUFBckIsQ0FBMEIsVUFBMUIsRUFBc0MsQ0FBdEM7O0FBRUFxUSxZQUFNL08sSUFBTixDQUFXLFlBQVc7QUFDcEIsWUFBSW1QLFFBQVFoUixFQUFFLElBQUYsQ0FBWjtBQUFBLFlBQ0lpUixPQUFPRCxNQUFNRSxRQUFOLENBQWUsSUFBZixDQURYOztBQUdBLFlBQUlELEtBQUt4TyxNQUFULEVBQWlCO0FBQ2Z1TyxnQkFDR2QsUUFESCxDQUNZYSxXQURaLEVBRUd4USxJQUZILENBRVE7QUFDSiw2QkFBaUIsSUFEYjtBQUVKLDZCQUFpQixLQUZiO0FBR0osMEJBQWN5USxNQUFNRSxRQUFOLENBQWUsU0FBZixFQUEwQi9DLElBQTFCO0FBSFYsV0FGUjs7QUFRQThDLGVBQ0dmLFFBREgsY0FDdUJXLFlBRHZCLEVBRUd0USxJQUZILENBRVE7QUFDSiw0QkFBZ0IsRUFEWjtBQUVKLDJCQUFlLElBRlg7QUFHSixvQkFBUTtBQUhKLFdBRlI7QUFPRDs7QUFFRCxZQUFJeVEsTUFBTTdJLE1BQU4sQ0FBYSxnQkFBYixFQUErQjFGLE1BQW5DLEVBQTJDO0FBQ3pDdU8sZ0JBQU1kLFFBQU4sc0JBQWtDWSxZQUFsQztBQUNEO0FBQ0YsT0F6QkQ7O0FBMkJBO0FBQ0QsS0F2Q1U7QUF5Q1hLLFFBekNXLFlBeUNOUixJQXpDTSxFQXlDQS9TLElBekNBLEVBeUNNO0FBQ2YsVUFBSWdULFFBQVFELEtBQUt0TixJQUFMLENBQVUsSUFBVixFQUFnQjlCLFVBQWhCLENBQTJCLFVBQTNCLENBQVo7QUFBQSxVQUNJc1AsdUJBQXFCalQsSUFBckIsYUFESjtBQUFBLFVBRUlrVCxlQUFrQkQsWUFBbEIsVUFGSjtBQUFBLFVBR0lFLHNCQUFvQm5ULElBQXBCLG9CQUhKOztBQUtBK1MsV0FDR3ROLElBREgsQ0FDUSx3QkFEUixFQUVHa0MsV0FGSCxDQUVrQnNMLFlBRmxCLFNBRWtDQyxZQUZsQyxTQUVrREMsV0FGbEQseUNBR0d4UCxVQUhILENBR2MsY0FIZCxFQUc4QmtMLEdBSDlCLENBR2tDLFNBSGxDLEVBRzZDLEVBSDdDOztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDRDtBQWxFVSxHQUFiOztBQXFFQXZNLGFBQVd1USxJQUFYLEdBQWtCQSxJQUFsQjtBQUVDLENBekVBLENBeUVDNUksTUF6RUQsQ0FBRDtDQ0ZBOztBQUVBLENBQUMsVUFBUzdILENBQVQsRUFBWTs7QUFFYixXQUFTb1IsS0FBVCxDQUFlbE8sSUFBZixFQUFxQm1PLE9BQXJCLEVBQThCakMsRUFBOUIsRUFBa0M7QUFDaEMsUUFBSXJOLFFBQVEsSUFBWjtBQUFBLFFBQ0l5TixXQUFXNkIsUUFBUTdCLFFBRHZCO0FBQUEsUUFDZ0M7QUFDNUI4QixnQkFBWWpQLE9BQU94QyxJQUFQLENBQVlxRCxLQUFLOUIsSUFBTCxFQUFaLEVBQXlCLENBQXpCLEtBQStCLE9BRi9DO0FBQUEsUUFHSW1RLFNBQVMsQ0FBQyxDQUhkO0FBQUEsUUFJSTFLLEtBSko7QUFBQSxRQUtJN0osS0FMSjs7QUFPQSxTQUFLd1UsUUFBTCxHQUFnQixLQUFoQjs7QUFFQSxTQUFLQyxPQUFMLEdBQWUsWUFBVztBQUN4QkYsZUFBUyxDQUFDLENBQVY7QUFDQS9ULG1CQUFhUixLQUFiO0FBQ0EsV0FBSzZKLEtBQUw7QUFDRCxLQUpEOztBQU1BLFNBQUtBLEtBQUwsR0FBYSxZQUFXO0FBQ3RCLFdBQUsySyxRQUFMLEdBQWdCLEtBQWhCO0FBQ0E7QUFDQWhVLG1CQUFhUixLQUFiO0FBQ0F1VSxlQUFTQSxVQUFVLENBQVYsR0FBYy9CLFFBQWQsR0FBeUIrQixNQUFsQztBQUNBck8sV0FBSzlCLElBQUwsQ0FBVSxRQUFWLEVBQW9CLEtBQXBCO0FBQ0F5RixjQUFRZixLQUFLQyxHQUFMLEVBQVI7QUFDQS9JLGNBQVFLLFdBQVcsWUFBVTtBQUMzQixZQUFHZ1UsUUFBUUssUUFBWCxFQUFvQjtBQUNsQjNQLGdCQUFNMFAsT0FBTixHQURrQixDQUNGO0FBQ2pCO0FBQ0QsWUFBSXJDLE1BQU0sT0FBT0EsRUFBUCxLQUFjLFVBQXhCLEVBQW9DO0FBQUVBO0FBQU87QUFDOUMsT0FMTyxFQUtMbUMsTUFMSyxDQUFSO0FBTUFyTyxXQUFLN0IsT0FBTCxvQkFBOEJpUSxTQUE5QjtBQUNELEtBZEQ7O0FBZ0JBLFNBQUtLLEtBQUwsR0FBYSxZQUFXO0FBQ3RCLFdBQUtILFFBQUwsR0FBZ0IsSUFBaEI7QUFDQTtBQUNBaFUsbUJBQWFSLEtBQWI7QUFDQWtHLFdBQUs5QixJQUFMLENBQVUsUUFBVixFQUFvQixJQUFwQjtBQUNBLFVBQUlrRCxNQUFNd0IsS0FBS0MsR0FBTCxFQUFWO0FBQ0F3TCxlQUFTQSxVQUFVak4sTUFBTXVDLEtBQWhCLENBQVQ7QUFDQTNELFdBQUs3QixPQUFMLHFCQUErQmlRLFNBQS9CO0FBQ0QsS0FSRDtBQVNEOztBQUVEOzs7OztBQUtBLFdBQVNNLGNBQVQsQ0FBd0JDLE1BQXhCLEVBQWdDcEwsUUFBaEMsRUFBeUM7QUFDdkMsUUFBSThGLE9BQU8sSUFBWDtBQUFBLFFBQ0l1RixXQUFXRCxPQUFPcFAsTUFEdEI7O0FBR0EsUUFBSXFQLGFBQWEsQ0FBakIsRUFBb0I7QUFDbEJyTDtBQUNEOztBQUVEb0wsV0FBT2hRLElBQVAsQ0FBWSxZQUFXO0FBQ3JCLFVBQUksS0FBS2tRLFFBQVQsRUFBbUI7QUFDakJDO0FBQ0QsT0FGRCxNQUdLLElBQUksT0FBTyxLQUFLQyxZQUFaLEtBQTZCLFdBQTdCLElBQTRDLEtBQUtBLFlBQUwsR0FBb0IsQ0FBcEUsRUFBdUU7QUFDMUVEO0FBQ0QsT0FGSSxNQUdBO0FBQ0hoUyxVQUFFLElBQUYsRUFBUXFRLEdBQVIsQ0FBWSxNQUFaLEVBQW9CLFlBQVc7QUFDN0IyQjtBQUNELFNBRkQ7QUFHRDtBQUNGLEtBWkQ7O0FBY0EsYUFBU0EsaUJBQVQsR0FBNkI7QUFDM0JGO0FBQ0EsVUFBSUEsYUFBYSxDQUFqQixFQUFvQjtBQUNsQnJMO0FBQ0Q7QUFDRjtBQUNGOztBQUVEdkcsYUFBV2tSLEtBQVgsR0FBbUJBLEtBQW5CO0FBQ0FsUixhQUFXMFIsY0FBWCxHQUE0QkEsY0FBNUI7QUFFQyxDQW5GQSxDQW1GQy9KLE1BbkZELENBQUQ7OztBQ0ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQyxVQUFTN0gsQ0FBVCxFQUFZOztBQUVYQSxHQUFFa1MsU0FBRixHQUFjO0FBQ1ovUixXQUFTLE9BREc7QUFFWmdTLFdBQVMsa0JBQWtCaFQsU0FBU2lULGVBRnhCO0FBR1pDLGtCQUFnQixLQUhKO0FBSVpDLGlCQUFlLEVBSkg7QUFLWkMsaUJBQWU7QUFMSCxFQUFkOztBQVFBLEtBQU1DLFNBQU47QUFBQSxLQUNNQyxTQUROO0FBQUEsS0FFTUMsU0FGTjtBQUFBLEtBR01DLFdBSE47QUFBQSxLQUlNQyxXQUFXLEtBSmpCOztBQU1BLFVBQVNDLFVBQVQsR0FBc0I7QUFDcEI7QUFDQSxPQUFLQyxtQkFBTCxDQUF5QixXQUF6QixFQUFzQ0MsV0FBdEM7QUFDQSxPQUFLRCxtQkFBTCxDQUF5QixVQUF6QixFQUFxQ0QsVUFBckM7QUFDQUQsYUFBVyxLQUFYO0FBQ0Q7O0FBRUQsVUFBU0csV0FBVCxDQUFxQm5QLENBQXJCLEVBQXdCO0FBQ3RCLE1BQUk1RCxFQUFFa1MsU0FBRixDQUFZRyxjQUFoQixFQUFnQztBQUFFek8sS0FBRXlPLGNBQUY7QUFBcUI7QUFDdkQsTUFBR08sUUFBSCxFQUFhO0FBQ1gsT0FBSUksSUFBSXBQLEVBQUVxUCxPQUFGLENBQVUsQ0FBVixFQUFhQyxLQUFyQjtBQUNBLE9BQUlDLElBQUl2UCxFQUFFcVAsT0FBRixDQUFVLENBQVYsRUFBYUcsS0FBckI7QUFDQSxPQUFJQyxLQUFLYixZQUFZUSxDQUFyQjtBQUNBLE9BQUlNLEtBQUtiLFlBQVlVLENBQXJCO0FBQ0EsT0FBSUksR0FBSjtBQUNBWixpQkFBYyxJQUFJN00sSUFBSixHQUFXRSxPQUFYLEtBQXVCME0sU0FBckM7QUFDQSxPQUFHL1AsS0FBSzZRLEdBQUwsQ0FBU0gsRUFBVCxLQUFnQnJULEVBQUVrUyxTQUFGLENBQVlJLGFBQTVCLElBQTZDSyxlQUFlM1MsRUFBRWtTLFNBQUYsQ0FBWUssYUFBM0UsRUFBMEY7QUFDeEZnQixVQUFNRixLQUFLLENBQUwsR0FBUyxNQUFULEdBQWtCLE9BQXhCO0FBQ0Q7QUFDRDtBQUNBO0FBQ0E7QUFDQSxPQUFHRSxHQUFILEVBQVE7QUFDTjNQLE1BQUV5TyxjQUFGO0FBQ0FRLGVBQVdwTixJQUFYLENBQWdCLElBQWhCO0FBQ0F6RixNQUFFLElBQUYsRUFBUXFCLE9BQVIsQ0FBZ0IsT0FBaEIsRUFBeUJrUyxHQUF6QixFQUE4QmxTLE9BQTlCLFdBQThDa1MsR0FBOUM7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsVUFBU0UsWUFBVCxDQUFzQjdQLENBQXRCLEVBQXlCO0FBQ3ZCLE1BQUlBLEVBQUVxUCxPQUFGLENBQVV4USxNQUFWLElBQW9CLENBQXhCLEVBQTJCO0FBQ3pCK1AsZUFBWTVPLEVBQUVxUCxPQUFGLENBQVUsQ0FBVixFQUFhQyxLQUF6QjtBQUNBVCxlQUFZN08sRUFBRXFQLE9BQUYsQ0FBVSxDQUFWLEVBQWFHLEtBQXpCO0FBQ0FSLGNBQVcsSUFBWDtBQUNBRixlQUFZLElBQUk1TSxJQUFKLEdBQVdFLE9BQVgsRUFBWjtBQUNBLFFBQUszRyxnQkFBTCxDQUFzQixXQUF0QixFQUFtQzBULFdBQW5DLEVBQWdELEtBQWhEO0FBQ0EsUUFBSzFULGdCQUFMLENBQXNCLFVBQXRCLEVBQWtDd1QsVUFBbEMsRUFBOEMsS0FBOUM7QUFDRDtBQUNGOztBQUVELFVBQVNhLElBQVQsR0FBZ0I7QUFDZCxPQUFLclUsZ0JBQUwsSUFBeUIsS0FBS0EsZ0JBQUwsQ0FBc0IsWUFBdEIsRUFBb0NvVSxZQUFwQyxFQUFrRCxLQUFsRCxDQUF6QjtBQUNEOztBQUVELFVBQVNFLFFBQVQsR0FBb0I7QUFDbEIsT0FBS2IsbUJBQUwsQ0FBeUIsWUFBekIsRUFBdUNXLFlBQXZDO0FBQ0Q7O0FBRUR6VCxHQUFFNUMsS0FBRixDQUFRd1csT0FBUixDQUFnQkMsS0FBaEIsR0FBd0IsRUFBRUMsT0FBT0osSUFBVCxFQUF4Qjs7QUFFQTFULEdBQUU2QixJQUFGLENBQU8sQ0FBQyxNQUFELEVBQVMsSUFBVCxFQUFlLE1BQWYsRUFBdUIsT0FBdkIsQ0FBUCxFQUF3QyxZQUFZO0FBQ2xEN0IsSUFBRTVDLEtBQUYsQ0FBUXdXLE9BQVIsV0FBd0IsSUFBeEIsSUFBa0MsRUFBRUUsT0FBTyxZQUFVO0FBQ25EOVQsTUFBRSxJQUFGLEVBQVFzTixFQUFSLENBQVcsT0FBWCxFQUFvQnROLEVBQUUrVCxJQUF0QjtBQUNELElBRmlDLEVBQWxDO0FBR0QsRUFKRDtBQUtELENBeEVELEVBd0VHbE0sTUF4RUg7QUF5RUE7OztBQUdBLENBQUMsVUFBUzdILENBQVQsRUFBVztBQUNWQSxHQUFFNkYsRUFBRixDQUFLbU8sUUFBTCxHQUFnQixZQUFVO0FBQ3hCLE9BQUtuUyxJQUFMLENBQVUsVUFBU3NCLENBQVQsRUFBV1ksRUFBWCxFQUFjO0FBQ3RCL0QsS0FBRStELEVBQUYsRUFBTWdELElBQU4sQ0FBVywyQ0FBWCxFQUF1RCxZQUFVO0FBQy9EO0FBQ0E7QUFDQWtOLGdCQUFZN1csS0FBWjtBQUNELElBSkQ7QUFLRCxHQU5EOztBQVFBLE1BQUk2VyxjQUFjLFVBQVM3VyxLQUFULEVBQWU7QUFDL0IsT0FBSTZWLFVBQVU3VixNQUFNOFcsY0FBcEI7QUFBQSxPQUNJQyxRQUFRbEIsUUFBUSxDQUFSLENBRFo7QUFBQSxPQUVJbUIsYUFBYTtBQUNYQyxnQkFBWSxXQUREO0FBRVhDLGVBQVcsV0FGQTtBQUdYQyxjQUFVO0FBSEMsSUFGakI7QUFBQSxPQU9JM1csT0FBT3dXLFdBQVdoWCxNQUFNUSxJQUFqQixDQVBYO0FBQUEsT0FRSTRXLGNBUko7O0FBV0EsT0FBRyxnQkFBZ0J0WSxNQUFoQixJQUEwQixPQUFPQSxPQUFPdVksVUFBZCxLQUE2QixVQUExRCxFQUFzRTtBQUNwRUQscUJBQWlCLElBQUl0WSxPQUFPdVksVUFBWCxDQUFzQjdXLElBQXRCLEVBQTRCO0FBQzNDLGdCQUFXLElBRGdDO0FBRTNDLG1CQUFjLElBRjZCO0FBRzNDLGdCQUFXdVcsTUFBTU8sT0FIMEI7QUFJM0MsZ0JBQVdQLE1BQU1RLE9BSjBCO0FBSzNDLGdCQUFXUixNQUFNUyxPQUwwQjtBQU0zQyxnQkFBV1QsTUFBTVU7QUFOMEIsS0FBNUIsQ0FBakI7QUFRRCxJQVRELE1BU087QUFDTEwscUJBQWlCclYsU0FBUzJWLFdBQVQsQ0FBcUIsWUFBckIsQ0FBakI7QUFDQU4sbUJBQWVPLGNBQWYsQ0FBOEJuWCxJQUE5QixFQUFvQyxJQUFwQyxFQUEwQyxJQUExQyxFQUFnRDFCLE1BQWhELEVBQXdELENBQXhELEVBQTJEaVksTUFBTU8sT0FBakUsRUFBMEVQLE1BQU1RLE9BQWhGLEVBQXlGUixNQUFNUyxPQUEvRixFQUF3R1QsTUFBTVUsT0FBOUcsRUFBdUgsS0FBdkgsRUFBOEgsS0FBOUgsRUFBcUksS0FBckksRUFBNEksS0FBNUksRUFBbUosQ0FBbkosQ0FBb0osUUFBcEosRUFBOEosSUFBOUo7QUFDRDtBQUNEVixTQUFNcFcsTUFBTixDQUFhaVgsYUFBYixDQUEyQlIsY0FBM0I7QUFDRCxHQTFCRDtBQTJCRCxFQXBDRDtBQXFDRCxDQXRDQSxDQXNDQzNNLE1BdENELENBQUQ7O0FBeUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDQy9IQTs7QUFFQSxDQUFDLFVBQVM3SCxDQUFULEVBQVk7O0FBRWIsTUFBTWlWLG1CQUFvQixZQUFZO0FBQ3BDLFFBQUlDLFdBQVcsQ0FBQyxRQUFELEVBQVcsS0FBWCxFQUFrQixHQUFsQixFQUF1QixJQUF2QixFQUE2QixFQUE3QixDQUFmO0FBQ0EsU0FBSyxJQUFJL1IsSUFBRSxDQUFYLEVBQWNBLElBQUkrUixTQUFTelMsTUFBM0IsRUFBbUNVLEdBQW5DLEVBQXdDO0FBQ3RDLFVBQU8rUixTQUFTL1IsQ0FBVCxDQUFILHlCQUFvQ2pILE1BQXhDLEVBQWdEO0FBQzlDLGVBQU9BLE9BQVVnWixTQUFTL1IsQ0FBVCxDQUFWLHNCQUFQO0FBQ0Q7QUFDRjtBQUNELFdBQU8sS0FBUDtBQUNELEdBUnlCLEVBQTFCOztBQVVBLE1BQU1nUyxXQUFXLFVBQUNwUixFQUFELEVBQUtuRyxJQUFMLEVBQWM7QUFDN0JtRyxPQUFHM0MsSUFBSCxDQUFReEQsSUFBUixFQUFjK0YsS0FBZCxDQUFvQixHQUFwQixFQUF5QnpCLE9BQXpCLENBQWlDLGNBQU07QUFDckNsQyxjQUFNOE4sRUFBTixFQUFhbFEsU0FBUyxPQUFULEdBQW1CLFNBQW5CLEdBQStCLGdCQUE1QyxFQUFpRUEsSUFBakUsa0JBQW9GLENBQUNtRyxFQUFELENBQXBGO0FBQ0QsS0FGRDtBQUdELEdBSkQ7QUFLQTtBQUNBL0QsSUFBRWIsUUFBRixFQUFZbU8sRUFBWixDQUFlLGtCQUFmLEVBQW1DLGFBQW5DLEVBQWtELFlBQVc7QUFDM0Q2SCxhQUFTblYsRUFBRSxJQUFGLENBQVQsRUFBa0IsTUFBbEI7QUFDRCxHQUZEOztBQUlBO0FBQ0E7QUFDQUEsSUFBRWIsUUFBRixFQUFZbU8sRUFBWixDQUFlLGtCQUFmLEVBQW1DLGNBQW5DLEVBQW1ELFlBQVc7QUFDNUQsUUFBSVEsS0FBSzlOLEVBQUUsSUFBRixFQUFRb0IsSUFBUixDQUFhLE9BQWIsQ0FBVDtBQUNBLFFBQUkwTSxFQUFKLEVBQVE7QUFDTnFILGVBQVNuVixFQUFFLElBQUYsQ0FBVCxFQUFrQixPQUFsQjtBQUNELEtBRkQsTUFHSztBQUNIQSxRQUFFLElBQUYsRUFBUXFCLE9BQVIsQ0FBZ0Isa0JBQWhCO0FBQ0Q7QUFDRixHQVJEOztBQVVBO0FBQ0FyQixJQUFFYixRQUFGLEVBQVltTyxFQUFaLENBQWUsa0JBQWYsRUFBbUMsZUFBbkMsRUFBb0QsWUFBVztBQUM3RDZILGFBQVNuVixFQUFFLElBQUYsQ0FBVCxFQUFrQixRQUFsQjtBQUNELEdBRkQ7O0FBSUE7QUFDQUEsSUFBRWIsUUFBRixFQUFZbU8sRUFBWixDQUFlLGtCQUFmLEVBQW1DLGlCQUFuQyxFQUFzRCxVQUFTMUosQ0FBVCxFQUFXO0FBQy9EQSxNQUFFd1IsZUFBRjtBQUNBLFFBQUlqRyxZQUFZblAsRUFBRSxJQUFGLEVBQVFvQixJQUFSLENBQWEsVUFBYixDQUFoQjs7QUFFQSxRQUFHK04sY0FBYyxFQUFqQixFQUFvQjtBQUNsQmpQLGlCQUFXK08sTUFBWCxDQUFrQkssVUFBbEIsQ0FBNkJ0UCxFQUFFLElBQUYsQ0FBN0IsRUFBc0NtUCxTQUF0QyxFQUFpRCxZQUFXO0FBQzFEblAsVUFBRSxJQUFGLEVBQVFxQixPQUFSLENBQWdCLFdBQWhCO0FBQ0QsT0FGRDtBQUdELEtBSkQsTUFJSztBQUNIckIsUUFBRSxJQUFGLEVBQVFxVixPQUFSLEdBQWtCaFUsT0FBbEIsQ0FBMEIsV0FBMUI7QUFDRDtBQUNGLEdBWEQ7O0FBYUFyQixJQUFFYixRQUFGLEVBQVltTyxFQUFaLENBQWUsa0NBQWYsRUFBbUQscUJBQW5ELEVBQTBFLFlBQVc7QUFDbkYsUUFBSVEsS0FBSzlOLEVBQUUsSUFBRixFQUFRb0IsSUFBUixDQUFhLGNBQWIsQ0FBVDtBQUNBcEIsWUFBTThOLEVBQU4sRUFBWXJKLGNBQVosQ0FBMkIsbUJBQTNCLEVBQWdELENBQUN6RSxFQUFFLElBQUYsQ0FBRCxDQUFoRDtBQUNELEdBSEQ7O0FBS0E7Ozs7O0FBS0FBLElBQUU5RCxNQUFGLEVBQVVvUixFQUFWLENBQWEsTUFBYixFQUFxQixZQUFNO0FBQ3pCZ0k7QUFDRCxHQUZEOztBQUlBLFdBQVNBLGNBQVQsR0FBMEI7QUFDeEJDO0FBQ0FDO0FBQ0FDO0FBQ0FDO0FBQ0Q7O0FBRUQ7QUFDQSxXQUFTQSxlQUFULENBQXlCM1UsVUFBekIsRUFBcUM7QUFDbkMsUUFBSTRVLFlBQVkzVixFQUFFLGlCQUFGLENBQWhCO0FBQUEsUUFDSTRWLFlBQVksQ0FBQyxVQUFELEVBQWEsU0FBYixFQUF3QixRQUF4QixDQURoQjs7QUFHQSxRQUFHN1UsVUFBSCxFQUFjO0FBQ1osVUFBRyxPQUFPQSxVQUFQLEtBQXNCLFFBQXpCLEVBQWtDO0FBQ2hDNlUsa0JBQVVqWCxJQUFWLENBQWVvQyxVQUFmO0FBQ0QsT0FGRCxNQUVNLElBQUcsT0FBT0EsVUFBUCxLQUFzQixRQUF0QixJQUFrQyxPQUFPQSxXQUFXLENBQVgsQ0FBUCxLQUF5QixRQUE5RCxFQUF1RTtBQUMzRTZVLGtCQUFVdk8sTUFBVixDQUFpQnRHLFVBQWpCO0FBQ0QsT0FGSyxNQUVEO0FBQ0h3QixnQkFBUUMsS0FBUixDQUFjLDhCQUFkO0FBQ0Q7QUFDRjtBQUNELFFBQUdtVCxVQUFVbFQsTUFBYixFQUFvQjtBQUNsQixVQUFJb1QsWUFBWUQsVUFBVTlSLEdBQVYsQ0FBYyxVQUFDckQsSUFBRCxFQUFVO0FBQ3RDLCtCQUFxQkEsSUFBckI7QUFDRCxPQUZlLEVBRWJxVixJQUZhLENBRVIsR0FGUSxDQUFoQjs7QUFJQTlWLFFBQUU5RCxNQUFGLEVBQVU2WixHQUFWLENBQWNGLFNBQWQsRUFBeUJ2SSxFQUF6QixDQUE0QnVJLFNBQTVCLEVBQXVDLFVBQVNqUyxDQUFULEVBQVlvUyxRQUFaLEVBQXFCO0FBQzFELFlBQUl4VixTQUFTb0QsRUFBRWxCLFNBQUYsQ0FBWWlCLEtBQVosQ0FBa0IsR0FBbEIsRUFBdUIsQ0FBdkIsQ0FBYjtBQUNBLFlBQUloQyxVQUFVM0IsYUFBV1EsTUFBWCxRQUFzQnlWLEdBQXRCLHNCQUE2Q0QsUUFBN0MsUUFBZDs7QUFFQXJVLGdCQUFRRSxJQUFSLENBQWEsWUFBVTtBQUNyQixjQUFJRSxRQUFRL0IsRUFBRSxJQUFGLENBQVo7O0FBRUErQixnQkFBTTBDLGNBQU4sQ0FBcUIsa0JBQXJCLEVBQXlDLENBQUMxQyxLQUFELENBQXpDO0FBQ0QsU0FKRDtBQUtELE9BVEQ7QUFVRDtBQUNGOztBQUVELFdBQVN5VCxjQUFULENBQXdCVSxRQUF4QixFQUFpQztBQUMvQixRQUFJbFosY0FBSjtBQUFBLFFBQ0ltWixTQUFTblcsRUFBRSxlQUFGLENBRGI7QUFFQSxRQUFHbVcsT0FBTzFULE1BQVYsRUFBaUI7QUFDZnpDLFFBQUU5RCxNQUFGLEVBQVU2WixHQUFWLENBQWMsbUJBQWQsRUFDQ3pJLEVBREQsQ0FDSSxtQkFESixFQUN5QixVQUFTMUosQ0FBVCxFQUFZO0FBQ25DLFlBQUk1RyxLQUFKLEVBQVc7QUFBRVEsdUJBQWFSLEtBQWI7QUFBc0I7O0FBRW5DQSxnQkFBUUssV0FBVyxZQUFVOztBQUUzQixjQUFHLENBQUM0WCxnQkFBSixFQUFxQjtBQUFDO0FBQ3BCa0IsbUJBQU90VSxJQUFQLENBQVksWUFBVTtBQUNwQjdCLGdCQUFFLElBQUYsRUFBUXlFLGNBQVIsQ0FBdUIscUJBQXZCO0FBQ0QsYUFGRDtBQUdEO0FBQ0Q7QUFDQTBSLGlCQUFPNVYsSUFBUCxDQUFZLGFBQVosRUFBMkIsUUFBM0I7QUFDRCxTQVRPLEVBU0wyVixZQUFZLEVBVFAsQ0FBUixDQUhtQyxDQVloQjtBQUNwQixPQWREO0FBZUQ7QUFDRjs7QUFFRCxXQUFTVCxjQUFULENBQXdCUyxRQUF4QixFQUFpQztBQUMvQixRQUFJbFosY0FBSjtBQUFBLFFBQ0ltWixTQUFTblcsRUFBRSxlQUFGLENBRGI7QUFFQSxRQUFHbVcsT0FBTzFULE1BQVYsRUFBaUI7QUFDZnpDLFFBQUU5RCxNQUFGLEVBQVU2WixHQUFWLENBQWMsbUJBQWQsRUFDQ3pJLEVBREQsQ0FDSSxtQkFESixFQUN5QixVQUFTMUosQ0FBVCxFQUFXO0FBQ2xDLFlBQUc1RyxLQUFILEVBQVM7QUFBRVEsdUJBQWFSLEtBQWI7QUFBc0I7O0FBRWpDQSxnQkFBUUssV0FBVyxZQUFVOztBQUUzQixjQUFHLENBQUM0WCxnQkFBSixFQUFxQjtBQUFDO0FBQ3BCa0IsbUJBQU90VSxJQUFQLENBQVksWUFBVTtBQUNwQjdCLGdCQUFFLElBQUYsRUFBUXlFLGNBQVIsQ0FBdUIscUJBQXZCO0FBQ0QsYUFGRDtBQUdEO0FBQ0Q7QUFDQTBSLGlCQUFPNVYsSUFBUCxDQUFZLGFBQVosRUFBMkIsUUFBM0I7QUFDRCxTQVRPLEVBU0wyVixZQUFZLEVBVFAsQ0FBUixDQUhrQyxDQVlmO0FBQ3BCLE9BZEQ7QUFlRDtBQUNGOztBQUVELFdBQVNYLGNBQVQsR0FBMEI7QUFDeEIsUUFBRyxDQUFDTixnQkFBSixFQUFxQjtBQUFFLGFBQU8sS0FBUDtBQUFlO0FBQ3RDLFFBQUltQixRQUFRalgsU0FBU2tYLGdCQUFULENBQTBCLDZDQUExQixDQUFaOztBQUVBO0FBQ0EsUUFBSUMsNEJBQTRCLFVBQVNDLG1CQUFULEVBQThCO0FBQzVELFVBQUlDLFVBQVV4VyxFQUFFdVcsb0JBQW9CLENBQXBCLEVBQXVCeFksTUFBekIsQ0FBZDtBQUNBO0FBQ0EsY0FBUXlZLFFBQVFqVyxJQUFSLENBQWEsYUFBYixDQUFSOztBQUVFLGFBQUssUUFBTDtBQUNBaVcsa0JBQVEvUixjQUFSLENBQXVCLHFCQUF2QixFQUE4QyxDQUFDK1IsT0FBRCxDQUE5QztBQUNBOztBQUVBLGFBQUssUUFBTDtBQUNBQSxrQkFBUS9SLGNBQVIsQ0FBdUIscUJBQXZCLEVBQThDLENBQUMrUixPQUFELEVBQVV0YSxPQUFPc04sV0FBakIsQ0FBOUM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxpQkFBTyxLQUFQO0FBQ0E7QUF0QkY7QUF3QkQsS0EzQkQ7O0FBNkJBLFFBQUc0TSxNQUFNM1QsTUFBVCxFQUFnQjtBQUNkO0FBQ0EsV0FBSyxJQUFJVSxJQUFJLENBQWIsRUFBZ0JBLEtBQUtpVCxNQUFNM1QsTUFBTixHQUFhLENBQWxDLEVBQXFDVSxHQUFyQyxFQUEwQztBQUN4QyxZQUFJc1Qsa0JBQWtCLElBQUl4QixnQkFBSixDQUFxQnFCLHlCQUFyQixDQUF0QjtBQUNBRyx3QkFBZ0JDLE9BQWhCLENBQXdCTixNQUFNalQsQ0FBTixDQUF4QixFQUFrQyxFQUFFd1QsWUFBWSxJQUFkLEVBQW9CQyxXQUFXLEtBQS9CLEVBQXNDQyxlQUFlLEtBQXJELEVBQTREQyxTQUFRLEtBQXBFLEVBQTJFQyxpQkFBZ0IsQ0FBQyxhQUFELENBQTNGLEVBQWxDO0FBQ0Q7QUFDRjtBQUNGOztBQUVEOztBQUVBO0FBQ0E7QUFDQTdXLGFBQVc4VyxRQUFYLEdBQXNCMUIsY0FBdEI7QUFDQTtBQUNBO0FBRUMsQ0F6TUEsQ0F5TUN6TixNQXpNRCxDQUFEOztBQTJNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtDQzlPQTs7Ozs7O0FBRUEsQ0FBQyxVQUFTN0gsQ0FBVCxFQUFZOztBQUViOzs7Ozs7OztBQUZhLE1BVVBpWCxTQVZPO0FBV1g7Ozs7Ozs7QUFPQSx1QkFBWS9PLE9BQVosRUFBcUJtSixPQUFyQixFQUE4QjtBQUFBOztBQUM1QixXQUFLbFEsUUFBTCxHQUFnQitHLE9BQWhCO0FBQ0EsV0FBS21KLE9BQUwsR0FBZXJSLEVBQUVxTCxNQUFGLENBQVMsRUFBVCxFQUFhNEwsVUFBVUMsUUFBdkIsRUFBaUMsS0FBSy9WLFFBQUwsQ0FBY0MsSUFBZCxFQUFqQyxFQUF1RGlRLE9BQXZELENBQWY7QUFDQSxXQUFLOEYsWUFBTCxHQUFvQm5YLEdBQXBCO0FBQ0EsV0FBS29YLFNBQUwsR0FBaUJwWCxHQUFqQjs7QUFFQSxXQUFLOEIsS0FBTDtBQUNBLFdBQUt1VixPQUFMOztBQUVBblgsaUJBQVdZLGNBQVgsQ0FBMEIsSUFBMUIsRUFBZ0MsV0FBaEM7QUFDQVosaUJBQVdtSyxRQUFYLENBQW9CdUIsUUFBcEIsQ0FBNkIsV0FBN0IsRUFBMEM7QUFDeEMsa0JBQVU7QUFEOEIsT0FBMUM7QUFJRDs7QUFFRDs7Ozs7OztBQWxDVztBQUFBO0FBQUEsOEJBdUNIO0FBQ04sWUFBSWtDLEtBQUssS0FBSzNNLFFBQUwsQ0FBY1osSUFBZCxDQUFtQixJQUFuQixDQUFUOztBQUVBLGFBQUtZLFFBQUwsQ0FBY1osSUFBZCxDQUFtQixhQUFuQixFQUFrQyxNQUFsQzs7QUFFQTtBQUNBLGFBQUs2VyxTQUFMLEdBQWlCcFgsRUFBRWIsUUFBRixFQUNka0UsSUFEYyxDQUNULGlCQUFleUssRUFBZixHQUFrQixtQkFBbEIsR0FBc0NBLEVBQXRDLEdBQXlDLG9CQUF6QyxHQUE4REEsRUFBOUQsR0FBaUUsSUFEeEQsRUFFZHZOLElBRmMsQ0FFVCxlQUZTLEVBRVEsT0FGUixFQUdkQSxJQUhjLENBR1QsZUFIUyxFQUdRdU4sRUFIUixDQUFqQjs7QUFLQTtBQUNBLFlBQUksS0FBS3VELE9BQUwsQ0FBYWlHLFlBQWpCLEVBQStCO0FBQzdCLGNBQUl0WCxFQUFFLHFCQUFGLEVBQXlCeUMsTUFBN0IsRUFBcUM7QUFDbkMsaUJBQUs4VSxPQUFMLEdBQWV2WCxFQUFFLHFCQUFGLENBQWY7QUFDRCxXQUZELE1BRU87QUFDTCxnQkFBSXdYLFNBQVNyWSxTQUFTSSxhQUFULENBQXVCLEtBQXZCLENBQWI7QUFDQWlZLG1CQUFPOVksWUFBUCxDQUFvQixPQUFwQixFQUE2QixvQkFBN0I7QUFDQXNCLGNBQUUsMkJBQUYsRUFBK0J5WCxNQUEvQixDQUFzQ0QsTUFBdEM7O0FBRUEsaUJBQUtELE9BQUwsR0FBZXZYLEVBQUV3WCxNQUFGLENBQWY7QUFDRDtBQUNGOztBQUVELGFBQUtuRyxPQUFMLENBQWFxRyxVQUFiLEdBQTBCLEtBQUtyRyxPQUFMLENBQWFxRyxVQUFiLElBQTJCLElBQUlDLE1BQUosQ0FBVyxLQUFLdEcsT0FBTCxDQUFhdUcsV0FBeEIsRUFBcUMsR0FBckMsRUFBMEN2UixJQUExQyxDQUErQyxLQUFLbEYsUUFBTCxDQUFjLENBQWQsRUFBaUJULFNBQWhFLENBQXJEOztBQUVBLFlBQUksS0FBSzJRLE9BQUwsQ0FBYXFHLFVBQWpCLEVBQTZCO0FBQzNCLGVBQUtyRyxPQUFMLENBQWF3RyxRQUFiLEdBQXdCLEtBQUt4RyxPQUFMLENBQWF3RyxRQUFiLElBQXlCLEtBQUsxVyxRQUFMLENBQWMsQ0FBZCxFQUFpQlQsU0FBakIsQ0FBMkJvWCxLQUEzQixDQUFpQyx1Q0FBakMsRUFBMEUsQ0FBMUUsRUFBNkVuVSxLQUE3RSxDQUFtRixHQUFuRixFQUF3RixDQUF4RixDQUFqRDtBQUNBLGVBQUtvVSxhQUFMO0FBQ0Q7QUFDRCxZQUFJLENBQUMsS0FBSzFHLE9BQUwsQ0FBYTJHLGNBQWxCLEVBQWtDO0FBQ2hDLGVBQUszRyxPQUFMLENBQWEyRyxjQUFiLEdBQThCclEsV0FBV3pMLE9BQU84UixnQkFBUCxDQUF3QmhPLEVBQUUsMkJBQUYsRUFBK0IsQ0FBL0IsQ0FBeEIsRUFBMkR3USxrQkFBdEUsSUFBNEYsSUFBMUg7QUFDRDtBQUNGOztBQUVEOzs7Ozs7QUExRVc7QUFBQTtBQUFBLGdDQStFRDtBQUNSLGFBQUtyUCxRQUFMLENBQWM0VSxHQUFkLENBQWtCLDJCQUFsQixFQUErQ3pJLEVBQS9DLENBQWtEO0FBQ2hELDZCQUFtQixLQUFLMkssSUFBTCxDQUFVbFIsSUFBVixDQUFlLElBQWYsQ0FENkI7QUFFaEQsOEJBQW9CLEtBQUttUixLQUFMLENBQVduUixJQUFYLENBQWdCLElBQWhCLENBRjRCO0FBR2hELCtCQUFxQixLQUFLb1IsTUFBTCxDQUFZcFIsSUFBWixDQUFpQixJQUFqQixDQUgyQjtBQUloRCxrQ0FBd0IsS0FBS3FSLGVBQUwsQ0FBcUJyUixJQUFyQixDQUEwQixJQUExQjtBQUp3QixTQUFsRDs7QUFPQSxZQUFJLEtBQUtzSyxPQUFMLENBQWFpRyxZQUFiLElBQTZCLEtBQUtDLE9BQUwsQ0FBYTlVLE1BQTlDLEVBQXNEO0FBQ3BELGVBQUs4VSxPQUFMLENBQWFqSyxFQUFiLENBQWdCLEVBQUMsc0JBQXNCLEtBQUs0SyxLQUFMLENBQVduUixJQUFYLENBQWdCLElBQWhCLENBQXZCLEVBQWhCO0FBQ0Q7QUFDRjs7QUFFRDs7Ozs7QUE1Rlc7QUFBQTtBQUFBLHNDQWdHSztBQUNkLFlBQUloRixRQUFRLElBQVo7O0FBRUEvQixVQUFFOUQsTUFBRixFQUFVb1IsRUFBVixDQUFhLHVCQUFiLEVBQXNDLFlBQVc7QUFDL0MsY0FBSXBOLFdBQVdzRixVQUFYLENBQXNCdUgsT0FBdEIsQ0FBOEJoTCxNQUFNc1AsT0FBTixDQUFjd0csUUFBNUMsQ0FBSixFQUEyRDtBQUN6RDlWLGtCQUFNc1csTUFBTixDQUFhLElBQWI7QUFDRCxXQUZELE1BRU87QUFDTHRXLGtCQUFNc1csTUFBTixDQUFhLEtBQWI7QUFDRDtBQUNGLFNBTkQsRUFNR2hJLEdBTkgsQ0FNTyxtQkFOUCxFQU00QixZQUFXO0FBQ3JDLGNBQUluUSxXQUFXc0YsVUFBWCxDQUFzQnVILE9BQXRCLENBQThCaEwsTUFBTXNQLE9BQU4sQ0FBY3dHLFFBQTVDLENBQUosRUFBMkQ7QUFDekQ5VixrQkFBTXNXLE1BQU4sQ0FBYSxJQUFiO0FBQ0Q7QUFDRixTQVZEO0FBV0Q7O0FBRUQ7Ozs7OztBQWhIVztBQUFBO0FBQUEsNkJBcUhKWCxVQXJISSxFQXFIUTtBQUNqQixZQUFJWSxVQUFVLEtBQUtuWCxRQUFMLENBQWNrQyxJQUFkLENBQW1CLGNBQW5CLENBQWQ7QUFDQSxZQUFJcVUsVUFBSixFQUFnQjtBQUNkLGVBQUtRLEtBQUw7QUFDQSxlQUFLUixVQUFMLEdBQWtCLElBQWxCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQUt2VyxRQUFMLENBQWM0VSxHQUFkLENBQWtCLG1DQUFsQjtBQUNBLGNBQUl1QyxRQUFRN1YsTUFBWixFQUFvQjtBQUFFNlYsb0JBQVEvSCxJQUFSO0FBQWlCO0FBQ3hDLFNBVkQsTUFVTztBQUNMLGVBQUttSCxVQUFMLEdBQWtCLEtBQWxCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFLdlcsUUFBTCxDQUFjbU0sRUFBZCxDQUFpQjtBQUNmLCtCQUFtQixLQUFLMkssSUFBTCxDQUFVbFIsSUFBVixDQUFlLElBQWYsQ0FESjtBQUVmLGlDQUFxQixLQUFLb1IsTUFBTCxDQUFZcFIsSUFBWixDQUFpQixJQUFqQjtBQUZOLFdBQWpCO0FBSUEsY0FBSXVSLFFBQVE3VixNQUFaLEVBQW9CO0FBQ2xCNlYsb0JBQVFuSSxJQUFSO0FBQ0Q7QUFDRjtBQUNGOztBQUVEOzs7Ozs7OztBQWpKVztBQUFBO0FBQUEsMkJBd0pOL1MsS0F4Sk0sRUF3SkNpRSxPQXhKRCxFQXdKVTtBQUNuQixZQUFJLEtBQUtGLFFBQUwsQ0FBY29YLFFBQWQsQ0FBdUIsU0FBdkIsS0FBcUMsS0FBS2IsVUFBOUMsRUFBMEQ7QUFBRTtBQUFTO0FBQ3JFLFlBQUkzVixRQUFRLElBQVo7QUFBQSxZQUNJeVcsUUFBUXhZLEVBQUViLFNBQVM5QyxJQUFYLENBRFo7O0FBR0EsWUFBSSxLQUFLZ1YsT0FBTCxDQUFhb0gsUUFBakIsRUFBMkI7QUFDekJ6WSxZQUFFLE1BQUYsRUFBVTBZLFNBQVYsQ0FBb0IsQ0FBcEI7QUFDRDtBQUNEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0EsWUFBSUMsV0FBVzNZLEVBQUUsMkJBQUYsQ0FBZjtBQUNBMlksaUJBQVN6SSxRQUFULENBQWtCLGdDQUErQm5PLE1BQU1zUCxPQUFOLENBQWN4SCxRQUEvRDs7QUFFQTlILGNBQU1aLFFBQU4sQ0FBZStPLFFBQWYsQ0FBd0IsU0FBeEI7O0FBRUU7QUFDQTtBQUNBOztBQUVGLGFBQUtrSCxTQUFMLENBQWU3VyxJQUFmLENBQW9CLGVBQXBCLEVBQXFDLE1BQXJDO0FBQ0EsYUFBS1ksUUFBTCxDQUFjWixJQUFkLENBQW1CLGFBQW5CLEVBQWtDLE9BQWxDLEVBQ0tjLE9BREwsQ0FDYSxxQkFEYjs7QUFHQSxZQUFJLEtBQUtnUSxPQUFMLENBQWFpRyxZQUFqQixFQUErQjtBQUM3QixlQUFLQyxPQUFMLENBQWFySCxRQUFiLENBQXNCLFlBQXRCO0FBQ0Q7O0FBRUQsWUFBSTdPLE9BQUosRUFBYTtBQUNYLGVBQUs4VixZQUFMLEdBQW9COVYsT0FBcEI7QUFDRDs7QUFFRCxZQUFJLEtBQUtnUSxPQUFMLENBQWF1SCxTQUFqQixFQUE0QjtBQUMxQkQsbUJBQVN0SSxHQUFULENBQWFuUSxXQUFXa0UsYUFBWCxDQUF5QnVVLFFBQXpCLENBQWIsRUFBaUQsWUFBVztBQUMxRCxnQkFBRzVXLE1BQU1aLFFBQU4sQ0FBZW9YLFFBQWYsQ0FBd0IsU0FBeEIsQ0FBSCxFQUF1QztBQUFFO0FBQ3ZDeFcsb0JBQU1aLFFBQU4sQ0FBZVosSUFBZixDQUFvQixVQUFwQixFQUFnQyxJQUFoQztBQUNBd0Isb0JBQU1aLFFBQU4sQ0FBZTBYLEtBQWY7QUFDRDtBQUNGLFdBTEQ7QUFNRDs7QUFFRCxZQUFJLEtBQUt4SCxPQUFMLENBQWF5SCxTQUFqQixFQUE0QjtBQUMxQkgsbUJBQVN0SSxHQUFULENBQWFuUSxXQUFXa0UsYUFBWCxDQUF5QnVVLFFBQXpCLENBQWIsRUFBaUQsWUFBVztBQUMxRCxnQkFBRzVXLE1BQU1aLFFBQU4sQ0FBZW9YLFFBQWYsQ0FBd0IsU0FBeEIsQ0FBSCxFQUF1QztBQUFFO0FBQ3ZDeFcsb0JBQU1aLFFBQU4sQ0FBZVosSUFBZixDQUFvQixVQUFwQixFQUFnQyxJQUFoQztBQUNBd0Isb0JBQU0rVyxTQUFOO0FBQ0Q7QUFDRixXQUxEO0FBTUQ7QUFDRjs7QUFFRDs7Ozs7QUF0Tlc7QUFBQTtBQUFBLG1DQTBORTtBQUNYLFlBQUlDLFlBQVk3WSxXQUFXbUssUUFBWCxDQUFvQm9CLGFBQXBCLENBQWtDLEtBQUt0SyxRQUF2QyxDQUFoQjtBQUFBLFlBQ0lnVCxRQUFRNEUsVUFBVWpKLEVBQVYsQ0FBYSxDQUFiLENBRFo7QUFBQSxZQUVJa0osT0FBT0QsVUFBVWpKLEVBQVYsQ0FBYSxDQUFDLENBQWQsQ0FGWDs7QUFJQWlKLGtCQUFVaEQsR0FBVixDQUFjLGVBQWQsRUFBK0J6SSxFQUEvQixDQUFrQyxzQkFBbEMsRUFBMEQsVUFBUzFKLENBQVQsRUFBWTtBQUNwRSxjQUFJbEcsTUFBTXdDLFdBQVdtSyxRQUFYLENBQW9CRSxRQUFwQixDQUE2QjNHLENBQTdCLENBQVY7QUFDQSxjQUFJbEcsUUFBUSxLQUFSLElBQWlCa0csRUFBRTdGLE1BQUYsS0FBYWliLEtBQUssQ0FBTCxDQUFsQyxFQUEyQztBQUN6Q3BWLGNBQUV5TyxjQUFGO0FBQ0E4QixrQkFBTTBFLEtBQU47QUFDRDtBQUNELGNBQUluYixRQUFRLFdBQVIsSUFBdUJrRyxFQUFFN0YsTUFBRixLQUFhb1csTUFBTSxDQUFOLENBQXhDLEVBQWtEO0FBQ2hEdlEsY0FBRXlPLGNBQUY7QUFDQTJHLGlCQUFLSCxLQUFMO0FBQ0Q7QUFDRixTQVZEO0FBV0Q7O0FBRUQ7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQS9QVztBQUFBO0FBQUEsNEJBcVFMekosRUFyUUssRUFxUUQ7QUFDUixZQUFJLENBQUMsS0FBS2pPLFFBQUwsQ0FBY29YLFFBQWQsQ0FBdUIsU0FBdkIsQ0FBRCxJQUFzQyxLQUFLYixVQUEvQyxFQUEyRDtBQUFFO0FBQVM7O0FBRXRFLFlBQUkzVixRQUFRLElBQVo7O0FBRUE7QUFDQS9CLFVBQUUsMkJBQUYsRUFBK0J1RixXQUEvQixpQ0FBeUV4RCxNQUFNc1AsT0FBTixDQUFjeEgsUUFBdkY7QUFDQTlILGNBQU1aLFFBQU4sQ0FBZW9FLFdBQWYsQ0FBMkIsU0FBM0I7QUFDRTtBQUNGO0FBQ0EsYUFBS3BFLFFBQUwsQ0FBY1osSUFBZCxDQUFtQixhQUFuQixFQUFrQyxNQUFsQztBQUNFOzs7O0FBREYsU0FLS2MsT0FMTCxDQUthLHFCQUxiO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBSSxLQUFLZ1EsT0FBTCxDQUFhaUcsWUFBakIsRUFBK0I7QUFDN0IsZUFBS0MsT0FBTCxDQUFhaFMsV0FBYixDQUF5QixZQUF6QjtBQUNEOztBQUVELGFBQUs2UixTQUFMLENBQWU3VyxJQUFmLENBQW9CLGVBQXBCLEVBQXFDLE9BQXJDO0FBQ0EsWUFBSSxLQUFLOFEsT0FBTCxDQUFheUgsU0FBakIsRUFBNEI7QUFDMUI5WSxZQUFFLDJCQUFGLEVBQStCdUIsVUFBL0IsQ0FBMEMsVUFBMUM7QUFDRDtBQUNGOztBQUVEOzs7Ozs7O0FBclNXO0FBQUE7QUFBQSw2QkEyU0puRSxLQTNTSSxFQTJTR2lFLE9BM1NILEVBMlNZO0FBQ3JCLFlBQUksS0FBS0YsUUFBTCxDQUFjb1gsUUFBZCxDQUF1QixTQUF2QixDQUFKLEVBQXVDO0FBQ3JDLGVBQUtMLEtBQUwsQ0FBVzlhLEtBQVgsRUFBa0JpRSxPQUFsQjtBQUNELFNBRkQsTUFHSztBQUNILGVBQUs0VyxJQUFMLENBQVU3YSxLQUFWLEVBQWlCaUUsT0FBakI7QUFDRDtBQUNGOztBQUVEOzs7Ozs7QUFwVFc7QUFBQTtBQUFBLHNDQXlUS3VDLENBelRMLEVBeVRRO0FBQUE7O0FBQ2pCMUQsbUJBQVdtSyxRQUFYLENBQW9CUyxTQUFwQixDQUE4QmxILENBQTlCLEVBQWlDLFdBQWpDLEVBQThDO0FBQzVDc1UsaUJBQU8sWUFBTTtBQUNYLG1CQUFLQSxLQUFMO0FBQ0EsbUJBQUtmLFlBQUwsQ0FBa0IwQixLQUFsQjtBQUNBLG1CQUFPLElBQVA7QUFDRCxXQUwyQztBQU01Q3ROLG1CQUFTLFlBQU07QUFDYjNILGNBQUV3UixlQUFGO0FBQ0F4UixjQUFFeU8sY0FBRjtBQUNEO0FBVDJDLFNBQTlDO0FBV0Q7O0FBRUQ7Ozs7O0FBdlVXO0FBQUE7QUFBQSxnQ0EyVUQ7QUFDUixhQUFLNkYsS0FBTDtBQUNBLGFBQUsvVyxRQUFMLENBQWM0VSxHQUFkLENBQWtCLDJCQUFsQjtBQUNBLGFBQUt3QixPQUFMLENBQWF4QixHQUFiLENBQWlCLGVBQWpCOztBQUVBN1YsbUJBQVdvQixnQkFBWCxDQUE0QixJQUE1QjtBQUNEO0FBalZVOztBQUFBO0FBQUE7O0FBb1ZiMlYsWUFBVUMsUUFBVixHQUFxQjtBQUNuQjs7Ozs7QUFLQUksa0JBQWMsSUFOSzs7QUFRbkI7Ozs7O0FBS0FVLG9CQUFnQixDQWJHOztBQWVuQjs7Ozs7QUFLQW5PLGNBQVUsTUFwQlM7O0FBc0JuQjs7Ozs7QUFLQTRPLGNBQVUsSUEzQlM7O0FBNkJuQjs7Ozs7QUFLQWYsZ0JBQVksS0FsQ087O0FBb0NuQjs7Ozs7QUFLQUcsY0FBVSxJQXpDUzs7QUEyQ25COzs7OztBQUtBZSxlQUFXLElBaERROztBQWtEbkI7Ozs7OztBQU1BaEIsaUJBQWEsYUF4RE07O0FBMERuQjs7Ozs7QUFLQWtCLGVBQVc7O0FBR2I7QUFsRXFCLEdBQXJCLENBbUVBNVksV0FBV00sTUFBWCxDQUFrQnlXLFNBQWxCLEVBQTZCLFdBQTdCO0FBRUMsQ0F6WkEsQ0F5WkNwUCxNQXpaRCxDQUFEO0NDRkE7Ozs7OztBQUVBLENBQUMsVUFBUzdILENBQVQsRUFBWTs7QUFFYjs7Ozs7Ozs7OztBQUZhLE1BWVBpWixNQVpPO0FBYVg7Ozs7OztBQU1BLG9CQUFZL1EsT0FBWixFQUFxQm1KLE9BQXJCLEVBQThCO0FBQUE7O0FBQzVCLFdBQUtsUSxRQUFMLEdBQWdCK0csT0FBaEI7QUFDQSxXQUFLbUosT0FBTCxHQUFlclIsRUFBRXFMLE1BQUYsQ0FBUyxFQUFULEVBQWE0TixPQUFPL0IsUUFBcEIsRUFBOEIsS0FBSy9WLFFBQUwsQ0FBY0MsSUFBZCxFQUE5QixFQUFvRGlRLE9BQXBELENBQWY7QUFDQSxXQUFLdlAsS0FBTDs7QUFFQTVCLGlCQUFXWSxjQUFYLENBQTBCLElBQTFCLEVBQWdDLFFBQWhDO0FBQ0FaLGlCQUFXbUssUUFBWCxDQUFvQnVCLFFBQXBCLENBQTZCLFFBQTdCLEVBQXVDO0FBQ3JDLGlCQUFTLE1BRDRCO0FBRXJDLGlCQUFTLE1BRjRCO0FBR3JDLGtCQUFVLE9BSDJCO0FBSXJDLGVBQU8sYUFKOEI7QUFLckMscUJBQWE7QUFMd0IsT0FBdkM7QUFPRDs7QUFFRDs7Ozs7O0FBbENXO0FBQUE7QUFBQSw4QkFzQ0g7QUFDTixhQUFLa0MsRUFBTCxHQUFVLEtBQUszTSxRQUFMLENBQWNaLElBQWQsQ0FBbUIsSUFBbkIsQ0FBVjtBQUNBLGFBQUsyWSxRQUFMLEdBQWdCLEtBQWhCO0FBQ0EsYUFBS0MsTUFBTCxHQUFjLEVBQUNDLElBQUlsWixXQUFXc0YsVUFBWCxDQUFzQjhHLE9BQTNCLEVBQWQ7QUFDQSxhQUFLK00sUUFBTCxHQUFnQkMsYUFBaEI7O0FBRUEsYUFBS0MsT0FBTCxHQUFldlosbUJBQWlCLEtBQUs4TixFQUF0QixTQUE4QnJMLE1BQTlCLEdBQXVDekMsbUJBQWlCLEtBQUs4TixFQUF0QixRQUF2QyxHQUF1RTlOLHFCQUFtQixLQUFLOE4sRUFBeEIsUUFBdEY7QUFDQSxhQUFLeUwsT0FBTCxDQUFhaFosSUFBYixDQUFrQjtBQUNoQiwyQkFBaUIsS0FBS3VOLEVBRE47QUFFaEIsMkJBQWlCLElBRkQ7QUFHaEIsc0JBQVk7QUFISSxTQUFsQjs7QUFNQSxZQUFJLEtBQUt1RCxPQUFMLENBQWFtSSxVQUFiLElBQTJCLEtBQUtyWSxRQUFMLENBQWNvWCxRQUFkLENBQXVCLE1BQXZCLENBQS9CLEVBQStEO0FBQzdELGVBQUtsSCxPQUFMLENBQWFtSSxVQUFiLEdBQTBCLElBQTFCO0FBQ0EsZUFBS25JLE9BQUwsQ0FBYW9JLE9BQWIsR0FBdUIsS0FBdkI7QUFDRDtBQUNELFlBQUksS0FBS3BJLE9BQUwsQ0FBYW9JLE9BQWIsSUFBd0IsQ0FBQyxLQUFLQyxRQUFsQyxFQUE0QztBQUMxQyxlQUFLQSxRQUFMLEdBQWdCLEtBQUtDLFlBQUwsQ0FBa0IsS0FBSzdMLEVBQXZCLENBQWhCO0FBQ0Q7O0FBRUQsYUFBSzNNLFFBQUwsQ0FBY1osSUFBZCxDQUFtQjtBQUNmLGtCQUFRLFFBRE87QUFFZix5QkFBZSxJQUZBO0FBR2YsMkJBQWlCLEtBQUt1TixFQUhQO0FBSWYseUJBQWUsS0FBS0E7QUFKTCxTQUFuQjs7QUFPQSxZQUFHLEtBQUs0TCxRQUFSLEVBQWtCO0FBQ2hCLGVBQUt2WSxRQUFMLENBQWN5WSxNQUFkLEdBQXVCdlUsUUFBdkIsQ0FBZ0MsS0FBS3FVLFFBQXJDO0FBQ0QsU0FGRCxNQUVPO0FBQ0wsZUFBS3ZZLFFBQUwsQ0FBY3lZLE1BQWQsR0FBdUJ2VSxRQUF2QixDQUFnQ3JGLEVBQUUsTUFBRixDQUFoQztBQUNBLGVBQUttQixRQUFMLENBQWMrTyxRQUFkLENBQXVCLGlCQUF2QjtBQUNEO0FBQ0QsYUFBS21ILE9BQUw7QUFDQSxZQUFJLEtBQUtoRyxPQUFMLENBQWF3SSxRQUFiLElBQXlCM2QsT0FBTzRkLFFBQVAsQ0FBZ0JDLElBQWhCLFdBQStCLEtBQUtqTSxFQUFqRSxFQUF3RTtBQUN0RTlOLFlBQUU5RCxNQUFGLEVBQVVtVSxHQUFWLENBQWMsZ0JBQWQsRUFBZ0MsS0FBSzRILElBQUwsQ0FBVWxSLElBQVYsQ0FBZSxJQUFmLENBQWhDO0FBQ0Q7QUFDRjs7QUFFRDs7Ozs7QUE5RVc7QUFBQTtBQUFBLG1DQWtGRStHLEVBbEZGLEVBa0ZNO0FBQ2YsWUFBSTRMLFdBQVcxWixFQUFFLGFBQUYsRUFDRWtRLFFBREYsQ0FDVyxnQkFEWCxFQUVFN0ssUUFGRixDQUVXLE1BRlgsQ0FBZjtBQUdBLGVBQU9xVSxRQUFQO0FBQ0Q7O0FBRUQ7Ozs7OztBQXpGVztBQUFBO0FBQUEsd0NBOEZPO0FBQ2hCLFlBQUk1USxRQUFRLEtBQUszSCxRQUFMLENBQWM2WSxVQUFkLEVBQVo7QUFDQSxZQUFJQSxhQUFhaGEsRUFBRTlELE1BQUYsRUFBVTRNLEtBQVYsRUFBakI7QUFDQSxZQUFJRCxTQUFTLEtBQUsxSCxRQUFMLENBQWM4WSxXQUFkLEVBQWI7QUFDQSxZQUFJQSxjQUFjamEsRUFBRTlELE1BQUYsRUFBVTJNLE1BQVYsRUFBbEI7QUFDQSxZQUFJSixJQUFKLEVBQVVGLEdBQVY7QUFDQSxZQUFJLEtBQUs4SSxPQUFMLENBQWF0SCxPQUFiLEtBQXlCLE1BQTdCLEVBQXFDO0FBQ25DdEIsaUJBQU95UixTQUFTLENBQUNGLGFBQWFsUixLQUFkLElBQXVCLENBQWhDLEVBQW1DLEVBQW5DLENBQVA7QUFDRCxTQUZELE1BRU87QUFDTEwsaUJBQU95UixTQUFTLEtBQUs3SSxPQUFMLENBQWF0SCxPQUF0QixFQUErQixFQUEvQixDQUFQO0FBQ0Q7QUFDRCxZQUFJLEtBQUtzSCxPQUFMLENBQWF2SCxPQUFiLEtBQXlCLE1BQTdCLEVBQXFDO0FBQ25DLGNBQUlqQixTQUFTb1IsV0FBYixFQUEwQjtBQUN4QjFSLGtCQUFNMlIsU0FBU3ZYLEtBQUt3WCxHQUFMLENBQVMsR0FBVCxFQUFjRixjQUFjLEVBQTVCLENBQVQsRUFBMEMsRUFBMUMsQ0FBTjtBQUNELFdBRkQsTUFFTztBQUNMMVIsa0JBQU0yUixTQUFTLENBQUNELGNBQWNwUixNQUFmLElBQXlCLENBQWxDLEVBQXFDLEVBQXJDLENBQU47QUFDRDtBQUNGLFNBTkQsTUFNTztBQUNMTixnQkFBTTJSLFNBQVMsS0FBSzdJLE9BQUwsQ0FBYXZILE9BQXRCLEVBQStCLEVBQS9CLENBQU47QUFDRDtBQUNELGFBQUszSSxRQUFMLENBQWNzTCxHQUFkLENBQWtCLEVBQUNsRSxLQUFLQSxNQUFNLElBQVosRUFBbEI7QUFDQTtBQUNBO0FBQ0EsWUFBRyxDQUFDLEtBQUttUixRQUFOLElBQW1CLEtBQUtySSxPQUFMLENBQWF0SCxPQUFiLEtBQXlCLE1BQS9DLEVBQXdEO0FBQ3RELGVBQUs1SSxRQUFMLENBQWNzTCxHQUFkLENBQWtCLEVBQUNoRSxNQUFNQSxPQUFPLElBQWQsRUFBbEI7QUFDQSxlQUFLdEgsUUFBTCxDQUFjc0wsR0FBZCxDQUFrQixFQUFDMk4sUUFBUSxLQUFULEVBQWxCO0FBQ0Q7QUFFRjs7QUFFRDs7Ozs7QUE1SFc7QUFBQTtBQUFBLGdDQWdJRDtBQUFBOztBQUNSLFlBQUlyWSxRQUFRLElBQVo7O0FBRUEsYUFBS1osUUFBTCxDQUFjbU0sRUFBZCxDQUFpQjtBQUNmLDZCQUFtQixLQUFLMkssSUFBTCxDQUFVbFIsSUFBVixDQUFlLElBQWYsQ0FESjtBQUVmLDhCQUFvQixVQUFDM0osS0FBRCxFQUFRK0QsUUFBUixFQUFxQjtBQUN2QyxnQkFBSy9ELE1BQU1XLE1BQU4sS0FBaUJnRSxNQUFNWixRQUFOLENBQWUsQ0FBZixDQUFsQixJQUNDbkIsRUFBRTVDLE1BQU1XLE1BQVIsRUFBZ0JzYyxPQUFoQixDQUF3QixpQkFBeEIsRUFBMkMsQ0FBM0MsTUFBa0RsWixRQUR2RCxFQUNrRTtBQUFFO0FBQ2xFLHFCQUFPLE9BQUsrVyxLQUFMLENBQVdqVCxLQUFYLENBQWlCLE1BQWpCLENBQVA7QUFDRDtBQUNGLFdBUGM7QUFRZiwrQkFBcUIsS0FBS2tULE1BQUwsQ0FBWXBSLElBQVosQ0FBaUIsSUFBakIsQ0FSTjtBQVNmLGlDQUF1QixZQUFXO0FBQ2hDaEYsa0JBQU11WSxlQUFOO0FBQ0Q7QUFYYyxTQUFqQjs7QUFjQSxZQUFJLEtBQUtmLE9BQUwsQ0FBYTlXLE1BQWpCLEVBQXlCO0FBQ3ZCLGVBQUs4VyxPQUFMLENBQWFqTSxFQUFiLENBQWdCLG1CQUFoQixFQUFxQyxVQUFTMUosQ0FBVCxFQUFZO0FBQy9DLGdCQUFJQSxFQUFFL0UsS0FBRixLQUFZLEVBQVosSUFBa0IrRSxFQUFFL0UsS0FBRixLQUFZLEVBQWxDLEVBQXNDO0FBQ3BDK0UsZ0JBQUV3UixlQUFGO0FBQ0F4UixnQkFBRXlPLGNBQUY7QUFDQXRRLG9CQUFNa1csSUFBTjtBQUNEO0FBQ0YsV0FORDtBQU9EOztBQUVELFlBQUksS0FBSzVHLE9BQUwsQ0FBYWlHLFlBQWIsSUFBNkIsS0FBS2pHLE9BQUwsQ0FBYW9JLE9BQTlDLEVBQXVEO0FBQ3JELGVBQUtDLFFBQUwsQ0FBYzNELEdBQWQsQ0FBa0IsWUFBbEIsRUFBZ0N6SSxFQUFoQyxDQUFtQyxpQkFBbkMsRUFBc0QsVUFBUzFKLENBQVQsRUFBWTtBQUNoRSxnQkFBSUEsRUFBRTdGLE1BQUYsS0FBYWdFLE1BQU1aLFFBQU4sQ0FBZSxDQUFmLENBQWIsSUFDRm5CLEVBQUV1YSxRQUFGLENBQVd4WSxNQUFNWixRQUFOLENBQWUsQ0FBZixDQUFYLEVBQThCeUMsRUFBRTdGLE1BQWhDLENBREUsSUFFQSxDQUFDaUMsRUFBRXVhLFFBQUYsQ0FBV3BiLFFBQVgsRUFBcUJ5RSxFQUFFN0YsTUFBdkIsQ0FGTCxFQUVxQztBQUMvQjtBQUNMO0FBQ0RnRSxrQkFBTW1XLEtBQU47QUFDRCxXQVBEO0FBUUQ7QUFDRCxZQUFJLEtBQUs3RyxPQUFMLENBQWF3SSxRQUFqQixFQUEyQjtBQUN6QjdaLFlBQUU5RCxNQUFGLEVBQVVvUixFQUFWLHlCQUFtQyxLQUFLUSxFQUF4QyxFQUE4QyxLQUFLME0sWUFBTCxDQUFrQnpULElBQWxCLENBQXVCLElBQXZCLENBQTlDO0FBQ0Q7QUFDRjs7QUFFRDs7Ozs7QUExS1c7QUFBQTtBQUFBLG1DQThLRW5ELENBOUtGLEVBOEtLO0FBQ2QsWUFBRzFILE9BQU80ZCxRQUFQLENBQWdCQyxJQUFoQixLQUEyQixNQUFNLEtBQUtqTSxFQUF0QyxJQUE2QyxDQUFDLEtBQUtvTCxRQUF0RCxFQUErRDtBQUFFLGVBQUtqQixJQUFMO0FBQWMsU0FBL0UsTUFDSTtBQUFFLGVBQUtDLEtBQUw7QUFBZTtBQUN0Qjs7QUFHRDs7Ozs7OztBQXBMVztBQUFBO0FBQUEsNkJBMExKO0FBQUE7O0FBQ0wsWUFBSSxLQUFLN0csT0FBTCxDQUFhd0ksUUFBakIsRUFBMkI7QUFDekIsY0FBSUUsYUFBVyxLQUFLak0sRUFBcEI7O0FBRUEsY0FBSTVSLE9BQU91ZSxPQUFQLENBQWVDLFNBQW5CLEVBQThCO0FBQzVCeGUsbUJBQU91ZSxPQUFQLENBQWVDLFNBQWYsQ0FBeUIsSUFBekIsRUFBK0IsSUFBL0IsRUFBcUNYLElBQXJDO0FBQ0QsV0FGRCxNQUVPO0FBQ0w3ZCxtQkFBTzRkLFFBQVAsQ0FBZ0JDLElBQWhCLEdBQXVCQSxJQUF2QjtBQUNEO0FBQ0Y7O0FBRUQsYUFBS2IsUUFBTCxHQUFnQixJQUFoQjs7QUFFQTtBQUNBLGFBQUsvWCxRQUFMLENBQ0tzTCxHQURMLENBQ1MsRUFBRSxjQUFjLFFBQWhCLEVBRFQsRUFFSzBELElBRkwsR0FHS3VJLFNBSEwsQ0FHZSxDQUhmO0FBSUEsWUFBSSxLQUFLckgsT0FBTCxDQUFhb0ksT0FBakIsRUFBMEI7QUFDeEIsZUFBS0MsUUFBTCxDQUFjak4sR0FBZCxDQUFrQixFQUFDLGNBQWMsUUFBZixFQUFsQixFQUE0QzBELElBQTVDO0FBQ0Q7O0FBRUQsYUFBS21LLGVBQUw7O0FBRUEsYUFBS25aLFFBQUwsQ0FDR29QLElBREgsR0FFRzlELEdBRkgsQ0FFTyxFQUFFLGNBQWMsRUFBaEIsRUFGUDs7QUFJQSxZQUFHLEtBQUtpTixRQUFSLEVBQWtCO0FBQ2hCLGVBQUtBLFFBQUwsQ0FBY2pOLEdBQWQsQ0FBa0IsRUFBQyxjQUFjLEVBQWYsRUFBbEIsRUFBc0M4RCxJQUF0QztBQUNBLGNBQUcsS0FBS3BQLFFBQUwsQ0FBY29YLFFBQWQsQ0FBdUIsTUFBdkIsQ0FBSCxFQUFtQztBQUNqQyxpQkFBS21CLFFBQUwsQ0FBY3hKLFFBQWQsQ0FBdUIsTUFBdkI7QUFDRCxXQUZELE1BRU8sSUFBSSxLQUFLL08sUUFBTCxDQUFjb1gsUUFBZCxDQUF1QixNQUF2QixDQUFKLEVBQW9DO0FBQ3pDLGlCQUFLbUIsUUFBTCxDQUFjeEosUUFBZCxDQUF1QixNQUF2QjtBQUNEO0FBQ0Y7O0FBR0QsWUFBSSxDQUFDLEtBQUttQixPQUFMLENBQWFzSixjQUFsQixFQUFrQztBQUNoQzs7Ozs7QUFLQSxlQUFLeFosUUFBTCxDQUFjRSxPQUFkLENBQXNCLG1CQUF0QixFQUEyQyxLQUFLeU0sRUFBaEQ7QUFDRDtBQUNEO0FBQ0EsWUFBSSxLQUFLdUQsT0FBTCxDQUFhdUosV0FBakIsRUFBOEI7QUFBQSxjQUVuQkMsbUJBRm1CLEdBRTVCLFlBQThCO0FBQzVCOVksa0JBQU1aLFFBQU4sQ0FDR1osSUFESCxDQUNRO0FBQ0osNkJBQWUsS0FEWDtBQUVKLDBCQUFZLENBQUM7QUFGVCxhQURSLEVBS0dzWSxLQUxIO0FBTUQsV0FUMkI7O0FBQzVCLGNBQUk5VyxRQUFRLElBQVo7O0FBU0EsY0FBSSxLQUFLc1AsT0FBTCxDQUFhb0ksT0FBakIsRUFBMEI7QUFDeEJ2Wix1QkFBVytPLE1BQVgsQ0FBa0JDLFNBQWxCLENBQTRCLEtBQUt3SyxRQUFqQyxFQUEyQyxTQUEzQztBQUNEO0FBQ0R4WixxQkFBVytPLE1BQVgsQ0FBa0JDLFNBQWxCLENBQTRCLEtBQUsvTixRQUFqQyxFQUEyQyxLQUFLa1EsT0FBTCxDQUFhdUosV0FBeEQsRUFBcUUsWUFBTTtBQUN6RSxtQkFBS0UsaUJBQUwsR0FBeUI1YSxXQUFXbUssUUFBWCxDQUFvQm9CLGFBQXBCLENBQWtDLE9BQUt0SyxRQUF2QyxDQUF6QjtBQUNBMFo7QUFDRCxXQUhEO0FBSUQ7QUFDRDtBQWxCQSxhQW1CSztBQUNILGdCQUFJLEtBQUt4SixPQUFMLENBQWFvSSxPQUFqQixFQUEwQjtBQUN4QixtQkFBS0MsUUFBTCxDQUFjdkosSUFBZCxDQUFtQixDQUFuQjtBQUNEO0FBQ0QsaUJBQUtoUCxRQUFMLENBQWNnUCxJQUFkLENBQW1CLEtBQUtrQixPQUFMLENBQWEwSixTQUFoQztBQUNEOztBQUVEO0FBQ0EsYUFBSzVaLFFBQUwsQ0FDR1osSUFESCxDQUNRO0FBQ0oseUJBQWUsS0FEWDtBQUVKLHNCQUFZLENBQUM7QUFGVCxTQURSLEVBS0dzWSxLQUxIOztBQU9BOzs7O0FBSUEsYUFBSzFYLFFBQUwsQ0FBY0UsT0FBZCxDQUFzQixnQkFBdEI7O0FBRUEsWUFBSSxLQUFLZ1ksUUFBVCxFQUFtQjtBQUNqQixlQUFLMkIsaUJBQUwsR0FBeUI5ZSxPQUFPc04sV0FBaEM7QUFDQXhKLFlBQUUsWUFBRixFQUFnQmtRLFFBQWhCLENBQXlCLGdCQUF6QjtBQUNELFNBSEQsTUFJSztBQUNIbFEsWUFBRSxNQUFGLEVBQVVrUSxRQUFWLENBQW1CLGdCQUFuQjtBQUNEOztBQUVEN1MsbUJBQVcsWUFBTTtBQUNmLGlCQUFLNGQsY0FBTDtBQUNELFNBRkQsRUFFRyxDQUZIO0FBR0Q7O0FBRUQ7Ozs7O0FBOVJXO0FBQUE7QUFBQSx1Q0FrU007QUFDZixZQUFJbFosUUFBUSxJQUFaO0FBQ0EsYUFBSytZLGlCQUFMLEdBQXlCNWEsV0FBV21LLFFBQVgsQ0FBb0JvQixhQUFwQixDQUFrQyxLQUFLdEssUUFBdkMsQ0FBekI7O0FBRUEsWUFBSSxDQUFDLEtBQUtrUSxPQUFMLENBQWFvSSxPQUFkLElBQXlCLEtBQUtwSSxPQUFMLENBQWFpRyxZQUF0QyxJQUFzRCxDQUFDLEtBQUtqRyxPQUFMLENBQWFtSSxVQUF4RSxFQUFvRjtBQUNsRnhaLFlBQUUsTUFBRixFQUFVc04sRUFBVixDQUFhLGlCQUFiLEVBQWdDLFVBQVMxSixDQUFULEVBQVk7QUFDMUMsZ0JBQUlBLEVBQUU3RixNQUFGLEtBQWFnRSxNQUFNWixRQUFOLENBQWUsQ0FBZixDQUFiLElBQ0ZuQixFQUFFdWEsUUFBRixDQUFXeFksTUFBTVosUUFBTixDQUFlLENBQWYsQ0FBWCxFQUE4QnlDLEVBQUU3RixNQUFoQyxDQURFLElBRUEsQ0FBQ2lDLEVBQUV1YSxRQUFGLENBQVdwYixRQUFYLEVBQXFCeUUsRUFBRTdGLE1BQXZCLENBRkwsRUFFcUM7QUFBRTtBQUFTO0FBQ2hEZ0Usa0JBQU1tVyxLQUFOO0FBQ0QsV0FMRDtBQU1EOztBQUVELFlBQUksS0FBSzdHLE9BQUwsQ0FBYTZKLFVBQWpCLEVBQTZCO0FBQzNCbGIsWUFBRTlELE1BQUYsRUFBVW9SLEVBQVYsQ0FBYSxtQkFBYixFQUFrQyxVQUFTMUosQ0FBVCxFQUFZO0FBQzVDMUQsdUJBQVdtSyxRQUFYLENBQW9CUyxTQUFwQixDQUE4QmxILENBQTlCLEVBQWlDLFFBQWpDLEVBQTJDO0FBQ3pDc1UscUJBQU8sWUFBVztBQUNoQixvQkFBSW5XLE1BQU1zUCxPQUFOLENBQWM2SixVQUFsQixFQUE4QjtBQUM1Qm5aLHdCQUFNbVcsS0FBTjtBQUNBblcsd0JBQU13WCxPQUFOLENBQWNWLEtBQWQ7QUFDRDtBQUNGO0FBTndDLGFBQTNDO0FBUUQsV0FURDtBQVVEOztBQUVEO0FBQ0EsYUFBSzFYLFFBQUwsQ0FBY21NLEVBQWQsQ0FBaUIsbUJBQWpCLEVBQXNDLFVBQVMxSixDQUFULEVBQVk7QUFDaEQsY0FBSTRTLFVBQVV4VyxFQUFFLElBQUYsQ0FBZDtBQUNBO0FBQ0FFLHFCQUFXbUssUUFBWCxDQUFvQlMsU0FBcEIsQ0FBOEJsSCxDQUE5QixFQUFpQyxRQUFqQyxFQUEyQztBQUN6Q3VYLHlCQUFhLFlBQVc7QUFDdEJwWixvQkFBTStZLGlCQUFOLEdBQTBCNWEsV0FBV21LLFFBQVgsQ0FBb0JvQixhQUFwQixDQUFrQzFKLE1BQU1aLFFBQXhDLENBQTFCO0FBQ0Esa0JBQUlZLE1BQU1aLFFBQU4sQ0FBZWtDLElBQWYsQ0FBb0IsUUFBcEIsRUFBOEJzSSxFQUE5QixDQUFpQzVKLE1BQU0rWSxpQkFBTixDQUF3QmhMLEVBQXhCLENBQTJCLENBQUMsQ0FBNUIsQ0FBakMsQ0FBSixFQUFzRTtBQUFFO0FBQ3RFL04sc0JBQU0rWSxpQkFBTixDQUF3QmhMLEVBQXhCLENBQTJCLENBQTNCLEVBQThCK0ksS0FBOUI7QUFDQSx1QkFBTyxJQUFQO0FBQ0Q7QUFDRCxrQkFBSTlXLE1BQU0rWSxpQkFBTixDQUF3QnJZLE1BQXhCLEtBQW1DLENBQXZDLEVBQTBDO0FBQUU7QUFDMUMsdUJBQU8sSUFBUDtBQUNEO0FBQ0YsYUFWd0M7QUFXekMyWSwwQkFBYyxZQUFXO0FBQ3ZCclosb0JBQU0rWSxpQkFBTixHQUEwQjVhLFdBQVdtSyxRQUFYLENBQW9Cb0IsYUFBcEIsQ0FBa0MxSixNQUFNWixRQUF4QyxDQUExQjtBQUNBLGtCQUFJWSxNQUFNWixRQUFOLENBQWVrQyxJQUFmLENBQW9CLFFBQXBCLEVBQThCc0ksRUFBOUIsQ0FBaUM1SixNQUFNK1ksaUJBQU4sQ0FBd0JoTCxFQUF4QixDQUEyQixDQUEzQixDQUFqQyxLQUFtRS9OLE1BQU1aLFFBQU4sQ0FBZXdLLEVBQWYsQ0FBa0IsUUFBbEIsQ0FBdkUsRUFBb0c7QUFBRTtBQUNwRzVKLHNCQUFNK1ksaUJBQU4sQ0FBd0JoTCxFQUF4QixDQUEyQixDQUFDLENBQTVCLEVBQStCK0ksS0FBL0I7QUFDQSx1QkFBTyxJQUFQO0FBQ0Q7QUFDRCxrQkFBSTlXLE1BQU0rWSxpQkFBTixDQUF3QnJZLE1BQXhCLEtBQW1DLENBQXZDLEVBQTBDO0FBQUU7QUFDMUMsdUJBQU8sSUFBUDtBQUNEO0FBQ0YsYUFwQndDO0FBcUJ6Q3dWLGtCQUFNLFlBQVc7QUFDZixrQkFBSWxXLE1BQU1aLFFBQU4sQ0FBZWtDLElBQWYsQ0FBb0IsUUFBcEIsRUFBOEJzSSxFQUE5QixDQUFpQzVKLE1BQU1aLFFBQU4sQ0FBZWtDLElBQWYsQ0FBb0IsY0FBcEIsQ0FBakMsQ0FBSixFQUEyRTtBQUN6RWhHLDJCQUFXLFlBQVc7QUFBRTtBQUN0QjBFLHdCQUFNd1gsT0FBTixDQUFjVixLQUFkO0FBQ0QsaUJBRkQsRUFFRyxDQUZIO0FBR0QsZUFKRCxNQUlPLElBQUlyQyxRQUFRN0ssRUFBUixDQUFXNUosTUFBTStZLGlCQUFqQixDQUFKLEVBQXlDO0FBQUU7QUFDaEQvWSxzQkFBTWtXLElBQU47QUFDRDtBQUNGLGFBN0J3QztBQThCekNDLG1CQUFPLFlBQVc7QUFDaEIsa0JBQUluVyxNQUFNc1AsT0FBTixDQUFjNkosVUFBbEIsRUFBOEI7QUFDNUJuWixzQkFBTW1XLEtBQU47QUFDQW5XLHNCQUFNd1gsT0FBTixDQUFjVixLQUFkO0FBQ0Q7QUFDRixhQW5Dd0M7QUFvQ3pDdE4scUJBQVMsVUFBUzhHLGNBQVQsRUFBeUI7QUFDaEMsa0JBQUlBLGNBQUosRUFBb0I7QUFDbEJ6TyxrQkFBRXlPLGNBQUY7QUFDRDtBQUNGO0FBeEN3QyxXQUEzQztBQTBDRCxTQTdDRDtBQThDRDs7QUFFRDs7Ozs7O0FBN1dXO0FBQUE7QUFBQSw4QkFrWEg7QUFDTixZQUFJLENBQUMsS0FBSzZHLFFBQU4sSUFBa0IsQ0FBQyxLQUFLL1gsUUFBTCxDQUFjd0ssRUFBZCxDQUFpQixVQUFqQixDQUF2QixFQUFxRDtBQUNuRCxpQkFBTyxLQUFQO0FBQ0Q7QUFDRCxZQUFJNUosUUFBUSxJQUFaOztBQUVBO0FBQ0EsWUFBSSxLQUFLc1AsT0FBTCxDQUFhZ0ssWUFBakIsRUFBK0I7QUFDN0IsY0FBSSxLQUFLaEssT0FBTCxDQUFhb0ksT0FBakIsRUFBMEI7QUFDeEJ2Wix1QkFBVytPLE1BQVgsQ0FBa0JLLFVBQWxCLENBQTZCLEtBQUtvSyxRQUFsQyxFQUE0QyxVQUE1QyxFQUF3RDRCLFFBQXhEO0FBQ0QsV0FGRCxNQUdLO0FBQ0hBO0FBQ0Q7O0FBRURwYixxQkFBVytPLE1BQVgsQ0FBa0JLLFVBQWxCLENBQTZCLEtBQUtuTyxRQUFsQyxFQUE0QyxLQUFLa1EsT0FBTCxDQUFhZ0ssWUFBekQ7QUFDRDtBQUNEO0FBVkEsYUFXSztBQUNILGdCQUFJLEtBQUtoSyxPQUFMLENBQWFvSSxPQUFqQixFQUEwQjtBQUN4QixtQkFBS0MsUUFBTCxDQUFjbkosSUFBZCxDQUFtQixDQUFuQixFQUFzQitLLFFBQXRCO0FBQ0QsYUFGRCxNQUdLO0FBQ0hBO0FBQ0Q7O0FBRUQsaUJBQUtuYSxRQUFMLENBQWNvUCxJQUFkLENBQW1CLEtBQUtjLE9BQUwsQ0FBYWtLLFNBQWhDO0FBQ0Q7O0FBRUQ7QUFDQSxZQUFJLEtBQUtsSyxPQUFMLENBQWE2SixVQUFqQixFQUE2QjtBQUMzQmxiLFlBQUU5RCxNQUFGLEVBQVU2WixHQUFWLENBQWMsbUJBQWQ7QUFDRDs7QUFFRCxZQUFJLENBQUMsS0FBSzFFLE9BQUwsQ0FBYW9JLE9BQWQsSUFBeUIsS0FBS3BJLE9BQUwsQ0FBYWlHLFlBQTFDLEVBQXdEO0FBQ3REdFgsWUFBRSxNQUFGLEVBQVUrVixHQUFWLENBQWMsaUJBQWQ7QUFDRDs7QUFFRCxhQUFLNVUsUUFBTCxDQUFjNFUsR0FBZCxDQUFrQixtQkFBbEI7O0FBRUEsaUJBQVN1RixRQUFULEdBQW9CO0FBQ2xCLGNBQUl2WixNQUFNc1gsUUFBVixFQUFvQjtBQUNsQnJaLGNBQUUsWUFBRixFQUFnQnVGLFdBQWhCLENBQTRCLGdCQUE1QjtBQUNBLGdCQUFHeEQsTUFBTWlaLGlCQUFULEVBQTRCO0FBQzFCaGIsZ0JBQUUsTUFBRixFQUFVMFksU0FBVixDQUFvQjNXLE1BQU1pWixpQkFBMUI7QUFDQWpaLG9CQUFNaVosaUJBQU4sR0FBMEIsSUFBMUI7QUFDRDtBQUNGLFdBTkQsTUFPSztBQUNIaGIsY0FBRSxNQUFGLEVBQVV1RixXQUFWLENBQXNCLGdCQUF0QjtBQUNEOztBQUVEeEQsZ0JBQU1aLFFBQU4sQ0FBZVosSUFBZixDQUFvQixhQUFwQixFQUFtQyxJQUFuQzs7QUFFQTs7OztBQUlBd0IsZ0JBQU1aLFFBQU4sQ0FBZUUsT0FBZixDQUF1QixrQkFBdkI7QUFDRDs7QUFFRDs7OztBQUlBLFlBQUksS0FBS2dRLE9BQUwsQ0FBYW1LLFlBQWpCLEVBQStCO0FBQzdCLGVBQUtyYSxRQUFMLENBQWNzYSxJQUFkLENBQW1CLEtBQUt0YSxRQUFMLENBQWNzYSxJQUFkLEVBQW5CO0FBQ0Q7O0FBRUQsYUFBS3ZDLFFBQUwsR0FBZ0IsS0FBaEI7QUFDQyxZQUFJblgsTUFBTXNQLE9BQU4sQ0FBY3dJLFFBQWxCLEVBQTRCO0FBQzFCLGNBQUkzZCxPQUFPdWUsT0FBUCxDQUFlaUIsWUFBbkIsRUFBaUM7QUFDL0J4ZixtQkFBT3VlLE9BQVAsQ0FBZWlCLFlBQWYsQ0FBNEIsRUFBNUIsRUFBZ0N2YyxTQUFTd2MsS0FBekMsRUFBZ0R6ZixPQUFPNGQsUUFBUCxDQUFnQjhCLFFBQWhFO0FBQ0QsV0FGRCxNQUVPO0FBQ0wxZixtQkFBTzRkLFFBQVAsQ0FBZ0JDLElBQWhCLEdBQXVCLEVBQXZCO0FBQ0Q7QUFDRjtBQUNIOztBQUVEOzs7OztBQWpjVztBQUFBO0FBQUEsK0JBcWNGO0FBQ1AsWUFBSSxLQUFLYixRQUFULEVBQW1CO0FBQ2pCLGVBQUtoQixLQUFMO0FBQ0QsU0FGRCxNQUVPO0FBQ0wsZUFBS0QsSUFBTDtBQUNEO0FBQ0Y7QUEzY1U7QUFBQTs7O0FBNmNYOzs7O0FBN2NXLGdDQWlkRDtBQUNSLFlBQUksS0FBSzVHLE9BQUwsQ0FBYW9JLE9BQWpCLEVBQTBCO0FBQ3hCLGVBQUt0WSxRQUFMLENBQWNrRSxRQUFkLENBQXVCckYsRUFBRSxNQUFGLENBQXZCLEVBRHdCLENBQ1c7QUFDbkMsZUFBSzBaLFFBQUwsQ0FBY25KLElBQWQsR0FBcUJ3RixHQUFyQixHQUEyQjhGLE1BQTNCO0FBQ0Q7QUFDRCxhQUFLMWEsUUFBTCxDQUFjb1AsSUFBZCxHQUFxQndGLEdBQXJCO0FBQ0EsYUFBS3dELE9BQUwsQ0FBYXhELEdBQWIsQ0FBaUIsS0FBakI7QUFDQS9WLFVBQUU5RCxNQUFGLEVBQVU2WixHQUFWLGlCQUE0QixLQUFLakksRUFBakM7O0FBRUE1TixtQkFBV29CLGdCQUFYLENBQTRCLElBQTVCO0FBQ0Q7QUEzZFU7O0FBQUE7QUFBQTs7QUE4ZGIyWCxTQUFPL0IsUUFBUCxHQUFrQjtBQUNoQjs7Ozs7QUFLQTBELGlCQUFhLEVBTkc7QUFPaEI7Ozs7O0FBS0FTLGtCQUFjLEVBWkU7QUFhaEI7Ozs7O0FBS0FOLGVBQVcsQ0FsQks7QUFtQmhCOzs7OztBQUtBUSxlQUFXLENBeEJLO0FBeUJoQjs7Ozs7QUFLQWpFLGtCQUFjLElBOUJFO0FBK0JoQjs7Ozs7QUFLQTRELGdCQUFZLElBcENJO0FBcUNoQjs7Ozs7QUFLQVAsb0JBQWdCLEtBMUNBO0FBMkNoQjs7Ozs7QUFLQTdRLGFBQVMsTUFoRE87QUFpRGhCOzs7OztBQUtBQyxhQUFTLE1BdERPO0FBdURoQjs7Ozs7QUFLQXlQLGdCQUFZLEtBNURJO0FBNkRoQjs7Ozs7QUFLQXNDLGtCQUFjLEVBbEVFO0FBbUVoQjs7Ozs7QUFLQXJDLGFBQVMsSUF4RU87QUF5RWhCOzs7OztBQUtBK0Isa0JBQWMsS0E5RUU7QUErRWhCOzs7OztBQUtBM0IsY0FBVTtBQXBGTSxHQUFsQjs7QUF1RkE7QUFDQTNaLGFBQVdNLE1BQVgsQ0FBa0J5WSxNQUFsQixFQUEwQixRQUExQjs7QUFFQSxXQUFTOEMsV0FBVCxHQUF1QjtBQUNyQixXQUFPLHNCQUFxQjFWLElBQXJCLENBQTBCbkssT0FBT29LLFNBQVAsQ0FBaUJDLFNBQTNDO0FBQVA7QUFDRDs7QUFFRCxXQUFTeVYsWUFBVCxHQUF3QjtBQUN0QixXQUFPLFdBQVUzVixJQUFWLENBQWVuSyxPQUFPb0ssU0FBUCxDQUFpQkMsU0FBaEM7QUFBUDtBQUNEOztBQUVELFdBQVMrUyxXQUFULEdBQXVCO0FBQ3JCLFdBQU95QyxpQkFBaUJDLGNBQXhCO0FBQ0Q7QUFFQSxDQXBrQkEsQ0Fva0JDblUsTUFwa0JELENBQUQ7OztBQ0ZBOzs7Ozs7Ozs7QUFTQSxDQUFDLENBQUMsVUFBUzNMLE1BQVQsRUFBaUI7QUFDZjs7QUFFQSxRQUFJK2YsUUFBUUEsU0FBUyxFQUFyQjtBQUNBLFFBQUlDLEtBQUsvYyxTQUFTa1gsZ0JBQVQsQ0FBMEJ0UCxJQUExQixDQUErQjVILFFBQS9CLENBQVQ7O0FBRUE7QUFDQSxhQUFTZ2QsUUFBVCxDQUFrQkMsR0FBbEIsRUFBdUI7QUFDbkIsZUFBT0EsUUFBUSxJQUFSLElBQWdCQSxRQUFRQSxJQUFJbGdCLE1BQW5DO0FBQ0g7O0FBRUQsYUFBU21nQixTQUFULENBQW1CblosSUFBbkIsRUFBeUI7QUFDckIsZUFBT2laLFNBQVNqWixJQUFULElBQWlCQSxJQUFqQixHQUF3QkEsS0FBS29aLFFBQUwsS0FBa0IsQ0FBbEIsSUFBdUJwWixLQUFLcVosV0FBM0Q7QUFDSDs7QUFFRCxhQUFTM1QsTUFBVCxDQUFnQjFGLElBQWhCLEVBQXNCO0FBQ2xCLFlBQUlzWixPQUFKO0FBQUEsWUFBYUMsR0FBYjtBQUFBLFlBQ0lDLE1BQU0sRUFBQ25VLEtBQUssQ0FBTixFQUFTRSxNQUFNLENBQWYsRUFEVjtBQUFBLFlBRUlrVSxNQUFNelosUUFBUUEsS0FBSzBaLGFBRnZCOztBQUlBSixrQkFBVUcsSUFBSXZLLGVBQWQ7O0FBRUEsWUFBSSxPQUFPbFAsS0FBS2lHLHFCQUFaLEtBQXNDLE9BQU8xSixTQUFqRCxFQUE0RDtBQUN4RGlkLGtCQUFNeFosS0FBS2lHLHFCQUFMLEVBQU47QUFDSDtBQUNEc1QsY0FBTUosVUFBVU0sR0FBVixDQUFOO0FBQ0EsZUFBTztBQUNIcFUsaUJBQUttVSxJQUFJblUsR0FBSixHQUFVa1UsSUFBSWpULFdBQWQsR0FBNEJnVCxRQUFRSyxTQUR0QztBQUVIcFUsa0JBQU1pVSxJQUFJalUsSUFBSixHQUFXZ1UsSUFBSS9TLFdBQWYsR0FBNkI4UyxRQUFRTTtBQUZ4QyxTQUFQO0FBSUg7O0FBRUQsYUFBU0MsWUFBVCxDQUFzQlgsR0FBdEIsRUFBMkI7QUFDdkIsWUFBSTVYLFFBQVEsRUFBWjs7QUFFQSxhQUFLLElBQUl3WSxDQUFULElBQWNaLEdBQWQsRUFBbUI7QUFDZixnQkFBSUEsSUFBSXhQLGNBQUosQ0FBbUJvUSxDQUFuQixDQUFKLEVBQTJCO0FBQ3ZCeFkseUJBQVV3WSxJQUFJLEdBQUosR0FBVVosSUFBSVksQ0FBSixDQUFWLEdBQW1CLEdBQTdCO0FBQ0g7QUFDSjs7QUFFRCxlQUFPeFksS0FBUDtBQUNIOztBQUVELFFBQUl5WSxTQUFTOztBQUVUO0FBQ0F6TixrQkFBVSxHQUhEOztBQUtUVyxjQUFNLFVBQVN2TSxDQUFULEVBQVlzRSxPQUFaLEVBQXFCOztBQUV2QjtBQUNBLGdCQUFJdEUsRUFBRXNaLE1BQUYsS0FBYSxDQUFqQixFQUFvQjtBQUNoQix1QkFBTyxLQUFQO0FBQ0g7O0FBRUQsZ0JBQUluWixLQUFLbUUsV0FBVyxJQUFwQjs7QUFFQTtBQUNBLGdCQUFJaVYsU0FBU2hlLFNBQVNJLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBYjtBQUNBNGQsbUJBQU96YyxTQUFQLEdBQW1CLGNBQW5CO0FBQ0FxRCxlQUFHcVosV0FBSCxDQUFlRCxNQUFmOztBQUVBO0FBQ0EsZ0JBQUlFLE1BQWN6VSxPQUFPN0UsRUFBUCxDQUFsQjtBQUNBLGdCQUFJdVosWUFBZTFaLEVBQUV3UCxLQUFGLEdBQVVpSyxJQUFJOVUsR0FBakM7QUFDQSxnQkFBSWdWLFlBQWUzWixFQUFFc1AsS0FBRixHQUFVbUssSUFBSTVVLElBQWpDO0FBQ0EsZ0JBQUkrVSxRQUFjLFdBQVd6WixHQUFHMFosV0FBSCxHQUFpQixHQUFsQixHQUF5QixFQUFuQyxHQUF1QyxHQUF6RDs7QUFFQTtBQUNBLGdCQUFJLGFBQWE3WixDQUFqQixFQUFvQjtBQUNsQjBaLDRCQUFlMVosRUFBRXFQLE9BQUYsQ0FBVSxDQUFWLEVBQWFHLEtBQWIsR0FBcUJpSyxJQUFJOVUsR0FBeEM7QUFDQWdWLDRCQUFlM1osRUFBRXFQLE9BQUYsQ0FBVSxDQUFWLEVBQWFDLEtBQWIsR0FBcUJtSyxJQUFJNVUsSUFBeEM7QUFDRDs7QUFFRDtBQUNBMFUsbUJBQU96ZSxZQUFQLENBQW9CLFdBQXBCLEVBQWlDb0gsS0FBS0MsR0FBTCxFQUFqQztBQUNBb1gsbUJBQU96ZSxZQUFQLENBQW9CLFlBQXBCLEVBQWtDOGUsS0FBbEM7QUFDQUwsbUJBQU96ZSxZQUFQLENBQW9CLFFBQXBCLEVBQThCNmUsU0FBOUI7QUFDQUosbUJBQU96ZSxZQUFQLENBQW9CLFFBQXBCLEVBQThCNGUsU0FBOUI7O0FBRUE7QUFDQSxnQkFBSUksY0FBYztBQUNkLHVCQUFPSixZQUFVLElBREg7QUFFZCx3QkFBUUMsWUFBVTtBQUZKLGFBQWxCOztBQUtBSixtQkFBT3pjLFNBQVAsR0FBbUJ5YyxPQUFPemMsU0FBUCxHQUFtQixxQkFBdEM7QUFDQXljLG1CQUFPemUsWUFBUCxDQUFvQixPQUFwQixFQUE2QnFlLGFBQWFXLFdBQWIsQ0FBN0I7QUFDQVAsbUJBQU96YyxTQUFQLEdBQW1CeWMsT0FBT3pjLFNBQVAsQ0FBaUJrSCxPQUFqQixDQUF5QixvQkFBekIsRUFBK0MsRUFBL0MsQ0FBbkI7O0FBRUE7QUFDQThWLHdCQUFZLG1CQUFaLElBQW1DRixLQUFuQztBQUNBRSx3QkFBWSxnQkFBWixJQUFnQ0YsS0FBaEM7QUFDQUUsd0JBQVksZUFBWixJQUErQkYsS0FBL0I7QUFDQUUsd0JBQVksY0FBWixJQUE4QkYsS0FBOUI7QUFDQUUsd0JBQVlDLFNBQVosR0FBd0JILEtBQXhCO0FBQ0FFLHdCQUFZRSxPQUFaLEdBQXdCLEdBQXhCOztBQUVBRix3QkFBWSw2QkFBWixJQUE2Q1QsT0FBT3pOLFFBQVAsR0FBa0IsSUFBL0Q7QUFDQWtPLHdCQUFZLDBCQUFaLElBQTZDVCxPQUFPek4sUUFBUCxHQUFrQixJQUEvRDtBQUNBa08sd0JBQVksd0JBQVosSUFBNkNULE9BQU96TixRQUFQLEdBQWtCLElBQS9EO0FBQ0FrTyx3QkFBWSxxQkFBWixJQUE2Q1QsT0FBT3pOLFFBQVAsR0FBa0IsSUFBL0Q7O0FBRUFrTyx3QkFBWSxvQ0FBWixJQUFvRCwwQ0FBcEQ7QUFDQUEsd0JBQVksaUNBQVosSUFBb0QsMENBQXBEO0FBQ0FBLHdCQUFZLCtCQUFaLElBQW9ELDBDQUFwRDtBQUNBQSx3QkFBWSw0QkFBWixJQUFvRCwwQ0FBcEQ7O0FBRUFQLG1CQUFPemUsWUFBUCxDQUFvQixPQUFwQixFQUE2QnFlLGFBQWFXLFdBQWIsQ0FBN0I7QUFDSCxTQWxFUTs7QUFvRVRuTixjQUFNLFVBQVMzTSxDQUFULEVBQVk7QUFDZGlhLHlCQUFhQyxPQUFiLENBQXFCbGEsQ0FBckI7O0FBRUEsZ0JBQUlHLEtBQUssSUFBVDtBQUNBLGdCQUFJK0UsUUFBUS9FLEdBQUcwWixXQUFILEdBQWlCLEdBQTdCOztBQUVBO0FBQ0EsZ0JBQUlOLFNBQVMsSUFBYjtBQUNBLGdCQUFJWSxVQUFVaGEsR0FBR2lhLHNCQUFILENBQTBCLGNBQTFCLENBQWQ7QUFDQSxnQkFBSUQsUUFBUXRiLE1BQVIsR0FBaUIsQ0FBckIsRUFBd0I7QUFDcEIwYSx5QkFBU1ksUUFBUUEsUUFBUXRiLE1BQVIsR0FBaUIsQ0FBekIsQ0FBVDtBQUNILGFBRkQsTUFFTztBQUNILHVCQUFPLEtBQVA7QUFDSDs7QUFFRCxnQkFBSThhLFlBQWNKLE9BQU8vZSxZQUFQLENBQW9CLFFBQXBCLENBQWxCO0FBQ0EsZ0JBQUlrZixZQUFjSCxPQUFPL2UsWUFBUCxDQUFvQixRQUFwQixDQUFsQjtBQUNBLGdCQUFJb2YsUUFBY0wsT0FBTy9lLFlBQVAsQ0FBb0IsWUFBcEIsQ0FBbEI7O0FBRUE7QUFDQSxnQkFBSTZmLE9BQU9uWSxLQUFLQyxHQUFMLEtBQWFtWSxPQUFPZixPQUFPL2UsWUFBUCxDQUFvQixXQUFwQixDQUFQLENBQXhCO0FBQ0EsZ0JBQUl5RyxRQUFRLE1BQU1vWixJQUFsQjs7QUFFQSxnQkFBSXBaLFFBQVEsQ0FBWixFQUFlO0FBQ1hBLHdCQUFRLENBQVI7QUFDSDs7QUFFRDtBQUNBeEgsdUJBQVcsWUFBVztBQUNsQixvQkFBSW1ILFFBQVE7QUFDUiwyQkFBTzhZLFlBQVUsSUFEVDtBQUVSLDRCQUFRQyxZQUFVLElBRlY7QUFHUiwrQkFBVyxHQUhIOztBQUtSO0FBQ0EsbURBQStCTixPQUFPek4sUUFBUCxHQUFrQixJQU56QztBQU9SLGdEQUE0QnlOLE9BQU96TixRQUFQLEdBQWtCLElBUHRDO0FBUVIsOENBQTBCeU4sT0FBT3pOLFFBQVAsR0FBa0IsSUFScEM7QUFTUiwyQ0FBdUJ5TixPQUFPek4sUUFBUCxHQUFrQixJQVRqQztBQVVSLHlDQUFxQmdPLEtBVmI7QUFXUixzQ0FBa0JBLEtBWFY7QUFZUixxQ0FBaUJBLEtBWlQ7QUFhUixvQ0FBZ0JBLEtBYlI7QUFjUixpQ0FBYUE7QUFkTCxpQkFBWjs7QUFpQkFMLHVCQUFPemUsWUFBUCxDQUFvQixPQUFwQixFQUE2QnFlLGFBQWF2WSxLQUFiLENBQTdCOztBQUVBbkgsMkJBQVcsWUFBVztBQUNsQix3QkFBSTtBQUNBMEcsMkJBQUdvYSxXQUFILENBQWVoQixNQUFmO0FBQ0gscUJBRkQsQ0FFRSxPQUFNdlosQ0FBTixFQUFTO0FBQ1AsK0JBQU8sS0FBUDtBQUNIO0FBQ0osaUJBTkQsRUFNR3FaLE9BQU96TixRQU5WO0FBT0gsYUEzQkQsRUEyQkczSyxLQTNCSDtBQTRCSCxTQTVIUTs7QUE4SFQ7QUFDQXVaLG1CQUFXLFVBQVNDLFFBQVQsRUFBbUI7QUFDMUIsaUJBQUssSUFBSXJCLElBQUksQ0FBYixFQUFnQkEsSUFBSXFCLFNBQVM1YixNQUE3QixFQUFxQ3VhLEdBQXJDLEVBQTBDO0FBQ3RDLG9CQUFJalosS0FBS3NhLFNBQVNyQixDQUFULENBQVQ7O0FBRUEsb0JBQUlqWixHQUFHdWEsT0FBSCxDQUFXcGdCLFdBQVgsT0FBNkIsT0FBakMsRUFBMEM7QUFDdEMsd0JBQUlpSyxTQUFTcEUsR0FBR3NGLFVBQWhCOztBQUVBO0FBQ0Esd0JBQUlsQixPQUFPbVcsT0FBUCxDQUFlcGdCLFdBQWYsT0FBaUMsR0FBakMsSUFBd0NpSyxPQUFPekgsU0FBUCxDQUFpQnBDLE9BQWpCLENBQXlCLGNBQXpCLE1BQTZDLENBQUMsQ0FBMUYsRUFBNkY7QUFDekY7QUFDSDs7QUFFRDtBQUNBLHdCQUFJaWdCLFVBQVVwZixTQUFTSSxhQUFULENBQXVCLEdBQXZCLENBQWQ7QUFDQWdmLDRCQUFRN2QsU0FBUixHQUFvQnFELEdBQUdyRCxTQUFILEdBQWUsc0JBQW5DOztBQUVBLHdCQUFJOGQsZUFBZXphLEdBQUczRixZQUFILENBQWdCLE9BQWhCLENBQW5COztBQUVBLHdCQUFJLENBQUNvZ0IsWUFBTCxFQUFtQjtBQUNmQSx1Q0FBZSxFQUFmO0FBQ0g7O0FBRURELDRCQUFRN2YsWUFBUixDQUFxQixPQUFyQixFQUE4QjhmLFlBQTlCOztBQUVBemEsdUJBQUdyRCxTQUFILEdBQWUsb0JBQWY7QUFDQXFELHVCQUFHMGEsZUFBSCxDQUFtQixPQUFuQjs7QUFFQTtBQUNBdFcsMkJBQU91VyxZQUFQLENBQW9CSCxPQUFwQixFQUE2QnhhLEVBQTdCO0FBQ0F3YSw0QkFBUW5CLFdBQVIsQ0FBb0JyWixFQUFwQjtBQUNIO0FBQ0o7QUFDSjtBQS9KUSxLQUFiOztBQW1LQTs7O0FBR0EsUUFBSThaLGVBQWU7QUFDZjs7OztBQUlBNUssaUJBQVMsQ0FMTTtBQU1mMEwsb0JBQVksVUFBUy9hLENBQVQsRUFBWTtBQUNwQixnQkFBSWdiLFFBQVEsSUFBWjs7QUFFQSxnQkFBSWhiLEVBQUVoRyxJQUFGLEtBQVcsWUFBZixFQUE2QjtBQUN6QmlnQiw2QkFBYTVLLE9BQWIsSUFBd0IsQ0FBeEIsQ0FEeUIsQ0FDRTtBQUM5QixhQUZELE1BRU8sSUFBSXJQLEVBQUVoRyxJQUFGLEtBQVcsVUFBWCxJQUF5QmdHLEVBQUVoRyxJQUFGLEtBQVcsYUFBeEMsRUFBdUQ7QUFDMURQLDJCQUFXLFlBQVc7QUFDbEIsd0JBQUl3Z0IsYUFBYTVLLE9BQWIsR0FBdUIsQ0FBM0IsRUFBOEI7QUFDMUI0SyxxQ0FBYTVLLE9BQWIsSUFBd0IsQ0FBeEIsQ0FEMEIsQ0FDQztBQUM5QjtBQUNKLGlCQUpELEVBSUcsR0FKSDtBQUtILGFBTk0sTUFNQSxJQUFJclAsRUFBRWhHLElBQUYsS0FBVyxXQUFYLElBQTBCaWdCLGFBQWE1SyxPQUFiLEdBQXVCLENBQXJELEVBQXdEO0FBQzNEMkwsd0JBQVEsS0FBUjtBQUNIOztBQUVELG1CQUFPQSxLQUFQO0FBQ0gsU0F0QmM7QUF1QmZkLGlCQUFTLFVBQVNsYSxDQUFULEVBQVk7QUFDakJpYSx5QkFBYWMsVUFBYixDQUF3Qi9hLENBQXhCO0FBQ0g7QUF6QmMsS0FBbkI7O0FBNkJBOzs7O0FBSUEsYUFBU2liLHFCQUFULENBQStCamIsQ0FBL0IsRUFBa0M7QUFDOUIsWUFBSWlhLGFBQWFjLFVBQWIsQ0FBd0IvYSxDQUF4QixNQUErQixLQUFuQyxFQUEwQztBQUN0QyxtQkFBTyxJQUFQO0FBQ0g7O0FBRUQsWUFBSXNFLFVBQVUsSUFBZDtBQUNBLFlBQUluSyxTQUFTNkYsRUFBRTdGLE1BQUYsSUFBWTZGLEVBQUU5RSxVQUEzQjs7QUFFQSxlQUFPZixPQUFPK2dCLGFBQVAsS0FBeUIsSUFBaEMsRUFBc0M7QUFDbEMsZ0JBQUksRUFBRS9nQixrQkFBa0JnaEIsVUFBcEIsS0FBbUNoaEIsT0FBTzJDLFNBQVAsQ0FBaUJwQyxPQUFqQixDQUF5QixjQUF6QixNQUE2QyxDQUFDLENBQXJGLEVBQXdGO0FBQ3BGNEosMEJBQVVuSyxNQUFWO0FBQ0E7QUFDSCxhQUhELE1BR08sSUFBSUEsT0FBT2loQixTQUFQLENBQWlCekUsUUFBakIsQ0FBMEIsY0FBMUIsQ0FBSixFQUErQztBQUNsRHJTLDBCQUFVbkssTUFBVjtBQUNBO0FBQ0g7QUFDREEscUJBQVNBLE9BQU8rZ0IsYUFBaEI7QUFDSDs7QUFFRCxlQUFPNVcsT0FBUDtBQUNIOztBQUVEOzs7QUFHQSxhQUFTK1csVUFBVCxDQUFvQnJiLENBQXBCLEVBQXVCO0FBQ25CLFlBQUlzRSxVQUFVMlcsc0JBQXNCamIsQ0FBdEIsQ0FBZDs7QUFFQSxZQUFJc0UsWUFBWSxJQUFoQixFQUFzQjtBQUNsQitVLG1CQUFPOU0sSUFBUCxDQUFZdk0sQ0FBWixFQUFlc0UsT0FBZjs7QUFFQSxnQkFBSSxrQkFBa0JoTSxNQUF0QixFQUE4QjtBQUMxQmdNLHdCQUFRN0ksZ0JBQVIsQ0FBeUIsVUFBekIsRUFBcUM0ZCxPQUFPMU0sSUFBNUMsRUFBa0QsS0FBbEQ7QUFDQXJJLHdCQUFRN0ksZ0JBQVIsQ0FBeUIsYUFBekIsRUFBd0M0ZCxPQUFPMU0sSUFBL0MsRUFBcUQsS0FBckQ7QUFDSDs7QUFFRHJJLG9CQUFRN0ksZ0JBQVIsQ0FBeUIsU0FBekIsRUFBb0M0ZCxPQUFPMU0sSUFBM0MsRUFBaUQsS0FBakQ7QUFDQXJJLG9CQUFRN0ksZ0JBQVIsQ0FBeUIsWUFBekIsRUFBdUM0ZCxPQUFPMU0sSUFBOUMsRUFBb0QsS0FBcEQ7QUFDSDtBQUNKOztBQUVEMEwsVUFBTWlELGFBQU4sR0FBc0IsVUFBUzdOLE9BQVQsRUFBa0I7QUFDcENBLGtCQUFVQSxXQUFXLEVBQXJCOztBQUVBLFlBQUksY0FBY0EsT0FBbEIsRUFBMkI7QUFDdkI0TCxtQkFBT3pOLFFBQVAsR0FBa0I2QixRQUFRN0IsUUFBMUI7QUFDSDs7QUFFRDtBQUNBeU4sZUFBT21CLFNBQVAsQ0FBaUJsQyxHQUFHLGVBQUgsQ0FBakI7O0FBRUEsWUFBSSxrQkFBa0JoZ0IsTUFBdEIsRUFBOEI7QUFDMUJpRCxxQkFBUzlDLElBQVQsQ0FBY2dELGdCQUFkLENBQStCLFlBQS9CLEVBQTZDNGYsVUFBN0MsRUFBeUQsS0FBekQ7QUFDSDs7QUFFRDlmLGlCQUFTOUMsSUFBVCxDQUFjZ0QsZ0JBQWQsQ0FBK0IsV0FBL0IsRUFBNEM0ZixVQUE1QyxFQUF3RCxLQUF4RDtBQUNILEtBZkQ7O0FBaUJBOzs7Ozs7QUFNQWhELFVBQU1rRCxNQUFOLEdBQWUsVUFBU2pYLE9BQVQsRUFBa0I7QUFDN0I7QUFDQTtBQUNBLFlBQUlBLFFBQVFvVyxPQUFSLENBQWdCcGdCLFdBQWhCLE9BQWtDLE9BQXRDLEVBQStDO0FBQzNDK2UsbUJBQU9tQixTQUFQLENBQWlCLENBQUNsVyxPQUFELENBQWpCO0FBQ0FBLHNCQUFVQSxRQUFRNFcsYUFBbEI7QUFDSDs7QUFFRCxZQUFJLGtCQUFrQjVpQixNQUF0QixFQUE4QjtBQUMxQmdNLG9CQUFRN0ksZ0JBQVIsQ0FBeUIsWUFBekIsRUFBdUM0ZixVQUF2QyxFQUFtRCxLQUFuRDtBQUNIOztBQUVEL1csZ0JBQVE3SSxnQkFBUixDQUF5QixXQUF6QixFQUFzQzRmLFVBQXRDLEVBQWtELEtBQWxEO0FBQ0gsS0FiRDs7QUFlQS9pQixXQUFPK2YsS0FBUCxHQUFlQSxLQUFmOztBQUVBOWMsYUFBU0UsZ0JBQVQsQ0FBMEIsa0JBQTFCLEVBQThDLFlBQVc7QUFDckQ0YyxjQUFNaUQsYUFBTjtBQUNILEtBRkQsRUFFRyxLQUZIO0FBSUgsQ0F4VUEsRUF3VUVoakIsTUF4VUY7OztBQ1RELENBQUMsWUFBVTtBQUFDO0FBQWEsT0FBSSxJQUFJcUksSUFBRXBGLFNBQVNrWCxnQkFBVCxDQUEwQixTQUExQixDQUFOLEVBQTJDelMsSUFBRSxDQUFqRCxFQUFtREEsSUFBRVcsRUFBRTlCLE1BQXZELEVBQThELEVBQUVtQixDQUFoRTtBQUFrRSxLQUFDLFlBQVU7QUFBQyxVQUFJVCxJQUFFb0IsRUFBRVgsQ0FBRixDQUFOLENBQVdXLEVBQUVYLENBQUYsRUFBS3ZFLGdCQUFMLENBQXNCLE9BQXRCLEVBQThCLFVBQVNrRixDQUFULEVBQVc7QUFBQ0EsVUFBRThOLGNBQUYsSUFBbUJsUCxFQUFFNmIsU0FBRixDQUFZN0csTUFBWixDQUFtQixXQUFuQixDQUFuQixFQUFtRGhWLEVBQUU2YixTQUFGLENBQVlJLEdBQVosQ0FBZ0IsWUFBaEIsQ0FBbkQsRUFBaUZsakIsT0FBTzRkLFFBQVAsR0FBZ0IzVyxFQUFFa2MsYUFBRixDQUFnQixjQUFoQixFQUFnQ2poQixZQUFoQyxDQUE2QyxNQUE3QyxDQUFqRztBQUFzSixPQUFoTTtBQUFrTSxLQUF4TixFQUFEO0FBQWxFO0FBQThSLENBQXRULEVBQUQ7OztBQ0FBLENBQUMsVUFBU21HLENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsY0FBVSxPQUFPMGIsT0FBakIsSUFBMEIsZUFBYSxPQUFPQyxNQUE5QyxHQUFxREEsT0FBT0QsT0FBUCxHQUFlMWIsRUFBRTRiLFFBQVEsUUFBUixDQUFGLENBQXBFLEdBQXlGLGNBQVksT0FBT0MsTUFBbkIsSUFBMkJBLE9BQU9DLEdBQWxDLEdBQXNDRCxPQUFPLENBQUMsUUFBRCxDQUFQLEVBQWtCN2IsQ0FBbEIsQ0FBdEMsR0FBMkRXLEVBQUVvYixPQUFGLEdBQVUvYixFQUFFVyxFQUFFc0QsTUFBSixDQUE5SjtBQUEwSyxDQUF4TCxDQUF5TCxJQUF6TCxFQUE4TCxVQUFTK1gsQ0FBVCxFQUFXO0FBQUM7QUFBYSxXQUFTQyxDQUFULENBQVd0YixDQUFYLEVBQWE7QUFBQyxXQUFNLENBQUNzYixJQUFFLGNBQVksT0FBT0MsTUFBbkIsSUFBMkIsWUFBVSxPQUFPQSxPQUFPQyxRQUFuRCxHQUE0RCxVQUFTeGIsQ0FBVCxFQUFXO0FBQUMsYUFBTyxPQUFPQSxDQUFkO0FBQWdCLEtBQXhGLEdBQXlGLFVBQVNBLENBQVQsRUFBVztBQUFDLGFBQU9BLEtBQUcsY0FBWSxPQUFPdWIsTUFBdEIsSUFBOEJ2YixFQUFFdkQsV0FBRixLQUFnQjhlLE1BQTlDLElBQXNEdmIsTUFBSXViLE9BQU9uZ0IsU0FBakUsR0FBMkUsUUFBM0UsR0FBb0YsT0FBTzRFLENBQWxHO0FBQW9HLEtBQTVNLEVBQThNQSxDQUE5TSxDQUFOO0FBQXVOLFlBQVN5YixDQUFULEdBQVk7QUFBQyxXQUFNLENBQUNBLElBQUUzZCxPQUFPNGQsTUFBUCxJQUFlLFVBQVMxYixDQUFULEVBQVc7QUFBQyxXQUFJLElBQUlYLElBQUUsQ0FBVixFQUFZQSxJQUFFb0IsVUFBVXZDLE1BQXhCLEVBQStCbUIsR0FBL0IsRUFBbUM7QUFBQyxZQUFJVCxJQUFFNkIsVUFBVXBCLENBQVYsQ0FBTixDQUFtQixLQUFJLElBQUlzYyxDQUFSLElBQWEvYyxDQUFiO0FBQWVkLGlCQUFPMUMsU0FBUCxDQUFpQmlOLGNBQWpCLENBQWdDbkgsSUFBaEMsQ0FBcUN0QyxDQUFyQyxFQUF1QytjLENBQXZDLE1BQTRDM2IsRUFBRTJiLENBQUYsSUFBSy9jLEVBQUUrYyxDQUFGLENBQWpEO0FBQWY7QUFBc0UsY0FBTzNiLENBQVA7QUFBUyxLQUFwSyxFQUFzS1UsS0FBdEssQ0FBNEssSUFBNUssRUFBaUxELFNBQWpMLENBQU47QUFBa00sWUFBU21iLENBQVQsQ0FBVzViLENBQVgsRUFBYVgsQ0FBYixFQUFlO0FBQUMsV0FBTyxVQUFTVyxDQUFULEVBQVc7QUFBQyxVQUFHN0UsTUFBTW9QLE9BQU4sQ0FBY3ZLLENBQWQsQ0FBSCxFQUFvQixPQUFPQSxDQUFQO0FBQVMsS0FBekMsQ0FBMENBLENBQTFDLEtBQThDLFVBQVNBLENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsVUFBSVQsSUFBRSxFQUFOO0FBQUEsVUFBUytjLElBQUUsQ0FBQyxDQUFaO0FBQUEsVUFBY0wsSUFBRSxDQUFDLENBQWpCO0FBQUEsVUFBbUJPLElBQUUsS0FBSyxDQUExQixDQUE0QixJQUFHO0FBQUMsYUFBSSxJQUFJcEQsQ0FBSixFQUFNZ0QsSUFBRXpiLEVBQUV1YixPQUFPQyxRQUFULEdBQVosRUFBaUMsRUFBRUcsSUFBRSxDQUFDbEQsSUFBRWdELEVBQUVLLElBQUYsRUFBSCxFQUFhQyxJQUFqQixNQUF5Qm5kLEVBQUV4RSxJQUFGLENBQU9xZSxFQUFFcmYsS0FBVCxHQUFnQixDQUFDaUcsQ0FBRCxJQUFJVCxFQUFFVixNQUFGLEtBQVdtQixDQUF4RCxDQUFqQyxFQUE0RnNjLElBQUUsQ0FBQyxDQUEvRjtBQUFtRyxPQUF2RyxDQUF1RyxPQUFNM2IsQ0FBTixFQUFRO0FBQUNzYixZQUFFLENBQUMsQ0FBSCxFQUFLTyxJQUFFN2IsQ0FBUDtBQUFTLE9BQXpILFNBQWdJO0FBQUMsWUFBRztBQUFDMmIsZUFBRyxRQUFNRixFQUFFTyxNQUFYLElBQW1CUCxFQUFFTyxNQUFGLEVBQW5CO0FBQThCLFNBQWxDLFNBQXlDO0FBQUMsY0FBR1YsQ0FBSCxFQUFLLE1BQU1PLENBQU47QUFBUTtBQUFDLGNBQU9qZCxDQUFQO0FBQVMsS0FBNU8sQ0FBNk9vQixDQUE3TyxFQUErT1gsQ0FBL08sQ0FBOUMsSUFBaVMsWUFBVTtBQUFDLFlBQU0sSUFBSWdDLFNBQUosQ0FBYyxzREFBZCxDQUFOO0FBQTRFLEtBQXZGLEVBQXhTO0FBQWtZLFlBQVM0YSxDQUFULENBQVdqYyxDQUFYLEVBQWE7QUFBQyxXQUFPLFVBQVNBLENBQVQsRUFBVztBQUFDLFVBQUc3RSxNQUFNb1AsT0FBTixDQUFjdkssQ0FBZCxDQUFILEVBQW9CO0FBQUMsYUFBSSxJQUFJWCxJQUFFLENBQU4sRUFBUVQsSUFBRSxJQUFJekQsS0FBSixDQUFVNkUsRUFBRTlCLE1BQVosQ0FBZCxFQUFrQ21CLElBQUVXLEVBQUU5QixNQUF0QyxFQUE2Q21CLEdBQTdDO0FBQWlEVCxZQUFFUyxDQUFGLElBQUtXLEVBQUVYLENBQUYsQ0FBTDtBQUFqRCxTQUEyRCxPQUFPVCxDQUFQO0FBQVM7QUFBQyxLQUF0RyxDQUF1R29CLENBQXZHLEtBQTJHLFVBQVNBLENBQVQsRUFBVztBQUFDLFVBQUd1YixPQUFPQyxRQUFQLElBQW1CMWQsT0FBT2tDLENBQVAsQ0FBbkIsSUFBOEIseUJBQXVCbEMsT0FBTzFDLFNBQVAsQ0FBaUJvRCxRQUFqQixDQUEwQjBDLElBQTFCLENBQStCbEIsQ0FBL0IsQ0FBeEQsRUFBMEYsT0FBTzdFLE1BQU0rZ0IsSUFBTixDQUFXbGMsQ0FBWCxDQUFQO0FBQXFCLEtBQTNILENBQTRIQSxDQUE1SCxDQUEzRyxJQUEyTyxZQUFVO0FBQUMsWUFBTSxJQUFJcUIsU0FBSixDQUFjLGlEQUFkLENBQU47QUFBdUUsS0FBbEYsRUFBbFA7QUFBdVUsT0FBSXpDLENBQUo7QUFBQSxNQUFNb0IsSUFBRSxDQUFSO0FBQUEsTUFBVVgsSUFBRSxFQUFaO0FBQUEsTUFBZThjLElBQUUsRUFBQ25nQixNQUFLLFVBQVNnRSxDQUFULEVBQVdYLENBQVgsRUFBYVQsQ0FBYixFQUFlO0FBQUMsVUFBSStjLENBQUo7QUFBQSxVQUFNTCxDQUFOO0FBQUEsVUFBUU8sQ0FBUjtBQUFBLFVBQVVwRCxJQUFFLElBQUlyRixNQUFKLENBQVcsTUFBSS9ULENBQWYsRUFBaUIsR0FBakIsQ0FBWixDQUFrQyxJQUFHLEtBQUssQ0FBTCxLQUFTVCxDQUFaLEVBQWNBLElBQUUsRUFBRixDQUFkLEtBQXdCLEtBQUkrYyxDQUFKLElBQVMvYyxDQUFUO0FBQVdBLFVBQUV5SixjQUFGLENBQWlCc1QsQ0FBakIsS0FBcUIsT0FBTy9jLEVBQUUrYyxDQUFGLENBQTVCO0FBQVgsT0FBNEMsSUFBRyxDQUFDM2IsQ0FBSixFQUFNLE9BQU9wQixDQUFQLENBQVMsS0FBSStjLElBQUUsQ0FBQ0UsSUFBRTdiLEVBQUVvUyxVQUFMLEVBQWlCbFUsTUFBdkIsRUFBOEJ5ZCxHQUE5QjtBQUFtQyxTQUFDTCxJQUFFTyxFQUFFRixDQUFGLENBQUgsS0FBVUwsRUFBRWMsU0FBWixJQUF1QjNELEVBQUUzVyxJQUFGLENBQU93WixFQUFFcGYsSUFBVCxDQUF2QixLQUF3QzBDLEVBQUUsS0FBS3lkLFFBQUwsQ0FBY2YsRUFBRXBmLElBQUYsQ0FBT3VDLEtBQVAsQ0FBYVksRUFBRW5CLE1BQWYsQ0FBZCxDQUFGLElBQXlDLEtBQUtvZSxnQkFBTCxDQUFzQmhCLEVBQUVsaUIsS0FBeEIsQ0FBakY7QUFBbkMsT0FBb0osT0FBT3dGLENBQVA7QUFBUyxLQUF4UyxFQUF5UzJkLFdBQVUsVUFBU3ZjLENBQVQsRUFBV1gsQ0FBWCxFQUFhVCxDQUFiLEVBQWU7QUFBQyxhQUFPb0IsRUFBRWxHLFlBQUYsQ0FBZXVGLElBQUVULENBQWpCLENBQVA7QUFBMkIsS0FBOVYsRUFBK1Y0ZCxTQUFRLFVBQVN4YyxDQUFULEVBQVdYLENBQVgsRUFBYVQsQ0FBYixFQUFlK2MsQ0FBZixFQUFpQjtBQUFDM2IsUUFBRTdGLFlBQUYsQ0FBZSxLQUFLc2lCLFNBQUwsQ0FBZXBkLElBQUVULENBQWpCLENBQWYsRUFBbUNxSCxPQUFPMFYsQ0FBUCxDQUFuQztBQUE4QyxLQUF2YSxFQUF3YWUsU0FBUSxVQUFTMWMsQ0FBVCxFQUFXO0FBQUMsYUFBT0EsRUFBRW5HLFlBQUYsQ0FBZSxNQUFmLEtBQXdCLE1BQS9CO0FBQXNDLEtBQWxlLEVBQW1lOGlCLFlBQVcsWUFBVTtBQUFDLGFBQU0sS0FBRzNjLEdBQVQ7QUFBYSxLQUF0Z0IsRUFBdWdCc2Msa0JBQWlCLFVBQVNqZCxDQUFULEVBQVc7QUFBQyxVQUFJVyxDQUFKLENBQU0sSUFBRztBQUFDLGVBQU9YLElBQUUsVUFBUUEsQ0FBUixJQUFXLFdBQVNBLENBQVQsS0FBYSxVQUFRQSxDQUFSLEdBQVUsSUFBVixHQUFlOEQsTUFBTW5ELElBQUUyWixPQUFPdGEsQ0FBUCxDQUFSLElBQW1CLFVBQVV5QyxJQUFWLENBQWV6QyxDQUFmLElBQWtCdWQsS0FBS0MsS0FBTCxDQUFXeGQsQ0FBWCxDQUFsQixHQUFnQ0EsQ0FBbkQsR0FBcURXLENBQWpGLENBQWIsR0FBaUdYLENBQXhHO0FBQTBHLE9BQTlHLENBQThHLE9BQU1XLENBQU4sRUFBUTtBQUFDLGVBQU9YLENBQVA7QUFBUztBQUFDLEtBQTNxQixFQUE0cUJnZCxVQUFTLFVBQVNyYyxDQUFULEVBQVc7QUFBQyxhQUFPQSxFQUFFcUQsT0FBRixDQUFVLFNBQVYsRUFBb0IsVUFBU3JELENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsZUFBT0EsSUFBRUEsRUFBRThHLFdBQUYsRUFBRixHQUFrQixFQUF6QjtBQUE0QixPQUE5RCxDQUFQO0FBQXVFLEtBQXh3QixFQUF5d0JzVyxXQUFVLFVBQVN6YyxDQUFULEVBQVc7QUFBQyxhQUFPQSxFQUFFcUQsT0FBRixDQUFVLEtBQVYsRUFBZ0IsR0FBaEIsRUFBcUJBLE9BQXJCLENBQTZCLHVCQUE3QixFQUFxRCxPQUFyRCxFQUE4REEsT0FBOUQsQ0FBc0UsbUJBQXRFLEVBQTBGLE9BQTFGLEVBQW1HQSxPQUFuRyxDQUEyRyxJQUEzRyxFQUFnSCxHQUFoSCxFQUFxSDFKLFdBQXJILEVBQVA7QUFBMEksS0FBejZCLEVBQTA2QnVGLE1BQUssWUFBVTtBQUFDLFVBQUljLENBQUosQ0FBTXJJLE9BQU9xRyxPQUFQLElBQWdCLGNBQVksT0FBT3JHLE9BQU9xRyxPQUFQLENBQWVrQixJQUFsRCxJQUF3RCxDQUFDYyxJQUFFckksT0FBT3FHLE9BQVYsRUFBbUJrQixJQUFuQixDQUF3QndCLEtBQXhCLENBQThCVixDQUE5QixFQUFnQ1MsU0FBaEMsQ0FBeEQ7QUFBbUcsS0FBbmlDLEVBQW9pQ3FjLFVBQVMsVUFBUzljLENBQVQsRUFBVztBQUFDWCxRQUFFVyxDQUFGLE1BQU9YLEVBQUVXLENBQUYsSUFBSyxDQUFDLENBQU4sRUFBUSxLQUFLZCxJQUFMLENBQVV3QixLQUFWLENBQWdCLElBQWhCLEVBQXFCRCxTQUFyQixDQUFmO0FBQWdELEtBQXptQyxFQUEwbUNzYyxnQkFBZSxZQUFVO0FBQUMxZCxVQUFFLEVBQUY7QUFBSyxLQUF6b0MsRUFBMG9DMmQsWUFBVyxVQUFTaGQsQ0FBVCxFQUFXO0FBQUMsYUFBT0EsRUFBRXFELE9BQUYsQ0FBVSxZQUFWLEVBQXVCLEVBQXZCLENBQVA7QUFBa0MsS0FBbnNDLEVBQW9zQ3daLE9BQU0sRUFBQ0ksTUFBSyxVQUFTamQsQ0FBVCxFQUFXO0FBQUMsWUFBSVgsSUFBRVcsRUFBRXVULEtBQUYsQ0FBUSwwQkFBUixDQUFOLENBQTBDLElBQUcsQ0FBQ2xVLENBQUosRUFBTSxPQUFPLElBQVAsQ0FBWSxJQUFJVCxJQUFFZ2QsRUFBRXZjLEVBQUVFLEdBQUYsQ0FBTSxVQUFTUyxDQUFULEVBQVc7QUFBQyxpQkFBTzJWLFNBQVMzVixDQUFULEVBQVcsRUFBWCxDQUFQO0FBQXNCLFNBQXhDLENBQUYsRUFBNEMsQ0FBNUMsQ0FBTjtBQUFBLFlBQXFEMmIsS0FBRy9jLEVBQUUsQ0FBRixHQUFLQSxFQUFFLENBQUYsQ0FBUixDQUFyRDtBQUFBLFlBQW1FMGMsSUFBRTFjLEVBQUUsQ0FBRixDQUFyRTtBQUFBLFlBQTBFaWQsSUFBRWpkLEVBQUUsQ0FBRixDQUE1RTtBQUFBLFlBQWlGNlosSUFBRSxJQUFJbFgsSUFBSixDQUFTb2EsQ0FBVCxFQUFXTCxJQUFFLENBQWIsRUFBZU8sQ0FBZixDQUFuRixDQUFxRyxPQUFPcEQsRUFBRXlFLFdBQUYsT0FBa0J2QixDQUFsQixJQUFxQmxELEVBQUUwRSxRQUFGLEtBQWEsQ0FBYixLQUFpQjdCLENBQXRDLElBQXlDN0MsRUFBRTJFLE9BQUYsT0FBY3ZCLENBQXZELEdBQXlELElBQXpELEdBQThEcEQsQ0FBckU7QUFBdUUsT0FBMVAsRUFBMlB2ZSxRQUFPLFVBQVM4RixDQUFULEVBQVc7QUFBQyxlQUFPQSxDQUFQO0FBQVMsT0FBdlIsRUFBd1JxZCxTQUFRLFVBQVNyZCxDQUFULEVBQVc7QUFBQyxlQUFPbUQsTUFBTW5ELENBQU4sSUFBUyxJQUFULEdBQWMyVixTQUFTM1YsQ0FBVCxFQUFXLEVBQVgsQ0FBckI7QUFBb0MsT0FBaFYsRUFBaVZzZCxRQUFPLFVBQVN0ZCxDQUFULEVBQVc7QUFBQyxZQUFHbUQsTUFBTW5ELENBQU4sQ0FBSCxFQUFZLE1BQU0sSUFBTixDQUFXLE9BQU9vRCxXQUFXcEQsQ0FBWCxDQUFQO0FBQXFCLE9BQWhaLEVBQWladWQsU0FBUSxVQUFTdmQsQ0FBVCxFQUFXO0FBQUMsZUFBTSxDQUFDLGlCQUFpQjhCLElBQWpCLENBQXNCOUIsQ0FBdEIsQ0FBUDtBQUFnQyxPQUFyYyxFQUFzY3dkLFFBQU8sVUFBU3hkLENBQVQsRUFBVztBQUFDLGVBQU9tYyxFQUFFRyxnQkFBRixDQUFtQnRjLENBQW5CLENBQVA7QUFBNkIsT0FBdGYsRUFBdWZ5ZCxRQUFPLFVBQVN6ZCxDQUFULEVBQVc7QUFBQyxZQUFJWCxJQUFFLEVBQU4sQ0FBUyxPQUFPVyxJQUFFLHNCQUFzQjhCLElBQXRCLENBQTJCOUIsQ0FBM0IsS0FBK0JYLElBQUVXLEVBQUVxRCxPQUFGLENBQVUsZ0JBQVYsRUFBMkIsSUFBM0IsQ0FBRixFQUFtQ3JELEVBQUVxRCxPQUFGLENBQVUsSUFBSStQLE1BQUosQ0FBVyxhQUFXL1QsQ0FBWCxHQUFhLEdBQXhCLENBQVYsRUFBdUMsSUFBdkMsQ0FBbEUsSUFBZ0gsTUFBSVcsQ0FBSixHQUFNLEdBQXhILEVBQTRILElBQUlvVCxNQUFKLENBQVdwVCxDQUFYLEVBQWFYLENBQWIsQ0FBbkk7QUFBbUosT0FBdHFCLEVBQTFzQyxFQUFrM0RxZSxrQkFBaUIsVUFBUzFkLENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsVUFBSVQsSUFBRSxLQUFLaWUsS0FBTCxDQUFXN2MsS0FBRyxRQUFkLENBQU4sQ0FBOEIsSUFBRyxDQUFDcEIsQ0FBSixFQUFNLE1BQUsseUNBQXVDb0IsQ0FBdkMsR0FBeUMsR0FBOUMsQ0FBa0QsSUFBSTJiLElBQUUvYyxFQUFFUyxDQUFGLENBQU4sQ0FBVyxJQUFHLFNBQU9zYyxDQUFWLEVBQVksTUFBSyx3QkFBd0I3WSxNQUF4QixDQUErQjlDLENBQS9CLEVBQWlDLEtBQWpDLEVBQXdDOEMsTUFBeEMsQ0FBK0N6RCxDQUEvQyxFQUFpRCxHQUFqRCxDQUFMLENBQTJELE9BQU9zYyxDQUFQO0FBQVMsS0FBbGtFLEVBQW1rRWdDLGlCQUFnQixVQUFTM2QsQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQyxhQUFNLENBQUNXLElBQUUsS0FBS2dkLFVBQUwsQ0FBZ0JoZCxLQUFHLEVBQW5CLEVBQXVCWixLQUF2QixDQUE2QixLQUE3QixDQUFILEVBQXdDLENBQXhDLElBQTJDaWMsRUFBRTliLEdBQUYsQ0FBTVMsQ0FBTixFQUFRLFVBQVNBLENBQVQsRUFBVztBQUFDLGVBQU0sR0FBRzhDLE1BQUgsQ0FBVTlDLENBQVYsRUFBWSxHQUFaLEVBQWlCOEMsTUFBakIsQ0FBd0J6RCxDQUF4QixDQUFOO0FBQWlDLE9BQXJELEVBQXVEa1MsSUFBdkQsQ0FBNEQsR0FBNUQsQ0FBM0MsR0FBNEcsRUFBbEg7QUFBcUgsS0FBdHRFLEVBQXV0RXFNLFlBQVcsVUFBUzVkLENBQVQsRUFBV3BCLENBQVgsRUFBYTtBQUFDLFVBQUkrYyxJQUFFLEVBQU4sQ0FBUyxPQUFPTixFQUFFL2QsSUFBRixDQUFPMEMsQ0FBUCxFQUFTLFVBQVNBLENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsU0FBQyxDQUFELElBQUlULEVBQUU3RSxPQUFGLENBQVVzRixDQUFWLENBQUosSUFBa0JzYyxFQUFFdmhCLElBQUYsQ0FBT2lGLENBQVAsQ0FBbEI7QUFBNEIsT0FBbkQsR0FBcURzYyxDQUE1RDtBQUE4RCxLQUF2ekUsRUFBd3pFa0MsS0FBSSxVQUFTN2QsQ0FBVCxFQUFXO0FBQUMsYUFBT3FiLEVBQUV5QyxJQUFGLENBQU9wZCxLQUFQLENBQWEyYSxDQUFiLEVBQWVZLEVBQUVqYyxDQUFGLEVBQUs4QyxNQUFMLENBQVksQ0FBQyxFQUFELEVBQUksRUFBSixDQUFaLENBQWYsQ0FBUDtBQUE0QyxLQUFwM0UsRUFBcTNFaWIsY0FBYWpnQixPQUFPa2dCLE1BQVAsS0FBZ0JwZixJQUFFLFlBQVUsQ0FBRSxDQUFkLEVBQWUsVUFBU29CLENBQVQsRUFBVztBQUFDLFVBQUcsSUFBRVMsVUFBVXZDLE1BQWYsRUFBc0IsTUFBTXdHLE1BQU0sK0JBQU4sQ0FBTixDQUE2QyxJQUFHLFlBQVU0VyxFQUFFdGIsQ0FBRixDQUFiLEVBQWtCLE1BQU1xQixVQUFVLDRCQUFWLENBQU4sQ0FBOEN6QyxFQUFFeEQsU0FBRixHQUFZNEUsQ0FBWixDQUFjLElBQUlYLElBQUUsSUFBSVQsQ0FBSixFQUFOLENBQVksT0FBT0EsRUFBRXhELFNBQUYsR0FBWSxJQUFaLEVBQWlCaUUsQ0FBeEI7QUFBMEIsS0FBbE8sQ0FBbDRFLEVBQXNtRjRlLGlCQUFnQixxQ0FBdG5GLEVBQWpCO0FBQUEsTUFBOHFGdEMsSUFBRSxFQUFDeGQsV0FBVSxlQUFYLEVBQTJCK2YsUUFBTyx5QkFBbEMsRUFBNERDLFVBQVMsK0VBQXJFLEVBQXFKQyxpQkFBZ0IsQ0FBQyxDQUF0SyxFQUF3S0MsVUFBUyxJQUFqTCxFQUFzTEMsT0FBTSxJQUE1TCxFQUFpTUMsV0FBVSxDQUFDLENBQTVNLEVBQThNQyxxQkFBb0IsQ0FBbE8sRUFBb09sSyxPQUFNLE9BQTFPLEVBQWtQeFgsU0FBUSxDQUFDLENBQTNQLEVBQTZQMmhCLHFCQUFvQixPQUFqUixFQUF5UkMsWUFBVyxlQUFwUyxFQUFvVEMsY0FBYSxpQkFBalUsRUFBbVZDLGNBQWEsVUFBUzVlLENBQVQsRUFBVyxDQUFFLENBQTdXLEVBQThXNmUsaUJBQWdCLFVBQVM3ZSxDQUFULEVBQVcsQ0FBRSxDQUEzWSxFQUE0WThlLGVBQWMsdUNBQTFaLEVBQWtjQyxlQUFjLFdBQWhkLEVBQWhyRjtBQUFBLE1BQTZvR2xELElBQUUsWUFBVTtBQUFDLFNBQUttRCxNQUFMLEdBQVk3QyxFQUFFUSxVQUFGLEVBQVo7QUFBMkIsR0FBcnJHLENBQXNyR2QsRUFBRXpnQixTQUFGLEdBQVksRUFBQzZqQixjQUFhLENBQUMsQ0FBZixFQUFpQkMsa0NBQWlDLFlBQVU7QUFBQyxVQUFJN2YsSUFBRSxJQUFOO0FBQUEsVUFBV1csSUFBRSxZQUFVO0FBQUMsWUFBSUEsSUFBRXFiLEVBQUU4RCxRQUFGLEVBQU4sQ0FBbUIsT0FBTSxDQUFDLENBQUQsS0FBSzlmLEVBQUUrZixnQkFBUCxJQUF5QnBmLEVBQUVxZixNQUFGLEVBQXpCLEVBQW9DcmYsRUFBRXNmLE9BQUYsR0FBWUMsT0FBWixFQUExQztBQUFnRSxPQUEzRyxDQUE0RyxPQUFNLENBQUN2ZixDQUFELEVBQUdBLENBQUgsQ0FBTjtBQUFZLEtBQXJMLEVBQXNMd2Ysa0JBQWlCLFlBQVU7QUFBQyxhQUFPckQsRUFBRW5nQixJQUFGLENBQU8sS0FBSzJILE9BQVosRUFBb0IsS0FBS21KLE9BQUwsQ0FBYTNPLFNBQWpDLEVBQTJDLEtBQUtzaEIsVUFBaEQsR0FBNEQsS0FBSzdiLE1BQUwsSUFBYSxLQUFLQSxNQUFMLENBQVk0YixnQkFBekIsSUFBMkMsS0FBSzViLE1BQUwsQ0FBWTRiLGdCQUFaLEVBQXZHLEVBQXNJLElBQTdJO0FBQWtKLEtBQXBXLEVBQXFXRSxlQUFjLFVBQVMxZixDQUFULEVBQVc7QUFBQyxXQUFJLElBQUlYLENBQVIsSUFBYSxLQUFLb2dCLFVBQUwsR0FBZ0J0RCxFQUFFNEIsWUFBRixDQUFlLEtBQUtuYSxNQUFMLENBQVlrSixPQUEzQixDQUFoQixFQUFvRCxLQUFLQSxPQUFMLEdBQWFxUCxFQUFFNEIsWUFBRixDQUFlLEtBQUswQixVQUFwQixDQUFqRSxFQUFpR3pmLENBQTlHO0FBQWdIQSxVQUFFcUksY0FBRixDQUFpQmhKLENBQWpCLE1BQXNCLEtBQUt5TixPQUFMLENBQWF6TixDQUFiLElBQWdCVyxFQUFFWCxDQUFGLENBQXRDO0FBQWhILE9BQTRKLEtBQUttZ0IsZ0JBQUw7QUFBd0IsS0FBbmpCLEVBQW9qQkcsWUFBVyxJQUEvakIsRUFBb2tCNVcsSUFBRyxVQUFTL0ksQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQyxhQUFPLEtBQUtzZ0IsVUFBTCxHQUFnQixLQUFLQSxVQUFMLElBQWlCLEVBQWpDLEVBQW9DLENBQUMsS0FBS0EsVUFBTCxDQUFnQjNmLENBQWhCLElBQW1CLEtBQUsyZixVQUFMLENBQWdCM2YsQ0FBaEIsS0FBb0IsRUFBeEMsRUFBNEM1RixJQUE1QyxDQUFpRGlGLENBQWpELENBQXBDLEVBQXdGLElBQS9GO0FBQW9HLEtBQXpyQixFQUEwckJ1Z0IsV0FBVSxVQUFTNWYsQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQ2djLFFBQUV3RSxRQUFGLENBQVcsSUFBWCxFQUFnQjdmLEVBQUVyRyxXQUFGLEVBQWhCLEVBQWdDMEYsQ0FBaEM7QUFBbUMsS0FBcnZCLEVBQXN2Qm1TLEtBQUksVUFBU3hSLENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsVUFBSVQsSUFBRSxLQUFLK2dCLFVBQUwsSUFBaUIsS0FBS0EsVUFBTCxDQUFnQjNmLENBQWhCLENBQXZCLENBQTBDLElBQUdwQixDQUFILEVBQUssSUFBR1MsQ0FBSCxFQUFLLEtBQUksSUFBSXNjLElBQUUvYyxFQUFFVixNQUFaLEVBQW1CeWQsR0FBbkI7QUFBd0IvYyxVQUFFK2MsQ0FBRixNQUFPdGMsQ0FBUCxJQUFVVCxFQUFFbEUsTUFBRixDQUFTaWhCLENBQVQsRUFBVyxDQUFYLENBQVY7QUFBeEIsT0FBTCxNQUEwRCxPQUFPLEtBQUtnRSxVQUFMLENBQWdCM2YsQ0FBaEIsQ0FBUCxDQUEwQixPQUFPLElBQVA7QUFBWSxLQUF2NUIsRUFBdzVCOGYsYUFBWSxVQUFTOWYsQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQ2djLFFBQUUwRSxhQUFGLENBQWdCLElBQWhCLEVBQXFCL2YsRUFBRXJHLFdBQUYsRUFBckI7QUFBc0MsS0FBeDlCLEVBQXk5Qm1ELFNBQVEsVUFBU2tELENBQVQsRUFBV1gsQ0FBWCxFQUFhVCxDQUFiLEVBQWU7QUFBQ1MsVUFBRUEsS0FBRyxJQUFMLENBQVUsSUFBSXNjLENBQUo7QUFBQSxVQUFNTCxJQUFFLEtBQUtxRSxVQUFMLElBQWlCLEtBQUtBLFVBQUwsQ0FBZ0IzZixDQUFoQixDQUF6QixDQUE0QyxJQUFHc2IsQ0FBSCxFQUFLLEtBQUksSUFBSU8sSUFBRVAsRUFBRXBkLE1BQVosRUFBbUIyZCxHQUFuQjtBQUF3QixZQUFHLENBQUMsQ0FBRCxNQUFNRixJQUFFTCxFQUFFTyxDQUFGLEVBQUszYSxJQUFMLENBQVU3QixDQUFWLEVBQVlBLENBQVosRUFBY1QsQ0FBZCxDQUFSLENBQUgsRUFBNkIsT0FBTytjLENBQVA7QUFBckQsT0FBOEQsT0FBTSxDQUFDLEtBQUsvWCxNQUFOLElBQWMsS0FBS0EsTUFBTCxDQUFZOUcsT0FBWixDQUFvQmtELENBQXBCLEVBQXNCWCxDQUF0QixFQUF3QlQsQ0FBeEIsQ0FBcEI7QUFBK0MsS0FBenBDLEVBQTBwQ29oQixjQUFhLFVBQVNoZ0IsQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQyxhQUFPOGMsRUFBRVcsUUFBRixDQUFXLDBEQUFYLEdBQXVFLEtBQUttRCxTQUFMLENBQWUsRUFBQzNCLE9BQU10ZSxDQUFQLEVBQVNrZ0IsT0FBTTdnQixDQUFmLEVBQWYsQ0FBOUU7QUFBZ0gsS0FBcnlDLEVBQXN5QzhnQixjQUFhLFlBQVU7QUFBQyxhQUFPLEtBQUtyVCxPQUFMLENBQWF1UixRQUFiLEdBQXNCaEQsRUFBRSxLQUFLelgsTUFBTCxDQUFZRCxPQUFaLENBQW9CbU8sZ0JBQXBCLENBQXFDLElBQUloUCxNQUFKLENBQVcsS0FBS2dLLE9BQUwsQ0FBYTNPLFNBQXhCLEVBQWtDLFlBQWxDLEVBQWdEMkUsTUFBaEQsQ0FBdUQsS0FBS2dLLE9BQUwsQ0FBYXVSLFFBQXBFLEVBQTZFLElBQTdFLENBQXJDLENBQUYsQ0FBdEIsR0FBa0osS0FBS3poQixRQUE5SjtBQUF1SyxLQUFyK0MsRUFBWixDQUFtL0MsSUFBSXdqQixJQUFFLFVBQVNwZ0IsQ0FBVCxFQUFXO0FBQUNxYixNQUFFdlUsTUFBRixDQUFTLENBQUMsQ0FBVixFQUFZLElBQVosRUFBaUI5RyxDQUFqQjtBQUFvQixHQUF0QyxDQUF1Q29nQixFQUFFaGxCLFNBQUYsR0FBWSxFQUFDaWxCLFVBQVMsVUFBU3JnQixDQUFULEVBQVdYLENBQVgsRUFBYTtBQUFDLFVBQUcsS0FBS2lDLEVBQVIsRUFBVyxPQUFPLElBQUViLFVBQVV2QyxNQUFaLEtBQXFCbUIsSUFBRSxHQUFHWixLQUFILENBQVN5QyxJQUFULENBQWNULFNBQWQsRUFBd0IsQ0FBeEIsRUFBMEIsQ0FBQyxDQUEzQixDQUF2QixHQUFzRCxLQUFLYSxFQUFMLENBQVF0QixDQUFSLEVBQVVYLENBQVYsQ0FBN0QsQ0FBMEUsSUFBR2xFLE1BQU1vUCxPQUFOLENBQWN2SyxDQUFkLENBQUgsRUFBb0I7QUFBQyxZQUFHLENBQUMsS0FBS3NnQixnQkFBVCxFQUEwQixNQUFLLGdCQUFjLEtBQUtwa0IsSUFBbkIsR0FBd0IsbUNBQTdCLENBQWlFLE9BQU8sS0FBS29rQixnQkFBTCxDQUFzQjVmLEtBQXRCLENBQTRCLElBQTVCLEVBQWlDRCxTQUFqQyxDQUFQO0FBQW1ELFdBQUk3QixJQUFFNkIsVUFBVUEsVUFBVXZDLE1BQVYsR0FBaUIsQ0FBM0IsQ0FBTixDQUFvQyxJQUFHLEtBQUtxaUIsWUFBTCxJQUFtQjNoQixFQUFFNGhCLFlBQUYsRUFBdEIsRUFBdUMsT0FBTyxVQUFReGdCLElBQUVtYyxFQUFFVSxLQUFGLENBQVFJLElBQVIsQ0FBYWpkLENBQWIsQ0FBVixLQUE0QixLQUFLdWdCLFlBQUwsQ0FBa0I3ZixLQUFsQixDQUF3QixJQUF4QixFQUE2QkQsU0FBN0IsQ0FBbkMsQ0FBMkUsSUFBRyxLQUFLZ2dCLGNBQVIsRUFBdUIsT0FBTSxDQUFDdGQsTUFBTW5ELENBQU4sQ0FBRCxLQUFZQSxJQUFFb0QsV0FBV3BELENBQVgsQ0FBRixFQUFnQixLQUFLeWdCLGNBQUwsQ0FBb0IvZixLQUFwQixDQUEwQixJQUExQixFQUErQkQsU0FBL0IsQ0FBNUIsQ0FBTixDQUE2RSxJQUFHLEtBQUtpZ0IsY0FBUixFQUF1QixPQUFPLEtBQUtBLGNBQUwsQ0FBb0JoZ0IsS0FBcEIsQ0FBMEIsSUFBMUIsRUFBK0JELFNBQS9CLENBQVAsQ0FBaUQsTUFBSyxnQkFBYyxLQUFLdkUsSUFBbkIsR0FBd0IsZ0NBQTdCO0FBQThELEtBQWhwQixFQUFpcEJ5a0IsbUJBQWtCLFVBQVMzZ0IsQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQyxVQUFHLFlBQVUsT0FBT1csQ0FBcEIsRUFBc0IsT0FBTzdFLE1BQU1vUCxPQUFOLENBQWN2SyxDQUFkLElBQWlCQSxDQUFqQixHQUFtQixDQUFDQSxDQUFELENBQTFCLENBQThCLElBQUlwQixJQUFFLEtBQUtnaUIsZUFBWCxDQUEyQixJQUFHemxCLE1BQU1vUCxPQUFOLENBQWMzTCxDQUFkLENBQUgsRUFBb0I7QUFBQyxhQUFJLElBQUkrYyxJQUFFLFVBQVMzYixDQUFULEVBQVdYLENBQVgsRUFBYTtBQUFDLGNBQUlULElBQUVvQixFQUFFdVQsS0FBRixDQUFRLGtCQUFSLENBQU4sQ0FBa0MsSUFBRyxDQUFDM1UsQ0FBSixFQUFNLE1BQUssbUNBQWlDb0IsQ0FBakMsR0FBbUMsR0FBeEMsQ0FBNEMsSUFBSTJiLElBQUUvYyxFQUFFLENBQUYsRUFBS1EsS0FBTCxDQUFXLEdBQVgsRUFBZ0JHLEdBQWhCLENBQW9CNGMsRUFBRWEsVUFBdEIsQ0FBTixDQUF3QyxJQUFHckIsRUFBRXpkLE1BQUYsS0FBV21CLENBQWQsRUFBZ0IsTUFBSyxxQkFBbUJzYyxFQUFFemQsTUFBckIsR0FBNEIsZUFBNUIsR0FBNENtQixDQUE1QyxHQUE4QyxhQUFuRCxDQUFpRSxPQUFPc2MsQ0FBUDtBQUFTLFNBQXBPLENBQXFPM2IsQ0FBck8sRUFBdU9wQixFQUFFVixNQUF6TyxDQUFOLEVBQXVQb2QsSUFBRSxDQUE3UCxFQUErUEEsSUFBRUssRUFBRXpkLE1BQW5RLEVBQTBRb2QsR0FBMVE7QUFBOFFLLFlBQUVMLENBQUYsSUFBS2EsRUFBRXVCLGdCQUFGLENBQW1COWUsRUFBRTBjLENBQUYsQ0FBbkIsRUFBd0JLLEVBQUVMLENBQUYsQ0FBeEIsQ0FBTDtBQUE5USxTQUFpVCxPQUFPSyxDQUFQO0FBQVMsY0FBT04sRUFBRXdGLGFBQUYsQ0FBZ0JqaUIsQ0FBaEIsSUFBbUIsVUFBU29CLENBQVQsRUFBV1gsQ0FBWCxFQUFhVCxDQUFiLEVBQWU7QUFBQyxZQUFJK2MsSUFBRSxJQUFOO0FBQUEsWUFBV0wsSUFBRSxFQUFiLENBQWdCLEtBQUksSUFBSU8sQ0FBUixJQUFhN2IsQ0FBYjtBQUFlLGNBQUc2YixDQUFILEVBQUs7QUFBQyxnQkFBSXBELElBQUU3WixFQUFFaWQsQ0FBRixDQUFOLENBQVcsWUFBVSxPQUFPcEQsQ0FBakIsS0FBcUJBLElBQUUwRCxFQUFFdUIsZ0JBQUYsQ0FBbUIxZCxFQUFFNmIsQ0FBRixDQUFuQixFQUF3QnBELENBQXhCLENBQXZCLEdBQW1ENkMsRUFBRU8sQ0FBRixJQUFLcEQsQ0FBeEQ7QUFBMEQsV0FBM0UsTUFBZ0ZrRCxJQUFFUSxFQUFFdUIsZ0JBQUYsQ0FBbUIxZCxFQUFFNmIsQ0FBRixDQUFuQixFQUF3QnhjLENBQXhCLENBQUY7QUFBL0YsU0FBNEgsT0FBTSxDQUFDc2MsQ0FBRCxFQUFHTCxDQUFILENBQU47QUFBWSxPQUF4SyxDQUF5SzFjLENBQXpLLEVBQTJLb0IsQ0FBM0ssRUFBNktYLENBQTdLLENBQW5CLEdBQW1NLENBQUM4YyxFQUFFdUIsZ0JBQUYsQ0FBbUI5ZSxDQUFuQixFQUFxQm9CLENBQXJCLENBQUQsQ0FBMU07QUFBb08sS0FBbnpDLEVBQW96QzRnQixpQkFBZ0IsUUFBcDBDLEVBQTYwQ0UsVUFBUyxDQUF0MUMsRUFBWixDQUFxMkMsSUFBSXJJLElBQUUsVUFBU3pZLENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsU0FBSzBoQixTQUFMLEdBQWUsbUJBQWYsRUFBbUMsS0FBS0MsTUFBTCxHQUFZLElBQS9DLEVBQW9ELEtBQUs3UixJQUFMLENBQVVuUCxLQUFHLEVBQWIsRUFBZ0JYLEtBQUcsRUFBbkIsQ0FBcEQ7QUFBMkUsR0FBL0Y7QUFBQSxNQUFnR3pCLElBQUUsRUFBQ3FqQixPQUFNLGc2QkFBUCxFQUF3NkIzRCxRQUFPLDhCQUEvNkIsRUFBODhCRCxTQUFRLFNBQXQ5QixFQUFnK0I2RCxRQUFPLE9BQXYrQixFQUErK0JDLFVBQVMsUUFBeC9CLEVBQWlnQ2xFLE1BQUssRUFBQ25iLE1BQUssVUFBUzlCLENBQVQsRUFBVztBQUFDLGVBQU8sU0FBT21jLEVBQUVVLEtBQUYsQ0FBUUksSUFBUixDQUFhamQsQ0FBYixDQUFkO0FBQThCLE9BQWhELEVBQXRnQyxFQUF3akNvaEIsS0FBSSxJQUFJaE8sTUFBSixDQUFXLG1YQUFYLENBQTVqQyxFQUFsRyxDQUEraER4VixFQUFFeWpCLEtBQUYsR0FBUXpqQixFQUFFMGYsTUFBVixDQUFpQixJQUFJZ0UsSUFBRSxVQUFTdGhCLENBQVQsRUFBVztBQUFDLFFBQUlYLElBQUUsQ0FBQyxLQUFHVyxDQUFKLEVBQU91VCxLQUFQLENBQWEsa0NBQWIsQ0FBTixDQUF1RCxPQUFPbFUsSUFBRWpCLEtBQUtnRSxHQUFMLENBQVMsQ0FBVCxFQUFXLENBQUMvQyxFQUFFLENBQUYsSUFBS0EsRUFBRSxDQUFGLEVBQUtuQixNQUFWLEdBQWlCLENBQWxCLEtBQXNCbUIsRUFBRSxDQUFGLElBQUssQ0FBQ0EsRUFBRSxDQUFGLENBQU4sR0FBVyxDQUFqQyxDQUFYLENBQUYsR0FBa0QsQ0FBekQ7QUFBMkQsR0FBcEk7QUFBQSxNQUFxSWtpQixJQUFFLFVBQVMxRixDQUFULEVBQVdwRCxDQUFYLEVBQWE7QUFBQyxXQUFPLFVBQVN6WSxDQUFULEVBQVc7QUFBQyxXQUFJLElBQUlYLElBQUVvQixVQUFVdkMsTUFBaEIsRUFBdUJVLElBQUUsSUFBSXpELEtBQUosQ0FBVSxJQUFFa0UsQ0FBRixHQUFJQSxJQUFFLENBQU4sR0FBUSxDQUFsQixDQUF6QixFQUE4Q3NjLElBQUUsQ0FBcEQsRUFBc0RBLElBQUV0YyxDQUF4RCxFQUEwRHNjLEdBQTFEO0FBQThEL2MsVUFBRStjLElBQUUsQ0FBSixJQUFPbGIsVUFBVWtiLENBQVYsQ0FBUDtBQUE5RCxPQUFrRixPQUFPL2MsRUFBRTRpQixHQUFGLElBQVEsQ0FBQ3hoQixDQUFELElBQUl5WSxFQUFFL1gsS0FBRixDQUFRLEtBQUssQ0FBYixFQUFlLENBQUNWLENBQUQsRUFBSThDLE1BQUosQ0FBV21aLEdBQUdYLElBQUVPLENBQUYsRUFBSWpkLEVBQUVXLEdBQUYsQ0FBTTRjLEVBQUVVLEtBQUYsQ0FBUXZCLENBQVIsQ0FBTixDQUFQLEVBQVgsQ0FBZixDQUFuQixDQUEwRSxJQUFJQSxDQUFKO0FBQU0sS0FBckw7QUFBc0wsR0FBM1U7QUFBQSxNQUE0VW1HLElBQUUsVUFBU3poQixDQUFULEVBQVc7QUFBQyxXQUFNLEVBQUN1Z0IsY0FBYWdCLEVBQUUsTUFBRixFQUFTdmhCLENBQVQsQ0FBZCxFQUEwQnlnQixnQkFBZWMsRUFBRSxRQUFGLEVBQVd2aEIsQ0FBWCxDQUF6QyxFQUF1RDRnQixpQkFBZ0I1Z0IsRUFBRTlCLE1BQUYsSUFBVSxDQUFWLEdBQVksUUFBWixHQUFxQixDQUFDLFFBQUQsRUFBVSxRQUFWLENBQTVGLEVBQWdINGlCLFVBQVMsRUFBekgsRUFBTjtBQUFtSSxHQUE3ZCxDQUE4ZHJJLEVBQUVyZCxTQUFGLEdBQVksRUFBQytULE1BQUssVUFBU25QLENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsV0FBSSxJQUFJVCxDQUFSLElBQWEsS0FBSzhpQixPQUFMLEdBQWFyaUIsQ0FBYixFQUFlLEtBQUtzaUIsVUFBTCxHQUFnQmxHLEVBQUUsRUFBRixFQUFLLEtBQUtrRyxVQUFWLENBQS9CLEVBQXFEM2hCLENBQWxFO0FBQW9FLGFBQUs0aEIsWUFBTCxDQUFrQmhqQixDQUFsQixFQUFvQm9CLEVBQUVwQixDQUFGLEVBQUswQyxFQUF6QixFQUE0QnRCLEVBQUVwQixDQUFGLEVBQUtraUIsUUFBakM7QUFBcEUsT0FBK0ducEIsT0FBT2txQixPQUFQLENBQWUva0IsT0FBZixDQUF1Qix3QkFBdkI7QUFBaUQsS0FBcEwsRUFBcUxnbEIsV0FBVSxVQUFTOWhCLENBQVQsRUFBVztBQUFDLFVBQUcsS0FBSyxDQUFMLEtBQVMsS0FBSzBoQixPQUFMLENBQWExaEIsQ0FBYixDQUFaLEVBQTRCLE1BQU0sSUFBSTBFLEtBQUosQ0FBVTFFLElBQUUsa0NBQVosQ0FBTixDQUFzRCxPQUFPLEtBQUtnaEIsTUFBTCxHQUFZaGhCLENBQVosRUFBYyxJQUFyQjtBQUEwQixLQUF2VCxFQUF3VCtoQixZQUFXLFVBQVMvaEIsQ0FBVCxFQUFXWCxDQUFYLEVBQWFULENBQWIsRUFBZTtBQUFDLGFBQU0sYUFBVzBjLEVBQUVqYyxDQUFGLENBQVgsS0FBa0IsS0FBS3FpQixPQUFMLENBQWExaEIsQ0FBYixJQUFnQlgsQ0FBbEMsR0FBcUMsQ0FBQyxDQUFELEtBQUtULENBQUwsR0FBTyxLQUFLa2pCLFNBQUwsQ0FBZTloQixDQUFmLENBQVAsR0FBeUIsSUFBcEU7QUFBeUUsS0FBNVosRUFBNlpnaUIsWUFBVyxVQUFTaGlCLENBQVQsRUFBV1gsQ0FBWCxFQUFhVCxDQUFiLEVBQWU7QUFBQyxhQUFPLEtBQUssQ0FBTCxLQUFTLEtBQUs4aUIsT0FBTCxDQUFhMWhCLENBQWIsQ0FBVCxLQUEyQixLQUFLMGhCLE9BQUwsQ0FBYTFoQixDQUFiLElBQWdCLEVBQTNDLEdBQStDLEtBQUswaEIsT0FBTCxDQUFhMWhCLENBQWIsRUFBZ0JYLENBQWhCLElBQW1CVCxDQUFsRSxFQUFvRSxJQUEzRTtBQUFnRixLQUF4Z0IsRUFBeWdCcWpCLGFBQVksVUFBU2ppQixDQUFULEVBQVdYLENBQVgsRUFBYTtBQUFDLFdBQUksSUFBSVQsQ0FBUixJQUFhUyxDQUFiO0FBQWUsYUFBSzJpQixVQUFMLENBQWdCaGlCLENBQWhCLEVBQWtCcEIsQ0FBbEIsRUFBb0JTLEVBQUVULENBQUYsQ0FBcEI7QUFBZixPQUF5QyxPQUFPLElBQVA7QUFBWSxLQUF4bEIsRUFBeWxCZ2pCLGNBQWEsVUFBUzVoQixDQUFULEVBQVdYLENBQVgsRUFBYVQsQ0FBYixFQUFlO0FBQUMsVUFBRyxLQUFLK2lCLFVBQUwsQ0FBZ0IzaEIsQ0FBaEIsQ0FBSCxFQUFzQm1jLEVBQUVqZCxJQUFGLENBQU8sZ0JBQWNjLENBQWQsR0FBZ0IsdUJBQXZCLEVBQXRCLEtBQTJFLElBQUcyYixFQUFFdFQsY0FBRixDQUFpQnJJLENBQWpCLENBQUgsRUFBdUIsT0FBTyxLQUFLbWMsRUFBRWpkLElBQUYsQ0FBTyxNQUFJYyxDQUFKLEdBQU0sOERBQWIsQ0FBWixDQUF5RixPQUFPLEtBQUtraUIsYUFBTCxDQUFtQnhoQixLQUFuQixDQUF5QixJQUF6QixFQUE4QkQsU0FBOUIsQ0FBUDtBQUFnRCxLQUFqMkIsRUFBazJCMGhCLGNBQWEsVUFBU25pQixDQUFULEVBQVc7QUFBQyxhQUFNLENBQUMsQ0FBQyxLQUFLMmhCLFVBQUwsQ0FBZ0IzaEIsQ0FBaEIsQ0FBUjtBQUEyQixLQUF0NUIsRUFBdTVCb2lCLGlCQUFnQixVQUFTcGlCLENBQVQsRUFBV1gsQ0FBWCxFQUFhVCxDQUFiLEVBQWU7QUFBQyxhQUFPLEtBQUsraUIsVUFBTCxDQUFnQjNoQixDQUFoQixJQUFtQixLQUFLa2lCLGFBQUwsQ0FBbUJ4aEIsS0FBbkIsQ0FBeUIsSUFBekIsRUFBOEJELFNBQTlCLENBQW5CLElBQTZEMGIsRUFBRWpkLElBQUYsQ0FBTyxnQkFBY2MsQ0FBZCxHQUFnQiwyQkFBdkIsR0FBb0QsS0FBSzRoQixZQUFMLENBQWtCbGhCLEtBQWxCLENBQXdCLElBQXhCLEVBQTZCRCxTQUE3QixDQUFqSCxDQUFQO0FBQWlLLEtBQXhsQyxFQUF5bEM0aEIsaUJBQWdCLFVBQVNyaUIsQ0FBVCxFQUFXO0FBQUMsYUFBTyxLQUFLMmhCLFVBQUwsQ0FBZ0IzaEIsQ0FBaEIsS0FBb0JtYyxFQUFFamQsSUFBRixDQUFPLGdCQUFjYyxDQUFkLEdBQWdCLG1CQUF2QixDQUFwQixFQUFnRSxPQUFPLEtBQUsyaEIsVUFBTCxDQUFnQjNoQixDQUFoQixDQUF2RSxFQUEwRixJQUFqRztBQUFzRyxLQUEzdEMsRUFBNHRDa2lCLGVBQWMsVUFBU2xpQixDQUFULEVBQVdYLENBQVgsRUFBYVQsQ0FBYixFQUFlO0FBQUMsV0FBSSxJQUFJK2MsQ0FBUixJQUFZLGFBQVdMLEVBQUVqYyxDQUFGLENBQVgsS0FBa0JBLElBQUUsRUFBQ2lDLElBQUdqQyxDQUFKLEVBQU15aEIsVUFBU2xpQixDQUFmLEVBQXBCLEdBQXVDUyxFQUFFZ2hCLFFBQUYsS0FBYWhoQixJQUFFLElBQUkrZ0IsQ0FBSixDQUFNL2dCLENBQU4sQ0FBZixDQUF2QyxFQUFnRSxDQUFDLEtBQUtzaUIsVUFBTCxDQUFnQjNoQixDQUFoQixJQUFtQlgsQ0FBcEIsRUFBdUJpakIsUUFBdkIsSUFBaUMsRUFBN0c7QUFBZ0gsYUFBS04sVUFBTCxDQUFnQnJHLENBQWhCLEVBQWtCM2IsQ0FBbEIsRUFBb0JYLEVBQUVpakIsUUFBRixDQUFXM0csQ0FBWCxDQUFwQjtBQUFoSCxPQUFtSixPQUFPLElBQVA7QUFBWSxLQUF6NUMsRUFBMDVDNEcsaUJBQWdCLFVBQVN2aUIsQ0FBVCxFQUFXO0FBQUMsVUFBSVgsQ0FBSixDQUFNLFdBQVNXLEVBQUU5RCxJQUFYLEdBQWdCbUQsSUFBRSxDQUFDLEtBQUtxaUIsT0FBTCxDQUFhLEtBQUtWLE1BQWxCLEVBQTBCaGhCLEVBQUU5RCxJQUE1QixLQUFtQyxFQUFwQyxFQUF3QzhELEVBQUV3aUIsWUFBMUMsQ0FBbEIsR0FBMEVuakIsSUFBRSxLQUFLb2pCLGFBQUwsQ0FBbUIsS0FBS2YsT0FBTCxDQUFhLEtBQUtWLE1BQWxCLEVBQTBCaGhCLEVBQUU5RCxJQUE1QixDQUFuQixFQUFxRDhELEVBQUV3aUIsWUFBdkQsQ0FBNUUsQ0FBaUosT0FBT25qQixLQUFHLEtBQUtxaUIsT0FBTCxDQUFhLEtBQUtWLE1BQWxCLEVBQTBCMEIsY0FBN0IsSUFBNkMsS0FBS2hCLE9BQUwsQ0FBYWlCLEVBQWIsQ0FBZ0JELGNBQXBFO0FBQW1GLEtBQWhxRCxFQUFpcURELGVBQWMsVUFBU3ppQixDQUFULEVBQVdYLENBQVgsRUFBYTtBQUFDLFVBQUcsYUFBV2ljLEVBQUVqYyxDQUFGLENBQWQsRUFBbUIsT0FBTSxZQUFVLE9BQU9XLENBQWpCLEdBQW1CQSxFQUFFcUQsT0FBRixDQUFVLEtBQVYsRUFBZ0JoRSxDQUFoQixDQUFuQixHQUFzQyxFQUE1QyxDQUErQyxLQUFJLElBQUlULENBQVIsSUFBYVMsQ0FBYjtBQUFlVyxZQUFFLEtBQUt5aUIsYUFBTCxDQUFtQnppQixDQUFuQixFQUFxQlgsRUFBRVQsQ0FBRixDQUFyQixDQUFGO0FBQWYsT0FBNEMsT0FBT29CLENBQVA7QUFBUyxLQUFwekQsRUFBcXpEMmhCLFlBQVcsRUFBQ2lCLFVBQVMsRUFBQ2xDLGdCQUFlLFVBQVMxZ0IsQ0FBVCxFQUFXO0FBQUMsaUJBQU0sTUFBSzhCLElBQUwsQ0FBVTlCLENBQVY7QUFBTjtBQUFtQixTQUEvQyxFQUFnRDhnQixVQUFTLENBQXpELEVBQVYsRUFBc0UrQixVQUFTLEVBQUN2QyxrQkFBaUIsVUFBU3RnQixDQUFULEVBQVc7QUFBQyxpQkFBTyxJQUFFQSxFQUFFOUIsTUFBWDtBQUFrQixTQUFoRCxFQUFpRHdpQixnQkFBZSxVQUFTMWdCLENBQVQsRUFBVztBQUFDLGlCQUFNLE1BQUs4QixJQUFMLENBQVU5QixDQUFWO0FBQU47QUFBbUIsU0FBL0YsRUFBZ0c4Z0IsVUFBUyxHQUF6RyxFQUEvRSxFQUE2THpuQixNQUFLLEVBQUNxbkIsZ0JBQWUsVUFBUzFnQixDQUFULEVBQVdYLENBQVgsRUFBYTtBQUFDLGNBQUlULElBQUUsSUFBRTZCLFVBQVV2QyxNQUFaLElBQW9CLEtBQUssQ0FBTCxLQUFTdUMsVUFBVSxDQUFWLENBQTdCLEdBQTBDQSxVQUFVLENBQVYsQ0FBMUMsR0FBdUQsRUFBN0Q7QUFBQSxjQUFnRWtiLElBQUUvYyxFQUFFa2tCLElBQXBFO0FBQUEsY0FBeUV4SCxJQUFFLEtBQUssQ0FBTCxLQUFTSyxDQUFULEdBQVcsS0FBWCxHQUFpQkEsQ0FBNUY7QUFBQSxjQUE4RkUsSUFBRWpkLEVBQUVta0IsSUFBbEc7QUFBQSxjQUF1R3RLLElBQUUsS0FBSyxDQUFMLEtBQVNvRCxDQUFULEdBQVcsQ0FBWCxHQUFhQSxDQUF0SDtBQUFBLGNBQXdISixJQUFFN2QsRUFBRXlCLENBQUYsQ0FBMUgsQ0FBK0gsSUFBRyxDQUFDb2MsQ0FBSixFQUFNLE1BQU0sSUFBSS9XLEtBQUosQ0FBVSxxQkFBbUJyRixDQUFuQixHQUFxQixvQkFBL0IsQ0FBTixDQUEyRCxJQUFHLENBQUNXLENBQUosRUFBTSxPQUFNLENBQUMsQ0FBUCxDQUFTLElBQUcsQ0FBQ3liLEVBQUUzWixJQUFGLENBQU85QixDQUFQLENBQUosRUFBYyxPQUFNLENBQUMsQ0FBUCxDQUFTLElBQUcsYUFBV1gsQ0FBWCxJQUFjLENBQUMsU0FBU3lDLElBQVQsQ0FBY3daLEtBQUcsRUFBakIsQ0FBbEIsRUFBdUM7QUFBQyxnQkFBSU0sSUFBRWpDLE9BQU8zWixDQUFQLENBQU47QUFBQSxnQkFBZ0JpYyxJQUFFN2QsS0FBS2dFLEdBQUwsQ0FBU2tmLEVBQUVoRyxDQUFGLENBQVQsRUFBY2dHLEVBQUU3SSxDQUFGLENBQWQsQ0FBbEIsQ0FBc0MsSUFBRzZJLEVBQUUxRixDQUFGLElBQUtLLENBQVIsRUFBVSxPQUFNLENBQUMsQ0FBUCxDQUFTLElBQUlFLElBQUUsVUFBU25jLENBQVQsRUFBVztBQUFDLHFCQUFPNUIsS0FBS0MsS0FBTCxDQUFXMkIsSUFBRTVCLEtBQUtFLEdBQUwsQ0FBUyxFQUFULEVBQVkyZCxDQUFaLENBQWIsQ0FBUDtBQUFvQyxhQUF0RCxDQUF1RCxJQUFHLENBQUNFLEVBQUVQLENBQUYsSUFBS08sRUFBRTFELENBQUYsQ0FBTixJQUFZMEQsRUFBRWIsQ0FBRixDQUFaLElBQWtCLENBQXJCLEVBQXVCLE9BQU0sQ0FBQyxDQUFQO0FBQVMsa0JBQU0sQ0FBQyxDQUFQO0FBQVMsU0FBcmMsRUFBc2NzRixpQkFBZ0IsRUFBQyxJQUFHLFFBQUosRUFBYWtDLE1BQUssUUFBbEIsRUFBMkJDLE1BQUssUUFBaEMsRUFBdGQsRUFBZ2dCakMsVUFBUyxHQUF6Z0IsRUFBbE0sRUFBZ3RCa0MsU0FBUSxFQUFDdEMsZ0JBQWUsVUFBUzFnQixDQUFULEVBQVdYLENBQVgsRUFBYTtBQUFDLGlCQUFNLENBQUNXLENBQUQsSUFBSVgsRUFBRXlDLElBQUYsQ0FBTzlCLENBQVAsQ0FBVjtBQUFvQixTQUFsRCxFQUFtRDRnQixpQkFBZ0IsUUFBbkUsRUFBNEVFLFVBQVMsRUFBckYsRUFBeHRCLEVBQWl6Qm1DLFdBQVUsRUFBQ3ZDLGdCQUFlLFVBQVMxZ0IsQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQyxpQkFBTSxDQUFDVyxDQUFELElBQUlBLEVBQUU5QixNQUFGLElBQVVtQixDQUFwQjtBQUFzQixTQUFwRCxFQUFxRHVoQixpQkFBZ0IsU0FBckUsRUFBK0VFLFVBQVMsRUFBeEYsRUFBM3pCLEVBQXU1Qm9DLFdBQVUsRUFBQ3hDLGdCQUFlLFVBQVMxZ0IsQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQyxpQkFBT1csRUFBRTlCLE1BQUYsSUFBVW1CLENBQWpCO0FBQW1CLFNBQWpELEVBQWtEdWhCLGlCQUFnQixTQUFsRSxFQUE0RUUsVUFBUyxFQUFyRixFQUFqNkIsRUFBMC9CNWlCLFFBQU8sRUFBQ3dpQixnQkFBZSxVQUFTMWdCLENBQVQsRUFBV1gsQ0FBWCxFQUFhVCxDQUFiLEVBQWU7QUFBQyxpQkFBTSxDQUFDb0IsQ0FBRCxJQUFJQSxFQUFFOUIsTUFBRixJQUFVbUIsQ0FBVixJQUFhVyxFQUFFOUIsTUFBRixJQUFVVSxDQUFqQztBQUFtQyxTQUFuRSxFQUFvRWdpQixpQkFBZ0IsQ0FBQyxTQUFELEVBQVcsU0FBWCxDQUFwRixFQUEwR0UsVUFBUyxFQUFuSCxFQUFqZ0MsRUFBd25DcUMsVUFBUyxFQUFDN0Msa0JBQWlCLFVBQVN0Z0IsQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQyxpQkFBT1csRUFBRTlCLE1BQUYsSUFBVW1CLENBQWpCO0FBQW1CLFNBQW5ELEVBQW9EdWhCLGlCQUFnQixTQUFwRSxFQUE4RUUsVUFBUyxFQUF2RixFQUFqb0MsRUFBNHRDc0MsVUFBUyxFQUFDOUMsa0JBQWlCLFVBQVN0Z0IsQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQyxpQkFBT1csRUFBRTlCLE1BQUYsSUFBVW1CLENBQWpCO0FBQW1CLFNBQW5ELEVBQW9EdWhCLGlCQUFnQixTQUFwRSxFQUE4RUUsVUFBUyxFQUF2RixFQUFydUMsRUFBZzBDdUMsT0FBTSxFQUFDL0Msa0JBQWlCLFVBQVN0Z0IsQ0FBVCxFQUFXWCxDQUFYLEVBQWFULENBQWIsRUFBZTtBQUFDLGlCQUFPb0IsRUFBRTlCLE1BQUYsSUFBVW1CLENBQVYsSUFBYVcsRUFBRTlCLE1BQUYsSUFBVVUsQ0FBOUI7QUFBZ0MsU0FBbEUsRUFBbUVnaUIsaUJBQWdCLENBQUMsU0FBRCxFQUFXLFNBQVgsQ0FBbkYsRUFBeUdFLFVBQVMsRUFBbEgsRUFBdDBDLEVBQTQ3Q2xMLEtBQUk2TCxFQUFFLFVBQVN6aEIsQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQyxlQUFPQSxLQUFHVyxDQUFWO0FBQVksT0FBNUIsQ0FBaDhDLEVBQTg5Q29DLEtBQUlxZixFQUFFLFVBQVN6aEIsQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQyxlQUFPVyxLQUFHWCxDQUFWO0FBQVksT0FBNUIsQ0FBbCtDLEVBQWdnRGdpQixPQUFNSSxFQUFFLFVBQVN6aEIsQ0FBVCxFQUFXWCxDQUFYLEVBQWFULENBQWIsRUFBZTtBQUFDLGVBQU9TLEtBQUdXLENBQUgsSUFBTUEsS0FBR3BCLENBQWhCO0FBQWtCLE9BQXBDLENBQXRnRCxFQUE0aUQwa0IsU0FBUSxFQUFDNUMsZ0JBQWUsVUFBUzFnQixDQUFULEVBQVdYLENBQVgsRUFBYTtBQUFDLGNBQUcsQ0FBQ1csQ0FBSixFQUFNLE9BQU0sQ0FBQyxDQUFQLENBQVMsSUFBSXBCLElBQUV5YyxFQUFFaGMsQ0FBRixDQUFOLENBQVcsT0FBT1QsRUFBRVYsTUFBRixHQUFTOEIsTUFBSXBCLEVBQUV5TCxHQUFGLEVBQWIsR0FBcUJySyxNQUFJWCxDQUFoQztBQUFrQyxTQUExRixFQUEyRnloQixVQUFTLEdBQXBHLEVBQXBqRCxFQUE2cER5QyxTQUFRLEVBQUM3QyxnQkFBZSxVQUFTMWdCLENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsY0FBRyxDQUFDVyxDQUFKLEVBQU0sT0FBTSxDQUFDLENBQVAsQ0FBUyxPQUFNLGlDQUFnQzhCLElBQWhDLENBQXFDOUIsQ0FBckM7QUFBTjtBQUE4QyxTQUEzRixFQUE0RjhnQixVQUFTLEVBQXJHLEVBQXJxRCxFQUFoMEQsRUFBWixDQUE0bEgsSUFBSTBDLElBQUUsRUFBTixDQUFTQSxFQUFFQyxJQUFGLEdBQU8sRUFBQ0Msb0JBQW1CLFlBQVU7QUFBQyxVQUFJcmtCLElBQUUsSUFBTixDQUFXLEtBQUt6QyxRQUFMLENBQWNtTSxFQUFkLENBQWlCLGdCQUFqQixFQUFrQyxVQUFTL0ksQ0FBVCxFQUFXO0FBQUNYLFVBQUVza0IsZ0JBQUYsQ0FBbUIzakIsQ0FBbkI7QUFBc0IsT0FBcEUsR0FBc0UsS0FBS3BELFFBQUwsQ0FBY21NLEVBQWQsQ0FBaUIsZUFBakIsRUFBaUNvVCxFQUFFOEIsZUFBbkMsRUFBbUQsVUFBU2plLENBQVQsRUFBVztBQUFDWCxVQUFFdWtCLGNBQUYsQ0FBaUI1akIsQ0FBakI7QUFBb0IsT0FBbkYsQ0FBdEUsRUFBMkosQ0FBQyxDQUFELEtBQUssS0FBSzhNLE9BQUwsQ0FBYXlSLFNBQWxCLElBQTZCLEtBQUs1YSxPQUFMLENBQWF4SixZQUFiLENBQTBCLFlBQTFCLEVBQXVDLEVBQXZDLENBQXhMO0FBQW1PLEtBQTdRLEVBQThRbWEsT0FBTSxZQUFVO0FBQUMsVUFBRyxFQUFFLEtBQUt1UCxhQUFMLEdBQW1CLElBQXJCLE1BQTZCLEtBQUt6RSxnQkFBbEMsSUFBb0QsV0FBUyxLQUFLdFMsT0FBTCxDQUFhd0gsS0FBN0UsRUFBbUYsT0FBTyxJQUFQLENBQVksS0FBSSxJQUFJdFUsSUFBRSxDQUFWLEVBQVlBLElBQUUsS0FBSzhqQixNQUFMLENBQVk1bEIsTUFBMUIsRUFBaUM4QixHQUFqQyxFQUFxQztBQUFDLFlBQUlYLElBQUUsS0FBS3lrQixNQUFMLENBQVk5akIsQ0FBWixDQUFOLENBQXFCLElBQUcsQ0FBQyxDQUFELEtBQUtYLEVBQUUrZixnQkFBUCxJQUF5QixJQUFFL2YsRUFBRStmLGdCQUFGLENBQW1CbGhCLE1BQTlDLElBQXNELEtBQUssQ0FBTCxLQUFTbUIsRUFBRXlOLE9BQUYsQ0FBVWlYLE9BQXpFLEtBQW1GLEtBQUtGLGFBQUwsR0FBbUJ4a0IsRUFBRXpDLFFBQXJCLEVBQThCLFlBQVUsS0FBS2tRLE9BQUwsQ0FBYXdILEtBQXhJLENBQUgsRUFBa0o7QUFBTSxjQUFPLFNBQU8sS0FBS3VQLGFBQVosR0FBMEIsSUFBMUIsR0FBK0IsS0FBS0EsYUFBTCxDQUFtQnZQLEtBQW5CLEVBQXRDO0FBQWlFLEtBQWxwQixFQUFtcEIwUCxZQUFXLFlBQVU7QUFBQyxXQUFLcG5CLFFBQUwsQ0FBYzRVLEdBQWQsQ0FBa0IsVUFBbEI7QUFBOEIsS0FBdnNCLEVBQVAsRUFBZ3RCZ1MsRUFBRVMsS0FBRixHQUFRLEVBQUNDLFdBQVUsWUFBVTtBQUFDLFVBQUcsS0FBS0MsUUFBTCxJQUFnQixLQUFLQyxHQUF4QixFQUE0QjtBQUFDLFlBQUlwa0IsSUFBRSxTQUFTQSxDQUFULENBQVdYLENBQVgsRUFBYVQsQ0FBYixFQUFlK2MsQ0FBZixFQUFpQjtBQUFDLGVBQUksSUFBSUwsSUFBRSxFQUFOLEVBQVNPLElBQUUsRUFBWCxFQUFjcEQsSUFBRSxDQUFwQixFQUFzQkEsSUFBRXBaLEVBQUVuQixNQUExQixFQUFpQ3VhLEdBQWpDLEVBQXFDO0FBQUMsaUJBQUksSUFBSWdELElBQUUsQ0FBQyxDQUFQLEVBQVNHLElBQUUsQ0FBZixFQUFpQkEsSUFBRWhkLEVBQUVWLE1BQXJCLEVBQTRCMGQsR0FBNUI7QUFBZ0Msa0JBQUd2YyxFQUFFb1osQ0FBRixFQUFLNEwsTUFBTCxDQUFZbm9CLElBQVosS0FBbUIwQyxFQUFFZ2QsQ0FBRixFQUFLeUksTUFBTCxDQUFZbm9CLElBQWxDLEVBQXVDO0FBQUN1ZixvQkFBRSxDQUFDLENBQUgsQ0FBSztBQUFNO0FBQW5GLGFBQW1GQSxJQUFFSSxFQUFFemhCLElBQUYsQ0FBT2lGLEVBQUVvWixDQUFGLENBQVAsQ0FBRixHQUFlNkMsRUFBRWxoQixJQUFGLENBQU9pRixFQUFFb1osQ0FBRixDQUFQLENBQWY7QUFBNEIsa0JBQU0sRUFBQzZMLE1BQUt6SSxDQUFOLEVBQVEwSSxPQUFNakosQ0FBZCxFQUFnQmtKLFNBQVE3SSxJQUFFLEVBQUYsR0FBSzNiLEVBQUVwQixDQUFGLEVBQUlTLENBQUosRUFBTSxDQUFDLENBQVAsRUFBVWtsQixLQUF2QyxFQUFOO0FBQW9ELFNBQTNOLENBQTROLEtBQUtuRixnQkFBak8sRUFBa1AsS0FBS2dGLEdBQUwsQ0FBU0ssb0JBQTNQLENBQU4sQ0FBdVIsS0FBS0wsR0FBTCxDQUFTSyxvQkFBVCxHQUE4QixLQUFLckYsZ0JBQW5DLEVBQW9ELEtBQUtzRixrQkFBTCxFQUFwRCxFQUE4RSxLQUFLQyxxQkFBTCxDQUEyQjNrQixDQUEzQixDQUE5RSxFQUE0RyxLQUFLMGpCLGtCQUFMLEVBQTVHLEVBQXNJLENBQUMxakIsRUFBRXNrQixJQUFGLENBQU9wbUIsTUFBUixJQUFnQixDQUFDOEIsRUFBRXVrQixLQUFGLENBQVFybUIsTUFBekIsSUFBaUMsS0FBSzBtQixXQUF0QyxLQUFvRCxLQUFLQSxXQUFMLEdBQWlCLENBQUMsQ0FBbEIsRUFBb0IsS0FBS2xCLGtCQUFMLEVBQXhFLENBQXRJO0FBQXlPO0FBQUMsS0FBcGpCLEVBQXFqQm1CLG1CQUFrQixZQUFVO0FBQUMsVUFBRyxDQUFDLENBQUQsS0FBSyxLQUFLekYsZ0JBQWIsRUFBOEIsT0FBTSxFQUFOLENBQVMsS0FBSSxJQUFJcGYsSUFBRSxFQUFOLEVBQVNYLElBQUUsQ0FBZixFQUFpQkEsSUFBRSxLQUFLK2YsZ0JBQUwsQ0FBc0JsaEIsTUFBekMsRUFBZ0RtQixHQUFoRDtBQUFvRFcsVUFBRTVGLElBQUYsQ0FBTyxLQUFLZ2xCLGdCQUFMLENBQXNCL2YsQ0FBdEIsRUFBeUJ5bEIsWUFBekIsSUFBdUMsS0FBS0MsZ0JBQUwsQ0FBc0IsS0FBSzNGLGdCQUFMLENBQXNCL2YsQ0FBdEIsRUFBeUJnbEIsTUFBL0MsQ0FBOUM7QUFBcEQsT0FBMEosT0FBT3JrQixDQUFQO0FBQVMsS0FBNXhCLEVBQTZ4QmdsQixVQUFTLFVBQVNobEIsQ0FBVCxFQUFXO0FBQUMsVUFBSVgsSUFBRSxJQUFFb0IsVUFBVXZDLE1BQVosSUFBb0IsS0FBSyxDQUFMLEtBQVN1QyxVQUFVLENBQVYsQ0FBN0IsR0FBMENBLFVBQVUsQ0FBVixDQUExQyxHQUF1RCxFQUE3RDtBQUFBLFVBQWdFN0IsSUFBRVMsRUFBRTRsQixPQUFwRTtBQUFBLFVBQTRFdEosSUFBRXRjLEVBQUVnbEIsTUFBaEY7QUFBQSxVQUF1Ri9JLElBQUVqYyxFQUFFNmxCLFdBQTNGO0FBQUEsVUFBdUdySixJQUFFLEtBQUssQ0FBTCxLQUFTUCxDQUFULElBQVlBLENBQXJILENBQXVILEtBQUs2SSxRQUFMLElBQWdCLEtBQUtnQixTQUFMLENBQWVubEIsQ0FBZixFQUFpQixFQUFDaWxCLFNBQVFybUIsQ0FBVCxFQUFXeWxCLFFBQU8xSSxDQUFsQixFQUFqQixDQUFoQixFQUF1REUsS0FBRyxLQUFLdUosV0FBTCxFQUExRDtBQUE2RSxLQUF0L0IsRUFBdS9CQyxhQUFZLFVBQVNybEIsQ0FBVCxFQUFXO0FBQUMsVUFBSVgsSUFBRSxJQUFFb0IsVUFBVXZDLE1BQVosSUFBb0IsS0FBSyxDQUFMLEtBQVN1QyxVQUFVLENBQVYsQ0FBN0IsR0FBMENBLFVBQVUsQ0FBVixDQUExQyxHQUF1RCxFQUE3RDtBQUFBLFVBQWdFN0IsSUFBRVMsRUFBRTRsQixPQUFwRTtBQUFBLFVBQTRFdEosSUFBRXRjLEVBQUVnbEIsTUFBaEY7QUFBQSxVQUF1Ri9JLElBQUVqYyxFQUFFNmxCLFdBQTNGO0FBQUEsVUFBdUdySixJQUFFLEtBQUssQ0FBTCxLQUFTUCxDQUFULElBQVlBLENBQXJILENBQXVILEtBQUs2SSxRQUFMLElBQWdCLEtBQUttQixZQUFMLENBQWtCdGxCLENBQWxCLEVBQW9CLEVBQUNpbEIsU0FBUXJtQixDQUFULEVBQVd5bEIsUUFBTzFJLENBQWxCLEVBQXBCLENBQWhCLEVBQTBERSxLQUFHLEtBQUt1SixXQUFMLEVBQTdEO0FBQWdGLEtBQXR0QyxFQUF1dENHLGFBQVksVUFBU3ZsQixDQUFULEVBQVc7QUFBQyxVQUFJWCxJQUFFLENBQUMsSUFBRW9CLFVBQVV2QyxNQUFaLElBQW9CLEtBQUssQ0FBTCxLQUFTdUMsVUFBVSxDQUFWLENBQTdCLEdBQTBDQSxVQUFVLENBQVYsQ0FBMUMsR0FBdUQsRUFBeEQsRUFBNER5a0IsV0FBbEU7QUFBQSxVQUE4RXRtQixJQUFFLEtBQUssQ0FBTCxLQUFTUyxDQUFULElBQVlBLENBQTVGLENBQThGLEtBQUs4a0IsUUFBTCxJQUFnQixLQUFLcUIsWUFBTCxDQUFrQnhsQixDQUFsQixDQUFoQixFQUFxQ3BCLEtBQUcsS0FBSzhsQixrQkFBTCxFQUF4QztBQUFrRSxLQUEvNEMsRUFBZzVDQSxvQkFBbUIsWUFBVTtBQUFDLFdBQUtlLGNBQUwsTUFBdUIsS0FBS0MsZUFBTCxFQUF2QixJQUErQyxDQUFDLENBQUQsS0FBSyxLQUFLdEcsZ0JBQXpELEdBQTBFLEtBQUt1RyxhQUFMLEVBQTFFLEdBQStGLElBQUUsS0FBS3ZHLGdCQUFMLENBQXNCbGhCLE1BQXhCLEdBQStCLEtBQUtrbkIsV0FBTCxFQUEvQixHQUFrRCxLQUFLUSxXQUFMLEVBQWpKO0FBQW9LLEtBQWxsRCxFQUFtbERqQix1QkFBc0IsVUFBUzNrQixDQUFULEVBQVc7QUFBQyxVQUFHLEtBQUssQ0FBTCxLQUFTLEtBQUs4TSxPQUFMLENBQWErWSxzQkFBekIsRUFBZ0Q7QUFBQyxZQUFHLEtBQUssQ0FBTCxLQUFTLEtBQUsvWSxPQUFMLENBQWFnWSxZQUF6QixFQUFzQyxPQUFPOWtCLEVBQUV1a0IsS0FBRixDQUFRcm1CLE1BQVIsSUFBZ0I4QixFQUFFc2tCLElBQUYsQ0FBT3BtQixNQUF2QixJQUErQixLQUFLNG5CLG1CQUFMLElBQTJCLE1BQUksS0FBSzFCLEdBQUwsQ0FBUzJCLGNBQVQsQ0FBd0JqbkIsSUFBeEIsQ0FBNkIsK0JBQTdCLEVBQThEWixNQUFsRSxJQUEwRSxLQUFLa21CLEdBQUwsQ0FBUzJCLGNBQVQsQ0FBd0I3UyxNQUF4QixDQUErQm1JLEVBQUUsS0FBS3ZPLE9BQUwsQ0FBYWlTLGFBQWYsRUFBOEJwVCxRQUE5QixDQUF1Qyw4QkFBdkMsQ0FBL0IsQ0FBckcsRUFBNE0sS0FBS3lZLEdBQUwsQ0FBUzJCLGNBQVQsQ0FBd0JwYSxRQUF4QixDQUFpQyxRQUFqQyxFQUEyQzdNLElBQTNDLENBQWdELCtCQUFoRCxFQUFpRm9ZLElBQWpGLENBQXNGLEtBQUtwSyxPQUFMLENBQWFnWSxZQUFuRyxDQUEzTyxJQUE2VixLQUFLVixHQUFMLENBQVMyQixjQUFULENBQXdCL2tCLFdBQXhCLENBQW9DLFFBQXBDLEVBQThDbEMsSUFBOUMsQ0FBbUQsK0JBQW5ELEVBQW9Gd1ksTUFBcEYsRUFBcFcsQ0FBaWMsS0FBSSxJQUFJalksSUFBRSxDQUFWLEVBQVlBLElBQUVXLEVBQUV3a0IsT0FBRixDQUFVdG1CLE1BQXhCLEVBQStCbUIsR0FBL0I7QUFBbUMsZUFBS21tQixZQUFMLENBQWtCeGxCLEVBQUV3a0IsT0FBRixDQUFVbmxCLENBQVYsRUFBYWdsQixNQUFiLENBQW9Cbm9CLElBQXRDO0FBQW5DLFNBQStFLEtBQUltRCxJQUFFLENBQU4sRUFBUUEsSUFBRVcsRUFBRXVrQixLQUFGLENBQVFybUIsTUFBbEIsRUFBeUJtQixHQUF6QjtBQUE2QixlQUFLOGxCLFNBQUwsQ0FBZW5sQixFQUFFdWtCLEtBQUYsQ0FBUWxsQixDQUFSLEVBQVdnbEIsTUFBWCxDQUFrQm5vQixJQUFqQyxFQUFzQyxFQUFDK29CLFNBQVFqbEIsRUFBRXVrQixLQUFGLENBQVFsbEIsQ0FBUixFQUFXeWxCLFlBQXBCLEVBQWlDVCxRQUFPcmtCLEVBQUV1a0IsS0FBRixDQUFRbGxCLENBQVIsRUFBV2dsQixNQUFuRCxFQUF0QztBQUE3QixTQUErSCxLQUFJaGxCLElBQUUsQ0FBTixFQUFRQSxJQUFFVyxFQUFFc2tCLElBQUYsQ0FBT3BtQixNQUFqQixFQUF3Qm1CLEdBQXhCO0FBQTRCLGVBQUtpbUIsWUFBTCxDQUFrQnRsQixFQUFFc2tCLElBQUYsQ0FBT2psQixDQUFQLEVBQVVnbEIsTUFBVixDQUFpQm5vQixJQUFuQyxFQUF3QyxFQUFDK29CLFNBQVFqbEIsRUFBRXNrQixJQUFGLENBQU9qbEIsQ0FBUCxFQUFVeWxCLFlBQW5CLEVBQWdDVCxRQUFPcmtCLEVBQUVza0IsSUFBRixDQUFPamxCLENBQVAsRUFBVWdsQixNQUFqRCxFQUF4QztBQUE1QjtBQUE4SDtBQUFDLEtBQTE5RSxFQUEyOUVjLFdBQVUsVUFBU25sQixDQUFULEVBQVdYLENBQVgsRUFBYTtBQUFDLFVBQUlULElBQUVTLEVBQUU0bEIsT0FBUjtBQUFBLFVBQWdCdEosSUFBRXRjLEVBQUVnbEIsTUFBcEIsQ0FBMkIsS0FBS3lCLG1CQUFMLElBQTJCLEtBQUsxQixHQUFMLENBQVM0QixrQkFBVCxDQUE0QmhxQixJQUE1QixDQUFpQyxrQkFBakMsRUFBb0QsS0FBS29vQixHQUFMLENBQVM2QixlQUE3RCxDQUEzQixFQUF5RyxLQUFLN0IsR0FBTCxDQUFTMkIsY0FBVCxDQUF3QnBhLFFBQXhCLENBQWlDLFFBQWpDLEVBQTJDdUgsTUFBM0MsQ0FBa0RtSSxFQUFFLEtBQUt2TyxPQUFMLENBQWFpUyxhQUFmLEVBQThCcFQsUUFBOUIsQ0FBdUMsYUFBVzNMLENBQWxELEVBQXFEa1gsSUFBckQsQ0FBMER0WSxLQUFHLEtBQUttbUIsZ0JBQUwsQ0FBc0JwSixDQUF0QixDQUE3RCxDQUFsRCxDQUF6RztBQUFtUCxLQUFqd0YsRUFBa3dGMkosY0FBYSxVQUFTdGxCLENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsVUFBSVQsSUFBRVMsRUFBRTRsQixPQUFSO0FBQUEsVUFBZ0J0SixJQUFFdGMsRUFBRWdsQixNQUFwQixDQUEyQixLQUFLRCxHQUFMLENBQVMyQixjQUFULENBQXdCcGEsUUFBeEIsQ0FBaUMsUUFBakMsRUFBMkM3TSxJQUEzQyxDQUFnRCxjQUFZa0IsQ0FBNUQsRUFBK0RrWCxJQUEvRCxDQUFvRXRZLEtBQUcsS0FBS21tQixnQkFBTCxDQUFzQnBKLENBQXRCLENBQXZFO0FBQWlHLEtBQXo1RixFQUEwNUY2SixjQUFhLFVBQVN4bEIsQ0FBVCxFQUFXO0FBQUMsV0FBS29rQixHQUFMLENBQVM0QixrQkFBVCxDQUE0QmhwQixVQUE1QixDQUF1QyxrQkFBdkMsR0FBMkQsS0FBS29uQixHQUFMLENBQVMyQixjQUFULENBQXdCL2tCLFdBQXhCLENBQW9DLFFBQXBDLEVBQThDbEMsSUFBOUMsQ0FBbUQsY0FBWWtCLENBQS9ELEVBQWtFc1gsTUFBbEUsRUFBM0Q7QUFBc0ksS0FBempHLEVBQTBqR3lOLGtCQUFpQixVQUFTL2tCLENBQVQsRUFBVztBQUFDLFVBQUlYLElBQUVXLEVBQUU5RCxJQUFGLEdBQU8sU0FBYixDQUF1QixPQUFPLEtBQUssQ0FBTCxLQUFTLEtBQUs0USxPQUFMLENBQWF6TixDQUFiLENBQVQsR0FBeUIxSCxPQUFPa3FCLE9BQVAsQ0FBZVksYUFBZixDQUE2QixLQUFLM1YsT0FBTCxDQUFhek4sQ0FBYixDQUE3QixFQUE2Q1csRUFBRXdpQixZQUEvQyxDQUF6QixHQUFzRjdxQixPQUFPa3FCLE9BQVAsQ0FBZVUsZUFBZixDQUErQnZpQixDQUEvQixDQUE3RjtBQUErSCxLQUE3dUcsRUFBOHVHbWtCLFVBQVMsWUFBVTtBQUFDLFVBQUcsQ0FBQyxLQUFLQyxHQUFOLElBQVcsQ0FBQyxDQUFELEtBQUssS0FBS3RYLE9BQUwsQ0FBYXlSLFNBQWhDLEVBQTBDO0FBQUMsWUFBSXZlLElBQUUsRUFBTixDQUFTLEtBQUsyRCxPQUFMLENBQWF4SixZQUFiLENBQTBCLEtBQUsyUyxPQUFMLENBQWEzTyxTQUFiLEdBQXVCLElBQWpELEVBQXNELEtBQUs2Z0IsTUFBM0QsR0FBbUVoZixFQUFFZ21CLGtCQUFGLEdBQXFCLEtBQUtFLG1CQUFMLEVBQXhGLEVBQW1IbG1CLEVBQUVpbUIsZUFBRixHQUFrQixpQkFBZSxLQUFLblosT0FBTCxDQUFhdVIsUUFBYixHQUFzQixjQUFZLEtBQUt2UixPQUFMLENBQWF1UixRQUEvQyxHQUF3RCxLQUFLVyxNQUE1RSxDQUFySSxFQUF5TmhmLEVBQUUrbEIsY0FBRixHQUFpQjFLLEVBQUUsS0FBS3ZPLE9BQUwsQ0FBYWdTLGFBQWYsRUFBOEI5aUIsSUFBOUIsQ0FBbUMsSUFBbkMsRUFBd0NnRSxFQUFFaW1CLGVBQTFDLENBQTFPLEVBQXFTam1CLEVBQUV5a0Isb0JBQUYsR0FBdUIsRUFBNVQsRUFBK1R6a0IsRUFBRW1tQiw0QkFBRixHQUErQixDQUFDLENBQS9WLEVBQWlXLEtBQUsvQixHQUFMLEdBQVNwa0IsQ0FBMVc7QUFBNFc7QUFBQyxLQUFucUgsRUFBb3FIa21CLHFCQUFvQixZQUFVO0FBQUMsVUFBRyxZQUFVLE9BQU8sS0FBS3BaLE9BQUwsQ0FBYThSLFlBQTlCLElBQTRDdkQsRUFBRSxLQUFLdk8sT0FBTCxDQUFhOFIsWUFBZixFQUE2QjFnQixNQUE1RSxFQUFtRixPQUFPbWQsRUFBRSxLQUFLdk8sT0FBTCxDQUFhOFIsWUFBZixDQUFQLENBQW9DLElBQUk1ZSxJQUFFLEtBQUs4TSxPQUFMLENBQWE4UixZQUFuQixDQUFnQyxJQUFHLFlBQVUsT0FBTyxLQUFLOVIsT0FBTCxDQUFhOFIsWUFBOUIsSUFBNEMsY0FBWSxPQUFPam5CLE9BQU8sS0FBS21WLE9BQUwsQ0FBYThSLFlBQXBCLENBQS9ELEtBQW1HNWUsSUFBRXJJLE9BQU8sS0FBS21WLE9BQUwsQ0FBYThSLFlBQXBCLENBQXJHLEdBQXdJLGNBQVksT0FBTzVlLENBQTlKLEVBQWdLO0FBQUMsWUFBSVgsSUFBRVcsRUFBRWtCLElBQUYsQ0FBTyxJQUFQLEVBQVksSUFBWixDQUFOLENBQXdCLElBQUcsS0FBSyxDQUFMLEtBQVM3QixDQUFULElBQVlBLEVBQUVuQixNQUFqQixFQUF3QixPQUFPbUIsQ0FBUDtBQUFTLE9BQTFOLE1BQThOO0FBQUMsWUFBRyxhQUFXaWMsRUFBRXRiLENBQUYsQ0FBWCxJQUFpQkEsYUFBYXNELE1BQTlCLElBQXNDdEQsRUFBRTlCLE1BQTNDLEVBQWtELE9BQU84QixDQUFQLENBQVNBLEtBQUdtYyxFQUFFamQsSUFBRixDQUFPLHdCQUFzQmMsQ0FBdEIsR0FBd0IscURBQS9CLENBQUg7QUFBeUYsY0FBTyxLQUFLb21CLFlBQUwsRUFBUDtBQUEyQixLQUF4dUksRUFBeXVJQSxjQUFhLFlBQVU7QUFBQyxhQUFPLEtBQUt0WixPQUFMLENBQWF1UixRQUFiLElBQXVCLGFBQVcsS0FBSzFhLE9BQUwsQ0FBYWpLLFFBQS9DLEdBQXdELEtBQUtrRCxRQUFMLENBQWNnSCxNQUFkLEVBQXhELEdBQStFLEtBQUtoSCxRQUEzRjtBQUFvRyxLQUFyMkksRUFBczJJa3BCLHFCQUFvQixZQUFVO0FBQUMsVUFBSTlsQixJQUFFLEtBQUs4TSxPQUFMLENBQWErUixlQUFuQixDQUFtQyxJQUFHLE1BQUksS0FBS3VGLEdBQUwsQ0FBUzJCLGNBQVQsQ0FBd0JuaUIsTUFBeEIsR0FBaUMxRixNQUF4QyxFQUErQyxPQUFPLEtBQUtrbUIsR0FBTCxDQUFTMkIsY0FBVCxDQUF3Qm5pQixNQUF4QixFQUFQLENBQXdDLElBQUcsWUFBVSxPQUFPNUQsQ0FBcEIsRUFBc0I7QUFBQyxZQUFHcWIsRUFBRXJiLENBQUYsRUFBSzlCLE1BQVIsRUFBZSxPQUFPbWQsRUFBRXJiLENBQUYsRUFBS2tULE1BQUwsQ0FBWSxLQUFLa1IsR0FBTCxDQUFTMkIsY0FBckIsQ0FBUCxDQUE0QyxjQUFZLE9BQU9wdUIsT0FBT3FJLENBQVAsQ0FBbkIsR0FBNkJBLElBQUVySSxPQUFPcUksQ0FBUCxDQUEvQixHQUF5Q21jLEVBQUVqZCxJQUFGLENBQU8sMkJBQXlCYyxDQUF6QixHQUEyQixxREFBbEMsQ0FBekM7QUFBa0ksY0FBTSxjQUFZLE9BQU9BLENBQW5CLEtBQXVCQSxJQUFFQSxFQUFFa0IsSUFBRixDQUFPLElBQVAsRUFBWSxJQUFaLENBQXpCLEdBQTRDLGFBQVdvYSxFQUFFdGIsQ0FBRixDQUFYLElBQWlCQSxFQUFFOUIsTUFBbkIsR0FBMEI4QixFQUFFa1QsTUFBRixDQUFTLEtBQUtrUixHQUFMLENBQVMyQixjQUFsQixDQUExQixHQUE0RCxLQUFLSyxZQUFMLEdBQW9CQyxLQUFwQixDQUEwQixLQUFLakMsR0FBTCxDQUFTMkIsY0FBbkMsQ0FBOUc7QUFBaUssS0FBcDNKLEVBQXEzSnJDLG9CQUFtQixZQUFVO0FBQUMsVUFBSTFqQixDQUFKO0FBQUEsVUFBTVgsSUFBRSxJQUFSO0FBQUEsVUFBYVQsSUFBRSxLQUFLdWhCLFlBQUwsRUFBZixDQUFtQ3ZoQixFQUFFNFMsR0FBRixDQUFNLFVBQU4sR0FBa0IsS0FBS29ULFdBQUwsR0FBaUJobUIsRUFBRW1LLEVBQUYsQ0FBS29ULEVBQUV3QixlQUFGLENBQWtCLEtBQUs3USxPQUFMLENBQWEyUixtQkFBL0IsRUFBbUQsU0FBbkQsQ0FBTCxFQUFtRSxZQUFVO0FBQUNwZixVQUFFaW5CLGlCQUFGO0FBQXNCLE9BQXBHLENBQWpCLEdBQXVILENBQUN0bUIsSUFBRW1jLEVBQUV3QixlQUFGLENBQWtCLEtBQUs3USxPQUFMLENBQWFoUSxPQUEvQixFQUF1QyxTQUF2QyxDQUFILEtBQXVEOEIsRUFBRW1LLEVBQUYsQ0FBSy9JLENBQUwsRUFBTyxVQUFTQSxDQUFULEVBQVc7QUFBQ1gsVUFBRWluQixpQkFBRixDQUFvQnRtQixDQUFwQjtBQUF1QixPQUExQyxDQUFoTTtBQUE0TyxLQUFscUssRUFBbXFLc21CLG1CQUFrQixVQUFTdG1CLENBQVQsRUFBVztBQUFDLFVBQUlYLElBQUUsSUFBTixDQUFXVyxLQUFHLFlBQVk4QixJQUFaLENBQWlCOUIsRUFBRTNHLElBQW5CLENBQUgsS0FBOEIsQ0FBQyxLQUFLK3FCLEdBQU4sSUFBVyxDQUFDLEtBQUtBLEdBQUwsQ0FBUytCLDRCQUFuRCxLQUFrRixLQUFLSSxRQUFMLEdBQWdCcm9CLE1BQWhCLElBQXdCLEtBQUs0TyxPQUFMLENBQWEwUixtQkFBdkgsS0FBNkksS0FBSzFSLE9BQUwsQ0FBYTZFLFFBQWIsSUFBdUJoYSxPQUFPc0IsWUFBUCxDQUFvQixLQUFLdXRCLFVBQXpCLEdBQXFDLEtBQUtBLFVBQUwsR0FBZ0I3dUIsT0FBT21CLFVBQVAsQ0FBa0IsWUFBVTtBQUFDLGVBQU91RyxFQUFFZ2hCLFFBQUYsRUFBUDtBQUFvQixPQUFqRCxFQUFrRCxLQUFLdlQsT0FBTCxDQUFhNkUsUUFBL0QsQ0FBNUUsSUFBc0osS0FBSzBPLFFBQUwsRUFBblM7QUFBb1QsS0FBaGdMLEVBQWlnTG9HLFVBQVMsWUFBVTtBQUFDLFdBQUs3QixXQUFMLEdBQWlCLENBQUMsQ0FBbEIsRUFBb0IsS0FBS2xCLGtCQUFMLEVBQXBCLEVBQThDLEtBQUssQ0FBTCxLQUFTLEtBQUtVLEdBQWQsS0FBb0IsS0FBS0EsR0FBTCxDQUFTMkIsY0FBVCxDQUF3Qi9rQixXQUF4QixDQUFvQyxRQUFwQyxFQUE4QzJMLFFBQTlDLEdBQXlEMkssTUFBekQsSUFBa0UsS0FBS3NPLFdBQUwsRUFBbEUsRUFBcUYsS0FBS3hCLEdBQUwsQ0FBU0ssb0JBQVQsR0FBOEIsRUFBbkgsRUFBc0gsS0FBS0wsR0FBTCxDQUFTK0IsNEJBQVQsR0FBc0MsQ0FBQyxDQUFqTCxDQUE5QztBQUFrTyxLQUF2dkwsRUFBd3ZMbkMsWUFBVyxZQUFVO0FBQUMsV0FBS3lDLFFBQUwsSUFBZ0IsS0FBSyxDQUFMLEtBQVMsS0FBS3JDLEdBQWQsSUFBbUIsS0FBS0EsR0FBTCxDQUFTMkIsY0FBVCxDQUF3QnpPLE1BQXhCLEVBQW5DLEVBQW9FLE9BQU8sS0FBSzhNLEdBQWhGO0FBQW9GLEtBQWwyTCxFQUFtMkx1QixlQUFjLFlBQVU7QUFBQyxXQUFLdkIsR0FBTCxDQUFTK0IsNEJBQVQsR0FBc0MsQ0FBQyxDQUF2QyxFQUF5QyxLQUFLL0IsR0FBTCxDQUFTNEIsa0JBQVQsQ0FBNEJobEIsV0FBNUIsQ0FBd0MsS0FBSzhMLE9BQUwsQ0FBYTRSLFVBQXJELEVBQWlFL1MsUUFBakUsQ0FBMEUsS0FBS21CLE9BQUwsQ0FBYTZSLFlBQXZGLENBQXpDO0FBQThJLEtBQTFnTSxFQUEyZ015RyxhQUFZLFlBQVU7QUFBQyxXQUFLaEIsR0FBTCxDQUFTK0IsNEJBQVQsR0FBc0MsQ0FBQyxDQUF2QyxFQUF5QyxLQUFLL0IsR0FBTCxDQUFTNEIsa0JBQVQsQ0FBNEJobEIsV0FBNUIsQ0FBd0MsS0FBSzhMLE9BQUwsQ0FBYTZSLFlBQXJELEVBQW1FaFQsUUFBbkUsQ0FBNEUsS0FBS21CLE9BQUwsQ0FBYTRSLFVBQXpGLENBQXpDO0FBQThJLEtBQWhyTSxFQUFpck1rSCxhQUFZLFlBQVU7QUFBQyxXQUFLeEIsR0FBTCxDQUFTNEIsa0JBQVQsQ0FBNEJobEIsV0FBNUIsQ0FBd0MsS0FBSzhMLE9BQUwsQ0FBYTZSLFlBQXJELEVBQW1FM2QsV0FBbkUsQ0FBK0UsS0FBSzhMLE9BQUwsQ0FBYTRSLFVBQTVGO0FBQXdHLEtBQWh6TSxFQUF4dEIsQ0FBMGdPLElBQUk5UCxJQUFFLFVBQVM1TyxDQUFULEVBQVdYLENBQVgsRUFBYVQsQ0FBYixFQUFlO0FBQUMsU0FBS21pQixTQUFMLEdBQWUsTUFBZixFQUFzQixLQUFLcGQsT0FBTCxHQUFhM0QsQ0FBbkMsRUFBcUMsS0FBS3BELFFBQUwsR0FBY3llLEVBQUVyYixDQUFGLENBQW5ELEVBQXdELEtBQUt5ZixVQUFMLEdBQWdCcGdCLENBQXhFLEVBQTBFLEtBQUt5TixPQUFMLEdBQWFsTyxDQUF2RixFQUF5RixLQUFLZ0YsTUFBTCxHQUFZak0sT0FBT2txQixPQUE1RyxFQUFvSCxLQUFLaUMsTUFBTCxHQUFZLEVBQWhJLEVBQW1JLEtBQUsxRSxnQkFBTCxHQUFzQixJQUF6SjtBQUE4SixHQUFwTDtBQUFBLE1BQXFMc0gsSUFBRSxFQUFDQyxTQUFRLElBQVQsRUFBY0MsVUFBUyxDQUFDLENBQXhCLEVBQTBCQyxVQUFTLENBQUMsQ0FBcEMsRUFBdkwsQ0FBOE5qWSxFQUFFeFQsU0FBRixHQUFZLEVBQUN1b0Isa0JBQWlCLFVBQVMzakIsQ0FBVCxFQUFXO0FBQUMsVUFBSVgsSUFBRSxJQUFOLENBQVcsSUFBRyxDQUFDLENBQUQsS0FBS1csRUFBRW9iLE9BQVYsRUFBa0I7QUFBQyxZQUFJeGMsSUFBRSxLQUFLa29CLGFBQUwsSUFBb0IsS0FBS2xxQixRQUFMLENBQWNrQyxJQUFkLENBQW1CcWQsRUFBRThCLGVBQXJCLEVBQXNDLENBQXRDLENBQTFCLENBQW1FLElBQUcsS0FBSzZJLGFBQUwsR0FBbUIsSUFBbkIsRUFBd0IsS0FBS2xxQixRQUFMLENBQWNrQyxJQUFkLENBQW1CLGtDQUFuQixFQUF1RDVCLElBQXZELENBQTRELFVBQTVELEVBQXVFLENBQUMsQ0FBeEUsQ0FBeEIsRUFBbUcsQ0FBQzBCLENBQUQsSUFBSSxTQUFPQSxFQUFFL0UsWUFBRixDQUFlLGdCQUFmLENBQWpILEVBQWtKO0FBQUNsQyxpQkFBT2txQixPQUFQLENBQWVrRixZQUFmLEdBQTRCLEVBQTVCLENBQStCLElBQUlwTCxJQUFFLEtBQUtxTCxZQUFMLENBQWtCLEVBQUNudUIsT0FBTW1ILENBQVAsRUFBbEIsQ0FBTixDQUFtQyxlQUFhMmIsRUFBRXNMLEtBQUYsRUFBYixJQUF3QixDQUFDLENBQUQsS0FBSyxLQUFLQyxRQUFMLENBQWMsUUFBZCxDQUE3QixLQUF1RGxuQixFQUFFbW5CLHdCQUFGLElBQTZCbm5CLEVBQUU4TixjQUFGLEVBQTdCLEVBQWdELGNBQVk2TixFQUFFc0wsS0FBRixFQUFaLElBQXVCdEwsRUFBRUksSUFBRixDQUFPLFlBQVU7QUFBQzFjLGNBQUUrbkIsT0FBRixDQUFVeG9CLENBQVY7QUFBYSxXQUEvQixDQUE5SDtBQUFnSztBQUFDO0FBQUMsS0FBdGYsRUFBdWZnbEIsZ0JBQWUsVUFBUzVqQixDQUFULEVBQVc7QUFBQyxXQUFLOG1CLGFBQUwsR0FBbUI5bUIsRUFBRXFuQixhQUFyQjtBQUFtQyxLQUFyakIsRUFBc2pCRCxTQUFRLFVBQVNwbkIsQ0FBVCxFQUFXO0FBQUMsVUFBRyxDQUFDLENBQUQsS0FBSyxLQUFLa25CLFFBQUwsQ0FBYyxRQUFkLENBQVIsRUFBZ0M7QUFBQyxZQUFHbG5CLENBQUgsRUFBSztBQUFDLGNBQUlYLElBQUUsS0FBS3pDLFFBQUwsQ0FBY2tDLElBQWQsQ0FBbUIsa0NBQW5CLEVBQXVENUIsSUFBdkQsQ0FBNEQsVUFBNUQsRUFBdUUsQ0FBQyxDQUF4RSxDQUFOLENBQWlGLE1BQUltQyxFQUFFbkIsTUFBTixLQUFlbUIsSUFBRWdjLEVBQUUsK0RBQUYsRUFBbUV2YSxRQUFuRSxDQUE0RSxLQUFLbEUsUUFBakYsQ0FBakIsR0FBNkd5QyxFQUFFckQsSUFBRixDQUFPLEVBQUNFLE1BQUs4RCxFQUFFbkcsWUFBRixDQUFlLE1BQWYsQ0FBTixFQUE2QlQsT0FBTTRHLEVBQUVuRyxZQUFGLENBQWUsT0FBZixDQUFuQyxFQUFQLENBQTdHO0FBQWlMLGNBQUsrQyxRQUFMLENBQWNFLE9BQWQsQ0FBc0IyZSxFQUFFSixFQUFFaU0sS0FBRixDQUFRLFFBQVIsQ0FBRixFQUFvQixFQUFDbE0sU0FBUSxDQUFDLENBQVYsRUFBcEIsQ0FBdEI7QUFBeUQ7QUFBQyxLQUE3NkIsRUFBODZCaUYsVUFBUyxVQUFTcmdCLENBQVQsRUFBVztBQUFDLFVBQUcsS0FBR1MsVUFBVXZDLE1BQWIsSUFBcUIsQ0FBQ21kLEVBQUV3RixhQUFGLENBQWdCN2dCLENBQWhCLENBQXpCLEVBQTRDO0FBQUNtYyxVQUFFVyxRQUFGLENBQVcsMEZBQVgsRUFBdUcsSUFBSXpkLElBQUVsRSxNQUFNQyxTQUFOLENBQWdCcUQsS0FBaEIsQ0FBc0J5QyxJQUF0QixDQUEyQlQsU0FBM0IsQ0FBTixDQUE0Q1QsSUFBRSxFQUFDc2UsT0FBTWpmLEVBQUUsQ0FBRixDQUFQLEVBQVk2Z0IsT0FBTTdnQixFQUFFLENBQUYsQ0FBbEIsRUFBdUJ4RyxPQUFNd0csRUFBRSxDQUFGLENBQTdCLEVBQUY7QUFBcUMsY0FBT3FuQixFQUFFLEtBQUtNLFlBQUwsQ0FBa0JobkIsQ0FBbEIsRUFBcUJpbkIsS0FBckIsRUFBRixDQUFQO0FBQXVDLEtBQS9zQyxFQUFndENELGNBQWEsWUFBVTtBQUFDLFVBQUlobkIsQ0FBSjtBQUFBLFVBQU1YLElBQUUsSUFBUjtBQUFBLFVBQWFULElBQUUsSUFBRTZCLFVBQVV2QyxNQUFaLElBQW9CLEtBQUssQ0FBTCxLQUFTdUMsVUFBVSxDQUFWLENBQTdCLEdBQTBDQSxVQUFVLENBQVYsQ0FBMUMsR0FBdUQsRUFBdEU7QUFBQSxVQUF5RWtiLElBQUUvYyxFQUFFMGYsS0FBN0U7QUFBQSxVQUFtRmhELElBQUUxYyxFQUFFc2hCLEtBQXZGO0FBQUEsVUFBNkZyRSxJQUFFamQsRUFBRS9GLEtBQWpHLENBQXVHLENBQUMsS0FBSzB1QixXQUFMLEdBQWlCMUwsQ0FBbEIsTUFBdUIsS0FBSzBMLFdBQUwsR0FBaUI5TCxFQUFFLEVBQUYsRUFBS0ksQ0FBTCxFQUFPLEVBQUMvTixnQkFBZSxZQUFVO0FBQUNxTyxZQUFFVyxRQUFGLENBQVcsd0dBQVgsR0FBcUh6ZCxFQUFFK2YsZ0JBQUYsR0FBbUIsQ0FBQyxDQUF6STtBQUEySSxTQUF0SyxFQUFQLENBQXhDLEdBQXlOLEtBQUtBLGdCQUFMLEdBQXNCLENBQUMsQ0FBaFAsRUFBa1AsS0FBSzhILFFBQUwsQ0FBYyxVQUFkLENBQWxQLEVBQTRRLEtBQUtNLGNBQUwsRUFBNVEsQ0FBa1MsSUFBSS9PLElBQUUsS0FBS2dQLGdDQUFMLENBQXNDLFlBQVU7QUFBQyxlQUFPcE0sRUFBRTliLEdBQUYsQ0FBTUYsRUFBRXlrQixNQUFSLEVBQWUsVUFBUzlqQixDQUFULEVBQVc7QUFBQyxpQkFBT0EsRUFBRWduQixZQUFGLENBQWUsRUFBQzlHLE9BQU01RSxDQUFQLEVBQVNnRCxPQUFNM0MsQ0FBZixFQUFmLENBQVA7QUFBeUMsU0FBcEUsQ0FBUDtBQUE2RSxPQUE5SCxDQUFOLENBQXNJLE9BQU0sQ0FBQzNiLElBQUVtYyxFQUFFMEIsR0FBRixDQUFNcEYsQ0FBTixFQUFTc0QsSUFBVCxDQUFjLFlBQVU7QUFBQzFjLFVBQUU2bkIsUUFBRixDQUFXLFNBQVg7QUFBc0IsT0FBL0MsRUFBaURRLElBQWpELENBQXNELFlBQVU7QUFBQ3JvQixVQUFFK2YsZ0JBQUYsR0FBbUIsQ0FBQyxDQUFwQixFQUFzQi9mLEVBQUVpVixLQUFGLEVBQXRCLEVBQWdDalYsRUFBRTZuQixRQUFGLENBQVcsT0FBWCxDQUFoQztBQUFvRCxPQUFySCxFQUF1SFMsTUFBdkgsQ0FBOEgsWUFBVTtBQUFDdG9CLFVBQUU2bkIsUUFBRixDQUFXLFdBQVg7QUFBd0IsT0FBakssQ0FBSCxFQUF1S1UsSUFBdkssQ0FBNEtsbkIsS0FBNUssQ0FBa0xWLENBQWxMLEVBQW9MaWMsRUFBRSxLQUFLaUQsZ0NBQUwsRUFBRixDQUFwTCxDQUFOO0FBQXNPLEtBQTc5RCxFQUE4OUQySSxTQUFRLFVBQVM3bkIsQ0FBVCxFQUFXO0FBQUMsVUFBRyxLQUFHUyxVQUFVdkMsTUFBYixJQUFxQixDQUFDbWQsRUFBRXdGLGFBQUYsQ0FBZ0I3Z0IsQ0FBaEIsQ0FBekIsRUFBNEM7QUFBQ21jLFVBQUVXLFFBQUYsQ0FBVyx5RkFBWCxFQUFzRyxJQUFJemQsSUFBRWxFLE1BQU1DLFNBQU4sQ0FBZ0JxRCxLQUFoQixDQUFzQnlDLElBQXRCLENBQTJCVCxTQUEzQixDQUFOLENBQTRDVCxJQUFFLEVBQUNzZSxPQUFNamYsRUFBRSxDQUFGLENBQVAsRUFBWTZnQixPQUFNN2dCLEVBQUUsQ0FBRixDQUFsQixFQUFGO0FBQTBCLGNBQU9xbkIsRUFBRSxLQUFLekcsU0FBTCxDQUFlamdCLENBQWYsRUFBa0JpbkIsS0FBbEIsRUFBRixDQUFQO0FBQW9DLEtBQS91RSxFQUFndkVoSCxXQUFVLFlBQVU7QUFBQyxVQUFJamdCLElBQUUsSUFBTjtBQUFBLFVBQVdYLElBQUUsSUFBRW9CLFVBQVV2QyxNQUFaLElBQW9CLEtBQUssQ0FBTCxLQUFTdUMsVUFBVSxDQUFWLENBQTdCLEdBQTBDQSxVQUFVLENBQVYsQ0FBMUMsR0FBdUQsRUFBcEU7QUFBQSxVQUF1RTdCLElBQUVTLEVBQUVpZixLQUEzRTtBQUFBLFVBQWlGM0MsSUFBRXRjLEVBQUU2Z0IsS0FBckYsQ0FBMkYsS0FBS3NILGNBQUwsR0FBc0IsSUFBSWxNLElBQUUsS0FBS21NLGdDQUFMLENBQXNDLFlBQVU7QUFBQyxlQUFPcE0sRUFBRTliLEdBQUYsQ0FBTVMsRUFBRThqQixNQUFSLEVBQWUsVUFBUzlqQixDQUFULEVBQVc7QUFBQyxpQkFBT0EsRUFBRWlnQixTQUFGLENBQVksRUFBQzNCLE9BQU0xZixDQUFQLEVBQVNzaEIsT0FBTXZFLENBQWYsRUFBWixDQUFQO0FBQXNDLFNBQWpFLENBQVA7QUFBMEUsT0FBM0gsQ0FBTixDQUFtSSxPQUFPUSxFQUFFMEIsR0FBRixDQUFNdkMsQ0FBTixDQUFQO0FBQWdCLEtBQXpnRixFQUEwZ0Z3TSxTQUFRLFlBQVU7QUFBQyxhQUFPLEtBQUtOLGNBQUwsSUFBc0IsSUFBN0I7QUFBa0MsS0FBL2pGLEVBQWdrRjliLE9BQU0sWUFBVTtBQUFDLFdBQUksSUFBSTFMLElBQUUsQ0FBVixFQUFZQSxJQUFFLEtBQUs4akIsTUFBTCxDQUFZNWxCLE1BQTFCLEVBQWlDOEIsR0FBakM7QUFBcUMsYUFBSzhqQixNQUFMLENBQVk5akIsQ0FBWixFQUFlMEwsS0FBZjtBQUFyQyxPQUE0RCxLQUFLd2IsUUFBTCxDQUFjLE9BQWQ7QUFBdUIsS0FBcHFGLEVBQXFxRmEsU0FBUSxZQUFVO0FBQUMsV0FBSy9ELFVBQUwsR0FBa0IsS0FBSSxJQUFJaGtCLElBQUUsQ0FBVixFQUFZQSxJQUFFLEtBQUs4akIsTUFBTCxDQUFZNWxCLE1BQTFCLEVBQWlDOEIsR0FBakM7QUFBcUMsYUFBSzhqQixNQUFMLENBQVk5akIsQ0FBWixFQUFlK25CLE9BQWY7QUFBckMsT0FBOEQsS0FBS25yQixRQUFMLENBQWNLLFVBQWQsQ0FBeUIsU0FBekIsR0FBb0MsS0FBS2lxQixRQUFMLENBQWMsU0FBZCxDQUFwQztBQUE2RCxLQUFyMEYsRUFBczBGTSxnQkFBZSxZQUFVO0FBQUMsYUFBTyxLQUFLaEksZ0JBQUwsR0FBd0J3SSxXQUF4QixFQUFQO0FBQTZDLEtBQTc0RixFQUE4NEZBLGFBQVksWUFBVTtBQUFDLFVBQUkxTSxJQUFFLElBQU47QUFBQSxVQUFXdGIsSUFBRSxLQUFLOGpCLE1BQWxCLENBQXlCLE9BQU8sS0FBS0EsTUFBTCxHQUFZLEVBQVosRUFBZSxLQUFLbUUsZ0JBQUwsR0FBc0IsRUFBckMsRUFBd0MsS0FBS1IsZ0NBQUwsQ0FBc0MsWUFBVTtBQUFDbk0sVUFBRTFlLFFBQUYsQ0FBV2tDLElBQVgsQ0FBZ0J3YyxFQUFFeE8sT0FBRixDQUFVb1IsTUFBMUIsRUFBa0N4TSxHQUFsQyxDQUFzQzRKLEVBQUV4TyxPQUFGLENBQVVxUixRQUFoRCxFQUEwRHpNLEdBQTFELENBQThELElBQUk1TyxNQUFKLENBQVd3WSxFQUFFeE8sT0FBRixDQUFVM08sU0FBckIsRUFBK0IsZ0JBQS9CLENBQTlELEVBQWdIYixJQUFoSCxDQUFxSCxVQUFTMEMsQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQyxjQUFJVCxJQUFFLElBQUlqSCxPQUFPa3FCLE9BQVAsQ0FBZXFHLE9BQW5CLENBQTJCN29CLENBQTNCLEVBQTZCLEVBQTdCLEVBQWdDaWMsQ0FBaEMsQ0FBTixDQUF5QyxJQUFHLFlBQVUxYyxFQUFFbWlCLFNBQVosSUFBdUIsb0JBQWtCbmlCLEVBQUVtaUIsU0FBOUMsRUFBd0Q7QUFBQyxnQkFBSXBGLElBQUUvYyxFQUFFbWlCLFNBQUYsR0FBWSxHQUFaLEdBQWdCbmlCLEVBQUVvZ0IsTUFBeEIsQ0FBK0IsS0FBSyxDQUFMLEtBQVMxRCxFQUFFMk0sZ0JBQUYsQ0FBbUJ0TSxDQUFuQixDQUFULEtBQWlDTCxFQUFFMk0sZ0JBQUYsQ0FBbUJ0TSxDQUFuQixJQUFzQi9jLENBQXRCLEVBQXdCMGMsRUFBRXdJLE1BQUYsQ0FBUzFwQixJQUFULENBQWN3RSxDQUFkLENBQXpEO0FBQTJFO0FBQUMsU0FBaFYsR0FBa1Z5YyxFQUFFL2QsSUFBRixDQUFPNmUsRUFBRXlCLFVBQUYsQ0FBYTVkLENBQWIsRUFBZXNiLEVBQUV3SSxNQUFqQixDQUFQLEVBQWdDLFVBQVM5akIsQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQ0EsWUFBRXFNLEtBQUY7QUFBVSxTQUF4RCxDQUFsVjtBQUE0WSxPQUE3YixDQUF4QyxFQUF1ZSxJQUE5ZTtBQUFtZixLQUFqN0csRUFBazdHK2Isa0NBQWlDLFVBQVN6bkIsQ0FBVCxFQUFXO0FBQUMsVUFBSVgsSUFBRSxLQUFLbWdCLGdCQUFYLENBQTRCLEtBQUtBLGdCQUFMLEdBQXNCLFlBQVU7QUFBQyxlQUFPLElBQVA7QUFBWSxPQUE3QyxDQUE4QyxJQUFJNWdCLElBQUVvQixHQUFOLENBQVUsT0FBTyxLQUFLd2YsZ0JBQUwsR0FBc0JuZ0IsQ0FBdEIsRUFBd0JULENBQS9CO0FBQWlDLEtBQXBsSCxFQUFxbEhzb0IsVUFBUyxVQUFTbG5CLENBQVQsRUFBVztBQUFDLGFBQU8sS0FBS2xELE9BQUwsQ0FBYSxVQUFRa0QsQ0FBckIsQ0FBUDtBQUErQixLQUF6b0gsRUFBWixDQUF1cEgsSUFBSW1vQixJQUFFLFVBQVNub0IsQ0FBVCxFQUFXWCxDQUFYLEVBQWFULENBQWIsRUFBZStjLENBQWYsRUFBaUJMLENBQWpCLEVBQW1CO0FBQUMsUUFBSU8sSUFBRWxrQixPQUFPa3FCLE9BQVAsQ0FBZXVHLGtCQUFmLENBQWtDekcsVUFBbEMsQ0FBNkN0aUIsQ0FBN0MsQ0FBTjtBQUFBLFFBQXNEb1osSUFBRSxJQUFJMkgsQ0FBSixDQUFNdkUsQ0FBTixDQUF4RCxDQUFpRUosRUFBRSxJQUFGLEVBQU8sRUFBQzRNLFdBQVU1UCxDQUFYLEVBQWF2YyxNQUFLbUQsQ0FBbEIsRUFBb0JtakIsY0FBYTVqQixDQUFqQyxFQUFtQ2tpQixVQUFTbkYsSUFBRUEsS0FBRzNiLEVBQUU4TSxPQUFGLENBQVV6TixJQUFFLFVBQVosQ0FBSCxJQUE0Qm9aLEVBQUVxSSxRQUE1RSxFQUFxRndILGlCQUFnQmhOLElBQUUsQ0FBQyxDQUFELEtBQUtBLENBQTVHLEVBQVAsR0FBdUgsS0FBS2lOLGtCQUFMLENBQXdCdm9CLEVBQUU4TSxPQUExQixDQUF2SDtBQUEwSixHQUFyUDtBQUFBLE1BQXNQMGIsSUFBRSxVQUFTeG9CLENBQVQsRUFBV1gsQ0FBWCxFQUFhVCxDQUFiLEVBQWUrYyxDQUFmLEVBQWlCO0FBQUMsU0FBS29GLFNBQUwsR0FBZSxPQUFmLEVBQXVCLEtBQUtwZCxPQUFMLEdBQWEzRCxDQUFwQyxFQUFzQyxLQUFLcEQsUUFBTCxHQUFjeWUsRUFBRXJiLENBQUYsQ0FBcEQsRUFBeUQsS0FBSyxDQUFMLEtBQVMyYixDQUFULEtBQWEsS0FBSy9YLE1BQUwsR0FBWStYLENBQXpCLENBQXpELEVBQXFGLEtBQUs3TyxPQUFMLEdBQWFsTyxDQUFsRyxFQUFvRyxLQUFLNmdCLFVBQUwsR0FBZ0JwZ0IsQ0FBcEgsRUFBc0gsS0FBS29wQixXQUFMLEdBQWlCLEVBQXZJLEVBQTBJLEtBQUtDLGlCQUFMLEdBQXVCLEVBQWpLLEVBQW9LLEtBQUt0SixnQkFBTCxHQUFzQixDQUFDLENBQTNMLEVBQTZMLEtBQUt1SixnQkFBTCxFQUE3TDtBQUFxTixHQUEvZDtBQUFBLE1BQWdlQyxJQUFFLEVBQUNqQyxTQUFRLElBQVQsRUFBY0MsVUFBUyxDQUFDLENBQXhCLEVBQTBCQyxVQUFTLEVBQUVzQixFQUFFL3NCLFNBQUYsR0FBWSxFQUFDaWxCLFVBQVMsVUFBU3JnQixDQUFULEVBQVdYLENBQVgsRUFBYTtBQUFDLFlBQUlULENBQUosQ0FBTSxPQUFNLENBQUNBLElBQUUsS0FBS3lwQixTQUFSLEVBQW1CaEksUUFBbkIsQ0FBNEIzZixLQUE1QixDQUFrQzlCLENBQWxDLEVBQW9DLENBQUNvQixDQUFELEVBQUk4QyxNQUFKLENBQVdtWixFQUFFLEtBQUs0TSxlQUFQLENBQVgsRUFBbUMsQ0FBQ3hwQixDQUFELENBQW5DLENBQXBDLENBQU47QUFBbUYsT0FBakgsRUFBa0hrcEIsb0JBQW1CLFVBQVMzcEIsQ0FBVCxFQUFXO0FBQUMsWUFBSStjLElBQUUsSUFBTixDQUFXLEtBQUtrTixlQUFMLEdBQXFCLEtBQUtSLFNBQUwsQ0FBZTFILGlCQUFmLENBQWlDLEtBQUs2QixZQUF0QyxFQUFtRCxVQUFTeGlCLENBQVQsRUFBVztBQUFDLGlCQUFPcEIsRUFBRStjLEVBQUV6ZixJQUFGLElBQVFtRCxJQUFFVyxDQUFGLEVBQUlYLEVBQUUsQ0FBRixFQUFLOEcsV0FBTCxLQUFtQjlHLEVBQUVaLEtBQUYsQ0FBUSxDQUFSLENBQS9CLENBQUYsQ0FBUCxDQUFxRCxJQUFJWSxDQUFKO0FBQU0sU0FBMUgsQ0FBckI7QUFBaUosT0FBN1MsRUFBZCxDQUFuQyxFQUFsZSxDQUFvMEJtcEIsRUFBRXB0QixTQUFGLEdBQVksRUFBQ2lsQixVQUFTLFVBQVNyZ0IsQ0FBVCxFQUFXO0FBQUMsV0FBR1MsVUFBVXZDLE1BQWIsSUFBcUIsQ0FBQ21kLEVBQUV3RixhQUFGLENBQWdCN2dCLENBQWhCLENBQXRCLEtBQTJDbWMsRUFBRVcsUUFBRixDQUFXLDJGQUFYLEdBQXdHOWMsSUFBRSxFQUFDOE0sU0FBUTlNLENBQVQsRUFBckosRUFBa0ssSUFBSVgsSUFBRSxLQUFLMm5CLFlBQUwsQ0FBa0JobkIsQ0FBbEIsQ0FBTixDQUEyQixJQUFHLENBQUNYLENBQUosRUFBTSxPQUFNLENBQUMsQ0FBUCxDQUFTLFFBQU9BLEVBQUU0bkIsS0FBRixFQUFQLEdBQWtCLEtBQUksU0FBSjtBQUFjLGlCQUFPLElBQVAsQ0FBWSxLQUFJLFVBQUo7QUFBZSxpQkFBTSxDQUFDLENBQVAsQ0FBUyxLQUFJLFVBQUo7QUFBZSxpQkFBTyxLQUFLN0gsZ0JBQVosQ0FBbkY7QUFBaUgsS0FBblYsRUFBb1Y0SCxjQUFhLFlBQVU7QUFBQyxVQUFJaG5CLENBQUo7QUFBQSxVQUFNWCxJQUFFLElBQVI7QUFBQSxVQUFhVCxJQUFFLElBQUU2QixVQUFVdkMsTUFBWixJQUFvQixLQUFLLENBQUwsS0FBU3VDLFVBQVUsQ0FBVixDQUE3QixHQUEwQ0EsVUFBVSxDQUFWLENBQTFDLEdBQXVELEVBQXRFO0FBQUEsVUFBeUVrYixJQUFFL2MsRUFBRXNoQixLQUE3RTtBQUFBLFVBQW1GNUUsSUFBRTFjLEVBQUUwZixLQUF2RixDQUE2RixJQUFHLEtBQUt3SixPQUFMLElBQWUsQ0FBQ3hNLENBQUQsSUFBSSxLQUFLd04sVUFBTCxDQUFnQnhOLENBQWhCLENBQXRCLEVBQXlDLE9BQU8sS0FBS2xpQixLQUFMLEdBQVcsS0FBS210QixRQUFMLEVBQVgsRUFBMkIsS0FBS1csUUFBTCxDQUFjLFVBQWQsQ0FBM0IsRUFBcUQsQ0FBQ2xuQixJQUFFLEtBQUtpZ0IsU0FBTCxDQUFlLEVBQUNDLE9BQU12RSxDQUFQLEVBQVN2aUIsT0FBTSxLQUFLQSxLQUFwQixFQUEwQjJ2QixZQUFXLENBQUMsQ0FBdEMsRUFBZixFQUF5RHBCLE1BQXpELENBQWdFLFlBQVU7QUFBQ3RvQixVQUFFNmtCLFNBQUY7QUFBYyxPQUF6RixFQUEyRm5JLElBQTNGLENBQWdHLFlBQVU7QUFBQzFjLFVBQUU2bkIsUUFBRixDQUFXLFNBQVg7QUFBc0IsT0FBakksRUFBbUlRLElBQW5JLENBQXdJLFlBQVU7QUFBQ3JvQixVQUFFNm5CLFFBQUYsQ0FBVyxPQUFYO0FBQW9CLE9BQXZLLEVBQXlLUyxNQUF6SyxDQUFnTCxZQUFVO0FBQUN0b0IsVUFBRTZuQixRQUFGLENBQVcsV0FBWDtBQUF3QixPQUFuTixDQUFILEVBQXlOVSxJQUF6TixDQUE4TmxuQixLQUE5TixDQUFvT1YsQ0FBcE8sRUFBc09pYyxFQUFFLEtBQUtpRCxnQ0FBTCxFQUFGLENBQXRPLENBQTVEO0FBQThVLEtBQWgwQixFQUFpMEJ1RyxnQkFBZSxZQUFVO0FBQUMsYUFBTyxNQUFJLEtBQUtnRCxXQUFMLENBQWlCdnFCLE1BQTVCO0FBQW1DLEtBQTkzQixFQUErM0J3bkIsaUJBQWdCLFVBQVMxbEIsQ0FBVCxFQUFXO0FBQUMsYUFBTyxLQUFLLENBQUwsS0FBU0EsQ0FBVCxLQUFhQSxJQUFFLEtBQUt1bUIsUUFBTCxFQUFmLEdBQWdDLEVBQUUsQ0FBQ3ZtQixFQUFFOUIsTUFBSCxJQUFXLENBQUMsS0FBSzhxQixXQUFMLEVBQVosSUFBZ0MsS0FBSyxDQUFMLEtBQVMsS0FBS2xjLE9BQUwsQ0FBYW1jLGVBQXhELENBQXZDO0FBQWdILEtBQTNnQyxFQUE0Z0NILFlBQVcsVUFBUzlvQixDQUFULEVBQVc7QUFBQyxhQUFPN0UsTUFBTW9QLE9BQU4sQ0FBYyxLQUFLdUMsT0FBTCxDQUFhd1IsS0FBM0IsSUFBa0MsQ0FBQyxDQUFELEtBQUtqRCxFQUFFNk4sT0FBRixDQUFVbHBCLENBQVYsRUFBWSxLQUFLOE0sT0FBTCxDQUFhd1IsS0FBekIsQ0FBdkMsR0FBdUUsS0FBS3hSLE9BQUwsQ0FBYXdSLEtBQWIsS0FBcUJ0ZSxDQUFuRztBQUFxRyxLQUF4b0MsRUFBeW9DNm5CLFNBQVEsVUFBUzduQixDQUFULEVBQVc7QUFBQyxVQUFHLEtBQUdTLFVBQVV2QyxNQUFiLElBQXFCLENBQUNtZCxFQUFFd0YsYUFBRixDQUFnQjdnQixDQUFoQixDQUF6QixFQUE0QztBQUFDbWMsVUFBRVcsUUFBRixDQUFXLDBGQUFYLEVBQXVHLElBQUl6ZCxJQUFFbEUsTUFBTUMsU0FBTixDQUFnQnFELEtBQWhCLENBQXNCeUMsSUFBdEIsQ0FBMkJULFNBQTNCLENBQU4sQ0FBNENULElBQUUsRUFBQ2tnQixPQUFNN2dCLEVBQUUsQ0FBRixDQUFQLEVBQVlqRyxPQUFNaUcsRUFBRSxDQUFGLENBQWxCLEVBQUY7QUFBMEIsV0FBSVQsSUFBRSxLQUFLcWhCLFNBQUwsQ0FBZWpnQixDQUFmLENBQU4sQ0FBd0IsT0FBTSxDQUFDcEIsQ0FBRCxJQUFJZ3FCLEVBQUVocUIsRUFBRXFvQixLQUFGLEVBQUYsQ0FBVjtBQUF1QixLQUF0NkMsRUFBdTZDaEgsV0FBVSxZQUFVO0FBQUMsVUFBSXRFLElBQUUsSUFBTjtBQUFBLFVBQVczYixJQUFFLElBQUVTLFVBQVV2QyxNQUFaLElBQW9CLEtBQUssQ0FBTCxLQUFTdUMsVUFBVSxDQUFWLENBQTdCLEdBQTBDQSxVQUFVLENBQVYsQ0FBMUMsR0FBdUQsRUFBcEU7QUFBQSxVQUF1RXBCLElBQUVXLEVBQUVrZ0IsS0FBM0U7QUFBQSxVQUFpRnRoQixJQUFFLEtBQUssQ0FBTCxLQUFTUyxDQUFULElBQVlBLENBQS9GO0FBQUEsVUFBaUdpYyxJQUFFdGIsRUFBRTVHLEtBQXJHO0FBQUEsVUFBMkd5aUIsSUFBRTdiLEVBQUVzZSxLQUEvRyxDQUFxSCxJQUFHdGUsRUFBRStvQixVQUFGLElBQWMsS0FBS2pCLE9BQUwsRUFBZCxFQUE2QixDQUFDak0sQ0FBRCxJQUFJLEtBQUtpTixVQUFMLENBQWdCak4sQ0FBaEIsQ0FBcEMsRUFBdUQ7QUFBQyxZQUFHLEtBQUt1RCxnQkFBTCxHQUFzQixDQUFDLENBQXZCLEVBQXlCLENBQUMsS0FBS3FHLGNBQUwsRUFBN0IsRUFBbUQsT0FBT3BLLEVBQUV5QyxJQUFGLEVBQVAsQ0FBZ0IsSUFBRyxRQUFNeEMsQ0FBTixLQUFVQSxJQUFFLEtBQUtpTCxRQUFMLEVBQVosR0FBNkIsQ0FBQyxLQUFLYixlQUFMLENBQXFCcEssQ0FBckIsQ0FBRCxJQUEwQixDQUFDLENBQUQsS0FBSzFjLENBQS9ELEVBQWlFLE9BQU95YyxFQUFFeUMsSUFBRixFQUFQLENBQWdCLElBQUlyRixJQUFFLEtBQUswUSxzQkFBTCxFQUFOO0FBQUEsWUFBb0MxTixJQUFFLEVBQXRDLENBQXlDLE9BQU9KLEVBQUUvZCxJQUFGLENBQU9tYixDQUFQLEVBQVMsVUFBU3pZLENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsY0FBSVQsSUFBRXVkLEVBQUUwQixHQUFGLENBQU14QyxFQUFFOWIsR0FBRixDQUFNRixDQUFOLEVBQVEsVUFBU1csQ0FBVCxFQUFXO0FBQUMsbUJBQU8yYixFQUFFeU4sbUJBQUYsQ0FBc0I5TixDQUF0QixFQUF3QnRiLENBQXhCLENBQVA7QUFBa0MsV0FBdEQsQ0FBTixDQUFOLENBQXFFLElBQUd5YixFQUFFcmhCLElBQUYsQ0FBT3dFLENBQVAsR0FBVSxlQUFhQSxFQUFFcW9CLEtBQUYsRUFBMUIsRUFBb0MsT0FBTSxDQUFDLENBQVA7QUFBUyxTQUF6SSxHQUEySTlLLEVBQUUwQixHQUFGLENBQU1wQyxDQUFOLENBQWxKO0FBQTJKO0FBQUMsS0FBbDhELEVBQW04RDJOLHFCQUFvQixVQUFTcHBCLENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsVUFBSVQsSUFBRSxJQUFOO0FBQUEsVUFBVytjLElBQUV0YyxFQUFFZ2hCLFFBQUYsQ0FBV3JnQixDQUFYLEVBQWEsSUFBYixDQUFiLENBQWdDLE9BQU0sQ0FBQyxDQUFELEtBQUsyYixDQUFMLEtBQVNBLElBQUVOLEVBQUU4RCxRQUFGLEdBQWFFLE1BQWIsRUFBWCxHQUFrQ2xELEVBQUUwQixHQUFGLENBQU0sQ0FBQ2xDLENBQUQsQ0FBTixFQUFXK0wsSUFBWCxDQUFnQixVQUFTMW5CLENBQVQsRUFBVztBQUFDcEIsVUFBRXdnQixnQkFBRixZQUE4QmprQixLQUE5QixLQUFzQ3lELEVBQUV3Z0IsZ0JBQUYsR0FBbUIsRUFBekQsR0FBNkR4Z0IsRUFBRXdnQixnQkFBRixDQUFtQmhsQixJQUFuQixDQUF3QixFQUFDaXFCLFFBQU9obEIsQ0FBUixFQUFVeWxCLGNBQWEsWUFBVSxPQUFPOWtCLENBQWpCLElBQW9CQSxDQUEzQyxFQUF4QixDQUE3RDtBQUFvSSxPQUFoSyxDQUF4QztBQUEwTSxLQUEvc0UsRUFBZ3RFdW1CLFVBQVMsWUFBVTtBQUFDLFVBQUl2bUIsQ0FBSixDQUFNLE9BQU8sU0FBT0EsSUFBRSxjQUFZLE9BQU8sS0FBSzhNLE9BQUwsQ0FBYTFULEtBQWhDLEdBQXNDLEtBQUswVCxPQUFMLENBQWExVCxLQUFiLENBQW1CLElBQW5CLENBQXRDLEdBQStELEtBQUssQ0FBTCxLQUFTLEtBQUswVCxPQUFMLENBQWExVCxLQUF0QixHQUE0QixLQUFLMFQsT0FBTCxDQUFhMVQsS0FBekMsR0FBK0MsS0FBS3dELFFBQUwsQ0FBY3lOLEdBQWQsRUFBdkgsSUFBNEksRUFBNUksR0FBK0ksS0FBS2dmLGlCQUFMLENBQXVCcnBCLENBQXZCLENBQXRKO0FBQWdMLEtBQTE1RSxFQUEyNUUwTCxPQUFNLFlBQVU7QUFBQyxhQUFPLEtBQUsrYSxRQUFMLElBQWdCLEtBQUtTLFFBQUwsQ0FBYyxPQUFkLENBQXZCO0FBQThDLEtBQTE5RSxFQUEyOUVhLFNBQVEsWUFBVTtBQUFDLFdBQUsvRCxVQUFMLElBQWtCLEtBQUtwbkIsUUFBTCxDQUFjSyxVQUFkLENBQXlCLFNBQXpCLENBQWxCLEVBQXNELEtBQUtMLFFBQUwsQ0FBY0ssVUFBZCxDQUF5QixlQUF6QixDQUF0RCxFQUFnRyxLQUFLaXFCLFFBQUwsQ0FBYyxTQUFkLENBQWhHO0FBQXlILEtBQXZtRixFQUF3bUZZLFNBQVEsWUFBVTtBQUFDLGFBQU8sS0FBS3dCLG1CQUFMLElBQTJCLElBQWxDO0FBQXVDLEtBQWxxRixFQUFtcUZBLHFCQUFvQixZQUFVO0FBQUMsYUFBTyxLQUFLOUosZ0JBQUwsR0FBd0JtSixnQkFBeEIsRUFBUDtBQUFrRCxLQUFwdkYsRUFBcXZGWSxvQkFBbUIsWUFBVTtBQUFDLGFBQU9wTixFQUFFVyxRQUFGLENBQVcsZ0VBQVgsR0FBNkUsS0FBS2dMLE9BQUwsRUFBcEY7QUFBbUcsS0FBdDNGLEVBQXUzRjBCLGVBQWMsVUFBU3hwQixDQUFULEVBQVdYLENBQVgsRUFBYVQsQ0FBYixFQUFlK2MsQ0FBZixFQUFpQjtBQUFDLFVBQUdoa0IsT0FBT2txQixPQUFQLENBQWV1RyxrQkFBZixDQUFrQ3pHLFVBQWxDLENBQTZDM2hCLENBQTdDLENBQUgsRUFBbUQ7QUFBQyxZQUFJc2IsSUFBRSxJQUFJNk0sQ0FBSixDQUFNLElBQU4sRUFBV25vQixDQUFYLEVBQWFYLENBQWIsRUFBZVQsQ0FBZixFQUFpQitjLENBQWpCLENBQU4sQ0FBMEIsZ0JBQWMsS0FBSytNLGlCQUFMLENBQXVCcE4sRUFBRXBmLElBQXpCLENBQWQsSUFBOEMsS0FBS3V0QixnQkFBTCxDQUFzQm5PLEVBQUVwZixJQUF4QixDQUE5QyxFQUE0RSxLQUFLdXNCLFdBQUwsQ0FBaUJydUIsSUFBakIsQ0FBc0JraEIsQ0FBdEIsQ0FBNUUsRUFBcUcsS0FBS29OLGlCQUFMLENBQXVCcE4sRUFBRXBmLElBQXpCLElBQStCb2YsQ0FBcEk7QUFBc0ksY0FBTyxJQUFQO0FBQVksS0FBdm5HLEVBQXduR21PLGtCQUFpQixVQUFTenBCLENBQVQsRUFBVztBQUFDLFdBQUksSUFBSVgsSUFBRSxDQUFWLEVBQVlBLElBQUUsS0FBS29wQixXQUFMLENBQWlCdnFCLE1BQS9CLEVBQXNDbUIsR0FBdEM7QUFBMEMsWUFBR1csTUFBSSxLQUFLeW9CLFdBQUwsQ0FBaUJwcEIsQ0FBakIsRUFBb0JuRCxJQUEzQixFQUFnQztBQUFDLGVBQUt1c0IsV0FBTCxDQUFpQi90QixNQUFqQixDQUF3QjJFLENBQXhCLEVBQTBCLENBQTFCLEVBQTZCO0FBQU07QUFBOUcsT0FBOEcsT0FBTyxPQUFPLEtBQUtxcEIsaUJBQUwsQ0FBdUIxb0IsQ0FBdkIsQ0FBUCxFQUFpQyxJQUF4QztBQUE2QyxLQUFoekcsRUFBaXpHMHBCLGtCQUFpQixVQUFTMXBCLENBQVQsRUFBV1gsQ0FBWCxFQUFhVCxDQUFiLEVBQWU7QUFBQyxhQUFPLEtBQUs2cUIsZ0JBQUwsQ0FBc0J6cEIsQ0FBdEIsRUFBeUJ3cEIsYUFBekIsQ0FBdUN4cEIsQ0FBdkMsRUFBeUNYLENBQXpDLEVBQTJDVCxDQUEzQyxDQUFQO0FBQXFELEtBQXY0RyxFQUF3NEcrcEIsa0JBQWlCLFlBQVU7QUFBQyxXQUFJLElBQUkzb0IsSUFBRSxFQUFOLEVBQVNYLElBQUUsRUFBWCxFQUFjVCxJQUFFLENBQXBCLEVBQXNCQSxJQUFFLEtBQUs2cEIsV0FBTCxDQUFpQnZxQixNQUF6QyxFQUFnRFUsR0FBaEQ7QUFBb0QsU0FBQyxDQUFELEtBQUssS0FBSzZwQixXQUFMLENBQWlCN3BCLENBQWpCLEVBQW9CMHBCLGVBQXpCLEtBQTJDdG9CLEVBQUU1RixJQUFGLENBQU8sS0FBS3F1QixXQUFMLENBQWlCN3BCLENBQWpCLENBQVAsR0FBNEJTLEVBQUUsS0FBS29wQixXQUFMLENBQWlCN3BCLENBQWpCLEVBQW9CMUMsSUFBdEIsSUFBNEIsS0FBS3VzQixXQUFMLENBQWlCN3BCLENBQWpCLENBQW5HO0FBQXBELE9BQTRLLEtBQUksSUFBSStjLENBQVIsSUFBYSxLQUFLOE0sV0FBTCxHQUFpQnpvQixDQUFqQixFQUFtQixLQUFLMG9CLGlCQUFMLEdBQXVCcnBCLENBQTFDLEVBQTRDLEtBQUt5TixPQUE5RDtBQUFzRSxhQUFLMGMsYUFBTCxDQUFtQjdOLENBQW5CLEVBQXFCLEtBQUs3TyxPQUFMLENBQWE2TyxDQUFiLENBQXJCLEVBQXFDLEtBQUssQ0FBMUMsRUFBNEMsQ0FBQyxDQUE3QztBQUF0RSxPQUFzSCxPQUFPLEtBQUtnTyxxQkFBTCxFQUFQO0FBQW9DLEtBQTF1SCxFQUEydUhBLHVCQUFzQixZQUFVO0FBQUMsZUFBTyxLQUFLaG1CLE9BQUwsQ0FBYTlKLFlBQWIsQ0FBMEIsVUFBMUIsQ0FBUCxJQUE4QyxLQUFLMnZCLGFBQUwsQ0FBbUIsVUFBbkIsRUFBOEIsQ0FBQyxDQUEvQixFQUFpQyxLQUFLLENBQXRDLEVBQXdDLENBQUMsQ0FBekMsQ0FBOUMsRUFBMEYsU0FBTyxLQUFLN2xCLE9BQUwsQ0FBYTlKLFlBQWIsQ0FBMEIsU0FBMUIsQ0FBUCxJQUE2QyxLQUFLMnZCLGFBQUwsQ0FBbUIsU0FBbkIsRUFBNkIsS0FBSzdsQixPQUFMLENBQWE5SixZQUFiLENBQTBCLFNBQTFCLENBQTdCLEVBQWtFLEtBQUssQ0FBdkUsRUFBeUUsQ0FBQyxDQUExRSxDQUF2SSxDQUFvTixJQUFJbUcsSUFBRSxLQUFLMkQsT0FBTCxDQUFhOUosWUFBYixDQUEwQixLQUExQixDQUFOO0FBQUEsVUFBdUN3RixJQUFFLEtBQUtzRSxPQUFMLENBQWE5SixZQUFiLENBQTBCLEtBQTFCLENBQXpDLENBQTBFLFNBQU9tRyxDQUFQLElBQVUsU0FBT1gsQ0FBakIsR0FBbUIsS0FBS21xQixhQUFMLENBQW1CLE9BQW5CLEVBQTJCLENBQUN4cEIsQ0FBRCxFQUFHWCxDQUFILENBQTNCLEVBQWlDLEtBQUssQ0FBdEMsRUFBd0MsQ0FBQyxDQUF6QyxDQUFuQixHQUErRCxTQUFPVyxDQUFQLEdBQVMsS0FBS3dwQixhQUFMLENBQW1CLEtBQW5CLEVBQXlCeHBCLENBQXpCLEVBQTJCLEtBQUssQ0FBaEMsRUFBa0MsQ0FBQyxDQUFuQyxDQUFULEdBQStDLFNBQU9YLENBQVAsSUFBVSxLQUFLbXFCLGFBQUwsQ0FBbUIsS0FBbkIsRUFBeUJucUIsQ0FBekIsRUFBMkIsS0FBSyxDQUFoQyxFQUFrQyxDQUFDLENBQW5DLENBQXhILEVBQThKLFNBQU8sS0FBS3NFLE9BQUwsQ0FBYTlKLFlBQWIsQ0FBMEIsV0FBMUIsQ0FBUCxJQUErQyxTQUFPLEtBQUs4SixPQUFMLENBQWE5SixZQUFiLENBQTBCLFdBQTFCLENBQXRELEdBQTZGLEtBQUsydkIsYUFBTCxDQUFtQixRQUFuQixFQUE0QixDQUFDLEtBQUs3bEIsT0FBTCxDQUFhOUosWUFBYixDQUEwQixXQUExQixDQUFELEVBQXdDLEtBQUs4SixPQUFMLENBQWE5SixZQUFiLENBQTBCLFdBQTFCLENBQXhDLENBQTVCLEVBQTRHLEtBQUssQ0FBakgsRUFBbUgsQ0FBQyxDQUFwSCxDQUE3RixHQUFvTixTQUFPLEtBQUs4SixPQUFMLENBQWE5SixZQUFiLENBQTBCLFdBQTFCLENBQVAsR0FBOEMsS0FBSzJ2QixhQUFMLENBQW1CLFdBQW5CLEVBQStCLEtBQUs3bEIsT0FBTCxDQUFhOUosWUFBYixDQUEwQixXQUExQixDQUEvQixFQUFzRSxLQUFLLENBQTNFLEVBQTZFLENBQUMsQ0FBOUUsQ0FBOUMsR0FBK0gsU0FBTyxLQUFLOEosT0FBTCxDQUFhOUosWUFBYixDQUEwQixXQUExQixDQUFQLElBQStDLEtBQUsydkIsYUFBTCxDQUFtQixXQUFuQixFQUErQixLQUFLN2xCLE9BQUwsQ0FBYTlKLFlBQWIsQ0FBMEIsV0FBMUIsQ0FBL0IsRUFBc0UsS0FBSyxDQUEzRSxFQUE2RSxDQUFDLENBQTlFLENBQWhpQixDQUFpbkIsSUFBSStFLElBQUV1ZCxFQUFFTyxPQUFGLENBQVUsS0FBSy9ZLE9BQWYsQ0FBTixDQUE4QixPQUFNLGFBQVcvRSxDQUFYLEdBQWEsS0FBSzRxQixhQUFMLENBQW1CLE1BQW5CLEVBQTBCLENBQUMsUUFBRCxFQUFVLEVBQUMxRyxNQUFLLEtBQUtuZixPQUFMLENBQWE5SixZQUFiLENBQTBCLE1BQTFCLEtBQW1DLEdBQXpDLEVBQTZDa3BCLE1BQUsvaUIsS0FBRyxLQUFLMkQsT0FBTCxDQUFhOUosWUFBYixDQUEwQixPQUExQixDQUFyRCxFQUFWLENBQTFCLEVBQThILEtBQUssQ0FBbkksRUFBcUksQ0FBQyxDQUF0SSxDQUFiLEdBQXNKLDRCQUE0QmlJLElBQTVCLENBQWlDbEQsQ0FBakMsSUFBb0MsS0FBSzRxQixhQUFMLENBQW1CLE1BQW5CLEVBQTBCNXFCLENBQTFCLEVBQTRCLEtBQUssQ0FBakMsRUFBbUMsQ0FBQyxDQUFwQyxDQUFwQyxHQUEyRSxJQUF2TztBQUE0TyxLQUFyNkosRUFBczZKb3FCLGFBQVksWUFBVTtBQUFDLGFBQU8sS0FBSyxDQUFMLEtBQVMsS0FBS04saUJBQUwsQ0FBdUI3RixRQUFoQyxJQUEwQyxDQUFDLENBQUQsS0FBSyxLQUFLNkYsaUJBQUwsQ0FBdUI3RixRQUF2QixDQUFnQ0wsWUFBdEY7QUFBbUcsS0FBaGlLLEVBQWlpSzBFLFVBQVMsVUFBU2xuQixDQUFULEVBQVc7QUFBQyxhQUFPLEtBQUtsRCxPQUFMLENBQWEsV0FBU2tELENBQXRCLENBQVA7QUFBZ0MsS0FBdGxLLEVBQXVsS3FwQixtQkFBa0IsVUFBU3JwQixDQUFULEVBQVc7QUFBQyxhQUFNLENBQUMsQ0FBRCxLQUFLLEtBQUs4TSxPQUFMLENBQWE4YyxTQUFsQixJQUE2QnpOLEVBQUVXLFFBQUYsQ0FBVyx5RkFBWCxDQUE3QixFQUFtSSxhQUFXLEtBQUtoUSxPQUFMLENBQWErYyxVQUF4QixLQUFxQzdwQixJQUFFQSxFQUFFcUQsT0FBRixDQUFVLFNBQVYsRUFBb0IsR0FBcEIsQ0FBdkMsQ0FBbkksRUFBb00sV0FBUyxLQUFLeUosT0FBTCxDQUFhK2MsVUFBdEIsSUFBa0MsYUFBVyxLQUFLL2MsT0FBTCxDQUFhK2MsVUFBMUQsSUFBc0UsQ0FBQyxDQUFELEtBQUssS0FBSy9jLE9BQUwsQ0FBYThjLFNBQXhGLEtBQW9HNXBCLElBQUVtYyxFQUFFYSxVQUFGLENBQWFoZCxDQUFiLENBQXRHLENBQXBNLEVBQTJUQSxDQUFqVTtBQUFtVSxLQUF4N0ssRUFBeTdLd2dCLGNBQWEsWUFBVTtBQUFDLFVBQUl4Z0IsSUFBRSxLQUFLMG9CLGlCQUFMLENBQXVCcnZCLElBQTdCLENBQWtDLE9BQU8yRyxLQUFHLFdBQVNBLEVBQUV3aUIsWUFBckI7QUFBa0MsS0FBcmhMLEVBQXNoTDJHLHdCQUF1QixZQUFVO0FBQUMsVUFBRyxDQUFDLENBQUQsS0FBSyxLQUFLcmMsT0FBTCxDQUFhc1IsZUFBckIsRUFBcUMsT0FBTSxDQUFDLEtBQUtxSyxXQUFOLENBQU4sQ0FBeUIsS0FBSSxJQUFJem9CLElBQUUsRUFBTixFQUFTWCxJQUFFLEVBQVgsRUFBY1QsSUFBRSxDQUFwQixFQUFzQkEsSUFBRSxLQUFLNnBCLFdBQUwsQ0FBaUJ2cUIsTUFBekMsRUFBZ0RVLEdBQWhELEVBQW9EO0FBQUMsWUFBSStjLElBQUUsS0FBSzhNLFdBQUwsQ0FBaUI3cEIsQ0FBakIsRUFBb0JraUIsUUFBMUIsQ0FBbUN6aEIsRUFBRXNjLENBQUYsS0FBTTNiLEVBQUU1RixJQUFGLENBQU9pRixFQUFFc2MsQ0FBRixJQUFLLEVBQVosQ0FBTixFQUFzQnRjLEVBQUVzYyxDQUFGLEVBQUt2aEIsSUFBTCxDQUFVLEtBQUtxdUIsV0FBTCxDQUFpQjdwQixDQUFqQixDQUFWLENBQXRCO0FBQXFELGNBQU9vQixFQUFFOHBCLElBQUYsQ0FBTyxVQUFTOXBCLENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsZUFBT0EsRUFBRSxDQUFGLEVBQUt5aEIsUUFBTCxHQUFjOWdCLEVBQUUsQ0FBRixFQUFLOGdCLFFBQTFCO0FBQW1DLE9BQXhELEdBQTBEOWdCLENBQWpFO0FBQW1FLEtBQXQwTCxFQUFaLENBQW8xTCxJQUFJK3BCLElBQUUsWUFBVTtBQUFDLFNBQUtoSixTQUFMLEdBQWUsZUFBZjtBQUErQixHQUFoRCxDQUFpRGdKLEVBQUUzdUIsU0FBRixHQUFZLEVBQUM0dUIsWUFBVyxVQUFTaHFCLENBQVQsRUFBVztBQUFDLGFBQU8sS0FBS2lxQixTQUFMLENBQWU3dkIsSUFBZixDQUFvQjRGLENBQXBCLEdBQXVCLElBQTlCO0FBQW1DLEtBQTNELEVBQTREc3BCLHFCQUFvQixZQUFVO0FBQUMsVUFBSXRwQixDQUFKLENBQU0sSUFBRyxLQUFLeW9CLFdBQUwsR0FBaUIsRUFBakIsRUFBb0IsYUFBVyxLQUFLOWtCLE9BQUwsQ0FBYWpLLFFBQS9DLEVBQXdELE9BQU8sS0FBSzhsQixnQkFBTCxHQUF3Qm1KLGdCQUF4QixJQUEyQyxJQUFsRCxDQUF1RCxLQUFJLElBQUl0cEIsSUFBRSxDQUFWLEVBQVlBLElBQUUsS0FBSzRxQixTQUFMLENBQWUvckIsTUFBN0IsRUFBb0NtQixHQUFwQztBQUF3QyxZQUFHZ2MsRUFBRSxNQUFGLEVBQVU2TyxHQUFWLENBQWMsS0FBS0QsU0FBTCxDQUFlNXFCLENBQWYsQ0FBZCxFQUFpQ25CLE1BQXBDLEVBQTJDO0FBQUM4QixjQUFFLEtBQUtpcUIsU0FBTCxDQUFlNXFCLENBQWYsRUFBa0J4QyxJQUFsQixDQUF1QixlQUF2QixFQUF3Q3lzQixtQkFBeEMsR0FBOERiLFdBQWhFLENBQTRFLEtBQUksSUFBSTdwQixJQUFFLENBQVYsRUFBWUEsSUFBRW9CLEVBQUU5QixNQUFoQixFQUF1QlUsR0FBdkI7QUFBMkIsaUJBQUs0cUIsYUFBTCxDQUFtQnhwQixFQUFFcEIsQ0FBRixFQUFLMUMsSUFBeEIsRUFBNkI4RCxFQUFFcEIsQ0FBRixFQUFLNGpCLFlBQWxDLEVBQStDeGlCLEVBQUVwQixDQUFGLEVBQUtraUIsUUFBcEQsRUFBNkQ5Z0IsRUFBRXBCLENBQUYsRUFBSzBwQixlQUFsRTtBQUEzQjtBQUE4RyxTQUF0TyxNQUEyTyxLQUFLMkIsU0FBTCxDQUFldnZCLE1BQWYsQ0FBc0IyRSxDQUF0QixFQUF3QixDQUF4QjtBQUFuUixPQUE4UyxPQUFPLElBQVA7QUFBWSxLQUExZ0IsRUFBMmdCa25CLFVBQVMsWUFBVTtBQUFDLFVBQUcsY0FBWSxPQUFPLEtBQUt6WixPQUFMLENBQWExVCxLQUFuQyxFQUF5QyxPQUFPLEtBQUswVCxPQUFMLENBQWExVCxLQUFiLENBQW1CLElBQW5CLENBQVAsQ0FBZ0MsSUFBRyxLQUFLLENBQUwsS0FBUyxLQUFLMFQsT0FBTCxDQUFhMVQsS0FBekIsRUFBK0IsT0FBTyxLQUFLMFQsT0FBTCxDQUFhMVQsS0FBcEIsQ0FBMEIsSUFBRyxZQUFVLEtBQUt1SyxPQUFMLENBQWFqSyxRQUExQixFQUFtQztBQUFDLFlBQUlzRyxJQUFFbWMsRUFBRU8sT0FBRixDQUFVLEtBQUsvWSxPQUFmLENBQU4sQ0FBOEIsSUFBRyxZQUFVM0QsQ0FBYixFQUFlLE9BQU8sS0FBS21nQixZQUFMLEdBQW9CaFosTUFBcEIsQ0FBMkIsVUFBM0IsRUFBdUNrRCxHQUF2QyxNQUE4QyxFQUFyRCxDQUF3RCxJQUFHLGVBQWFySyxDQUFoQixFQUFrQjtBQUFDLGNBQUlYLElBQUUsRUFBTixDQUFTLE9BQU8sS0FBSzhnQixZQUFMLEdBQW9CaFosTUFBcEIsQ0FBMkIsVUFBM0IsRUFBdUM3SixJQUF2QyxDQUE0QyxZQUFVO0FBQUMrQixjQUFFakYsSUFBRixDQUFPaWhCLEVBQUUsSUFBRixFQUFRaFIsR0FBUixFQUFQO0FBQXNCLFdBQTdFLEdBQStFaEwsQ0FBdEY7QUFBd0Y7QUFBQyxjQUFNLGFBQVcsS0FBS3NFLE9BQUwsQ0FBYWpLLFFBQXhCLElBQWtDLFNBQU8sS0FBS2tELFFBQUwsQ0FBY3lOLEdBQWQsRUFBekMsR0FBNkQsRUFBN0QsR0FBZ0UsS0FBS3pOLFFBQUwsQ0FBY3lOLEdBQWQsRUFBdEU7QUFBMEYsS0FBei9CLEVBQTAvQjlNLE9BQU0sWUFBVTtBQUFDLGFBQU8sS0FBSzBzQixTQUFMLEdBQWUsQ0FBQyxLQUFLcnRCLFFBQU4sQ0FBZixFQUErQixJQUF0QztBQUEyQyxLQUF0akMsRUFBWixDQUFva0MsSUFBSXV0QixJQUFFLFVBQVNucUIsQ0FBVCxFQUFXWCxDQUFYLEVBQWFULENBQWIsRUFBZTtBQUFDLFNBQUsrRSxPQUFMLEdBQWEzRCxDQUFiLEVBQWUsS0FBS3BELFFBQUwsR0FBY3llLEVBQUVyYixDQUFGLENBQTdCLENBQWtDLElBQUkyYixJQUFFLEtBQUsvZSxRQUFMLENBQWNDLElBQWQsQ0FBbUIsU0FBbkIsQ0FBTixDQUFvQyxJQUFHOGUsQ0FBSCxFQUFLLE9BQU8sS0FBSyxDQUFMLEtBQVMvYyxDQUFULElBQVkrYyxFQUFFL1gsTUFBRixLQUFXak0sT0FBT2txQixPQUE5QixLQUF3Q2xHLEVBQUUvWCxNQUFGLEdBQVNoRixDQUFULEVBQVcrYyxFQUFFK0QsYUFBRixDQUFnQi9ELEVBQUU3TyxPQUFsQixDQUFuRCxHQUErRSxhQUFXd08sRUFBRWpjLENBQUYsQ0FBWCxJQUFpQm9jLEVBQUVFLEVBQUU3TyxPQUFKLEVBQVl6TixDQUFaLENBQWhHLEVBQStHc2MsQ0FBdEgsQ0FBd0gsSUFBRyxDQUFDLEtBQUsvZSxRQUFMLENBQWNzQixNQUFsQixFQUF5QixNQUFNLElBQUl3RyxLQUFKLENBQVUsK0NBQVYsQ0FBTixDQUFpRSxJQUFHLEtBQUssQ0FBTCxLQUFTOUYsQ0FBVCxJQUFZLFdBQVNBLEVBQUVtaUIsU0FBMUIsRUFBb0MsTUFBTSxJQUFJcmMsS0FBSixDQUFVLHlDQUFWLENBQU4sQ0FBMkQsT0FBTyxLQUFLZCxNQUFMLEdBQVloRixLQUFHakgsT0FBT2txQixPQUF0QixFQUE4QixLQUFLMVMsSUFBTCxDQUFVOVAsQ0FBVixDQUFyQztBQUFrRCxHQUFwYyxDQUFxYzhxQixFQUFFL3VCLFNBQUYsR0FBWSxFQUFDK1QsTUFBSyxVQUFTblAsQ0FBVCxFQUFXO0FBQUMsYUFBTyxLQUFLK2dCLFNBQUwsR0FBZSxTQUFmLEVBQXlCLEtBQUtxSixXQUFMLEdBQWlCLE9BQTFDLEVBQWtELEtBQUtwTCxNQUFMLEdBQVk3QyxFQUFFUSxVQUFGLEVBQTlELEVBQTZFLEtBQUsrQyxhQUFMLENBQW1CMWYsQ0FBbkIsQ0FBN0UsRUFBbUcsV0FBUyxLQUFLMkQsT0FBTCxDQUFhakssUUFBdEIsSUFBZ0N5aUIsRUFBRUksU0FBRixDQUFZLEtBQUs1WSxPQUFqQixFQUF5QixLQUFLbUosT0FBTCxDQUFhM08sU0FBdEMsRUFBZ0QsVUFBaEQsS0FBNkQsQ0FBQyxLQUFLdkIsUUFBTCxDQUFjd0ssRUFBZCxDQUFpQixLQUFLMEYsT0FBTCxDQUFhb1IsTUFBOUIsQ0FBOUYsR0FBb0ksS0FBSzFiLElBQUwsQ0FBVSxhQUFWLENBQXBJLEdBQTZKLEtBQUs2bkIsVUFBTCxLQUFrQixLQUFLQyxjQUFMLEVBQWxCLEdBQXdDLEtBQUs5bkIsSUFBTCxDQUFVLGNBQVYsQ0FBL1M7QUFBeVUsS0FBM1YsRUFBNFY2bkIsWUFBVyxZQUFVO0FBQUMsVUFBSXJxQixJQUFFbWMsRUFBRU8sT0FBRixDQUFVLEtBQUsvWSxPQUFmLENBQU4sQ0FBOEIsT0FBTSxZQUFVM0QsQ0FBVixJQUFhLGVBQWFBLENBQTFCLElBQTZCLGFBQVcsS0FBSzJELE9BQUwsQ0FBYWpLLFFBQXhCLElBQWtDLFNBQU8sS0FBS2lLLE9BQUwsQ0FBYTlKLFlBQWIsQ0FBMEIsVUFBMUIsQ0FBNUU7QUFBa0gsS0FBbGdCLEVBQW1nQnl3QixnQkFBZSxZQUFVO0FBQUMsVUFBSXRxQixDQUFKO0FBQUEsVUFBTVgsQ0FBTjtBQUFBLFVBQVFzYyxJQUFFLElBQVYsQ0FBZSxJQUFHLEtBQUs3TyxPQUFMLENBQWF1UixRQUFiLEdBQXNCLEtBQUt2UixPQUFMLENBQWF1UixRQUFiLEtBQXdCcmUsSUFBRSxLQUFLMkQsT0FBTCxDQUFhOUosWUFBYixDQUEwQixNQUExQixDQUExQixLQUE4RCxLQUFLOEosT0FBTCxDQUFhOUosWUFBYixDQUEwQixJQUExQixDQUFwRixFQUFvSCxhQUFXLEtBQUs4SixPQUFMLENBQWFqSyxRQUF4QixJQUFrQyxTQUFPLEtBQUtpSyxPQUFMLENBQWE5SixZQUFiLENBQTBCLFVBQTFCLENBQWhLLEVBQXNNLE9BQU8sS0FBS2lULE9BQUwsQ0FBYXVSLFFBQWIsR0FBc0IsS0FBS3ZSLE9BQUwsQ0FBYXVSLFFBQWIsSUFBdUIsS0FBS1csTUFBbEQsRUFBeUQsS0FBS3hjLElBQUwsQ0FBVSxzQkFBVixDQUFoRSxDQUFrRyxJQUFHLENBQUMsS0FBS3NLLE9BQUwsQ0FBYXVSLFFBQWpCLEVBQTBCLE9BQU9sQyxFQUFFamQsSUFBRixDQUFPLHVIQUFQLEVBQStILEtBQUt0QyxRQUFwSSxHQUE4SSxJQUFySixDQUEwSixLQUFLa1EsT0FBTCxDQUFhdVIsUUFBYixHQUFzQixLQUFLdlIsT0FBTCxDQUFhdVIsUUFBYixDQUFzQmhiLE9BQXRCLENBQThCLHdCQUE5QixFQUF1RCxFQUF2RCxDQUF0QixFQUFpRnJELEtBQUdxYixFQUFFLGlCQUFlcmIsQ0FBZixHQUFpQixJQUFuQixFQUF5QjFDLElBQXpCLENBQThCLFVBQVMwQyxDQUFULEVBQVdYLENBQVgsRUFBYTtBQUFDLFlBQUlULElBQUV1ZCxFQUFFTyxPQUFGLENBQVVyZCxDQUFWLENBQU4sQ0FBbUIsWUFBVVQsQ0FBVixJQUFhLGVBQWFBLENBQTFCLElBQTZCUyxFQUFFbEYsWUFBRixDQUFld2hCLEVBQUU3TyxPQUFGLENBQVUzTyxTQUFWLEdBQW9CLFVBQW5DLEVBQThDd2QsRUFBRTdPLE9BQUYsQ0FBVXVSLFFBQXhELENBQTdCO0FBQStGLE9BQTlKLENBQXBGLENBQW9QLEtBQUksSUFBSXpmLElBQUUsS0FBS3VoQixZQUFMLEVBQU4sRUFBMEI3RSxJQUFFLENBQWhDLEVBQWtDQSxJQUFFMWMsRUFBRVYsTUFBdEMsRUFBNkNvZCxHQUE3QztBQUFpRCxZQUFHLEtBQUssQ0FBTCxNQUFVamMsSUFBRWdjLEVBQUV6YyxFQUFFK0osR0FBRixDQUFNMlMsQ0FBTixDQUFGLEVBQVl6ZSxJQUFaLENBQWlCLFNBQWpCLENBQVosQ0FBSCxFQUE0QztBQUFDLGVBQUtELFFBQUwsQ0FBY0MsSUFBZCxDQUFtQixlQUFuQixLQUFxQ3dDLEVBQUUycUIsVUFBRixDQUFhLEtBQUtwdEIsUUFBbEIsQ0FBckMsQ0FBaUU7QUFBTTtBQUFySyxPQUFxSyxPQUFPLEtBQUs0RixJQUFMLENBQVUsY0FBVixFQUF5QixDQUFDLENBQTFCLEdBQTZCbkQsS0FBRyxLQUFLbUQsSUFBTCxDQUFVLHNCQUFWLENBQXZDO0FBQXlFLEtBQTErQyxFQUEyK0NBLE1BQUssVUFBU3hDLENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsVUFBSVQsQ0FBSixDQUFNLFFBQU9vQixDQUFQLEdBQVUsS0FBSSxhQUFKO0FBQWtCcEIsY0FBRXljLEVBQUV2VSxNQUFGLENBQVMsSUFBSThILENBQUosQ0FBTSxLQUFLakwsT0FBWCxFQUFtQixLQUFLOGIsVUFBeEIsRUFBbUMsS0FBSzNTLE9BQXhDLENBQVQsRUFBMEQsSUFBSStPLENBQUosRUFBMUQsRUFBZ0Vsa0IsT0FBTzR5QixhQUF2RSxFQUFzRnZDLFdBQXRGLEVBQUYsQ0FBc0csTUFBTSxLQUFJLGNBQUo7QUFBbUJwcEIsY0FBRXljLEVBQUV2VSxNQUFGLENBQVMsSUFBSTBoQixDQUFKLENBQU0sS0FBSzdrQixPQUFYLEVBQW1CLEtBQUs4YixVQUF4QixFQUFtQyxLQUFLM1MsT0FBeEMsRUFBZ0QsS0FBS2xKLE1BQXJELENBQVQsRUFBc0UsSUFBSWlZLENBQUosRUFBdEUsRUFBNEVsa0IsT0FBTzR5QixhQUFuRixDQUFGLENBQW9HLE1BQU0sS0FBSSxzQkFBSjtBQUEyQjNyQixjQUFFeWMsRUFBRXZVLE1BQUYsQ0FBUyxJQUFJMGhCLENBQUosQ0FBTSxLQUFLN2tCLE9BQVgsRUFBbUIsS0FBSzhiLFVBQXhCLEVBQW1DLEtBQUszUyxPQUF4QyxFQUFnRCxLQUFLbEosTUFBckQsQ0FBVCxFQUFzRSxJQUFJbW1CLENBQUosRUFBdEUsRUFBNEUsSUFBSWxPLENBQUosRUFBNUUsRUFBa0Zsa0IsT0FBTzR5QixhQUF6RixFQUF3R2h0QixLQUF4RyxFQUFGLENBQWtILE1BQU07QUFBUSxnQkFBTSxJQUFJbUgsS0FBSixDQUFVMUUsSUFBRSxpQ0FBWixDQUFOLENBQWhhLENBQXFkLE9BQU8sS0FBSzhNLE9BQUwsQ0FBYXVSLFFBQWIsSUFBdUJsQyxFQUFFSyxPQUFGLENBQVUsS0FBSzdZLE9BQWYsRUFBdUIsS0FBS21KLE9BQUwsQ0FBYTNPLFNBQXBDLEVBQThDLFVBQTlDLEVBQXlELEtBQUsyTyxPQUFMLENBQWF1UixRQUF0RSxDQUF2QixFQUF1RyxLQUFLLENBQUwsS0FBU2hmLENBQVQsR0FBVyxLQUFLekMsUUFBTCxDQUFjQyxJQUFkLENBQW1CLGVBQW5CLEVBQW1DK0IsQ0FBbkMsQ0FBWCxJQUFrRCxLQUFLaEMsUUFBTCxDQUFjQyxJQUFkLENBQW1CLFNBQW5CLEVBQTZCK0IsQ0FBN0IsR0FBZ0NBLEVBQUU4a0Isa0JBQUYsRUFBaEMsRUFBdUQ5a0IsRUFBRXNvQixRQUFGLENBQVcsTUFBWCxDQUF6RyxDQUF2RyxFQUFvT3RvQixDQUEzTztBQUE2TyxLQUF0c0UsRUFBWixDQUFvdEUsSUFBSTRyQixJQUFFblAsRUFBRS9aLEVBQUYsQ0FBS21wQixNQUFMLENBQVlyckIsS0FBWixDQUFrQixHQUFsQixDQUFOLENBQTZCLElBQUd1VyxTQUFTNlUsRUFBRSxDQUFGLENBQVQsS0FBZ0IsQ0FBaEIsSUFBbUI3VSxTQUFTNlUsRUFBRSxDQUFGLENBQVQsSUFBZSxDQUFyQyxFQUF1QyxNQUFLLDZFQUFMLENBQW1GQSxFQUFFN3NCLE9BQUYsSUFBV3dlLEVBQUVqZCxJQUFGLENBQU8sMkZBQVAsQ0FBWCxDQUErRyxJQUFJdVAsSUFBRWdOLEVBQUUsSUFBSUksQ0FBSixFQUFGLEVBQVEsRUFBQ2xZLFNBQVEvSSxRQUFULEVBQWtCZ0MsVUFBU3llLEVBQUV6Z0IsUUFBRixDQUEzQixFQUF1QzRrQixrQkFBaUIsSUFBeEQsRUFBNkRFLGVBQWMsSUFBM0UsRUFBZ0Z3SSxTQUFRaUMsQ0FBeEYsRUFBMEZ2dUIsU0FBUSxPQUFsRyxFQUFSLENBQU4sQ0FBMEg2ZixFQUFFK00sRUFBRXB0QixTQUFKLEVBQWNvb0IsRUFBRVMsS0FBaEIsRUFBc0JwSSxFQUFFemdCLFNBQXhCLEdBQW1DcWdCLEVBQUU3TSxFQUFFeFQsU0FBSixFQUFjb29CLEVBQUVDLElBQWhCLEVBQXFCNUgsRUFBRXpnQixTQUF2QixDQUFuQyxFQUFxRXFnQixFQUFFME8sRUFBRS91QixTQUFKLEVBQWN5Z0IsRUFBRXpnQixTQUFoQixDQUFyRSxFQUFnR2lnQixFQUFFL1osRUFBRixDQUFLOFosT0FBTCxHQUFhQyxFQUFFL1osRUFBRixDQUFLb3BCLElBQUwsR0FBVSxVQUFTMXFCLENBQVQsRUFBVztBQUFDLFFBQUcsSUFBRSxLQUFLOUIsTUFBVixFQUFpQjtBQUFDLFVBQUltQixJQUFFLEVBQU4sQ0FBUyxPQUFPLEtBQUsvQixJQUFMLENBQVUsWUFBVTtBQUFDK0IsVUFBRWpGLElBQUYsQ0FBT2loQixFQUFFLElBQUYsRUFBUUQsT0FBUixDQUFnQnBiLENBQWhCLENBQVA7QUFBMkIsT0FBaEQsR0FBa0RYLENBQXpEO0FBQTJELFNBQUcsS0FBRyxLQUFLbkIsTUFBWCxFQUFrQixPQUFPLElBQUlpc0IsQ0FBSixDQUFNLEtBQUssQ0FBTCxDQUFOLEVBQWNucUIsQ0FBZCxDQUFQO0FBQXdCLEdBQW5RLEVBQW9RLEtBQUssQ0FBTCxLQUFTckksT0FBTzR5QixhQUFoQixLQUFnQzV5QixPQUFPNHlCLGFBQVAsR0FBcUIsRUFBckQsQ0FBcFEsRUFBNlQ5YixFQUFFM0IsT0FBRixHQUFVMk8sRUFBRVUsRUFBRTRCLFlBQUYsQ0FBZXBDLENBQWYsQ0FBRixFQUFvQmhrQixPQUFPZ3pCLGFBQTNCLENBQXZVLEVBQWlYaHpCLE9BQU9nekIsYUFBUCxHQUFxQmxjLEVBQUUzQixPQUF4WSxFQUFnWm5WLE9BQU9rcUIsT0FBUCxHQUFlbHFCLE9BQU8reUIsSUFBUCxHQUFZamMsQ0FBM2EsRUFBNmFBLEVBQUVtYyxLQUFGLEdBQVF6TyxDQUFyYixFQUF1YnhrQixPQUFPa3pCLFlBQVAsR0FBb0IsRUFBM2MsRUFBOGN4UCxFQUFFL2QsSUFBRixDQUFPNmUsQ0FBUCxFQUFTLFVBQVNuYyxDQUFULEVBQVdYLENBQVgsRUFBYTtBQUFDLGtCQUFZLE9BQU9BLENBQW5CLEtBQXVCMUgsT0FBT2t6QixZQUFQLENBQW9CN3FCLENBQXBCLElBQXVCLFlBQVU7QUFBQyxhQUFPbWMsRUFBRVcsUUFBRixDQUFXLG9GQUFYLEdBQWlHWCxFQUFFbmMsQ0FBRixFQUFLVSxLQUFMLENBQVd5YixDQUFYLEVBQWExYixTQUFiLENBQXhHO0FBQWdJLEtBQXpMO0FBQTJMLEdBQWxOLENBQTljLENBQWtxQixJQUFJaEYsSUFBRTlELE9BQU9rcUIsT0FBUCxDQUFldUcsa0JBQWYsR0FBa0MsSUFBSTNQLENBQUosQ0FBTTlnQixPQUFPZ3pCLGFBQVAsQ0FBcUJoSixVQUEzQixFQUFzQ2hxQixPQUFPZ3pCLGFBQVAsQ0FBcUJHLElBQTNELENBQXhDLENBQXlHbnpCLE9BQU9vekIsZ0JBQVAsR0FBd0IsRUFBeEIsRUFBMkIxUCxFQUFFL2QsSUFBRixDQUFPLHNJQUFzSThCLEtBQXRJLENBQTRJLEdBQTVJLENBQVAsRUFBd0osVUFBU1ksQ0FBVCxFQUFXWCxDQUFYLEVBQWE7QUFBQzFILFdBQU9rcUIsT0FBUCxDQUFleGlCLENBQWYsSUFBa0IsWUFBVTtBQUFDLGFBQU81RCxFQUFFNEQsQ0FBRixFQUFLcUIsS0FBTCxDQUFXakYsQ0FBWCxFQUFhZ0YsU0FBYixDQUFQO0FBQStCLEtBQTVELEVBQTZEOUksT0FBT296QixnQkFBUCxDQUF3QjFyQixDQUF4QixJQUEyQixZQUFVO0FBQUMsVUFBSVcsQ0FBSixDQUFNLE9BQU9tYyxFQUFFVyxRQUFGLENBQVcseUJBQXlCaGEsTUFBekIsQ0FBZ0N6RCxDQUFoQyxFQUFrQyxpRUFBbEMsRUFBcUd5RCxNQUFyRyxDQUE0R3pELENBQTVHLEVBQThHLFFBQTlHLENBQVgsR0FBb0ksQ0FBQ1csSUFBRXJJLE9BQU9rcUIsT0FBVixFQUFtQnhpQixDQUFuQixFQUFzQnFCLEtBQXRCLENBQTRCVixDQUE1QixFQUE4QlMsU0FBOUIsQ0FBM0k7QUFBb0wsS0FBN1I7QUFBOFIsR0FBcGMsQ0FBM0IsRUFBaWU5SSxPQUFPa3FCLE9BQVAsQ0FBZW1KLEVBQWYsR0FBa0J4SCxDQUFuZixFQUFxZjdyQixPQUFPc3pCLFNBQVAsR0FBaUIsRUFBQzFGLGFBQVksVUFBU3ZsQixDQUFULEVBQVdYLENBQVgsRUFBYVQsQ0FBYixFQUFlO0FBQUMsVUFBSStjLElBQUUsQ0FBQyxDQUFELEtBQUsvYyxDQUFYLENBQWEsT0FBT3VkLEVBQUVXLFFBQUYsQ0FBVyw0SUFBWCxHQUF5SjljLEVBQUV1bEIsV0FBRixDQUFjbG1CLENBQWQsRUFBZ0IsRUFBQzZsQixhQUFZdkosQ0FBYixFQUFoQixDQUFoSztBQUFpTSxLQUEzTyxFQUE0T2tKLG1CQUFrQixVQUFTN2tCLENBQVQsRUFBVztBQUFDLGFBQU9tYyxFQUFFVyxRQUFGLENBQVcsZ0ZBQVgsR0FBNkY5YyxFQUFFNmtCLGlCQUFGLEVBQXBHO0FBQTBILEtBQXBZLEVBQXRnQixFQUE0NEJ4SixFQUFFL2QsSUFBRixDQUFPLHVCQUF1QjhCLEtBQXZCLENBQTZCLEdBQTdCLENBQVAsRUFBeUMsVUFBU1ksQ0FBVCxFQUFXeVksQ0FBWCxFQUFhO0FBQUM5Z0IsV0FBT3N6QixTQUFQLENBQWlCeFMsQ0FBakIsSUFBb0IsVUFBU3pZLENBQVQsRUFBV1gsQ0FBWCxFQUFhVCxDQUFiLEVBQWUrYyxDQUFmLEVBQWlCTCxDQUFqQixFQUFtQjtBQUFDLFVBQUlPLElBQUUsQ0FBQyxDQUFELEtBQUtQLENBQVgsQ0FBYSxPQUFPYSxFQUFFVyxRQUFGLENBQVcscUNBQXFDaGEsTUFBckMsQ0FBNEMyVixDQUE1QyxFQUE4QywrRkFBOUMsQ0FBWCxHQUEySnpZLEVBQUV5WSxDQUFGLEVBQUtwWixDQUFMLEVBQU8sRUFBQzRsQixTQUFRcm1CLENBQVQsRUFBV3lsQixRQUFPMUksQ0FBbEIsRUFBb0J1SixhQUFZckosQ0FBaEMsRUFBUCxDQUFsSztBQUE2TSxLQUFsUTtBQUFtUSxHQUExVCxDQUE1NEIsRUFBd3NDLENBQUMsQ0FBRCxLQUFLbGtCLE9BQU9nekIsYUFBUCxDQUFxQk8sUUFBMUIsSUFBb0M3UCxFQUFFLFlBQVU7QUFBQ0EsTUFBRSx5QkFBRixFQUE2Qm5kLE1BQTdCLElBQXFDbWQsRUFBRSx5QkFBRixFQUE2QkQsT0FBN0IsRUFBckM7QUFBNEUsR0FBekYsQ0FBNXVDLENBQXUwQyxJQUFJK1AsSUFBRTlQLEVBQUUsRUFBRixDQUFOO0FBQUEsTUFBWStQLElBQUUsWUFBVTtBQUFDalAsTUFBRVcsUUFBRixDQUFXLDhHQUFYO0FBQTJILEdBQXBKLENBQXFKLFNBQVN1TyxDQUFULENBQVdoc0IsQ0FBWCxFQUFhVCxDQUFiLEVBQWU7QUFBQyxXQUFPUyxFQUFFaXNCLHNCQUFGLEtBQTJCanNCLEVBQUVpc0Isc0JBQUYsR0FBeUIsWUFBVTtBQUFDLFVBQUl0ckIsSUFBRTdFLE1BQU1DLFNBQU4sQ0FBZ0JxRCxLQUFoQixDQUFzQnlDLElBQXRCLENBQTJCVCxTQUEzQixFQUFxQyxDQUFyQyxDQUFOLENBQThDVCxFQUFFdXJCLE9BQUYsQ0FBVSxJQUFWLEdBQWdCbHNCLEVBQUVxQixLQUFGLENBQVE5QixLQUFHdXNCLENBQVgsRUFBYW5yQixDQUFiLENBQWhCO0FBQWdDLEtBQTdJLEdBQStJWCxFQUFFaXNCLHNCQUF4SjtBQUErSyxPQUFJRSxJQUFFLFVBQU4sQ0FBaUIsU0FBU0MsQ0FBVCxDQUFXenJCLENBQVgsRUFBYTtBQUFDLFdBQU8sTUFBSUEsRUFBRTByQixXQUFGLENBQWNGLENBQWQsRUFBZ0IsQ0FBaEIsQ0FBSixHQUF1QnhyQixFQUFFMnJCLE1BQUYsQ0FBU0gsRUFBRXR0QixNQUFYLENBQXZCLEdBQTBDOEIsQ0FBakQ7QUFBbUQsVUFBT3FiLEVBQUV1USxNQUFGLEdBQVMsVUFBUzVyQixDQUFULEVBQVdYLENBQVgsRUFBYTtBQUFDLFFBQUlULENBQUosQ0FBTSxJQUFHd3NCLEtBQUksYUFBVzlQLEVBQUU3YSxVQUFVLENBQVYsQ0FBRixDQUFYLElBQTRCLGNBQVksT0FBT0EsVUFBVSxDQUFWLENBQS9DLEtBQThEN0IsSUFBRTZCLFVBQVUsQ0FBVixDQUFGLEVBQWVwQixJQUFFb0IsVUFBVSxDQUFWLENBQS9FLENBQUosRUFBaUcsY0FBWSxPQUFPcEIsQ0FBdkgsRUFBeUgsTUFBTSxJQUFJcUYsS0FBSixDQUFVLGtCQUFWLENBQU4sQ0FBb0MvTSxPQUFPa3FCLE9BQVAsQ0FBZTlZLEVBQWYsQ0FBa0IwaUIsRUFBRXpyQixDQUFGLENBQWxCLEVBQXVCcXJCLEVBQUVoc0IsQ0FBRixFQUFJVCxDQUFKLENBQXZCO0FBQStCLEdBQXpOLEVBQTBOeWMsRUFBRXdFLFFBQUYsR0FBVyxVQUFTN2YsQ0FBVCxFQUFXWCxDQUFYLEVBQWFULENBQWIsRUFBZTtBQUFDLFFBQUd3c0IsS0FBSSxFQUFFcHJCLGFBQWF3b0IsQ0FBYixJQUFnQnhvQixhQUFhNE8sQ0FBL0IsQ0FBUCxFQUF5QyxNQUFNLElBQUlsSyxLQUFKLENBQVUsNEJBQVYsQ0FBTixDQUE4QyxJQUFHLFlBQVUsT0FBT3JGLENBQWpCLElBQW9CLGNBQVksT0FBT1QsQ0FBMUMsRUFBNEMsTUFBTSxJQUFJOEYsS0FBSixDQUFVLGtCQUFWLENBQU4sQ0FBb0MxRSxFQUFFK0ksRUFBRixDQUFLMGlCLEVBQUVwc0IsQ0FBRixDQUFMLEVBQVVnc0IsRUFBRXpzQixDQUFGLENBQVY7QUFBZ0IsR0FBNWEsRUFBNmF5YyxFQUFFeUUsV0FBRixHQUFjLFVBQVM5ZixDQUFULEVBQVdYLENBQVgsRUFBYTtBQUFDLFFBQUcrckIsS0FBSSxZQUFVLE9BQU9wckIsQ0FBakIsSUFBb0IsY0FBWSxPQUFPWCxDQUE5QyxFQUFnRCxNQUFNLElBQUlxRixLQUFKLENBQVUsaUJBQVYsQ0FBTixDQUFtQy9NLE9BQU9rcUIsT0FBUCxDQUFlclEsR0FBZixDQUFtQmlhLEVBQUV6ckIsQ0FBRixDQUFuQixFQUF3QlgsRUFBRWlzQixzQkFBMUI7QUFBa0QsR0FBOWtCLEVBQStrQmpRLEVBQUUwRSxhQUFGLEdBQWdCLFVBQVMvZixDQUFULEVBQVdYLENBQVgsRUFBYTtBQUFDLFFBQUcrckIsS0FBSSxFQUFFcHJCLGFBQWF3b0IsQ0FBYixJQUFnQnhvQixhQUFhNE8sQ0FBL0IsQ0FBUCxFQUF5QyxNQUFNLElBQUlsSyxLQUFKLENBQVUsNEJBQVYsQ0FBTixDQUE4QzFFLEVBQUV3UixHQUFGLENBQU1pYSxFQUFFcHNCLENBQUYsQ0FBTjtBQUFZLEdBQWh0QixFQUFpdEJnYyxFQUFFd1EsY0FBRixHQUFpQixVQUFTeHNCLENBQVQsRUFBVztBQUFDK3JCLFNBQUl6ekIsT0FBT2txQixPQUFQLENBQWVyUSxHQUFmLENBQW1CaWEsRUFBRXBzQixDQUFGLENBQW5CLENBQUosRUFBNkJnYyxFQUFFLDRCQUFGLEVBQWdDL2QsSUFBaEMsQ0FBcUMsWUFBVTtBQUFDLFVBQUkwQyxJQUFFcWIsRUFBRSxJQUFGLEVBQVF4ZSxJQUFSLENBQWEsU0FBYixDQUFOLENBQThCbUQsS0FBR0EsRUFBRXdSLEdBQUYsQ0FBTWlhLEVBQUVwc0IsQ0FBRixDQUFOLENBQUg7QUFBZSxLQUE3RixDQUE3QjtBQUE0SCxHQUExMkIsRUFBMjJCZ2MsRUFBRXlRLElBQUYsR0FBTyxVQUFTOXJCLENBQVQsRUFBV1gsQ0FBWCxFQUFhO0FBQUMsUUFBSVQsQ0FBSixDQUFNd3NCLElBQUksSUFBSXpQLElBQUV0YyxhQUFhbXBCLENBQWIsSUFBZ0JucEIsYUFBYXVQLENBQW5DO0FBQUEsUUFBcUMwTSxJQUFFbmdCLE1BQU1DLFNBQU4sQ0FBZ0JxRCxLQUFoQixDQUFzQnlDLElBQXRCLENBQTJCVCxTQUEzQixFQUFxQ2tiLElBQUUsQ0FBRixHQUFJLENBQXpDLENBQXZDLENBQW1GTCxFQUFFaVEsT0FBRixDQUFVRSxFQUFFenJCLENBQUYsQ0FBVixHQUFnQjJiLE1BQUl0YyxJQUFFMUgsT0FBT2txQixPQUFiLENBQWhCLEVBQXNDLENBQUNqakIsSUFBRVMsQ0FBSCxFQUFNdkMsT0FBTixDQUFjNEQsS0FBZCxDQUFvQjlCLENBQXBCLEVBQXNCcWQsRUFBRVgsQ0FBRixDQUF0QixDQUF0QztBQUFrRSxHQUEvaEMsRUFBZ2lDRCxFQUFFdlUsTUFBRixDQUFTLENBQUMsQ0FBVixFQUFZMkgsQ0FBWixFQUFjLEVBQUNzZCxpQkFBZ0IsRUFBQ0MsU0FBUSxFQUFDMXFCLElBQUcsVUFBU3RCLENBQVQsRUFBVztBQUFDLGlCQUFPLE9BQUtBLEVBQUVpc0IsTUFBUCxJQUFlanNCLEVBQUVpc0IsTUFBRixHQUFTLEdBQS9CO0FBQW1DLFNBQW5ELEVBQW9EN0ssS0FBSSxDQUFDLENBQXpELEVBQVQsRUFBcUU4SyxTQUFRLEVBQUM1cUIsSUFBRyxVQUFTdEIsQ0FBVCxFQUFXO0FBQUMsaUJBQU9BLEVBQUVpc0IsTUFBRixHQUFTLEdBQVQsSUFBYyxPQUFLanNCLEVBQUVpc0IsTUFBNUI7QUFBbUMsU0FBbkQsRUFBb0Q3SyxLQUFJLENBQUMsQ0FBekQsRUFBN0UsRUFBakIsRUFBMkorSyxtQkFBa0IsVUFBU25zQixDQUFULEVBQVdYLENBQVgsRUFBYVQsQ0FBYixFQUFlK2MsQ0FBZixFQUFpQjtBQUFDLGFBQU9sTixFQUFFc2QsZUFBRixDQUFrQi9yQixDQUFsQixJQUFxQixFQUFDc0IsSUFBR2pDLENBQUosRUFBTStoQixLQUFJeGlCLEtBQUcsQ0FBQyxDQUFkLEVBQWdCa08sU0FBUTZPLEtBQUcsRUFBM0IsRUFBckIsRUFBb0QsSUFBM0Q7QUFBZ0UsS0FBL1AsRUFBZCxDQUFoaUMsRUFBZ3pDbE4sRUFBRW1ULFlBQUYsQ0FBZSxRQUFmLEVBQXdCLEVBQUNoQixpQkFBZ0IsRUFBQyxJQUFHLFFBQUosRUFBYXlILFdBQVUsUUFBdkIsRUFBZ0M2RCxTQUFRLFNBQXhDLEVBQWtEcGYsU0FBUSxRQUExRCxFQUFqQixFQUFxRjRULGdCQUFlLFVBQVMxZ0IsQ0FBVCxFQUFXWCxDQUFYLEVBQWFULENBQWIsRUFBZStjLENBQWYsRUFBaUI7QUFBQyxVQUFJTCxDQUFKO0FBQUEsVUFBTU8sQ0FBTjtBQUFBLFVBQVFwRCxJQUFFLEVBQVY7QUFBQSxVQUFhZ0QsSUFBRTdjLEVBQUV5cEIsU0FBRixLQUFjLENBQUMsQ0FBRCxLQUFLenBCLEVBQUVzdEIsT0FBUCxHQUFlLFNBQWYsR0FBeUIsU0FBdkMsQ0FBZixDQUFpRSxJQUFHLEtBQUssQ0FBTCxLQUFTemQsRUFBRXNkLGVBQUYsQ0FBa0J0USxDQUFsQixDQUFaLEVBQWlDLE1BQU0sSUFBSS9XLEtBQUosQ0FBVSw0Q0FBMEMrVyxDQUExQyxHQUE0QyxHQUF0RCxDQUFOLENBQWlFLENBQUMsQ0FBRCxHQUFHLENBQUNwYyxJQUFFb1AsRUFBRXNkLGVBQUYsQ0FBa0J0USxDQUFsQixFQUFxQjJGLEdBQXJCLElBQTBCL2hCLENBQTdCLEVBQWdDdEYsT0FBaEMsQ0FBd0MsU0FBeEMsQ0FBSCxHQUFzRHNGLElBQUVBLEVBQUVnRSxPQUFGLENBQVUsU0FBVixFQUFvQitvQixtQkFBbUJwc0IsQ0FBbkIsQ0FBcEIsQ0FBeEQsR0FBbUd5WSxFQUFFa0QsRUFBRWhZLE9BQUYsQ0FBVTlKLFlBQVYsQ0FBdUIsTUFBdkIsS0FBZ0M4aEIsRUFBRWhZLE9BQUYsQ0FBVTlKLFlBQVYsQ0FBdUIsSUFBdkIsQ0FBbEMsSUFBZ0VtRyxDQUFuSyxDQUFxSyxJQUFJNGIsSUFBRVAsRUFBRXZVLE1BQUYsQ0FBUyxDQUFDLENBQVYsRUFBWWxJLEVBQUVrTyxPQUFGLElBQVcsRUFBdkIsRUFBMEIyQixFQUFFc2QsZUFBRixDQUFrQnRRLENBQWxCLEVBQXFCM08sT0FBL0MsQ0FBTixDQUE4RHdPLElBQUVELEVBQUV2VSxNQUFGLENBQVMsQ0FBQyxDQUFWLEVBQVksRUFBWixFQUFlLEVBQUNzYSxLQUFJL2hCLENBQUwsRUFBT3hDLE1BQUs0YixDQUFaLEVBQWNwZixNQUFLLEtBQW5CLEVBQWYsRUFBeUN1aUIsQ0FBekMsQ0FBRixFQUE4Q0QsRUFBRTdlLE9BQUYsQ0FBVSxtQkFBVixFQUE4QjZlLENBQTlCLEVBQWdDTCxDQUFoQyxDQUE5QyxFQUFpRk8sSUFBRVIsRUFBRWxSLEtBQUYsQ0FBUW1SLENBQVIsQ0FBbkYsRUFBOEYsS0FBSyxDQUFMLEtBQVM3TSxFQUFFc1ksWUFBWCxLQUEwQnRZLEVBQUVzWSxZQUFGLEdBQWUsRUFBekMsQ0FBOUYsQ0FBMkksSUFBSTlLLElBQUV4TixFQUFFc1ksWUFBRixDQUFlbEwsQ0FBZixJQUFrQnBOLEVBQUVzWSxZQUFGLENBQWVsTCxDQUFmLEtBQW1CUixFQUFFZ1IsSUFBRixDQUFPL1EsQ0FBUCxDQUEzQztBQUFBLFVBQXFEYSxJQUFFLFlBQVU7QUFBQyxZQUFJbmMsSUFBRXlPLEVBQUVzZCxlQUFGLENBQWtCdFEsQ0FBbEIsRUFBcUJuYSxFQUFyQixDQUF3QkosSUFBeEIsQ0FBNkJ5YSxDQUE3QixFQUErQk0sQ0FBL0IsRUFBaUM1YyxDQUFqQyxFQUFtQ1QsQ0FBbkMsQ0FBTixDQUE0QyxPQUFPb0IsTUFBSUEsSUFBRXFiLEVBQUU4RCxRQUFGLEdBQWFFLE1BQWIsRUFBTixHQUE2QmhFLEVBQUV5QyxJQUFGLENBQU85ZCxDQUFQLENBQXBDO0FBQThDLE9BQTVKLENBQTZKLE9BQU9pYyxFQUFFcVEsSUFBRixDQUFPblEsQ0FBUCxFQUFTQSxDQUFULENBQVA7QUFBbUIsS0FBdnpCLEVBQXd6QjJFLFVBQVMsQ0FBQyxDQUFsMEIsRUFBeEIsQ0FBaHpDLEVBQThvRXJTLEVBQUUxRixFQUFGLENBQUssYUFBTCxFQUFtQixZQUFVO0FBQUMwRixNQUFFc1ksWUFBRixHQUFlLEVBQWY7QUFBa0IsR0FBaEQsQ0FBOW9FLEVBQWdzRWxMLEVBQUV6Z0IsU0FBRixDQUFZK3dCLGlCQUFaLEdBQThCLFlBQVU7QUFBQyxXQUFPaFEsRUFBRVcsUUFBRixDQUFXLDBIQUFYLEdBQXVJck8sRUFBRTBkLGlCQUFGLENBQW9CenJCLEtBQXBCLENBQTBCK04sQ0FBMUIsRUFBNEJoTyxTQUE1QixDQUE5STtBQUFxTCxHQUE5NUUsRUFBKzVFZ08sRUFBRXdULFdBQUYsQ0FBYyxJQUFkLEVBQW1CLEVBQUNTLGdCQUFlLGlDQUFoQixFQUFrRHJwQixNQUFLLEVBQUM0bkIsT0FBTSxxQ0FBUCxFQUE2Q0csS0FBSSxtQ0FBakQsRUFBcUY5RCxRQUFPLHNDQUE1RixFQUFtSUQsU0FBUSx1Q0FBM0ksRUFBbUw2RCxRQUFPLDhCQUExTCxFQUF5TkMsVUFBUyxvQ0FBbE8sRUFBdkQsRUFBK1R5QixVQUFTLGlDQUF4VSxFQUEwV0MsVUFBUyx5QkFBblgsRUFBNllHLFNBQVEsaUNBQXJaLEVBQXVicE4sS0FBSSxtREFBM2IsRUFBK2V4VCxLQUFJLGlEQUFuZixFQUFxaUJpZixPQUFNLHlDQUEzaUIsRUFBcWxCNEIsV0FBVSxnRUFBL2xCLEVBQWdxQkMsV0FBVSxnRUFBMXFCLEVBQTJ1QmhsQixRQUFPLCtFQUFsdkIsRUFBazBCaWxCLFVBQVMsc0NBQTMwQixFQUFrM0JDLFVBQVMsc0NBQTMzQixFQUFrNkJDLE9BQU0sNENBQXg2QixFQUFxOUJDLFNBQVEsZ0NBQTc5QixFQUE4L0JDLFNBQVEsNkNBQXRnQyxFQUFuQixDQUEvNUUsRUFBdytHOVUsRUFBRXFULFNBQUYsQ0FBWSxJQUFaLENBQXgrRyxFQUEyL0csSUFBSSxZQUFVO0FBQUMsUUFBSW5HLElBQUUsSUFBTjtBQUFBLFFBQVdMLElBQUUzakIsVUFBUTQwQixNQUFyQixDQUE0QjlRLEVBQUUsSUFBRixFQUFPLEVBQUMrUSxlQUFjLFVBQVN4c0IsQ0FBVCxFQUFXO0FBQUMsZUFBT0EsRUFBRXlzQixhQUFGLElBQWlCLENBQUMsQ0FBRCxLQUFLenNCLEVBQUV5c0IsYUFBRixDQUFnQkMsU0FBN0M7QUFBdUQsT0FBbEYsRUFBbUZDLGdCQUFlLFVBQVMzc0IsQ0FBVCxFQUFXO0FBQUMyYixVQUFFNlEsYUFBRixDQUFnQnhzQixDQUFoQixLQUFvQnFiLEVBQUVyYixFQUFFeEcsTUFBSixFQUFZc0QsT0FBWixDQUFvQixPQUFwQixDQUFwQjtBQUFpRCxPQUEvSixFQUFnSzh2QixZQUFXLFVBQVM1c0IsQ0FBVCxFQUFXO0FBQUMyYixVQUFFNlEsYUFBRixDQUFnQnhzQixDQUFoQixNQUFxQjJiLEVBQUVrUixTQUFGLENBQVk3c0IsQ0FBWixHQUFlcWIsRUFBRXpnQixRQUFGLEVBQVltTyxFQUFaLENBQWUsbUJBQWYsRUFBbUMvSSxFQUFFbkQsSUFBRixDQUFPaXdCLFFBQTFDLEVBQW1EblIsRUFBRWdSLGNBQXJELENBQWYsRUFBb0ZoUixFQUFFZ1IsY0FBRixDQUFpQjNzQixDQUFqQixDQUF6RztBQUE4SCxPQUFyVCxFQUFzVDZzQixXQUFVLFVBQVM3c0IsQ0FBVCxFQUFXO0FBQUMyYixVQUFFNlEsYUFBRixDQUFnQnhzQixDQUFoQixLQUFvQnFiLEVBQUV6Z0IsUUFBRixFQUFZNFcsR0FBWixDQUFnQixrQkFBaEIsRUFBbUN4UixFQUFFbkQsSUFBRixDQUFPaXdCLFFBQTFDLEVBQW1EblIsRUFBRWtSLFNBQXJELEVBQWdFcmIsR0FBaEUsQ0FBb0UsbUJBQXBFLEVBQXdGeFIsRUFBRW5ELElBQUYsQ0FBT2l3QixRQUEvRixFQUF3R25SLEVBQUVpUixVQUExRyxDQUFwQjtBQUEwSSxPQUF0ZCxFQUF1ZEcsU0FBUSxZQUFVO0FBQUMsWUFBRyxDQUFDelIsRUFBRTBSLGlCQUFOLEVBQXdCO0FBQUMxUixZQUFFMFIsaUJBQUYsR0FBb0IsT0FBcEIsQ0FBNEIsS0FBSSxJQUFJaHRCLElBQUUsQ0FBQyxRQUFELEVBQVUsd0JBQVYsRUFBbUMscUJBQW5DLEVBQXlELG9CQUF6RCxDQUFOLEVBQXFGWCxJQUFFLENBQTNGLEVBQTZGQSxJQUFFVyxFQUFFOUIsTUFBakcsRUFBd0dtQixHQUF4RyxFQUE0RztBQUFDLGdCQUFJVCxJQUFFb0IsRUFBRVgsQ0FBRixDQUFOLENBQVdnYyxFQUFFemdCLFFBQUYsRUFBWW1PLEVBQVosQ0FBZSxrQkFBZixFQUFrQ25LLENBQWxDLEVBQW9DLEVBQUNrdUIsVUFBU2x1QixDQUFWLEVBQXBDLEVBQWlEK2MsRUFBRWtSLFNBQW5ELEVBQThEOWpCLEVBQTlELENBQWlFLG1CQUFqRSxFQUFxRm5LLENBQXJGLEVBQXVGLEVBQUNrdUIsVUFBU2x1QixDQUFWLEVBQXZGLEVBQW9HK2MsRUFBRWlSLFVBQXRHO0FBQWtIO0FBQUM7QUFBQyxPQUEzd0IsRUFBNHdCSyxXQUFVLFlBQVU7QUFBQyxlQUFPM1IsRUFBRTBSLGlCQUFULEVBQTJCM1IsRUFBRXpnQixRQUFGLEVBQVk0VyxHQUFaLENBQWdCLGFBQWhCLENBQTNCO0FBQTBELE9BQTMxQixFQUFQO0FBQXEyQixHQUFoNUIsRUFBRCxDQUFtNUJ1YixPQUFuNUIsRUFBMS9HLEVBQXU1SXRlLENBQTk1STtBQUFnNkksQ0FBdmx6QyxDQUFEO0FBQ0E7OztBQ0RBOzs7QUFHQSxDQUFDLFVBQVVoVCxDQUFWLEVBQWE7QUFDVjs7QUFDQUEsTUFBRSxzQkFBRixFQUEwQnl4QixLQUExQixDQUFnQyxZQUFXO0FBQ3ZDenhCLFVBQUUsSUFBRixFQUFRMHhCLFdBQVIsQ0FBb0IsUUFBcEI7QUFDQTF4QixVQUFFLElBQUYsRUFBUW1JLE1BQVIsR0FBaUJ1cEIsV0FBakIsQ0FBNkIsUUFBN0I7QUFDSCxLQUhEO0FBSUgsQ0FORCxFQU1HN3BCLE1BTkg7OztBQ0hBLENBQUMsVUFBUzdILENBQVQsRUFBWTtBQUNYLE1BQUkyeEIsV0FBVzN4QixFQUFFLHVCQUFGLEVBQTJCMmYsT0FBM0IsRUFBZjtBQUNBLE1BQUlpUyxPQUFPNXhCLEVBQUUsTUFBRixFQUFVTyxJQUFWLENBQWUsTUFBZixFQUF1QnlDLEtBQXZCLENBQTZCLENBQTdCLEVBQStCLENBQS9CLENBQVg7O0FBR0EsTUFBSTZqQixXQUFXO0FBQ2IsVUFBTTtBQUNKLGVBQVMsaURBREw7QUFFSixjQUFRO0FBRkosS0FETztBQUtiLFVBQU07QUFDSixlQUFTLHFDQURMO0FBRUosY0FBUTtBQUZKO0FBTE8sR0FBZjs7QUFZQTdtQixJQUFFLGFBQUYsRUFDR08sSUFESCxDQUNRLFVBRFIsRUFDb0IsSUFEcEIsRUFFR0EsSUFGSCxDQUVRLG1CQUZSLEVBRTZCLE9BRjdCLEVBR0dBLElBSEgsQ0FHUSw0QkFIUixFQUdzQ3NtQixTQUFTK0ssSUFBVCxFQUFlcE0sS0FIckQ7O0FBS0F4bEIsSUFBRSxtQkFBRixFQUNHTyxJQURILENBQ1EsVUFEUixFQUNvQixJQURwQixFQUVHQSxJQUZILENBRVEsNEJBRlIsRUFFc0NzbUIsU0FBUytLLElBQVQsRUFBZXh3QixJQUZyRDs7QUFLQSxNQUFJeXdCLGtCQUFrQjd4QixFQUFFLGNBQUYsRUFBa0IyZixPQUFsQixFQUF0QjtBQUNBM2YsSUFBRSxTQUFGLEVBQWFPLElBQWIsQ0FBa0IsVUFBbEIsRUFBOEIsSUFBOUI7QUFDQVAsSUFBRSxRQUFGLEVBQVlPLElBQVosQ0FBaUIsVUFBakIsRUFBNkIsSUFBN0IsRUFDR0EsSUFESCxDQUNRLG1CQURSLEVBQzZCLE9BRDdCLEVBRUdBLElBRkgsQ0FFUSw0QkFGUixFQUVzQ3NtQixTQUFTK0ssSUFBVCxFQUFlcE0sS0FGckQ7O0FBSUF4bEIsSUFBRSxVQUFGLEVBQWNPLElBQWQsQ0FBbUIsVUFBbkIsRUFBK0IsSUFBL0IsRUFBcUNBLElBQXJDLENBQTBDLHdCQUExQyxFQUFvRSxDQUFwRTtBQUVELENBbkNELEVBbUNHc0gsTUFuQ0g7OztBQ0FBQSxPQUFPMUksUUFBUCxFQUFpQmlELFVBQWpCIiwiZmlsZSI6ImZvdW5kYXRpb24uanMiLCJzb3VyY2VzQ29udGVudCI6WyJ3aW5kb3cud2hhdElucHV0ID0gKGZ1bmN0aW9uKCkge1xuXG4gICd1c2Ugc3RyaWN0JztcblxuICAvKlxuICAgIC0tLS0tLS0tLS0tLS0tLVxuICAgIHZhcmlhYmxlc1xuICAgIC0tLS0tLS0tLS0tLS0tLVxuICAqL1xuXG4gIC8vIGFycmF5IG9mIGFjdGl2ZWx5IHByZXNzZWQga2V5c1xuICB2YXIgYWN0aXZlS2V5cyA9IFtdO1xuXG4gIC8vIGNhY2hlIGRvY3VtZW50LmJvZHlcbiAgdmFyIGJvZHk7XG5cbiAgLy8gYm9vbGVhbjogdHJ1ZSBpZiB0b3VjaCBidWZmZXIgdGltZXIgaXMgcnVubmluZ1xuICB2YXIgYnVmZmVyID0gZmFsc2U7XG5cbiAgLy8gdGhlIGxhc3QgdXNlZCBpbnB1dCB0eXBlXG4gIHZhciBjdXJyZW50SW5wdXQgPSBudWxsO1xuXG4gIC8vIGBpbnB1dGAgdHlwZXMgdGhhdCBkb24ndCBhY2NlcHQgdGV4dFxuICB2YXIgbm9uVHlwaW5nSW5wdXRzID0gW1xuICAgICdidXR0b24nLFxuICAgICdjaGVja2JveCcsXG4gICAgJ2ZpbGUnLFxuICAgICdpbWFnZScsXG4gICAgJ3JhZGlvJyxcbiAgICAncmVzZXQnLFxuICAgICdzdWJtaXQnXG4gIF07XG5cbiAgLy8gZGV0ZWN0IHZlcnNpb24gb2YgbW91c2Ugd2hlZWwgZXZlbnQgdG8gdXNlXG4gIC8vIHZpYSBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9FdmVudHMvd2hlZWxcbiAgdmFyIG1vdXNlV2hlZWwgPSBkZXRlY3RXaGVlbCgpO1xuXG4gIC8vIGxpc3Qgb2YgbW9kaWZpZXIga2V5cyBjb21tb25seSB1c2VkIHdpdGggdGhlIG1vdXNlIGFuZFxuICAvLyBjYW4gYmUgc2FmZWx5IGlnbm9yZWQgdG8gcHJldmVudCBmYWxzZSBrZXlib2FyZCBkZXRlY3Rpb25cbiAgdmFyIGlnbm9yZU1hcCA9IFtcbiAgICAxNiwgLy8gc2hpZnRcbiAgICAxNywgLy8gY29udHJvbFxuICAgIDE4LCAvLyBhbHRcbiAgICA5MSwgLy8gV2luZG93cyBrZXkgLyBsZWZ0IEFwcGxlIGNtZFxuICAgIDkzICAvLyBXaW5kb3dzIG1lbnUgLyByaWdodCBBcHBsZSBjbWRcbiAgXTtcblxuICAvLyBtYXBwaW5nIG9mIGV2ZW50cyB0byBpbnB1dCB0eXBlc1xuICB2YXIgaW5wdXRNYXAgPSB7XG4gICAgJ2tleWRvd24nOiAna2V5Ym9hcmQnLFxuICAgICdrZXl1cCc6ICdrZXlib2FyZCcsXG4gICAgJ21vdXNlZG93bic6ICdtb3VzZScsXG4gICAgJ21vdXNlbW92ZSc6ICdtb3VzZScsXG4gICAgJ01TUG9pbnRlckRvd24nOiAncG9pbnRlcicsXG4gICAgJ01TUG9pbnRlck1vdmUnOiAncG9pbnRlcicsXG4gICAgJ3BvaW50ZXJkb3duJzogJ3BvaW50ZXInLFxuICAgICdwb2ludGVybW92ZSc6ICdwb2ludGVyJyxcbiAgICAndG91Y2hzdGFydCc6ICd0b3VjaCdcbiAgfTtcblxuICAvLyBhZGQgY29ycmVjdCBtb3VzZSB3aGVlbCBldmVudCBtYXBwaW5nIHRvIGBpbnB1dE1hcGBcbiAgaW5wdXRNYXBbZGV0ZWN0V2hlZWwoKV0gPSAnbW91c2UnO1xuXG4gIC8vIGFycmF5IG9mIGFsbCB1c2VkIGlucHV0IHR5cGVzXG4gIHZhciBpbnB1dFR5cGVzID0gW107XG5cbiAgLy8gbWFwcGluZyBvZiBrZXkgY29kZXMgdG8gYSBjb21tb24gbmFtZVxuICB2YXIga2V5TWFwID0ge1xuICAgIDk6ICd0YWInLFxuICAgIDEzOiAnZW50ZXInLFxuICAgIDE2OiAnc2hpZnQnLFxuICAgIDI3OiAnZXNjJyxcbiAgICAzMjogJ3NwYWNlJyxcbiAgICAzNzogJ2xlZnQnLFxuICAgIDM4OiAndXAnLFxuICAgIDM5OiAncmlnaHQnLFxuICAgIDQwOiAnZG93bidcbiAgfTtcblxuICAvLyBtYXAgb2YgSUUgMTAgcG9pbnRlciBldmVudHNcbiAgdmFyIHBvaW50ZXJNYXAgPSB7XG4gICAgMjogJ3RvdWNoJyxcbiAgICAzOiAndG91Y2gnLCAvLyB0cmVhdCBwZW4gbGlrZSB0b3VjaFxuICAgIDQ6ICdtb3VzZSdcbiAgfTtcblxuICAvLyB0b3VjaCBidWZmZXIgdGltZXJcbiAgdmFyIHRpbWVyO1xuXG5cbiAgLypcbiAgICAtLS0tLS0tLS0tLS0tLS1cbiAgICBmdW5jdGlvbnNcbiAgICAtLS0tLS0tLS0tLS0tLS1cbiAgKi9cblxuICAvLyBhbGxvd3MgZXZlbnRzIHRoYXQgYXJlIGFsc28gdHJpZ2dlcmVkIHRvIGJlIGZpbHRlcmVkIG91dCBmb3IgYHRvdWNoc3RhcnRgXG4gIGZ1bmN0aW9uIGV2ZW50QnVmZmVyKCkge1xuICAgIGNsZWFyVGltZXIoKTtcbiAgICBzZXRJbnB1dChldmVudCk7XG5cbiAgICBidWZmZXIgPSB0cnVlO1xuICAgIHRpbWVyID0gd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICBidWZmZXIgPSBmYWxzZTtcbiAgICB9LCA2NTApO1xuICB9XG5cbiAgZnVuY3Rpb24gYnVmZmVyZWRFdmVudChldmVudCkge1xuICAgIGlmICghYnVmZmVyKSBzZXRJbnB1dChldmVudCk7XG4gIH1cblxuICBmdW5jdGlvbiB1bkJ1ZmZlcmVkRXZlbnQoZXZlbnQpIHtcbiAgICBjbGVhclRpbWVyKCk7XG4gICAgc2V0SW5wdXQoZXZlbnQpO1xuICB9XG5cbiAgZnVuY3Rpb24gY2xlYXJUaW1lcigpIHtcbiAgICB3aW5kb3cuY2xlYXJUaW1lb3V0KHRpbWVyKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHNldElucHV0KGV2ZW50KSB7XG4gICAgdmFyIGV2ZW50S2V5ID0ga2V5KGV2ZW50KTtcbiAgICB2YXIgdmFsdWUgPSBpbnB1dE1hcFtldmVudC50eXBlXTtcbiAgICBpZiAodmFsdWUgPT09ICdwb2ludGVyJykgdmFsdWUgPSBwb2ludGVyVHlwZShldmVudCk7XG5cbiAgICAvLyBkb24ndCBkbyBhbnl0aGluZyBpZiB0aGUgdmFsdWUgbWF0Y2hlcyB0aGUgaW5wdXQgdHlwZSBhbHJlYWR5IHNldFxuICAgIGlmIChjdXJyZW50SW5wdXQgIT09IHZhbHVlKSB7XG4gICAgICB2YXIgZXZlbnRUYXJnZXQgPSB0YXJnZXQoZXZlbnQpO1xuICAgICAgdmFyIGV2ZW50VGFyZ2V0Tm9kZSA9IGV2ZW50VGFyZ2V0Lm5vZGVOYW1lLnRvTG93ZXJDYXNlKCk7XG4gICAgICB2YXIgZXZlbnRUYXJnZXRUeXBlID0gKGV2ZW50VGFyZ2V0Tm9kZSA9PT0gJ2lucHV0JykgPyBldmVudFRhcmdldC5nZXRBdHRyaWJ1dGUoJ3R5cGUnKSA6IG51bGw7XG5cbiAgICAgIGlmIChcbiAgICAgICAgKC8vIG9ubHkgaWYgdGhlIHVzZXIgZmxhZyB0byBhbGxvdyB0eXBpbmcgaW4gZm9ybSBmaWVsZHMgaXNuJ3Qgc2V0XG4gICAgICAgICFib2R5Lmhhc0F0dHJpYnV0ZSgnZGF0YS13aGF0aW5wdXQtZm9ybXR5cGluZycpICYmXG5cbiAgICAgICAgLy8gb25seSBpZiBjdXJyZW50SW5wdXQgaGFzIGEgdmFsdWVcbiAgICAgICAgY3VycmVudElucHV0ICYmXG5cbiAgICAgICAgLy8gb25seSBpZiB0aGUgaW5wdXQgaXMgYGtleWJvYXJkYFxuICAgICAgICB2YWx1ZSA9PT0gJ2tleWJvYXJkJyAmJlxuXG4gICAgICAgIC8vIG5vdCBpZiB0aGUga2V5IGlzIGBUQUJgXG4gICAgICAgIGtleU1hcFtldmVudEtleV0gIT09ICd0YWInICYmXG5cbiAgICAgICAgLy8gb25seSBpZiB0aGUgdGFyZ2V0IGlzIGEgZm9ybSBpbnB1dCB0aGF0IGFjY2VwdHMgdGV4dFxuICAgICAgICAoXG4gICAgICAgICAgIGV2ZW50VGFyZ2V0Tm9kZSA9PT0gJ3RleHRhcmVhJyB8fFxuICAgICAgICAgICBldmVudFRhcmdldE5vZGUgPT09ICdzZWxlY3QnIHx8XG4gICAgICAgICAgIChldmVudFRhcmdldE5vZGUgPT09ICdpbnB1dCcgJiYgbm9uVHlwaW5nSW5wdXRzLmluZGV4T2YoZXZlbnRUYXJnZXRUeXBlKSA8IDApXG4gICAgICAgICkpIHx8IChcbiAgICAgICAgICAvLyBpZ25vcmUgbW9kaWZpZXIga2V5c1xuICAgICAgICAgIGlnbm9yZU1hcC5pbmRleE9mKGV2ZW50S2V5KSA+IC0xXG4gICAgICAgIClcbiAgICAgICkge1xuICAgICAgICAvLyBpZ25vcmUga2V5Ym9hcmQgdHlwaW5nXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzd2l0Y2hJbnB1dCh2YWx1ZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHZhbHVlID09PSAna2V5Ym9hcmQnKSBsb2dLZXlzKGV2ZW50S2V5KTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHN3aXRjaElucHV0KHN0cmluZykge1xuICAgIGN1cnJlbnRJbnB1dCA9IHN0cmluZztcbiAgICBib2R5LnNldEF0dHJpYnV0ZSgnZGF0YS13aGF0aW5wdXQnLCBjdXJyZW50SW5wdXQpO1xuXG4gICAgaWYgKGlucHV0VHlwZXMuaW5kZXhPZihjdXJyZW50SW5wdXQpID09PSAtMSkgaW5wdXRUeXBlcy5wdXNoKGN1cnJlbnRJbnB1dCk7XG4gIH1cblxuICBmdW5jdGlvbiBrZXkoZXZlbnQpIHtcbiAgICByZXR1cm4gKGV2ZW50LmtleUNvZGUpID8gZXZlbnQua2V5Q29kZSA6IGV2ZW50LndoaWNoO1xuICB9XG5cbiAgZnVuY3Rpb24gdGFyZ2V0KGV2ZW50KSB7XG4gICAgcmV0dXJuIGV2ZW50LnRhcmdldCB8fCBldmVudC5zcmNFbGVtZW50O1xuICB9XG5cbiAgZnVuY3Rpb24gcG9pbnRlclR5cGUoZXZlbnQpIHtcbiAgICBpZiAodHlwZW9mIGV2ZW50LnBvaW50ZXJUeXBlID09PSAnbnVtYmVyJykge1xuICAgICAgcmV0dXJuIHBvaW50ZXJNYXBbZXZlbnQucG9pbnRlclR5cGVdO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gKGV2ZW50LnBvaW50ZXJUeXBlID09PSAncGVuJykgPyAndG91Y2gnIDogZXZlbnQucG9pbnRlclR5cGU7IC8vIHRyZWF0IHBlbiBsaWtlIHRvdWNoXG4gICAgfVxuICB9XG5cbiAgLy8ga2V5Ym9hcmQgbG9nZ2luZ1xuICBmdW5jdGlvbiBsb2dLZXlzKGV2ZW50S2V5KSB7XG4gICAgaWYgKGFjdGl2ZUtleXMuaW5kZXhPZihrZXlNYXBbZXZlbnRLZXldKSA9PT0gLTEgJiYga2V5TWFwW2V2ZW50S2V5XSkgYWN0aXZlS2V5cy5wdXNoKGtleU1hcFtldmVudEtleV0pO1xuICB9XG5cbiAgZnVuY3Rpb24gdW5Mb2dLZXlzKGV2ZW50KSB7XG4gICAgdmFyIGV2ZW50S2V5ID0ga2V5KGV2ZW50KTtcbiAgICB2YXIgYXJyYXlQb3MgPSBhY3RpdmVLZXlzLmluZGV4T2Yoa2V5TWFwW2V2ZW50S2V5XSk7XG5cbiAgICBpZiAoYXJyYXlQb3MgIT09IC0xKSBhY3RpdmVLZXlzLnNwbGljZShhcnJheVBvcywgMSk7XG4gIH1cblxuICBmdW5jdGlvbiBiaW5kRXZlbnRzKCkge1xuICAgIGJvZHkgPSBkb2N1bWVudC5ib2R5O1xuXG4gICAgLy8gcG9pbnRlciBldmVudHMgKG1vdXNlLCBwZW4sIHRvdWNoKVxuICAgIGlmICh3aW5kb3cuUG9pbnRlckV2ZW50KSB7XG4gICAgICBib2R5LmFkZEV2ZW50TGlzdGVuZXIoJ3BvaW50ZXJkb3duJywgYnVmZmVyZWRFdmVudCk7XG4gICAgICBib2R5LmFkZEV2ZW50TGlzdGVuZXIoJ3BvaW50ZXJtb3ZlJywgYnVmZmVyZWRFdmVudCk7XG4gICAgfSBlbHNlIGlmICh3aW5kb3cuTVNQb2ludGVyRXZlbnQpIHtcbiAgICAgIGJvZHkuYWRkRXZlbnRMaXN0ZW5lcignTVNQb2ludGVyRG93bicsIGJ1ZmZlcmVkRXZlbnQpO1xuICAgICAgYm9keS5hZGRFdmVudExpc3RlbmVyKCdNU1BvaW50ZXJNb3ZlJywgYnVmZmVyZWRFdmVudCk7XG4gICAgfSBlbHNlIHtcblxuICAgICAgLy8gbW91c2UgZXZlbnRzXG4gICAgICBib2R5LmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlZG93bicsIGJ1ZmZlcmVkRXZlbnQpO1xuICAgICAgYm9keS5hZGRFdmVudExpc3RlbmVyKCdtb3VzZW1vdmUnLCBidWZmZXJlZEV2ZW50KTtcblxuICAgICAgLy8gdG91Y2ggZXZlbnRzXG4gICAgICBpZiAoJ29udG91Y2hzdGFydCcgaW4gd2luZG93KSB7XG4gICAgICAgIGJvZHkuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hzdGFydCcsIGV2ZW50QnVmZmVyKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBtb3VzZSB3aGVlbFxuICAgIGJvZHkuYWRkRXZlbnRMaXN0ZW5lcihtb3VzZVdoZWVsLCBidWZmZXJlZEV2ZW50KTtcblxuICAgIC8vIGtleWJvYXJkIGV2ZW50c1xuICAgIGJvZHkuYWRkRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsIHVuQnVmZmVyZWRFdmVudCk7XG4gICAgYm9keS5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIHVuQnVmZmVyZWRFdmVudCk7XG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigna2V5dXAnLCB1bkxvZ0tleXMpO1xuICB9XG5cblxuICAvKlxuICAgIC0tLS0tLS0tLS0tLS0tLVxuICAgIHV0aWxpdGllc1xuICAgIC0tLS0tLS0tLS0tLS0tLVxuICAqL1xuXG4gIC8vIGRldGVjdCB2ZXJzaW9uIG9mIG1vdXNlIHdoZWVsIGV2ZW50IHRvIHVzZVxuICAvLyB2aWEgaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4tVVMvZG9jcy9XZWIvRXZlbnRzL3doZWVsXG4gIGZ1bmN0aW9uIGRldGVjdFdoZWVsKCkge1xuICAgIHJldHVybiBtb3VzZVdoZWVsID0gJ29ud2hlZWwnIGluIGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpID9cbiAgICAgICd3aGVlbCcgOiAvLyBNb2Rlcm4gYnJvd3NlcnMgc3VwcG9ydCBcIndoZWVsXCJcblxuICAgICAgZG9jdW1lbnQub25tb3VzZXdoZWVsICE9PSB1bmRlZmluZWQgP1xuICAgICAgICAnbW91c2V3aGVlbCcgOiAvLyBXZWJraXQgYW5kIElFIHN1cHBvcnQgYXQgbGVhc3QgXCJtb3VzZXdoZWVsXCJcbiAgICAgICAgJ0RPTU1vdXNlU2Nyb2xsJzsgLy8gbGV0J3MgYXNzdW1lIHRoYXQgcmVtYWluaW5nIGJyb3dzZXJzIGFyZSBvbGRlciBGaXJlZm94XG4gIH1cblxuXG4gIC8qXG4gICAgLS0tLS0tLS0tLS0tLS0tXG4gICAgaW5pdFxuXG4gICAgZG9uJ3Qgc3RhcnQgc2NyaXB0IHVubGVzcyBicm93c2VyIGN1dHMgdGhlIG11c3RhcmQsXG4gICAgYWxzbyBwYXNzZXMgaWYgcG9seWZpbGxzIGFyZSB1c2VkXG4gICAgLS0tLS0tLS0tLS0tLS0tXG4gICovXG5cbiAgaWYgKFxuICAgICdhZGRFdmVudExpc3RlbmVyJyBpbiB3aW5kb3cgJiZcbiAgICBBcnJheS5wcm90b3R5cGUuaW5kZXhPZlxuICApIHtcblxuICAgIC8vIGlmIHRoZSBkb20gaXMgYWxyZWFkeSByZWFkeSBhbHJlYWR5IChzY3JpcHQgd2FzIHBsYWNlZCBhdCBib3R0b20gb2YgPGJvZHk+KVxuICAgIGlmIChkb2N1bWVudC5ib2R5KSB7XG4gICAgICBiaW5kRXZlbnRzKCk7XG5cbiAgICAvLyBvdGhlcndpc2Ugd2FpdCBmb3IgdGhlIGRvbSB0byBsb2FkIChzY3JpcHQgd2FzIHBsYWNlZCBpbiB0aGUgPGhlYWQ+KVxuICAgIH0gZWxzZSB7XG4gICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgYmluZEV2ZW50cyk7XG4gICAgfVxuICB9XG5cblxuICAvKlxuICAgIC0tLS0tLS0tLS0tLS0tLVxuICAgIGFwaVxuICAgIC0tLS0tLS0tLS0tLS0tLVxuICAqL1xuXG4gIHJldHVybiB7XG5cbiAgICAvLyByZXR1cm5zIHN0cmluZzogdGhlIGN1cnJlbnQgaW5wdXQgdHlwZVxuICAgIGFzazogZnVuY3Rpb24oKSB7IHJldHVybiBjdXJyZW50SW5wdXQ7IH0sXG5cbiAgICAvLyByZXR1cm5zIGFycmF5OiBjdXJyZW50bHkgcHJlc3NlZCBrZXlzXG4gICAga2V5czogZnVuY3Rpb24oKSB7IHJldHVybiBhY3RpdmVLZXlzOyB9LFxuXG4gICAgLy8gcmV0dXJucyBhcnJheTogYWxsIHRoZSBkZXRlY3RlZCBpbnB1dCB0eXBlc1xuICAgIHR5cGVzOiBmdW5jdGlvbigpIHsgcmV0dXJuIGlucHV0VHlwZXM7IH0sXG5cbiAgICAvLyBhY2NlcHRzIHN0cmluZzogbWFudWFsbHkgc2V0IHRoZSBpbnB1dCB0eXBlXG4gICAgc2V0OiBzd2l0Y2hJbnB1dFxuICB9O1xuXG59KCkpO1xuIiwiIWZ1bmN0aW9uKCQpIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBGT1VOREFUSU9OX1ZFUlNJT04gPSAnNi4yLjQnO1xuXG4vLyBHbG9iYWwgRm91bmRhdGlvbiBvYmplY3Rcbi8vIFRoaXMgaXMgYXR0YWNoZWQgdG8gdGhlIHdpbmRvdywgb3IgdXNlZCBhcyBhIG1vZHVsZSBmb3IgQU1EL0Jyb3dzZXJpZnlcbnZhciBGb3VuZGF0aW9uID0ge1xuICB2ZXJzaW9uOiBGT1VOREFUSU9OX1ZFUlNJT04sXG5cbiAgLyoqXG4gICAqIFN0b3JlcyBpbml0aWFsaXplZCBwbHVnaW5zLlxuICAgKi9cbiAgX3BsdWdpbnM6IHt9LFxuXG4gIC8qKlxuICAgKiBTdG9yZXMgZ2VuZXJhdGVkIHVuaXF1ZSBpZHMgZm9yIHBsdWdpbiBpbnN0YW5jZXNcbiAgICovXG4gIF91dWlkczogW10sXG5cbiAgLyoqXG4gICAqIFJldHVybnMgYSBib29sZWFuIGZvciBSVEwgc3VwcG9ydFxuICAgKi9cbiAgcnRsOiBmdW5jdGlvbigpe1xuICAgIHJldHVybiAkKCdodG1sJykuYXR0cignZGlyJykgPT09ICdydGwnO1xuICB9LFxuICAvKipcbiAgICogRGVmaW5lcyBhIEZvdW5kYXRpb24gcGx1Z2luLCBhZGRpbmcgaXQgdG8gdGhlIGBGb3VuZGF0aW9uYCBuYW1lc3BhY2UgYW5kIHRoZSBsaXN0IG9mIHBsdWdpbnMgdG8gaW5pdGlhbGl6ZSB3aGVuIHJlZmxvd2luZy5cbiAgICogQHBhcmFtIHtPYmplY3R9IHBsdWdpbiAtIFRoZSBjb25zdHJ1Y3RvciBvZiB0aGUgcGx1Z2luLlxuICAgKi9cbiAgcGx1Z2luOiBmdW5jdGlvbihwbHVnaW4sIG5hbWUpIHtcbiAgICAvLyBPYmplY3Qga2V5IHRvIHVzZSB3aGVuIGFkZGluZyB0byBnbG9iYWwgRm91bmRhdGlvbiBvYmplY3RcbiAgICAvLyBFeGFtcGxlczogRm91bmRhdGlvbi5SZXZlYWwsIEZvdW5kYXRpb24uT2ZmQ2FudmFzXG4gICAgdmFyIGNsYXNzTmFtZSA9IChuYW1lIHx8IGZ1bmN0aW9uTmFtZShwbHVnaW4pKTtcbiAgICAvLyBPYmplY3Qga2V5IHRvIHVzZSB3aGVuIHN0b3JpbmcgdGhlIHBsdWdpbiwgYWxzbyB1c2VkIHRvIGNyZWF0ZSB0aGUgaWRlbnRpZnlpbmcgZGF0YSBhdHRyaWJ1dGUgZm9yIHRoZSBwbHVnaW5cbiAgICAvLyBFeGFtcGxlczogZGF0YS1yZXZlYWwsIGRhdGEtb2ZmLWNhbnZhc1xuICAgIHZhciBhdHRyTmFtZSAgPSBoeXBoZW5hdGUoY2xhc3NOYW1lKTtcblxuICAgIC8vIEFkZCB0byB0aGUgRm91bmRhdGlvbiBvYmplY3QgYW5kIHRoZSBwbHVnaW5zIGxpc3QgKGZvciByZWZsb3dpbmcpXG4gICAgdGhpcy5fcGx1Z2luc1thdHRyTmFtZV0gPSB0aGlzW2NsYXNzTmFtZV0gPSBwbHVnaW47XG4gIH0sXG4gIC8qKlxuICAgKiBAZnVuY3Rpb25cbiAgICogUG9wdWxhdGVzIHRoZSBfdXVpZHMgYXJyYXkgd2l0aCBwb2ludGVycyB0byBlYWNoIGluZGl2aWR1YWwgcGx1Z2luIGluc3RhbmNlLlxuICAgKiBBZGRzIHRoZSBgemZQbHVnaW5gIGRhdGEtYXR0cmlidXRlIHRvIHByb2dyYW1tYXRpY2FsbHkgY3JlYXRlZCBwbHVnaW5zIHRvIGFsbG93IHVzZSBvZiAkKHNlbGVjdG9yKS5mb3VuZGF0aW9uKG1ldGhvZCkgY2FsbHMuXG4gICAqIEFsc28gZmlyZXMgdGhlIGluaXRpYWxpemF0aW9uIGV2ZW50IGZvciBlYWNoIHBsdWdpbiwgY29uc29saWRhdGluZyByZXBldGl0aXZlIGNvZGUuXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBwbHVnaW4gLSBhbiBpbnN0YW5jZSBvZiBhIHBsdWdpbiwgdXN1YWxseSBgdGhpc2AgaW4gY29udGV4dC5cbiAgICogQHBhcmFtIHtTdHJpbmd9IG5hbWUgLSB0aGUgbmFtZSBvZiB0aGUgcGx1Z2luLCBwYXNzZWQgYXMgYSBjYW1lbENhc2VkIHN0cmluZy5cbiAgICogQGZpcmVzIFBsdWdpbiNpbml0XG4gICAqL1xuICByZWdpc3RlclBsdWdpbjogZnVuY3Rpb24ocGx1Z2luLCBuYW1lKXtcbiAgICB2YXIgcGx1Z2luTmFtZSA9IG5hbWUgPyBoeXBoZW5hdGUobmFtZSkgOiBmdW5jdGlvbk5hbWUocGx1Z2luLmNvbnN0cnVjdG9yKS50b0xvd2VyQ2FzZSgpO1xuICAgIHBsdWdpbi51dWlkID0gdGhpcy5HZXRZb0RpZ2l0cyg2LCBwbHVnaW5OYW1lKTtcblxuICAgIGlmKCFwbHVnaW4uJGVsZW1lbnQuYXR0cihgZGF0YS0ke3BsdWdpbk5hbWV9YCkpeyBwbHVnaW4uJGVsZW1lbnQuYXR0cihgZGF0YS0ke3BsdWdpbk5hbWV9YCwgcGx1Z2luLnV1aWQpOyB9XG4gICAgaWYoIXBsdWdpbi4kZWxlbWVudC5kYXRhKCd6ZlBsdWdpbicpKXsgcGx1Z2luLiRlbGVtZW50LmRhdGEoJ3pmUGx1Z2luJywgcGx1Z2luKTsgfVxuICAgICAgICAgIC8qKlxuICAgICAgICAgICAqIEZpcmVzIHdoZW4gdGhlIHBsdWdpbiBoYXMgaW5pdGlhbGl6ZWQuXG4gICAgICAgICAgICogQGV2ZW50IFBsdWdpbiNpbml0XG4gICAgICAgICAgICovXG4gICAgcGx1Z2luLiRlbGVtZW50LnRyaWdnZXIoYGluaXQuemYuJHtwbHVnaW5OYW1lfWApO1xuXG4gICAgdGhpcy5fdXVpZHMucHVzaChwbHVnaW4udXVpZCk7XG5cbiAgICByZXR1cm47XG4gIH0sXG4gIC8qKlxuICAgKiBAZnVuY3Rpb25cbiAgICogUmVtb3ZlcyB0aGUgcGx1Z2lucyB1dWlkIGZyb20gdGhlIF91dWlkcyBhcnJheS5cbiAgICogUmVtb3ZlcyB0aGUgemZQbHVnaW4gZGF0YSBhdHRyaWJ1dGUsIGFzIHdlbGwgYXMgdGhlIGRhdGEtcGx1Z2luLW5hbWUgYXR0cmlidXRlLlxuICAgKiBBbHNvIGZpcmVzIHRoZSBkZXN0cm95ZWQgZXZlbnQgZm9yIHRoZSBwbHVnaW4sIGNvbnNvbGlkYXRpbmcgcmVwZXRpdGl2ZSBjb2RlLlxuICAgKiBAcGFyYW0ge09iamVjdH0gcGx1Z2luIC0gYW4gaW5zdGFuY2Ugb2YgYSBwbHVnaW4sIHVzdWFsbHkgYHRoaXNgIGluIGNvbnRleHQuXG4gICAqIEBmaXJlcyBQbHVnaW4jZGVzdHJveWVkXG4gICAqL1xuICB1bnJlZ2lzdGVyUGx1Z2luOiBmdW5jdGlvbihwbHVnaW4pe1xuICAgIHZhciBwbHVnaW5OYW1lID0gaHlwaGVuYXRlKGZ1bmN0aW9uTmFtZShwbHVnaW4uJGVsZW1lbnQuZGF0YSgnemZQbHVnaW4nKS5jb25zdHJ1Y3RvcikpO1xuXG4gICAgdGhpcy5fdXVpZHMuc3BsaWNlKHRoaXMuX3V1aWRzLmluZGV4T2YocGx1Z2luLnV1aWQpLCAxKTtcbiAgICBwbHVnaW4uJGVsZW1lbnQucmVtb3ZlQXR0cihgZGF0YS0ke3BsdWdpbk5hbWV9YCkucmVtb3ZlRGF0YSgnemZQbHVnaW4nKVxuICAgICAgICAgIC8qKlxuICAgICAgICAgICAqIEZpcmVzIHdoZW4gdGhlIHBsdWdpbiBoYXMgYmVlbiBkZXN0cm95ZWQuXG4gICAgICAgICAgICogQGV2ZW50IFBsdWdpbiNkZXN0cm95ZWRcbiAgICAgICAgICAgKi9cbiAgICAgICAgICAudHJpZ2dlcihgZGVzdHJveWVkLnpmLiR7cGx1Z2luTmFtZX1gKTtcbiAgICBmb3IodmFyIHByb3AgaW4gcGx1Z2luKXtcbiAgICAgIHBsdWdpbltwcm9wXSA9IG51bGw7Ly9jbGVhbiB1cCBzY3JpcHQgdG8gcHJlcCBmb3IgZ2FyYmFnZSBjb2xsZWN0aW9uLlxuICAgIH1cbiAgICByZXR1cm47XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBmdW5jdGlvblxuICAgKiBDYXVzZXMgb25lIG9yIG1vcmUgYWN0aXZlIHBsdWdpbnMgdG8gcmUtaW5pdGlhbGl6ZSwgcmVzZXR0aW5nIGV2ZW50IGxpc3RlbmVycywgcmVjYWxjdWxhdGluZyBwb3NpdGlvbnMsIGV0Yy5cbiAgICogQHBhcmFtIHtTdHJpbmd9IHBsdWdpbnMgLSBvcHRpb25hbCBzdHJpbmcgb2YgYW4gaW5kaXZpZHVhbCBwbHVnaW4ga2V5LCBhdHRhaW5lZCBieSBjYWxsaW5nIGAkKGVsZW1lbnQpLmRhdGEoJ3BsdWdpbk5hbWUnKWAsIG9yIHN0cmluZyBvZiBhIHBsdWdpbiBjbGFzcyBpLmUuIGAnZHJvcGRvd24nYFxuICAgKiBAZGVmYXVsdCBJZiBubyBhcmd1bWVudCBpcyBwYXNzZWQsIHJlZmxvdyBhbGwgY3VycmVudGx5IGFjdGl2ZSBwbHVnaW5zLlxuICAgKi9cbiAgIHJlSW5pdDogZnVuY3Rpb24ocGx1Z2lucyl7XG4gICAgIHZhciBpc0pRID0gcGx1Z2lucyBpbnN0YW5jZW9mICQ7XG4gICAgIHRyeXtcbiAgICAgICBpZihpc0pRKXtcbiAgICAgICAgIHBsdWdpbnMuZWFjaChmdW5jdGlvbigpe1xuICAgICAgICAgICAkKHRoaXMpLmRhdGEoJ3pmUGx1Z2luJykuX2luaXQoKTtcbiAgICAgICAgIH0pO1xuICAgICAgIH1lbHNle1xuICAgICAgICAgdmFyIHR5cGUgPSB0eXBlb2YgcGx1Z2lucyxcbiAgICAgICAgIF90aGlzID0gdGhpcyxcbiAgICAgICAgIGZucyA9IHtcbiAgICAgICAgICAgJ29iamVjdCc6IGZ1bmN0aW9uKHBsZ3Mpe1xuICAgICAgICAgICAgIHBsZ3MuZm9yRWFjaChmdW5jdGlvbihwKXtcbiAgICAgICAgICAgICAgIHAgPSBoeXBoZW5hdGUocCk7XG4gICAgICAgICAgICAgICAkKCdbZGF0YS0nKyBwICsnXScpLmZvdW5kYXRpb24oJ19pbml0Jyk7XG4gICAgICAgICAgICAgfSk7XG4gICAgICAgICAgIH0sXG4gICAgICAgICAgICdzdHJpbmcnOiBmdW5jdGlvbigpe1xuICAgICAgICAgICAgIHBsdWdpbnMgPSBoeXBoZW5hdGUocGx1Z2lucyk7XG4gICAgICAgICAgICAgJCgnW2RhdGEtJysgcGx1Z2lucyArJ10nKS5mb3VuZGF0aW9uKCdfaW5pdCcpO1xuICAgICAgICAgICB9LFxuICAgICAgICAgICAndW5kZWZpbmVkJzogZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICB0aGlzWydvYmplY3QnXShPYmplY3Qua2V5cyhfdGhpcy5fcGx1Z2lucykpO1xuICAgICAgICAgICB9XG4gICAgICAgICB9O1xuICAgICAgICAgZm5zW3R5cGVdKHBsdWdpbnMpO1xuICAgICAgIH1cbiAgICAgfWNhdGNoKGVycil7XG4gICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xuICAgICB9ZmluYWxseXtcbiAgICAgICByZXR1cm4gcGx1Z2lucztcbiAgICAgfVxuICAgfSxcblxuICAvKipcbiAgICogcmV0dXJucyBhIHJhbmRvbSBiYXNlLTM2IHVpZCB3aXRoIG5hbWVzcGFjaW5nXG4gICAqIEBmdW5jdGlvblxuICAgKiBAcGFyYW0ge051bWJlcn0gbGVuZ3RoIC0gbnVtYmVyIG9mIHJhbmRvbSBiYXNlLTM2IGRpZ2l0cyBkZXNpcmVkLiBJbmNyZWFzZSBmb3IgbW9yZSByYW5kb20gc3RyaW5ncy5cbiAgICogQHBhcmFtIHtTdHJpbmd9IG5hbWVzcGFjZSAtIG5hbWUgb2YgcGx1Z2luIHRvIGJlIGluY29ycG9yYXRlZCBpbiB1aWQsIG9wdGlvbmFsLlxuICAgKiBAZGVmYXVsdCB7U3RyaW5nfSAnJyAtIGlmIG5vIHBsdWdpbiBuYW1lIGlzIHByb3ZpZGVkLCBub3RoaW5nIGlzIGFwcGVuZGVkIHRvIHRoZSB1aWQuXG4gICAqIEByZXR1cm5zIHtTdHJpbmd9IC0gdW5pcXVlIGlkXG4gICAqL1xuICBHZXRZb0RpZ2l0czogZnVuY3Rpb24obGVuZ3RoLCBuYW1lc3BhY2Upe1xuICAgIGxlbmd0aCA9IGxlbmd0aCB8fCA2O1xuICAgIHJldHVybiBNYXRoLnJvdW5kKChNYXRoLnBvdygzNiwgbGVuZ3RoICsgMSkgLSBNYXRoLnJhbmRvbSgpICogTWF0aC5wb3coMzYsIGxlbmd0aCkpKS50b1N0cmluZygzNikuc2xpY2UoMSkgKyAobmFtZXNwYWNlID8gYC0ke25hbWVzcGFjZX1gIDogJycpO1xuICB9LFxuICAvKipcbiAgICogSW5pdGlhbGl6ZSBwbHVnaW5zIG9uIGFueSBlbGVtZW50cyB3aXRoaW4gYGVsZW1gIChhbmQgYGVsZW1gIGl0c2VsZikgdGhhdCBhcmVuJ3QgYWxyZWFkeSBpbml0aWFsaXplZC5cbiAgICogQHBhcmFtIHtPYmplY3R9IGVsZW0gLSBqUXVlcnkgb2JqZWN0IGNvbnRhaW5pbmcgdGhlIGVsZW1lbnQgdG8gY2hlY2sgaW5zaWRlLiBBbHNvIGNoZWNrcyB0aGUgZWxlbWVudCBpdHNlbGYsIHVubGVzcyBpdCdzIHRoZSBgZG9jdW1lbnRgIG9iamVjdC5cbiAgICogQHBhcmFtIHtTdHJpbmd8QXJyYXl9IHBsdWdpbnMgLSBBIGxpc3Qgb2YgcGx1Z2lucyB0byBpbml0aWFsaXplLiBMZWF2ZSB0aGlzIG91dCB0byBpbml0aWFsaXplIGV2ZXJ5dGhpbmcuXG4gICAqL1xuICByZWZsb3c6IGZ1bmN0aW9uKGVsZW0sIHBsdWdpbnMpIHtcblxuICAgIC8vIElmIHBsdWdpbnMgaXMgdW5kZWZpbmVkLCBqdXN0IGdyYWIgZXZlcnl0aGluZ1xuICAgIGlmICh0eXBlb2YgcGx1Z2lucyA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHBsdWdpbnMgPSBPYmplY3Qua2V5cyh0aGlzLl9wbHVnaW5zKTtcbiAgICB9XG4gICAgLy8gSWYgcGx1Z2lucyBpcyBhIHN0cmluZywgY29udmVydCBpdCB0byBhbiBhcnJheSB3aXRoIG9uZSBpdGVtXG4gICAgZWxzZSBpZiAodHlwZW9mIHBsdWdpbnMgPT09ICdzdHJpbmcnKSB7XG4gICAgICBwbHVnaW5zID0gW3BsdWdpbnNdO1xuICAgIH1cblxuICAgIHZhciBfdGhpcyA9IHRoaXM7XG5cbiAgICAvLyBJdGVyYXRlIHRocm91Z2ggZWFjaCBwbHVnaW5cbiAgICAkLmVhY2gocGx1Z2lucywgZnVuY3Rpb24oaSwgbmFtZSkge1xuICAgICAgLy8gR2V0IHRoZSBjdXJyZW50IHBsdWdpblxuICAgICAgdmFyIHBsdWdpbiA9IF90aGlzLl9wbHVnaW5zW25hbWVdO1xuXG4gICAgICAvLyBMb2NhbGl6ZSB0aGUgc2VhcmNoIHRvIGFsbCBlbGVtZW50cyBpbnNpZGUgZWxlbSwgYXMgd2VsbCBhcyBlbGVtIGl0c2VsZiwgdW5sZXNzIGVsZW0gPT09IGRvY3VtZW50XG4gICAgICB2YXIgJGVsZW0gPSAkKGVsZW0pLmZpbmQoJ1tkYXRhLScrbmFtZSsnXScpLmFkZEJhY2soJ1tkYXRhLScrbmFtZSsnXScpO1xuXG4gICAgICAvLyBGb3IgZWFjaCBwbHVnaW4gZm91bmQsIGluaXRpYWxpemUgaXRcbiAgICAgICRlbGVtLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciAkZWwgPSAkKHRoaXMpLFxuICAgICAgICAgICAgb3B0cyA9IHt9O1xuICAgICAgICAvLyBEb24ndCBkb3VibGUtZGlwIG9uIHBsdWdpbnNcbiAgICAgICAgaWYgKCRlbC5kYXRhKCd6ZlBsdWdpbicpKSB7XG4gICAgICAgICAgY29uc29sZS53YXJuKFwiVHJpZWQgdG8gaW5pdGlhbGl6ZSBcIituYW1lK1wiIG9uIGFuIGVsZW1lbnQgdGhhdCBhbHJlYWR5IGhhcyBhIEZvdW5kYXRpb24gcGx1Z2luLlwiKTtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZigkZWwuYXR0cignZGF0YS1vcHRpb25zJykpe1xuICAgICAgICAgIHZhciB0aGluZyA9ICRlbC5hdHRyKCdkYXRhLW9wdGlvbnMnKS5zcGxpdCgnOycpLmZvckVhY2goZnVuY3Rpb24oZSwgaSl7XG4gICAgICAgICAgICB2YXIgb3B0ID0gZS5zcGxpdCgnOicpLm1hcChmdW5jdGlvbihlbCl7IHJldHVybiBlbC50cmltKCk7IH0pO1xuICAgICAgICAgICAgaWYob3B0WzBdKSBvcHRzW29wdFswXV0gPSBwYXJzZVZhbHVlKG9wdFsxXSk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgdHJ5e1xuICAgICAgICAgICRlbC5kYXRhKCd6ZlBsdWdpbicsIG5ldyBwbHVnaW4oJCh0aGlzKSwgb3B0cykpO1xuICAgICAgICB9Y2F0Y2goZXIpe1xuICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXIpO1xuICAgICAgICB9ZmluYWxseXtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0pO1xuICB9LFxuICBnZXRGbk5hbWU6IGZ1bmN0aW9uTmFtZSxcbiAgdHJhbnNpdGlvbmVuZDogZnVuY3Rpb24oJGVsZW0pe1xuICAgIHZhciB0cmFuc2l0aW9ucyA9IHtcbiAgICAgICd0cmFuc2l0aW9uJzogJ3RyYW5zaXRpb25lbmQnLFxuICAgICAgJ1dlYmtpdFRyYW5zaXRpb24nOiAnd2Via2l0VHJhbnNpdGlvbkVuZCcsXG4gICAgICAnTW96VHJhbnNpdGlvbic6ICd0cmFuc2l0aW9uZW5kJyxcbiAgICAgICdPVHJhbnNpdGlvbic6ICdvdHJhbnNpdGlvbmVuZCdcbiAgICB9O1xuICAgIHZhciBlbGVtID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JyksXG4gICAgICAgIGVuZDtcblxuICAgIGZvciAodmFyIHQgaW4gdHJhbnNpdGlvbnMpe1xuICAgICAgaWYgKHR5cGVvZiBlbGVtLnN0eWxlW3RdICE9PSAndW5kZWZpbmVkJyl7XG4gICAgICAgIGVuZCA9IHRyYW5zaXRpb25zW3RdO1xuICAgICAgfVxuICAgIH1cbiAgICBpZihlbmQpe1xuICAgICAgcmV0dXJuIGVuZDtcbiAgICB9ZWxzZXtcbiAgICAgIGVuZCA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICAgICAgJGVsZW0udHJpZ2dlckhhbmRsZXIoJ3RyYW5zaXRpb25lbmQnLCBbJGVsZW1dKTtcbiAgICAgIH0sIDEpO1xuICAgICAgcmV0dXJuICd0cmFuc2l0aW9uZW5kJztcbiAgICB9XG4gIH1cbn07XG5cbkZvdW5kYXRpb24udXRpbCA9IHtcbiAgLyoqXG4gICAqIEZ1bmN0aW9uIGZvciBhcHBseWluZyBhIGRlYm91bmNlIGVmZmVjdCB0byBhIGZ1bmN0aW9uIGNhbGwuXG4gICAqIEBmdW5jdGlvblxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBmdW5jIC0gRnVuY3Rpb24gdG8gYmUgY2FsbGVkIGF0IGVuZCBvZiB0aW1lb3V0LlxuICAgKiBAcGFyYW0ge051bWJlcn0gZGVsYXkgLSBUaW1lIGluIG1zIHRvIGRlbGF5IHRoZSBjYWxsIG9mIGBmdW5jYC5cbiAgICogQHJldHVybnMgZnVuY3Rpb25cbiAgICovXG4gIHRocm90dGxlOiBmdW5jdGlvbiAoZnVuYywgZGVsYXkpIHtcbiAgICB2YXIgdGltZXIgPSBudWxsO1xuXG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBjb250ZXh0ID0gdGhpcywgYXJncyA9IGFyZ3VtZW50cztcblxuICAgICAgaWYgKHRpbWVyID09PSBudWxsKSB7XG4gICAgICAgIHRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgZnVuYy5hcHBseShjb250ZXh0LCBhcmdzKTtcbiAgICAgICAgICB0aW1lciA9IG51bGw7XG4gICAgICAgIH0sIGRlbGF5KTtcbiAgICAgIH1cbiAgICB9O1xuICB9XG59O1xuXG4vLyBUT0RPOiBjb25zaWRlciBub3QgbWFraW5nIHRoaXMgYSBqUXVlcnkgZnVuY3Rpb25cbi8vIFRPRE86IG5lZWQgd2F5IHRvIHJlZmxvdyB2cy4gcmUtaW5pdGlhbGl6ZVxuLyoqXG4gKiBUaGUgRm91bmRhdGlvbiBqUXVlcnkgbWV0aG9kLlxuICogQHBhcmFtIHtTdHJpbmd8QXJyYXl9IG1ldGhvZCAtIEFuIGFjdGlvbiB0byBwZXJmb3JtIG9uIHRoZSBjdXJyZW50IGpRdWVyeSBvYmplY3QuXG4gKi9cbnZhciBmb3VuZGF0aW9uID0gZnVuY3Rpb24obWV0aG9kKSB7XG4gIHZhciB0eXBlID0gdHlwZW9mIG1ldGhvZCxcbiAgICAgICRtZXRhID0gJCgnbWV0YS5mb3VuZGF0aW9uLW1xJyksXG4gICAgICAkbm9KUyA9ICQoJy5uby1qcycpO1xuXG4gIGlmKCEkbWV0YS5sZW5ndGgpe1xuICAgICQoJzxtZXRhIGNsYXNzPVwiZm91bmRhdGlvbi1tcVwiPicpLmFwcGVuZFRvKGRvY3VtZW50LmhlYWQpO1xuICB9XG4gIGlmKCRub0pTLmxlbmd0aCl7XG4gICAgJG5vSlMucmVtb3ZlQ2xhc3MoJ25vLWpzJyk7XG4gIH1cblxuICBpZih0eXBlID09PSAndW5kZWZpbmVkJyl7Ly9uZWVkcyB0byBpbml0aWFsaXplIHRoZSBGb3VuZGF0aW9uIG9iamVjdCwgb3IgYW4gaW5kaXZpZHVhbCBwbHVnaW4uXG4gICAgRm91bmRhdGlvbi5NZWRpYVF1ZXJ5Ll9pbml0KCk7XG4gICAgRm91bmRhdGlvbi5yZWZsb3codGhpcyk7XG4gIH1lbHNlIGlmKHR5cGUgPT09ICdzdHJpbmcnKXsvL2FuIGluZGl2aWR1YWwgbWV0aG9kIHRvIGludm9rZSBvbiBhIHBsdWdpbiBvciBncm91cCBvZiBwbHVnaW5zXG4gICAgdmFyIGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpOy8vY29sbGVjdCBhbGwgdGhlIGFyZ3VtZW50cywgaWYgbmVjZXNzYXJ5XG4gICAgdmFyIHBsdWdDbGFzcyA9IHRoaXMuZGF0YSgnemZQbHVnaW4nKTsvL2RldGVybWluZSB0aGUgY2xhc3Mgb2YgcGx1Z2luXG5cbiAgICBpZihwbHVnQ2xhc3MgIT09IHVuZGVmaW5lZCAmJiBwbHVnQ2xhc3NbbWV0aG9kXSAhPT0gdW5kZWZpbmVkKXsvL21ha2Ugc3VyZSBib3RoIHRoZSBjbGFzcyBhbmQgbWV0aG9kIGV4aXN0XG4gICAgICBpZih0aGlzLmxlbmd0aCA9PT0gMSl7Ly9pZiB0aGVyZSdzIG9ubHkgb25lLCBjYWxsIGl0IGRpcmVjdGx5LlxuICAgICAgICAgIHBsdWdDbGFzc1ttZXRob2RdLmFwcGx5KHBsdWdDbGFzcywgYXJncyk7XG4gICAgICB9ZWxzZXtcbiAgICAgICAgdGhpcy5lYWNoKGZ1bmN0aW9uKGksIGVsKXsvL290aGVyd2lzZSBsb29wIHRocm91Z2ggdGhlIGpRdWVyeSBjb2xsZWN0aW9uIGFuZCBpbnZva2UgdGhlIG1ldGhvZCBvbiBlYWNoXG4gICAgICAgICAgcGx1Z0NsYXNzW21ldGhvZF0uYXBwbHkoJChlbCkuZGF0YSgnemZQbHVnaW4nKSwgYXJncyk7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH1lbHNley8vZXJyb3IgZm9yIG5vIGNsYXNzIG9yIG5vIG1ldGhvZFxuICAgICAgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwiV2UncmUgc29ycnksICdcIiArIG1ldGhvZCArIFwiJyBpcyBub3QgYW4gYXZhaWxhYmxlIG1ldGhvZCBmb3IgXCIgKyAocGx1Z0NsYXNzID8gZnVuY3Rpb25OYW1lKHBsdWdDbGFzcykgOiAndGhpcyBlbGVtZW50JykgKyAnLicpO1xuICAgIH1cbiAgfWVsc2V7Ly9lcnJvciBmb3IgaW52YWxpZCBhcmd1bWVudCB0eXBlXG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcihgV2UncmUgc29ycnksICR7dHlwZX0gaXMgbm90IGEgdmFsaWQgcGFyYW1ldGVyLiBZb3UgbXVzdCB1c2UgYSBzdHJpbmcgcmVwcmVzZW50aW5nIHRoZSBtZXRob2QgeW91IHdpc2ggdG8gaW52b2tlLmApO1xuICB9XG4gIHJldHVybiB0aGlzO1xufTtcblxud2luZG93LkZvdW5kYXRpb24gPSBGb3VuZGF0aW9uO1xuJC5mbi5mb3VuZGF0aW9uID0gZm91bmRhdGlvbjtcblxuLy8gUG9seWZpbGwgZm9yIHJlcXVlc3RBbmltYXRpb25GcmFtZVxuKGZ1bmN0aW9uKCkge1xuICBpZiAoIURhdGUubm93IHx8ICF3aW5kb3cuRGF0ZS5ub3cpXG4gICAgd2luZG93LkRhdGUubm93ID0gRGF0ZS5ub3cgPSBmdW5jdGlvbigpIHsgcmV0dXJuIG5ldyBEYXRlKCkuZ2V0VGltZSgpOyB9O1xuXG4gIHZhciB2ZW5kb3JzID0gWyd3ZWJraXQnLCAnbW96J107XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgdmVuZG9ycy5sZW5ndGggJiYgIXdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWU7ICsraSkge1xuICAgICAgdmFyIHZwID0gdmVuZG9yc1tpXTtcbiAgICAgIHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUgPSB3aW5kb3dbdnArJ1JlcXVlc3RBbmltYXRpb25GcmFtZSddO1xuICAgICAgd2luZG93LmNhbmNlbEFuaW1hdGlvbkZyYW1lID0gKHdpbmRvd1t2cCsnQ2FuY2VsQW5pbWF0aW9uRnJhbWUnXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfHwgd2luZG93W3ZwKydDYW5jZWxSZXF1ZXN0QW5pbWF0aW9uRnJhbWUnXSk7XG4gIH1cbiAgaWYgKC9pUChhZHxob25lfG9kKS4qT1MgNi8udGVzdCh3aW5kb3cubmF2aWdhdG9yLnVzZXJBZ2VudClcbiAgICB8fCAhd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSB8fCAhd2luZG93LmNhbmNlbEFuaW1hdGlvbkZyYW1lKSB7XG4gICAgdmFyIGxhc3RUaW1lID0gMDtcbiAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lID0gZnVuY3Rpb24oY2FsbGJhY2spIHtcbiAgICAgICAgdmFyIG5vdyA9IERhdGUubm93KCk7XG4gICAgICAgIHZhciBuZXh0VGltZSA9IE1hdGgubWF4KGxhc3RUaW1lICsgMTYsIG5vdyk7XG4gICAgICAgIHJldHVybiBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkgeyBjYWxsYmFjayhsYXN0VGltZSA9IG5leHRUaW1lKTsgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgbmV4dFRpbWUgLSBub3cpO1xuICAgIH07XG4gICAgd2luZG93LmNhbmNlbEFuaW1hdGlvbkZyYW1lID0gY2xlYXJUaW1lb3V0O1xuICB9XG4gIC8qKlxuICAgKiBQb2x5ZmlsbCBmb3IgcGVyZm9ybWFuY2Uubm93LCByZXF1aXJlZCBieSByQUZcbiAgICovXG4gIGlmKCF3aW5kb3cucGVyZm9ybWFuY2UgfHwgIXdpbmRvdy5wZXJmb3JtYW5jZS5ub3cpe1xuICAgIHdpbmRvdy5wZXJmb3JtYW5jZSA9IHtcbiAgICAgIHN0YXJ0OiBEYXRlLm5vdygpLFxuICAgICAgbm93OiBmdW5jdGlvbigpeyByZXR1cm4gRGF0ZS5ub3coKSAtIHRoaXMuc3RhcnQ7IH1cbiAgICB9O1xuICB9XG59KSgpO1xuaWYgKCFGdW5jdGlvbi5wcm90b3R5cGUuYmluZCkge1xuICBGdW5jdGlvbi5wcm90b3R5cGUuYmluZCA9IGZ1bmN0aW9uKG9UaGlzKSB7XG4gICAgaWYgKHR5cGVvZiB0aGlzICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgICAvLyBjbG9zZXN0IHRoaW5nIHBvc3NpYmxlIHRvIHRoZSBFQ01BU2NyaXB0IDVcbiAgICAgIC8vIGludGVybmFsIElzQ2FsbGFibGUgZnVuY3Rpb25cbiAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0Z1bmN0aW9uLnByb3RvdHlwZS5iaW5kIC0gd2hhdCBpcyB0cnlpbmcgdG8gYmUgYm91bmQgaXMgbm90IGNhbGxhYmxlJyk7XG4gICAgfVxuXG4gICAgdmFyIGFBcmdzICAgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpLFxuICAgICAgICBmVG9CaW5kID0gdGhpcyxcbiAgICAgICAgZk5PUCAgICA9IGZ1bmN0aW9uKCkge30sXG4gICAgICAgIGZCb3VuZCAgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICByZXR1cm4gZlRvQmluZC5hcHBseSh0aGlzIGluc3RhbmNlb2YgZk5PUFxuICAgICAgICAgICAgICAgICA/IHRoaXNcbiAgICAgICAgICAgICAgICAgOiBvVGhpcyxcbiAgICAgICAgICAgICAgICAgYUFyZ3MuY29uY2F0KEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cykpKTtcbiAgICAgICAgfTtcblxuICAgIGlmICh0aGlzLnByb3RvdHlwZSkge1xuICAgICAgLy8gbmF0aXZlIGZ1bmN0aW9ucyBkb24ndCBoYXZlIGEgcHJvdG90eXBlXG4gICAgICBmTk9QLnByb3RvdHlwZSA9IHRoaXMucHJvdG90eXBlO1xuICAgIH1cbiAgICBmQm91bmQucHJvdG90eXBlID0gbmV3IGZOT1AoKTtcblxuICAgIHJldHVybiBmQm91bmQ7XG4gIH07XG59XG4vLyBQb2x5ZmlsbCB0byBnZXQgdGhlIG5hbWUgb2YgYSBmdW5jdGlvbiBpbiBJRTlcbmZ1bmN0aW9uIGZ1bmN0aW9uTmFtZShmbikge1xuICBpZiAoRnVuY3Rpb24ucHJvdG90eXBlLm5hbWUgPT09IHVuZGVmaW5lZCkge1xuICAgIHZhciBmdW5jTmFtZVJlZ2V4ID0gL2Z1bmN0aW9uXFxzKFteKF17MSx9KVxcKC87XG4gICAgdmFyIHJlc3VsdHMgPSAoZnVuY05hbWVSZWdleCkuZXhlYygoZm4pLnRvU3RyaW5nKCkpO1xuICAgIHJldHVybiAocmVzdWx0cyAmJiByZXN1bHRzLmxlbmd0aCA+IDEpID8gcmVzdWx0c1sxXS50cmltKCkgOiBcIlwiO1xuICB9XG4gIGVsc2UgaWYgKGZuLnByb3RvdHlwZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgcmV0dXJuIGZuLmNvbnN0cnVjdG9yLm5hbWU7XG4gIH1cbiAgZWxzZSB7XG4gICAgcmV0dXJuIGZuLnByb3RvdHlwZS5jb25zdHJ1Y3Rvci5uYW1lO1xuICB9XG59XG5mdW5jdGlvbiBwYXJzZVZhbHVlKHN0cil7XG4gIGlmKC90cnVlLy50ZXN0KHN0cikpIHJldHVybiB0cnVlO1xuICBlbHNlIGlmKC9mYWxzZS8udGVzdChzdHIpKSByZXR1cm4gZmFsc2U7XG4gIGVsc2UgaWYoIWlzTmFOKHN0ciAqIDEpKSByZXR1cm4gcGFyc2VGbG9hdChzdHIpO1xuICByZXR1cm4gc3RyO1xufVxuLy8gQ29udmVydCBQYXNjYWxDYXNlIHRvIGtlYmFiLWNhc2Vcbi8vIFRoYW5rIHlvdTogaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL2EvODk1NTU4MFxuZnVuY3Rpb24gaHlwaGVuYXRlKHN0cikge1xuICByZXR1cm4gc3RyLnJlcGxhY2UoLyhbYS16XSkoW0EtWl0pL2csICckMS0kMicpLnRvTG93ZXJDYXNlKCk7XG59XG5cbn0oalF1ZXJ5KTtcbiIsIid1c2Ugc3RyaWN0JztcblxuIWZ1bmN0aW9uKCQpIHtcblxuRm91bmRhdGlvbi5Cb3ggPSB7XG4gIEltTm90VG91Y2hpbmdZb3U6IEltTm90VG91Y2hpbmdZb3UsXG4gIEdldERpbWVuc2lvbnM6IEdldERpbWVuc2lvbnMsXG4gIEdldE9mZnNldHM6IEdldE9mZnNldHNcbn1cblxuLyoqXG4gKiBDb21wYXJlcyB0aGUgZGltZW5zaW9ucyBvZiBhbiBlbGVtZW50IHRvIGEgY29udGFpbmVyIGFuZCBkZXRlcm1pbmVzIGNvbGxpc2lvbiBldmVudHMgd2l0aCBjb250YWluZXIuXG4gKiBAZnVuY3Rpb25cbiAqIEBwYXJhbSB7alF1ZXJ5fSBlbGVtZW50IC0galF1ZXJ5IG9iamVjdCB0byB0ZXN0IGZvciBjb2xsaXNpb25zLlxuICogQHBhcmFtIHtqUXVlcnl9IHBhcmVudCAtIGpRdWVyeSBvYmplY3QgdG8gdXNlIGFzIGJvdW5kaW5nIGNvbnRhaW5lci5cbiAqIEBwYXJhbSB7Qm9vbGVhbn0gbHJPbmx5IC0gc2V0IHRvIHRydWUgdG8gY2hlY2sgbGVmdCBhbmQgcmlnaHQgdmFsdWVzIG9ubHkuXG4gKiBAcGFyYW0ge0Jvb2xlYW59IHRiT25seSAtIHNldCB0byB0cnVlIHRvIGNoZWNrIHRvcCBhbmQgYm90dG9tIHZhbHVlcyBvbmx5LlxuICogQGRlZmF1bHQgaWYgbm8gcGFyZW50IG9iamVjdCBwYXNzZWQsIGRldGVjdHMgY29sbGlzaW9ucyB3aXRoIGB3aW5kb3dgLlxuICogQHJldHVybnMge0Jvb2xlYW59IC0gdHJ1ZSBpZiBjb2xsaXNpb24gZnJlZSwgZmFsc2UgaWYgYSBjb2xsaXNpb24gaW4gYW55IGRpcmVjdGlvbi5cbiAqL1xuZnVuY3Rpb24gSW1Ob3RUb3VjaGluZ1lvdShlbGVtZW50LCBwYXJlbnQsIGxyT25seSwgdGJPbmx5KSB7XG4gIHZhciBlbGVEaW1zID0gR2V0RGltZW5zaW9ucyhlbGVtZW50KSxcbiAgICAgIHRvcCwgYm90dG9tLCBsZWZ0LCByaWdodDtcblxuICBpZiAocGFyZW50KSB7XG4gICAgdmFyIHBhckRpbXMgPSBHZXREaW1lbnNpb25zKHBhcmVudCk7XG5cbiAgICBib3R0b20gPSAoZWxlRGltcy5vZmZzZXQudG9wICsgZWxlRGltcy5oZWlnaHQgPD0gcGFyRGltcy5oZWlnaHQgKyBwYXJEaW1zLm9mZnNldC50b3ApO1xuICAgIHRvcCAgICA9IChlbGVEaW1zLm9mZnNldC50b3AgPj0gcGFyRGltcy5vZmZzZXQudG9wKTtcbiAgICBsZWZ0ICAgPSAoZWxlRGltcy5vZmZzZXQubGVmdCA+PSBwYXJEaW1zLm9mZnNldC5sZWZ0KTtcbiAgICByaWdodCAgPSAoZWxlRGltcy5vZmZzZXQubGVmdCArIGVsZURpbXMud2lkdGggPD0gcGFyRGltcy53aWR0aCArIHBhckRpbXMub2Zmc2V0LmxlZnQpO1xuICB9XG4gIGVsc2Uge1xuICAgIGJvdHRvbSA9IChlbGVEaW1zLm9mZnNldC50b3AgKyBlbGVEaW1zLmhlaWdodCA8PSBlbGVEaW1zLndpbmRvd0RpbXMuaGVpZ2h0ICsgZWxlRGltcy53aW5kb3dEaW1zLm9mZnNldC50b3ApO1xuICAgIHRvcCAgICA9IChlbGVEaW1zLm9mZnNldC50b3AgPj0gZWxlRGltcy53aW5kb3dEaW1zLm9mZnNldC50b3ApO1xuICAgIGxlZnQgICA9IChlbGVEaW1zLm9mZnNldC5sZWZ0ID49IGVsZURpbXMud2luZG93RGltcy5vZmZzZXQubGVmdCk7XG4gICAgcmlnaHQgID0gKGVsZURpbXMub2Zmc2V0LmxlZnQgKyBlbGVEaW1zLndpZHRoIDw9IGVsZURpbXMud2luZG93RGltcy53aWR0aCk7XG4gIH1cblxuICB2YXIgYWxsRGlycyA9IFtib3R0b20sIHRvcCwgbGVmdCwgcmlnaHRdO1xuXG4gIGlmIChsck9ubHkpIHtcbiAgICByZXR1cm4gbGVmdCA9PT0gcmlnaHQgPT09IHRydWU7XG4gIH1cblxuICBpZiAodGJPbmx5KSB7XG4gICAgcmV0dXJuIHRvcCA9PT0gYm90dG9tID09PSB0cnVlO1xuICB9XG5cbiAgcmV0dXJuIGFsbERpcnMuaW5kZXhPZihmYWxzZSkgPT09IC0xO1xufTtcblxuLyoqXG4gKiBVc2VzIG5hdGl2ZSBtZXRob2RzIHRvIHJldHVybiBhbiBvYmplY3Qgb2YgZGltZW5zaW9uIHZhbHVlcy5cbiAqIEBmdW5jdGlvblxuICogQHBhcmFtIHtqUXVlcnkgfHwgSFRNTH0gZWxlbWVudCAtIGpRdWVyeSBvYmplY3Qgb3IgRE9NIGVsZW1lbnQgZm9yIHdoaWNoIHRvIGdldCB0aGUgZGltZW5zaW9ucy4gQ2FuIGJlIGFueSBlbGVtZW50IG90aGVyIHRoYXQgZG9jdW1lbnQgb3Igd2luZG93LlxuICogQHJldHVybnMge09iamVjdH0gLSBuZXN0ZWQgb2JqZWN0IG9mIGludGVnZXIgcGl4ZWwgdmFsdWVzXG4gKiBUT0RPIC0gaWYgZWxlbWVudCBpcyB3aW5kb3csIHJldHVybiBvbmx5IHRob3NlIHZhbHVlcy5cbiAqL1xuZnVuY3Rpb24gR2V0RGltZW5zaW9ucyhlbGVtLCB0ZXN0KXtcbiAgZWxlbSA9IGVsZW0ubGVuZ3RoID8gZWxlbVswXSA6IGVsZW07XG5cbiAgaWYgKGVsZW0gPT09IHdpbmRvdyB8fCBlbGVtID09PSBkb2N1bWVudCkge1xuICAgIHRocm93IG5ldyBFcnJvcihcIkknbSBzb3JyeSwgRGF2ZS4gSSdtIGFmcmFpZCBJIGNhbid0IGRvIHRoYXQuXCIpO1xuICB9XG5cbiAgdmFyIHJlY3QgPSBlbGVtLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLFxuICAgICAgcGFyUmVjdCA9IGVsZW0ucGFyZW50Tm9kZS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSxcbiAgICAgIHdpblJlY3QgPSBkb2N1bWVudC5ib2R5LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLFxuICAgICAgd2luWSA9IHdpbmRvdy5wYWdlWU9mZnNldCxcbiAgICAgIHdpblggPSB3aW5kb3cucGFnZVhPZmZzZXQ7XG5cbiAgcmV0dXJuIHtcbiAgICB3aWR0aDogcmVjdC53aWR0aCxcbiAgICBoZWlnaHQ6IHJlY3QuaGVpZ2h0LFxuICAgIG9mZnNldDoge1xuICAgICAgdG9wOiByZWN0LnRvcCArIHdpblksXG4gICAgICBsZWZ0OiByZWN0LmxlZnQgKyB3aW5YXG4gICAgfSxcbiAgICBwYXJlbnREaW1zOiB7XG4gICAgICB3aWR0aDogcGFyUmVjdC53aWR0aCxcbiAgICAgIGhlaWdodDogcGFyUmVjdC5oZWlnaHQsXG4gICAgICBvZmZzZXQ6IHtcbiAgICAgICAgdG9wOiBwYXJSZWN0LnRvcCArIHdpblksXG4gICAgICAgIGxlZnQ6IHBhclJlY3QubGVmdCArIHdpblhcbiAgICAgIH1cbiAgICB9LFxuICAgIHdpbmRvd0RpbXM6IHtcbiAgICAgIHdpZHRoOiB3aW5SZWN0LndpZHRoLFxuICAgICAgaGVpZ2h0OiB3aW5SZWN0LmhlaWdodCxcbiAgICAgIG9mZnNldDoge1xuICAgICAgICB0b3A6IHdpblksXG4gICAgICAgIGxlZnQ6IHdpblhcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuLyoqXG4gKiBSZXR1cm5zIGFuIG9iamVjdCBvZiB0b3AgYW5kIGxlZnQgaW50ZWdlciBwaXhlbCB2YWx1ZXMgZm9yIGR5bmFtaWNhbGx5IHJlbmRlcmVkIGVsZW1lbnRzLFxuICogc3VjaCBhczogVG9vbHRpcCwgUmV2ZWFsLCBhbmQgRHJvcGRvd25cbiAqIEBmdW5jdGlvblxuICogQHBhcmFtIHtqUXVlcnl9IGVsZW1lbnQgLSBqUXVlcnkgb2JqZWN0IGZvciB0aGUgZWxlbWVudCBiZWluZyBwb3NpdGlvbmVkLlxuICogQHBhcmFtIHtqUXVlcnl9IGFuY2hvciAtIGpRdWVyeSBvYmplY3QgZm9yIHRoZSBlbGVtZW50J3MgYW5jaG9yIHBvaW50LlxuICogQHBhcmFtIHtTdHJpbmd9IHBvc2l0aW9uIC0gYSBzdHJpbmcgcmVsYXRpbmcgdG8gdGhlIGRlc2lyZWQgcG9zaXRpb24gb2YgdGhlIGVsZW1lbnQsIHJlbGF0aXZlIHRvIGl0J3MgYW5jaG9yXG4gKiBAcGFyYW0ge051bWJlcn0gdk9mZnNldCAtIGludGVnZXIgcGl4ZWwgdmFsdWUgb2YgZGVzaXJlZCB2ZXJ0aWNhbCBzZXBhcmF0aW9uIGJldHdlZW4gYW5jaG9yIGFuZCBlbGVtZW50LlxuICogQHBhcmFtIHtOdW1iZXJ9IGhPZmZzZXQgLSBpbnRlZ2VyIHBpeGVsIHZhbHVlIG9mIGRlc2lyZWQgaG9yaXpvbnRhbCBzZXBhcmF0aW9uIGJldHdlZW4gYW5jaG9yIGFuZCBlbGVtZW50LlxuICogQHBhcmFtIHtCb29sZWFufSBpc092ZXJmbG93IC0gaWYgYSBjb2xsaXNpb24gZXZlbnQgaXMgZGV0ZWN0ZWQsIHNldHMgdG8gdHJ1ZSB0byBkZWZhdWx0IHRoZSBlbGVtZW50IHRvIGZ1bGwgd2lkdGggLSBhbnkgZGVzaXJlZCBvZmZzZXQuXG4gKiBUT0RPIGFsdGVyL3Jld3JpdGUgdG8gd29yayB3aXRoIGBlbWAgdmFsdWVzIGFzIHdlbGwvaW5zdGVhZCBvZiBwaXhlbHNcbiAqL1xuZnVuY3Rpb24gR2V0T2Zmc2V0cyhlbGVtZW50LCBhbmNob3IsIHBvc2l0aW9uLCB2T2Zmc2V0LCBoT2Zmc2V0LCBpc092ZXJmbG93KSB7XG4gIHZhciAkZWxlRGltcyA9IEdldERpbWVuc2lvbnMoZWxlbWVudCksXG4gICAgICAkYW5jaG9yRGltcyA9IGFuY2hvciA/IEdldERpbWVuc2lvbnMoYW5jaG9yKSA6IG51bGw7XG5cbiAgc3dpdGNoIChwb3NpdGlvbikge1xuICAgIGNhc2UgJ3RvcCc6XG4gICAgICByZXR1cm4ge1xuICAgICAgICBsZWZ0OiAoRm91bmRhdGlvbi5ydGwoKSA/ICRhbmNob3JEaW1zLm9mZnNldC5sZWZ0IC0gJGVsZURpbXMud2lkdGggKyAkYW5jaG9yRGltcy53aWR0aCA6ICRhbmNob3JEaW1zLm9mZnNldC5sZWZ0KSxcbiAgICAgICAgdG9wOiAkYW5jaG9yRGltcy5vZmZzZXQudG9wIC0gKCRlbGVEaW1zLmhlaWdodCArIHZPZmZzZXQpXG4gICAgICB9XG4gICAgICBicmVhaztcbiAgICBjYXNlICdsZWZ0JzpcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGxlZnQ6ICRhbmNob3JEaW1zLm9mZnNldC5sZWZ0IC0gKCRlbGVEaW1zLndpZHRoICsgaE9mZnNldCksXG4gICAgICAgIHRvcDogJGFuY2hvckRpbXMub2Zmc2V0LnRvcFxuICAgICAgfVxuICAgICAgYnJlYWs7XG4gICAgY2FzZSAncmlnaHQnOlxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbGVmdDogJGFuY2hvckRpbXMub2Zmc2V0LmxlZnQgKyAkYW5jaG9yRGltcy53aWR0aCArIGhPZmZzZXQsXG4gICAgICAgIHRvcDogJGFuY2hvckRpbXMub2Zmc2V0LnRvcFxuICAgICAgfVxuICAgICAgYnJlYWs7XG4gICAgY2FzZSAnY2VudGVyIHRvcCc6XG4gICAgICByZXR1cm4ge1xuICAgICAgICBsZWZ0OiAoJGFuY2hvckRpbXMub2Zmc2V0LmxlZnQgKyAoJGFuY2hvckRpbXMud2lkdGggLyAyKSkgLSAoJGVsZURpbXMud2lkdGggLyAyKSxcbiAgICAgICAgdG9wOiAkYW5jaG9yRGltcy5vZmZzZXQudG9wIC0gKCRlbGVEaW1zLmhlaWdodCArIHZPZmZzZXQpXG4gICAgICB9XG4gICAgICBicmVhaztcbiAgICBjYXNlICdjZW50ZXIgYm90dG9tJzpcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGxlZnQ6IGlzT3ZlcmZsb3cgPyBoT2Zmc2V0IDogKCgkYW5jaG9yRGltcy5vZmZzZXQubGVmdCArICgkYW5jaG9yRGltcy53aWR0aCAvIDIpKSAtICgkZWxlRGltcy53aWR0aCAvIDIpKSxcbiAgICAgICAgdG9wOiAkYW5jaG9yRGltcy5vZmZzZXQudG9wICsgJGFuY2hvckRpbXMuaGVpZ2h0ICsgdk9mZnNldFxuICAgICAgfVxuICAgICAgYnJlYWs7XG4gICAgY2FzZSAnY2VudGVyIGxlZnQnOlxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbGVmdDogJGFuY2hvckRpbXMub2Zmc2V0LmxlZnQgLSAoJGVsZURpbXMud2lkdGggKyBoT2Zmc2V0KSxcbiAgICAgICAgdG9wOiAoJGFuY2hvckRpbXMub2Zmc2V0LnRvcCArICgkYW5jaG9yRGltcy5oZWlnaHQgLyAyKSkgLSAoJGVsZURpbXMuaGVpZ2h0IC8gMilcbiAgICAgIH1cbiAgICAgIGJyZWFrO1xuICAgIGNhc2UgJ2NlbnRlciByaWdodCc6XG4gICAgICByZXR1cm4ge1xuICAgICAgICBsZWZ0OiAkYW5jaG9yRGltcy5vZmZzZXQubGVmdCArICRhbmNob3JEaW1zLndpZHRoICsgaE9mZnNldCArIDEsXG4gICAgICAgIHRvcDogKCRhbmNob3JEaW1zLm9mZnNldC50b3AgKyAoJGFuY2hvckRpbXMuaGVpZ2h0IC8gMikpIC0gKCRlbGVEaW1zLmhlaWdodCAvIDIpXG4gICAgICB9XG4gICAgICBicmVhaztcbiAgICBjYXNlICdjZW50ZXInOlxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbGVmdDogKCRlbGVEaW1zLndpbmRvd0RpbXMub2Zmc2V0LmxlZnQgKyAoJGVsZURpbXMud2luZG93RGltcy53aWR0aCAvIDIpKSAtICgkZWxlRGltcy53aWR0aCAvIDIpLFxuICAgICAgICB0b3A6ICgkZWxlRGltcy53aW5kb3dEaW1zLm9mZnNldC50b3AgKyAoJGVsZURpbXMud2luZG93RGltcy5oZWlnaHQgLyAyKSkgLSAoJGVsZURpbXMuaGVpZ2h0IC8gMilcbiAgICAgIH1cbiAgICAgIGJyZWFrO1xuICAgIGNhc2UgJ3JldmVhbCc6XG4gICAgICByZXR1cm4ge1xuICAgICAgICBsZWZ0OiAoJGVsZURpbXMud2luZG93RGltcy53aWR0aCAtICRlbGVEaW1zLndpZHRoKSAvIDIsXG4gICAgICAgIHRvcDogJGVsZURpbXMud2luZG93RGltcy5vZmZzZXQudG9wICsgdk9mZnNldFxuICAgICAgfVxuICAgIGNhc2UgJ3JldmVhbCBmdWxsJzpcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGxlZnQ6ICRlbGVEaW1zLndpbmRvd0RpbXMub2Zmc2V0LmxlZnQsXG4gICAgICAgIHRvcDogJGVsZURpbXMud2luZG93RGltcy5vZmZzZXQudG9wXG4gICAgICB9XG4gICAgICBicmVhaztcbiAgICBjYXNlICdsZWZ0IGJvdHRvbSc6XG4gICAgICByZXR1cm4ge1xuICAgICAgICBsZWZ0OiAkYW5jaG9yRGltcy5vZmZzZXQubGVmdCxcbiAgICAgICAgdG9wOiAkYW5jaG9yRGltcy5vZmZzZXQudG9wICsgJGFuY2hvckRpbXMuaGVpZ2h0XG4gICAgICB9O1xuICAgICAgYnJlYWs7XG4gICAgY2FzZSAncmlnaHQgYm90dG9tJzpcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGxlZnQ6ICRhbmNob3JEaW1zLm9mZnNldC5sZWZ0ICsgJGFuY2hvckRpbXMud2lkdGggKyBoT2Zmc2V0IC0gJGVsZURpbXMud2lkdGgsXG4gICAgICAgIHRvcDogJGFuY2hvckRpbXMub2Zmc2V0LnRvcCArICRhbmNob3JEaW1zLmhlaWdodFxuICAgICAgfTtcbiAgICAgIGJyZWFrO1xuICAgIGRlZmF1bHQ6XG4gICAgICByZXR1cm4ge1xuICAgICAgICBsZWZ0OiAoRm91bmRhdGlvbi5ydGwoKSA/ICRhbmNob3JEaW1zLm9mZnNldC5sZWZ0IC0gJGVsZURpbXMud2lkdGggKyAkYW5jaG9yRGltcy53aWR0aCA6ICRhbmNob3JEaW1zLm9mZnNldC5sZWZ0ICsgaE9mZnNldCksXG4gICAgICAgIHRvcDogJGFuY2hvckRpbXMub2Zmc2V0LnRvcCArICRhbmNob3JEaW1zLmhlaWdodCArIHZPZmZzZXRcbiAgICAgIH1cbiAgfVxufVxuXG59KGpRdWVyeSk7XG4iLCIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICpcbiAqIFRoaXMgdXRpbCB3YXMgY3JlYXRlZCBieSBNYXJpdXMgT2xiZXJ0eiAqXG4gKiBQbGVhc2UgdGhhbmsgTWFyaXVzIG9uIEdpdEh1YiAvb3dsYmVydHogKlxuICogb3IgdGhlIHdlYiBodHRwOi8vd3d3Lm1hcml1c29sYmVydHouZGUvICpcbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAqXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG4ndXNlIHN0cmljdCc7XG5cbiFmdW5jdGlvbigkKSB7XG5cbmNvbnN0IGtleUNvZGVzID0ge1xuICA5OiAnVEFCJyxcbiAgMTM6ICdFTlRFUicsXG4gIDI3OiAnRVNDQVBFJyxcbiAgMzI6ICdTUEFDRScsXG4gIDM3OiAnQVJST1dfTEVGVCcsXG4gIDM4OiAnQVJST1dfVVAnLFxuICAzOTogJ0FSUk9XX1JJR0hUJyxcbiAgNDA6ICdBUlJPV19ET1dOJ1xufVxuXG52YXIgY29tbWFuZHMgPSB7fVxuXG52YXIgS2V5Ym9hcmQgPSB7XG4gIGtleXM6IGdldEtleUNvZGVzKGtleUNvZGVzKSxcblxuICAvKipcbiAgICogUGFyc2VzIHRoZSAoa2V5Ym9hcmQpIGV2ZW50IGFuZCByZXR1cm5zIGEgU3RyaW5nIHRoYXQgcmVwcmVzZW50cyBpdHMga2V5XG4gICAqIENhbiBiZSB1c2VkIGxpa2UgRm91bmRhdGlvbi5wYXJzZUtleShldmVudCkgPT09IEZvdW5kYXRpb24ua2V5cy5TUEFDRVxuICAgKiBAcGFyYW0ge0V2ZW50fSBldmVudCAtIHRoZSBldmVudCBnZW5lcmF0ZWQgYnkgdGhlIGV2ZW50IGhhbmRsZXJcbiAgICogQHJldHVybiBTdHJpbmcga2V5IC0gU3RyaW5nIHRoYXQgcmVwcmVzZW50cyB0aGUga2V5IHByZXNzZWRcbiAgICovXG4gIHBhcnNlS2V5KGV2ZW50KSB7XG4gICAgdmFyIGtleSA9IGtleUNvZGVzW2V2ZW50LndoaWNoIHx8IGV2ZW50LmtleUNvZGVdIHx8IFN0cmluZy5mcm9tQ2hhckNvZGUoZXZlbnQud2hpY2gpLnRvVXBwZXJDYXNlKCk7XG4gICAgaWYgKGV2ZW50LnNoaWZ0S2V5KSBrZXkgPSBgU0hJRlRfJHtrZXl9YDtcbiAgICBpZiAoZXZlbnQuY3RybEtleSkga2V5ID0gYENUUkxfJHtrZXl9YDtcbiAgICBpZiAoZXZlbnQuYWx0S2V5KSBrZXkgPSBgQUxUXyR7a2V5fWA7XG4gICAgcmV0dXJuIGtleTtcbiAgfSxcblxuICAvKipcbiAgICogSGFuZGxlcyB0aGUgZ2l2ZW4gKGtleWJvYXJkKSBldmVudFxuICAgKiBAcGFyYW0ge0V2ZW50fSBldmVudCAtIHRoZSBldmVudCBnZW5lcmF0ZWQgYnkgdGhlIGV2ZW50IGhhbmRsZXJcbiAgICogQHBhcmFtIHtTdHJpbmd9IGNvbXBvbmVudCAtIEZvdW5kYXRpb24gY29tcG9uZW50J3MgbmFtZSwgZS5nLiBTbGlkZXIgb3IgUmV2ZWFsXG4gICAqIEBwYXJhbSB7T2JqZWN0c30gZnVuY3Rpb25zIC0gY29sbGVjdGlvbiBvZiBmdW5jdGlvbnMgdGhhdCBhcmUgdG8gYmUgZXhlY3V0ZWRcbiAgICovXG4gIGhhbmRsZUtleShldmVudCwgY29tcG9uZW50LCBmdW5jdGlvbnMpIHtcbiAgICB2YXIgY29tbWFuZExpc3QgPSBjb21tYW5kc1tjb21wb25lbnRdLFxuICAgICAga2V5Q29kZSA9IHRoaXMucGFyc2VLZXkoZXZlbnQpLFxuICAgICAgY21kcyxcbiAgICAgIGNvbW1hbmQsXG4gICAgICBmbjtcblxuICAgIGlmICghY29tbWFuZExpc3QpIHJldHVybiBjb25zb2xlLndhcm4oJ0NvbXBvbmVudCBub3QgZGVmaW5lZCEnKTtcblxuICAgIGlmICh0eXBlb2YgY29tbWFuZExpc3QubHRyID09PSAndW5kZWZpbmVkJykgeyAvLyB0aGlzIGNvbXBvbmVudCBkb2VzIG5vdCBkaWZmZXJlbnRpYXRlIGJldHdlZW4gbHRyIGFuZCBydGxcbiAgICAgICAgY21kcyA9IGNvbW1hbmRMaXN0OyAvLyB1c2UgcGxhaW4gbGlzdFxuICAgIH0gZWxzZSB7IC8vIG1lcmdlIGx0ciBhbmQgcnRsOiBpZiBkb2N1bWVudCBpcyBydGwsIHJ0bCBvdmVyd3JpdGVzIGx0ciBhbmQgdmljZSB2ZXJzYVxuICAgICAgICBpZiAoRm91bmRhdGlvbi5ydGwoKSkgY21kcyA9ICQuZXh0ZW5kKHt9LCBjb21tYW5kTGlzdC5sdHIsIGNvbW1hbmRMaXN0LnJ0bCk7XG5cbiAgICAgICAgZWxzZSBjbWRzID0gJC5leHRlbmQoe30sIGNvbW1hbmRMaXN0LnJ0bCwgY29tbWFuZExpc3QubHRyKTtcbiAgICB9XG4gICAgY29tbWFuZCA9IGNtZHNba2V5Q29kZV07XG5cbiAgICBmbiA9IGZ1bmN0aW9uc1tjb21tYW5kXTtcbiAgICBpZiAoZm4gJiYgdHlwZW9mIGZuID09PSAnZnVuY3Rpb24nKSB7IC8vIGV4ZWN1dGUgZnVuY3Rpb24gIGlmIGV4aXN0c1xuICAgICAgdmFyIHJldHVyblZhbHVlID0gZm4uYXBwbHkoKTtcbiAgICAgIGlmIChmdW5jdGlvbnMuaGFuZGxlZCB8fCB0eXBlb2YgZnVuY3Rpb25zLmhhbmRsZWQgPT09ICdmdW5jdGlvbicpIHsgLy8gZXhlY3V0ZSBmdW5jdGlvbiB3aGVuIGV2ZW50IHdhcyBoYW5kbGVkXG4gICAgICAgICAgZnVuY3Rpb25zLmhhbmRsZWQocmV0dXJuVmFsdWUpO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoZnVuY3Rpb25zLnVuaGFuZGxlZCB8fCB0eXBlb2YgZnVuY3Rpb25zLnVuaGFuZGxlZCA9PT0gJ2Z1bmN0aW9uJykgeyAvLyBleGVjdXRlIGZ1bmN0aW9uIHdoZW4gZXZlbnQgd2FzIG5vdCBoYW5kbGVkXG4gICAgICAgICAgZnVuY3Rpb25zLnVuaGFuZGxlZCgpO1xuICAgICAgfVxuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogRmluZHMgYWxsIGZvY3VzYWJsZSBlbGVtZW50cyB3aXRoaW4gdGhlIGdpdmVuIGAkZWxlbWVudGBcbiAgICogQHBhcmFtIHtqUXVlcnl9ICRlbGVtZW50IC0galF1ZXJ5IG9iamVjdCB0byBzZWFyY2ggd2l0aGluXG4gICAqIEByZXR1cm4ge2pRdWVyeX0gJGZvY3VzYWJsZSAtIGFsbCBmb2N1c2FibGUgZWxlbWVudHMgd2l0aGluIGAkZWxlbWVudGBcbiAgICovXG4gIGZpbmRGb2N1c2FibGUoJGVsZW1lbnQpIHtcbiAgICByZXR1cm4gJGVsZW1lbnQuZmluZCgnYVtocmVmXSwgYXJlYVtocmVmXSwgaW5wdXQ6bm90KFtkaXNhYmxlZF0pLCBzZWxlY3Q6bm90KFtkaXNhYmxlZF0pLCB0ZXh0YXJlYTpub3QoW2Rpc2FibGVkXSksIGJ1dHRvbjpub3QoW2Rpc2FibGVkXSksIGlmcmFtZSwgb2JqZWN0LCBlbWJlZCwgKlt0YWJpbmRleF0sICpbY29udGVudGVkaXRhYmxlXScpLmZpbHRlcihmdW5jdGlvbigpIHtcbiAgICAgIGlmICghJCh0aGlzKS5pcygnOnZpc2libGUnKSB8fCAkKHRoaXMpLmF0dHIoJ3RhYmluZGV4JykgPCAwKSB7IHJldHVybiBmYWxzZTsgfSAvL29ubHkgaGF2ZSB2aXNpYmxlIGVsZW1lbnRzIGFuZCB0aG9zZSB0aGF0IGhhdmUgYSB0YWJpbmRleCBncmVhdGVyIG9yIGVxdWFsIDBcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH0pO1xuICB9LFxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRoZSBjb21wb25lbnQgbmFtZSBuYW1lXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBjb21wb25lbnQgLSBGb3VuZGF0aW9uIGNvbXBvbmVudCwgZS5nLiBTbGlkZXIgb3IgUmV2ZWFsXG4gICAqIEByZXR1cm4gU3RyaW5nIGNvbXBvbmVudE5hbWVcbiAgICovXG5cbiAgcmVnaXN0ZXIoY29tcG9uZW50TmFtZSwgY21kcykge1xuICAgIGNvbW1hbmRzW2NvbXBvbmVudE5hbWVdID0gY21kcztcbiAgfVxufVxuXG4vKlxuICogQ29uc3RhbnRzIGZvciBlYXNpZXIgY29tcGFyaW5nLlxuICogQ2FuIGJlIHVzZWQgbGlrZSBGb3VuZGF0aW9uLnBhcnNlS2V5KGV2ZW50KSA9PT0gRm91bmRhdGlvbi5rZXlzLlNQQUNFXG4gKi9cbmZ1bmN0aW9uIGdldEtleUNvZGVzKGtjcykge1xuICB2YXIgayA9IHt9O1xuICBmb3IgKHZhciBrYyBpbiBrY3MpIGtba2NzW2tjXV0gPSBrY3Nba2NdO1xuICByZXR1cm4gaztcbn1cblxuRm91bmRhdGlvbi5LZXlib2FyZCA9IEtleWJvYXJkO1xuXG59KGpRdWVyeSk7XG4iLCIndXNlIHN0cmljdCc7XG5cbiFmdW5jdGlvbigkKSB7XG5cbi8vIERlZmF1bHQgc2V0IG9mIG1lZGlhIHF1ZXJpZXNcbmNvbnN0IGRlZmF1bHRRdWVyaWVzID0ge1xuICAnZGVmYXVsdCcgOiAnb25seSBzY3JlZW4nLFxuICBsYW5kc2NhcGUgOiAnb25seSBzY3JlZW4gYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKScsXG4gIHBvcnRyYWl0IDogJ29ubHkgc2NyZWVuIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KScsXG4gIHJldGluYSA6ICdvbmx5IHNjcmVlbiBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMiksJyArXG4gICAgJ29ubHkgc2NyZWVuIGFuZCAobWluLS1tb3otZGV2aWNlLXBpeGVsLXJhdGlvOiAyKSwnICtcbiAgICAnb25seSBzY3JlZW4gYW5kICgtby1taW4tZGV2aWNlLXBpeGVsLXJhdGlvOiAyLzEpLCcgK1xuICAgICdvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpLCcgK1xuICAgICdvbmx5IHNjcmVlbiBhbmQgKG1pbi1yZXNvbHV0aW9uOiAxOTJkcGkpLCcgK1xuICAgICdvbmx5IHNjcmVlbiBhbmQgKG1pbi1yZXNvbHV0aW9uOiAyZHBweCknXG59O1xuXG52YXIgTWVkaWFRdWVyeSA9IHtcbiAgcXVlcmllczogW10sXG5cbiAgY3VycmVudDogJycsXG5cbiAgLyoqXG4gICAqIEluaXRpYWxpemVzIHRoZSBtZWRpYSBxdWVyeSBoZWxwZXIsIGJ5IGV4dHJhY3RpbmcgdGhlIGJyZWFrcG9pbnQgbGlzdCBmcm9tIHRoZSBDU1MgYW5kIGFjdGl2YXRpbmcgdGhlIGJyZWFrcG9pbnQgd2F0Y2hlci5cbiAgICogQGZ1bmN0aW9uXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBfaW5pdCgpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgdmFyIGV4dHJhY3RlZFN0eWxlcyA9ICQoJy5mb3VuZGF0aW9uLW1xJykuY3NzKCdmb250LWZhbWlseScpO1xuICAgIHZhciBuYW1lZFF1ZXJpZXM7XG5cbiAgICBuYW1lZFF1ZXJpZXMgPSBwYXJzZVN0eWxlVG9PYmplY3QoZXh0cmFjdGVkU3R5bGVzKTtcblxuICAgIGZvciAodmFyIGtleSBpbiBuYW1lZFF1ZXJpZXMpIHtcbiAgICAgIGlmKG5hbWVkUXVlcmllcy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgIHNlbGYucXVlcmllcy5wdXNoKHtcbiAgICAgICAgICBuYW1lOiBrZXksXG4gICAgICAgICAgdmFsdWU6IGBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogJHtuYW1lZFF1ZXJpZXNba2V5XX0pYFxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB0aGlzLmN1cnJlbnQgPSB0aGlzLl9nZXRDdXJyZW50U2l6ZSgpO1xuXG4gICAgdGhpcy5fd2F0Y2hlcigpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBDaGVja3MgaWYgdGhlIHNjcmVlbiBpcyBhdCBsZWFzdCBhcyB3aWRlIGFzIGEgYnJlYWtwb2ludC5cbiAgICogQGZ1bmN0aW9uXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBzaXplIC0gTmFtZSBvZiB0aGUgYnJlYWtwb2ludCB0byBjaGVjay5cbiAgICogQHJldHVybnMge0Jvb2xlYW59IGB0cnVlYCBpZiB0aGUgYnJlYWtwb2ludCBtYXRjaGVzLCBgZmFsc2VgIGlmIGl0J3Mgc21hbGxlci5cbiAgICovXG4gIGF0TGVhc3Qoc2l6ZSkge1xuICAgIHZhciBxdWVyeSA9IHRoaXMuZ2V0KHNpemUpO1xuXG4gICAgaWYgKHF1ZXJ5KSB7XG4gICAgICByZXR1cm4gd2luZG93Lm1hdGNoTWVkaWEocXVlcnkpLm1hdGNoZXM7XG4gICAgfVxuXG4gICAgcmV0dXJuIGZhbHNlO1xuICB9LFxuXG4gIC8qKlxuICAgKiBHZXRzIHRoZSBtZWRpYSBxdWVyeSBvZiBhIGJyZWFrcG9pbnQuXG4gICAqIEBmdW5jdGlvblxuICAgKiBAcGFyYW0ge1N0cmluZ30gc2l6ZSAtIE5hbWUgb2YgdGhlIGJyZWFrcG9pbnQgdG8gZ2V0LlxuICAgKiBAcmV0dXJucyB7U3RyaW5nfG51bGx9IC0gVGhlIG1lZGlhIHF1ZXJ5IG9mIHRoZSBicmVha3BvaW50LCBvciBgbnVsbGAgaWYgdGhlIGJyZWFrcG9pbnQgZG9lc24ndCBleGlzdC5cbiAgICovXG4gIGdldChzaXplKSB7XG4gICAgZm9yICh2YXIgaSBpbiB0aGlzLnF1ZXJpZXMpIHtcbiAgICAgIGlmKHRoaXMucXVlcmllcy5oYXNPd25Qcm9wZXJ0eShpKSkge1xuICAgICAgICB2YXIgcXVlcnkgPSB0aGlzLnF1ZXJpZXNbaV07XG4gICAgICAgIGlmIChzaXplID09PSBxdWVyeS5uYW1lKSByZXR1cm4gcXVlcnkudmFsdWU7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIG51bGw7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIGN1cnJlbnQgYnJlYWtwb2ludCBuYW1lIGJ5IHRlc3RpbmcgZXZlcnkgYnJlYWtwb2ludCBhbmQgcmV0dXJuaW5nIHRoZSBsYXN0IG9uZSB0byBtYXRjaCAodGhlIGJpZ2dlc3Qgb25lKS5cbiAgICogQGZ1bmN0aW9uXG4gICAqIEBwcml2YXRlXG4gICAqIEByZXR1cm5zIHtTdHJpbmd9IE5hbWUgb2YgdGhlIGN1cnJlbnQgYnJlYWtwb2ludC5cbiAgICovXG4gIF9nZXRDdXJyZW50U2l6ZSgpIHtcbiAgICB2YXIgbWF0Y2hlZDtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5xdWVyaWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgcXVlcnkgPSB0aGlzLnF1ZXJpZXNbaV07XG5cbiAgICAgIGlmICh3aW5kb3cubWF0Y2hNZWRpYShxdWVyeS52YWx1ZSkubWF0Y2hlcykge1xuICAgICAgICBtYXRjaGVkID0gcXVlcnk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiBtYXRjaGVkID09PSAnb2JqZWN0Jykge1xuICAgICAgcmV0dXJuIG1hdGNoZWQubmFtZTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIG1hdGNoZWQ7XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiBBY3RpdmF0ZXMgdGhlIGJyZWFrcG9pbnQgd2F0Y2hlciwgd2hpY2ggZmlyZXMgYW4gZXZlbnQgb24gdGhlIHdpbmRvdyB3aGVuZXZlciB0aGUgYnJlYWtwb2ludCBjaGFuZ2VzLlxuICAgKiBAZnVuY3Rpb25cbiAgICogQHByaXZhdGVcbiAgICovXG4gIF93YXRjaGVyKCkge1xuICAgICQod2luZG93KS5vbigncmVzaXplLnpmLm1lZGlhcXVlcnknLCAoKSA9PiB7XG4gICAgICB2YXIgbmV3U2l6ZSA9IHRoaXMuX2dldEN1cnJlbnRTaXplKCksIGN1cnJlbnRTaXplID0gdGhpcy5jdXJyZW50O1xuXG4gICAgICBpZiAobmV3U2l6ZSAhPT0gY3VycmVudFNpemUpIHtcbiAgICAgICAgLy8gQ2hhbmdlIHRoZSBjdXJyZW50IG1lZGlhIHF1ZXJ5XG4gICAgICAgIHRoaXMuY3VycmVudCA9IG5ld1NpemU7XG5cbiAgICAgICAgLy8gQnJvYWRjYXN0IHRoZSBtZWRpYSBxdWVyeSBjaGFuZ2Ugb24gdGhlIHdpbmRvd1xuICAgICAgICAkKHdpbmRvdykudHJpZ2dlcignY2hhbmdlZC56Zi5tZWRpYXF1ZXJ5JywgW25ld1NpemUsIGN1cnJlbnRTaXplXSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cbn07XG5cbkZvdW5kYXRpb24uTWVkaWFRdWVyeSA9IE1lZGlhUXVlcnk7XG5cbi8vIG1hdGNoTWVkaWEoKSBwb2x5ZmlsbCAtIFRlc3QgYSBDU1MgbWVkaWEgdHlwZS9xdWVyeSBpbiBKUy5cbi8vIEF1dGhvcnMgJiBjb3B5cmlnaHQgKGMpIDIwMTI6IFNjb3R0IEplaGwsIFBhdWwgSXJpc2gsIE5pY2hvbGFzIFpha2FzLCBEYXZpZCBLbmlnaHQuIER1YWwgTUlUL0JTRCBsaWNlbnNlXG53aW5kb3cubWF0Y2hNZWRpYSB8fCAod2luZG93Lm1hdGNoTWVkaWEgPSBmdW5jdGlvbigpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIC8vIEZvciBicm93c2VycyB0aGF0IHN1cHBvcnQgbWF0Y2hNZWRpdW0gYXBpIHN1Y2ggYXMgSUUgOSBhbmQgd2Via2l0XG4gIHZhciBzdHlsZU1lZGlhID0gKHdpbmRvdy5zdHlsZU1lZGlhIHx8IHdpbmRvdy5tZWRpYSk7XG5cbiAgLy8gRm9yIHRob3NlIHRoYXQgZG9uJ3Qgc3VwcG9ydCBtYXRjaE1lZGl1bVxuICBpZiAoIXN0eWxlTWVkaWEpIHtcbiAgICB2YXIgc3R5bGUgICA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3N0eWxlJyksXG4gICAgc2NyaXB0ICAgICAgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnc2NyaXB0JylbMF0sXG4gICAgaW5mbyAgICAgICAgPSBudWxsO1xuXG4gICAgc3R5bGUudHlwZSAgPSAndGV4dC9jc3MnO1xuICAgIHN0eWxlLmlkICAgID0gJ21hdGNobWVkaWFqcy10ZXN0JztcblxuICAgIHNjcmlwdCAmJiBzY3JpcHQucGFyZW50Tm9kZSAmJiBzY3JpcHQucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUoc3R5bGUsIHNjcmlwdCk7XG5cbiAgICAvLyAnc3R5bGUuY3VycmVudFN0eWxlJyBpcyB1c2VkIGJ5IElFIDw9IDggYW5kICd3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZScgZm9yIGFsbCBvdGhlciBicm93c2Vyc1xuICAgIGluZm8gPSAoJ2dldENvbXB1dGVkU3R5bGUnIGluIHdpbmRvdykgJiYgd2luZG93LmdldENvbXB1dGVkU3R5bGUoc3R5bGUsIG51bGwpIHx8IHN0eWxlLmN1cnJlbnRTdHlsZTtcblxuICAgIHN0eWxlTWVkaWEgPSB7XG4gICAgICBtYXRjaE1lZGl1bShtZWRpYSkge1xuICAgICAgICB2YXIgdGV4dCA9IGBAbWVkaWEgJHttZWRpYX17ICNtYXRjaG1lZGlhanMtdGVzdCB7IHdpZHRoOiAxcHg7IH0gfWA7XG5cbiAgICAgICAgLy8gJ3N0eWxlLnN0eWxlU2hlZXQnIGlzIHVzZWQgYnkgSUUgPD0gOCBhbmQgJ3N0eWxlLnRleHRDb250ZW50JyBmb3IgYWxsIG90aGVyIGJyb3dzZXJzXG4gICAgICAgIGlmIChzdHlsZS5zdHlsZVNoZWV0KSB7XG4gICAgICAgICAgc3R5bGUuc3R5bGVTaGVldC5jc3NUZXh0ID0gdGV4dDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBzdHlsZS50ZXh0Q29udGVudCA9IHRleHQ7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBUZXN0IGlmIG1lZGlhIHF1ZXJ5IGlzIHRydWUgb3IgZmFsc2VcbiAgICAgICAgcmV0dXJuIGluZm8ud2lkdGggPT09ICcxcHgnO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiBmdW5jdGlvbihtZWRpYSkge1xuICAgIHJldHVybiB7XG4gICAgICBtYXRjaGVzOiBzdHlsZU1lZGlhLm1hdGNoTWVkaXVtKG1lZGlhIHx8ICdhbGwnKSxcbiAgICAgIG1lZGlhOiBtZWRpYSB8fCAnYWxsJ1xuICAgIH07XG4gIH1cbn0oKSk7XG5cbi8vIFRoYW5rIHlvdTogaHR0cHM6Ly9naXRodWIuY29tL3NpbmRyZXNvcmh1cy9xdWVyeS1zdHJpbmdcbmZ1bmN0aW9uIHBhcnNlU3R5bGVUb09iamVjdChzdHIpIHtcbiAgdmFyIHN0eWxlT2JqZWN0ID0ge307XG5cbiAgaWYgKHR5cGVvZiBzdHIgIT09ICdzdHJpbmcnKSB7XG4gICAgcmV0dXJuIHN0eWxlT2JqZWN0O1xuICB9XG5cbiAgc3RyID0gc3RyLnRyaW0oKS5zbGljZSgxLCAtMSk7IC8vIGJyb3dzZXJzIHJlLXF1b3RlIHN0cmluZyBzdHlsZSB2YWx1ZXNcblxuICBpZiAoIXN0cikge1xuICAgIHJldHVybiBzdHlsZU9iamVjdDtcbiAgfVxuXG4gIHN0eWxlT2JqZWN0ID0gc3RyLnNwbGl0KCcmJykucmVkdWNlKGZ1bmN0aW9uKHJldCwgcGFyYW0pIHtcbiAgICB2YXIgcGFydHMgPSBwYXJhbS5yZXBsYWNlKC9cXCsvZywgJyAnKS5zcGxpdCgnPScpO1xuICAgIHZhciBrZXkgPSBwYXJ0c1swXTtcbiAgICB2YXIgdmFsID0gcGFydHNbMV07XG4gICAga2V5ID0gZGVjb2RlVVJJQ29tcG9uZW50KGtleSk7XG5cbiAgICAvLyBtaXNzaW5nIGA9YCBzaG91bGQgYmUgYG51bGxgOlxuICAgIC8vIGh0dHA6Ly93My5vcmcvVFIvMjAxMi9XRC11cmwtMjAxMjA1MjQvI2NvbGxlY3QtdXJsLXBhcmFtZXRlcnNcbiAgICB2YWwgPSB2YWwgPT09IHVuZGVmaW5lZCA/IG51bGwgOiBkZWNvZGVVUklDb21wb25lbnQodmFsKTtcblxuICAgIGlmICghcmV0Lmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgIHJldFtrZXldID0gdmFsO1xuICAgIH0gZWxzZSBpZiAoQXJyYXkuaXNBcnJheShyZXRba2V5XSkpIHtcbiAgICAgIHJldFtrZXldLnB1c2godmFsKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0W2tleV0gPSBbcmV0W2tleV0sIHZhbF07XG4gICAgfVxuICAgIHJldHVybiByZXQ7XG4gIH0sIHt9KTtcblxuICByZXR1cm4gc3R5bGVPYmplY3Q7XG59XG5cbkZvdW5kYXRpb24uTWVkaWFRdWVyeSA9IE1lZGlhUXVlcnk7XG5cbn0oalF1ZXJ5KTtcbiIsIid1c2Ugc3RyaWN0JztcblxuIWZ1bmN0aW9uKCQpIHtcblxuLyoqXG4gKiBNb3Rpb24gbW9kdWxlLlxuICogQG1vZHVsZSBmb3VuZGF0aW9uLm1vdGlvblxuICovXG5cbmNvbnN0IGluaXRDbGFzc2VzICAgPSBbJ211aS1lbnRlcicsICdtdWktbGVhdmUnXTtcbmNvbnN0IGFjdGl2ZUNsYXNzZXMgPSBbJ211aS1lbnRlci1hY3RpdmUnLCAnbXVpLWxlYXZlLWFjdGl2ZSddO1xuXG5jb25zdCBNb3Rpb24gPSB7XG4gIGFuaW1hdGVJbjogZnVuY3Rpb24oZWxlbWVudCwgYW5pbWF0aW9uLCBjYikge1xuICAgIGFuaW1hdGUodHJ1ZSwgZWxlbWVudCwgYW5pbWF0aW9uLCBjYik7XG4gIH0sXG5cbiAgYW5pbWF0ZU91dDogZnVuY3Rpb24oZWxlbWVudCwgYW5pbWF0aW9uLCBjYikge1xuICAgIGFuaW1hdGUoZmFsc2UsIGVsZW1lbnQsIGFuaW1hdGlvbiwgY2IpO1xuICB9XG59XG5cbmZ1bmN0aW9uIE1vdmUoZHVyYXRpb24sIGVsZW0sIGZuKXtcbiAgdmFyIGFuaW0sIHByb2csIHN0YXJ0ID0gbnVsbDtcbiAgLy8gY29uc29sZS5sb2coJ2NhbGxlZCcpO1xuXG4gIGZ1bmN0aW9uIG1vdmUodHMpe1xuICAgIGlmKCFzdGFydCkgc3RhcnQgPSB3aW5kb3cucGVyZm9ybWFuY2Uubm93KCk7XG4gICAgLy8gY29uc29sZS5sb2coc3RhcnQsIHRzKTtcbiAgICBwcm9nID0gdHMgLSBzdGFydDtcbiAgICBmbi5hcHBseShlbGVtKTtcblxuICAgIGlmKHByb2cgPCBkdXJhdGlvbil7IGFuaW0gPSB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKG1vdmUsIGVsZW0pOyB9XG4gICAgZWxzZXtcbiAgICAgIHdpbmRvdy5jYW5jZWxBbmltYXRpb25GcmFtZShhbmltKTtcbiAgICAgIGVsZW0udHJpZ2dlcignZmluaXNoZWQuemYuYW5pbWF0ZScsIFtlbGVtXSkudHJpZ2dlckhhbmRsZXIoJ2ZpbmlzaGVkLnpmLmFuaW1hdGUnLCBbZWxlbV0pO1xuICAgIH1cbiAgfVxuICBhbmltID0gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZShtb3ZlKTtcbn1cblxuLyoqXG4gKiBBbmltYXRlcyBhbiBlbGVtZW50IGluIG9yIG91dCB1c2luZyBhIENTUyB0cmFuc2l0aW9uIGNsYXNzLlxuICogQGZ1bmN0aW9uXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtCb29sZWFufSBpc0luIC0gRGVmaW5lcyBpZiB0aGUgYW5pbWF0aW9uIGlzIGluIG9yIG91dC5cbiAqIEBwYXJhbSB7T2JqZWN0fSBlbGVtZW50IC0galF1ZXJ5IG9yIEhUTUwgb2JqZWN0IHRvIGFuaW1hdGUuXG4gKiBAcGFyYW0ge1N0cmluZ30gYW5pbWF0aW9uIC0gQ1NTIGNsYXNzIHRvIHVzZS5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNiIC0gQ2FsbGJhY2sgdG8gcnVuIHdoZW4gYW5pbWF0aW9uIGlzIGZpbmlzaGVkLlxuICovXG5mdW5jdGlvbiBhbmltYXRlKGlzSW4sIGVsZW1lbnQsIGFuaW1hdGlvbiwgY2IpIHtcbiAgZWxlbWVudCA9ICQoZWxlbWVudCkuZXEoMCk7XG5cbiAgaWYgKCFlbGVtZW50Lmxlbmd0aCkgcmV0dXJuO1xuXG4gIHZhciBpbml0Q2xhc3MgPSBpc0luID8gaW5pdENsYXNzZXNbMF0gOiBpbml0Q2xhc3Nlc1sxXTtcbiAgdmFyIGFjdGl2ZUNsYXNzID0gaXNJbiA/IGFjdGl2ZUNsYXNzZXNbMF0gOiBhY3RpdmVDbGFzc2VzWzFdO1xuXG4gIC8vIFNldCB1cCB0aGUgYW5pbWF0aW9uXG4gIHJlc2V0KCk7XG5cbiAgZWxlbWVudFxuICAgIC5hZGRDbGFzcyhhbmltYXRpb24pXG4gICAgLmNzcygndHJhbnNpdGlvbicsICdub25lJyk7XG5cbiAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKCgpID0+IHtcbiAgICBlbGVtZW50LmFkZENsYXNzKGluaXRDbGFzcyk7XG4gICAgaWYgKGlzSW4pIGVsZW1lbnQuc2hvdygpO1xuICB9KTtcblxuICAvLyBTdGFydCB0aGUgYW5pbWF0aW9uXG4gIHJlcXVlc3RBbmltYXRpb25GcmFtZSgoKSA9PiB7XG4gICAgZWxlbWVudFswXS5vZmZzZXRXaWR0aDtcbiAgICBlbGVtZW50XG4gICAgICAuY3NzKCd0cmFuc2l0aW9uJywgJycpXG4gICAgICAuYWRkQ2xhc3MoYWN0aXZlQ2xhc3MpO1xuICB9KTtcblxuICAvLyBDbGVhbiB1cCB0aGUgYW5pbWF0aW9uIHdoZW4gaXQgZmluaXNoZXNcbiAgZWxlbWVudC5vbmUoRm91bmRhdGlvbi50cmFuc2l0aW9uZW5kKGVsZW1lbnQpLCBmaW5pc2gpO1xuXG4gIC8vIEhpZGVzIHRoZSBlbGVtZW50IChmb3Igb3V0IGFuaW1hdGlvbnMpLCByZXNldHMgdGhlIGVsZW1lbnQsIGFuZCBydW5zIGEgY2FsbGJhY2tcbiAgZnVuY3Rpb24gZmluaXNoKCkge1xuICAgIGlmICghaXNJbikgZWxlbWVudC5oaWRlKCk7XG4gICAgcmVzZXQoKTtcbiAgICBpZiAoY2IpIGNiLmFwcGx5KGVsZW1lbnQpO1xuICB9XG5cbiAgLy8gUmVzZXRzIHRyYW5zaXRpb25zIGFuZCByZW1vdmVzIG1vdGlvbi1zcGVjaWZpYyBjbGFzc2VzXG4gIGZ1bmN0aW9uIHJlc2V0KCkge1xuICAgIGVsZW1lbnRbMF0uc3R5bGUudHJhbnNpdGlvbkR1cmF0aW9uID0gMDtcbiAgICBlbGVtZW50LnJlbW92ZUNsYXNzKGAke2luaXRDbGFzc30gJHthY3RpdmVDbGFzc30gJHthbmltYXRpb259YCk7XG4gIH1cbn1cblxuRm91bmRhdGlvbi5Nb3ZlID0gTW92ZTtcbkZvdW5kYXRpb24uTW90aW9uID0gTW90aW9uO1xuXG59KGpRdWVyeSk7XG4iLCIndXNlIHN0cmljdCc7XG5cbiFmdW5jdGlvbigkKSB7XG5cbmNvbnN0IE5lc3QgPSB7XG4gIEZlYXRoZXIobWVudSwgdHlwZSA9ICd6ZicpIHtcbiAgICBtZW51LmF0dHIoJ3JvbGUnLCAnbWVudWJhcicpO1xuXG4gICAgdmFyIGl0ZW1zID0gbWVudS5maW5kKCdsaScpLmF0dHIoeydyb2xlJzogJ21lbnVpdGVtJ30pLFxuICAgICAgICBzdWJNZW51Q2xhc3MgPSBgaXMtJHt0eXBlfS1zdWJtZW51YCxcbiAgICAgICAgc3ViSXRlbUNsYXNzID0gYCR7c3ViTWVudUNsYXNzfS1pdGVtYCxcbiAgICAgICAgaGFzU3ViQ2xhc3MgPSBgaXMtJHt0eXBlfS1zdWJtZW51LXBhcmVudGA7XG5cbiAgICBtZW51LmZpbmQoJ2E6Zmlyc3QnKS5hdHRyKCd0YWJpbmRleCcsIDApO1xuXG4gICAgaXRlbXMuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgIHZhciAkaXRlbSA9ICQodGhpcyksXG4gICAgICAgICAgJHN1YiA9ICRpdGVtLmNoaWxkcmVuKCd1bCcpO1xuXG4gICAgICBpZiAoJHN1Yi5sZW5ndGgpIHtcbiAgICAgICAgJGl0ZW1cbiAgICAgICAgICAuYWRkQ2xhc3MoaGFzU3ViQ2xhc3MpXG4gICAgICAgICAgLmF0dHIoe1xuICAgICAgICAgICAgJ2FyaWEtaGFzcG9wdXAnOiB0cnVlLFxuICAgICAgICAgICAgJ2FyaWEtZXhwYW5kZWQnOiBmYWxzZSxcbiAgICAgICAgICAgICdhcmlhLWxhYmVsJzogJGl0ZW0uY2hpbGRyZW4oJ2E6Zmlyc3QnKS50ZXh0KClcbiAgICAgICAgICB9KTtcblxuICAgICAgICAkc3ViXG4gICAgICAgICAgLmFkZENsYXNzKGBzdWJtZW51ICR7c3ViTWVudUNsYXNzfWApXG4gICAgICAgICAgLmF0dHIoe1xuICAgICAgICAgICAgJ2RhdGEtc3VibWVudSc6ICcnLFxuICAgICAgICAgICAgJ2FyaWEtaGlkZGVuJzogdHJ1ZSxcbiAgICAgICAgICAgICdyb2xlJzogJ21lbnUnXG4gICAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIGlmICgkaXRlbS5wYXJlbnQoJ1tkYXRhLXN1Ym1lbnVdJykubGVuZ3RoKSB7XG4gICAgICAgICRpdGVtLmFkZENsYXNzKGBpcy1zdWJtZW51LWl0ZW0gJHtzdWJJdGVtQ2xhc3N9YCk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICByZXR1cm47XG4gIH0sXG5cbiAgQnVybihtZW51LCB0eXBlKSB7XG4gICAgdmFyIGl0ZW1zID0gbWVudS5maW5kKCdsaScpLnJlbW92ZUF0dHIoJ3RhYmluZGV4JyksXG4gICAgICAgIHN1Yk1lbnVDbGFzcyA9IGBpcy0ke3R5cGV9LXN1Ym1lbnVgLFxuICAgICAgICBzdWJJdGVtQ2xhc3MgPSBgJHtzdWJNZW51Q2xhc3N9LWl0ZW1gLFxuICAgICAgICBoYXNTdWJDbGFzcyA9IGBpcy0ke3R5cGV9LXN1Ym1lbnUtcGFyZW50YDtcblxuICAgIG1lbnVcbiAgICAgIC5maW5kKCc+bGksIC5tZW51LCAubWVudSA+IGxpJylcbiAgICAgIC5yZW1vdmVDbGFzcyhgJHtzdWJNZW51Q2xhc3N9ICR7c3ViSXRlbUNsYXNzfSAke2hhc1N1YkNsYXNzfSBpcy1zdWJtZW51LWl0ZW0gc3VibWVudSBpcy1hY3RpdmVgKVxuICAgICAgLnJlbW92ZUF0dHIoJ2RhdGEtc3VibWVudScpLmNzcygnZGlzcGxheScsICcnKTtcblxuICAgIC8vIGNvbnNvbGUubG9nKCAgICAgIG1lbnUuZmluZCgnLicgKyBzdWJNZW51Q2xhc3MgKyAnLCAuJyArIHN1Ykl0ZW1DbGFzcyArICcsIC5oYXMtc3VibWVudSwgLmlzLXN1Ym1lbnUtaXRlbSwgLnN1Ym1lbnUsIFtkYXRhLXN1Ym1lbnVdJylcbiAgICAvLyAgICAgICAgICAgLnJlbW92ZUNsYXNzKHN1Yk1lbnVDbGFzcyArICcgJyArIHN1Ykl0ZW1DbGFzcyArICcgaGFzLXN1Ym1lbnUgaXMtc3VibWVudS1pdGVtIHN1Ym1lbnUnKVxuICAgIC8vICAgICAgICAgICAucmVtb3ZlQXR0cignZGF0YS1zdWJtZW51JykpO1xuICAgIC8vIGl0ZW1zLmVhY2goZnVuY3Rpb24oKXtcbiAgICAvLyAgIHZhciAkaXRlbSA9ICQodGhpcyksXG4gICAgLy8gICAgICAgJHN1YiA9ICRpdGVtLmNoaWxkcmVuKCd1bCcpO1xuICAgIC8vICAgaWYoJGl0ZW0ucGFyZW50KCdbZGF0YS1zdWJtZW51XScpLmxlbmd0aCl7XG4gICAgLy8gICAgICRpdGVtLnJlbW92ZUNsYXNzKCdpcy1zdWJtZW51LWl0ZW0gJyArIHN1Ykl0ZW1DbGFzcyk7XG4gICAgLy8gICB9XG4gICAgLy8gICBpZigkc3ViLmxlbmd0aCl7XG4gICAgLy8gICAgICRpdGVtLnJlbW92ZUNsYXNzKCdoYXMtc3VibWVudScpO1xuICAgIC8vICAgICAkc3ViLnJlbW92ZUNsYXNzKCdzdWJtZW51ICcgKyBzdWJNZW51Q2xhc3MpLnJlbW92ZUF0dHIoJ2RhdGEtc3VibWVudScpO1xuICAgIC8vICAgfVxuICAgIC8vIH0pO1xuICB9XG59XG5cbkZvdW5kYXRpb24uTmVzdCA9IE5lc3Q7XG5cbn0oalF1ZXJ5KTtcbiIsIid1c2Ugc3RyaWN0JztcblxuIWZ1bmN0aW9uKCQpIHtcblxuZnVuY3Rpb24gVGltZXIoZWxlbSwgb3B0aW9ucywgY2IpIHtcbiAgdmFyIF90aGlzID0gdGhpcyxcbiAgICAgIGR1cmF0aW9uID0gb3B0aW9ucy5kdXJhdGlvbiwvL29wdGlvbnMgaXMgYW4gb2JqZWN0IGZvciBlYXNpbHkgYWRkaW5nIGZlYXR1cmVzIGxhdGVyLlxuICAgICAgbmFtZVNwYWNlID0gT2JqZWN0LmtleXMoZWxlbS5kYXRhKCkpWzBdIHx8ICd0aW1lcicsXG4gICAgICByZW1haW4gPSAtMSxcbiAgICAgIHN0YXJ0LFxuICAgICAgdGltZXI7XG5cbiAgdGhpcy5pc1BhdXNlZCA9IGZhbHNlO1xuXG4gIHRoaXMucmVzdGFydCA9IGZ1bmN0aW9uKCkge1xuICAgIHJlbWFpbiA9IC0xO1xuICAgIGNsZWFyVGltZW91dCh0aW1lcik7XG4gICAgdGhpcy5zdGFydCgpO1xuICB9XG5cbiAgdGhpcy5zdGFydCA9IGZ1bmN0aW9uKCkge1xuICAgIHRoaXMuaXNQYXVzZWQgPSBmYWxzZTtcbiAgICAvLyBpZighZWxlbS5kYXRhKCdwYXVzZWQnKSl7IHJldHVybiBmYWxzZTsgfS8vbWF5YmUgaW1wbGVtZW50IHRoaXMgc2FuaXR5IGNoZWNrIGlmIHVzZWQgZm9yIG90aGVyIHRoaW5ncy5cbiAgICBjbGVhclRpbWVvdXQodGltZXIpO1xuICAgIHJlbWFpbiA9IHJlbWFpbiA8PSAwID8gZHVyYXRpb24gOiByZW1haW47XG4gICAgZWxlbS5kYXRhKCdwYXVzZWQnLCBmYWxzZSk7XG4gICAgc3RhcnQgPSBEYXRlLm5vdygpO1xuICAgIHRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbigpe1xuICAgICAgaWYob3B0aW9ucy5pbmZpbml0ZSl7XG4gICAgICAgIF90aGlzLnJlc3RhcnQoKTsvL3JlcnVuIHRoZSB0aW1lci5cbiAgICAgIH1cbiAgICAgIGlmIChjYiAmJiB0eXBlb2YgY2IgPT09ICdmdW5jdGlvbicpIHsgY2IoKTsgfVxuICAgIH0sIHJlbWFpbik7XG4gICAgZWxlbS50cmlnZ2VyKGB0aW1lcnN0YXJ0LnpmLiR7bmFtZVNwYWNlfWApO1xuICB9XG5cbiAgdGhpcy5wYXVzZSA9IGZ1bmN0aW9uKCkge1xuICAgIHRoaXMuaXNQYXVzZWQgPSB0cnVlO1xuICAgIC8vaWYoZWxlbS5kYXRhKCdwYXVzZWQnKSl7IHJldHVybiBmYWxzZTsgfS8vbWF5YmUgaW1wbGVtZW50IHRoaXMgc2FuaXR5IGNoZWNrIGlmIHVzZWQgZm9yIG90aGVyIHRoaW5ncy5cbiAgICBjbGVhclRpbWVvdXQodGltZXIpO1xuICAgIGVsZW0uZGF0YSgncGF1c2VkJywgdHJ1ZSk7XG4gICAgdmFyIGVuZCA9IERhdGUubm93KCk7XG4gICAgcmVtYWluID0gcmVtYWluIC0gKGVuZCAtIHN0YXJ0KTtcbiAgICBlbGVtLnRyaWdnZXIoYHRpbWVycGF1c2VkLnpmLiR7bmFtZVNwYWNlfWApO1xuICB9XG59XG5cbi8qKlxuICogUnVucyBhIGNhbGxiYWNrIGZ1bmN0aW9uIHdoZW4gaW1hZ2VzIGFyZSBmdWxseSBsb2FkZWQuXG4gKiBAcGFyYW0ge09iamVjdH0gaW1hZ2VzIC0gSW1hZ2UocykgdG8gY2hlY2sgaWYgbG9hZGVkLlxuICogQHBhcmFtIHtGdW5jfSBjYWxsYmFjayAtIEZ1bmN0aW9uIHRvIGV4ZWN1dGUgd2hlbiBpbWFnZSBpcyBmdWxseSBsb2FkZWQuXG4gKi9cbmZ1bmN0aW9uIG9uSW1hZ2VzTG9hZGVkKGltYWdlcywgY2FsbGJhY2spe1xuICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICB1bmxvYWRlZCA9IGltYWdlcy5sZW5ndGg7XG5cbiAgaWYgKHVubG9hZGVkID09PSAwKSB7XG4gICAgY2FsbGJhY2soKTtcbiAgfVxuXG4gIGltYWdlcy5lYWNoKGZ1bmN0aW9uKCkge1xuICAgIGlmICh0aGlzLmNvbXBsZXRlKSB7XG4gICAgICBzaW5nbGVJbWFnZUxvYWRlZCgpO1xuICAgIH1cbiAgICBlbHNlIGlmICh0eXBlb2YgdGhpcy5uYXR1cmFsV2lkdGggIT09ICd1bmRlZmluZWQnICYmIHRoaXMubmF0dXJhbFdpZHRoID4gMCkge1xuICAgICAgc2luZ2xlSW1hZ2VMb2FkZWQoKTtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICAkKHRoaXMpLm9uZSgnbG9hZCcsIGZ1bmN0aW9uKCkge1xuICAgICAgICBzaW5nbGVJbWFnZUxvYWRlZCgpO1xuICAgICAgfSk7XG4gICAgfVxuICB9KTtcblxuICBmdW5jdGlvbiBzaW5nbGVJbWFnZUxvYWRlZCgpIHtcbiAgICB1bmxvYWRlZC0tO1xuICAgIGlmICh1bmxvYWRlZCA9PT0gMCkge1xuICAgICAgY2FsbGJhY2soKTtcbiAgICB9XG4gIH1cbn1cblxuRm91bmRhdGlvbi5UaW1lciA9IFRpbWVyO1xuRm91bmRhdGlvbi5vbkltYWdlc0xvYWRlZCA9IG9uSW1hZ2VzTG9hZGVkO1xuXG59KGpRdWVyeSk7XG4iLCIvLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4vLyoqV29yayBpbnNwaXJlZCBieSBtdWx0aXBsZSBqcXVlcnkgc3dpcGUgcGx1Z2lucyoqXG4vLyoqRG9uZSBieSBZb2hhaSBBcmFyYXQgKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4vLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4oZnVuY3Rpb24oJCkge1xuXG4gICQuc3BvdFN3aXBlID0ge1xuICAgIHZlcnNpb246ICcxLjAuMCcsXG4gICAgZW5hYmxlZDogJ29udG91Y2hzdGFydCcgaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LFxuICAgIHByZXZlbnREZWZhdWx0OiBmYWxzZSxcbiAgICBtb3ZlVGhyZXNob2xkOiA3NSxcbiAgICB0aW1lVGhyZXNob2xkOiAyMDBcbiAgfTtcblxuICB2YXIgICBzdGFydFBvc1gsXG4gICAgICAgIHN0YXJ0UG9zWSxcbiAgICAgICAgc3RhcnRUaW1lLFxuICAgICAgICBlbGFwc2VkVGltZSxcbiAgICAgICAgaXNNb3ZpbmcgPSBmYWxzZTtcblxuICBmdW5jdGlvbiBvblRvdWNoRW5kKCkge1xuICAgIC8vICBhbGVydCh0aGlzKTtcbiAgICB0aGlzLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3RvdWNobW92ZScsIG9uVG91Y2hNb3ZlKTtcbiAgICB0aGlzLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3RvdWNoZW5kJywgb25Ub3VjaEVuZCk7XG4gICAgaXNNb3ZpbmcgPSBmYWxzZTtcbiAgfVxuXG4gIGZ1bmN0aW9uIG9uVG91Y2hNb3ZlKGUpIHtcbiAgICBpZiAoJC5zcG90U3dpcGUucHJldmVudERlZmF1bHQpIHsgZS5wcmV2ZW50RGVmYXVsdCgpOyB9XG4gICAgaWYoaXNNb3ZpbmcpIHtcbiAgICAgIHZhciB4ID0gZS50b3VjaGVzWzBdLnBhZ2VYO1xuICAgICAgdmFyIHkgPSBlLnRvdWNoZXNbMF0ucGFnZVk7XG4gICAgICB2YXIgZHggPSBzdGFydFBvc1ggLSB4O1xuICAgICAgdmFyIGR5ID0gc3RhcnRQb3NZIC0geTtcbiAgICAgIHZhciBkaXI7XG4gICAgICBlbGFwc2VkVGltZSA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpIC0gc3RhcnRUaW1lO1xuICAgICAgaWYoTWF0aC5hYnMoZHgpID49ICQuc3BvdFN3aXBlLm1vdmVUaHJlc2hvbGQgJiYgZWxhcHNlZFRpbWUgPD0gJC5zcG90U3dpcGUudGltZVRocmVzaG9sZCkge1xuICAgICAgICBkaXIgPSBkeCA+IDAgPyAnbGVmdCcgOiAncmlnaHQnO1xuICAgICAgfVxuICAgICAgLy8gZWxzZSBpZihNYXRoLmFicyhkeSkgPj0gJC5zcG90U3dpcGUubW92ZVRocmVzaG9sZCAmJiBlbGFwc2VkVGltZSA8PSAkLnNwb3RTd2lwZS50aW1lVGhyZXNob2xkKSB7XG4gICAgICAvLyAgIGRpciA9IGR5ID4gMCA/ICdkb3duJyA6ICd1cCc7XG4gICAgICAvLyB9XG4gICAgICBpZihkaXIpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBvblRvdWNoRW5kLmNhbGwodGhpcyk7XG4gICAgICAgICQodGhpcykudHJpZ2dlcignc3dpcGUnLCBkaXIpLnRyaWdnZXIoYHN3aXBlJHtkaXJ9YCk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gb25Ub3VjaFN0YXJ0KGUpIHtcbiAgICBpZiAoZS50b3VjaGVzLmxlbmd0aCA9PSAxKSB7XG4gICAgICBzdGFydFBvc1ggPSBlLnRvdWNoZXNbMF0ucGFnZVg7XG4gICAgICBzdGFydFBvc1kgPSBlLnRvdWNoZXNbMF0ucGFnZVk7XG4gICAgICBpc01vdmluZyA9IHRydWU7XG4gICAgICBzdGFydFRpbWUgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICAgIHRoaXMuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2htb3ZlJywgb25Ub3VjaE1vdmUsIGZhbHNlKTtcbiAgICAgIHRoaXMuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hlbmQnLCBvblRvdWNoRW5kLCBmYWxzZSk7XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gaW5pdCgpIHtcbiAgICB0aGlzLmFkZEV2ZW50TGlzdGVuZXIgJiYgdGhpcy5hZGRFdmVudExpc3RlbmVyKCd0b3VjaHN0YXJ0Jywgb25Ub3VjaFN0YXJ0LCBmYWxzZSk7XG4gIH1cblxuICBmdW5jdGlvbiB0ZWFyZG93bigpIHtcbiAgICB0aGlzLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3RvdWNoc3RhcnQnLCBvblRvdWNoU3RhcnQpO1xuICB9XG5cbiAgJC5ldmVudC5zcGVjaWFsLnN3aXBlID0geyBzZXR1cDogaW5pdCB9O1xuXG4gICQuZWFjaChbJ2xlZnQnLCAndXAnLCAnZG93bicsICdyaWdodCddLCBmdW5jdGlvbiAoKSB7XG4gICAgJC5ldmVudC5zcGVjaWFsW2Bzd2lwZSR7dGhpc31gXSA9IHsgc2V0dXA6IGZ1bmN0aW9uKCl7XG4gICAgICAkKHRoaXMpLm9uKCdzd2lwZScsICQubm9vcCk7XG4gICAgfSB9O1xuICB9KTtcbn0pKGpRdWVyeSk7XG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICogTWV0aG9kIGZvciBhZGRpbmcgcHN1ZWRvIGRyYWcgZXZlbnRzIHRvIGVsZW1lbnRzICpcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4hZnVuY3Rpb24oJCl7XG4gICQuZm4uYWRkVG91Y2ggPSBmdW5jdGlvbigpe1xuICAgIHRoaXMuZWFjaChmdW5jdGlvbihpLGVsKXtcbiAgICAgICQoZWwpLmJpbmQoJ3RvdWNoc3RhcnQgdG91Y2htb3ZlIHRvdWNoZW5kIHRvdWNoY2FuY2VsJyxmdW5jdGlvbigpe1xuICAgICAgICAvL3dlIHBhc3MgdGhlIG9yaWdpbmFsIGV2ZW50IG9iamVjdCBiZWNhdXNlIHRoZSBqUXVlcnkgZXZlbnRcbiAgICAgICAgLy9vYmplY3QgaXMgbm9ybWFsaXplZCB0byB3M2Mgc3BlY3MgYW5kIGRvZXMgbm90IHByb3ZpZGUgdGhlIFRvdWNoTGlzdFxuICAgICAgICBoYW5kbGVUb3VjaChldmVudCk7XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIHZhciBoYW5kbGVUb3VjaCA9IGZ1bmN0aW9uKGV2ZW50KXtcbiAgICAgIHZhciB0b3VjaGVzID0gZXZlbnQuY2hhbmdlZFRvdWNoZXMsXG4gICAgICAgICAgZmlyc3QgPSB0b3VjaGVzWzBdLFxuICAgICAgICAgIGV2ZW50VHlwZXMgPSB7XG4gICAgICAgICAgICB0b3VjaHN0YXJ0OiAnbW91c2Vkb3duJyxcbiAgICAgICAgICAgIHRvdWNobW92ZTogJ21vdXNlbW92ZScsXG4gICAgICAgICAgICB0b3VjaGVuZDogJ21vdXNldXAnXG4gICAgICAgICAgfSxcbiAgICAgICAgICB0eXBlID0gZXZlbnRUeXBlc1tldmVudC50eXBlXSxcbiAgICAgICAgICBzaW11bGF0ZWRFdmVudFxuICAgICAgICA7XG5cbiAgICAgIGlmKCdNb3VzZUV2ZW50JyBpbiB3aW5kb3cgJiYgdHlwZW9mIHdpbmRvdy5Nb3VzZUV2ZW50ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIHNpbXVsYXRlZEV2ZW50ID0gbmV3IHdpbmRvdy5Nb3VzZUV2ZW50KHR5cGUsIHtcbiAgICAgICAgICAnYnViYmxlcyc6IHRydWUsXG4gICAgICAgICAgJ2NhbmNlbGFibGUnOiB0cnVlLFxuICAgICAgICAgICdzY3JlZW5YJzogZmlyc3Quc2NyZWVuWCxcbiAgICAgICAgICAnc2NyZWVuWSc6IGZpcnN0LnNjcmVlblksXG4gICAgICAgICAgJ2NsaWVudFgnOiBmaXJzdC5jbGllbnRYLFxuICAgICAgICAgICdjbGllbnRZJzogZmlyc3QuY2xpZW50WVxuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHNpbXVsYXRlZEV2ZW50ID0gZG9jdW1lbnQuY3JlYXRlRXZlbnQoJ01vdXNlRXZlbnQnKTtcbiAgICAgICAgc2ltdWxhdGVkRXZlbnQuaW5pdE1vdXNlRXZlbnQodHlwZSwgdHJ1ZSwgdHJ1ZSwgd2luZG93LCAxLCBmaXJzdC5zY3JlZW5YLCBmaXJzdC5zY3JlZW5ZLCBmaXJzdC5jbGllbnRYLCBmaXJzdC5jbGllbnRZLCBmYWxzZSwgZmFsc2UsIGZhbHNlLCBmYWxzZSwgMC8qbGVmdCovLCBudWxsKTtcbiAgICAgIH1cbiAgICAgIGZpcnN0LnRhcmdldC5kaXNwYXRjaEV2ZW50KHNpbXVsYXRlZEV2ZW50KTtcbiAgICB9O1xuICB9O1xufShqUXVlcnkpO1xuXG5cbi8vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuLy8qKkZyb20gdGhlIGpRdWVyeSBNb2JpbGUgTGlicmFyeSoqXG4vLyoqbmVlZCB0byByZWNyZWF0ZSBmdW5jdGlvbmFsaXR5Kipcbi8vKiphbmQgdHJ5IHRvIGltcHJvdmUgaWYgcG9zc2libGUqKlxuLy8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG5cbi8qIFJlbW92aW5nIHRoZSBqUXVlcnkgZnVuY3Rpb24gKioqKlxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG5cbihmdW5jdGlvbiggJCwgd2luZG93LCB1bmRlZmluZWQgKSB7XG5cblx0dmFyICRkb2N1bWVudCA9ICQoIGRvY3VtZW50ICksXG5cdFx0Ly8gc3VwcG9ydFRvdWNoID0gJC5tb2JpbGUuc3VwcG9ydC50b3VjaCxcblx0XHR0b3VjaFN0YXJ0RXZlbnQgPSAndG91Y2hzdGFydCcvL3N1cHBvcnRUb3VjaCA/IFwidG91Y2hzdGFydFwiIDogXCJtb3VzZWRvd25cIixcblx0XHR0b3VjaFN0b3BFdmVudCA9ICd0b3VjaGVuZCcvL3N1cHBvcnRUb3VjaCA/IFwidG91Y2hlbmRcIiA6IFwibW91c2V1cFwiLFxuXHRcdHRvdWNoTW92ZUV2ZW50ID0gJ3RvdWNobW92ZScvL3N1cHBvcnRUb3VjaCA/IFwidG91Y2htb3ZlXCIgOiBcIm1vdXNlbW92ZVwiO1xuXG5cdC8vIHNldHVwIG5ldyBldmVudCBzaG9ydGN1dHNcblx0JC5lYWNoKCAoIFwidG91Y2hzdGFydCB0b3VjaG1vdmUgdG91Y2hlbmQgXCIgK1xuXHRcdFwic3dpcGUgc3dpcGVsZWZ0IHN3aXBlcmlnaHRcIiApLnNwbGl0KCBcIiBcIiApLCBmdW5jdGlvbiggaSwgbmFtZSApIHtcblxuXHRcdCQuZm5bIG5hbWUgXSA9IGZ1bmN0aW9uKCBmbiApIHtcblx0XHRcdHJldHVybiBmbiA/IHRoaXMuYmluZCggbmFtZSwgZm4gKSA6IHRoaXMudHJpZ2dlciggbmFtZSApO1xuXHRcdH07XG5cblx0XHQvLyBqUXVlcnkgPCAxLjhcblx0XHRpZiAoICQuYXR0ckZuICkge1xuXHRcdFx0JC5hdHRyRm5bIG5hbWUgXSA9IHRydWU7XG5cdFx0fVxuXHR9KTtcblxuXHRmdW5jdGlvbiB0cmlnZ2VyQ3VzdG9tRXZlbnQoIG9iaiwgZXZlbnRUeXBlLCBldmVudCwgYnViYmxlICkge1xuXHRcdHZhciBvcmlnaW5hbFR5cGUgPSBldmVudC50eXBlO1xuXHRcdGV2ZW50LnR5cGUgPSBldmVudFR5cGU7XG5cdFx0aWYgKCBidWJibGUgKSB7XG5cdFx0XHQkLmV2ZW50LnRyaWdnZXIoIGV2ZW50LCB1bmRlZmluZWQsIG9iaiApO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHQkLmV2ZW50LmRpc3BhdGNoLmNhbGwoIG9iaiwgZXZlbnQgKTtcblx0XHR9XG5cdFx0ZXZlbnQudHlwZSA9IG9yaWdpbmFsVHlwZTtcblx0fVxuXG5cdC8vIGFsc28gaGFuZGxlcyB0YXBob2xkXG5cblx0Ly8gQWxzbyBoYW5kbGVzIHN3aXBlbGVmdCwgc3dpcGVyaWdodFxuXHQkLmV2ZW50LnNwZWNpYWwuc3dpcGUgPSB7XG5cblx0XHQvLyBNb3JlIHRoYW4gdGhpcyBob3Jpem9udGFsIGRpc3BsYWNlbWVudCwgYW5kIHdlIHdpbGwgc3VwcHJlc3Mgc2Nyb2xsaW5nLlxuXHRcdHNjcm9sbFN1cHJlc3Npb25UaHJlc2hvbGQ6IDMwLFxuXG5cdFx0Ly8gTW9yZSB0aW1lIHRoYW4gdGhpcywgYW5kIGl0IGlzbid0IGEgc3dpcGUuXG5cdFx0ZHVyYXRpb25UaHJlc2hvbGQ6IDEwMDAsXG5cblx0XHQvLyBTd2lwZSBob3Jpem9udGFsIGRpc3BsYWNlbWVudCBtdXN0IGJlIG1vcmUgdGhhbiB0aGlzLlxuXHRcdGhvcml6b250YWxEaXN0YW5jZVRocmVzaG9sZDogd2luZG93LmRldmljZVBpeGVsUmF0aW8gPj0gMiA/IDE1IDogMzAsXG5cblx0XHQvLyBTd2lwZSB2ZXJ0aWNhbCBkaXNwbGFjZW1lbnQgbXVzdCBiZSBsZXNzIHRoYW4gdGhpcy5cblx0XHR2ZXJ0aWNhbERpc3RhbmNlVGhyZXNob2xkOiB3aW5kb3cuZGV2aWNlUGl4ZWxSYXRpbyA+PSAyID8gMTUgOiAzMCxcblxuXHRcdGdldExvY2F0aW9uOiBmdW5jdGlvbiAoIGV2ZW50ICkge1xuXHRcdFx0dmFyIHdpblBhZ2VYID0gd2luZG93LnBhZ2VYT2Zmc2V0LFxuXHRcdFx0XHR3aW5QYWdlWSA9IHdpbmRvdy5wYWdlWU9mZnNldCxcblx0XHRcdFx0eCA9IGV2ZW50LmNsaWVudFgsXG5cdFx0XHRcdHkgPSBldmVudC5jbGllbnRZO1xuXG5cdFx0XHRpZiAoIGV2ZW50LnBhZ2VZID09PSAwICYmIE1hdGguZmxvb3IoIHkgKSA+IE1hdGguZmxvb3IoIGV2ZW50LnBhZ2VZICkgfHxcblx0XHRcdFx0ZXZlbnQucGFnZVggPT09IDAgJiYgTWF0aC5mbG9vciggeCApID4gTWF0aC5mbG9vciggZXZlbnQucGFnZVggKSApIHtcblxuXHRcdFx0XHQvLyBpT1M0IGNsaWVudFgvY2xpZW50WSBoYXZlIHRoZSB2YWx1ZSB0aGF0IHNob3VsZCBoYXZlIGJlZW5cblx0XHRcdFx0Ly8gaW4gcGFnZVgvcGFnZVkuIFdoaWxlIHBhZ2VYL3BhZ2UvIGhhdmUgdGhlIHZhbHVlIDBcblx0XHRcdFx0eCA9IHggLSB3aW5QYWdlWDtcblx0XHRcdFx0eSA9IHkgLSB3aW5QYWdlWTtcblx0XHRcdH0gZWxzZSBpZiAoIHkgPCAoIGV2ZW50LnBhZ2VZIC0gd2luUGFnZVkpIHx8IHggPCAoIGV2ZW50LnBhZ2VYIC0gd2luUGFnZVggKSApIHtcblxuXHRcdFx0XHQvLyBTb21lIEFuZHJvaWQgYnJvd3NlcnMgaGF2ZSB0b3RhbGx5IGJvZ3VzIHZhbHVlcyBmb3IgY2xpZW50WC9ZXG5cdFx0XHRcdC8vIHdoZW4gc2Nyb2xsaW5nL3pvb21pbmcgYSBwYWdlLiBEZXRlY3RhYmxlIHNpbmNlIGNsaWVudFgvY2xpZW50WVxuXHRcdFx0XHQvLyBzaG91bGQgbmV2ZXIgYmUgc21hbGxlciB0aGFuIHBhZ2VYL3BhZ2VZIG1pbnVzIHBhZ2Ugc2Nyb2xsXG5cdFx0XHRcdHggPSBldmVudC5wYWdlWCAtIHdpblBhZ2VYO1xuXHRcdFx0XHR5ID0gZXZlbnQucGFnZVkgLSB3aW5QYWdlWTtcblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0eDogeCxcblx0XHRcdFx0eTogeVxuXHRcdFx0fTtcblx0XHR9LFxuXG5cdFx0c3RhcnQ6IGZ1bmN0aW9uKCBldmVudCApIHtcblx0XHRcdHZhciBkYXRhID0gZXZlbnQub3JpZ2luYWxFdmVudC50b3VjaGVzID9cblx0XHRcdFx0XHRldmVudC5vcmlnaW5hbEV2ZW50LnRvdWNoZXNbIDAgXSA6IGV2ZW50LFxuXHRcdFx0XHRsb2NhdGlvbiA9ICQuZXZlbnQuc3BlY2lhbC5zd2lwZS5nZXRMb2NhdGlvbiggZGF0YSApO1xuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0XHRcdHRpbWU6ICggbmV3IERhdGUoKSApLmdldFRpbWUoKSxcblx0XHRcdFx0XHRcdGNvb3JkczogWyBsb2NhdGlvbi54LCBsb2NhdGlvbi55IF0sXG5cdFx0XHRcdFx0XHRvcmlnaW46ICQoIGV2ZW50LnRhcmdldCApXG5cdFx0XHRcdFx0fTtcblx0XHR9LFxuXG5cdFx0c3RvcDogZnVuY3Rpb24oIGV2ZW50ICkge1xuXHRcdFx0dmFyIGRhdGEgPSBldmVudC5vcmlnaW5hbEV2ZW50LnRvdWNoZXMgP1xuXHRcdFx0XHRcdGV2ZW50Lm9yaWdpbmFsRXZlbnQudG91Y2hlc1sgMCBdIDogZXZlbnQsXG5cdFx0XHRcdGxvY2F0aW9uID0gJC5ldmVudC5zcGVjaWFsLnN3aXBlLmdldExvY2F0aW9uKCBkYXRhICk7XG5cdFx0XHRyZXR1cm4ge1xuXHRcdFx0XHRcdFx0dGltZTogKCBuZXcgRGF0ZSgpICkuZ2V0VGltZSgpLFxuXHRcdFx0XHRcdFx0Y29vcmRzOiBbIGxvY2F0aW9uLngsIGxvY2F0aW9uLnkgXVxuXHRcdFx0XHRcdH07XG5cdFx0fSxcblxuXHRcdGhhbmRsZVN3aXBlOiBmdW5jdGlvbiggc3RhcnQsIHN0b3AsIHRoaXNPYmplY3QsIG9yaWdUYXJnZXQgKSB7XG5cdFx0XHRpZiAoIHN0b3AudGltZSAtIHN0YXJ0LnRpbWUgPCAkLmV2ZW50LnNwZWNpYWwuc3dpcGUuZHVyYXRpb25UaHJlc2hvbGQgJiZcblx0XHRcdFx0TWF0aC5hYnMoIHN0YXJ0LmNvb3Jkc1sgMCBdIC0gc3RvcC5jb29yZHNbIDAgXSApID4gJC5ldmVudC5zcGVjaWFsLnN3aXBlLmhvcml6b250YWxEaXN0YW5jZVRocmVzaG9sZCAmJlxuXHRcdFx0XHRNYXRoLmFicyggc3RhcnQuY29vcmRzWyAxIF0gLSBzdG9wLmNvb3Jkc1sgMSBdICkgPCAkLmV2ZW50LnNwZWNpYWwuc3dpcGUudmVydGljYWxEaXN0YW5jZVRocmVzaG9sZCApIHtcblx0XHRcdFx0dmFyIGRpcmVjdGlvbiA9IHN0YXJ0LmNvb3Jkc1swXSA+IHN0b3AuY29vcmRzWyAwIF0gPyBcInN3aXBlbGVmdFwiIDogXCJzd2lwZXJpZ2h0XCI7XG5cblx0XHRcdFx0dHJpZ2dlckN1c3RvbUV2ZW50KCB0aGlzT2JqZWN0LCBcInN3aXBlXCIsICQuRXZlbnQoIFwic3dpcGVcIiwgeyB0YXJnZXQ6IG9yaWdUYXJnZXQsIHN3aXBlc3RhcnQ6IHN0YXJ0LCBzd2lwZXN0b3A6IHN0b3AgfSksIHRydWUgKTtcblx0XHRcdFx0dHJpZ2dlckN1c3RvbUV2ZW50KCB0aGlzT2JqZWN0LCBkaXJlY3Rpb24sJC5FdmVudCggZGlyZWN0aW9uLCB7IHRhcmdldDogb3JpZ1RhcmdldCwgc3dpcGVzdGFydDogc3RhcnQsIHN3aXBlc3RvcDogc3RvcCB9ICksIHRydWUgKTtcblx0XHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0XHR9XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cblx0XHR9LFxuXG5cdFx0Ly8gVGhpcyBzZXJ2ZXMgYXMgYSBmbGFnIHRvIGVuc3VyZSB0aGF0IGF0IG1vc3Qgb25lIHN3aXBlIGV2ZW50IGV2ZW50IGlzXG5cdFx0Ly8gaW4gd29yayBhdCBhbnkgZ2l2ZW4gdGltZVxuXHRcdGV2ZW50SW5Qcm9ncmVzczogZmFsc2UsXG5cblx0XHRzZXR1cDogZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgZXZlbnRzLFxuXHRcdFx0XHR0aGlzT2JqZWN0ID0gdGhpcyxcblx0XHRcdFx0JHRoaXMgPSAkKCB0aGlzT2JqZWN0ICksXG5cdFx0XHRcdGNvbnRleHQgPSB7fTtcblxuXHRcdFx0Ly8gUmV0cmlldmUgdGhlIGV2ZW50cyBkYXRhIGZvciB0aGlzIGVsZW1lbnQgYW5kIGFkZCB0aGUgc3dpcGUgY29udGV4dFxuXHRcdFx0ZXZlbnRzID0gJC5kYXRhKCB0aGlzLCBcIm1vYmlsZS1ldmVudHNcIiApO1xuXHRcdFx0aWYgKCAhZXZlbnRzICkge1xuXHRcdFx0XHRldmVudHMgPSB7IGxlbmd0aDogMCB9O1xuXHRcdFx0XHQkLmRhdGEoIHRoaXMsIFwibW9iaWxlLWV2ZW50c1wiLCBldmVudHMgKTtcblx0XHRcdH1cblx0XHRcdGV2ZW50cy5sZW5ndGgrKztcblx0XHRcdGV2ZW50cy5zd2lwZSA9IGNvbnRleHQ7XG5cblx0XHRcdGNvbnRleHQuc3RhcnQgPSBmdW5jdGlvbiggZXZlbnQgKSB7XG5cblx0XHRcdFx0Ly8gQmFpbCBpZiB3ZSdyZSBhbHJlYWR5IHdvcmtpbmcgb24gYSBzd2lwZSBldmVudFxuXHRcdFx0XHRpZiAoICQuZXZlbnQuc3BlY2lhbC5zd2lwZS5ldmVudEluUHJvZ3Jlc3MgKSB7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdCQuZXZlbnQuc3BlY2lhbC5zd2lwZS5ldmVudEluUHJvZ3Jlc3MgPSB0cnVlO1xuXG5cdFx0XHRcdHZhciBzdG9wLFxuXHRcdFx0XHRcdHN0YXJ0ID0gJC5ldmVudC5zcGVjaWFsLnN3aXBlLnN0YXJ0KCBldmVudCApLFxuXHRcdFx0XHRcdG9yaWdUYXJnZXQgPSBldmVudC50YXJnZXQsXG5cdFx0XHRcdFx0ZW1pdHRlZCA9IGZhbHNlO1xuXG5cdFx0XHRcdGNvbnRleHQubW92ZSA9IGZ1bmN0aW9uKCBldmVudCApIHtcblx0XHRcdFx0XHRpZiAoICFzdGFydCB8fCBldmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSApIHtcblx0XHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRzdG9wID0gJC5ldmVudC5zcGVjaWFsLnN3aXBlLnN0b3AoIGV2ZW50ICk7XG5cdFx0XHRcdFx0aWYgKCAhZW1pdHRlZCApIHtcblx0XHRcdFx0XHRcdGVtaXR0ZWQgPSAkLmV2ZW50LnNwZWNpYWwuc3dpcGUuaGFuZGxlU3dpcGUoIHN0YXJ0LCBzdG9wLCB0aGlzT2JqZWN0LCBvcmlnVGFyZ2V0ICk7XG5cdFx0XHRcdFx0XHRpZiAoIGVtaXR0ZWQgKSB7XG5cblx0XHRcdFx0XHRcdFx0Ly8gUmVzZXQgdGhlIGNvbnRleHQgdG8gbWFrZSB3YXkgZm9yIHRoZSBuZXh0IHN3aXBlIGV2ZW50XG5cdFx0XHRcdFx0XHRcdCQuZXZlbnQuc3BlY2lhbC5zd2lwZS5ldmVudEluUHJvZ3Jlc3MgPSBmYWxzZTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0Ly8gcHJldmVudCBzY3JvbGxpbmdcblx0XHRcdFx0XHRpZiAoIE1hdGguYWJzKCBzdGFydC5jb29yZHNbIDAgXSAtIHN0b3AuY29vcmRzWyAwIF0gKSA+ICQuZXZlbnQuc3BlY2lhbC5zd2lwZS5zY3JvbGxTdXByZXNzaW9uVGhyZXNob2xkICkge1xuXHRcdFx0XHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH07XG5cblx0XHRcdFx0Y29udGV4dC5zdG9wID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRlbWl0dGVkID0gdHJ1ZTtcblxuXHRcdFx0XHRcdFx0Ly8gUmVzZXQgdGhlIGNvbnRleHQgdG8gbWFrZSB3YXkgZm9yIHRoZSBuZXh0IHN3aXBlIGV2ZW50XG5cdFx0XHRcdFx0XHQkLmV2ZW50LnNwZWNpYWwuc3dpcGUuZXZlbnRJblByb2dyZXNzID0gZmFsc2U7XG5cdFx0XHRcdFx0XHQkZG9jdW1lbnQub2ZmKCB0b3VjaE1vdmVFdmVudCwgY29udGV4dC5tb3ZlICk7XG5cdFx0XHRcdFx0XHRjb250ZXh0Lm1vdmUgPSBudWxsO1xuXHRcdFx0XHR9O1xuXG5cdFx0XHRcdCRkb2N1bWVudC5vbiggdG91Y2hNb3ZlRXZlbnQsIGNvbnRleHQubW92ZSApXG5cdFx0XHRcdFx0Lm9uZSggdG91Y2hTdG9wRXZlbnQsIGNvbnRleHQuc3RvcCApO1xuXHRcdFx0fTtcblx0XHRcdCR0aGlzLm9uKCB0b3VjaFN0YXJ0RXZlbnQsIGNvbnRleHQuc3RhcnQgKTtcblx0XHR9LFxuXG5cdFx0dGVhcmRvd246IGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIGV2ZW50cywgY29udGV4dDtcblxuXHRcdFx0ZXZlbnRzID0gJC5kYXRhKCB0aGlzLCBcIm1vYmlsZS1ldmVudHNcIiApO1xuXHRcdFx0aWYgKCBldmVudHMgKSB7XG5cdFx0XHRcdGNvbnRleHQgPSBldmVudHMuc3dpcGU7XG5cdFx0XHRcdGRlbGV0ZSBldmVudHMuc3dpcGU7XG5cdFx0XHRcdGV2ZW50cy5sZW5ndGgtLTtcblx0XHRcdFx0aWYgKCBldmVudHMubGVuZ3RoID09PSAwICkge1xuXHRcdFx0XHRcdCQucmVtb3ZlRGF0YSggdGhpcywgXCJtb2JpbGUtZXZlbnRzXCIgKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRpZiAoIGNvbnRleHQgKSB7XG5cdFx0XHRcdGlmICggY29udGV4dC5zdGFydCApIHtcblx0XHRcdFx0XHQkKCB0aGlzICkub2ZmKCB0b3VjaFN0YXJ0RXZlbnQsIGNvbnRleHQuc3RhcnQgKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAoIGNvbnRleHQubW92ZSApIHtcblx0XHRcdFx0XHQkZG9jdW1lbnQub2ZmKCB0b3VjaE1vdmVFdmVudCwgY29udGV4dC5tb3ZlICk7XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKCBjb250ZXh0LnN0b3AgKSB7XG5cdFx0XHRcdFx0JGRvY3VtZW50Lm9mZiggdG91Y2hTdG9wRXZlbnQsIGNvbnRleHQuc3RvcCApO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fVxuXHR9O1xuXHQkLmVhY2goe1xuXHRcdHN3aXBlbGVmdDogXCJzd2lwZS5sZWZ0XCIsXG5cdFx0c3dpcGVyaWdodDogXCJzd2lwZS5yaWdodFwiXG5cdH0sIGZ1bmN0aW9uKCBldmVudCwgc291cmNlRXZlbnQgKSB7XG5cblx0XHQkLmV2ZW50LnNwZWNpYWxbIGV2ZW50IF0gPSB7XG5cdFx0XHRzZXR1cDogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdCQoIHRoaXMgKS5iaW5kKCBzb3VyY2VFdmVudCwgJC5ub29wICk7XG5cdFx0XHR9LFxuXHRcdFx0dGVhcmRvd246IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkKCB0aGlzICkudW5iaW5kKCBzb3VyY2VFdmVudCApO1xuXHRcdFx0fVxuXHRcdH07XG5cdH0pO1xufSkoIGpRdWVyeSwgdGhpcyApO1xuKi9cbiIsIid1c2Ugc3RyaWN0JztcblxuIWZ1bmN0aW9uKCQpIHtcblxuY29uc3QgTXV0YXRpb25PYnNlcnZlciA9IChmdW5jdGlvbiAoKSB7XG4gIHZhciBwcmVmaXhlcyA9IFsnV2ViS2l0JywgJ01veicsICdPJywgJ01zJywgJyddO1xuICBmb3IgKHZhciBpPTA7IGkgPCBwcmVmaXhlcy5sZW5ndGg7IGkrKykge1xuICAgIGlmIChgJHtwcmVmaXhlc1tpXX1NdXRhdGlvbk9ic2VydmVyYCBpbiB3aW5kb3cpIHtcbiAgICAgIHJldHVybiB3aW5kb3dbYCR7cHJlZml4ZXNbaV19TXV0YXRpb25PYnNlcnZlcmBdO1xuICAgIH1cbiAgfVxuICByZXR1cm4gZmFsc2U7XG59KCkpO1xuXG5jb25zdCB0cmlnZ2VycyA9IChlbCwgdHlwZSkgPT4ge1xuICBlbC5kYXRhKHR5cGUpLnNwbGl0KCcgJykuZm9yRWFjaChpZCA9PiB7XG4gICAgJChgIyR7aWR9YClbIHR5cGUgPT09ICdjbG9zZScgPyAndHJpZ2dlcicgOiAndHJpZ2dlckhhbmRsZXInXShgJHt0eXBlfS56Zi50cmlnZ2VyYCwgW2VsXSk7XG4gIH0pO1xufTtcbi8vIEVsZW1lbnRzIHdpdGggW2RhdGEtb3Blbl0gd2lsbCByZXZlYWwgYSBwbHVnaW4gdGhhdCBzdXBwb3J0cyBpdCB3aGVuIGNsaWNrZWQuXG4kKGRvY3VtZW50KS5vbignY2xpY2suemYudHJpZ2dlcicsICdbZGF0YS1vcGVuXScsIGZ1bmN0aW9uKCkge1xuICB0cmlnZ2VycygkKHRoaXMpLCAnb3BlbicpO1xufSk7XG5cbi8vIEVsZW1lbnRzIHdpdGggW2RhdGEtY2xvc2VdIHdpbGwgY2xvc2UgYSBwbHVnaW4gdGhhdCBzdXBwb3J0cyBpdCB3aGVuIGNsaWNrZWQuXG4vLyBJZiB1c2VkIHdpdGhvdXQgYSB2YWx1ZSBvbiBbZGF0YS1jbG9zZV0sIHRoZSBldmVudCB3aWxsIGJ1YmJsZSwgYWxsb3dpbmcgaXQgdG8gY2xvc2UgYSBwYXJlbnQgY29tcG9uZW50LlxuJChkb2N1bWVudCkub24oJ2NsaWNrLnpmLnRyaWdnZXInLCAnW2RhdGEtY2xvc2VdJywgZnVuY3Rpb24oKSB7XG4gIGxldCBpZCA9ICQodGhpcykuZGF0YSgnY2xvc2UnKTtcbiAgaWYgKGlkKSB7XG4gICAgdHJpZ2dlcnMoJCh0aGlzKSwgJ2Nsb3NlJyk7XG4gIH1cbiAgZWxzZSB7XG4gICAgJCh0aGlzKS50cmlnZ2VyKCdjbG9zZS56Zi50cmlnZ2VyJyk7XG4gIH1cbn0pO1xuXG4vLyBFbGVtZW50cyB3aXRoIFtkYXRhLXRvZ2dsZV0gd2lsbCB0b2dnbGUgYSBwbHVnaW4gdGhhdCBzdXBwb3J0cyBpdCB3aGVuIGNsaWNrZWQuXG4kKGRvY3VtZW50KS5vbignY2xpY2suemYudHJpZ2dlcicsICdbZGF0YS10b2dnbGVdJywgZnVuY3Rpb24oKSB7XG4gIHRyaWdnZXJzKCQodGhpcyksICd0b2dnbGUnKTtcbn0pO1xuXG4vLyBFbGVtZW50cyB3aXRoIFtkYXRhLWNsb3NhYmxlXSB3aWxsIHJlc3BvbmQgdG8gY2xvc2UuemYudHJpZ2dlciBldmVudHMuXG4kKGRvY3VtZW50KS5vbignY2xvc2UuemYudHJpZ2dlcicsICdbZGF0YS1jbG9zYWJsZV0nLCBmdW5jdGlvbihlKXtcbiAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgbGV0IGFuaW1hdGlvbiA9ICQodGhpcykuZGF0YSgnY2xvc2FibGUnKTtcblxuICBpZihhbmltYXRpb24gIT09ICcnKXtcbiAgICBGb3VuZGF0aW9uLk1vdGlvbi5hbmltYXRlT3V0KCQodGhpcyksIGFuaW1hdGlvbiwgZnVuY3Rpb24oKSB7XG4gICAgICAkKHRoaXMpLnRyaWdnZXIoJ2Nsb3NlZC56ZicpO1xuICAgIH0pO1xuICB9ZWxzZXtcbiAgICAkKHRoaXMpLmZhZGVPdXQoKS50cmlnZ2VyKCdjbG9zZWQuemYnKTtcbiAgfVxufSk7XG5cbiQoZG9jdW1lbnQpLm9uKCdmb2N1cy56Zi50cmlnZ2VyIGJsdXIuemYudHJpZ2dlcicsICdbZGF0YS10b2dnbGUtZm9jdXNdJywgZnVuY3Rpb24oKSB7XG4gIGxldCBpZCA9ICQodGhpcykuZGF0YSgndG9nZ2xlLWZvY3VzJyk7XG4gICQoYCMke2lkfWApLnRyaWdnZXJIYW5kbGVyKCd0b2dnbGUuemYudHJpZ2dlcicsIFskKHRoaXMpXSk7XG59KTtcblxuLyoqXG4qIEZpcmVzIG9uY2UgYWZ0ZXIgYWxsIG90aGVyIHNjcmlwdHMgaGF2ZSBsb2FkZWRcbiogQGZ1bmN0aW9uXG4qIEBwcml2YXRlXG4qL1xuJCh3aW5kb3cpLm9uKCdsb2FkJywgKCkgPT4ge1xuICBjaGVja0xpc3RlbmVycygpO1xufSk7XG5cbmZ1bmN0aW9uIGNoZWNrTGlzdGVuZXJzKCkge1xuICBldmVudHNMaXN0ZW5lcigpO1xuICByZXNpemVMaXN0ZW5lcigpO1xuICBzY3JvbGxMaXN0ZW5lcigpO1xuICBjbG9zZW1lTGlzdGVuZXIoKTtcbn1cblxuLy8qKioqKioqKiBvbmx5IGZpcmVzIHRoaXMgZnVuY3Rpb24gb25jZSBvbiBsb2FkLCBpZiB0aGVyZSdzIHNvbWV0aGluZyB0byB3YXRjaCAqKioqKioqKlxuZnVuY3Rpb24gY2xvc2VtZUxpc3RlbmVyKHBsdWdpbk5hbWUpIHtcbiAgdmFyIHlldGlCb3hlcyA9ICQoJ1tkYXRhLXlldGktYm94XScpLFxuICAgICAgcGx1Z05hbWVzID0gWydkcm9wZG93bicsICd0b29sdGlwJywgJ3JldmVhbCddO1xuXG4gIGlmKHBsdWdpbk5hbWUpe1xuICAgIGlmKHR5cGVvZiBwbHVnaW5OYW1lID09PSAnc3RyaW5nJyl7XG4gICAgICBwbHVnTmFtZXMucHVzaChwbHVnaW5OYW1lKTtcbiAgICB9ZWxzZSBpZih0eXBlb2YgcGx1Z2luTmFtZSA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIHBsdWdpbk5hbWVbMF0gPT09ICdzdHJpbmcnKXtcbiAgICAgIHBsdWdOYW1lcy5jb25jYXQocGx1Z2luTmFtZSk7XG4gICAgfWVsc2V7XG4gICAgICBjb25zb2xlLmVycm9yKCdQbHVnaW4gbmFtZXMgbXVzdCBiZSBzdHJpbmdzJyk7XG4gICAgfVxuICB9XG4gIGlmKHlldGlCb3hlcy5sZW5ndGgpe1xuICAgIGxldCBsaXN0ZW5lcnMgPSBwbHVnTmFtZXMubWFwKChuYW1lKSA9PiB7XG4gICAgICByZXR1cm4gYGNsb3NlbWUuemYuJHtuYW1lfWA7XG4gICAgfSkuam9pbignICcpO1xuXG4gICAgJCh3aW5kb3cpLm9mZihsaXN0ZW5lcnMpLm9uKGxpc3RlbmVycywgZnVuY3Rpb24oZSwgcGx1Z2luSWQpe1xuICAgICAgbGV0IHBsdWdpbiA9IGUubmFtZXNwYWNlLnNwbGl0KCcuJylbMF07XG4gICAgICBsZXQgcGx1Z2lucyA9ICQoYFtkYXRhLSR7cGx1Z2lufV1gKS5ub3QoYFtkYXRhLXlldGktYm94PVwiJHtwbHVnaW5JZH1cIl1gKTtcblxuICAgICAgcGx1Z2lucy5lYWNoKGZ1bmN0aW9uKCl7XG4gICAgICAgIGxldCBfdGhpcyA9ICQodGhpcyk7XG5cbiAgICAgICAgX3RoaXMudHJpZ2dlckhhbmRsZXIoJ2Nsb3NlLnpmLnRyaWdnZXInLCBbX3RoaXNdKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG59XG5cbmZ1bmN0aW9uIHJlc2l6ZUxpc3RlbmVyKGRlYm91bmNlKXtcbiAgbGV0IHRpbWVyLFxuICAgICAgJG5vZGVzID0gJCgnW2RhdGEtcmVzaXplXScpO1xuICBpZigkbm9kZXMubGVuZ3RoKXtcbiAgICAkKHdpbmRvdykub2ZmKCdyZXNpemUuemYudHJpZ2dlcicpXG4gICAgLm9uKCdyZXNpemUuemYudHJpZ2dlcicsIGZ1bmN0aW9uKGUpIHtcbiAgICAgIGlmICh0aW1lcikgeyBjbGVhclRpbWVvdXQodGltZXIpOyB9XG5cbiAgICAgIHRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbigpe1xuXG4gICAgICAgIGlmKCFNdXRhdGlvbk9ic2VydmVyKXsvL2ZhbGxiYWNrIGZvciBJRSA5XG4gICAgICAgICAgJG5vZGVzLmVhY2goZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICQodGhpcykudHJpZ2dlckhhbmRsZXIoJ3Jlc2l6ZW1lLnpmLnRyaWdnZXInKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICAvL3RyaWdnZXIgYWxsIGxpc3RlbmluZyBlbGVtZW50cyBhbmQgc2lnbmFsIGEgcmVzaXplIGV2ZW50XG4gICAgICAgICRub2Rlcy5hdHRyKCdkYXRhLWV2ZW50cycsIFwicmVzaXplXCIpO1xuICAgICAgfSwgZGVib3VuY2UgfHwgMTApOy8vZGVmYXVsdCB0aW1lIHRvIGVtaXQgcmVzaXplIGV2ZW50XG4gICAgfSk7XG4gIH1cbn1cblxuZnVuY3Rpb24gc2Nyb2xsTGlzdGVuZXIoZGVib3VuY2Upe1xuICBsZXQgdGltZXIsXG4gICAgICAkbm9kZXMgPSAkKCdbZGF0YS1zY3JvbGxdJyk7XG4gIGlmKCRub2Rlcy5sZW5ndGgpe1xuICAgICQod2luZG93KS5vZmYoJ3Njcm9sbC56Zi50cmlnZ2VyJylcbiAgICAub24oJ3Njcm9sbC56Zi50cmlnZ2VyJywgZnVuY3Rpb24oZSl7XG4gICAgICBpZih0aW1lcil7IGNsZWFyVGltZW91dCh0aW1lcik7IH1cblxuICAgICAgdGltZXIgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG5cbiAgICAgICAgaWYoIU11dGF0aW9uT2JzZXJ2ZXIpey8vZmFsbGJhY2sgZm9yIElFIDlcbiAgICAgICAgICAkbm9kZXMuZWFjaChmdW5jdGlvbigpe1xuICAgICAgICAgICAgJCh0aGlzKS50cmlnZ2VySGFuZGxlcignc2Nyb2xsbWUuemYudHJpZ2dlcicpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIC8vdHJpZ2dlciBhbGwgbGlzdGVuaW5nIGVsZW1lbnRzIGFuZCBzaWduYWwgYSBzY3JvbGwgZXZlbnRcbiAgICAgICAgJG5vZGVzLmF0dHIoJ2RhdGEtZXZlbnRzJywgXCJzY3JvbGxcIik7XG4gICAgICB9LCBkZWJvdW5jZSB8fCAxMCk7Ly9kZWZhdWx0IHRpbWUgdG8gZW1pdCBzY3JvbGwgZXZlbnRcbiAgICB9KTtcbiAgfVxufVxuXG5mdW5jdGlvbiBldmVudHNMaXN0ZW5lcigpIHtcbiAgaWYoIU11dGF0aW9uT2JzZXJ2ZXIpeyByZXR1cm4gZmFsc2U7IH1cbiAgbGV0IG5vZGVzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtcmVzaXplXSwgW2RhdGEtc2Nyb2xsXSwgW2RhdGEtbXV0YXRlXScpO1xuXG4gIC8vZWxlbWVudCBjYWxsYmFja1xuICB2YXIgbGlzdGVuaW5nRWxlbWVudHNNdXRhdGlvbiA9IGZ1bmN0aW9uKG11dGF0aW9uUmVjb3Jkc0xpc3QpIHtcbiAgICB2YXIgJHRhcmdldCA9ICQobXV0YXRpb25SZWNvcmRzTGlzdFswXS50YXJnZXQpO1xuICAgIC8vdHJpZ2dlciB0aGUgZXZlbnQgaGFuZGxlciBmb3IgdGhlIGVsZW1lbnQgZGVwZW5kaW5nIG9uIHR5cGVcbiAgICBzd2l0Y2ggKCR0YXJnZXQuYXR0cihcImRhdGEtZXZlbnRzXCIpKSB7XG5cbiAgICAgIGNhc2UgXCJyZXNpemVcIiA6XG4gICAgICAkdGFyZ2V0LnRyaWdnZXJIYW5kbGVyKCdyZXNpemVtZS56Zi50cmlnZ2VyJywgWyR0YXJnZXRdKTtcbiAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIFwic2Nyb2xsXCIgOlxuICAgICAgJHRhcmdldC50cmlnZ2VySGFuZGxlcignc2Nyb2xsbWUuemYudHJpZ2dlcicsIFskdGFyZ2V0LCB3aW5kb3cucGFnZVlPZmZzZXRdKTtcbiAgICAgIGJyZWFrO1xuXG4gICAgICAvLyBjYXNlIFwibXV0YXRlXCIgOlxuICAgICAgLy8gY29uc29sZS5sb2coJ211dGF0ZScsICR0YXJnZXQpO1xuICAgICAgLy8gJHRhcmdldC50cmlnZ2VySGFuZGxlcignbXV0YXRlLnpmLnRyaWdnZXInKTtcbiAgICAgIC8vXG4gICAgICAvLyAvL21ha2Ugc3VyZSB3ZSBkb24ndCBnZXQgc3R1Y2sgaW4gYW4gaW5maW5pdGUgbG9vcCBmcm9tIHNsb3BweSBjb2RlaW5nXG4gICAgICAvLyBpZiAoJHRhcmdldC5pbmRleCgnW2RhdGEtbXV0YXRlXScpID09ICQoXCJbZGF0YS1tdXRhdGVdXCIpLmxlbmd0aC0xKSB7XG4gICAgICAvLyAgIGRvbU11dGF0aW9uT2JzZXJ2ZXIoKTtcbiAgICAgIC8vIH1cbiAgICAgIC8vIGJyZWFrO1xuXG4gICAgICBkZWZhdWx0IDpcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIC8vbm90aGluZ1xuICAgIH1cbiAgfVxuXG4gIGlmKG5vZGVzLmxlbmd0aCl7XG4gICAgLy9mb3IgZWFjaCBlbGVtZW50IHRoYXQgbmVlZHMgdG8gbGlzdGVuIGZvciByZXNpemluZywgc2Nyb2xsaW5nLCAob3IgY29taW5nIHNvb24gbXV0YXRpb24pIGFkZCBhIHNpbmdsZSBvYnNlcnZlclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDw9IG5vZGVzLmxlbmd0aC0xOyBpKyspIHtcbiAgICAgIGxldCBlbGVtZW50T2JzZXJ2ZXIgPSBuZXcgTXV0YXRpb25PYnNlcnZlcihsaXN0ZW5pbmdFbGVtZW50c011dGF0aW9uKTtcbiAgICAgIGVsZW1lbnRPYnNlcnZlci5vYnNlcnZlKG5vZGVzW2ldLCB7IGF0dHJpYnV0ZXM6IHRydWUsIGNoaWxkTGlzdDogZmFsc2UsIGNoYXJhY3RlckRhdGE6IGZhbHNlLCBzdWJ0cmVlOmZhbHNlLCBhdHRyaWJ1dGVGaWx0ZXI6W1wiZGF0YS1ldmVudHNcIl19KTtcbiAgICB9XG4gIH1cbn1cblxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cbi8vIFtQSF1cbi8vIEZvdW5kYXRpb24uQ2hlY2tXYXRjaGVycyA9IGNoZWNrV2F0Y2hlcnM7XG5Gb3VuZGF0aW9uLklIZWFyWW91ID0gY2hlY2tMaXN0ZW5lcnM7XG4vLyBGb3VuZGF0aW9uLklTZWVZb3UgPSBzY3JvbGxMaXN0ZW5lcjtcbi8vIEZvdW5kYXRpb24uSUZlZWxZb3UgPSBjbG9zZW1lTGlzdGVuZXI7XG5cbn0oalF1ZXJ5KTtcblxuLy8gZnVuY3Rpb24gZG9tTXV0YXRpb25PYnNlcnZlcihkZWJvdW5jZSkge1xuLy8gICAvLyAhISEgVGhpcyBpcyBjb21pbmcgc29vbiBhbmQgbmVlZHMgbW9yZSB3b3JrOyBub3QgYWN0aXZlICAhISEgLy9cbi8vICAgdmFyIHRpbWVyLFxuLy8gICBub2RlcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ1tkYXRhLW11dGF0ZV0nKTtcbi8vICAgLy9cbi8vICAgaWYgKG5vZGVzLmxlbmd0aCkge1xuLy8gICAgIC8vIHZhciBNdXRhdGlvbk9ic2VydmVyID0gKGZ1bmN0aW9uICgpIHtcbi8vICAgICAvLyAgIHZhciBwcmVmaXhlcyA9IFsnV2ViS2l0JywgJ01veicsICdPJywgJ01zJywgJyddO1xuLy8gICAgIC8vICAgZm9yICh2YXIgaT0wOyBpIDwgcHJlZml4ZXMubGVuZ3RoOyBpKyspIHtcbi8vICAgICAvLyAgICAgaWYgKHByZWZpeGVzW2ldICsgJ011dGF0aW9uT2JzZXJ2ZXInIGluIHdpbmRvdykge1xuLy8gICAgIC8vICAgICAgIHJldHVybiB3aW5kb3dbcHJlZml4ZXNbaV0gKyAnTXV0YXRpb25PYnNlcnZlciddO1xuLy8gICAgIC8vICAgICB9XG4vLyAgICAgLy8gICB9XG4vLyAgICAgLy8gICByZXR1cm4gZmFsc2U7XG4vLyAgICAgLy8gfSgpKTtcbi8vXG4vL1xuLy8gICAgIC8vZm9yIHRoZSBib2R5LCB3ZSBuZWVkIHRvIGxpc3RlbiBmb3IgYWxsIGNoYW5nZXMgZWZmZWN0aW5nIHRoZSBzdHlsZSBhbmQgY2xhc3MgYXR0cmlidXRlc1xuLy8gICAgIHZhciBib2R5T2JzZXJ2ZXIgPSBuZXcgTXV0YXRpb25PYnNlcnZlcihib2R5TXV0YXRpb24pO1xuLy8gICAgIGJvZHlPYnNlcnZlci5vYnNlcnZlKGRvY3VtZW50LmJvZHksIHsgYXR0cmlidXRlczogdHJ1ZSwgY2hpbGRMaXN0OiB0cnVlLCBjaGFyYWN0ZXJEYXRhOiBmYWxzZSwgc3VidHJlZTp0cnVlLCBhdHRyaWJ1dGVGaWx0ZXI6W1wic3R5bGVcIiwgXCJjbGFzc1wiXX0pO1xuLy9cbi8vXG4vLyAgICAgLy9ib2R5IGNhbGxiYWNrXG4vLyAgICAgZnVuY3Rpb24gYm9keU11dGF0aW9uKG11dGF0ZSkge1xuLy8gICAgICAgLy90cmlnZ2VyIGFsbCBsaXN0ZW5pbmcgZWxlbWVudHMgYW5kIHNpZ25hbCBhIG11dGF0aW9uIGV2ZW50XG4vLyAgICAgICBpZiAodGltZXIpIHsgY2xlYXJUaW1lb3V0KHRpbWVyKTsgfVxuLy9cbi8vICAgICAgIHRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcbi8vICAgICAgICAgYm9keU9ic2VydmVyLmRpc2Nvbm5lY3QoKTtcbi8vICAgICAgICAgJCgnW2RhdGEtbXV0YXRlXScpLmF0dHIoJ2RhdGEtZXZlbnRzJyxcIm11dGF0ZVwiKTtcbi8vICAgICAgIH0sIGRlYm91bmNlIHx8IDE1MCk7XG4vLyAgICAgfVxuLy8gICB9XG4vLyB9XG4iLCIndXNlIHN0cmljdCc7XG5cbiFmdW5jdGlvbigkKSB7XG5cbi8qKlxuICogT2ZmQ2FudmFzIG1vZHVsZS5cbiAqIEBtb2R1bGUgZm91bmRhdGlvbi5vZmZjYW52YXNcbiAqIEByZXF1aXJlcyBmb3VuZGF0aW9uLnV0aWwubWVkaWFRdWVyeVxuICogQHJlcXVpcmVzIGZvdW5kYXRpb24udXRpbC50cmlnZ2Vyc1xuICogQHJlcXVpcmVzIGZvdW5kYXRpb24udXRpbC5tb3Rpb25cbiAqL1xuXG5jbGFzcyBPZmZDYW52YXMge1xuICAvKipcbiAgICogQ3JlYXRlcyBhIG5ldyBpbnN0YW5jZSBvZiBhbiBvZmYtY2FudmFzIHdyYXBwZXIuXG4gICAqIEBjbGFzc1xuICAgKiBAZmlyZXMgT2ZmQ2FudmFzI2luaXRcbiAgICogQHBhcmFtIHtPYmplY3R9IGVsZW1lbnQgLSBqUXVlcnkgb2JqZWN0IHRvIGluaXRpYWxpemUuXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zIC0gT3ZlcnJpZGVzIHRvIHRoZSBkZWZhdWx0IHBsdWdpbiBzZXR0aW5ncy5cbiAgICovXG4gIGNvbnN0cnVjdG9yKGVsZW1lbnQsIG9wdGlvbnMpIHtcbiAgICB0aGlzLiRlbGVtZW50ID0gZWxlbWVudDtcbiAgICB0aGlzLm9wdGlvbnMgPSAkLmV4dGVuZCh7fSwgT2ZmQ2FudmFzLmRlZmF1bHRzLCB0aGlzLiRlbGVtZW50LmRhdGEoKSwgb3B0aW9ucyk7XG4gICAgdGhpcy4kbGFzdFRyaWdnZXIgPSAkKCk7XG4gICAgdGhpcy4kdHJpZ2dlcnMgPSAkKCk7XG5cbiAgICB0aGlzLl9pbml0KCk7XG4gICAgdGhpcy5fZXZlbnRzKCk7XG5cbiAgICBGb3VuZGF0aW9uLnJlZ2lzdGVyUGx1Z2luKHRoaXMsICdPZmZDYW52YXMnKVxuICAgIEZvdW5kYXRpb24uS2V5Ym9hcmQucmVnaXN0ZXIoJ09mZkNhbnZhcycsIHtcbiAgICAgICdFU0NBUEUnOiAnY2xvc2UnXG4gICAgfSk7XG5cbiAgfVxuXG4gIC8qKlxuICAgKiBJbml0aWFsaXplcyB0aGUgb2ZmLWNhbnZhcyB3cmFwcGVyIGJ5IGFkZGluZyB0aGUgZXhpdCBvdmVybGF5IChpZiBuZWVkZWQpLlxuICAgKiBAZnVuY3Rpb25cbiAgICogQHByaXZhdGVcbiAgICovXG4gIF9pbml0KCkge1xuICAgIHZhciBpZCA9IHRoaXMuJGVsZW1lbnQuYXR0cignaWQnKTtcblxuICAgIHRoaXMuJGVsZW1lbnQuYXR0cignYXJpYS1oaWRkZW4nLCAndHJ1ZScpO1xuXG4gICAgLy8gRmluZCB0cmlnZ2VycyB0aGF0IGFmZmVjdCB0aGlzIGVsZW1lbnQgYW5kIGFkZCBhcmlhLWV4cGFuZGVkIHRvIHRoZW1cbiAgICB0aGlzLiR0cmlnZ2VycyA9ICQoZG9jdW1lbnQpXG4gICAgICAuZmluZCgnW2RhdGEtb3Blbj1cIicraWQrJ1wiXSwgW2RhdGEtY2xvc2U9XCInK2lkKydcIl0sIFtkYXRhLXRvZ2dsZT1cIicraWQrJ1wiXScpXG4gICAgICAuYXR0cignYXJpYS1leHBhbmRlZCcsICdmYWxzZScpXG4gICAgICAuYXR0cignYXJpYS1jb250cm9scycsIGlkKTtcblxuICAgIC8vIEFkZCBhIGNsb3NlIHRyaWdnZXIgb3ZlciB0aGUgYm9keSBpZiBuZWNlc3NhcnlcbiAgICBpZiAodGhpcy5vcHRpb25zLmNsb3NlT25DbGljaykge1xuICAgICAgaWYgKCQoJy5qcy1vZmYtY2FudmFzLWV4aXQnKS5sZW5ndGgpIHtcbiAgICAgICAgdGhpcy4kZXhpdGVyID0gJCgnLmpzLW9mZi1jYW52YXMtZXhpdCcpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIGV4aXRlciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgICBleGl0ZXIuc2V0QXR0cmlidXRlKCdjbGFzcycsICdqcy1vZmYtY2FudmFzLWV4aXQnKTtcbiAgICAgICAgJCgnW2RhdGEtb2ZmLWNhbnZhcy1jb250ZW50XScpLmFwcGVuZChleGl0ZXIpO1xuXG4gICAgICAgIHRoaXMuJGV4aXRlciA9ICQoZXhpdGVyKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB0aGlzLm9wdGlvbnMuaXNSZXZlYWxlZCA9IHRoaXMub3B0aW9ucy5pc1JldmVhbGVkIHx8IG5ldyBSZWdFeHAodGhpcy5vcHRpb25zLnJldmVhbENsYXNzLCAnZycpLnRlc3QodGhpcy4kZWxlbWVudFswXS5jbGFzc05hbWUpO1xuXG4gICAgaWYgKHRoaXMub3B0aW9ucy5pc1JldmVhbGVkKSB7XG4gICAgICB0aGlzLm9wdGlvbnMucmV2ZWFsT24gPSB0aGlzLm9wdGlvbnMucmV2ZWFsT24gfHwgdGhpcy4kZWxlbWVudFswXS5jbGFzc05hbWUubWF0Y2goLyhyZXZlYWwtZm9yLW1lZGl1bXxyZXZlYWwtZm9yLWxhcmdlKS9nKVswXS5zcGxpdCgnLScpWzJdO1xuICAgICAgdGhpcy5fc2V0TVFDaGVja2VyKCk7XG4gICAgfVxuICAgIGlmICghdGhpcy5vcHRpb25zLnRyYW5zaXRpb25UaW1lKSB7XG4gICAgICB0aGlzLm9wdGlvbnMudHJhbnNpdGlvblRpbWUgPSBwYXJzZUZsb2F0KHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKCQoJ1tkYXRhLW9mZi1jYW52YXMtd3JhcHBlcl0nKVswXSkudHJhbnNpdGlvbkR1cmF0aW9uKSAqIDEwMDA7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEFkZHMgZXZlbnQgaGFuZGxlcnMgdG8gdGhlIG9mZi1jYW52YXMgd3JhcHBlciBhbmQgdGhlIGV4aXQgb3ZlcmxheS5cbiAgICogQGZ1bmN0aW9uXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBfZXZlbnRzKCkge1xuICAgIHRoaXMuJGVsZW1lbnQub2ZmKCcuemYudHJpZ2dlciAuemYub2ZmY2FudmFzJykub24oe1xuICAgICAgJ29wZW4uemYudHJpZ2dlcic6IHRoaXMub3Blbi5iaW5kKHRoaXMpLFxuICAgICAgJ2Nsb3NlLnpmLnRyaWdnZXInOiB0aGlzLmNsb3NlLmJpbmQodGhpcyksXG4gICAgICAndG9nZ2xlLnpmLnRyaWdnZXInOiB0aGlzLnRvZ2dsZS5iaW5kKHRoaXMpLFxuICAgICAgJ2tleWRvd24uemYub2ZmY2FudmFzJzogdGhpcy5faGFuZGxlS2V5Ym9hcmQuYmluZCh0aGlzKVxuICAgIH0pO1xuXG4gICAgaWYgKHRoaXMub3B0aW9ucy5jbG9zZU9uQ2xpY2sgJiYgdGhpcy4kZXhpdGVyLmxlbmd0aCkge1xuICAgICAgdGhpcy4kZXhpdGVyLm9uKHsnY2xpY2suemYub2ZmY2FudmFzJzogdGhpcy5jbG9zZS5iaW5kKHRoaXMpfSk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEFwcGxpZXMgZXZlbnQgbGlzdGVuZXIgZm9yIGVsZW1lbnRzIHRoYXQgd2lsbCByZXZlYWwgYXQgY2VydGFpbiBicmVha3BvaW50cy5cbiAgICogQHByaXZhdGVcbiAgICovXG4gIF9zZXRNUUNoZWNrZXIoKSB7XG4gICAgdmFyIF90aGlzID0gdGhpcztcblxuICAgICQod2luZG93KS5vbignY2hhbmdlZC56Zi5tZWRpYXF1ZXJ5JywgZnVuY3Rpb24oKSB7XG4gICAgICBpZiAoRm91bmRhdGlvbi5NZWRpYVF1ZXJ5LmF0TGVhc3QoX3RoaXMub3B0aW9ucy5yZXZlYWxPbikpIHtcbiAgICAgICAgX3RoaXMucmV2ZWFsKHRydWUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgX3RoaXMucmV2ZWFsKGZhbHNlKTtcbiAgICAgIH1cbiAgICB9KS5vbmUoJ2xvYWQuemYub2ZmY2FudmFzJywgZnVuY3Rpb24oKSB7XG4gICAgICBpZiAoRm91bmRhdGlvbi5NZWRpYVF1ZXJ5LmF0TGVhc3QoX3RoaXMub3B0aW9ucy5yZXZlYWxPbikpIHtcbiAgICAgICAgX3RoaXMucmV2ZWFsKHRydWUpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIEhhbmRsZXMgdGhlIHJldmVhbGluZy9oaWRpbmcgdGhlIG9mZi1jYW52YXMgYXQgYnJlYWtwb2ludHMsIG5vdCB0aGUgc2FtZSBhcyBvcGVuLlxuICAgKiBAcGFyYW0ge0Jvb2xlYW59IGlzUmV2ZWFsZWQgLSB0cnVlIGlmIGVsZW1lbnQgc2hvdWxkIGJlIHJldmVhbGVkLlxuICAgKiBAZnVuY3Rpb25cbiAgICovXG4gIHJldmVhbChpc1JldmVhbGVkKSB7XG4gICAgdmFyICRjbG9zZXIgPSB0aGlzLiRlbGVtZW50LmZpbmQoJ1tkYXRhLWNsb3NlXScpO1xuICAgIGlmIChpc1JldmVhbGVkKSB7XG4gICAgICB0aGlzLmNsb3NlKCk7XG4gICAgICB0aGlzLmlzUmV2ZWFsZWQgPSB0cnVlO1xuICAgICAgLy8gaWYgKCF0aGlzLm9wdGlvbnMuZm9yY2VUb3ApIHtcbiAgICAgIC8vICAgdmFyIHNjcm9sbFBvcyA9IHBhcnNlSW50KHdpbmRvdy5wYWdlWU9mZnNldCk7XG4gICAgICAvLyAgIHRoaXMuJGVsZW1lbnRbMF0uc3R5bGUudHJhbnNmb3JtID0gJ3RyYW5zbGF0ZSgwLCcgKyBzY3JvbGxQb3MgKyAncHgpJztcbiAgICAgIC8vIH1cbiAgICAgIC8vIGlmICh0aGlzLm9wdGlvbnMuaXNTdGlja3kpIHsgdGhpcy5fc3RpY2soKTsgfVxuICAgICAgdGhpcy4kZWxlbWVudC5vZmYoJ29wZW4uemYudHJpZ2dlciB0b2dnbGUuemYudHJpZ2dlcicpO1xuICAgICAgaWYgKCRjbG9zZXIubGVuZ3RoKSB7ICRjbG9zZXIuaGlkZSgpOyB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuaXNSZXZlYWxlZCA9IGZhbHNlO1xuICAgICAgLy8gaWYgKHRoaXMub3B0aW9ucy5pc1N0aWNreSB8fCAhdGhpcy5vcHRpb25zLmZvcmNlVG9wKSB7XG4gICAgICAvLyAgIHRoaXMuJGVsZW1lbnRbMF0uc3R5bGUudHJhbnNmb3JtID0gJyc7XG4gICAgICAvLyAgICQod2luZG93KS5vZmYoJ3Njcm9sbC56Zi5vZmZjYW52YXMnKTtcbiAgICAgIC8vIH1cbiAgICAgIHRoaXMuJGVsZW1lbnQub24oe1xuICAgICAgICAnb3Blbi56Zi50cmlnZ2VyJzogdGhpcy5vcGVuLmJpbmQodGhpcyksXG4gICAgICAgICd0b2dnbGUuemYudHJpZ2dlcic6IHRoaXMudG9nZ2xlLmJpbmQodGhpcylcbiAgICAgIH0pO1xuICAgICAgaWYgKCRjbG9zZXIubGVuZ3RoKSB7XG4gICAgICAgICRjbG9zZXIuc2hvdygpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBPcGVucyB0aGUgb2ZmLWNhbnZhcyBtZW51LlxuICAgKiBAZnVuY3Rpb25cbiAgICogQHBhcmFtIHtPYmplY3R9IGV2ZW50IC0gRXZlbnQgb2JqZWN0IHBhc3NlZCBmcm9tIGxpc3RlbmVyLlxuICAgKiBAcGFyYW0ge2pRdWVyeX0gdHJpZ2dlciAtIGVsZW1lbnQgdGhhdCB0cmlnZ2VyZWQgdGhlIG9mZi1jYW52YXMgdG8gb3Blbi5cbiAgICogQGZpcmVzIE9mZkNhbnZhcyNvcGVuZWRcbiAgICovXG4gIG9wZW4oZXZlbnQsIHRyaWdnZXIpIHtcbiAgICBpZiAodGhpcy4kZWxlbWVudC5oYXNDbGFzcygnaXMtb3BlbicpIHx8IHRoaXMuaXNSZXZlYWxlZCkgeyByZXR1cm47IH1cbiAgICB2YXIgX3RoaXMgPSB0aGlzLFxuICAgICAgICAkYm9keSA9ICQoZG9jdW1lbnQuYm9keSk7XG5cbiAgICBpZiAodGhpcy5vcHRpb25zLmZvcmNlVG9wKSB7XG4gICAgICAkKCdib2R5Jykuc2Nyb2xsVG9wKDApO1xuICAgIH1cbiAgICAvLyB3aW5kb3cucGFnZVlPZmZzZXQgPSAwO1xuXG4gICAgLy8gaWYgKCF0aGlzLm9wdGlvbnMuZm9yY2VUb3ApIHtcbiAgICAvLyAgIHZhciBzY3JvbGxQb3MgPSBwYXJzZUludCh3aW5kb3cucGFnZVlPZmZzZXQpO1xuICAgIC8vICAgdGhpcy4kZWxlbWVudFswXS5zdHlsZS50cmFuc2Zvcm0gPSAndHJhbnNsYXRlKDAsJyArIHNjcm9sbFBvcyArICdweCknO1xuICAgIC8vICAgaWYgKHRoaXMuJGV4aXRlci5sZW5ndGgpIHtcbiAgICAvLyAgICAgdGhpcy4kZXhpdGVyWzBdLnN0eWxlLnRyYW5zZm9ybSA9ICd0cmFuc2xhdGUoMCwnICsgc2Nyb2xsUG9zICsgJ3B4KSc7XG4gICAgLy8gICB9XG4gICAgLy8gfVxuICAgIC8qKlxuICAgICAqIEZpcmVzIHdoZW4gdGhlIG9mZi1jYW52YXMgbWVudSBvcGVucy5cbiAgICAgKiBAZXZlbnQgT2ZmQ2FudmFzI29wZW5lZFxuICAgICAqL1xuXG4gICAgdmFyICR3cmFwcGVyID0gJCgnW2RhdGEtb2ZmLWNhbnZhcy13cmFwcGVyXScpO1xuICAgICR3cmFwcGVyLmFkZENsYXNzKCdpcy1vZmYtY2FudmFzLW9wZW4gaXMtb3Blbi0nKyBfdGhpcy5vcHRpb25zLnBvc2l0aW9uKTtcblxuICAgIF90aGlzLiRlbGVtZW50LmFkZENsYXNzKCdpcy1vcGVuJylcblxuICAgICAgLy8gaWYgKF90aGlzLm9wdGlvbnMuaXNTdGlja3kpIHtcbiAgICAgIC8vICAgX3RoaXMuX3N0aWNrKCk7XG4gICAgICAvLyB9XG5cbiAgICB0aGlzLiR0cmlnZ2Vycy5hdHRyKCdhcmlhLWV4cGFuZGVkJywgJ3RydWUnKTtcbiAgICB0aGlzLiRlbGVtZW50LmF0dHIoJ2FyaWEtaGlkZGVuJywgJ2ZhbHNlJylcbiAgICAgICAgLnRyaWdnZXIoJ29wZW5lZC56Zi5vZmZjYW52YXMnKTtcblxuICAgIGlmICh0aGlzLm9wdGlvbnMuY2xvc2VPbkNsaWNrKSB7XG4gICAgICB0aGlzLiRleGl0ZXIuYWRkQ2xhc3MoJ2lzLXZpc2libGUnKTtcbiAgICB9XG5cbiAgICBpZiAodHJpZ2dlcikge1xuICAgICAgdGhpcy4kbGFzdFRyaWdnZXIgPSB0cmlnZ2VyO1xuICAgIH1cblxuICAgIGlmICh0aGlzLm9wdGlvbnMuYXV0b0ZvY3VzKSB7XG4gICAgICAkd3JhcHBlci5vbmUoRm91bmRhdGlvbi50cmFuc2l0aW9uZW5kKCR3cmFwcGVyKSwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGlmKF90aGlzLiRlbGVtZW50Lmhhc0NsYXNzKCdpcy1vcGVuJykpIHsgLy8gaGFuZGxlIGRvdWJsZSBjbGlja3NcbiAgICAgICAgICBfdGhpcy4kZWxlbWVudC5hdHRyKCd0YWJpbmRleCcsICctMScpO1xuICAgICAgICAgIF90aGlzLiRlbGVtZW50LmZvY3VzKCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlmICh0aGlzLm9wdGlvbnMudHJhcEZvY3VzKSB7XG4gICAgICAkd3JhcHBlci5vbmUoRm91bmRhdGlvbi50cmFuc2l0aW9uZW5kKCR3cmFwcGVyKSwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGlmKF90aGlzLiRlbGVtZW50Lmhhc0NsYXNzKCdpcy1vcGVuJykpIHsgLy8gaGFuZGxlIGRvdWJsZSBjbGlja3NcbiAgICAgICAgICBfdGhpcy4kZWxlbWVudC5hdHRyKCd0YWJpbmRleCcsICctMScpO1xuICAgICAgICAgIF90aGlzLnRyYXBGb2N1cygpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogVHJhcHMgZm9jdXMgd2l0aGluIHRoZSBvZmZjYW52YXMgb24gb3Blbi5cbiAgICogQHByaXZhdGVcbiAgICovXG4gIF90cmFwRm9jdXMoKSB7XG4gICAgdmFyIGZvY3VzYWJsZSA9IEZvdW5kYXRpb24uS2V5Ym9hcmQuZmluZEZvY3VzYWJsZSh0aGlzLiRlbGVtZW50KSxcbiAgICAgICAgZmlyc3QgPSBmb2N1c2FibGUuZXEoMCksXG4gICAgICAgIGxhc3QgPSBmb2N1c2FibGUuZXEoLTEpO1xuXG4gICAgZm9jdXNhYmxlLm9mZignLnpmLm9mZmNhbnZhcycpLm9uKCdrZXlkb3duLnpmLm9mZmNhbnZhcycsIGZ1bmN0aW9uKGUpIHtcbiAgICAgIHZhciBrZXkgPSBGb3VuZGF0aW9uLktleWJvYXJkLnBhcnNlS2V5KGUpO1xuICAgICAgaWYgKGtleSA9PT0gJ1RBQicgJiYgZS50YXJnZXQgPT09IGxhc3RbMF0pIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBmaXJzdC5mb2N1cygpO1xuICAgICAgfVxuICAgICAgaWYgKGtleSA9PT0gJ1NISUZUX1RBQicgJiYgZS50YXJnZXQgPT09IGZpcnN0WzBdKSB7XG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgbGFzdC5mb2N1cygpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIEFsbG93cyB0aGUgb2ZmY2FudmFzIHRvIGFwcGVhciBzdGlja3kgdXRpbGl6aW5nIHRyYW5zbGF0ZSBwcm9wZXJ0aWVzLlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgLy8gT2ZmQ2FudmFzLnByb3RvdHlwZS5fc3RpY2sgPSBmdW5jdGlvbigpIHtcbiAgLy8gICB2YXIgZWxTdHlsZSA9IHRoaXMuJGVsZW1lbnRbMF0uc3R5bGU7XG4gIC8vXG4gIC8vICAgaWYgKHRoaXMub3B0aW9ucy5jbG9zZU9uQ2xpY2spIHtcbiAgLy8gICAgIHZhciBleGl0U3R5bGUgPSB0aGlzLiRleGl0ZXJbMF0uc3R5bGU7XG4gIC8vICAgfVxuICAvL1xuICAvLyAgICQod2luZG93KS5vbignc2Nyb2xsLnpmLm9mZmNhbnZhcycsIGZ1bmN0aW9uKGUpIHtcbiAgLy8gICAgIGNvbnNvbGUubG9nKGUpO1xuICAvLyAgICAgdmFyIHBhZ2VZID0gd2luZG93LnBhZ2VZT2Zmc2V0O1xuICAvLyAgICAgZWxTdHlsZS50cmFuc2Zvcm0gPSAndHJhbnNsYXRlKDAsJyArIHBhZ2VZICsgJ3B4KSc7XG4gIC8vICAgICBpZiAoZXhpdFN0eWxlICE9PSB1bmRlZmluZWQpIHsgZXhpdFN0eWxlLnRyYW5zZm9ybSA9ICd0cmFuc2xhdGUoMCwnICsgcGFnZVkgKyAncHgpJzsgfVxuICAvLyAgIH0pO1xuICAvLyAgIC8vIHRoaXMuJGVsZW1lbnQudHJpZ2dlcignc3R1Y2suemYub2ZmY2FudmFzJyk7XG4gIC8vIH07XG4gIC8qKlxuICAgKiBDbG9zZXMgdGhlIG9mZi1jYW52YXMgbWVudS5cbiAgICogQGZ1bmN0aW9uXG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IGNiIC0gb3B0aW9uYWwgY2IgdG8gZmlyZSBhZnRlciBjbG9zdXJlLlxuICAgKiBAZmlyZXMgT2ZmQ2FudmFzI2Nsb3NlZFxuICAgKi9cbiAgY2xvc2UoY2IpIHtcbiAgICBpZiAoIXRoaXMuJGVsZW1lbnQuaGFzQ2xhc3MoJ2lzLW9wZW4nKSB8fCB0aGlzLmlzUmV2ZWFsZWQpIHsgcmV0dXJuOyB9XG5cbiAgICB2YXIgX3RoaXMgPSB0aGlzO1xuXG4gICAgLy8gIEZvdW5kYXRpb24uTW92ZSh0aGlzLm9wdGlvbnMudHJhbnNpdGlvblRpbWUsIHRoaXMuJGVsZW1lbnQsIGZ1bmN0aW9uKCkge1xuICAgICQoJ1tkYXRhLW9mZi1jYW52YXMtd3JhcHBlcl0nKS5yZW1vdmVDbGFzcyhgaXMtb2ZmLWNhbnZhcy1vcGVuIGlzLW9wZW4tJHtfdGhpcy5vcHRpb25zLnBvc2l0aW9ufWApO1xuICAgIF90aGlzLiRlbGVtZW50LnJlbW92ZUNsYXNzKCdpcy1vcGVuJyk7XG4gICAgICAvLyBGb3VuZGF0aW9uLl9yZWZsb3coKTtcbiAgICAvLyB9KTtcbiAgICB0aGlzLiRlbGVtZW50LmF0dHIoJ2FyaWEtaGlkZGVuJywgJ3RydWUnKVxuICAgICAgLyoqXG4gICAgICAgKiBGaXJlcyB3aGVuIHRoZSBvZmYtY2FudmFzIG1lbnUgb3BlbnMuXG4gICAgICAgKiBAZXZlbnQgT2ZmQ2FudmFzI2Nsb3NlZFxuICAgICAgICovXG4gICAgICAgIC50cmlnZ2VyKCdjbG9zZWQuemYub2ZmY2FudmFzJyk7XG4gICAgLy8gaWYgKF90aGlzLm9wdGlvbnMuaXNTdGlja3kgfHwgIV90aGlzLm9wdGlvbnMuZm9yY2VUb3ApIHtcbiAgICAvLyAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgLy8gICAgIF90aGlzLiRlbGVtZW50WzBdLnN0eWxlLnRyYW5zZm9ybSA9ICcnO1xuICAgIC8vICAgICAkKHdpbmRvdykub2ZmKCdzY3JvbGwuemYub2ZmY2FudmFzJyk7XG4gICAgLy8gICB9LCB0aGlzLm9wdGlvbnMudHJhbnNpdGlvblRpbWUpO1xuICAgIC8vIH1cbiAgICBpZiAodGhpcy5vcHRpb25zLmNsb3NlT25DbGljaykge1xuICAgICAgdGhpcy4kZXhpdGVyLnJlbW92ZUNsYXNzKCdpcy12aXNpYmxlJyk7XG4gICAgfVxuXG4gICAgdGhpcy4kdHJpZ2dlcnMuYXR0cignYXJpYS1leHBhbmRlZCcsICdmYWxzZScpO1xuICAgIGlmICh0aGlzLm9wdGlvbnMudHJhcEZvY3VzKSB7XG4gICAgICAkKCdbZGF0YS1vZmYtY2FudmFzLWNvbnRlbnRdJykucmVtb3ZlQXR0cigndGFiaW5kZXgnKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogVG9nZ2xlcyB0aGUgb2ZmLWNhbnZhcyBtZW51IG9wZW4gb3IgY2xvc2VkLlxuICAgKiBAZnVuY3Rpb25cbiAgICogQHBhcmFtIHtPYmplY3R9IGV2ZW50IC0gRXZlbnQgb2JqZWN0IHBhc3NlZCBmcm9tIGxpc3RlbmVyLlxuICAgKiBAcGFyYW0ge2pRdWVyeX0gdHJpZ2dlciAtIGVsZW1lbnQgdGhhdCB0cmlnZ2VyZWQgdGhlIG9mZi1jYW52YXMgdG8gb3Blbi5cbiAgICovXG4gIHRvZ2dsZShldmVudCwgdHJpZ2dlcikge1xuICAgIGlmICh0aGlzLiRlbGVtZW50Lmhhc0NsYXNzKCdpcy1vcGVuJykpIHtcbiAgICAgIHRoaXMuY2xvc2UoZXZlbnQsIHRyaWdnZXIpO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIHRoaXMub3BlbihldmVudCwgdHJpZ2dlcik7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEhhbmRsZXMga2V5Ym9hcmQgaW5wdXQgd2hlbiBkZXRlY3RlZC4gV2hlbiB0aGUgZXNjYXBlIGtleSBpcyBwcmVzc2VkLCB0aGUgb2ZmLWNhbnZhcyBtZW51IGNsb3NlcywgYW5kIGZvY3VzIGlzIHJlc3RvcmVkIHRvIHRoZSBlbGVtZW50IHRoYXQgb3BlbmVkIHRoZSBtZW51LlxuICAgKiBAZnVuY3Rpb25cbiAgICogQHByaXZhdGVcbiAgICovXG4gIF9oYW5kbGVLZXlib2FyZChlKSB7XG4gICAgRm91bmRhdGlvbi5LZXlib2FyZC5oYW5kbGVLZXkoZSwgJ09mZkNhbnZhcycsIHtcbiAgICAgIGNsb3NlOiAoKSA9PiB7XG4gICAgICAgIHRoaXMuY2xvc2UoKTtcbiAgICAgICAgdGhpcy4kbGFzdFRyaWdnZXIuZm9jdXMoKTtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9LFxuICAgICAgaGFuZGxlZDogKCkgPT4ge1xuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogRGVzdHJveXMgdGhlIG9mZmNhbnZhcyBwbHVnaW4uXG4gICAqIEBmdW5jdGlvblxuICAgKi9cbiAgZGVzdHJveSgpIHtcbiAgICB0aGlzLmNsb3NlKCk7XG4gICAgdGhpcy4kZWxlbWVudC5vZmYoJy56Zi50cmlnZ2VyIC56Zi5vZmZjYW52YXMnKTtcbiAgICB0aGlzLiRleGl0ZXIub2ZmKCcuemYub2ZmY2FudmFzJyk7XG5cbiAgICBGb3VuZGF0aW9uLnVucmVnaXN0ZXJQbHVnaW4odGhpcyk7XG4gIH1cbn1cblxuT2ZmQ2FudmFzLmRlZmF1bHRzID0ge1xuICAvKipcbiAgICogQWxsb3cgdGhlIHVzZXIgdG8gY2xpY2sgb3V0c2lkZSBvZiB0aGUgbWVudSB0byBjbG9zZSBpdC5cbiAgICogQG9wdGlvblxuICAgKiBAZXhhbXBsZSB0cnVlXG4gICAqL1xuICBjbG9zZU9uQ2xpY2s6IHRydWUsXG5cbiAgLyoqXG4gICAqIEFtb3VudCBvZiB0aW1lIGluIG1zIHRoZSBvcGVuIGFuZCBjbG9zZSB0cmFuc2l0aW9uIHJlcXVpcmVzLiBJZiBub25lIHNlbGVjdGVkLCBwdWxscyBmcm9tIGJvZHkgc3R5bGUuXG4gICAqIEBvcHRpb25cbiAgICogQGV4YW1wbGUgNTAwXG4gICAqL1xuICB0cmFuc2l0aW9uVGltZTogMCxcblxuICAvKipcbiAgICogRGlyZWN0aW9uIHRoZSBvZmZjYW52YXMgb3BlbnMgZnJvbS4gRGV0ZXJtaW5lcyBjbGFzcyBhcHBsaWVkIHRvIGJvZHkuXG4gICAqIEBvcHRpb25cbiAgICogQGV4YW1wbGUgbGVmdFxuICAgKi9cbiAgcG9zaXRpb246ICdsZWZ0JyxcblxuICAvKipcbiAgICogRm9yY2UgdGhlIHBhZ2UgdG8gc2Nyb2xsIHRvIHRvcCBvbiBvcGVuLlxuICAgKiBAb3B0aW9uXG4gICAqIEBleGFtcGxlIHRydWVcbiAgICovXG4gIGZvcmNlVG9wOiB0cnVlLFxuXG4gIC8qKlxuICAgKiBBbGxvdyB0aGUgb2ZmY2FudmFzIHRvIHJlbWFpbiBvcGVuIGZvciBjZXJ0YWluIGJyZWFrcG9pbnRzLlxuICAgKiBAb3B0aW9uXG4gICAqIEBleGFtcGxlIGZhbHNlXG4gICAqL1xuICBpc1JldmVhbGVkOiBmYWxzZSxcblxuICAvKipcbiAgICogQnJlYWtwb2ludCBhdCB3aGljaCB0byByZXZlYWwuIEpTIHdpbGwgdXNlIGEgUmVnRXhwIHRvIHRhcmdldCBzdGFuZGFyZCBjbGFzc2VzLCBpZiBjaGFuZ2luZyBjbGFzc25hbWVzLCBwYXNzIHlvdXIgY2xhc3Mgd2l0aCB0aGUgYHJldmVhbENsYXNzYCBvcHRpb24uXG4gICAqIEBvcHRpb25cbiAgICogQGV4YW1wbGUgcmV2ZWFsLWZvci1sYXJnZVxuICAgKi9cbiAgcmV2ZWFsT246IG51bGwsXG5cbiAgLyoqXG4gICAqIEZvcmNlIGZvY3VzIHRvIHRoZSBvZmZjYW52YXMgb24gb3Blbi4gSWYgdHJ1ZSwgd2lsbCBmb2N1cyB0aGUgb3BlbmluZyB0cmlnZ2VyIG9uIGNsb3NlLiBTZXRzIHRhYmluZGV4IG9mIFtkYXRhLW9mZi1jYW52YXMtY29udGVudF0gdG8gLTEgZm9yIGFjY2Vzc2liaWxpdHkgcHVycG9zZXMuXG4gICAqIEBvcHRpb25cbiAgICogQGV4YW1wbGUgdHJ1ZVxuICAgKi9cbiAgYXV0b0ZvY3VzOiB0cnVlLFxuXG4gIC8qKlxuICAgKiBDbGFzcyB1c2VkIHRvIGZvcmNlIGFuIG9mZmNhbnZhcyB0byByZW1haW4gb3Blbi4gRm91bmRhdGlvbiBkZWZhdWx0cyBmb3IgdGhpcyBhcmUgYHJldmVhbC1mb3ItbGFyZ2VgICYgYHJldmVhbC1mb3ItbWVkaXVtYC5cbiAgICogQG9wdGlvblxuICAgKiBUT0RPIGltcHJvdmUgdGhlIHJlZ2V4IHRlc3RpbmcgZm9yIHRoaXMuXG4gICAqIEBleGFtcGxlIHJldmVhbC1mb3ItbGFyZ2VcbiAgICovXG4gIHJldmVhbENsYXNzOiAncmV2ZWFsLWZvci0nLFxuXG4gIC8qKlxuICAgKiBUcmlnZ2VycyBvcHRpb25hbCBmb2N1cyB0cmFwcGluZyB3aGVuIG9wZW5pbmcgYW4gb2ZmY2FudmFzLiBTZXRzIHRhYmluZGV4IG9mIFtkYXRhLW9mZi1jYW52YXMtY29udGVudF0gdG8gLTEgZm9yIGFjY2Vzc2liaWxpdHkgcHVycG9zZXMuXG4gICAqIEBvcHRpb25cbiAgICogQGV4YW1wbGUgdHJ1ZVxuICAgKi9cbiAgdHJhcEZvY3VzOiBmYWxzZVxufVxuXG4vLyBXaW5kb3cgZXhwb3J0c1xuRm91bmRhdGlvbi5wbHVnaW4oT2ZmQ2FudmFzLCAnT2ZmQ2FudmFzJyk7XG5cbn0oalF1ZXJ5KTtcbiIsIid1c2Ugc3RyaWN0JztcblxuIWZ1bmN0aW9uKCQpIHtcblxuLyoqXG4gKiBSZXZlYWwgbW9kdWxlLlxuICogQG1vZHVsZSBmb3VuZGF0aW9uLnJldmVhbFxuICogQHJlcXVpcmVzIGZvdW5kYXRpb24udXRpbC5rZXlib2FyZFxuICogQHJlcXVpcmVzIGZvdW5kYXRpb24udXRpbC5ib3hcbiAqIEByZXF1aXJlcyBmb3VuZGF0aW9uLnV0aWwudHJpZ2dlcnNcbiAqIEByZXF1aXJlcyBmb3VuZGF0aW9uLnV0aWwubWVkaWFRdWVyeVxuICogQHJlcXVpcmVzIGZvdW5kYXRpb24udXRpbC5tb3Rpb24gaWYgdXNpbmcgYW5pbWF0aW9uc1xuICovXG5cbmNsYXNzIFJldmVhbCB7XG4gIC8qKlxuICAgKiBDcmVhdGVzIGEgbmV3IGluc3RhbmNlIG9mIFJldmVhbC5cbiAgICogQGNsYXNzXG4gICAqIEBwYXJhbSB7alF1ZXJ5fSBlbGVtZW50IC0galF1ZXJ5IG9iamVjdCB0byB1c2UgZm9yIHRoZSBtb2RhbC5cbiAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnMgLSBvcHRpb25hbCBwYXJhbWV0ZXJzLlxuICAgKi9cbiAgY29uc3RydWN0b3IoZWxlbWVudCwgb3B0aW9ucykge1xuICAgIHRoaXMuJGVsZW1lbnQgPSBlbGVtZW50O1xuICAgIHRoaXMub3B0aW9ucyA9ICQuZXh0ZW5kKHt9LCBSZXZlYWwuZGVmYXVsdHMsIHRoaXMuJGVsZW1lbnQuZGF0YSgpLCBvcHRpb25zKTtcbiAgICB0aGlzLl9pbml0KCk7XG5cbiAgICBGb3VuZGF0aW9uLnJlZ2lzdGVyUGx1Z2luKHRoaXMsICdSZXZlYWwnKTtcbiAgICBGb3VuZGF0aW9uLktleWJvYXJkLnJlZ2lzdGVyKCdSZXZlYWwnLCB7XG4gICAgICAnRU5URVInOiAnb3BlbicsXG4gICAgICAnU1BBQ0UnOiAnb3BlbicsXG4gICAgICAnRVNDQVBFJzogJ2Nsb3NlJyxcbiAgICAgICdUQUInOiAndGFiX2ZvcndhcmQnLFxuICAgICAgJ1NISUZUX1RBQic6ICd0YWJfYmFja3dhcmQnXG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogSW5pdGlhbGl6ZXMgdGhlIG1vZGFsIGJ5IGFkZGluZyB0aGUgb3ZlcmxheSBhbmQgY2xvc2UgYnV0dG9ucywgKGlmIHNlbGVjdGVkKS5cbiAgICogQHByaXZhdGVcbiAgICovXG4gIF9pbml0KCkge1xuICAgIHRoaXMuaWQgPSB0aGlzLiRlbGVtZW50LmF0dHIoJ2lkJyk7XG4gICAgdGhpcy5pc0FjdGl2ZSA9IGZhbHNlO1xuICAgIHRoaXMuY2FjaGVkID0ge21xOiBGb3VuZGF0aW9uLk1lZGlhUXVlcnkuY3VycmVudH07XG4gICAgdGhpcy5pc01vYmlsZSA9IG1vYmlsZVNuaWZmKCk7XG5cbiAgICB0aGlzLiRhbmNob3IgPSAkKGBbZGF0YS1vcGVuPVwiJHt0aGlzLmlkfVwiXWApLmxlbmd0aCA/ICQoYFtkYXRhLW9wZW49XCIke3RoaXMuaWR9XCJdYCkgOiAkKGBbZGF0YS10b2dnbGU9XCIke3RoaXMuaWR9XCJdYCk7XG4gICAgdGhpcy4kYW5jaG9yLmF0dHIoe1xuICAgICAgJ2FyaWEtY29udHJvbHMnOiB0aGlzLmlkLFxuICAgICAgJ2FyaWEtaGFzcG9wdXAnOiB0cnVlLFxuICAgICAgJ3RhYmluZGV4JzogMFxuICAgIH0pO1xuXG4gICAgaWYgKHRoaXMub3B0aW9ucy5mdWxsU2NyZWVuIHx8IHRoaXMuJGVsZW1lbnQuaGFzQ2xhc3MoJ2Z1bGwnKSkge1xuICAgICAgdGhpcy5vcHRpb25zLmZ1bGxTY3JlZW4gPSB0cnVlO1xuICAgICAgdGhpcy5vcHRpb25zLm92ZXJsYXkgPSBmYWxzZTtcbiAgICB9XG4gICAgaWYgKHRoaXMub3B0aW9ucy5vdmVybGF5ICYmICF0aGlzLiRvdmVybGF5KSB7XG4gICAgICB0aGlzLiRvdmVybGF5ID0gdGhpcy5fbWFrZU92ZXJsYXkodGhpcy5pZCk7XG4gICAgfVxuXG4gICAgdGhpcy4kZWxlbWVudC5hdHRyKHtcbiAgICAgICAgJ3JvbGUnOiAnZGlhbG9nJyxcbiAgICAgICAgJ2FyaWEtaGlkZGVuJzogdHJ1ZSxcbiAgICAgICAgJ2RhdGEteWV0aS1ib3gnOiB0aGlzLmlkLFxuICAgICAgICAnZGF0YS1yZXNpemUnOiB0aGlzLmlkXG4gICAgfSk7XG5cbiAgICBpZih0aGlzLiRvdmVybGF5KSB7XG4gICAgICB0aGlzLiRlbGVtZW50LmRldGFjaCgpLmFwcGVuZFRvKHRoaXMuJG92ZXJsYXkpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLiRlbGVtZW50LmRldGFjaCgpLmFwcGVuZFRvKCQoJ2JvZHknKSk7XG4gICAgICB0aGlzLiRlbGVtZW50LmFkZENsYXNzKCd3aXRob3V0LW92ZXJsYXknKTtcbiAgICB9XG4gICAgdGhpcy5fZXZlbnRzKCk7XG4gICAgaWYgKHRoaXMub3B0aW9ucy5kZWVwTGluayAmJiB3aW5kb3cubG9jYXRpb24uaGFzaCA9PT0gKCBgIyR7dGhpcy5pZH1gKSkge1xuICAgICAgJCh3aW5kb3cpLm9uZSgnbG9hZC56Zi5yZXZlYWwnLCB0aGlzLm9wZW4uYmluZCh0aGlzKSk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIENyZWF0ZXMgYW4gb3ZlcmxheSBkaXYgdG8gZGlzcGxheSBiZWhpbmQgdGhlIG1vZGFsLlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgX21ha2VPdmVybGF5KGlkKSB7XG4gICAgdmFyICRvdmVybGF5ID0gJCgnPGRpdj48L2Rpdj4nKVxuICAgICAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoJ3JldmVhbC1vdmVybGF5JylcbiAgICAgICAgICAgICAgICAgICAgLmFwcGVuZFRvKCdib2R5Jyk7XG4gICAgcmV0dXJuICRvdmVybGF5O1xuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZXMgcG9zaXRpb24gb2YgbW9kYWxcbiAgICogVE9ETzogIEZpZ3VyZSBvdXQgaWYgd2UgYWN0dWFsbHkgbmVlZCB0byBjYWNoZSB0aGVzZSB2YWx1ZXMgb3IgaWYgaXQgZG9lc24ndCBtYXR0ZXJcbiAgICogQHByaXZhdGVcbiAgICovXG4gIF91cGRhdGVQb3NpdGlvbigpIHtcbiAgICB2YXIgd2lkdGggPSB0aGlzLiRlbGVtZW50Lm91dGVyV2lkdGgoKTtcbiAgICB2YXIgb3V0ZXJXaWR0aCA9ICQod2luZG93KS53aWR0aCgpO1xuICAgIHZhciBoZWlnaHQgPSB0aGlzLiRlbGVtZW50Lm91dGVySGVpZ2h0KCk7XG4gICAgdmFyIG91dGVySGVpZ2h0ID0gJCh3aW5kb3cpLmhlaWdodCgpO1xuICAgIHZhciBsZWZ0LCB0b3A7XG4gICAgaWYgKHRoaXMub3B0aW9ucy5oT2Zmc2V0ID09PSAnYXV0bycpIHtcbiAgICAgIGxlZnQgPSBwYXJzZUludCgob3V0ZXJXaWR0aCAtIHdpZHRoKSAvIDIsIDEwKTtcbiAgICB9IGVsc2Uge1xuICAgICAgbGVmdCA9IHBhcnNlSW50KHRoaXMub3B0aW9ucy5oT2Zmc2V0LCAxMCk7XG4gICAgfVxuICAgIGlmICh0aGlzLm9wdGlvbnMudk9mZnNldCA9PT0gJ2F1dG8nKSB7XG4gICAgICBpZiAoaGVpZ2h0ID4gb3V0ZXJIZWlnaHQpIHtcbiAgICAgICAgdG9wID0gcGFyc2VJbnQoTWF0aC5taW4oMTAwLCBvdXRlckhlaWdodCAvIDEwKSwgMTApO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdG9wID0gcGFyc2VJbnQoKG91dGVySGVpZ2h0IC0gaGVpZ2h0KSAvIDQsIDEwKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgdG9wID0gcGFyc2VJbnQodGhpcy5vcHRpb25zLnZPZmZzZXQsIDEwKTtcbiAgICB9XG4gICAgdGhpcy4kZWxlbWVudC5jc3Moe3RvcDogdG9wICsgJ3B4J30pO1xuICAgIC8vIG9ubHkgd29ycnkgYWJvdXQgbGVmdCBpZiB3ZSBkb24ndCBoYXZlIGFuIG92ZXJsYXkgb3Igd2UgaGF2ZWEgIGhvcml6b250YWwgb2Zmc2V0LFxuICAgIC8vIG90aGVyd2lzZSB3ZSdyZSBwZXJmZWN0bHkgaW4gdGhlIG1pZGRsZVxuICAgIGlmKCF0aGlzLiRvdmVybGF5IHx8ICh0aGlzLm9wdGlvbnMuaE9mZnNldCAhPT0gJ2F1dG8nKSkge1xuICAgICAgdGhpcy4kZWxlbWVudC5jc3Moe2xlZnQ6IGxlZnQgKyAncHgnfSk7XG4gICAgICB0aGlzLiRlbGVtZW50LmNzcyh7bWFyZ2luOiAnMHB4J30pO1xuICAgIH1cblxuICB9XG5cbiAgLyoqXG4gICAqIEFkZHMgZXZlbnQgaGFuZGxlcnMgZm9yIHRoZSBtb2RhbC5cbiAgICogQHByaXZhdGVcbiAgICovXG4gIF9ldmVudHMoKSB7XG4gICAgdmFyIF90aGlzID0gdGhpcztcblxuICAgIHRoaXMuJGVsZW1lbnQub24oe1xuICAgICAgJ29wZW4uemYudHJpZ2dlcic6IHRoaXMub3Blbi5iaW5kKHRoaXMpLFxuICAgICAgJ2Nsb3NlLnpmLnRyaWdnZXInOiAoZXZlbnQsICRlbGVtZW50KSA9PiB7XG4gICAgICAgIGlmICgoZXZlbnQudGFyZ2V0ID09PSBfdGhpcy4kZWxlbWVudFswXSkgfHxcbiAgICAgICAgICAgICgkKGV2ZW50LnRhcmdldCkucGFyZW50cygnW2RhdGEtY2xvc2FibGVdJylbMF0gPT09ICRlbGVtZW50KSkgeyAvLyBvbmx5IGNsb3NlIHJldmVhbCB3aGVuIGl0J3MgZXhwbGljaXRseSBjYWxsZWRcbiAgICAgICAgICByZXR1cm4gdGhpcy5jbG9zZS5hcHBseSh0aGlzKTtcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgICd0b2dnbGUuemYudHJpZ2dlcic6IHRoaXMudG9nZ2xlLmJpbmQodGhpcyksXG4gICAgICAncmVzaXplbWUuemYudHJpZ2dlcic6IGZ1bmN0aW9uKCkge1xuICAgICAgICBfdGhpcy5fdXBkYXRlUG9zaXRpb24oKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIGlmICh0aGlzLiRhbmNob3IubGVuZ3RoKSB7XG4gICAgICB0aGlzLiRhbmNob3Iub24oJ2tleWRvd24uemYucmV2ZWFsJywgZnVuY3Rpb24oZSkge1xuICAgICAgICBpZiAoZS53aGljaCA9PT0gMTMgfHwgZS53aGljaCA9PT0gMzIpIHtcbiAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICBfdGhpcy5vcGVuKCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlmICh0aGlzLm9wdGlvbnMuY2xvc2VPbkNsaWNrICYmIHRoaXMub3B0aW9ucy5vdmVybGF5KSB7XG4gICAgICB0aGlzLiRvdmVybGF5Lm9mZignLnpmLnJldmVhbCcpLm9uKCdjbGljay56Zi5yZXZlYWwnLCBmdW5jdGlvbihlKSB7XG4gICAgICAgIGlmIChlLnRhcmdldCA9PT0gX3RoaXMuJGVsZW1lbnRbMF0gfHwgXG4gICAgICAgICAgJC5jb250YWlucyhfdGhpcy4kZWxlbWVudFswXSwgZS50YXJnZXQpIHx8IFxuICAgICAgICAgICAgISQuY29udGFpbnMoZG9jdW1lbnQsIGUudGFyZ2V0KSkgeyBcbiAgICAgICAgICAgICAgcmV0dXJuOyBcbiAgICAgICAgfVxuICAgICAgICBfdGhpcy5jbG9zZSgpO1xuICAgICAgfSk7XG4gICAgfVxuICAgIGlmICh0aGlzLm9wdGlvbnMuZGVlcExpbmspIHtcbiAgICAgICQod2luZG93KS5vbihgcG9wc3RhdGUuemYucmV2ZWFsOiR7dGhpcy5pZH1gLCB0aGlzLl9oYW5kbGVTdGF0ZS5iaW5kKHRoaXMpKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogSGFuZGxlcyBtb2RhbCBtZXRob2RzIG9uIGJhY2svZm9yd2FyZCBidXR0b24gY2xpY2tzIG9yIGFueSBvdGhlciBldmVudCB0aGF0IHRyaWdnZXJzIHBvcHN0YXRlLlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgX2hhbmRsZVN0YXRlKGUpIHtcbiAgICBpZih3aW5kb3cubG9jYXRpb24uaGFzaCA9PT0gKCAnIycgKyB0aGlzLmlkKSAmJiAhdGhpcy5pc0FjdGl2ZSl7IHRoaXMub3BlbigpOyB9XG4gICAgZWxzZXsgdGhpcy5jbG9zZSgpOyB9XG4gIH1cblxuXG4gIC8qKlxuICAgKiBPcGVucyB0aGUgbW9kYWwgY29udHJvbGxlZCBieSBgdGhpcy4kYW5jaG9yYCwgYW5kIGNsb3NlcyBhbGwgb3RoZXJzIGJ5IGRlZmF1bHQuXG4gICAqIEBmdW5jdGlvblxuICAgKiBAZmlyZXMgUmV2ZWFsI2Nsb3NlbWVcbiAgICogQGZpcmVzIFJldmVhbCNvcGVuXG4gICAqL1xuICBvcGVuKCkge1xuICAgIGlmICh0aGlzLm9wdGlvbnMuZGVlcExpbmspIHtcbiAgICAgIHZhciBoYXNoID0gYCMke3RoaXMuaWR9YDtcblxuICAgICAgaWYgKHdpbmRvdy5oaXN0b3J5LnB1c2hTdGF0ZSkge1xuICAgICAgICB3aW5kb3cuaGlzdG9yeS5wdXNoU3RhdGUobnVsbCwgbnVsbCwgaGFzaCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB3aW5kb3cubG9jYXRpb24uaGFzaCA9IGhhc2g7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdGhpcy5pc0FjdGl2ZSA9IHRydWU7XG5cbiAgICAvLyBNYWtlIGVsZW1lbnRzIGludmlzaWJsZSwgYnV0IHJlbW92ZSBkaXNwbGF5OiBub25lIHNvIHdlIGNhbiBnZXQgc2l6ZSBhbmQgcG9zaXRpb25pbmdcbiAgICB0aGlzLiRlbGVtZW50XG4gICAgICAgIC5jc3MoeyAndmlzaWJpbGl0eSc6ICdoaWRkZW4nIH0pXG4gICAgICAgIC5zaG93KClcbiAgICAgICAgLnNjcm9sbFRvcCgwKTtcbiAgICBpZiAodGhpcy5vcHRpb25zLm92ZXJsYXkpIHtcbiAgICAgIHRoaXMuJG92ZXJsYXkuY3NzKHsndmlzaWJpbGl0eSc6ICdoaWRkZW4nfSkuc2hvdygpO1xuICAgIH1cblxuICAgIHRoaXMuX3VwZGF0ZVBvc2l0aW9uKCk7XG5cbiAgICB0aGlzLiRlbGVtZW50XG4gICAgICAuaGlkZSgpXG4gICAgICAuY3NzKHsgJ3Zpc2liaWxpdHknOiAnJyB9KTtcblxuICAgIGlmKHRoaXMuJG92ZXJsYXkpIHtcbiAgICAgIHRoaXMuJG92ZXJsYXkuY3NzKHsndmlzaWJpbGl0eSc6ICcnfSkuaGlkZSgpO1xuICAgICAgaWYodGhpcy4kZWxlbWVudC5oYXNDbGFzcygnZmFzdCcpKSB7XG4gICAgICAgIHRoaXMuJG92ZXJsYXkuYWRkQ2xhc3MoJ2Zhc3QnKTtcbiAgICAgIH0gZWxzZSBpZiAodGhpcy4kZWxlbWVudC5oYXNDbGFzcygnc2xvdycpKSB7XG4gICAgICAgIHRoaXMuJG92ZXJsYXkuYWRkQ2xhc3MoJ3Nsb3cnKTtcbiAgICAgIH1cbiAgICB9XG5cblxuICAgIGlmICghdGhpcy5vcHRpb25zLm11bHRpcGxlT3BlbmVkKSB7XG4gICAgICAvKipcbiAgICAgICAqIEZpcmVzIGltbWVkaWF0ZWx5IGJlZm9yZSB0aGUgbW9kYWwgb3BlbnMuXG4gICAgICAgKiBDbG9zZXMgYW55IG90aGVyIG1vZGFscyB0aGF0IGFyZSBjdXJyZW50bHkgb3BlblxuICAgICAgICogQGV2ZW50IFJldmVhbCNjbG9zZW1lXG4gICAgICAgKi9cbiAgICAgIHRoaXMuJGVsZW1lbnQudHJpZ2dlcignY2xvc2VtZS56Zi5yZXZlYWwnLCB0aGlzLmlkKTtcbiAgICB9XG4gICAgLy8gTW90aW9uIFVJIG1ldGhvZCBvZiByZXZlYWxcbiAgICBpZiAodGhpcy5vcHRpb25zLmFuaW1hdGlvbkluKSB7XG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgZnVuY3Rpb24gYWZ0ZXJBbmltYXRpb25Gb2N1cygpe1xuICAgICAgICBfdGhpcy4kZWxlbWVudFxuICAgICAgICAgIC5hdHRyKHtcbiAgICAgICAgICAgICdhcmlhLWhpZGRlbic6IGZhbHNlLFxuICAgICAgICAgICAgJ3RhYmluZGV4JzogLTFcbiAgICAgICAgICB9KVxuICAgICAgICAgIC5mb2N1cygpO1xuICAgICAgfVxuICAgICAgaWYgKHRoaXMub3B0aW9ucy5vdmVybGF5KSB7XG4gICAgICAgIEZvdW5kYXRpb24uTW90aW9uLmFuaW1hdGVJbih0aGlzLiRvdmVybGF5LCAnZmFkZS1pbicpO1xuICAgICAgfVxuICAgICAgRm91bmRhdGlvbi5Nb3Rpb24uYW5pbWF0ZUluKHRoaXMuJGVsZW1lbnQsIHRoaXMub3B0aW9ucy5hbmltYXRpb25JbiwgKCkgPT4ge1xuICAgICAgICB0aGlzLmZvY3VzYWJsZUVsZW1lbnRzID0gRm91bmRhdGlvbi5LZXlib2FyZC5maW5kRm9jdXNhYmxlKHRoaXMuJGVsZW1lbnQpO1xuICAgICAgICBhZnRlckFuaW1hdGlvbkZvY3VzKCk7XG4gICAgICB9KTtcbiAgICB9XG4gICAgLy8galF1ZXJ5IG1ldGhvZCBvZiByZXZlYWxcbiAgICBlbHNlIHtcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMub3ZlcmxheSkge1xuICAgICAgICB0aGlzLiRvdmVybGF5LnNob3coMCk7XG4gICAgICB9XG4gICAgICB0aGlzLiRlbGVtZW50LnNob3codGhpcy5vcHRpb25zLnNob3dEZWxheSk7XG4gICAgfVxuXG4gICAgLy8gaGFuZGxlIGFjY2Vzc2liaWxpdHlcbiAgICB0aGlzLiRlbGVtZW50XG4gICAgICAuYXR0cih7XG4gICAgICAgICdhcmlhLWhpZGRlbic6IGZhbHNlLFxuICAgICAgICAndGFiaW5kZXgnOiAtMVxuICAgICAgfSlcbiAgICAgIC5mb2N1cygpO1xuXG4gICAgLyoqXG4gICAgICogRmlyZXMgd2hlbiB0aGUgbW9kYWwgaGFzIHN1Y2Nlc3NmdWxseSBvcGVuZWQuXG4gICAgICogQGV2ZW50IFJldmVhbCNvcGVuXG4gICAgICovXG4gICAgdGhpcy4kZWxlbWVudC50cmlnZ2VyKCdvcGVuLnpmLnJldmVhbCcpO1xuXG4gICAgaWYgKHRoaXMuaXNNb2JpbGUpIHtcbiAgICAgIHRoaXMub3JpZ2luYWxTY3JvbGxQb3MgPSB3aW5kb3cucGFnZVlPZmZzZXQ7XG4gICAgICAkKCdodG1sLCBib2R5JykuYWRkQ2xhc3MoJ2lzLXJldmVhbC1vcGVuJyk7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgJCgnYm9keScpLmFkZENsYXNzKCdpcy1yZXZlYWwtb3BlbicpO1xuICAgIH1cblxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgdGhpcy5fZXh0cmFIYW5kbGVycygpO1xuICAgIH0sIDApO1xuICB9XG5cbiAgLyoqXG4gICAqIEFkZHMgZXh0cmEgZXZlbnQgaGFuZGxlcnMgZm9yIHRoZSBib2R5IGFuZCB3aW5kb3cgaWYgbmVjZXNzYXJ5LlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgX2V4dHJhSGFuZGxlcnMoKSB7XG4gICAgdmFyIF90aGlzID0gdGhpcztcbiAgICB0aGlzLmZvY3VzYWJsZUVsZW1lbnRzID0gRm91bmRhdGlvbi5LZXlib2FyZC5maW5kRm9jdXNhYmxlKHRoaXMuJGVsZW1lbnQpO1xuXG4gICAgaWYgKCF0aGlzLm9wdGlvbnMub3ZlcmxheSAmJiB0aGlzLm9wdGlvbnMuY2xvc2VPbkNsaWNrICYmICF0aGlzLm9wdGlvbnMuZnVsbFNjcmVlbikge1xuICAgICAgJCgnYm9keScpLm9uKCdjbGljay56Zi5yZXZlYWwnLCBmdW5jdGlvbihlKSB7XG4gICAgICAgIGlmIChlLnRhcmdldCA9PT0gX3RoaXMuJGVsZW1lbnRbMF0gfHwgXG4gICAgICAgICAgJC5jb250YWlucyhfdGhpcy4kZWxlbWVudFswXSwgZS50YXJnZXQpIHx8IFxuICAgICAgICAgICAgISQuY29udGFpbnMoZG9jdW1lbnQsIGUudGFyZ2V0KSkgeyByZXR1cm47IH1cbiAgICAgICAgX3RoaXMuY2xvc2UoKTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlmICh0aGlzLm9wdGlvbnMuY2xvc2VPbkVzYykge1xuICAgICAgJCh3aW5kb3cpLm9uKCdrZXlkb3duLnpmLnJldmVhbCcsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgRm91bmRhdGlvbi5LZXlib2FyZC5oYW5kbGVLZXkoZSwgJ1JldmVhbCcsIHtcbiAgICAgICAgICBjbG9zZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZiAoX3RoaXMub3B0aW9ucy5jbG9zZU9uRXNjKSB7XG4gICAgICAgICAgICAgIF90aGlzLmNsb3NlKCk7XG4gICAgICAgICAgICAgIF90aGlzLiRhbmNob3IuZm9jdXMoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgLy8gbG9jayBmb2N1cyB3aXRoaW4gbW9kYWwgd2hpbGUgdGFiYmluZ1xuICAgIHRoaXMuJGVsZW1lbnQub24oJ2tleWRvd24uemYucmV2ZWFsJywgZnVuY3Rpb24oZSkge1xuICAgICAgdmFyICR0YXJnZXQgPSAkKHRoaXMpO1xuICAgICAgLy8gaGFuZGxlIGtleWJvYXJkIGV2ZW50IHdpdGgga2V5Ym9hcmQgdXRpbFxuICAgICAgRm91bmRhdGlvbi5LZXlib2FyZC5oYW5kbGVLZXkoZSwgJ1JldmVhbCcsIHtcbiAgICAgICAgdGFiX2ZvcndhcmQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIF90aGlzLmZvY3VzYWJsZUVsZW1lbnRzID0gRm91bmRhdGlvbi5LZXlib2FyZC5maW5kRm9jdXNhYmxlKF90aGlzLiRlbGVtZW50KTtcbiAgICAgICAgICBpZiAoX3RoaXMuJGVsZW1lbnQuZmluZCgnOmZvY3VzJykuaXMoX3RoaXMuZm9jdXNhYmxlRWxlbWVudHMuZXEoLTEpKSkgeyAvLyBsZWZ0IG1vZGFsIGRvd253YXJkcywgc2V0dGluZyBmb2N1cyB0byBmaXJzdCBlbGVtZW50XG4gICAgICAgICAgICBfdGhpcy5mb2N1c2FibGVFbGVtZW50cy5lcSgwKS5mb2N1cygpO1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmIChfdGhpcy5mb2N1c2FibGVFbGVtZW50cy5sZW5ndGggPT09IDApIHsgLy8gbm8gZm9jdXNhYmxlIGVsZW1lbnRzIGluc2lkZSB0aGUgbW9kYWwgYXQgYWxsLCBwcmV2ZW50IHRhYmJpbmcgaW4gZ2VuZXJhbFxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB0YWJfYmFja3dhcmQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIF90aGlzLmZvY3VzYWJsZUVsZW1lbnRzID0gRm91bmRhdGlvbi5LZXlib2FyZC5maW5kRm9jdXNhYmxlKF90aGlzLiRlbGVtZW50KTtcbiAgICAgICAgICBpZiAoX3RoaXMuJGVsZW1lbnQuZmluZCgnOmZvY3VzJykuaXMoX3RoaXMuZm9jdXNhYmxlRWxlbWVudHMuZXEoMCkpIHx8IF90aGlzLiRlbGVtZW50LmlzKCc6Zm9jdXMnKSkgeyAvLyBsZWZ0IG1vZGFsIHVwd2FyZHMsIHNldHRpbmcgZm9jdXMgdG8gbGFzdCBlbGVtZW50XG4gICAgICAgICAgICBfdGhpcy5mb2N1c2FibGVFbGVtZW50cy5lcSgtMSkuZm9jdXMoKTtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoX3RoaXMuZm9jdXNhYmxlRWxlbWVudHMubGVuZ3RoID09PSAwKSB7IC8vIG5vIGZvY3VzYWJsZSBlbGVtZW50cyBpbnNpZGUgdGhlIG1vZGFsIGF0IGFsbCwgcHJldmVudCB0YWJiaW5nIGluIGdlbmVyYWxcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgb3BlbjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYgKF90aGlzLiRlbGVtZW50LmZpbmQoJzpmb2N1cycpLmlzKF90aGlzLiRlbGVtZW50LmZpbmQoJ1tkYXRhLWNsb3NlXScpKSkge1xuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHsgLy8gc2V0IGZvY3VzIGJhY2sgdG8gYW5jaG9yIGlmIGNsb3NlIGJ1dHRvbiBoYXMgYmVlbiBhY3RpdmF0ZWRcbiAgICAgICAgICAgICAgX3RoaXMuJGFuY2hvci5mb2N1cygpO1xuICAgICAgICAgICAgfSwgMSk7XG4gICAgICAgICAgfSBlbHNlIGlmICgkdGFyZ2V0LmlzKF90aGlzLmZvY3VzYWJsZUVsZW1lbnRzKSkgeyAvLyBkb250J3QgdHJpZ2dlciBpZiBhY3VhbCBlbGVtZW50IGhhcyBmb2N1cyAoaS5lLiBpbnB1dHMsIGxpbmtzLCAuLi4pXG4gICAgICAgICAgICBfdGhpcy5vcGVuKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBjbG9zZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYgKF90aGlzLm9wdGlvbnMuY2xvc2VPbkVzYykge1xuICAgICAgICAgICAgX3RoaXMuY2xvc2UoKTtcbiAgICAgICAgICAgIF90aGlzLiRhbmNob3IuZm9jdXMoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGhhbmRsZWQ6IGZ1bmN0aW9uKHByZXZlbnREZWZhdWx0KSB7XG4gICAgICAgICAgaWYgKHByZXZlbnREZWZhdWx0KSB7XG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDbG9zZXMgdGhlIG1vZGFsLlxuICAgKiBAZnVuY3Rpb25cbiAgICogQGZpcmVzIFJldmVhbCNjbG9zZWRcbiAgICovXG4gIGNsb3NlKCkge1xuICAgIGlmICghdGhpcy5pc0FjdGl2ZSB8fCAhdGhpcy4kZWxlbWVudC5pcygnOnZpc2libGUnKSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICB2YXIgX3RoaXMgPSB0aGlzO1xuXG4gICAgLy8gTW90aW9uIFVJIG1ldGhvZCBvZiBoaWRpbmdcbiAgICBpZiAodGhpcy5vcHRpb25zLmFuaW1hdGlvbk91dCkge1xuICAgICAgaWYgKHRoaXMub3B0aW9ucy5vdmVybGF5KSB7XG4gICAgICAgIEZvdW5kYXRpb24uTW90aW9uLmFuaW1hdGVPdXQodGhpcy4kb3ZlcmxheSwgJ2ZhZGUtb3V0JywgZmluaXNoVXApO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGZpbmlzaFVwKCk7XG4gICAgICB9XG5cbiAgICAgIEZvdW5kYXRpb24uTW90aW9uLmFuaW1hdGVPdXQodGhpcy4kZWxlbWVudCwgdGhpcy5vcHRpb25zLmFuaW1hdGlvbk91dCk7XG4gICAgfVxuICAgIC8vIGpRdWVyeSBtZXRob2Qgb2YgaGlkaW5nXG4gICAgZWxzZSB7XG4gICAgICBpZiAodGhpcy5vcHRpb25zLm92ZXJsYXkpIHtcbiAgICAgICAgdGhpcy4kb3ZlcmxheS5oaWRlKDAsIGZpbmlzaFVwKTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBmaW5pc2hVcCgpO1xuICAgICAgfVxuXG4gICAgICB0aGlzLiRlbGVtZW50LmhpZGUodGhpcy5vcHRpb25zLmhpZGVEZWxheSk7XG4gICAgfVxuXG4gICAgLy8gQ29uZGl0aW9uYWxzIHRvIHJlbW92ZSBleHRyYSBldmVudCBsaXN0ZW5lcnMgYWRkZWQgb24gb3BlblxuICAgIGlmICh0aGlzLm9wdGlvbnMuY2xvc2VPbkVzYykge1xuICAgICAgJCh3aW5kb3cpLm9mZigna2V5ZG93bi56Zi5yZXZlYWwnKTtcbiAgICB9XG5cbiAgICBpZiAoIXRoaXMub3B0aW9ucy5vdmVybGF5ICYmIHRoaXMub3B0aW9ucy5jbG9zZU9uQ2xpY2spIHtcbiAgICAgICQoJ2JvZHknKS5vZmYoJ2NsaWNrLnpmLnJldmVhbCcpO1xuICAgIH1cblxuICAgIHRoaXMuJGVsZW1lbnQub2ZmKCdrZXlkb3duLnpmLnJldmVhbCcpO1xuXG4gICAgZnVuY3Rpb24gZmluaXNoVXAoKSB7XG4gICAgICBpZiAoX3RoaXMuaXNNb2JpbGUpIHtcbiAgICAgICAgJCgnaHRtbCwgYm9keScpLnJlbW92ZUNsYXNzKCdpcy1yZXZlYWwtb3BlbicpO1xuICAgICAgICBpZihfdGhpcy5vcmlnaW5hbFNjcm9sbFBvcykge1xuICAgICAgICAgICQoJ2JvZHknKS5zY3JvbGxUb3AoX3RoaXMub3JpZ2luYWxTY3JvbGxQb3MpO1xuICAgICAgICAgIF90aGlzLm9yaWdpbmFsU2Nyb2xsUG9zID0gbnVsbDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgICQoJ2JvZHknKS5yZW1vdmVDbGFzcygnaXMtcmV2ZWFsLW9wZW4nKTtcbiAgICAgIH1cblxuICAgICAgX3RoaXMuJGVsZW1lbnQuYXR0cignYXJpYS1oaWRkZW4nLCB0cnVlKTtcblxuICAgICAgLyoqXG4gICAgICAqIEZpcmVzIHdoZW4gdGhlIG1vZGFsIGlzIGRvbmUgY2xvc2luZy5cbiAgICAgICogQGV2ZW50IFJldmVhbCNjbG9zZWRcbiAgICAgICovXG4gICAgICBfdGhpcy4kZWxlbWVudC50cmlnZ2VyKCdjbG9zZWQuemYucmV2ZWFsJyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgKiBSZXNldHMgdGhlIG1vZGFsIGNvbnRlbnRcbiAgICAqIFRoaXMgcHJldmVudHMgYSBydW5uaW5nIHZpZGVvIHRvIGtlZXAgZ29pbmcgaW4gdGhlIGJhY2tncm91bmRcbiAgICAqL1xuICAgIGlmICh0aGlzLm9wdGlvbnMucmVzZXRPbkNsb3NlKSB7XG4gICAgICB0aGlzLiRlbGVtZW50Lmh0bWwodGhpcy4kZWxlbWVudC5odG1sKCkpO1xuICAgIH1cblxuICAgIHRoaXMuaXNBY3RpdmUgPSBmYWxzZTtcbiAgICAgaWYgKF90aGlzLm9wdGlvbnMuZGVlcExpbmspIHtcbiAgICAgICBpZiAod2luZG93Lmhpc3RvcnkucmVwbGFjZVN0YXRlKSB7XG4gICAgICAgICB3aW5kb3cuaGlzdG9yeS5yZXBsYWNlU3RhdGUoXCJcIiwgZG9jdW1lbnQudGl0bGUsIHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZSk7XG4gICAgICAgfSBlbHNlIHtcbiAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5oYXNoID0gJyc7XG4gICAgICAgfVxuICAgICB9XG4gIH1cblxuICAvKipcbiAgICogVG9nZ2xlcyB0aGUgb3Blbi9jbG9zZWQgc3RhdGUgb2YgYSBtb2RhbC5cbiAgICogQGZ1bmN0aW9uXG4gICAqL1xuICB0b2dnbGUoKSB7XG4gICAgaWYgKHRoaXMuaXNBY3RpdmUpIHtcbiAgICAgIHRoaXMuY2xvc2UoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5vcGVuKCk7XG4gICAgfVxuICB9O1xuXG4gIC8qKlxuICAgKiBEZXN0cm95cyBhbiBpbnN0YW5jZSBvZiBhIG1vZGFsLlxuICAgKiBAZnVuY3Rpb25cbiAgICovXG4gIGRlc3Ryb3koKSB7XG4gICAgaWYgKHRoaXMub3B0aW9ucy5vdmVybGF5KSB7XG4gICAgICB0aGlzLiRlbGVtZW50LmFwcGVuZFRvKCQoJ2JvZHknKSk7IC8vIG1vdmUgJGVsZW1lbnQgb3V0c2lkZSBvZiAkb3ZlcmxheSB0byBwcmV2ZW50IGVycm9yIHVucmVnaXN0ZXJQbHVnaW4oKVxuICAgICAgdGhpcy4kb3ZlcmxheS5oaWRlKCkub2ZmKCkucmVtb3ZlKCk7XG4gICAgfVxuICAgIHRoaXMuJGVsZW1lbnQuaGlkZSgpLm9mZigpO1xuICAgIHRoaXMuJGFuY2hvci5vZmYoJy56ZicpO1xuICAgICQod2luZG93KS5vZmYoYC56Zi5yZXZlYWw6JHt0aGlzLmlkfWApO1xuXG4gICAgRm91bmRhdGlvbi51bnJlZ2lzdGVyUGx1Z2luKHRoaXMpO1xuICB9O1xufVxuXG5SZXZlYWwuZGVmYXVsdHMgPSB7XG4gIC8qKlxuICAgKiBNb3Rpb24tVUkgY2xhc3MgdG8gdXNlIGZvciBhbmltYXRlZCBlbGVtZW50cy4gSWYgbm9uZSB1c2VkLCBkZWZhdWx0cyB0byBzaW1wbGUgc2hvdy9oaWRlLlxuICAgKiBAb3B0aW9uXG4gICAqIEBleGFtcGxlICdzbGlkZS1pbi1sZWZ0J1xuICAgKi9cbiAgYW5pbWF0aW9uSW46ICcnLFxuICAvKipcbiAgICogTW90aW9uLVVJIGNsYXNzIHRvIHVzZSBmb3IgYW5pbWF0ZWQgZWxlbWVudHMuIElmIG5vbmUgdXNlZCwgZGVmYXVsdHMgdG8gc2ltcGxlIHNob3cvaGlkZS5cbiAgICogQG9wdGlvblxuICAgKiBAZXhhbXBsZSAnc2xpZGUtb3V0LXJpZ2h0J1xuICAgKi9cbiAgYW5pbWF0aW9uT3V0OiAnJyxcbiAgLyoqXG4gICAqIFRpbWUsIGluIG1zLCB0byBkZWxheSB0aGUgb3BlbmluZyBvZiBhIG1vZGFsIGFmdGVyIGEgY2xpY2sgaWYgbm8gYW5pbWF0aW9uIHVzZWQuXG4gICAqIEBvcHRpb25cbiAgICogQGV4YW1wbGUgMTBcbiAgICovXG4gIHNob3dEZWxheTogMCxcbiAgLyoqXG4gICAqIFRpbWUsIGluIG1zLCB0byBkZWxheSB0aGUgY2xvc2luZyBvZiBhIG1vZGFsIGFmdGVyIGEgY2xpY2sgaWYgbm8gYW5pbWF0aW9uIHVzZWQuXG4gICAqIEBvcHRpb25cbiAgICogQGV4YW1wbGUgMTBcbiAgICovXG4gIGhpZGVEZWxheTogMCxcbiAgLyoqXG4gICAqIEFsbG93cyBhIGNsaWNrIG9uIHRoZSBib2R5L292ZXJsYXkgdG8gY2xvc2UgdGhlIG1vZGFsLlxuICAgKiBAb3B0aW9uXG4gICAqIEBleGFtcGxlIHRydWVcbiAgICovXG4gIGNsb3NlT25DbGljazogdHJ1ZSxcbiAgLyoqXG4gICAqIEFsbG93cyB0aGUgbW9kYWwgdG8gY2xvc2UgaWYgdGhlIHVzZXIgcHJlc3NlcyB0aGUgYEVTQ0FQRWAga2V5LlxuICAgKiBAb3B0aW9uXG4gICAqIEBleGFtcGxlIHRydWVcbiAgICovXG4gIGNsb3NlT25Fc2M6IHRydWUsXG4gIC8qKlxuICAgKiBJZiB0cnVlLCBhbGxvd3MgbXVsdGlwbGUgbW9kYWxzIHRvIGJlIGRpc3BsYXllZCBhdCBvbmNlLlxuICAgKiBAb3B0aW9uXG4gICAqIEBleGFtcGxlIGZhbHNlXG4gICAqL1xuICBtdWx0aXBsZU9wZW5lZDogZmFsc2UsXG4gIC8qKlxuICAgKiBEaXN0YW5jZSwgaW4gcGl4ZWxzLCB0aGUgbW9kYWwgc2hvdWxkIHB1c2ggZG93biBmcm9tIHRoZSB0b3Agb2YgdGhlIHNjcmVlbi5cbiAgICogQG9wdGlvblxuICAgKiBAZXhhbXBsZSBhdXRvXG4gICAqL1xuICB2T2Zmc2V0OiAnYXV0bycsXG4gIC8qKlxuICAgKiBEaXN0YW5jZSwgaW4gcGl4ZWxzLCB0aGUgbW9kYWwgc2hvdWxkIHB1c2ggaW4gZnJvbSB0aGUgc2lkZSBvZiB0aGUgc2NyZWVuLlxuICAgKiBAb3B0aW9uXG4gICAqIEBleGFtcGxlIGF1dG9cbiAgICovXG4gIGhPZmZzZXQ6ICdhdXRvJyxcbiAgLyoqXG4gICAqIEFsbG93cyB0aGUgbW9kYWwgdG8gYmUgZnVsbHNjcmVlbiwgY29tcGxldGVseSBibG9ja2luZyBvdXQgdGhlIHJlc3Qgb2YgdGhlIHZpZXcuIEpTIGNoZWNrcyBmb3IgdGhpcyBhcyB3ZWxsLlxuICAgKiBAb3B0aW9uXG4gICAqIEBleGFtcGxlIGZhbHNlXG4gICAqL1xuICBmdWxsU2NyZWVuOiBmYWxzZSxcbiAgLyoqXG4gICAqIFBlcmNlbnRhZ2Ugb2Ygc2NyZWVuIGhlaWdodCB0aGUgbW9kYWwgc2hvdWxkIHB1c2ggdXAgZnJvbSB0aGUgYm90dG9tIG9mIHRoZSB2aWV3LlxuICAgKiBAb3B0aW9uXG4gICAqIEBleGFtcGxlIDEwXG4gICAqL1xuICBidG1PZmZzZXRQY3Q6IDEwLFxuICAvKipcbiAgICogQWxsb3dzIHRoZSBtb2RhbCB0byBnZW5lcmF0ZSBhbiBvdmVybGF5IGRpdiwgd2hpY2ggd2lsbCBjb3ZlciB0aGUgdmlldyB3aGVuIG1vZGFsIG9wZW5zLlxuICAgKiBAb3B0aW9uXG4gICAqIEBleGFtcGxlIHRydWVcbiAgICovXG4gIG92ZXJsYXk6IHRydWUsXG4gIC8qKlxuICAgKiBBbGxvd3MgdGhlIG1vZGFsIHRvIHJlbW92ZSBhbmQgcmVpbmplY3QgbWFya3VwIG9uIGNsb3NlLiBTaG91bGQgYmUgdHJ1ZSBpZiB1c2luZyB2aWRlbyBlbGVtZW50cyB3L28gdXNpbmcgcHJvdmlkZXIncyBhcGksIG90aGVyd2lzZSwgdmlkZW9zIHdpbGwgY29udGludWUgdG8gcGxheSBpbiB0aGUgYmFja2dyb3VuZC5cbiAgICogQG9wdGlvblxuICAgKiBAZXhhbXBsZSBmYWxzZVxuICAgKi9cbiAgcmVzZXRPbkNsb3NlOiBmYWxzZSxcbiAgLyoqXG4gICAqIEFsbG93cyB0aGUgbW9kYWwgdG8gYWx0ZXIgdGhlIHVybCBvbiBvcGVuL2Nsb3NlLCBhbmQgYWxsb3dzIHRoZSB1c2Ugb2YgdGhlIGBiYWNrYCBidXR0b24gdG8gY2xvc2UgbW9kYWxzLiBBTFNPLCBhbGxvd3MgYSBtb2RhbCB0byBhdXRvLW1hbmlhY2FsbHkgb3BlbiBvbiBwYWdlIGxvYWQgSUYgdGhlIGhhc2ggPT09IHRoZSBtb2RhbCdzIHVzZXItc2V0IGlkLlxuICAgKiBAb3B0aW9uXG4gICAqIEBleGFtcGxlIGZhbHNlXG4gICAqL1xuICBkZWVwTGluazogZmFsc2Vcbn07XG5cbi8vIFdpbmRvdyBleHBvcnRzXG5Gb3VuZGF0aW9uLnBsdWdpbihSZXZlYWwsICdSZXZlYWwnKTtcblxuZnVuY3Rpb24gaVBob25lU25pZmYoKSB7XG4gIHJldHVybiAvaVAoYWR8aG9uZXxvZCkuKk9TLy50ZXN0KHdpbmRvdy5uYXZpZ2F0b3IudXNlckFnZW50KTtcbn1cblxuZnVuY3Rpb24gYW5kcm9pZFNuaWZmKCkge1xuICByZXR1cm4gL0FuZHJvaWQvLnRlc3Qod2luZG93Lm5hdmlnYXRvci51c2VyQWdlbnQpO1xufVxuXG5mdW5jdGlvbiBtb2JpbGVTbmlmZigpIHtcbiAgcmV0dXJuIGlQaG9uZVNuaWZmKCkgfHwgYW5kcm9pZFNuaWZmKCk7XG59XG5cbn0oalF1ZXJ5KTtcbiIsIi8qIVxuICogV2F2ZXMgdjAuNi40XG4gKiBodHRwOi8vZmlhbi5teS5pZC9XYXZlc1xuICpcbiAqIENvcHlyaWdodCAyMDE0IEFsZmlhbmEgRS4gU2lidWVhIGFuZCBvdGhlciBjb250cmlidXRvcnNcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxuICogaHR0cHM6Ly9naXRodWIuY29tL2ZpYW5zL1dhdmVzL2Jsb2IvbWFzdGVyL0xJQ0VOU0VcbiAqL1xuXG47KGZ1bmN0aW9uKHdpbmRvdykge1xuICAgICd1c2Ugc3RyaWN0JztcblxuICAgIHZhciBXYXZlcyA9IFdhdmVzIHx8IHt9O1xuICAgIHZhciAkJCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwuYmluZChkb2N1bWVudCk7XG5cbiAgICAvLyBGaW5kIGV4YWN0IHBvc2l0aW9uIG9mIGVsZW1lbnRcbiAgICBmdW5jdGlvbiBpc1dpbmRvdyhvYmopIHtcbiAgICAgICAgcmV0dXJuIG9iaiAhPT0gbnVsbCAmJiBvYmogPT09IG9iai53aW5kb3c7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0V2luZG93KGVsZW0pIHtcbiAgICAgICAgcmV0dXJuIGlzV2luZG93KGVsZW0pID8gZWxlbSA6IGVsZW0ubm9kZVR5cGUgPT09IDkgJiYgZWxlbS5kZWZhdWx0VmlldztcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBvZmZzZXQoZWxlbSkge1xuICAgICAgICB2YXIgZG9jRWxlbSwgd2luLFxuICAgICAgICAgICAgYm94ID0ge3RvcDogMCwgbGVmdDogMH0sXG4gICAgICAgICAgICBkb2MgPSBlbGVtICYmIGVsZW0ub3duZXJEb2N1bWVudDtcblxuICAgICAgICBkb2NFbGVtID0gZG9jLmRvY3VtZW50RWxlbWVudDtcblxuICAgICAgICBpZiAodHlwZW9mIGVsZW0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0ICE9PSB0eXBlb2YgdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBib3ggPSBlbGVtLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuICAgICAgICB9XG4gICAgICAgIHdpbiA9IGdldFdpbmRvdyhkb2MpO1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgdG9wOiBib3gudG9wICsgd2luLnBhZ2VZT2Zmc2V0IC0gZG9jRWxlbS5jbGllbnRUb3AsXG4gICAgICAgICAgICBsZWZ0OiBib3gubGVmdCArIHdpbi5wYWdlWE9mZnNldCAtIGRvY0VsZW0uY2xpZW50TGVmdFxuICAgICAgICB9O1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNvbnZlcnRTdHlsZShvYmopIHtcbiAgICAgICAgdmFyIHN0eWxlID0gJyc7XG5cbiAgICAgICAgZm9yICh2YXIgYSBpbiBvYmopIHtcbiAgICAgICAgICAgIGlmIChvYmouaGFzT3duUHJvcGVydHkoYSkpIHtcbiAgICAgICAgICAgICAgICBzdHlsZSArPSAoYSArICc6JyArIG9ialthXSArICc7Jyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gc3R5bGU7XG4gICAgfVxuXG4gICAgdmFyIEVmZmVjdCA9IHtcblxuICAgICAgICAvLyBFZmZlY3QgZGVsYXlcbiAgICAgICAgZHVyYXRpb246IDc1MCxcblxuICAgICAgICBzaG93OiBmdW5jdGlvbihlLCBlbGVtZW50KSB7XG5cbiAgICAgICAgICAgIC8vIERpc2FibGUgcmlnaHQgY2xpY2tcbiAgICAgICAgICAgIGlmIChlLmJ1dHRvbiA9PT0gMikge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIGVsID0gZWxlbWVudCB8fCB0aGlzO1xuXG4gICAgICAgICAgICAvLyBDcmVhdGUgcmlwcGxlXG4gICAgICAgICAgICB2YXIgcmlwcGxlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgICAgICByaXBwbGUuY2xhc3NOYW1lID0gJ3dhdmVzLXJpcHBsZSc7XG4gICAgICAgICAgICBlbC5hcHBlbmRDaGlsZChyaXBwbGUpO1xuXG4gICAgICAgICAgICAvLyBHZXQgY2xpY2sgY29vcmRpbmF0ZSBhbmQgZWxlbWVudCB3aXRkaFxuICAgICAgICAgICAgdmFyIHBvcyAgICAgICAgID0gb2Zmc2V0KGVsKTtcbiAgICAgICAgICAgIHZhciByZWxhdGl2ZVkgICA9IChlLnBhZ2VZIC0gcG9zLnRvcCk7XG4gICAgICAgICAgICB2YXIgcmVsYXRpdmVYICAgPSAoZS5wYWdlWCAtIHBvcy5sZWZ0KTtcbiAgICAgICAgICAgIHZhciBzY2FsZSAgICAgICA9ICdzY2FsZSgnKygoZWwuY2xpZW50V2lkdGggLyAxMDApICogMTApKycpJztcblxuICAgICAgICAgICAgLy8gU3VwcG9ydCBmb3IgdG91Y2ggZGV2aWNlc1xuICAgICAgICAgICAgaWYgKCd0b3VjaGVzJyBpbiBlKSB7XG4gICAgICAgICAgICAgIHJlbGF0aXZlWSAgID0gKGUudG91Y2hlc1swXS5wYWdlWSAtIHBvcy50b3ApO1xuICAgICAgICAgICAgICByZWxhdGl2ZVggICA9IChlLnRvdWNoZXNbMF0ucGFnZVggLSBwb3MubGVmdCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIEF0dGFjaCBkYXRhIHRvIGVsZW1lbnRcbiAgICAgICAgICAgIHJpcHBsZS5zZXRBdHRyaWJ1dGUoJ2RhdGEtaG9sZCcsIERhdGUubm93KCkpO1xuICAgICAgICAgICAgcmlwcGxlLnNldEF0dHJpYnV0ZSgnZGF0YS1zY2FsZScsIHNjYWxlKTtcbiAgICAgICAgICAgIHJpcHBsZS5zZXRBdHRyaWJ1dGUoJ2RhdGEteCcsIHJlbGF0aXZlWCk7XG4gICAgICAgICAgICByaXBwbGUuc2V0QXR0cmlidXRlKCdkYXRhLXknLCByZWxhdGl2ZVkpO1xuXG4gICAgICAgICAgICAvLyBTZXQgcmlwcGxlIHBvc2l0aW9uXG4gICAgICAgICAgICB2YXIgcmlwcGxlU3R5bGUgPSB7XG4gICAgICAgICAgICAgICAgJ3RvcCc6IHJlbGF0aXZlWSsncHgnLFxuICAgICAgICAgICAgICAgICdsZWZ0JzogcmVsYXRpdmVYKydweCdcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIHJpcHBsZS5jbGFzc05hbWUgPSByaXBwbGUuY2xhc3NOYW1lICsgJyB3YXZlcy1ub3RyYW5zaXRpb24nO1xuICAgICAgICAgICAgcmlwcGxlLnNldEF0dHJpYnV0ZSgnc3R5bGUnLCBjb252ZXJ0U3R5bGUocmlwcGxlU3R5bGUpKTtcbiAgICAgICAgICAgIHJpcHBsZS5jbGFzc05hbWUgPSByaXBwbGUuY2xhc3NOYW1lLnJlcGxhY2UoJ3dhdmVzLW5vdHJhbnNpdGlvbicsICcnKTtcblxuICAgICAgICAgICAgLy8gU2NhbGUgdGhlIHJpcHBsZVxuICAgICAgICAgICAgcmlwcGxlU3R5bGVbJy13ZWJraXQtdHJhbnNmb3JtJ10gPSBzY2FsZTtcbiAgICAgICAgICAgIHJpcHBsZVN0eWxlWyctbW96LXRyYW5zZm9ybSddID0gc2NhbGU7XG4gICAgICAgICAgICByaXBwbGVTdHlsZVsnLW1zLXRyYW5zZm9ybSddID0gc2NhbGU7XG4gICAgICAgICAgICByaXBwbGVTdHlsZVsnLW8tdHJhbnNmb3JtJ10gPSBzY2FsZTtcbiAgICAgICAgICAgIHJpcHBsZVN0eWxlLnRyYW5zZm9ybSA9IHNjYWxlO1xuICAgICAgICAgICAgcmlwcGxlU3R5bGUub3BhY2l0eSAgID0gJzEnO1xuXG4gICAgICAgICAgICByaXBwbGVTdHlsZVsnLXdlYmtpdC10cmFuc2l0aW9uLWR1cmF0aW9uJ10gPSBFZmZlY3QuZHVyYXRpb24gKyAnbXMnO1xuICAgICAgICAgICAgcmlwcGxlU3R5bGVbJy1tb3otdHJhbnNpdGlvbi1kdXJhdGlvbiddICAgID0gRWZmZWN0LmR1cmF0aW9uICsgJ21zJztcbiAgICAgICAgICAgIHJpcHBsZVN0eWxlWyctby10cmFuc2l0aW9uLWR1cmF0aW9uJ10gICAgICA9IEVmZmVjdC5kdXJhdGlvbiArICdtcyc7XG4gICAgICAgICAgICByaXBwbGVTdHlsZVsndHJhbnNpdGlvbi1kdXJhdGlvbiddICAgICAgICAgPSBFZmZlY3QuZHVyYXRpb24gKyAnbXMnO1xuXG4gICAgICAgICAgICByaXBwbGVTdHlsZVsnLXdlYmtpdC10cmFuc2l0aW9uLXRpbWluZy1mdW5jdGlvbiddID0gJ2N1YmljLWJlemllcigwLjI1MCwgMC40NjAsIDAuNDUwLCAwLjk0MCknO1xuICAgICAgICAgICAgcmlwcGxlU3R5bGVbJy1tb3otdHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb24nXSAgICA9ICdjdWJpYy1iZXppZXIoMC4yNTAsIDAuNDYwLCAwLjQ1MCwgMC45NDApJztcbiAgICAgICAgICAgIHJpcHBsZVN0eWxlWyctby10cmFuc2l0aW9uLXRpbWluZy1mdW5jdGlvbiddICAgICAgPSAnY3ViaWMtYmV6aWVyKDAuMjUwLCAwLjQ2MCwgMC40NTAsIDAuOTQwKSc7XG4gICAgICAgICAgICByaXBwbGVTdHlsZVsndHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb24nXSAgICAgICAgID0gJ2N1YmljLWJlemllcigwLjI1MCwgMC40NjAsIDAuNDUwLCAwLjk0MCknO1xuXG4gICAgICAgICAgICByaXBwbGUuc2V0QXR0cmlidXRlKCdzdHlsZScsIGNvbnZlcnRTdHlsZShyaXBwbGVTdHlsZSkpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGhpZGU6IGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICAgIFRvdWNoSGFuZGxlci50b3VjaHVwKGUpO1xuXG4gICAgICAgICAgICB2YXIgZWwgPSB0aGlzO1xuICAgICAgICAgICAgdmFyIHdpZHRoID0gZWwuY2xpZW50V2lkdGggKiAxLjQ7XG5cbiAgICAgICAgICAgIC8vIEdldCBmaXJzdCByaXBwbGVcbiAgICAgICAgICAgIHZhciByaXBwbGUgPSBudWxsO1xuICAgICAgICAgICAgdmFyIHJpcHBsZXMgPSBlbC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCd3YXZlcy1yaXBwbGUnKTtcbiAgICAgICAgICAgIGlmIChyaXBwbGVzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICByaXBwbGUgPSByaXBwbGVzW3JpcHBsZXMubGVuZ3RoIC0gMV07XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIHJlbGF0aXZlWCAgID0gcmlwcGxlLmdldEF0dHJpYnV0ZSgnZGF0YS14Jyk7XG4gICAgICAgICAgICB2YXIgcmVsYXRpdmVZICAgPSByaXBwbGUuZ2V0QXR0cmlidXRlKCdkYXRhLXknKTtcbiAgICAgICAgICAgIHZhciBzY2FsZSAgICAgICA9IHJpcHBsZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtc2NhbGUnKTtcblxuICAgICAgICAgICAgLy8gR2V0IGRlbGF5IGJlZXR3ZWVuIG1vdXNlZG93biBhbmQgbW91c2UgbGVhdmVcbiAgICAgICAgICAgIHZhciBkaWZmID0gRGF0ZS5ub3coKSAtIE51bWJlcihyaXBwbGUuZ2V0QXR0cmlidXRlKCdkYXRhLWhvbGQnKSk7XG4gICAgICAgICAgICB2YXIgZGVsYXkgPSAzNTAgLSBkaWZmO1xuXG4gICAgICAgICAgICBpZiAoZGVsYXkgPCAwKSB7XG4gICAgICAgICAgICAgICAgZGVsYXkgPSAwO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBGYWRlIG91dCByaXBwbGUgYWZ0ZXIgZGVsYXlcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgdmFyIHN0eWxlID0ge1xuICAgICAgICAgICAgICAgICAgICAndG9wJzogcmVsYXRpdmVZKydweCcsXG4gICAgICAgICAgICAgICAgICAgICdsZWZ0JzogcmVsYXRpdmVYKydweCcsXG4gICAgICAgICAgICAgICAgICAgICdvcGFjaXR5JzogJzAnLFxuXG4gICAgICAgICAgICAgICAgICAgIC8vIER1cmF0aW9uXG4gICAgICAgICAgICAgICAgICAgICctd2Via2l0LXRyYW5zaXRpb24tZHVyYXRpb24nOiBFZmZlY3QuZHVyYXRpb24gKyAnbXMnLFxuICAgICAgICAgICAgICAgICAgICAnLW1vei10cmFuc2l0aW9uLWR1cmF0aW9uJzogRWZmZWN0LmR1cmF0aW9uICsgJ21zJyxcbiAgICAgICAgICAgICAgICAgICAgJy1vLXRyYW5zaXRpb24tZHVyYXRpb24nOiBFZmZlY3QuZHVyYXRpb24gKyAnbXMnLFxuICAgICAgICAgICAgICAgICAgICAndHJhbnNpdGlvbi1kdXJhdGlvbic6IEVmZmVjdC5kdXJhdGlvbiArICdtcycsXG4gICAgICAgICAgICAgICAgICAgICctd2Via2l0LXRyYW5zZm9ybSc6IHNjYWxlLFxuICAgICAgICAgICAgICAgICAgICAnLW1vei10cmFuc2Zvcm0nOiBzY2FsZSxcbiAgICAgICAgICAgICAgICAgICAgJy1tcy10cmFuc2Zvcm0nOiBzY2FsZSxcbiAgICAgICAgICAgICAgICAgICAgJy1vLXRyYW5zZm9ybSc6IHNjYWxlLFxuICAgICAgICAgICAgICAgICAgICAndHJhbnNmb3JtJzogc2NhbGUsXG4gICAgICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgICAgIHJpcHBsZS5zZXRBdHRyaWJ1dGUoJ3N0eWxlJywgY29udmVydFN0eWxlKHN0eWxlKSk7XG5cbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZWwucmVtb3ZlQ2hpbGQocmlwcGxlKTtcbiAgICAgICAgICAgICAgICAgICAgfSBjYXRjaChlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LCBFZmZlY3QuZHVyYXRpb24pO1xuICAgICAgICAgICAgfSwgZGVsYXkpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8vIExpdHRsZSBoYWNrIHRvIG1ha2UgPGlucHV0PiBjYW4gcGVyZm9ybSB3YXZlcyBlZmZlY3RcbiAgICAgICAgd3JhcElucHV0OiBmdW5jdGlvbihlbGVtZW50cykge1xuICAgICAgICAgICAgZm9yICh2YXIgYSA9IDA7IGEgPCBlbGVtZW50cy5sZW5ndGg7IGErKykge1xuICAgICAgICAgICAgICAgIHZhciBlbCA9IGVsZW1lbnRzW2FdO1xuXG4gICAgICAgICAgICAgICAgaWYgKGVsLnRhZ05hbWUudG9Mb3dlckNhc2UoKSA9PT0gJ2lucHV0Jykge1xuICAgICAgICAgICAgICAgICAgICB2YXIgcGFyZW50ID0gZWwucGFyZW50Tm9kZTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBJZiBpbnB1dCBhbHJlYWR5IGhhdmUgcGFyZW50IGp1c3QgcGFzcyB0aHJvdWdoXG4gICAgICAgICAgICAgICAgICAgIGlmIChwYXJlbnQudGFnTmFtZS50b0xvd2VyQ2FzZSgpID09PSAnaScgJiYgcGFyZW50LmNsYXNzTmFtZS5pbmRleE9mKCd3YXZlcy1lZmZlY3QnKSAhPT0gLTEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gUHV0IGVsZW1lbnQgY2xhc3MgYW5kIHN0eWxlIHRvIHRoZSBzcGVjaWZpZWQgcGFyZW50XG4gICAgICAgICAgICAgICAgICAgIHZhciB3cmFwcGVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaScpO1xuICAgICAgICAgICAgICAgICAgICB3cmFwcGVyLmNsYXNzTmFtZSA9IGVsLmNsYXNzTmFtZSArICcgd2F2ZXMtaW5wdXQtd3JhcHBlcic7XG5cbiAgICAgICAgICAgICAgICAgICAgdmFyIGVsZW1lbnRTdHlsZSA9IGVsLmdldEF0dHJpYnV0ZSgnc3R5bGUnKTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAoIWVsZW1lbnRTdHlsZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudFN0eWxlID0gJyc7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICB3cmFwcGVyLnNldEF0dHJpYnV0ZSgnc3R5bGUnLCBlbGVtZW50U3R5bGUpO1xuXG4gICAgICAgICAgICAgICAgICAgIGVsLmNsYXNzTmFtZSA9ICd3YXZlcy1idXR0b24taW5wdXQnO1xuICAgICAgICAgICAgICAgICAgICBlbC5yZW1vdmVBdHRyaWJ1dGUoJ3N0eWxlJyk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gUHV0IGVsZW1lbnQgYXMgY2hpbGRcbiAgICAgICAgICAgICAgICAgICAgcGFyZW50LnJlcGxhY2VDaGlsZCh3cmFwcGVyLCBlbCk7XG4gICAgICAgICAgICAgICAgICAgIHdyYXBwZXIuYXBwZW5kQ2hpbGQoZWwpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH07XG5cblxuICAgIC8qKlxuICAgICAqIERpc2FibGUgbW91c2Vkb3duIGV2ZW50IGZvciA1MDBtcyBkdXJpbmcgYW5kIGFmdGVyIHRvdWNoXG4gICAgICovXG4gICAgdmFyIFRvdWNoSGFuZGxlciA9IHtcbiAgICAgICAgLyogdXNlcyBhbiBpbnRlZ2VyIHJhdGhlciB0aGFuIGJvb2wgc28gdGhlcmUncyBubyBpc3N1ZXMgd2l0aFxuICAgICAgICAgKiBuZWVkaW5nIHRvIGNsZWFyIHRpbWVvdXRzIGlmIGFub3RoZXIgdG91Y2ggZXZlbnQgb2NjdXJyZWRcbiAgICAgICAgICogd2l0aGluIHRoZSA1MDBtcy4gQ2Fubm90IG1vdXNldXAgYmV0d2VlbiB0b3VjaHN0YXJ0IGFuZFxuICAgICAgICAgKiB0b3VjaGVuZCwgbm9yIGluIHRoZSA1MDBtcyBhZnRlciB0b3VjaGVuZC4gKi9cbiAgICAgICAgdG91Y2hlczogMCxcbiAgICAgICAgYWxsb3dFdmVudDogZnVuY3Rpb24oZSkge1xuICAgICAgICAgICAgdmFyIGFsbG93ID0gdHJ1ZTtcblxuICAgICAgICAgICAgaWYgKGUudHlwZSA9PT0gJ3RvdWNoc3RhcnQnKSB7XG4gICAgICAgICAgICAgICAgVG91Y2hIYW5kbGVyLnRvdWNoZXMgKz0gMTsgLy9wdXNoXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGUudHlwZSA9PT0gJ3RvdWNoZW5kJyB8fCBlLnR5cGUgPT09ICd0b3VjaGNhbmNlbCcpIHtcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoVG91Y2hIYW5kbGVyLnRvdWNoZXMgPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBUb3VjaEhhbmRsZXIudG91Y2hlcyAtPSAxOyAvL3BvcCBhZnRlciA1MDBtc1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSwgNTAwKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZS50eXBlID09PSAnbW91c2Vkb3duJyAmJiBUb3VjaEhhbmRsZXIudG91Y2hlcyA+IDApIHtcbiAgICAgICAgICAgICAgICBhbGxvdyA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gYWxsb3c7XG4gICAgICAgIH0sXG4gICAgICAgIHRvdWNodXA6IGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICAgIFRvdWNoSGFuZGxlci5hbGxvd0V2ZW50KGUpO1xuICAgICAgICB9XG4gICAgfTtcblxuXG4gICAgLyoqXG4gICAgICogRGVsZWdhdGVkIGNsaWNrIGhhbmRsZXIgZm9yIC53YXZlcy1lZmZlY3QgZWxlbWVudC5cbiAgICAgKiByZXR1cm5zIG51bGwgd2hlbiAud2F2ZXMtZWZmZWN0IGVsZW1lbnQgbm90IGluIFwiY2xpY2sgdHJlZVwiXG4gICAgICovXG4gICAgZnVuY3Rpb24gZ2V0V2F2ZXNFZmZlY3RFbGVtZW50KGUpIHtcbiAgICAgICAgaWYgKFRvdWNoSGFuZGxlci5hbGxvd0V2ZW50KGUpID09PSBmYWxzZSkge1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgZWxlbWVudCA9IG51bGw7XG4gICAgICAgIHZhciB0YXJnZXQgPSBlLnRhcmdldCB8fCBlLnNyY0VsZW1lbnQ7XG5cbiAgICAgICAgd2hpbGUgKHRhcmdldC5wYXJlbnRFbGVtZW50ICE9PSBudWxsKSB7XG4gICAgICAgICAgICBpZiAoISh0YXJnZXQgaW5zdGFuY2VvZiBTVkdFbGVtZW50KSAmJiB0YXJnZXQuY2xhc3NOYW1lLmluZGV4T2YoJ3dhdmVzLWVmZmVjdCcpICE9PSAtMSkge1xuICAgICAgICAgICAgICAgIGVsZW1lbnQgPSB0YXJnZXQ7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRhcmdldC5jbGFzc0xpc3QuY29udGFpbnMoJ3dhdmVzLWVmZmVjdCcpKSB7XG4gICAgICAgICAgICAgICAgZWxlbWVudCA9IHRhcmdldDtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRhcmdldCA9IHRhcmdldC5wYXJlbnRFbGVtZW50O1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGVsZW1lbnQ7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQnViYmxlIHRoZSBjbGljayBhbmQgc2hvdyBlZmZlY3QgaWYgLndhdmVzLWVmZmVjdCBlbGVtIHdhcyBmb3VuZFxuICAgICAqL1xuICAgIGZ1bmN0aW9uIHNob3dFZmZlY3QoZSkge1xuICAgICAgICB2YXIgZWxlbWVudCA9IGdldFdhdmVzRWZmZWN0RWxlbWVudChlKTtcblxuICAgICAgICBpZiAoZWxlbWVudCAhPT0gbnVsbCkge1xuICAgICAgICAgICAgRWZmZWN0LnNob3coZSwgZWxlbWVudCk7XG5cbiAgICAgICAgICAgIGlmICgnb250b3VjaHN0YXJ0JyBpbiB3aW5kb3cpIHtcbiAgICAgICAgICAgICAgICBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNoZW5kJywgRWZmZWN0LmhpZGUsIGZhbHNlKTtcbiAgICAgICAgICAgICAgICBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNoY2FuY2VsJywgRWZmZWN0LmhpZGUsIGZhbHNlKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZXVwJywgRWZmZWN0LmhpZGUsIGZhbHNlKTtcbiAgICAgICAgICAgIGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignbW91c2VsZWF2ZScsIEVmZmVjdC5oaWRlLCBmYWxzZSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBXYXZlcy5kaXNwbGF5RWZmZWN0ID0gZnVuY3Rpb24ob3B0aW9ucykge1xuICAgICAgICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcblxuICAgICAgICBpZiAoJ2R1cmF0aW9uJyBpbiBvcHRpb25zKSB7XG4gICAgICAgICAgICBFZmZlY3QuZHVyYXRpb24gPSBvcHRpb25zLmR1cmF0aW9uO1xuICAgICAgICB9XG5cbiAgICAgICAgLy9XcmFwIGlucHV0IGluc2lkZSA8aT4gdGFnXG4gICAgICAgIEVmZmVjdC53cmFwSW5wdXQoJCQoJy53YXZlcy1lZmZlY3QnKSk7XG5cbiAgICAgICAgaWYgKCdvbnRvdWNoc3RhcnQnIGluIHdpbmRvdykge1xuICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5hZGRFdmVudExpc3RlbmVyKCd0b3VjaHN0YXJ0Jywgc2hvd0VmZmVjdCwgZmFsc2UpO1xuICAgICAgICB9XG5cbiAgICAgICAgZG9jdW1lbnQuYm9keS5hZGRFdmVudExpc3RlbmVyKCdtb3VzZWRvd24nLCBzaG93RWZmZWN0LCBmYWxzZSk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEF0dGFjaCBXYXZlcyB0byBhbiBpbnB1dCBlbGVtZW50IChvciBhbnkgZWxlbWVudCB3aGljaCBkb2Vzbid0XG4gICAgICogYnViYmxlIG1vdXNldXAvbW91c2Vkb3duIGV2ZW50cykuXG4gICAgICogICBJbnRlbmRlZCB0byBiZSB1c2VkIHdpdGggZHluYW1pY2FsbHkgbG9hZGVkIGZvcm1zL2lucHV0cywgb3JcbiAgICAgKiB3aGVyZSB0aGUgdXNlciBkb2Vzbid0IHdhbnQgYSBkZWxlZ2F0ZWQgY2xpY2sgaGFuZGxlci5cbiAgICAgKi9cbiAgICBXYXZlcy5hdHRhY2ggPSBmdW5jdGlvbihlbGVtZW50KSB7XG4gICAgICAgIC8vRlVUVVJFOiBhdXRvbWF0aWNhbGx5IGFkZCB3YXZlcyBjbGFzc2VzIGFuZCBhbGxvdyB1c2Vyc1xuICAgICAgICAvLyB0byBzcGVjaWZ5IHRoZW0gd2l0aCBhbiBvcHRpb25zIHBhcmFtPyBFZy4gbGlnaHQvY2xhc3NpYy9idXR0b25cbiAgICAgICAgaWYgKGVsZW1lbnQudGFnTmFtZS50b0xvd2VyQ2FzZSgpID09PSAnaW5wdXQnKSB7XG4gICAgICAgICAgICBFZmZlY3Qud3JhcElucHV0KFtlbGVtZW50XSk7XG4gICAgICAgICAgICBlbGVtZW50ID0gZWxlbWVudC5wYXJlbnRFbGVtZW50O1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCdvbnRvdWNoc3RhcnQnIGluIHdpbmRvdykge1xuICAgICAgICAgICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCd0b3VjaHN0YXJ0Jywgc2hvd0VmZmVjdCwgZmFsc2UpO1xuICAgICAgICB9XG5cbiAgICAgICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZWRvd24nLCBzaG93RWZmZWN0LCBmYWxzZSk7XG4gICAgfTtcblxuICAgIHdpbmRvdy5XYXZlcyA9IFdhdmVzO1xuXG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIGZ1bmN0aW9uKCkge1xuICAgICAgICBXYXZlcy5kaXNwbGF5RWZmZWN0KCk7XG4gICAgfSwgZmFsc2UpO1xuXG59KSh3aW5kb3cpO1xuIiwiIWZ1bmN0aW9uKCl7XCJ1c2Ugc3RyaWN0XCI7Zm9yKHZhciB0PWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIuc3dpdGNoXCIpLGU9MDtlPHQubGVuZ3RoOysrZSkhZnVuY3Rpb24oKXt2YXIgaT10W2VdO3RbZV0uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsZnVuY3Rpb24odCl7dC5wcmV2ZW50RGVmYXVsdCgpLGkuY2xhc3NMaXN0LnRvZ2dsZShcImlzLWFjdGl2ZVwiKSxpLmNsYXNzTGlzdC5hZGQoXCJpcy1sb2FkaW5nXCIpLHdpbmRvdy5sb2NhdGlvbj1pLnF1ZXJ5U2VsZWN0b3IoXCJhLm5vdC1hY3RpdmVcIikuZ2V0QXR0cmlidXRlKFwiaHJlZlwiKX0pfSgpfSgpOyIsIiFmdW5jdGlvbih0LGUpe1wib2JqZWN0XCI9PXR5cGVvZiBleHBvcnRzJiZcInVuZGVmaW5lZFwiIT10eXBlb2YgbW9kdWxlP21vZHVsZS5leHBvcnRzPWUocmVxdWlyZShcImpxdWVyeVwiKSk6XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShbXCJqcXVlcnlcIl0sZSk6dC5wYXJzbGV5PWUodC5qUXVlcnkpfSh0aGlzLGZ1bmN0aW9uKGgpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIHIodCl7cmV0dXJuKHI9XCJmdW5jdGlvblwiPT10eXBlb2YgU3ltYm9sJiZcInN5bWJvbFwiPT10eXBlb2YgU3ltYm9sLml0ZXJhdG9yP2Z1bmN0aW9uKHQpe3JldHVybiB0eXBlb2YgdH06ZnVuY3Rpb24odCl7cmV0dXJuIHQmJlwiZnVuY3Rpb25cIj09dHlwZW9mIFN5bWJvbCYmdC5jb25zdHJ1Y3Rvcj09PVN5bWJvbCYmdCE9PVN5bWJvbC5wcm90b3R5cGU/XCJzeW1ib2xcIjp0eXBlb2YgdH0pKHQpfWZ1bmN0aW9uIG8oKXtyZXR1cm4obz1PYmplY3QuYXNzaWdufHxmdW5jdGlvbih0KXtmb3IodmFyIGU9MTtlPGFyZ3VtZW50cy5sZW5ndGg7ZSsrKXt2YXIgaT1hcmd1bWVudHNbZV07Zm9yKHZhciBuIGluIGkpT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKGksbikmJih0W25dPWlbbl0pfXJldHVybiB0fSkuYXBwbHkodGhpcyxhcmd1bWVudHMpfWZ1bmN0aW9uIGwodCxlKXtyZXR1cm4gZnVuY3Rpb24odCl7aWYoQXJyYXkuaXNBcnJheSh0KSlyZXR1cm4gdH0odCl8fGZ1bmN0aW9uKHQsZSl7dmFyIGk9W10sbj0hMCxyPSExLHM9dm9pZCAwO3RyeXtmb3IodmFyIGEsbz10W1N5bWJvbC5pdGVyYXRvcl0oKTshKG49KGE9by5uZXh0KCkpLmRvbmUpJiYoaS5wdXNoKGEudmFsdWUpLCFlfHxpLmxlbmd0aCE9PWUpO249ITApO31jYXRjaCh0KXtyPSEwLHM9dH1maW5hbGx5e3RyeXtufHxudWxsPT1vLnJldHVybnx8by5yZXR1cm4oKX1maW5hbGx5e2lmKHIpdGhyb3cgc319cmV0dXJuIGl9KHQsZSl8fGZ1bmN0aW9uKCl7dGhyb3cgbmV3IFR5cGVFcnJvcihcIkludmFsaWQgYXR0ZW1wdCB0byBkZXN0cnVjdHVyZSBub24taXRlcmFibGUgaW5zdGFuY2VcIil9KCl9ZnVuY3Rpb24gdSh0KXtyZXR1cm4gZnVuY3Rpb24odCl7aWYoQXJyYXkuaXNBcnJheSh0KSl7Zm9yKHZhciBlPTAsaT1uZXcgQXJyYXkodC5sZW5ndGgpO2U8dC5sZW5ndGg7ZSsrKWlbZV09dFtlXTtyZXR1cm4gaX19KHQpfHxmdW5jdGlvbih0KXtpZihTeW1ib2wuaXRlcmF0b3IgaW4gT2JqZWN0KHQpfHxcIltvYmplY3QgQXJndW1lbnRzXVwiPT09T2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKHQpKXJldHVybiBBcnJheS5mcm9tKHQpfSh0KXx8ZnVuY3Rpb24oKXt0aHJvdyBuZXcgVHlwZUVycm9yKFwiSW52YWxpZCBhdHRlbXB0IHRvIHNwcmVhZCBub24taXRlcmFibGUgaW5zdGFuY2VcIil9KCl9dmFyIGksdD0xLGU9e30sZD17YXR0cjpmdW5jdGlvbih0LGUsaSl7dmFyIG4scixzLGE9bmV3IFJlZ0V4cChcIl5cIitlLFwiaVwiKTtpZih2b2lkIDA9PT1pKWk9e307ZWxzZSBmb3IobiBpbiBpKWkuaGFzT3duUHJvcGVydHkobikmJmRlbGV0ZSBpW25dO2lmKCF0KXJldHVybiBpO2ZvcihuPShzPXQuYXR0cmlidXRlcykubGVuZ3RoO24tLTspKHI9c1tuXSkmJnIuc3BlY2lmaWVkJiZhLnRlc3Qoci5uYW1lKSYmKGlbdGhpcy5jYW1lbGl6ZShyLm5hbWUuc2xpY2UoZS5sZW5ndGgpKV09dGhpcy5kZXNlcmlhbGl6ZVZhbHVlKHIudmFsdWUpKTtyZXR1cm4gaX0sY2hlY2tBdHRyOmZ1bmN0aW9uKHQsZSxpKXtyZXR1cm4gdC5oYXNBdHRyaWJ1dGUoZStpKX0sc2V0QXR0cjpmdW5jdGlvbih0LGUsaSxuKXt0LnNldEF0dHJpYnV0ZSh0aGlzLmRhc2hlcml6ZShlK2kpLFN0cmluZyhuKSl9LGdldFR5cGU6ZnVuY3Rpb24odCl7cmV0dXJuIHQuZ2V0QXR0cmlidXRlKFwidHlwZVwiKXx8XCJ0ZXh0XCJ9LGdlbmVyYXRlSUQ6ZnVuY3Rpb24oKXtyZXR1cm5cIlwiK3QrK30sZGVzZXJpYWxpemVWYWx1ZTpmdW5jdGlvbihlKXt2YXIgdDt0cnl7cmV0dXJuIGU/XCJ0cnVlXCI9PWV8fFwiZmFsc2VcIiE9ZSYmKFwibnVsbFwiPT1lP251bGw6aXNOYU4odD1OdW1iZXIoZSkpPy9eW1xcW1xce10vLnRlc3QoZSk/SlNPTi5wYXJzZShlKTplOnQpOmV9Y2F0Y2godCl7cmV0dXJuIGV9fSxjYW1lbGl6ZTpmdW5jdGlvbih0KXtyZXR1cm4gdC5yZXBsYWNlKC8tKyguKT8vZyxmdW5jdGlvbih0LGUpe3JldHVybiBlP2UudG9VcHBlckNhc2UoKTpcIlwifSl9LGRhc2hlcml6ZTpmdW5jdGlvbih0KXtyZXR1cm4gdC5yZXBsYWNlKC86Oi9nLFwiL1wiKS5yZXBsYWNlKC8oW0EtWl0rKShbQS1aXVthLXpdKS9nLFwiJDFfJDJcIikucmVwbGFjZSgvKFthLXpcXGRdKShbQS1aXSkvZyxcIiQxXyQyXCIpLnJlcGxhY2UoL18vZyxcIi1cIikudG9Mb3dlckNhc2UoKX0sd2FybjpmdW5jdGlvbigpe3ZhciB0O3dpbmRvdy5jb25zb2xlJiZcImZ1bmN0aW9uXCI9PXR5cGVvZiB3aW5kb3cuY29uc29sZS53YXJuJiYodD13aW5kb3cuY29uc29sZSkud2Fybi5hcHBseSh0LGFyZ3VtZW50cyl9LHdhcm5PbmNlOmZ1bmN0aW9uKHQpe2VbdF18fChlW3RdPSEwLHRoaXMud2Fybi5hcHBseSh0aGlzLGFyZ3VtZW50cykpfSxfcmVzZXRXYXJuaW5nczpmdW5jdGlvbigpe2U9e319LHRyaW1TdHJpbmc6ZnVuY3Rpb24odCl7cmV0dXJuIHQucmVwbGFjZSgvXlxccyt8XFxzKyQvZyxcIlwiKX0scGFyc2U6e2RhdGU6ZnVuY3Rpb24odCl7dmFyIGU9dC5tYXRjaCgvXihcXGR7NCx9KS0oXFxkXFxkKS0oXFxkXFxkKSQvKTtpZighZSlyZXR1cm4gbnVsbDt2YXIgaT1sKGUubWFwKGZ1bmN0aW9uKHQpe3JldHVybiBwYXJzZUludCh0LDEwKX0pLDQpLG49KGlbMF0saVsxXSkscj1pWzJdLHM9aVszXSxhPW5ldyBEYXRlKG4sci0xLHMpO3JldHVybiBhLmdldEZ1bGxZZWFyKCkhPT1ufHxhLmdldE1vbnRoKCkrMSE9PXJ8fGEuZ2V0RGF0ZSgpIT09cz9udWxsOmF9LHN0cmluZzpmdW5jdGlvbih0KXtyZXR1cm4gdH0saW50ZWdlcjpmdW5jdGlvbih0KXtyZXR1cm4gaXNOYU4odCk/bnVsbDpwYXJzZUludCh0LDEwKX0sbnVtYmVyOmZ1bmN0aW9uKHQpe2lmKGlzTmFOKHQpKXRocm93IG51bGw7cmV0dXJuIHBhcnNlRmxvYXQodCl9LGJvb2xlYW46ZnVuY3Rpb24odCl7cmV0dXJuIS9eXFxzKmZhbHNlXFxzKiQvaS50ZXN0KHQpfSxvYmplY3Q6ZnVuY3Rpb24odCl7cmV0dXJuIGQuZGVzZXJpYWxpemVWYWx1ZSh0KX0scmVnZXhwOmZ1bmN0aW9uKHQpe3ZhciBlPVwiXCI7cmV0dXJuIHQ9L15cXC8uKlxcLyg/OltnaW15XSopJC8udGVzdCh0KT8oZT10LnJlcGxhY2UoLy4qXFwvKFtnaW15XSopJC8sXCIkMVwiKSx0LnJlcGxhY2UobmV3IFJlZ0V4cChcIl4vKC4qPykvXCIrZStcIiRcIiksXCIkMVwiKSk6XCJeXCIrdCtcIiRcIixuZXcgUmVnRXhwKHQsZSl9fSxwYXJzZVJlcXVpcmVtZW50OmZ1bmN0aW9uKHQsZSl7dmFyIGk9dGhpcy5wYXJzZVt0fHxcInN0cmluZ1wiXTtpZighaSl0aHJvdydVbmtub3duIHJlcXVpcmVtZW50IHNwZWNpZmljYXRpb246IFwiJyt0KydcIic7dmFyIG49aShlKTtpZihudWxsPT09bil0aHJvd1wiUmVxdWlyZW1lbnQgaXMgbm90IGEgXCIuY29uY2F0KHQsJzogXCInKS5jb25jYXQoZSwnXCInKTtyZXR1cm4gbn0sbmFtZXNwYWNlRXZlbnRzOmZ1bmN0aW9uKHQsZSl7cmV0dXJuKHQ9dGhpcy50cmltU3RyaW5nKHR8fFwiXCIpLnNwbGl0KC9cXHMrLykpWzBdP2gubWFwKHQsZnVuY3Rpb24odCl7cmV0dXJuXCJcIi5jb25jYXQodCxcIi5cIikuY29uY2F0KGUpfSkuam9pbihcIiBcIik6XCJcIn0sZGlmZmVyZW5jZTpmdW5jdGlvbih0LGkpe3ZhciBuPVtdO3JldHVybiBoLmVhY2godCxmdW5jdGlvbih0LGUpey0xPT1pLmluZGV4T2YoZSkmJm4ucHVzaChlKX0pLG59LGFsbDpmdW5jdGlvbih0KXtyZXR1cm4gaC53aGVuLmFwcGx5KGgsdSh0KS5jb25jYXQoWzQyLDQyXSkpfSxvYmplY3RDcmVhdGU6T2JqZWN0LmNyZWF0ZXx8KGk9ZnVuY3Rpb24oKXt9LGZ1bmN0aW9uKHQpe2lmKDE8YXJndW1lbnRzLmxlbmd0aCl0aHJvdyBFcnJvcihcIlNlY29uZCBhcmd1bWVudCBub3Qgc3VwcG9ydGVkXCIpO2lmKFwib2JqZWN0XCIhPXIodCkpdGhyb3cgVHlwZUVycm9yKFwiQXJndW1lbnQgbXVzdCBiZSBhbiBvYmplY3RcIik7aS5wcm90b3R5cGU9dDt2YXIgZT1uZXcgaTtyZXR1cm4gaS5wcm90b3R5cGU9bnVsbCxlfSksX1N1Ym1pdFNlbGVjdG9yOidpbnB1dFt0eXBlPVwic3VibWl0XCJdLCBidXR0b246c3VibWl0J30sbj17bmFtZXNwYWNlOlwiZGF0YS1wYXJzbGV5LVwiLGlucHV0czpcImlucHV0LCB0ZXh0YXJlYSwgc2VsZWN0XCIsZXhjbHVkZWQ6XCJpbnB1dFt0eXBlPWJ1dHRvbl0sIGlucHV0W3R5cGU9c3VibWl0XSwgaW5wdXRbdHlwZT1yZXNldF0sIGlucHV0W3R5cGU9aGlkZGVuXVwiLHByaW9yaXR5RW5hYmxlZDohMCxtdWx0aXBsZTpudWxsLGdyb3VwOm51bGwsdWlFbmFibGVkOiEwLHZhbGlkYXRpb25UaHJlc2hvbGQ6Myxmb2N1czpcImZpcnN0XCIsdHJpZ2dlcjohMSx0cmlnZ2VyQWZ0ZXJGYWlsdXJlOlwiaW5wdXRcIixlcnJvckNsYXNzOlwicGFyc2xleS1lcnJvclwiLHN1Y2Nlc3NDbGFzczpcInBhcnNsZXktc3VjY2Vzc1wiLGNsYXNzSGFuZGxlcjpmdW5jdGlvbih0KXt9LGVycm9yc0NvbnRhaW5lcjpmdW5jdGlvbih0KXt9LGVycm9yc1dyYXBwZXI6Jzx1bCBjbGFzcz1cInBhcnNsZXktZXJyb3JzLWxpc3RcIj48L3VsPicsZXJyb3JUZW1wbGF0ZTpcIjxsaT48L2xpPlwifSxzPWZ1bmN0aW9uKCl7dGhpcy5fX2lkX189ZC5nZW5lcmF0ZUlEKCl9O3MucHJvdG90eXBlPXthc3luY1N1cHBvcnQ6ITAsX3BpcGVBY2NvcmRpbmdUb1ZhbGlkYXRpb25SZXN1bHQ6ZnVuY3Rpb24oKXt2YXIgZT10aGlzLHQ9ZnVuY3Rpb24oKXt2YXIgdD1oLkRlZmVycmVkKCk7cmV0dXJuITAhPT1lLnZhbGlkYXRpb25SZXN1bHQmJnQucmVqZWN0KCksdC5yZXNvbHZlKCkucHJvbWlzZSgpfTtyZXR1cm5bdCx0XX0sYWN0dWFsaXplT3B0aW9uczpmdW5jdGlvbigpe3JldHVybiBkLmF0dHIodGhpcy5lbGVtZW50LHRoaXMub3B0aW9ucy5uYW1lc3BhY2UsdGhpcy5kb21PcHRpb25zKSx0aGlzLnBhcmVudCYmdGhpcy5wYXJlbnQuYWN0dWFsaXplT3B0aW9ucyYmdGhpcy5wYXJlbnQuYWN0dWFsaXplT3B0aW9ucygpLHRoaXN9LF9yZXNldE9wdGlvbnM6ZnVuY3Rpb24odCl7Zm9yKHZhciBlIGluIHRoaXMuZG9tT3B0aW9ucz1kLm9iamVjdENyZWF0ZSh0aGlzLnBhcmVudC5vcHRpb25zKSx0aGlzLm9wdGlvbnM9ZC5vYmplY3RDcmVhdGUodGhpcy5kb21PcHRpb25zKSx0KXQuaGFzT3duUHJvcGVydHkoZSkmJih0aGlzLm9wdGlvbnNbZV09dFtlXSk7dGhpcy5hY3R1YWxpemVPcHRpb25zKCl9LF9saXN0ZW5lcnM6bnVsbCxvbjpmdW5jdGlvbih0LGUpe3JldHVybiB0aGlzLl9saXN0ZW5lcnM9dGhpcy5fbGlzdGVuZXJzfHx7fSwodGhpcy5fbGlzdGVuZXJzW3RdPXRoaXMuX2xpc3RlbmVyc1t0XXx8W10pLnB1c2goZSksdGhpc30sc3Vic2NyaWJlOmZ1bmN0aW9uKHQsZSl7aC5saXN0ZW5Ubyh0aGlzLHQudG9Mb3dlckNhc2UoKSxlKX0sb2ZmOmZ1bmN0aW9uKHQsZSl7dmFyIGk9dGhpcy5fbGlzdGVuZXJzJiZ0aGlzLl9saXN0ZW5lcnNbdF07aWYoaSlpZihlKWZvcih2YXIgbj1pLmxlbmd0aDtuLS07KWlbbl09PT1lJiZpLnNwbGljZShuLDEpO2Vsc2UgZGVsZXRlIHRoaXMuX2xpc3RlbmVyc1t0XTtyZXR1cm4gdGhpc30sdW5zdWJzY3JpYmU6ZnVuY3Rpb24odCxlKXtoLnVuc3Vic2NyaWJlVG8odGhpcyx0LnRvTG93ZXJDYXNlKCkpfSx0cmlnZ2VyOmZ1bmN0aW9uKHQsZSxpKXtlPWV8fHRoaXM7dmFyIG4scj10aGlzLl9saXN0ZW5lcnMmJnRoaXMuX2xpc3RlbmVyc1t0XTtpZihyKWZvcih2YXIgcz1yLmxlbmd0aDtzLS07KWlmKCExPT09KG49cltzXS5jYWxsKGUsZSxpKSkpcmV0dXJuIG47cmV0dXJuIXRoaXMucGFyZW50fHx0aGlzLnBhcmVudC50cmlnZ2VyKHQsZSxpKX0sYXN5bmNJc1ZhbGlkOmZ1bmN0aW9uKHQsZSl7cmV0dXJuIGQud2Fybk9uY2UoXCJhc3luY0lzVmFsaWQgaXMgZGVwcmVjYXRlZDsgcGxlYXNlIHVzZSB3aGVuVmFsaWQgaW5zdGVhZFwiKSx0aGlzLndoZW5WYWxpZCh7Z3JvdXA6dCxmb3JjZTplfSl9LF9maW5kUmVsYXRlZDpmdW5jdGlvbigpe3JldHVybiB0aGlzLm9wdGlvbnMubXVsdGlwbGU/aCh0aGlzLnBhcmVudC5lbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCJbXCIuY29uY2F0KHRoaXMub3B0aW9ucy5uYW1lc3BhY2UsJ211bHRpcGxlPVwiJykuY29uY2F0KHRoaXMub3B0aW9ucy5tdWx0aXBsZSwnXCJdJykpKTp0aGlzLiRlbGVtZW50fX07dmFyIGM9ZnVuY3Rpb24odCl7aC5leHRlbmQoITAsdGhpcyx0KX07Yy5wcm90b3R5cGU9e3ZhbGlkYXRlOmZ1bmN0aW9uKHQsZSl7aWYodGhpcy5mbilyZXR1cm4gMzxhcmd1bWVudHMubGVuZ3RoJiYoZT1bXS5zbGljZS5jYWxsKGFyZ3VtZW50cywxLC0xKSksdGhpcy5mbih0LGUpO2lmKEFycmF5LmlzQXJyYXkodCkpe2lmKCF0aGlzLnZhbGlkYXRlTXVsdGlwbGUpdGhyb3dcIlZhbGlkYXRvciBgXCIrdGhpcy5uYW1lK1wiYCBkb2VzIG5vdCBoYW5kbGUgbXVsdGlwbGUgdmFsdWVzXCI7cmV0dXJuIHRoaXMudmFsaWRhdGVNdWx0aXBsZS5hcHBseSh0aGlzLGFyZ3VtZW50cyl9dmFyIGk9YXJndW1lbnRzW2FyZ3VtZW50cy5sZW5ndGgtMV07aWYodGhpcy52YWxpZGF0ZURhdGUmJmkuX2lzRGF0ZUlucHV0KCkpcmV0dXJuIG51bGwhPT0odD1kLnBhcnNlLmRhdGUodCkpJiZ0aGlzLnZhbGlkYXRlRGF0ZS5hcHBseSh0aGlzLGFyZ3VtZW50cyk7aWYodGhpcy52YWxpZGF0ZU51bWJlcilyZXR1cm4haXNOYU4odCkmJih0PXBhcnNlRmxvYXQodCksdGhpcy52YWxpZGF0ZU51bWJlci5hcHBseSh0aGlzLGFyZ3VtZW50cykpO2lmKHRoaXMudmFsaWRhdGVTdHJpbmcpcmV0dXJuIHRoaXMudmFsaWRhdGVTdHJpbmcuYXBwbHkodGhpcyxhcmd1bWVudHMpO3Rocm93XCJWYWxpZGF0b3IgYFwiK3RoaXMubmFtZStcImAgb25seSBoYW5kbGVzIG11bHRpcGxlIHZhbHVlc1wifSxwYXJzZVJlcXVpcmVtZW50czpmdW5jdGlvbih0LGUpe2lmKFwic3RyaW5nXCIhPXR5cGVvZiB0KXJldHVybiBBcnJheS5pc0FycmF5KHQpP3Q6W3RdO3ZhciBpPXRoaXMucmVxdWlyZW1lbnRUeXBlO2lmKEFycmF5LmlzQXJyYXkoaSkpe2Zvcih2YXIgbj1mdW5jdGlvbih0LGUpe3ZhciBpPXQubWF0Y2goL15cXHMqXFxbKC4qKVxcXVxccyokLyk7aWYoIWkpdGhyb3cnUmVxdWlyZW1lbnQgaXMgbm90IGFuIGFycmF5OiBcIicrdCsnXCInO3ZhciBuPWlbMV0uc3BsaXQoXCIsXCIpLm1hcChkLnRyaW1TdHJpbmcpO2lmKG4ubGVuZ3RoIT09ZSl0aHJvd1wiUmVxdWlyZW1lbnQgaGFzIFwiK24ubGVuZ3RoK1wiIHZhbHVlcyB3aGVuIFwiK2UrXCIgYXJlIG5lZWRlZFwiO3JldHVybiBufSh0LGkubGVuZ3RoKSxyPTA7cjxuLmxlbmd0aDtyKyspbltyXT1kLnBhcnNlUmVxdWlyZW1lbnQoaVtyXSxuW3JdKTtyZXR1cm4gbn1yZXR1cm4gaC5pc1BsYWluT2JqZWN0KGkpP2Z1bmN0aW9uKHQsZSxpKXt2YXIgbj1udWxsLHI9e307Zm9yKHZhciBzIGluIHQpaWYocyl7dmFyIGE9aShzKTtcInN0cmluZ1wiPT10eXBlb2YgYSYmKGE9ZC5wYXJzZVJlcXVpcmVtZW50KHRbc10sYSkpLHJbc109YX1lbHNlIG49ZC5wYXJzZVJlcXVpcmVtZW50KHRbc10sZSk7cmV0dXJuW24scl19KGksdCxlKTpbZC5wYXJzZVJlcXVpcmVtZW50KGksdCldfSxyZXF1aXJlbWVudFR5cGU6XCJzdHJpbmdcIixwcmlvcml0eToyfTt2YXIgYT1mdW5jdGlvbih0LGUpe3RoaXMuX19jbGFzc19fPVwiVmFsaWRhdG9yUmVnaXN0cnlcIix0aGlzLmxvY2FsZT1cImVuXCIsdGhpcy5pbml0KHR8fHt9LGV8fHt9KX0scD17ZW1haWw6L14oKChbYS16QS1aXXxcXGR8WyEjXFwkJSYnXFwqXFwrXFwtXFwvPVxcP1xcXl9ge1xcfH1+XXxbXFx1MDBBMC1cXHVEN0ZGXFx1RjkwMC1cXHVGRENGXFx1RkRGMC1cXHVGRkVGXSkrKFxcLihbYS16QS1aXXxcXGR8WyEjXFwkJSYnXFwqXFwrXFwtXFwvPVxcP1xcXl9ge1xcfH1+XXxbXFx1MDBBMC1cXHVEN0ZGXFx1RjkwMC1cXHVGRENGXFx1RkRGMC1cXHVGRkVGXSkrKSopfCgoXFx4MjIpKCgoKFxceDIwfFxceDA5KSooXFx4MGRcXHgwYSkpPyhcXHgyMHxcXHgwOSkrKT8oKFtcXHgwMS1cXHgwOFxceDBiXFx4MGNcXHgwZS1cXHgxZlxceDdmXXxcXHgyMXxbXFx4MjMtXFx4NWJdfFtcXHg1ZC1cXHg3ZV18W1xcdTAwQTAtXFx1RDdGRlxcdUY5MDAtXFx1RkRDRlxcdUZERjAtXFx1RkZFRl0pfChcXFxcKFtcXHgwMS1cXHgwOVxceDBiXFx4MGNcXHgwZC1cXHg3Zl18W1xcdTAwQTAtXFx1RDdGRlxcdUY5MDAtXFx1RkRDRlxcdUZERjAtXFx1RkZFRl0pKSkpKigoKFxceDIwfFxceDA5KSooXFx4MGRcXHgwYSkpPyhcXHgyMHxcXHgwOSkrKT8oXFx4MjIpKSlAKCgoW2EtekEtWl18XFxkfFtcXHUwMEEwLVxcdUQ3RkZcXHVGOTAwLVxcdUZEQ0ZcXHVGREYwLVxcdUZGRUZdKXwoKFthLXpBLVpdfFxcZHxbXFx1MDBBMC1cXHVEN0ZGXFx1RjkwMC1cXHVGRENGXFx1RkRGMC1cXHVGRkVGXSkoW2EtekEtWl18XFxkfC18X3x+fFtcXHUwMEEwLVxcdUQ3RkZcXHVGOTAwLVxcdUZEQ0ZcXHVGREYwLVxcdUZGRUZdKSooW2EtekEtWl18XFxkfFtcXHUwMEEwLVxcdUQ3RkZcXHVGOTAwLVxcdUZEQ0ZcXHVGREYwLVxcdUZGRUZdKSkpXFwuKSsoKFthLXpBLVpdfFtcXHUwMEEwLVxcdUQ3RkZcXHVGOTAwLVxcdUZEQ0ZcXHVGREYwLVxcdUZGRUZdKXwoKFthLXpBLVpdfFtcXHUwMEEwLVxcdUQ3RkZcXHVGOTAwLVxcdUZEQ0ZcXHVGREYwLVxcdUZGRUZdKShbYS16QS1aXXxcXGR8LXxffH58W1xcdTAwQTAtXFx1RDdGRlxcdUY5MDAtXFx1RkRDRlxcdUZERjAtXFx1RkZFRl0pKihbYS16QS1aXXxbXFx1MDBBMC1cXHVEN0ZGXFx1RjkwMC1cXHVGRENGXFx1RkRGMC1cXHVGRkVGXSkpKSQvLG51bWJlcjovXi0/KFxcZCpcXC4pP1xcZCsoZVstK10/XFxkKyk/JC9pLGludGVnZXI6L14tP1xcZCskLyxkaWdpdHM6L15cXGQrJC8sYWxwaGFudW06L15cXHcrJC9pLGRhdGU6e3Rlc3Q6ZnVuY3Rpb24odCl7cmV0dXJuIG51bGwhPT1kLnBhcnNlLmRhdGUodCl9fSx1cmw6bmV3IFJlZ0V4cChcIl4oPzooPzpodHRwcz98ZnRwKTovLyk/KD86XFxcXFMrKD86OlxcXFxTKik/QCk/KD86KD86WzEtOV1cXFxcZD98MVxcXFxkXFxcXGR8MlswMV1cXFxcZHwyMlswLTNdKSg/OlxcXFwuKD86MT9cXFxcZHsxLDJ9fDJbMC00XVxcXFxkfDI1WzAtNV0pKXsyfSg/OlxcXFwuKD86WzEtOV1cXFxcZD98MVxcXFxkXFxcXGR8MlswLTRdXFxcXGR8MjVbMC00XSkpfCg/Oig/OlthLXpBLVpcXFxcdTAwYTEtXFxcXHVmZmZmMC05XS0qKSpbYS16QS1aXFxcXHUwMGExLVxcXFx1ZmZmZjAtOV0rKSg/OlxcXFwuKD86W2EtekEtWlxcXFx1MDBhMS1cXFxcdWZmZmYwLTldLSopKlthLXpBLVpcXFxcdTAwYTEtXFxcXHVmZmZmMC05XSspKig/OlxcXFwuKD86W2EtekEtWlxcXFx1MDBhMS1cXFxcdWZmZmZdezIsfSkpKSg/OjpcXFxcZHsyLDV9KT8oPzovXFxcXFMqKT8kXCIpfTtwLnJhbmdlPXAubnVtYmVyO3ZhciBmPWZ1bmN0aW9uKHQpe3ZhciBlPShcIlwiK3QpLm1hdGNoKC8oPzpcXC4oXFxkKykpPyg/OltlRV0oWystXT9cXGQrKSk/JC8pO3JldHVybiBlP01hdGgubWF4KDAsKGVbMV0/ZVsxXS5sZW5ndGg6MCktKGVbMl0/K2VbMl06MCkpOjB9LG09ZnVuY3Rpb24ocyxhKXtyZXR1cm4gZnVuY3Rpb24odCl7Zm9yKHZhciBlPWFyZ3VtZW50cy5sZW5ndGgsaT1uZXcgQXJyYXkoMTxlP2UtMTowKSxuPTE7bjxlO24rKylpW24tMV09YXJndW1lbnRzW25dO3JldHVybiBpLnBvcCgpLCF0fHxhLmFwcGx5KHZvaWQgMCxbdF0uY29uY2F0KHUoKHI9cyxpLm1hcChkLnBhcnNlW3JdKSkpKSk7dmFyIHJ9fSxnPWZ1bmN0aW9uKHQpe3JldHVybnt2YWxpZGF0ZURhdGU6bShcImRhdGVcIix0KSx2YWxpZGF0ZU51bWJlcjptKFwibnVtYmVyXCIsdCkscmVxdWlyZW1lbnRUeXBlOnQubGVuZ3RoPD0yP1wic3RyaW5nXCI6W1wic3RyaW5nXCIsXCJzdHJpbmdcIl0scHJpb3JpdHk6MzB9fTthLnByb3RvdHlwZT17aW5pdDpmdW5jdGlvbih0LGUpe2Zvcih2YXIgaSBpbiB0aGlzLmNhdGFsb2c9ZSx0aGlzLnZhbGlkYXRvcnM9byh7fSx0aGlzLnZhbGlkYXRvcnMpLHQpdGhpcy5hZGRWYWxpZGF0b3IoaSx0W2ldLmZuLHRbaV0ucHJpb3JpdHkpO3dpbmRvdy5QYXJzbGV5LnRyaWdnZXIoXCJwYXJzbGV5OnZhbGlkYXRvcjppbml0XCIpfSxzZXRMb2NhbGU6ZnVuY3Rpb24odCl7aWYodm9pZCAwPT09dGhpcy5jYXRhbG9nW3RdKXRocm93IG5ldyBFcnJvcih0K1wiIGlzIG5vdCBhdmFpbGFibGUgaW4gdGhlIGNhdGFsb2dcIik7cmV0dXJuIHRoaXMubG9jYWxlPXQsdGhpc30sYWRkQ2F0YWxvZzpmdW5jdGlvbih0LGUsaSl7cmV0dXJuXCJvYmplY3RcIj09PXIoZSkmJih0aGlzLmNhdGFsb2dbdF09ZSksITA9PT1pP3RoaXMuc2V0TG9jYWxlKHQpOnRoaXN9LGFkZE1lc3NhZ2U6ZnVuY3Rpb24odCxlLGkpe3JldHVybiB2b2lkIDA9PT10aGlzLmNhdGFsb2dbdF0mJih0aGlzLmNhdGFsb2dbdF09e30pLHRoaXMuY2F0YWxvZ1t0XVtlXT1pLHRoaXN9LGFkZE1lc3NhZ2VzOmZ1bmN0aW9uKHQsZSl7Zm9yKHZhciBpIGluIGUpdGhpcy5hZGRNZXNzYWdlKHQsaSxlW2ldKTtyZXR1cm4gdGhpc30sYWRkVmFsaWRhdG9yOmZ1bmN0aW9uKHQsZSxpKXtpZih0aGlzLnZhbGlkYXRvcnNbdF0pZC53YXJuKCdWYWxpZGF0b3IgXCInK3QrJ1wiIGlzIGFscmVhZHkgZGVmaW5lZC4nKTtlbHNlIGlmKG4uaGFzT3duUHJvcGVydHkodCkpcmV0dXJuIHZvaWQgZC53YXJuKCdcIicrdCsnXCIgaXMgYSByZXN0cmljdGVkIGtleXdvcmQgYW5kIGlzIG5vdCBhIHZhbGlkIHZhbGlkYXRvciBuYW1lLicpO3JldHVybiB0aGlzLl9zZXRWYWxpZGF0b3IuYXBwbHkodGhpcyxhcmd1bWVudHMpfSxoYXNWYWxpZGF0b3I6ZnVuY3Rpb24odCl7cmV0dXJuISF0aGlzLnZhbGlkYXRvcnNbdF19LHVwZGF0ZVZhbGlkYXRvcjpmdW5jdGlvbih0LGUsaSl7cmV0dXJuIHRoaXMudmFsaWRhdG9yc1t0XT90aGlzLl9zZXRWYWxpZGF0b3IuYXBwbHkodGhpcyxhcmd1bWVudHMpOihkLndhcm4oJ1ZhbGlkYXRvciBcIicrdCsnXCIgaXMgbm90IGFscmVhZHkgZGVmaW5lZC4nKSx0aGlzLmFkZFZhbGlkYXRvci5hcHBseSh0aGlzLGFyZ3VtZW50cykpfSxyZW1vdmVWYWxpZGF0b3I6ZnVuY3Rpb24odCl7cmV0dXJuIHRoaXMudmFsaWRhdG9yc1t0XXx8ZC53YXJuKCdWYWxpZGF0b3IgXCInK3QrJ1wiIGlzIG5vdCBkZWZpbmVkLicpLGRlbGV0ZSB0aGlzLnZhbGlkYXRvcnNbdF0sdGhpc30sX3NldFZhbGlkYXRvcjpmdW5jdGlvbih0LGUsaSl7Zm9yKHZhciBuIGluXCJvYmplY3RcIiE9PXIoZSkmJihlPXtmbjplLHByaW9yaXR5Oml9KSxlLnZhbGlkYXRlfHwoZT1uZXcgYyhlKSksKHRoaXMudmFsaWRhdG9yc1t0XT1lKS5tZXNzYWdlc3x8e30pdGhpcy5hZGRNZXNzYWdlKG4sdCxlLm1lc3NhZ2VzW25dKTtyZXR1cm4gdGhpc30sZ2V0RXJyb3JNZXNzYWdlOmZ1bmN0aW9uKHQpe3ZhciBlO1widHlwZVwiPT09dC5uYW1lP2U9KHRoaXMuY2F0YWxvZ1t0aGlzLmxvY2FsZV1bdC5uYW1lXXx8e30pW3QucmVxdWlyZW1lbnRzXTplPXRoaXMuZm9ybWF0TWVzc2FnZSh0aGlzLmNhdGFsb2dbdGhpcy5sb2NhbGVdW3QubmFtZV0sdC5yZXF1aXJlbWVudHMpO3JldHVybiBlfHx0aGlzLmNhdGFsb2dbdGhpcy5sb2NhbGVdLmRlZmF1bHRNZXNzYWdlfHx0aGlzLmNhdGFsb2cuZW4uZGVmYXVsdE1lc3NhZ2V9LGZvcm1hdE1lc3NhZ2U6ZnVuY3Rpb24odCxlKXtpZihcIm9iamVjdFwiIT09cihlKSlyZXR1cm5cInN0cmluZ1wiPT10eXBlb2YgdD90LnJlcGxhY2UoLyVzL2ksZSk6XCJcIjtmb3IodmFyIGkgaW4gZSl0PXRoaXMuZm9ybWF0TWVzc2FnZSh0LGVbaV0pO3JldHVybiB0fSx2YWxpZGF0b3JzOntub3RibGFuazp7dmFsaWRhdGVTdHJpbmc6ZnVuY3Rpb24odCl7cmV0dXJuL1xcUy8udGVzdCh0KX0scHJpb3JpdHk6Mn0scmVxdWlyZWQ6e3ZhbGlkYXRlTXVsdGlwbGU6ZnVuY3Rpb24odCl7cmV0dXJuIDA8dC5sZW5ndGh9LHZhbGlkYXRlU3RyaW5nOmZ1bmN0aW9uKHQpe3JldHVybi9cXFMvLnRlc3QodCl9LHByaW9yaXR5OjUxMn0sdHlwZTp7dmFsaWRhdGVTdHJpbmc6ZnVuY3Rpb24odCxlKXt2YXIgaT0yPGFyZ3VtZW50cy5sZW5ndGgmJnZvaWQgMCE9PWFyZ3VtZW50c1syXT9hcmd1bWVudHNbMl06e30sbj1pLnN0ZXAscj12b2lkIDA9PT1uP1wiYW55XCI6bixzPWkuYmFzZSxhPXZvaWQgMD09PXM/MDpzLG89cFtlXTtpZighbyl0aHJvdyBuZXcgRXJyb3IoXCJ2YWxpZGF0b3IgdHlwZSBgXCIrZStcImAgaXMgbm90IHN1cHBvcnRlZFwiKTtpZighdClyZXR1cm4hMDtpZighby50ZXN0KHQpKXJldHVybiExO2lmKFwibnVtYmVyXCI9PT1lJiYhL15hbnkkL2kudGVzdChyfHxcIlwiKSl7dmFyIGw9TnVtYmVyKHQpLHU9TWF0aC5tYXgoZihyKSxmKGEpKTtpZihmKGwpPnUpcmV0dXJuITE7dmFyIGQ9ZnVuY3Rpb24odCl7cmV0dXJuIE1hdGgucm91bmQodCpNYXRoLnBvdygxMCx1KSl9O2lmKChkKGwpLWQoYSkpJWQocikhPTApcmV0dXJuITF9cmV0dXJuITB9LHJlcXVpcmVtZW50VHlwZTp7XCJcIjpcInN0cmluZ1wiLHN0ZXA6XCJzdHJpbmdcIixiYXNlOlwibnVtYmVyXCJ9LHByaW9yaXR5OjI1Nn0scGF0dGVybjp7dmFsaWRhdGVTdHJpbmc6ZnVuY3Rpb24odCxlKXtyZXR1cm4hdHx8ZS50ZXN0KHQpfSxyZXF1aXJlbWVudFR5cGU6XCJyZWdleHBcIixwcmlvcml0eTo2NH0sbWlubGVuZ3RoOnt2YWxpZGF0ZVN0cmluZzpmdW5jdGlvbih0LGUpe3JldHVybiF0fHx0Lmxlbmd0aD49ZX0scmVxdWlyZW1lbnRUeXBlOlwiaW50ZWdlclwiLHByaW9yaXR5OjMwfSxtYXhsZW5ndGg6e3ZhbGlkYXRlU3RyaW5nOmZ1bmN0aW9uKHQsZSl7cmV0dXJuIHQubGVuZ3RoPD1lfSxyZXF1aXJlbWVudFR5cGU6XCJpbnRlZ2VyXCIscHJpb3JpdHk6MzB9LGxlbmd0aDp7dmFsaWRhdGVTdHJpbmc6ZnVuY3Rpb24odCxlLGkpe3JldHVybiF0fHx0Lmxlbmd0aD49ZSYmdC5sZW5ndGg8PWl9LHJlcXVpcmVtZW50VHlwZTpbXCJpbnRlZ2VyXCIsXCJpbnRlZ2VyXCJdLHByaW9yaXR5OjMwfSxtaW5jaGVjazp7dmFsaWRhdGVNdWx0aXBsZTpmdW5jdGlvbih0LGUpe3JldHVybiB0Lmxlbmd0aD49ZX0scmVxdWlyZW1lbnRUeXBlOlwiaW50ZWdlclwiLHByaW9yaXR5OjMwfSxtYXhjaGVjazp7dmFsaWRhdGVNdWx0aXBsZTpmdW5jdGlvbih0LGUpe3JldHVybiB0Lmxlbmd0aDw9ZX0scmVxdWlyZW1lbnRUeXBlOlwiaW50ZWdlclwiLHByaW9yaXR5OjMwfSxjaGVjazp7dmFsaWRhdGVNdWx0aXBsZTpmdW5jdGlvbih0LGUsaSl7cmV0dXJuIHQubGVuZ3RoPj1lJiZ0Lmxlbmd0aDw9aX0scmVxdWlyZW1lbnRUeXBlOltcImludGVnZXJcIixcImludGVnZXJcIl0scHJpb3JpdHk6MzB9LG1pbjpnKGZ1bmN0aW9uKHQsZSl7cmV0dXJuIGU8PXR9KSxtYXg6ZyhmdW5jdGlvbih0LGUpe3JldHVybiB0PD1lfSkscmFuZ2U6ZyhmdW5jdGlvbih0LGUsaSl7cmV0dXJuIGU8PXQmJnQ8PWl9KSxlcXVhbHRvOnt2YWxpZGF0ZVN0cmluZzpmdW5jdGlvbih0LGUpe2lmKCF0KXJldHVybiEwO3ZhciBpPWgoZSk7cmV0dXJuIGkubGVuZ3RoP3Q9PT1pLnZhbCgpOnQ9PT1lfSxwcmlvcml0eToyNTZ9LGV1dmF0aW46e3ZhbGlkYXRlU3RyaW5nOmZ1bmN0aW9uKHQsZSl7aWYoIXQpcmV0dXJuITA7cmV0dXJuL15bQS1aXVtBLVpdW0EtWmEtejAtOSAtXXsyLH0kLy50ZXN0KHQpfSxwcmlvcml0eTozMH19fTt2YXIgdj17fTt2LkZvcm09e19hY3R1YWxpemVUcmlnZ2VyczpmdW5jdGlvbigpe3ZhciBlPXRoaXM7dGhpcy4kZWxlbWVudC5vbihcInN1Ym1pdC5QYXJzbGV5XCIsZnVuY3Rpb24odCl7ZS5vblN1Ym1pdFZhbGlkYXRlKHQpfSksdGhpcy4kZWxlbWVudC5vbihcImNsaWNrLlBhcnNsZXlcIixkLl9TdWJtaXRTZWxlY3RvcixmdW5jdGlvbih0KXtlLm9uU3VibWl0QnV0dG9uKHQpfSksITEhPT10aGlzLm9wdGlvbnMudWlFbmFibGVkJiZ0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlKFwibm92YWxpZGF0ZVwiLFwiXCIpfSxmb2N1czpmdW5jdGlvbigpe2lmKCEodGhpcy5fZm9jdXNlZEZpZWxkPW51bGwpPT09dGhpcy52YWxpZGF0aW9uUmVzdWx0fHxcIm5vbmVcIj09PXRoaXMub3B0aW9ucy5mb2N1cylyZXR1cm4gbnVsbDtmb3IodmFyIHQ9MDt0PHRoaXMuZmllbGRzLmxlbmd0aDt0Kyspe3ZhciBlPXRoaXMuZmllbGRzW3RdO2lmKCEwIT09ZS52YWxpZGF0aW9uUmVzdWx0JiYwPGUudmFsaWRhdGlvblJlc3VsdC5sZW5ndGgmJnZvaWQgMD09PWUub3B0aW9ucy5ub0ZvY3VzJiYodGhpcy5fZm9jdXNlZEZpZWxkPWUuJGVsZW1lbnQsXCJmaXJzdFwiPT09dGhpcy5vcHRpb25zLmZvY3VzKSlicmVha31yZXR1cm4gbnVsbD09PXRoaXMuX2ZvY3VzZWRGaWVsZD9udWxsOnRoaXMuX2ZvY3VzZWRGaWVsZC5mb2N1cygpfSxfZGVzdHJveVVJOmZ1bmN0aW9uKCl7dGhpcy4kZWxlbWVudC5vZmYoXCIuUGFyc2xleVwiKX19LHYuRmllbGQ9e19yZWZsb3dVSTpmdW5jdGlvbigpe2lmKHRoaXMuX2J1aWxkVUkoKSx0aGlzLl91aSl7dmFyIHQ9ZnVuY3Rpb24gdChlLGksbil7Zm9yKHZhciByPVtdLHM9W10sYT0wO2E8ZS5sZW5ndGg7YSsrKXtmb3IodmFyIG89ITEsbD0wO2w8aS5sZW5ndGg7bCsrKWlmKGVbYV0uYXNzZXJ0Lm5hbWU9PT1pW2xdLmFzc2VydC5uYW1lKXtvPSEwO2JyZWFrfW8/cy5wdXNoKGVbYV0pOnIucHVzaChlW2FdKX1yZXR1cm57a2VwdDpzLGFkZGVkOnIscmVtb3ZlZDpuP1tdOnQoaSxlLCEwKS5hZGRlZH19KHRoaXMudmFsaWRhdGlvblJlc3VsdCx0aGlzLl91aS5sYXN0VmFsaWRhdGlvblJlc3VsdCk7dGhpcy5fdWkubGFzdFZhbGlkYXRpb25SZXN1bHQ9dGhpcy52YWxpZGF0aW9uUmVzdWx0LHRoaXMuX21hbmFnZVN0YXR1c0NsYXNzKCksdGhpcy5fbWFuYWdlRXJyb3JzTWVzc2FnZXModCksdGhpcy5fYWN0dWFsaXplVHJpZ2dlcnMoKSwhdC5rZXB0Lmxlbmd0aCYmIXQuYWRkZWQubGVuZ3RofHx0aGlzLl9mYWlsZWRPbmNlfHwodGhpcy5fZmFpbGVkT25jZT0hMCx0aGlzLl9hY3R1YWxpemVUcmlnZ2VycygpKX19LGdldEVycm9yc01lc3NhZ2VzOmZ1bmN0aW9uKCl7aWYoITA9PT10aGlzLnZhbGlkYXRpb25SZXN1bHQpcmV0dXJuW107Zm9yKHZhciB0PVtdLGU9MDtlPHRoaXMudmFsaWRhdGlvblJlc3VsdC5sZW5ndGg7ZSsrKXQucHVzaCh0aGlzLnZhbGlkYXRpb25SZXN1bHRbZV0uZXJyb3JNZXNzYWdlfHx0aGlzLl9nZXRFcnJvck1lc3NhZ2UodGhpcy52YWxpZGF0aW9uUmVzdWx0W2VdLmFzc2VydCkpO3JldHVybiB0fSxhZGRFcnJvcjpmdW5jdGlvbih0KXt2YXIgZT0xPGFyZ3VtZW50cy5sZW5ndGgmJnZvaWQgMCE9PWFyZ3VtZW50c1sxXT9hcmd1bWVudHNbMV06e30saT1lLm1lc3NhZ2Usbj1lLmFzc2VydCxyPWUudXBkYXRlQ2xhc3Mscz12b2lkIDA9PT1yfHxyO3RoaXMuX2J1aWxkVUkoKSx0aGlzLl9hZGRFcnJvcih0LHttZXNzYWdlOmksYXNzZXJ0Om59KSxzJiZ0aGlzLl9lcnJvckNsYXNzKCl9LHVwZGF0ZUVycm9yOmZ1bmN0aW9uKHQpe3ZhciBlPTE8YXJndW1lbnRzLmxlbmd0aCYmdm9pZCAwIT09YXJndW1lbnRzWzFdP2FyZ3VtZW50c1sxXTp7fSxpPWUubWVzc2FnZSxuPWUuYXNzZXJ0LHI9ZS51cGRhdGVDbGFzcyxzPXZvaWQgMD09PXJ8fHI7dGhpcy5fYnVpbGRVSSgpLHRoaXMuX3VwZGF0ZUVycm9yKHQse21lc3NhZ2U6aSxhc3NlcnQ6bn0pLHMmJnRoaXMuX2Vycm9yQ2xhc3MoKX0scmVtb3ZlRXJyb3I6ZnVuY3Rpb24odCl7dmFyIGU9KDE8YXJndW1lbnRzLmxlbmd0aCYmdm9pZCAwIT09YXJndW1lbnRzWzFdP2FyZ3VtZW50c1sxXTp7fSkudXBkYXRlQ2xhc3MsaT12b2lkIDA9PT1lfHxlO3RoaXMuX2J1aWxkVUkoKSx0aGlzLl9yZW1vdmVFcnJvcih0KSxpJiZ0aGlzLl9tYW5hZ2VTdGF0dXNDbGFzcygpfSxfbWFuYWdlU3RhdHVzQ2xhc3M6ZnVuY3Rpb24oKXt0aGlzLmhhc0NvbnN0cmFpbnRzKCkmJnRoaXMubmVlZHNWYWxpZGF0aW9uKCkmJiEwPT09dGhpcy52YWxpZGF0aW9uUmVzdWx0P3RoaXMuX3N1Y2Nlc3NDbGFzcygpOjA8dGhpcy52YWxpZGF0aW9uUmVzdWx0Lmxlbmd0aD90aGlzLl9lcnJvckNsYXNzKCk6dGhpcy5fcmVzZXRDbGFzcygpfSxfbWFuYWdlRXJyb3JzTWVzc2FnZXM6ZnVuY3Rpb24odCl7aWYodm9pZCAwPT09dGhpcy5vcHRpb25zLmVycm9yc01lc3NhZ2VzRGlzYWJsZWQpe2lmKHZvaWQgMCE9PXRoaXMub3B0aW9ucy5lcnJvck1lc3NhZ2UpcmV0dXJuIHQuYWRkZWQubGVuZ3RofHx0LmtlcHQubGVuZ3RoPyh0aGlzLl9pbnNlcnRFcnJvcldyYXBwZXIoKSwwPT09dGhpcy5fdWkuJGVycm9yc1dyYXBwZXIuZmluZChcIi5wYXJzbGV5LWN1c3RvbS1lcnJvci1tZXNzYWdlXCIpLmxlbmd0aCYmdGhpcy5fdWkuJGVycm9yc1dyYXBwZXIuYXBwZW5kKGgodGhpcy5vcHRpb25zLmVycm9yVGVtcGxhdGUpLmFkZENsYXNzKFwicGFyc2xleS1jdXN0b20tZXJyb3ItbWVzc2FnZVwiKSksdGhpcy5fdWkuJGVycm9yc1dyYXBwZXIuYWRkQ2xhc3MoXCJmaWxsZWRcIikuZmluZChcIi5wYXJzbGV5LWN1c3RvbS1lcnJvci1tZXNzYWdlXCIpLmh0bWwodGhpcy5vcHRpb25zLmVycm9yTWVzc2FnZSkpOnRoaXMuX3VpLiRlcnJvcnNXcmFwcGVyLnJlbW92ZUNsYXNzKFwiZmlsbGVkXCIpLmZpbmQoXCIucGFyc2xleS1jdXN0b20tZXJyb3ItbWVzc2FnZVwiKS5yZW1vdmUoKTtmb3IodmFyIGU9MDtlPHQucmVtb3ZlZC5sZW5ndGg7ZSsrKXRoaXMuX3JlbW92ZUVycm9yKHQucmVtb3ZlZFtlXS5hc3NlcnQubmFtZSk7Zm9yKGU9MDtlPHQuYWRkZWQubGVuZ3RoO2UrKyl0aGlzLl9hZGRFcnJvcih0LmFkZGVkW2VdLmFzc2VydC5uYW1lLHttZXNzYWdlOnQuYWRkZWRbZV0uZXJyb3JNZXNzYWdlLGFzc2VydDp0LmFkZGVkW2VdLmFzc2VydH0pO2ZvcihlPTA7ZTx0LmtlcHQubGVuZ3RoO2UrKyl0aGlzLl91cGRhdGVFcnJvcih0LmtlcHRbZV0uYXNzZXJ0Lm5hbWUse21lc3NhZ2U6dC5rZXB0W2VdLmVycm9yTWVzc2FnZSxhc3NlcnQ6dC5rZXB0W2VdLmFzc2VydH0pfX0sX2FkZEVycm9yOmZ1bmN0aW9uKHQsZSl7dmFyIGk9ZS5tZXNzYWdlLG49ZS5hc3NlcnQ7dGhpcy5faW5zZXJ0RXJyb3JXcmFwcGVyKCksdGhpcy5fdWkuJGVycm9yQ2xhc3NIYW5kbGVyLmF0dHIoXCJhcmlhLWRlc2NyaWJlZGJ5XCIsdGhpcy5fdWkuZXJyb3JzV3JhcHBlcklkKSx0aGlzLl91aS4kZXJyb3JzV3JhcHBlci5hZGRDbGFzcyhcImZpbGxlZFwiKS5hcHBlbmQoaCh0aGlzLm9wdGlvbnMuZXJyb3JUZW1wbGF0ZSkuYWRkQ2xhc3MoXCJwYXJzbGV5LVwiK3QpLmh0bWwoaXx8dGhpcy5fZ2V0RXJyb3JNZXNzYWdlKG4pKSl9LF91cGRhdGVFcnJvcjpmdW5jdGlvbih0LGUpe3ZhciBpPWUubWVzc2FnZSxuPWUuYXNzZXJ0O3RoaXMuX3VpLiRlcnJvcnNXcmFwcGVyLmFkZENsYXNzKFwiZmlsbGVkXCIpLmZpbmQoXCIucGFyc2xleS1cIit0KS5odG1sKGl8fHRoaXMuX2dldEVycm9yTWVzc2FnZShuKSl9LF9yZW1vdmVFcnJvcjpmdW5jdGlvbih0KXt0aGlzLl91aS4kZXJyb3JDbGFzc0hhbmRsZXIucmVtb3ZlQXR0cihcImFyaWEtZGVzY3JpYmVkYnlcIiksdGhpcy5fdWkuJGVycm9yc1dyYXBwZXIucmVtb3ZlQ2xhc3MoXCJmaWxsZWRcIikuZmluZChcIi5wYXJzbGV5LVwiK3QpLnJlbW92ZSgpfSxfZ2V0RXJyb3JNZXNzYWdlOmZ1bmN0aW9uKHQpe3ZhciBlPXQubmFtZStcIk1lc3NhZ2VcIjtyZXR1cm4gdm9pZCAwIT09dGhpcy5vcHRpb25zW2VdP3dpbmRvdy5QYXJzbGV5LmZvcm1hdE1lc3NhZ2UodGhpcy5vcHRpb25zW2VdLHQucmVxdWlyZW1lbnRzKTp3aW5kb3cuUGFyc2xleS5nZXRFcnJvck1lc3NhZ2UodCl9LF9idWlsZFVJOmZ1bmN0aW9uKCl7aWYoIXRoaXMuX3VpJiYhMSE9PXRoaXMub3B0aW9ucy51aUVuYWJsZWQpe3ZhciB0PXt9O3RoaXMuZWxlbWVudC5zZXRBdHRyaWJ1dGUodGhpcy5vcHRpb25zLm5hbWVzcGFjZStcImlkXCIsdGhpcy5fX2lkX18pLHQuJGVycm9yQ2xhc3NIYW5kbGVyPXRoaXMuX21hbmFnZUNsYXNzSGFuZGxlcigpLHQuZXJyb3JzV3JhcHBlcklkPVwicGFyc2xleS1pZC1cIisodGhpcy5vcHRpb25zLm11bHRpcGxlP1wibXVsdGlwbGUtXCIrdGhpcy5vcHRpb25zLm11bHRpcGxlOnRoaXMuX19pZF9fKSx0LiRlcnJvcnNXcmFwcGVyPWgodGhpcy5vcHRpb25zLmVycm9yc1dyYXBwZXIpLmF0dHIoXCJpZFwiLHQuZXJyb3JzV3JhcHBlcklkKSx0Lmxhc3RWYWxpZGF0aW9uUmVzdWx0PVtdLHQudmFsaWRhdGlvbkluZm9ybWF0aW9uVmlzaWJsZT0hMSx0aGlzLl91aT10fX0sX21hbmFnZUNsYXNzSGFuZGxlcjpmdW5jdGlvbigpe2lmKFwic3RyaW5nXCI9PXR5cGVvZiB0aGlzLm9wdGlvbnMuY2xhc3NIYW5kbGVyJiZoKHRoaXMub3B0aW9ucy5jbGFzc0hhbmRsZXIpLmxlbmd0aClyZXR1cm4gaCh0aGlzLm9wdGlvbnMuY2xhc3NIYW5kbGVyKTt2YXIgdD10aGlzLm9wdGlvbnMuY2xhc3NIYW5kbGVyO2lmKFwic3RyaW5nXCI9PXR5cGVvZiB0aGlzLm9wdGlvbnMuY2xhc3NIYW5kbGVyJiZcImZ1bmN0aW9uXCI9PXR5cGVvZiB3aW5kb3dbdGhpcy5vcHRpb25zLmNsYXNzSGFuZGxlcl0mJih0PXdpbmRvd1t0aGlzLm9wdGlvbnMuY2xhc3NIYW5kbGVyXSksXCJmdW5jdGlvblwiPT10eXBlb2YgdCl7dmFyIGU9dC5jYWxsKHRoaXMsdGhpcyk7aWYodm9pZCAwIT09ZSYmZS5sZW5ndGgpcmV0dXJuIGV9ZWxzZXtpZihcIm9iamVjdFwiPT09cih0KSYmdCBpbnN0YW5jZW9mIGpRdWVyeSYmdC5sZW5ndGgpcmV0dXJuIHQ7dCYmZC53YXJuKFwiVGhlIGNsYXNzIGhhbmRsZXIgYFwiK3QrXCJgIGRvZXMgbm90IGV4aXN0IGluIERPTSBub3IgYXMgYSBnbG9iYWwgSlMgZnVuY3Rpb25cIil9cmV0dXJuIHRoaXMuX2lucHV0SG9sZGVyKCl9LF9pbnB1dEhvbGRlcjpmdW5jdGlvbigpe3JldHVybiB0aGlzLm9wdGlvbnMubXVsdGlwbGUmJlwiU0VMRUNUXCIhPT10aGlzLmVsZW1lbnQubm9kZU5hbWU/dGhpcy4kZWxlbWVudC5wYXJlbnQoKTp0aGlzLiRlbGVtZW50fSxfaW5zZXJ0RXJyb3JXcmFwcGVyOmZ1bmN0aW9uKCl7dmFyIHQ9dGhpcy5vcHRpb25zLmVycm9yc0NvbnRhaW5lcjtpZigwIT09dGhpcy5fdWkuJGVycm9yc1dyYXBwZXIucGFyZW50KCkubGVuZ3RoKXJldHVybiB0aGlzLl91aS4kZXJyb3JzV3JhcHBlci5wYXJlbnQoKTtpZihcInN0cmluZ1wiPT10eXBlb2YgdCl7aWYoaCh0KS5sZW5ndGgpcmV0dXJuIGgodCkuYXBwZW5kKHRoaXMuX3VpLiRlcnJvcnNXcmFwcGVyKTtcImZ1bmN0aW9uXCI9PXR5cGVvZiB3aW5kb3dbdF0/dD13aW5kb3dbdF06ZC53YXJuKFwiVGhlIGVycm9ycyBjb250YWluZXIgYFwiK3QrXCJgIGRvZXMgbm90IGV4aXN0IGluIERPTSBub3IgYXMgYSBnbG9iYWwgSlMgZnVuY3Rpb25cIil9cmV0dXJuXCJmdW5jdGlvblwiPT10eXBlb2YgdCYmKHQ9dC5jYWxsKHRoaXMsdGhpcykpLFwib2JqZWN0XCI9PT1yKHQpJiZ0Lmxlbmd0aD90LmFwcGVuZCh0aGlzLl91aS4kZXJyb3JzV3JhcHBlcik6dGhpcy5faW5wdXRIb2xkZXIoKS5hZnRlcih0aGlzLl91aS4kZXJyb3JzV3JhcHBlcil9LF9hY3R1YWxpemVUcmlnZ2VyczpmdW5jdGlvbigpe3ZhciB0LGU9dGhpcyxpPXRoaXMuX2ZpbmRSZWxhdGVkKCk7aS5vZmYoXCIuUGFyc2xleVwiKSx0aGlzLl9mYWlsZWRPbmNlP2kub24oZC5uYW1lc3BhY2VFdmVudHModGhpcy5vcHRpb25zLnRyaWdnZXJBZnRlckZhaWx1cmUsXCJQYXJzbGV5XCIpLGZ1bmN0aW9uKCl7ZS5fdmFsaWRhdGVJZk5lZWRlZCgpfSk6KHQ9ZC5uYW1lc3BhY2VFdmVudHModGhpcy5vcHRpb25zLnRyaWdnZXIsXCJQYXJzbGV5XCIpKSYmaS5vbih0LGZ1bmN0aW9uKHQpe2UuX3ZhbGlkYXRlSWZOZWVkZWQodCl9KX0sX3ZhbGlkYXRlSWZOZWVkZWQ6ZnVuY3Rpb24odCl7dmFyIGU9dGhpczt0JiYva2V5fGlucHV0Ly50ZXN0KHQudHlwZSkmJighdGhpcy5fdWl8fCF0aGlzLl91aS52YWxpZGF0aW9uSW5mb3JtYXRpb25WaXNpYmxlKSYmdGhpcy5nZXRWYWx1ZSgpLmxlbmd0aDw9dGhpcy5vcHRpb25zLnZhbGlkYXRpb25UaHJlc2hvbGR8fCh0aGlzLm9wdGlvbnMuZGVib3VuY2U/KHdpbmRvdy5jbGVhclRpbWVvdXQodGhpcy5fZGVib3VuY2VkKSx0aGlzLl9kZWJvdW5jZWQ9d2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24oKXtyZXR1cm4gZS52YWxpZGF0ZSgpfSx0aGlzLm9wdGlvbnMuZGVib3VuY2UpKTp0aGlzLnZhbGlkYXRlKCkpfSxfcmVzZXRVSTpmdW5jdGlvbigpe3RoaXMuX2ZhaWxlZE9uY2U9ITEsdGhpcy5fYWN0dWFsaXplVHJpZ2dlcnMoKSx2b2lkIDAhPT10aGlzLl91aSYmKHRoaXMuX3VpLiRlcnJvcnNXcmFwcGVyLnJlbW92ZUNsYXNzKFwiZmlsbGVkXCIpLmNoaWxkcmVuKCkucmVtb3ZlKCksdGhpcy5fcmVzZXRDbGFzcygpLHRoaXMuX3VpLmxhc3RWYWxpZGF0aW9uUmVzdWx0PVtdLHRoaXMuX3VpLnZhbGlkYXRpb25JbmZvcm1hdGlvblZpc2libGU9ITEpfSxfZGVzdHJveVVJOmZ1bmN0aW9uKCl7dGhpcy5fcmVzZXRVSSgpLHZvaWQgMCE9PXRoaXMuX3VpJiZ0aGlzLl91aS4kZXJyb3JzV3JhcHBlci5yZW1vdmUoKSxkZWxldGUgdGhpcy5fdWl9LF9zdWNjZXNzQ2xhc3M6ZnVuY3Rpb24oKXt0aGlzLl91aS52YWxpZGF0aW9uSW5mb3JtYXRpb25WaXNpYmxlPSEwLHRoaXMuX3VpLiRlcnJvckNsYXNzSGFuZGxlci5yZW1vdmVDbGFzcyh0aGlzLm9wdGlvbnMuZXJyb3JDbGFzcykuYWRkQ2xhc3ModGhpcy5vcHRpb25zLnN1Y2Nlc3NDbGFzcyl9LF9lcnJvckNsYXNzOmZ1bmN0aW9uKCl7dGhpcy5fdWkudmFsaWRhdGlvbkluZm9ybWF0aW9uVmlzaWJsZT0hMCx0aGlzLl91aS4kZXJyb3JDbGFzc0hhbmRsZXIucmVtb3ZlQ2xhc3ModGhpcy5vcHRpb25zLnN1Y2Nlc3NDbGFzcykuYWRkQ2xhc3ModGhpcy5vcHRpb25zLmVycm9yQ2xhc3MpfSxfcmVzZXRDbGFzczpmdW5jdGlvbigpe3RoaXMuX3VpLiRlcnJvckNsYXNzSGFuZGxlci5yZW1vdmVDbGFzcyh0aGlzLm9wdGlvbnMuc3VjY2Vzc0NsYXNzKS5yZW1vdmVDbGFzcyh0aGlzLm9wdGlvbnMuZXJyb3JDbGFzcyl9fTt2YXIgeT1mdW5jdGlvbih0LGUsaSl7dGhpcy5fX2NsYXNzX189XCJGb3JtXCIsdGhpcy5lbGVtZW50PXQsdGhpcy4kZWxlbWVudD1oKHQpLHRoaXMuZG9tT3B0aW9ucz1lLHRoaXMub3B0aW9ucz1pLHRoaXMucGFyZW50PXdpbmRvdy5QYXJzbGV5LHRoaXMuZmllbGRzPVtdLHRoaXMudmFsaWRhdGlvblJlc3VsdD1udWxsfSxfPXtwZW5kaW5nOm51bGwscmVzb2x2ZWQ6ITAscmVqZWN0ZWQ6ITF9O3kucHJvdG90eXBlPXtvblN1Ym1pdFZhbGlkYXRlOmZ1bmN0aW9uKHQpe3ZhciBlPXRoaXM7aWYoITAhPT10LnBhcnNsZXkpe3ZhciBpPXRoaXMuX3N1Ym1pdFNvdXJjZXx8dGhpcy4kZWxlbWVudC5maW5kKGQuX1N1Ym1pdFNlbGVjdG9yKVswXTtpZih0aGlzLl9zdWJtaXRTb3VyY2U9bnVsbCx0aGlzLiRlbGVtZW50LmZpbmQoXCIucGFyc2xleS1zeW50aGV0aWMtc3VibWl0LWJ1dHRvblwiKS5wcm9wKFwiZGlzYWJsZWRcIiwhMCksIWl8fG51bGw9PT1pLmdldEF0dHJpYnV0ZShcImZvcm1ub3ZhbGlkYXRlXCIpKXt3aW5kb3cuUGFyc2xleS5fcmVtb3RlQ2FjaGU9e307dmFyIG49dGhpcy53aGVuVmFsaWRhdGUoe2V2ZW50OnR9KTtcInJlc29sdmVkXCI9PT1uLnN0YXRlKCkmJiExIT09dGhpcy5fdHJpZ2dlcihcInN1Ym1pdFwiKXx8KHQuc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uKCksdC5wcmV2ZW50RGVmYXVsdCgpLFwicGVuZGluZ1wiPT09bi5zdGF0ZSgpJiZuLmRvbmUoZnVuY3Rpb24oKXtlLl9zdWJtaXQoaSl9KSl9fX0sb25TdWJtaXRCdXR0b246ZnVuY3Rpb24odCl7dGhpcy5fc3VibWl0U291cmNlPXQuY3VycmVudFRhcmdldH0sX3N1Ym1pdDpmdW5jdGlvbih0KXtpZighMSE9PXRoaXMuX3RyaWdnZXIoXCJzdWJtaXRcIikpe2lmKHQpe3ZhciBlPXRoaXMuJGVsZW1lbnQuZmluZChcIi5wYXJzbGV5LXN5bnRoZXRpYy1zdWJtaXQtYnV0dG9uXCIpLnByb3AoXCJkaXNhYmxlZFwiLCExKTswPT09ZS5sZW5ndGgmJihlPWgoJzxpbnB1dCBjbGFzcz1cInBhcnNsZXktc3ludGhldGljLXN1Ym1pdC1idXR0b25cIiB0eXBlPVwiaGlkZGVuXCI+JykuYXBwZW5kVG8odGhpcy4kZWxlbWVudCkpLGUuYXR0cih7bmFtZTp0LmdldEF0dHJpYnV0ZShcIm5hbWVcIiksdmFsdWU6dC5nZXRBdHRyaWJ1dGUoXCJ2YWx1ZVwiKX0pfXRoaXMuJGVsZW1lbnQudHJpZ2dlcihvKGguRXZlbnQoXCJzdWJtaXRcIikse3BhcnNsZXk6ITB9KSl9fSx2YWxpZGF0ZTpmdW5jdGlvbih0KXtpZigxPD1hcmd1bWVudHMubGVuZ3RoJiYhaC5pc1BsYWluT2JqZWN0KHQpKXtkLndhcm5PbmNlKFwiQ2FsbGluZyB2YWxpZGF0ZSBvbiBhIHBhcnNsZXkgZm9ybSB3aXRob3V0IHBhc3NpbmcgYXJndW1lbnRzIGFzIGFuIG9iamVjdCBpcyBkZXByZWNhdGVkLlwiKTt2YXIgZT1BcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpO3Q9e2dyb3VwOmVbMF0sZm9yY2U6ZVsxXSxldmVudDplWzJdfX1yZXR1cm4gX1t0aGlzLndoZW5WYWxpZGF0ZSh0KS5zdGF0ZSgpXX0sd2hlblZhbGlkYXRlOmZ1bmN0aW9uKCl7dmFyIHQsZT10aGlzLGk9MDxhcmd1bWVudHMubGVuZ3RoJiZ2b2lkIDAhPT1hcmd1bWVudHNbMF0/YXJndW1lbnRzWzBdOnt9LG49aS5ncm91cCxyPWkuZm9yY2Uscz1pLmV2ZW50Oyh0aGlzLnN1Ym1pdEV2ZW50PXMpJiYodGhpcy5zdWJtaXRFdmVudD1vKHt9LHMse3ByZXZlbnREZWZhdWx0OmZ1bmN0aW9uKCl7ZC53YXJuT25jZShcIlVzaW5nIGB0aGlzLnN1Ym1pdEV2ZW50LnByZXZlbnREZWZhdWx0KClgIGlzIGRlcHJlY2F0ZWQ7IGluc3RlYWQsIGNhbGwgYHRoaXMudmFsaWRhdGlvblJlc3VsdCA9IGZhbHNlYFwiKSxlLnZhbGlkYXRpb25SZXN1bHQ9ITF9fSkpLHRoaXMudmFsaWRhdGlvblJlc3VsdD0hMCx0aGlzLl90cmlnZ2VyKFwidmFsaWRhdGVcIiksdGhpcy5fcmVmcmVzaEZpZWxkcygpO3ZhciBhPXRoaXMuX3dpdGhvdXRSZWFjdHVhbGl6aW5nRm9ybU9wdGlvbnMoZnVuY3Rpb24oKXtyZXR1cm4gaC5tYXAoZS5maWVsZHMsZnVuY3Rpb24odCl7cmV0dXJuIHQud2hlblZhbGlkYXRlKHtmb3JjZTpyLGdyb3VwOm59KX0pfSk7cmV0dXJuKHQ9ZC5hbGwoYSkuZG9uZShmdW5jdGlvbigpe2UuX3RyaWdnZXIoXCJzdWNjZXNzXCIpfSkuZmFpbChmdW5jdGlvbigpe2UudmFsaWRhdGlvblJlc3VsdD0hMSxlLmZvY3VzKCksZS5fdHJpZ2dlcihcImVycm9yXCIpfSkuYWx3YXlzKGZ1bmN0aW9uKCl7ZS5fdHJpZ2dlcihcInZhbGlkYXRlZFwiKX0pKS5waXBlLmFwcGx5KHQsdSh0aGlzLl9waXBlQWNjb3JkaW5nVG9WYWxpZGF0aW9uUmVzdWx0KCkpKX0saXNWYWxpZDpmdW5jdGlvbih0KXtpZigxPD1hcmd1bWVudHMubGVuZ3RoJiYhaC5pc1BsYWluT2JqZWN0KHQpKXtkLndhcm5PbmNlKFwiQ2FsbGluZyBpc1ZhbGlkIG9uIGEgcGFyc2xleSBmb3JtIHdpdGhvdXQgcGFzc2luZyBhcmd1bWVudHMgYXMgYW4gb2JqZWN0IGlzIGRlcHJlY2F0ZWQuXCIpO3ZhciBlPUFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cyk7dD17Z3JvdXA6ZVswXSxmb3JjZTplWzFdfX1yZXR1cm4gX1t0aGlzLndoZW5WYWxpZCh0KS5zdGF0ZSgpXX0sd2hlblZhbGlkOmZ1bmN0aW9uKCl7dmFyIHQ9dGhpcyxlPTA8YXJndW1lbnRzLmxlbmd0aCYmdm9pZCAwIT09YXJndW1lbnRzWzBdP2FyZ3VtZW50c1swXTp7fSxpPWUuZ3JvdXAsbj1lLmZvcmNlO3RoaXMuX3JlZnJlc2hGaWVsZHMoKTt2YXIgcj10aGlzLl93aXRob3V0UmVhY3R1YWxpemluZ0Zvcm1PcHRpb25zKGZ1bmN0aW9uKCl7cmV0dXJuIGgubWFwKHQuZmllbGRzLGZ1bmN0aW9uKHQpe3JldHVybiB0LndoZW5WYWxpZCh7Z3JvdXA6aSxmb3JjZTpufSl9KX0pO3JldHVybiBkLmFsbChyKX0scmVmcmVzaDpmdW5jdGlvbigpe3JldHVybiB0aGlzLl9yZWZyZXNoRmllbGRzKCksdGhpc30scmVzZXQ6ZnVuY3Rpb24oKXtmb3IodmFyIHQ9MDt0PHRoaXMuZmllbGRzLmxlbmd0aDt0KyspdGhpcy5maWVsZHNbdF0ucmVzZXQoKTt0aGlzLl90cmlnZ2VyKFwicmVzZXRcIil9LGRlc3Ryb3k6ZnVuY3Rpb24oKXt0aGlzLl9kZXN0cm95VUkoKTtmb3IodmFyIHQ9MDt0PHRoaXMuZmllbGRzLmxlbmd0aDt0KyspdGhpcy5maWVsZHNbdF0uZGVzdHJveSgpO3RoaXMuJGVsZW1lbnQucmVtb3ZlRGF0YShcIlBhcnNsZXlcIiksdGhpcy5fdHJpZ2dlcihcImRlc3Ryb3lcIil9LF9yZWZyZXNoRmllbGRzOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuYWN0dWFsaXplT3B0aW9ucygpLl9iaW5kRmllbGRzKCl9LF9iaW5kRmllbGRzOmZ1bmN0aW9uKCl7dmFyIHI9dGhpcyx0PXRoaXMuZmllbGRzO3JldHVybiB0aGlzLmZpZWxkcz1bXSx0aGlzLmZpZWxkc01hcHBlZEJ5SWQ9e30sdGhpcy5fd2l0aG91dFJlYWN0dWFsaXppbmdGb3JtT3B0aW9ucyhmdW5jdGlvbigpe3IuJGVsZW1lbnQuZmluZChyLm9wdGlvbnMuaW5wdXRzKS5ub3Qoci5vcHRpb25zLmV4Y2x1ZGVkKS5ub3QoXCJbXCIuY29uY2F0KHIub3B0aW9ucy5uYW1lc3BhY2UsXCJleGNsdWRlZD10cnVlXVwiKSkuZWFjaChmdW5jdGlvbih0LGUpe3ZhciBpPW5ldyB3aW5kb3cuUGFyc2xleS5GYWN0b3J5KGUse30scik7aWYoXCJGaWVsZFwiPT09aS5fX2NsYXNzX198fFwiRmllbGRNdWx0aXBsZVwiPT09aS5fX2NsYXNzX18pe3ZhciBuPWkuX19jbGFzc19fK1wiLVwiK2kuX19pZF9fO3ZvaWQgMD09PXIuZmllbGRzTWFwcGVkQnlJZFtuXSYmKHIuZmllbGRzTWFwcGVkQnlJZFtuXT1pLHIuZmllbGRzLnB1c2goaSkpfX0pLGguZWFjaChkLmRpZmZlcmVuY2UodCxyLmZpZWxkcyksZnVuY3Rpb24odCxlKXtlLnJlc2V0KCl9KX0pLHRoaXN9LF93aXRob3V0UmVhY3R1YWxpemluZ0Zvcm1PcHRpb25zOmZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMuYWN0dWFsaXplT3B0aW9uczt0aGlzLmFjdHVhbGl6ZU9wdGlvbnM9ZnVuY3Rpb24oKXtyZXR1cm4gdGhpc307dmFyIGk9dCgpO3JldHVybiB0aGlzLmFjdHVhbGl6ZU9wdGlvbnM9ZSxpfSxfdHJpZ2dlcjpmdW5jdGlvbih0KXtyZXR1cm4gdGhpcy50cmlnZ2VyKFwiZm9ybTpcIit0KX19O3ZhciB3PWZ1bmN0aW9uKHQsZSxpLG4scil7dmFyIHM9d2luZG93LlBhcnNsZXkuX3ZhbGlkYXRvclJlZ2lzdHJ5LnZhbGlkYXRvcnNbZV0sYT1uZXcgYyhzKTtvKHRoaXMse3ZhbGlkYXRvcjphLG5hbWU6ZSxyZXF1aXJlbWVudHM6aSxwcmlvcml0eTpuPW58fHQub3B0aW9uc1tlK1wiUHJpb3JpdHlcIl18fGEucHJpb3JpdHksaXNEb21Db25zdHJhaW50OnI9ITA9PT1yfSksdGhpcy5fcGFyc2VSZXF1aXJlbWVudHModC5vcHRpb25zKX0sYj1mdW5jdGlvbih0LGUsaSxuKXt0aGlzLl9fY2xhc3NfXz1cIkZpZWxkXCIsdGhpcy5lbGVtZW50PXQsdGhpcy4kZWxlbWVudD1oKHQpLHZvaWQgMCE9PW4mJih0aGlzLnBhcmVudD1uKSx0aGlzLm9wdGlvbnM9aSx0aGlzLmRvbU9wdGlvbnM9ZSx0aGlzLmNvbnN0cmFpbnRzPVtdLHRoaXMuY29uc3RyYWludHNCeU5hbWU9e30sdGhpcy52YWxpZGF0aW9uUmVzdWx0PSEwLHRoaXMuX2JpbmRDb25zdHJhaW50cygpfSxGPXtwZW5kaW5nOm51bGwscmVzb2x2ZWQ6ITAscmVqZWN0ZWQ6ISh3LnByb3RvdHlwZT17dmFsaWRhdGU6ZnVuY3Rpb24odCxlKXt2YXIgaTtyZXR1cm4oaT10aGlzLnZhbGlkYXRvcikudmFsaWRhdGUuYXBwbHkoaSxbdF0uY29uY2F0KHUodGhpcy5yZXF1aXJlbWVudExpc3QpLFtlXSkpfSxfcGFyc2VSZXF1aXJlbWVudHM6ZnVuY3Rpb24oaSl7dmFyIG49dGhpczt0aGlzLnJlcXVpcmVtZW50TGlzdD10aGlzLnZhbGlkYXRvci5wYXJzZVJlcXVpcmVtZW50cyh0aGlzLnJlcXVpcmVtZW50cyxmdW5jdGlvbih0KXtyZXR1cm4gaVtuLm5hbWUrKGU9dCxlWzBdLnRvVXBwZXJDYXNlKCkrZS5zbGljZSgxKSldO3ZhciBlfSl9fSl9O2IucHJvdG90eXBlPXt2YWxpZGF0ZTpmdW5jdGlvbih0KXsxPD1hcmd1bWVudHMubGVuZ3RoJiYhaC5pc1BsYWluT2JqZWN0KHQpJiYoZC53YXJuT25jZShcIkNhbGxpbmcgdmFsaWRhdGUgb24gYSBwYXJzbGV5IGZpZWxkIHdpdGhvdXQgcGFzc2luZyBhcmd1bWVudHMgYXMgYW4gb2JqZWN0IGlzIGRlcHJlY2F0ZWQuXCIpLHQ9e29wdGlvbnM6dH0pO3ZhciBlPXRoaXMud2hlblZhbGlkYXRlKHQpO2lmKCFlKXJldHVybiEwO3N3aXRjaChlLnN0YXRlKCkpe2Nhc2VcInBlbmRpbmdcIjpyZXR1cm4gbnVsbDtjYXNlXCJyZXNvbHZlZFwiOnJldHVybiEwO2Nhc2VcInJlamVjdGVkXCI6cmV0dXJuIHRoaXMudmFsaWRhdGlvblJlc3VsdH19LHdoZW5WYWxpZGF0ZTpmdW5jdGlvbigpe3ZhciB0LGU9dGhpcyxpPTA8YXJndW1lbnRzLmxlbmd0aCYmdm9pZCAwIT09YXJndW1lbnRzWzBdP2FyZ3VtZW50c1swXTp7fSxuPWkuZm9yY2Uscj1pLmdyb3VwO2lmKHRoaXMucmVmcmVzaCgpLCFyfHx0aGlzLl9pc0luR3JvdXAocikpcmV0dXJuIHRoaXMudmFsdWU9dGhpcy5nZXRWYWx1ZSgpLHRoaXMuX3RyaWdnZXIoXCJ2YWxpZGF0ZVwiKSwodD10aGlzLndoZW5WYWxpZCh7Zm9yY2U6bix2YWx1ZTp0aGlzLnZhbHVlLF9yZWZyZXNoZWQ6ITB9KS5hbHdheXMoZnVuY3Rpb24oKXtlLl9yZWZsb3dVSSgpfSkuZG9uZShmdW5jdGlvbigpe2UuX3RyaWdnZXIoXCJzdWNjZXNzXCIpfSkuZmFpbChmdW5jdGlvbigpe2UuX3RyaWdnZXIoXCJlcnJvclwiKX0pLmFsd2F5cyhmdW5jdGlvbigpe2UuX3RyaWdnZXIoXCJ2YWxpZGF0ZWRcIil9KSkucGlwZS5hcHBseSh0LHUodGhpcy5fcGlwZUFjY29yZGluZ1RvVmFsaWRhdGlvblJlc3VsdCgpKSl9LGhhc0NvbnN0cmFpbnRzOmZ1bmN0aW9uKCl7cmV0dXJuIDAhPT10aGlzLmNvbnN0cmFpbnRzLmxlbmd0aH0sbmVlZHNWYWxpZGF0aW9uOmZ1bmN0aW9uKHQpe3JldHVybiB2b2lkIDA9PT10JiYodD10aGlzLmdldFZhbHVlKCkpLCEoIXQubGVuZ3RoJiYhdGhpcy5faXNSZXF1aXJlZCgpJiZ2b2lkIDA9PT10aGlzLm9wdGlvbnMudmFsaWRhdGVJZkVtcHR5KX0sX2lzSW5Hcm91cDpmdW5jdGlvbih0KXtyZXR1cm4gQXJyYXkuaXNBcnJheSh0aGlzLm9wdGlvbnMuZ3JvdXApPy0xIT09aC5pbkFycmF5KHQsdGhpcy5vcHRpb25zLmdyb3VwKTp0aGlzLm9wdGlvbnMuZ3JvdXA9PT10fSxpc1ZhbGlkOmZ1bmN0aW9uKHQpe2lmKDE8PWFyZ3VtZW50cy5sZW5ndGgmJiFoLmlzUGxhaW5PYmplY3QodCkpe2Qud2Fybk9uY2UoXCJDYWxsaW5nIGlzVmFsaWQgb24gYSBwYXJzbGV5IGZpZWxkIHdpdGhvdXQgcGFzc2luZyBhcmd1bWVudHMgYXMgYW4gb2JqZWN0IGlzIGRlcHJlY2F0ZWQuXCIpO3ZhciBlPUFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cyk7dD17Zm9yY2U6ZVswXSx2YWx1ZTplWzFdfX12YXIgaT10aGlzLndoZW5WYWxpZCh0KTtyZXR1cm4haXx8RltpLnN0YXRlKCldfSx3aGVuVmFsaWQ6ZnVuY3Rpb24oKXt2YXIgbj10aGlzLHQ9MDxhcmd1bWVudHMubGVuZ3RoJiZ2b2lkIDAhPT1hcmd1bWVudHNbMF0/YXJndW1lbnRzWzBdOnt9LGU9dC5mb3JjZSxpPXZvaWQgMCE9PWUmJmUscj10LnZhbHVlLHM9dC5ncm91cDtpZih0Ll9yZWZyZXNoZWR8fHRoaXMucmVmcmVzaCgpLCFzfHx0aGlzLl9pc0luR3JvdXAocykpe2lmKHRoaXMudmFsaWRhdGlvblJlc3VsdD0hMCwhdGhpcy5oYXNDb25zdHJhaW50cygpKXJldHVybiBoLndoZW4oKTtpZihudWxsPT1yJiYocj10aGlzLmdldFZhbHVlKCkpLCF0aGlzLm5lZWRzVmFsaWRhdGlvbihyKSYmITAhPT1pKXJldHVybiBoLndoZW4oKTt2YXIgYT10aGlzLl9nZXRHcm91cGVkQ29uc3RyYWludHMoKSxvPVtdO3JldHVybiBoLmVhY2goYSxmdW5jdGlvbih0LGUpe3ZhciBpPWQuYWxsKGgubWFwKGUsZnVuY3Rpb24odCl7cmV0dXJuIG4uX3ZhbGlkYXRlQ29uc3RyYWludChyLHQpfSkpO2lmKG8ucHVzaChpKSxcInJlamVjdGVkXCI9PT1pLnN0YXRlKCkpcmV0dXJuITF9KSxkLmFsbChvKX19LF92YWxpZGF0ZUNvbnN0cmFpbnQ6ZnVuY3Rpb24odCxlKXt2YXIgaT10aGlzLG49ZS52YWxpZGF0ZSh0LHRoaXMpO3JldHVybiExPT09biYmKG49aC5EZWZlcnJlZCgpLnJlamVjdCgpKSxkLmFsbChbbl0pLmZhaWwoZnVuY3Rpb24odCl7aS52YWxpZGF0aW9uUmVzdWx0IGluc3RhbmNlb2YgQXJyYXl8fChpLnZhbGlkYXRpb25SZXN1bHQ9W10pLGkudmFsaWRhdGlvblJlc3VsdC5wdXNoKHthc3NlcnQ6ZSxlcnJvck1lc3NhZ2U6XCJzdHJpbmdcIj09dHlwZW9mIHQmJnR9KX0pfSxnZXRWYWx1ZTpmdW5jdGlvbigpe3ZhciB0O3JldHVybiBudWxsPT0odD1cImZ1bmN0aW9uXCI9PXR5cGVvZiB0aGlzLm9wdGlvbnMudmFsdWU/dGhpcy5vcHRpb25zLnZhbHVlKHRoaXMpOnZvaWQgMCE9PXRoaXMub3B0aW9ucy52YWx1ZT90aGlzLm9wdGlvbnMudmFsdWU6dGhpcy4kZWxlbWVudC52YWwoKSk/XCJcIjp0aGlzLl9oYW5kbGVXaGl0ZXNwYWNlKHQpfSxyZXNldDpmdW5jdGlvbigpe3JldHVybiB0aGlzLl9yZXNldFVJKCksdGhpcy5fdHJpZ2dlcihcInJlc2V0XCIpfSxkZXN0cm95OmZ1bmN0aW9uKCl7dGhpcy5fZGVzdHJveVVJKCksdGhpcy4kZWxlbWVudC5yZW1vdmVEYXRhKFwiUGFyc2xleVwiKSx0aGlzLiRlbGVtZW50LnJlbW92ZURhdGEoXCJGaWVsZE11bHRpcGxlXCIpLHRoaXMuX3RyaWdnZXIoXCJkZXN0cm95XCIpfSxyZWZyZXNoOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuX3JlZnJlc2hDb25zdHJhaW50cygpLHRoaXN9LF9yZWZyZXNoQ29uc3RyYWludHM6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5hY3R1YWxpemVPcHRpb25zKCkuX2JpbmRDb25zdHJhaW50cygpfSxyZWZyZXNoQ29uc3RyYWludHM6ZnVuY3Rpb24oKXtyZXR1cm4gZC53YXJuT25jZShcIlBhcnNsZXkncyByZWZyZXNoQ29uc3RyYWludHMgaXMgZGVwcmVjYXRlZC4gUGxlYXNlIHVzZSByZWZyZXNoXCIpLHRoaXMucmVmcmVzaCgpfSxhZGRDb25zdHJhaW50OmZ1bmN0aW9uKHQsZSxpLG4pe2lmKHdpbmRvdy5QYXJzbGV5Ll92YWxpZGF0b3JSZWdpc3RyeS52YWxpZGF0b3JzW3RdKXt2YXIgcj1uZXcgdyh0aGlzLHQsZSxpLG4pO1widW5kZWZpbmVkXCIhPT10aGlzLmNvbnN0cmFpbnRzQnlOYW1lW3IubmFtZV0mJnRoaXMucmVtb3ZlQ29uc3RyYWludChyLm5hbWUpLHRoaXMuY29uc3RyYWludHMucHVzaChyKSx0aGlzLmNvbnN0cmFpbnRzQnlOYW1lW3IubmFtZV09cn1yZXR1cm4gdGhpc30scmVtb3ZlQ29uc3RyYWludDpmdW5jdGlvbih0KXtmb3IodmFyIGU9MDtlPHRoaXMuY29uc3RyYWludHMubGVuZ3RoO2UrKylpZih0PT09dGhpcy5jb25zdHJhaW50c1tlXS5uYW1lKXt0aGlzLmNvbnN0cmFpbnRzLnNwbGljZShlLDEpO2JyZWFrfXJldHVybiBkZWxldGUgdGhpcy5jb25zdHJhaW50c0J5TmFtZVt0XSx0aGlzfSx1cGRhdGVDb25zdHJhaW50OmZ1bmN0aW9uKHQsZSxpKXtyZXR1cm4gdGhpcy5yZW1vdmVDb25zdHJhaW50KHQpLmFkZENvbnN0cmFpbnQodCxlLGkpfSxfYmluZENvbnN0cmFpbnRzOmZ1bmN0aW9uKCl7Zm9yKHZhciB0PVtdLGU9e30saT0wO2k8dGhpcy5jb25zdHJhaW50cy5sZW5ndGg7aSsrKSExPT09dGhpcy5jb25zdHJhaW50c1tpXS5pc0RvbUNvbnN0cmFpbnQmJih0LnB1c2godGhpcy5jb25zdHJhaW50c1tpXSksZVt0aGlzLmNvbnN0cmFpbnRzW2ldLm5hbWVdPXRoaXMuY29uc3RyYWludHNbaV0pO2Zvcih2YXIgbiBpbiB0aGlzLmNvbnN0cmFpbnRzPXQsdGhpcy5jb25zdHJhaW50c0J5TmFtZT1lLHRoaXMub3B0aW9ucyl0aGlzLmFkZENvbnN0cmFpbnQobix0aGlzLm9wdGlvbnNbbl0sdm9pZCAwLCEwKTtyZXR1cm4gdGhpcy5fYmluZEh0bWw1Q29uc3RyYWludHMoKX0sX2JpbmRIdG1sNUNvbnN0cmFpbnRzOmZ1bmN0aW9uKCl7bnVsbCE9PXRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJyZXF1aXJlZFwiKSYmdGhpcy5hZGRDb25zdHJhaW50KFwicmVxdWlyZWRcIiwhMCx2b2lkIDAsITApLG51bGwhPT10aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKFwicGF0dGVyblwiKSYmdGhpcy5hZGRDb25zdHJhaW50KFwicGF0dGVyblwiLHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJwYXR0ZXJuXCIpLHZvaWQgMCwhMCk7dmFyIHQ9dGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcIm1pblwiKSxlPXRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJtYXhcIik7bnVsbCE9PXQmJm51bGwhPT1lP3RoaXMuYWRkQ29uc3RyYWludChcInJhbmdlXCIsW3QsZV0sdm9pZCAwLCEwKTpudWxsIT09dD90aGlzLmFkZENvbnN0cmFpbnQoXCJtaW5cIix0LHZvaWQgMCwhMCk6bnVsbCE9PWUmJnRoaXMuYWRkQ29uc3RyYWludChcIm1heFwiLGUsdm9pZCAwLCEwKSxudWxsIT09dGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcIm1pbmxlbmd0aFwiKSYmbnVsbCE9PXRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJtYXhsZW5ndGhcIik/dGhpcy5hZGRDb25zdHJhaW50KFwibGVuZ3RoXCIsW3RoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJtaW5sZW5ndGhcIiksdGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcIm1heGxlbmd0aFwiKV0sdm9pZCAwLCEwKTpudWxsIT09dGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcIm1pbmxlbmd0aFwiKT90aGlzLmFkZENvbnN0cmFpbnQoXCJtaW5sZW5ndGhcIix0aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKFwibWlubGVuZ3RoXCIpLHZvaWQgMCwhMCk6bnVsbCE9PXRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJtYXhsZW5ndGhcIikmJnRoaXMuYWRkQ29uc3RyYWludChcIm1heGxlbmd0aFwiLHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJtYXhsZW5ndGhcIiksdm9pZCAwLCEwKTt2YXIgaT1kLmdldFR5cGUodGhpcy5lbGVtZW50KTtyZXR1cm5cIm51bWJlclwiPT09aT90aGlzLmFkZENvbnN0cmFpbnQoXCJ0eXBlXCIsW1wibnVtYmVyXCIse3N0ZXA6dGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcInN0ZXBcIil8fFwiMVwiLGJhc2U6dHx8dGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcInZhbHVlXCIpfV0sdm9pZCAwLCEwKTovXihlbWFpbHx1cmx8cmFuZ2V8ZGF0ZSkkL2kudGVzdChpKT90aGlzLmFkZENvbnN0cmFpbnQoXCJ0eXBlXCIsaSx2b2lkIDAsITApOnRoaXN9LF9pc1JlcXVpcmVkOmZ1bmN0aW9uKCl7cmV0dXJuIHZvaWQgMCE9PXRoaXMuY29uc3RyYWludHNCeU5hbWUucmVxdWlyZWQmJiExIT09dGhpcy5jb25zdHJhaW50c0J5TmFtZS5yZXF1aXJlZC5yZXF1aXJlbWVudHN9LF90cmlnZ2VyOmZ1bmN0aW9uKHQpe3JldHVybiB0aGlzLnRyaWdnZXIoXCJmaWVsZDpcIit0KX0sX2hhbmRsZVdoaXRlc3BhY2U6ZnVuY3Rpb24odCl7cmV0dXJuITA9PT10aGlzLm9wdGlvbnMudHJpbVZhbHVlJiZkLndhcm5PbmNlKCdkYXRhLXBhcnNsZXktdHJpbS12YWx1ZT1cInRydWVcIiBpcyBkZXByZWNhdGVkLCBwbGVhc2UgdXNlIGRhdGEtcGFyc2xleS13aGl0ZXNwYWNlPVwidHJpbVwiJyksXCJzcXVpc2hcIj09PXRoaXMub3B0aW9ucy53aGl0ZXNwYWNlJiYodD10LnJlcGxhY2UoL1xcc3syLH0vZyxcIiBcIikpLFwidHJpbVwiIT09dGhpcy5vcHRpb25zLndoaXRlc3BhY2UmJlwic3F1aXNoXCIhPT10aGlzLm9wdGlvbnMud2hpdGVzcGFjZSYmITAhPT10aGlzLm9wdGlvbnMudHJpbVZhbHVlfHwodD1kLnRyaW1TdHJpbmcodCkpLHR9LF9pc0RhdGVJbnB1dDpmdW5jdGlvbigpe3ZhciB0PXRoaXMuY29uc3RyYWludHNCeU5hbWUudHlwZTtyZXR1cm4gdCYmXCJkYXRlXCI9PT10LnJlcXVpcmVtZW50c30sX2dldEdyb3VwZWRDb25zdHJhaW50czpmdW5jdGlvbigpe2lmKCExPT09dGhpcy5vcHRpb25zLnByaW9yaXR5RW5hYmxlZClyZXR1cm5bdGhpcy5jb25zdHJhaW50c107Zm9yKHZhciB0PVtdLGU9e30saT0wO2k8dGhpcy5jb25zdHJhaW50cy5sZW5ndGg7aSsrKXt2YXIgbj10aGlzLmNvbnN0cmFpbnRzW2ldLnByaW9yaXR5O2Vbbl18fHQucHVzaChlW25dPVtdKSxlW25dLnB1c2godGhpcy5jb25zdHJhaW50c1tpXSl9cmV0dXJuIHQuc29ydChmdW5jdGlvbih0LGUpe3JldHVybiBlWzBdLnByaW9yaXR5LXRbMF0ucHJpb3JpdHl9KSx0fX07dmFyIEM9ZnVuY3Rpb24oKXt0aGlzLl9fY2xhc3NfXz1cIkZpZWxkTXVsdGlwbGVcIn07Qy5wcm90b3R5cGU9e2FkZEVsZW1lbnQ6ZnVuY3Rpb24odCl7cmV0dXJuIHRoaXMuJGVsZW1lbnRzLnB1c2godCksdGhpc30sX3JlZnJlc2hDb25zdHJhaW50czpmdW5jdGlvbigpe3ZhciB0O2lmKHRoaXMuY29uc3RyYWludHM9W10sXCJTRUxFQ1RcIj09PXRoaXMuZWxlbWVudC5ub2RlTmFtZSlyZXR1cm4gdGhpcy5hY3R1YWxpemVPcHRpb25zKCkuX2JpbmRDb25zdHJhaW50cygpLHRoaXM7Zm9yKHZhciBlPTA7ZTx0aGlzLiRlbGVtZW50cy5sZW5ndGg7ZSsrKWlmKGgoXCJodG1sXCIpLmhhcyh0aGlzLiRlbGVtZW50c1tlXSkubGVuZ3RoKXt0PXRoaXMuJGVsZW1lbnRzW2VdLmRhdGEoXCJGaWVsZE11bHRpcGxlXCIpLl9yZWZyZXNoQ29uc3RyYWludHMoKS5jb25zdHJhaW50cztmb3IodmFyIGk9MDtpPHQubGVuZ3RoO2krKyl0aGlzLmFkZENvbnN0cmFpbnQodFtpXS5uYW1lLHRbaV0ucmVxdWlyZW1lbnRzLHRbaV0ucHJpb3JpdHksdFtpXS5pc0RvbUNvbnN0cmFpbnQpfWVsc2UgdGhpcy4kZWxlbWVudHMuc3BsaWNlKGUsMSk7cmV0dXJuIHRoaXN9LGdldFZhbHVlOmZ1bmN0aW9uKCl7aWYoXCJmdW5jdGlvblwiPT10eXBlb2YgdGhpcy5vcHRpb25zLnZhbHVlKXJldHVybiB0aGlzLm9wdGlvbnMudmFsdWUodGhpcyk7aWYodm9pZCAwIT09dGhpcy5vcHRpb25zLnZhbHVlKXJldHVybiB0aGlzLm9wdGlvbnMudmFsdWU7aWYoXCJJTlBVVFwiPT09dGhpcy5lbGVtZW50Lm5vZGVOYW1lKXt2YXIgdD1kLmdldFR5cGUodGhpcy5lbGVtZW50KTtpZihcInJhZGlvXCI9PT10KXJldHVybiB0aGlzLl9maW5kUmVsYXRlZCgpLmZpbHRlcihcIjpjaGVja2VkXCIpLnZhbCgpfHxcIlwiO2lmKFwiY2hlY2tib3hcIj09PXQpe3ZhciBlPVtdO3JldHVybiB0aGlzLl9maW5kUmVsYXRlZCgpLmZpbHRlcihcIjpjaGVja2VkXCIpLmVhY2goZnVuY3Rpb24oKXtlLnB1c2goaCh0aGlzKS52YWwoKSl9KSxlfX1yZXR1cm5cIlNFTEVDVFwiPT09dGhpcy5lbGVtZW50Lm5vZGVOYW1lJiZudWxsPT09dGhpcy4kZWxlbWVudC52YWwoKT9bXTp0aGlzLiRlbGVtZW50LnZhbCgpfSxfaW5pdDpmdW5jdGlvbigpe3JldHVybiB0aGlzLiRlbGVtZW50cz1bdGhpcy4kZWxlbWVudF0sdGhpc319O3ZhciBBPWZ1bmN0aW9uKHQsZSxpKXt0aGlzLmVsZW1lbnQ9dCx0aGlzLiRlbGVtZW50PWgodCk7dmFyIG49dGhpcy4kZWxlbWVudC5kYXRhKFwiUGFyc2xleVwiKTtpZihuKXJldHVybiB2b2lkIDAhPT1pJiZuLnBhcmVudD09PXdpbmRvdy5QYXJzbGV5JiYobi5wYXJlbnQ9aSxuLl9yZXNldE9wdGlvbnMobi5vcHRpb25zKSksXCJvYmplY3RcIj09PXIoZSkmJm8obi5vcHRpb25zLGUpLG47aWYoIXRoaXMuJGVsZW1lbnQubGVuZ3RoKXRocm93IG5ldyBFcnJvcihcIllvdSBtdXN0IGJpbmQgUGFyc2xleSBvbiBhbiBleGlzdGluZyBlbGVtZW50LlwiKTtpZih2b2lkIDAhPT1pJiZcIkZvcm1cIiE9PWkuX19jbGFzc19fKXRocm93IG5ldyBFcnJvcihcIlBhcmVudCBpbnN0YW5jZSBtdXN0IGJlIGEgRm9ybSBpbnN0YW5jZVwiKTtyZXR1cm4gdGhpcy5wYXJlbnQ9aXx8d2luZG93LlBhcnNsZXksdGhpcy5pbml0KGUpfTtBLnByb3RvdHlwZT17aW5pdDpmdW5jdGlvbih0KXtyZXR1cm4gdGhpcy5fX2NsYXNzX189XCJQYXJzbGV5XCIsdGhpcy5fX3ZlcnNpb25fXz1cIjIuOS4wXCIsdGhpcy5fX2lkX189ZC5nZW5lcmF0ZUlEKCksdGhpcy5fcmVzZXRPcHRpb25zKHQpLFwiRk9STVwiPT09dGhpcy5lbGVtZW50Lm5vZGVOYW1lfHxkLmNoZWNrQXR0cih0aGlzLmVsZW1lbnQsdGhpcy5vcHRpb25zLm5hbWVzcGFjZSxcInZhbGlkYXRlXCIpJiYhdGhpcy4kZWxlbWVudC5pcyh0aGlzLm9wdGlvbnMuaW5wdXRzKT90aGlzLmJpbmQoXCJwYXJzbGV5Rm9ybVwiKTp0aGlzLmlzTXVsdGlwbGUoKT90aGlzLmhhbmRsZU11bHRpcGxlKCk6dGhpcy5iaW5kKFwicGFyc2xleUZpZWxkXCIpfSxpc011bHRpcGxlOmZ1bmN0aW9uKCl7dmFyIHQ9ZC5nZXRUeXBlKHRoaXMuZWxlbWVudCk7cmV0dXJuXCJyYWRpb1wiPT09dHx8XCJjaGVja2JveFwiPT09dHx8XCJTRUxFQ1RcIj09PXRoaXMuZWxlbWVudC5ub2RlTmFtZSYmbnVsbCE9PXRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJtdWx0aXBsZVwiKX0saGFuZGxlTXVsdGlwbGU6ZnVuY3Rpb24oKXt2YXIgdCxlLG49dGhpcztpZih0aGlzLm9wdGlvbnMubXVsdGlwbGU9dGhpcy5vcHRpb25zLm11bHRpcGxlfHwodD10aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKFwibmFtZVwiKSl8fHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJpZFwiKSxcIlNFTEVDVFwiPT09dGhpcy5lbGVtZW50Lm5vZGVOYW1lJiZudWxsIT09dGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcIm11bHRpcGxlXCIpKXJldHVybiB0aGlzLm9wdGlvbnMubXVsdGlwbGU9dGhpcy5vcHRpb25zLm11bHRpcGxlfHx0aGlzLl9faWRfXyx0aGlzLmJpbmQoXCJwYXJzbGV5RmllbGRNdWx0aXBsZVwiKTtpZighdGhpcy5vcHRpb25zLm11bHRpcGxlKXJldHVybiBkLndhcm4oXCJUbyBiZSBib3VuZCBieSBQYXJzbGV5LCBhIHJhZGlvLCBhIGNoZWNrYm94IGFuZCBhIG11bHRpcGxlIHNlbGVjdCBpbnB1dCBtdXN0IGhhdmUgZWl0aGVyIGEgbmFtZSBvciBhIG11bHRpcGxlIG9wdGlvbi5cIix0aGlzLiRlbGVtZW50KSx0aGlzO3RoaXMub3B0aW9ucy5tdWx0aXBsZT10aGlzLm9wdGlvbnMubXVsdGlwbGUucmVwbGFjZSgvKDp8XFwufFxcW3xcXF18XFx7fFxcfXxcXCQpL2csXCJcIiksdCYmaCgnaW5wdXRbbmFtZT1cIicrdCsnXCJdJykuZWFjaChmdW5jdGlvbih0LGUpe3ZhciBpPWQuZ2V0VHlwZShlKTtcInJhZGlvXCIhPT1pJiZcImNoZWNrYm94XCIhPT1pfHxlLnNldEF0dHJpYnV0ZShuLm9wdGlvbnMubmFtZXNwYWNlK1wibXVsdGlwbGVcIixuLm9wdGlvbnMubXVsdGlwbGUpfSk7Zm9yKHZhciBpPXRoaXMuX2ZpbmRSZWxhdGVkKCkscj0wO3I8aS5sZW5ndGg7cisrKWlmKHZvaWQgMCE9PShlPWgoaS5nZXQocikpLmRhdGEoXCJQYXJzbGV5XCIpKSl7dGhpcy4kZWxlbWVudC5kYXRhKFwiRmllbGRNdWx0aXBsZVwiKXx8ZS5hZGRFbGVtZW50KHRoaXMuJGVsZW1lbnQpO2JyZWFrfXJldHVybiB0aGlzLmJpbmQoXCJwYXJzbGV5RmllbGRcIiwhMCksZXx8dGhpcy5iaW5kKFwicGFyc2xleUZpZWxkTXVsdGlwbGVcIil9LGJpbmQ6ZnVuY3Rpb24odCxlKXt2YXIgaTtzd2l0Y2godCl7Y2FzZVwicGFyc2xleUZvcm1cIjppPWguZXh0ZW5kKG5ldyB5KHRoaXMuZWxlbWVudCx0aGlzLmRvbU9wdGlvbnMsdGhpcy5vcHRpb25zKSxuZXcgcyx3aW5kb3cuUGFyc2xleUV4dGVuZCkuX2JpbmRGaWVsZHMoKTticmVhaztjYXNlXCJwYXJzbGV5RmllbGRcIjppPWguZXh0ZW5kKG5ldyBiKHRoaXMuZWxlbWVudCx0aGlzLmRvbU9wdGlvbnMsdGhpcy5vcHRpb25zLHRoaXMucGFyZW50KSxuZXcgcyx3aW5kb3cuUGFyc2xleUV4dGVuZCk7YnJlYWs7Y2FzZVwicGFyc2xleUZpZWxkTXVsdGlwbGVcIjppPWguZXh0ZW5kKG5ldyBiKHRoaXMuZWxlbWVudCx0aGlzLmRvbU9wdGlvbnMsdGhpcy5vcHRpb25zLHRoaXMucGFyZW50KSxuZXcgQyxuZXcgcyx3aW5kb3cuUGFyc2xleUV4dGVuZCkuX2luaXQoKTticmVhaztkZWZhdWx0OnRocm93IG5ldyBFcnJvcih0K1wiaXMgbm90IGEgc3VwcG9ydGVkIFBhcnNsZXkgdHlwZVwiKX1yZXR1cm4gdGhpcy5vcHRpb25zLm11bHRpcGxlJiZkLnNldEF0dHIodGhpcy5lbGVtZW50LHRoaXMub3B0aW9ucy5uYW1lc3BhY2UsXCJtdWx0aXBsZVwiLHRoaXMub3B0aW9ucy5tdWx0aXBsZSksdm9pZCAwIT09ZT90aGlzLiRlbGVtZW50LmRhdGEoXCJGaWVsZE11bHRpcGxlXCIsaSk6KHRoaXMuJGVsZW1lbnQuZGF0YShcIlBhcnNsZXlcIixpKSxpLl9hY3R1YWxpemVUcmlnZ2VycygpLGkuX3RyaWdnZXIoXCJpbml0XCIpKSxpfX07dmFyIEU9aC5mbi5qcXVlcnkuc3BsaXQoXCIuXCIpO2lmKHBhcnNlSW50KEVbMF0pPD0xJiZwYXJzZUludChFWzFdKTw4KXRocm93XCJUaGUgbG9hZGVkIHZlcnNpb24gb2YgalF1ZXJ5IGlzIHRvbyBvbGQuIFBsZWFzZSB1cGdyYWRlIHRvIDEuOC54IG9yIGJldHRlci5cIjtFLmZvckVhY2h8fGQud2FybihcIlBhcnNsZXkgcmVxdWlyZXMgRVM1IHRvIHJ1biBwcm9wZXJseS4gUGxlYXNlIGluY2x1ZGUgaHR0cHM6Ly9naXRodWIuY29tL2VzLXNoaW1zL2VzNS1zaGltXCIpO3ZhciB4PW8obmV3IHMse2VsZW1lbnQ6ZG9jdW1lbnQsJGVsZW1lbnQ6aChkb2N1bWVudCksYWN0dWFsaXplT3B0aW9uczpudWxsLF9yZXNldE9wdGlvbnM6bnVsbCxGYWN0b3J5OkEsdmVyc2lvbjpcIjIuOS4wXCJ9KTtvKGIucHJvdG90eXBlLHYuRmllbGQscy5wcm90b3R5cGUpLG8oeS5wcm90b3R5cGUsdi5Gb3JtLHMucHJvdG90eXBlKSxvKEEucHJvdG90eXBlLHMucHJvdG90eXBlKSxoLmZuLnBhcnNsZXk9aC5mbi5wc2x5PWZ1bmN0aW9uKHQpe2lmKDE8dGhpcy5sZW5ndGgpe3ZhciBlPVtdO3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKXtlLnB1c2goaCh0aGlzKS5wYXJzbGV5KHQpKX0pLGV9aWYoMCE9dGhpcy5sZW5ndGgpcmV0dXJuIG5ldyBBKHRoaXNbMF0sdCl9LHZvaWQgMD09PXdpbmRvdy5QYXJzbGV5RXh0ZW5kJiYod2luZG93LlBhcnNsZXlFeHRlbmQ9e30pLHgub3B0aW9ucz1vKGQub2JqZWN0Q3JlYXRlKG4pLHdpbmRvdy5QYXJzbGV5Q29uZmlnKSx3aW5kb3cuUGFyc2xleUNvbmZpZz14Lm9wdGlvbnMsd2luZG93LlBhcnNsZXk9d2luZG93LnBzbHk9eCx4LlV0aWxzPWQsd2luZG93LlBhcnNsZXlVdGlscz17fSxoLmVhY2goZCxmdW5jdGlvbih0LGUpe1wiZnVuY3Rpb25cIj09dHlwZW9mIGUmJih3aW5kb3cuUGFyc2xleVV0aWxzW3RdPWZ1bmN0aW9uKCl7cmV0dXJuIGQud2Fybk9uY2UoXCJBY2Nlc3NpbmcgYHdpbmRvdy5QYXJzbGV5VXRpbHNgIGlzIGRlcHJlY2F0ZWQuIFVzZSBgd2luZG93LlBhcnNsZXkuVXRpbHNgIGluc3RlYWQuXCIpLGRbdF0uYXBwbHkoZCxhcmd1bWVudHMpfSl9KTt2YXIgJD13aW5kb3cuUGFyc2xleS5fdmFsaWRhdG9yUmVnaXN0cnk9bmV3IGEod2luZG93LlBhcnNsZXlDb25maWcudmFsaWRhdG9ycyx3aW5kb3cuUGFyc2xleUNvbmZpZy5pMThuKTt3aW5kb3cuUGFyc2xleVZhbGlkYXRvcj17fSxoLmVhY2goXCJzZXRMb2NhbGUgYWRkQ2F0YWxvZyBhZGRNZXNzYWdlIGFkZE1lc3NhZ2VzIGdldEVycm9yTWVzc2FnZSBmb3JtYXRNZXNzYWdlIGFkZFZhbGlkYXRvciB1cGRhdGVWYWxpZGF0b3IgcmVtb3ZlVmFsaWRhdG9yIGhhc1ZhbGlkYXRvclwiLnNwbGl0KFwiIFwiKSxmdW5jdGlvbih0LGUpe3dpbmRvdy5QYXJzbGV5W2VdPWZ1bmN0aW9uKCl7cmV0dXJuICRbZV0uYXBwbHkoJCxhcmd1bWVudHMpfSx3aW5kb3cuUGFyc2xleVZhbGlkYXRvcltlXT1mdW5jdGlvbigpe3ZhciB0O3JldHVybiBkLndhcm5PbmNlKFwiQWNjZXNzaW5nIHRoZSBtZXRob2QgJ1wiLmNvbmNhdChlLFwiJyB0aHJvdWdoIFZhbGlkYXRvciBpcyBkZXByZWNhdGVkLiBTaW1wbHkgY2FsbCAnd2luZG93LlBhcnNsZXkuXCIpLmNvbmNhdChlLFwiKC4uLiknXCIpKSwodD13aW5kb3cuUGFyc2xleSlbZV0uYXBwbHkodCxhcmd1bWVudHMpfX0pLHdpbmRvdy5QYXJzbGV5LlVJPXYsd2luZG93LlBhcnNsZXlVST17cmVtb3ZlRXJyb3I6ZnVuY3Rpb24odCxlLGkpe3ZhciBuPSEwIT09aTtyZXR1cm4gZC53YXJuT25jZShcIkFjY2Vzc2luZyBVSSBpcyBkZXByZWNhdGVkLiBDYWxsICdyZW1vdmVFcnJvcicgb24gdGhlIGluc3RhbmNlIGRpcmVjdGx5LiBQbGVhc2UgY29tbWVudCBpbiBpc3N1ZSAxMDczIGFzIHRvIHlvdXIgbmVlZCB0byBjYWxsIHRoaXMgbWV0aG9kLlwiKSx0LnJlbW92ZUVycm9yKGUse3VwZGF0ZUNsYXNzOm59KX0sZ2V0RXJyb3JzTWVzc2FnZXM6ZnVuY3Rpb24odCl7cmV0dXJuIGQud2Fybk9uY2UoXCJBY2Nlc3NpbmcgVUkgaXMgZGVwcmVjYXRlZC4gQ2FsbCAnZ2V0RXJyb3JzTWVzc2FnZXMnIG9uIHRoZSBpbnN0YW5jZSBkaXJlY3RseS5cIiksdC5nZXRFcnJvcnNNZXNzYWdlcygpfX0saC5lYWNoKFwiYWRkRXJyb3IgdXBkYXRlRXJyb3JcIi5zcGxpdChcIiBcIiksZnVuY3Rpb24odCxhKXt3aW5kb3cuUGFyc2xleVVJW2FdPWZ1bmN0aW9uKHQsZSxpLG4scil7dmFyIHM9ITAhPT1yO3JldHVybiBkLndhcm5PbmNlKFwiQWNjZXNzaW5nIFVJIGlzIGRlcHJlY2F0ZWQuIENhbGwgJ1wiLmNvbmNhdChhLFwiJyBvbiB0aGUgaW5zdGFuY2UgZGlyZWN0bHkuIFBsZWFzZSBjb21tZW50IGluIGlzc3VlIDEwNzMgYXMgdG8geW91ciBuZWVkIHRvIGNhbGwgdGhpcyBtZXRob2QuXCIpKSx0W2FdKGUse21lc3NhZ2U6aSxhc3NlcnQ6bix1cGRhdGVDbGFzczpzfSl9fSksITEhPT13aW5kb3cuUGFyc2xleUNvbmZpZy5hdXRvQmluZCYmaChmdW5jdGlvbigpe2goXCJbZGF0YS1wYXJzbGV5LXZhbGlkYXRlXVwiKS5sZW5ndGgmJmgoXCJbZGF0YS1wYXJzbGV5LXZhbGlkYXRlXVwiKS5wYXJzbGV5KCl9KTt2YXIgVj1oKHt9KSxQPWZ1bmN0aW9uKCl7ZC53YXJuT25jZShcIlBhcnNsZXkncyBwdWJzdWIgbW9kdWxlIGlzIGRlcHJlY2F0ZWQ7IHVzZSB0aGUgJ29uJyBhbmQgJ29mZicgbWV0aG9kcyBvbiBwYXJzbGV5IGluc3RhbmNlcyBvciB3aW5kb3cuUGFyc2xleVwiKX07ZnVuY3Rpb24gTyhlLGkpe3JldHVybiBlLnBhcnNsZXlBZGFwdGVkQ2FsbGJhY2t8fChlLnBhcnNsZXlBZGFwdGVkQ2FsbGJhY2s9ZnVuY3Rpb24oKXt2YXIgdD1BcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsMCk7dC51bnNoaWZ0KHRoaXMpLGUuYXBwbHkoaXx8Vix0KX0pLGUucGFyc2xleUFkYXB0ZWRDYWxsYmFja312YXIgVD1cInBhcnNsZXk6XCI7ZnVuY3Rpb24gTSh0KXtyZXR1cm4gMD09PXQubGFzdEluZGV4T2YoVCwwKT90LnN1YnN0cihULmxlbmd0aCk6dH1yZXR1cm4gaC5saXN0ZW49ZnVuY3Rpb24odCxlKXt2YXIgaTtpZihQKCksXCJvYmplY3RcIj09PXIoYXJndW1lbnRzWzFdKSYmXCJmdW5jdGlvblwiPT10eXBlb2YgYXJndW1lbnRzWzJdJiYoaT1hcmd1bWVudHNbMV0sZT1hcmd1bWVudHNbMl0pLFwiZnVuY3Rpb25cIiE9dHlwZW9mIGUpdGhyb3cgbmV3IEVycm9yKFwiV3JvbmcgcGFyYW1ldGVyc1wiKTt3aW5kb3cuUGFyc2xleS5vbihNKHQpLE8oZSxpKSl9LGgubGlzdGVuVG89ZnVuY3Rpb24odCxlLGkpe2lmKFAoKSwhKHQgaW5zdGFuY2VvZiBifHx0IGluc3RhbmNlb2YgeSkpdGhyb3cgbmV3IEVycm9yKFwiTXVzdCBnaXZlIFBhcnNsZXkgaW5zdGFuY2VcIik7aWYoXCJzdHJpbmdcIiE9dHlwZW9mIGV8fFwiZnVuY3Rpb25cIiE9dHlwZW9mIGkpdGhyb3cgbmV3IEVycm9yKFwiV3JvbmcgcGFyYW1ldGVyc1wiKTt0Lm9uKE0oZSksTyhpKSl9LGgudW5zdWJzY3JpYmU9ZnVuY3Rpb24odCxlKXtpZihQKCksXCJzdHJpbmdcIiE9dHlwZW9mIHR8fFwiZnVuY3Rpb25cIiE9dHlwZW9mIGUpdGhyb3cgbmV3IEVycm9yKFwiV3JvbmcgYXJndW1lbnRzXCIpO3dpbmRvdy5QYXJzbGV5Lm9mZihNKHQpLGUucGFyc2xleUFkYXB0ZWRDYWxsYmFjayl9LGgudW5zdWJzY3JpYmVUbz1mdW5jdGlvbih0LGUpe2lmKFAoKSwhKHQgaW5zdGFuY2VvZiBifHx0IGluc3RhbmNlb2YgeSkpdGhyb3cgbmV3IEVycm9yKFwiTXVzdCBnaXZlIFBhcnNsZXkgaW5zdGFuY2VcIik7dC5vZmYoTShlKSl9LGgudW5zdWJzY3JpYmVBbGw9ZnVuY3Rpb24oZSl7UCgpLHdpbmRvdy5QYXJzbGV5Lm9mZihNKGUpKSxoKFwiZm9ybSxpbnB1dCx0ZXh0YXJlYSxzZWxlY3RcIikuZWFjaChmdW5jdGlvbigpe3ZhciB0PWgodGhpcykuZGF0YShcIlBhcnNsZXlcIik7dCYmdC5vZmYoTShlKSl9KX0saC5lbWl0PWZ1bmN0aW9uKHQsZSl7dmFyIGk7UCgpO3ZhciBuPWUgaW5zdGFuY2VvZiBifHxlIGluc3RhbmNlb2YgeSxyPUFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cyxuPzI6MSk7ci51bnNoaWZ0KE0odCkpLG58fChlPXdpbmRvdy5QYXJzbGV5KSwoaT1lKS50cmlnZ2VyLmFwcGx5KGksdShyKSl9LGguZXh0ZW5kKCEwLHgse2FzeW5jVmFsaWRhdG9yczp7ZGVmYXVsdDp7Zm46ZnVuY3Rpb24odCl7cmV0dXJuIDIwMDw9dC5zdGF0dXMmJnQuc3RhdHVzPDMwMH0sdXJsOiExfSxyZXZlcnNlOntmbjpmdW5jdGlvbih0KXtyZXR1cm4gdC5zdGF0dXM8MjAwfHwzMDA8PXQuc3RhdHVzfSx1cmw6ITF9fSxhZGRBc3luY1ZhbGlkYXRvcjpmdW5jdGlvbih0LGUsaSxuKXtyZXR1cm4geC5hc3luY1ZhbGlkYXRvcnNbdF09e2ZuOmUsdXJsOml8fCExLG9wdGlvbnM6bnx8e319LHRoaXN9fSkseC5hZGRWYWxpZGF0b3IoXCJyZW1vdGVcIix7cmVxdWlyZW1lbnRUeXBlOntcIlwiOlwic3RyaW5nXCIsdmFsaWRhdG9yOlwic3RyaW5nXCIscmV2ZXJzZTpcImJvb2xlYW5cIixvcHRpb25zOlwib2JqZWN0XCJ9LHZhbGlkYXRlU3RyaW5nOmZ1bmN0aW9uKHQsZSxpLG4pe3ZhciByLHMsYT17fSxvPWkudmFsaWRhdG9yfHwoITA9PT1pLnJldmVyc2U/XCJyZXZlcnNlXCI6XCJkZWZhdWx0XCIpO2lmKHZvaWQgMD09PXguYXN5bmNWYWxpZGF0b3JzW29dKXRocm93IG5ldyBFcnJvcihcIkNhbGxpbmcgYW4gdW5kZWZpbmVkIGFzeW5jIHZhbGlkYXRvcjogYFwiK28rXCJgXCIpOy0xPChlPXguYXN5bmNWYWxpZGF0b3JzW29dLnVybHx8ZSkuaW5kZXhPZihcInt2YWx1ZX1cIik/ZT1lLnJlcGxhY2UoXCJ7dmFsdWV9XCIsZW5jb2RlVVJJQ29tcG9uZW50KHQpKTphW24uZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJuYW1lXCIpfHxuLmVsZW1lbnQuZ2V0QXR0cmlidXRlKFwiaWRcIildPXQ7dmFyIGw9aC5leHRlbmQoITAsaS5vcHRpb25zfHx7fSx4LmFzeW5jVmFsaWRhdG9yc1tvXS5vcHRpb25zKTtyPWguZXh0ZW5kKCEwLHt9LHt1cmw6ZSxkYXRhOmEsdHlwZTpcIkdFVFwifSxsKSxuLnRyaWdnZXIoXCJmaWVsZDphamF4b3B0aW9uc1wiLG4scikscz1oLnBhcmFtKHIpLHZvaWQgMD09PXguX3JlbW90ZUNhY2hlJiYoeC5fcmVtb3RlQ2FjaGU9e30pO3ZhciB1PXguX3JlbW90ZUNhY2hlW3NdPXguX3JlbW90ZUNhY2hlW3NdfHxoLmFqYXgociksZD1mdW5jdGlvbigpe3ZhciB0PXguYXN5bmNWYWxpZGF0b3JzW29dLmZuLmNhbGwobix1LGUsaSk7cmV0dXJuIHR8fCh0PWguRGVmZXJyZWQoKS5yZWplY3QoKSksaC53aGVuKHQpfTtyZXR1cm4gdS50aGVuKGQsZCl9LHByaW9yaXR5Oi0xfSkseC5vbihcImZvcm06c3VibWl0XCIsZnVuY3Rpb24oKXt4Ll9yZW1vdGVDYWNoZT17fX0pLHMucHJvdG90eXBlLmFkZEFzeW5jVmFsaWRhdG9yPWZ1bmN0aW9uKCl7cmV0dXJuIGQud2Fybk9uY2UoXCJBY2Nlc3NpbmcgdGhlIG1ldGhvZCBgYWRkQXN5bmNWYWxpZGF0b3JgIHRocm91Z2ggYW4gaW5zdGFuY2UgaXMgZGVwcmVjYXRlZC4gU2ltcGx5IGNhbGwgYFBhcnNsZXkuYWRkQXN5bmNWYWxpZGF0b3IoLi4uKWBcIikseC5hZGRBc3luY1ZhbGlkYXRvci5hcHBseSh4LGFyZ3VtZW50cyl9LHguYWRkTWVzc2FnZXMoXCJlblwiLHtkZWZhdWx0TWVzc2FnZTpcIlRoaXMgdmFsdWUgc2VlbXMgdG8gYmUgaW52YWxpZC5cIix0eXBlOntlbWFpbDpcIlRoaXMgdmFsdWUgc2hvdWxkIGJlIGEgdmFsaWQgZW1haWwuXCIsdXJsOlwiVGhpcyB2YWx1ZSBzaG91bGQgYmUgYSB2YWxpZCB1cmwuXCIsbnVtYmVyOlwiVGhpcyB2YWx1ZSBzaG91bGQgYmUgYSB2YWxpZCBudW1iZXIuXCIsaW50ZWdlcjpcIlRoaXMgdmFsdWUgc2hvdWxkIGJlIGEgdmFsaWQgaW50ZWdlci5cIixkaWdpdHM6XCJUaGlzIHZhbHVlIHNob3VsZCBiZSBkaWdpdHMuXCIsYWxwaGFudW06XCJUaGlzIHZhbHVlIHNob3VsZCBiZSBhbHBoYW51bWVyaWMuXCJ9LG5vdGJsYW5rOlwiVGhpcyB2YWx1ZSBzaG91bGQgbm90IGJlIGJsYW5rLlwiLHJlcXVpcmVkOlwiVGhpcyB2YWx1ZSBpcyByZXF1aXJlZC5cIixwYXR0ZXJuOlwiVGhpcyB2YWx1ZSBzZWVtcyB0byBiZSBpbnZhbGlkLlwiLG1pbjpcIlRoaXMgdmFsdWUgc2hvdWxkIGJlIGdyZWF0ZXIgdGhhbiBvciBlcXVhbCB0byAlcy5cIixtYXg6XCJUaGlzIHZhbHVlIHNob3VsZCBiZSBsb3dlciB0aGFuIG9yIGVxdWFsIHRvICVzLlwiLHJhbmdlOlwiVGhpcyB2YWx1ZSBzaG91bGQgYmUgYmV0d2VlbiAlcyBhbmQgJXMuXCIsbWlubGVuZ3RoOlwiVGhpcyB2YWx1ZSBpcyB0b28gc2hvcnQuIEl0IHNob3VsZCBoYXZlICVzIGNoYXJhY3RlcnMgb3IgbW9yZS5cIixtYXhsZW5ndGg6XCJUaGlzIHZhbHVlIGlzIHRvbyBsb25nLiBJdCBzaG91bGQgaGF2ZSAlcyBjaGFyYWN0ZXJzIG9yIGZld2VyLlwiLGxlbmd0aDpcIlRoaXMgdmFsdWUgbGVuZ3RoIGlzIGludmFsaWQuIEl0IHNob3VsZCBiZSBiZXR3ZWVuICVzIGFuZCAlcyBjaGFyYWN0ZXJzIGxvbmcuXCIsbWluY2hlY2s6XCJZb3UgbXVzdCBzZWxlY3QgYXQgbGVhc3QgJXMgY2hvaWNlcy5cIixtYXhjaGVjazpcIllvdSBtdXN0IHNlbGVjdCAlcyBjaG9pY2VzIG9yIGZld2VyLlwiLGNoZWNrOlwiWW91IG11c3Qgc2VsZWN0IGJldHdlZW4gJXMgYW5kICVzIGNob2ljZXMuXCIsZXF1YWx0bzpcIlRoaXMgdmFsdWUgc2hvdWxkIGJlIHRoZSBzYW1lLlwiLGV1dmF0aW46XCJJdCdzIG5vdCBhIHZhbGlkIFZBVCBJZGVudGlmaWNhdGlvbiBOdW1iZXIuXCJ9KSx4LnNldExvY2FsZShcImVuXCIpLChuZXcgZnVuY3Rpb24oKXt2YXIgbj10aGlzLHI9d2luZG93fHxnbG9iYWw7byh0aGlzLHtpc05hdGl2ZUV2ZW50OmZ1bmN0aW9uKHQpe3JldHVybiB0Lm9yaWdpbmFsRXZlbnQmJiExIT09dC5vcmlnaW5hbEV2ZW50LmlzVHJ1c3RlZH0sZmFrZUlucHV0RXZlbnQ6ZnVuY3Rpb24odCl7bi5pc05hdGl2ZUV2ZW50KHQpJiZoKHQudGFyZ2V0KS50cmlnZ2VyKFwiaW5wdXRcIil9LG1pc2JlaGF2ZXM6ZnVuY3Rpb24odCl7bi5pc05hdGl2ZUV2ZW50KHQpJiYobi5iZWhhdmVzT2sodCksaChkb2N1bWVudCkub24oXCJjaGFuZ2UuaW5wdXRldmVudFwiLHQuZGF0YS5zZWxlY3RvcixuLmZha2VJbnB1dEV2ZW50KSxuLmZha2VJbnB1dEV2ZW50KHQpKX0sYmVoYXZlc09rOmZ1bmN0aW9uKHQpe24uaXNOYXRpdmVFdmVudCh0KSYmaChkb2N1bWVudCkub2ZmKFwiaW5wdXQuaW5wdXRldmVudFwiLHQuZGF0YS5zZWxlY3RvcixuLmJlaGF2ZXNPaykub2ZmKFwiY2hhbmdlLmlucHV0ZXZlbnRcIix0LmRhdGEuc2VsZWN0b3Isbi5taXNiZWhhdmVzKX0saW5zdGFsbDpmdW5jdGlvbigpe2lmKCFyLmlucHV0RXZlbnRQYXRjaGVkKXtyLmlucHV0RXZlbnRQYXRjaGVkPVwiMC4wLjNcIjtmb3IodmFyIHQ9W1wic2VsZWN0XCIsJ2lucHV0W3R5cGU9XCJjaGVja2JveFwiXScsJ2lucHV0W3R5cGU9XCJyYWRpb1wiXScsJ2lucHV0W3R5cGU9XCJmaWxlXCJdJ10sZT0wO2U8dC5sZW5ndGg7ZSsrKXt2YXIgaT10W2VdO2goZG9jdW1lbnQpLm9uKFwiaW5wdXQuaW5wdXRldmVudFwiLGkse3NlbGVjdG9yOml9LG4uYmVoYXZlc09rKS5vbihcImNoYW5nZS5pbnB1dGV2ZW50XCIsaSx7c2VsZWN0b3I6aX0sbi5taXNiZWhhdmVzKX19fSx1bmluc3RhbGw6ZnVuY3Rpb24oKXtkZWxldGUgci5pbnB1dEV2ZW50UGF0Y2hlZCxoKGRvY3VtZW50KS5vZmYoXCIuaW5wdXRldmVudFwiKX19KX0pLmluc3RhbGwoKSx4fSk7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1wYXJzbGV5Lm1pbi5qcy5tYXBcbiIsIi8qKlxuICogQ3JlYXRlZCBieSBlbWFrIG9uIDIxLjA0LjE2LlxuICovXG4oZnVuY3Rpb24gKCQpIHtcbiAgICAndXNlIHN0cmljdCc7XG4gICAgJCgnLmF1dGhvci1tb2R1bGUgLm9wZW4nKS5jbGljayhmdW5jdGlvbigpIHtcbiAgICAgICAgJCh0aGlzKS50b2dnbGVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICQodGhpcykucGFyZW50KCkudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgIH0pO1xufSkoalF1ZXJ5KTtcbiIsIihmdW5jdGlvbigkKSB7XG4gIHZhciBpbnN0YW5jZSA9ICQoJy5uZXdzbGV0dGVyLWZvcm0gZm9ybScpLnBhcnNsZXkoKTtcbiAgdmFyIGxhbmcgPSAkKCdodG1sJykuYXR0cignbGFuZycpLnNsaWNlKDAsMik7XG5cblxuICB2YXIgbWVzc2FnZXMgPSB7XG4gICAgJ2RlJzoge1xuICAgICAgJ2VtYWlsJzogJ0JpdHRlIGdlYmVuIFNpZSBlaW5lIGfDvGx0aWdlIEUtTWFpbC1BZHJlc3NlIGFuLicsXG4gICAgICAnZGF0YSc6ICdCaXR0ZSBha3plcHRpZXJlbiBTaWUgZGllIERhdGVuc2NodXR6YmVzdGltbXVuZ2VuLidcbiAgICB9LFxuICAgICdlbic6IHtcbiAgICAgICdlbWFpbCc6ICdQbGVhc2UgZW50ZXIgYSB2YWxpZCBlLW1haWxhZGRyZXNzLicsXG4gICAgICAnZGF0YSc6ICcgUGxlYXNlIGFjY2VwdCB0aGUgcHJpdmFjeSBwb2xpY3kuJ1xuICAgIH1cbiAgfVxuXG5cbiAgJCgnI2Zvcm1fRU1BSUwnKVxuICAgIC5hdHRyKCdyZXF1aXJlZCcsIHRydWUpXG4gICAgLmF0dHIoJ2RhdGEtcGFyc2xleS10eXBlJywgJ2VtYWlsJylcbiAgICAuYXR0cignZGF0YS1wYXJzbGV5LWVycm9yLW1lc3NhZ2UnLCBtZXNzYWdlc1tsYW5nXS5lbWFpbCk7XG5cbiAgJCgnI2Zvcm1fTkxMVUZGVFBPU1QnKVxuICAgIC5hdHRyKCdyZXF1aXJlZCcsIHRydWUpXG4gICAgLmF0dHIoJ2RhdGEtcGFyc2xleS1lcnJvci1tZXNzYWdlJywgbWVzc2FnZXNbbGFuZ10uZGF0YSk7XG5cblxuICB2YXIgY29tbWVudEluc3RhbmNlID0gJCgnI2NvbW1lbnRmb3JtJykucGFyc2xleSgpO1xuICAkKCcjYXV0aG9yJykuYXR0cigncmVxdWlyZWQnLCB0cnVlKTtcbiAgJCgnI2VtYWlsJykuYXR0cigncmVxdWlyZWQnLCB0cnVlKVxuICAgIC5hdHRyKCdkYXRhLXBhcnNsZXktdHlwZScsICdlbWFpbCcpXG4gICAgLmF0dHIoJ2RhdGEtcGFyc2xleS1lcnJvci1tZXNzYWdlJywgbWVzc2FnZXNbbGFuZ10uZW1haWwpO1xuICAgIFxuICAkKCcjY29tbWVudCcpLmF0dHIoJ3JlcXVpcmVkJywgdHJ1ZSkuYXR0cignZGF0YS1wYXJzbGV5LW1pbmxlbmd0aCcsIDMpO1xuXG59KShqUXVlcnkpO1xuIiwialF1ZXJ5KGRvY3VtZW50KS5mb3VuZGF0aW9uKCk7XG4iXX0=
