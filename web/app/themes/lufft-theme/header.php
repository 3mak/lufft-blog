<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
    <!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
    <head>
        <meta charset="<?php bloginfo('charset'); ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/favicon.ico"
              type="image/x-icon">
        <link rel="apple-touch-icon" sizes="144x144"
              href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="114x114"
              href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="72x72"
              href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon"
              href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon.png">
        <meta name="google-site-verification" content="YgxBEAqX0BEN8wy9jHmKBYF2Z4DGfcAc-TQgbF3ioic"/>
        <?php wp_head(); ?>
    </head>
<body <?php body_class(); ?>>
<?php do_action('foundationpress_after_body'); ?>

<?php if (get_theme_mod('wpt_mobile_menu_layout') === 'offcanvas') : ?>
<div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
    <?php get_template_part('template-parts/mobile-off-canvas'); ?>
<?php endif; ?>

<?php do_action('foundationpress_layout_start'); ?>
<?php if (!is_404()) : ?>
    <header id="masthead" class="site-header" role="banner">
        <div class="title-bar" data-responsive-toggle="site-navigation">
            <div class="menu-button-bg">
                <button class="menu-button" type="button" data-toggle="mobile-menu"></button>
            </div>
            <div class="title-bar-title">
                <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">Lufft <span>Blog</span></a>
            </div>
            <?php $languages = apply_filters("wpml_active_languages", null); ?>
            <?php if (count($languages) > 1): ?>
                <div class="switch <?php echo is_default_lang() ? 'is-default' : 'is-active' ?>">
                    <div class="pill"></div>
                    <div class="text">
                        <?php foreach (apply_filters("wpml_active_languages", null) as $lang): ?>
                            <a class="<?php echo ($lang['active']) ? 'is-active' : 'not-active' ?>"
                               href="<?php echo $lang['url'] ?>"><?php echo $lang['language_code'] ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <nav id="site-navigation" class="main-navigation menu-centered" role="navigation">
            <?php foundationpress_top_bar_r(); ?>
            <?php $languages = apply_filters("wpml_active_languages", null); ?>
            <?php if (count($languages) > 1): ?>
                <div class="switch <?php echo is_default_lang() ? 'is-default' : 'is-active' ?>">
                    <div class="pill"></div>
                    <div class="text">
                        <?php foreach ($languages as $lang): ?>
                            <a class="<?php echo ($lang['active']) ? 'is-active' : 'not-active' ?>"
                               href="<?php echo $lang['url'] ?>"><?php echo $lang['language_code'] ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (!get_theme_mod('wpt_mobile_menu_layout') || get_theme_mod('wpt_mobile_menu_layout') === 'topbar') : ?>
                <?php get_template_part('template-parts/mobile-top-bar'); ?>
            <?php endif; ?>
            <div class="line"></div>
        </nav>
    </header>
<?php endif; ?>

    <section class="container">
<?php do_action('foundationpress_after_header');
