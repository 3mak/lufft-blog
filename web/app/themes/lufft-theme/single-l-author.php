<?php
/**
 * The template for displaying all single author and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="single-post" class="author-post-list" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
        <div class="author-info">
            <div class="author-image">
                <div class="author-image-wrapper">
                    <?php echo get_the_post_thumbnail(get_the_ID(), 'thumbnail'); ?>
                </div>
            </div>
            <div class="author-text">
                <h5 class="author-name"><?php the_title() ?></h5>
                <p class="author-bio"><?php the_content() ?></p>
            </div>
        </div>
        <?php wp_reset_postdata(); ?>

        <?php
        $args = array(
            'meta_key' => 'l_post_author',
            'meta_value' => get_the_ID(),
            'post_type' => 'post',
            'post_status' => 'publish',
            'suppress_filters' => 0,
        );
        $posts = get_posts($args);
        ?>
        <?php
            foreach ($posts as $post ) : setup_postdata($post);
                get_template_part('template-parts/content');
            endforeach;
        ?>
        <footer>
            <?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
        </footer>
	</article>

<?php endwhile;?>

<?php do_action( 'foundationpress_after_content' ); ?>
<?php get_sidebar(); ?>
</div>
<?php get_footer();
