<?php
/**
 * Tags template part
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 * @package Foundationpress
 */

?>
<?php if(has_tag()): ?>
<section class="tags">
    <h3><?php _e('More about:', 'foundationpress')?></h3>
    <ul class="tags">
        <?php foreach (get_the_tags() as $tag ) : ?>
            <li><a href="<?php echo get_term_link($tag->term_id)?>"><?php echo $tag->name; ?></a></li>
        <?php endforeach; ?>
    </ul>
</section>
<?php endif; ?>
