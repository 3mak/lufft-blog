<?php
/**
 * Teaser image template part
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 * @package  Foundationpress
 */

?>
<?php
$meta = get_post(get_post_thumbnail_id())
?>

<?php if (strtotime(get_post()->post_date) >= strtotime('30-03-2016') ) : ?>
    <?php if ( has_post_thumbnail() ) : ?>
        <figure class="teaser-image">
            <?php the_post_thumbnail(); ?>
            <div class="hover"></div>
            <figcaption>
                <?php print($meta->post_excerpt); ?>
            </figcaption>
        </figure>
    <?php endif; ?>
<?php endif; ?>
