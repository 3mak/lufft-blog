<?php
/**
 * Related Links
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 * @package Foundationpress
 */

$related_links = get_post_meta( get_the_ID(), 'related_links_group', true );
?>
<?php if ( ! empty($related_links) ) : ?>
<ul class="related-links">
    <h3><?php _e('Related links', 'foundationpress')?></h3>
<?php foreach ($related_links as $link ) : ?>
    <li><a target="_blank" href="<?php echo $link['l_post_link_url']?>"><?php echo $link['l_post_link_name']?></a></li>
<?php endforeach; ?>
</ul>
<?php endif; ?>