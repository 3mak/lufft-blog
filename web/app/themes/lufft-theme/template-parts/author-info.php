<?php
/**
 * Show Author info
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 * @package  Foundationpress
 */

$author_id = get_post_meta(get_the_ID(), 'l_post_author', true);
$author = WP_POST::get_instance($author_id);
$role = wp_get_post_terms($author_id, 'l-role');
if (isset($role[0])) {
    $linked_page_id = get_term_meta($role[0]->term_id, 'l_role_page', true);
}

?>
<?php if ($author) : ?>
    <div class="author-info">
        <h3 class="author-header"><?php _e('Information about the author', 'foundationpress') ?></h3>
        <?php if (isset($role[0])): ?>
            <a href="<?php echo get_permalink($linked_page_id); ?>" target="_blank">
        <?php endif; ?>
            <div class="author-content">
                <div class="author-image">
                    <div class="author-image-wrapper">
                        <?php echo get_the_post_thumbnail($author, 'thumbnail'); ?>
                    </div>
                </div>
                <div class="author-text">
                    <h5 class="author-name"><?php echo $author->post_title; ?></h5>
                    <p class="author-bio"><?php echo $author->post_content; ?></p>
                </div>
            </div>
            <?php if (isset($role[0])): ?>
        </a>
    <?php endif; ?>
    </div>
<?php endif; ?>
<div class="guest-author-wdgt">
    <?php _e('Become a guest author at Lufft', 'foundationpress'); ?> <a class="round-button"
                                                                         href="<?php the_permalink(10657); ?>"><?php _e('Make inquiry ›', 'foundationpress'); ?></a>
</div>
