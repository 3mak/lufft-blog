<?php
/**
 * Event Item template
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 * @package  Foundationpress
 */

?>
<?php
$start_date = get_post_meta(get_the_ID(), 'l_calender_start_date', true);
$end_date = get_post_meta(get_the_ID(), 'l_calender_end_date', true);
?>

<div class="event-column">
    <div class="event">
        <div class="event-date">
            <time datetime="<?php echo date('c', $start_date); ?>"><?php echo date(__('d.m.Y'), $start_date)?></time> -
            <time datetime="<?php echo date('c', $end_date); ?>"><?php echo date(__('d.m.Y'), $end_date)?></time>
        </div>
        <div class="event-title">
            <?php the_title() ?>
        </div>
        <div class="event-location">
            <?php echo get_post_meta(get_the_ID(), 'l_calender_location', true); ?>
        </div>
    </div>
</div>
