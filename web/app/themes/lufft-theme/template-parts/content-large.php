<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class('blogpost-entry large'); ?>>
	<div class="image">
		<a href="<?php the_permalink(); ?>">
		<?php the_post_thumbnail('medium'); ?>
	</a>
	</div>
	<div class="text-container">
		<header>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		  <?php lufft_entry_meta(); ?>
		</header>
		<div class="entry-content">
			<?php the_content( __( 'Continue reading ›', 'foundationpress' ) ); ?>
			<div class="line"></div>
		</div>
	</div>
</div>
